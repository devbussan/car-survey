package com.example.a0426611017.carsurvey.Object.Personal.FullForm;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.a0426611017.carsurvey.MainFunction.DatePicker;
import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataJobData;
import com.example.a0426611017.carsurvey.R;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;

/**
 * Created by 0426611017 on 2/9/2018.
 */

public class ObjectDataJobDataPending {
    private View view;
    private Context context;
    private DatePicker datePicker = new DatePicker();

    private EditText profesionName, jobPosition, jobStatus,
            namaPerusahaan, industryTypeName, employmentEstablishment,
            jobAddress, jobRt, jobRw, jobZipCode, jobKelurahan,
            jobKecamatan, jobCity, jobPhone1;

    public ObjectDataJobDataPending(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        ViewGroup container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_data_job_data_pending, container, false);

        setField();
    }

    public View getView() {
        return view;
    }

    public void setField() {
        profesionName = view.findViewById(R.id.edit_text_profesion_name_pending);
        jobPosition = view.findViewById(R.id.edit_text_job_position_pending);
        jobStatus = view.findViewById(R.id.edit_text_job_status_pending);
        namaPerusahaan = view.findViewById(R.id.edit_text_nama_perusahaan_usaha_pending);
        industryTypeName = view.findViewById(R.id.edit_text_industry_type_name_pending);
        employmentEstablishment = view.findViewById(R.id.edit_text_employment_establishment_date_pending);
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, employmentEstablishment, context);

        jobAddress = view.findViewById(R.id.edit_text_job_address_pending);
        jobRt = view.findViewById(R.id.edit_text_job_rt_pending);
        jobRw = view.findViewById(R.id.edit_text_job_rw_pending);
        jobZipCode = view.findViewById(R.id.edit_text_job_zip_code_pending);
        jobKelurahan = view.findViewById(R.id.edit_text_job_kelurahan_pending);
        jobKecamatan = view.findViewById(R.id.edit_text_job_kecamatan_pending);
        jobCity = view.findViewById(R.id.edit_text_job_city_pending);
        jobPhone1 = view.findViewById(R.id.edit_text_job_phone_1_pending);
        jobPhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (jobPhone1.getText().length() < 7) {
                    jobPhone1.setError("Minimum 7 digits");
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 7) {
                    jobPhone1.setError("Minimum 7 digits");
                }
            }
        });
    }

    public void setForm(ModelDataJobData modelDataJobData) {
        profesionName.setText(modelDataJobData.getProffesionname());
        jobPosition.setText(modelDataJobData.getJobpos());
        jobStatus.setText(modelDataJobData.getJobstat());
        namaPerusahaan.setText(modelDataJobData.getCompanyname());
        industryTypeName.setText(modelDataJobData.getIndustrytype());

        if (modelDataJobData.getEstablishmentdtm() != null && !modelDataJobData.getEstablishmentdtm().equals("")) {
            modelDataJobData.setEstablishmentdtm(
                    modelDataJobData.getEstablishmentdtm().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelDataJobData.getEstablishmentdtm().substring(4, 6))]
                            + " " + modelDataJobData.getEstablishmentdtm().substring(0, 4)
            );
        }
        employmentEstablishment.setText(modelDataJobData.getEstablishmentdtm());

        jobAddress.setText(modelDataJobData.getJobaddr());
        jobRt.setText(modelDataJobData.getJobrt());
        jobRw.setText(modelDataJobData.getJobrw());
        jobZipCode.setText(modelDataJobData.getJobzipode());
        jobKelurahan.setText(modelDataJobData.getJobkelurahan());
        jobKecamatan.setText(modelDataJobData.getJobkecamatan());
        jobCity.setText(modelDataJobData.getJobcity());
        jobPhone1.setText(modelDataJobData.getJobphone());
    }

    public ModelDataJobData getForm() {
        ModelDataJobData modelDataJobData = new ModelDataJobData();

        modelDataJobData.setProffesionname(profesionName.getText().toString());
        modelDataJobData.setJobpos(jobPosition.getText().toString());
        modelDataJobData.setJobstat(jobStatus.getText().toString());
        modelDataJobData.setCompanyname(namaPerusahaan.getText().toString());
        modelDataJobData.setIndustrytype(industryTypeName.getText().toString());

        modelDataJobData.setEstablishmentdtm(employmentEstablishment.getText().toString());
        if (modelDataJobData.getEstablishmentdtm() != null) {
            if (!modelDataJobData.getEstablishmentdtm().equals("")) {
                modelDataJobData.setEstablishmentdtm(
                        modelDataJobData.getEstablishmentdtm().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(modelDataJobData.getEstablishmentdtm().split(" ")[1])),
                                2,
                                "0"
                        )
                                + modelDataJobData.getEstablishmentdtm().split(" ")[0]
                );
            }
        }

        modelDataJobData.setJobaddr(jobAddress.getText().toString());
        modelDataJobData.setJobrt(jobRt.getText().toString());
        modelDataJobData.setJobrw(jobRw.getText().toString());
        modelDataJobData.setJobzipode(jobZipCode.getText().toString());
        modelDataJobData.setJobkelurahan(jobKelurahan.getText().toString());
        modelDataJobData.setJobkecamatan(jobKecamatan.getText().toString());
        modelDataJobData.setJobcity(jobCity.getText().toString());
        modelDataJobData.setJobphone(jobPhone1.getText().toString());

        modelDataJobData.setModified(true);

        return modelDataJobData;
    }

    public ModelDataJobData getComplete() {
        ModelDataJobData modelDataJobData = getForm();
        modelDataJobData.setComplete(true);

        if (modelDataJobData.getProffesionname().length() == 0) {
            profesionName.setError("Profesion Name is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getProffesionname().trim().length() == 0) {
            profesionName.setError("Profesion Name not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getJobpos().length() == 0) {
            jobPosition.setError("Job Position is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobpos().trim().length() == 0) {
            jobPosition.setError("Job Position not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getJobstat().length() == 0) {
            jobStatus.setError("Job Status is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobstat().trim().length() == 0) {
            jobStatus.setError("Job Status not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getCompanyname().length() == 0) {
            namaPerusahaan.setError("Nama Perusahaan is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getCompanyname().trim().length() == 0) {
            namaPerusahaan.setError("Nama Perusahaan not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getIndustrytype().length() == 0) {
            industryTypeName.setError("Industry Yype Name is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getIndustrytype().trim().length() == 0) {
            industryTypeName.setError("Industry Yype Name not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getEstablishmentdtm().length() == 0) {
            employmentEstablishment.setError("Employment Establishment is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getEstablishmentdtm().trim().length() == 0) {
            employmentEstablishment.setError("Employment Establishment not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getJobaddr().length() == 0) {
            jobAddress.setError("Job Address is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobaddr().trim().length() == 0) {
            jobAddress.setError("Job Address not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getJobrt().length() == 0) {
            jobRt.setError("Job RT is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobrt().trim().length() == 0) {
            jobRt.setError("Job RT not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getJobrw().length() == 0) {
            jobRw.setError("Job RW is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobrw().trim().length() == 0) {
            jobRw.setError("Job RW not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getJobzipode().length() == 0) {
            jobZipCode.setError("Job Zip Code is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobzipode().trim().length() == 0) {
            jobZipCode.setError("Job Zip Code not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getJobkelurahan().length() == 0) {
            jobKelurahan.setError("Job Kelurahan is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobkelurahan().trim().length() == 0) {
            jobKelurahan.setError("Job Kelurahan not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getJobkecamatan().length() == 0) {
            jobKecamatan.setError("Job Kecamatan is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobkecamatan().trim().length() == 0) {
            jobKecamatan.setError("Job Kecamatan not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getJobcity().length() == 0) {
            jobCity.setError("Job City is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobcity().trim().length() == 0) {
            jobCity.setError("Job City is not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        }

        if (modelDataJobData.getJobphone().length() == 0) {
            jobPhone1.setError("Job Phone is required");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobphone().trim().length() == 0) {
            jobPhone1.setError("Job Phone not allowed whitespaces only");
            modelDataJobData.setComplete(false);
        } else if (modelDataJobData.getJobphone().trim().length() < 7) {
            jobPhone1.setError("Minimum 7 digits");
            modelDataJobData.setComplete(false);
        }

        return modelDataJobData;
    }
}
