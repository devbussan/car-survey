package com.example.a0426611017.carsurvey.Model.Financial;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 0414831216 on 2/9/2018.
 */

public class ModelFinance implements Serializable{

    @SerializedName("ORDER_ID")
    private String orderId;

    @SerializedName("FINANCIAL_ID")
    private String pembiayaanId;

    @SerializedName("DEALERNAME")
    private String namaDealer;

    @SerializedName("MERK")
    private String merk;

    @SerializedName("TYPEFINANCING")
    private String typePembiayaan;

    @SerializedName("SERIAL")
    private String serial;

    @SerializedName("COLOR")
    private String warna;

    @SerializedName("ASSETCOND")
    private String asetCondition;

    @SerializedName("OWNERNAME")
    private String namaBpkb;

    @SerializedName("BPKBCITYISSUED")
    private String bpkbCityIssued;

    @SerializedName("ASSETPRICE")
    private String otr;

    @SerializedName("CASHBACK")
    private String cashback;

    @SerializedName("PERCENTAGEDP")
    private String dpPercent;

    @SerializedName("DPAMOUNT")
    private String dpAmount;

    @SerializedName("TENOR")
    private String tenor;

    @SerializedName("PAYFREQ")
    private String paymentRequest;

    @SerializedName("ASURANSI")
    private String asuransi;

    @SerializedName("ANGSURAN")
    private String angsuran;

    @SerializedName("RATE")
    private String rate;

    @SerializedName("ADMIN")
    private String admin;

    @SerializedName("PROVISI")
    private String provisi;

    @SerializedName("PEMBAYARAN")
    private String pembayaran;

    @SerializedName("CUSTNOTIF")
    private String custNotifikasi;

    @SerializedName("USRCRT")
    private String userCreate;

    @SerializedName("FINANCING")
    private String responseFinancing;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getResponseFinancing() {
        return responseFinancing;
    }

    public void setResponseFinancing(String responseFinancing) {
        this.responseFinancing = responseFinancing;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getNamaDealer() {
        return namaDealer;
    }

    public void setNamaDealer(String namaDealer) {
        this.namaDealer = namaDealer;
    }

    public String getMerk() {
        return merk;
    }

    public void setMerk(String merk) {
        this.merk = merk;
    }

    public String getTypePembiayaan() {
        return typePembiayaan;
    }

    public void setTypePembiayaan(String typePembiayaan) {
        this.typePembiayaan = typePembiayaan;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getWarna() {
        return warna;
    }

    public void setWarna(String warna) {
        this.warna = warna;
    }

    public String getAsetCondition() {
        return asetCondition;
    }

    public void setAsetCondition(String asetCondition) {
        this.asetCondition = asetCondition;
    }

    public String getNamaBpkb() {
        return namaBpkb;
    }

    public void setNamaBpkb(String namaBpkb) {
        this.namaBpkb = namaBpkb;
    }

    public String getBpkbCityIssued() {
        return bpkbCityIssued;
    }

    public void setBpkbCityIssued(String bpkbCityIssued) {
        this.bpkbCityIssued = bpkbCityIssued;
    }

    public String getPaymentRequest() {
        return paymentRequest;
    }

    public void setPaymentRequest(String paymentRequest) {
        this.paymentRequest = paymentRequest;
    }

    public String getAsuransi() {
        return asuransi;
    }

    public void setAsuransi(String asuransi) {
        this.asuransi = asuransi;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public String getProvisi() {
        return provisi;
    }

    public void setProvisi(String provisi) {
        this.provisi = provisi;
    }

    public String getPembayaran() {
        return pembayaran;
    }

    public void setPembayaran(String pembayaran) {
        this.pembayaran = pembayaran;
    }

    public String getCustNotifikasi() {
        return custNotifikasi;
    }

    public void setCustNotifikasi(String custNotifikasi) {
        this.custNotifikasi = custNotifikasi;
    }

    public String getPembiayaanId() {
        return pembiayaanId;
    }

    public void setPembiayaanId(String pembiayaanId) {
        this.pembiayaanId = pembiayaanId;
    }

    public String getTenor() {
        return tenor;
    }

    public void setTenor(String tenor) {
        this.tenor = tenor;
    }

    public String getOtr() {
        return otr;
    }

    public void setOtr(String otr) {
        this.otr = otr;
    }

    public String getCashback() {
        return cashback;
    }

    public void setCashback(String cashback) {
        this.cashback = cashback;
    }

    public String getDpPercent() {
        return dpPercent;
    }

    public void setDpPercent(String dpPercent) {
        this.dpPercent = dpPercent;
    }

    public String getDpAmount() {
        return dpAmount;
    }

    public void setDpAmount(String dpAmount) {
        this.dpAmount = dpAmount;
    }

    public String getAngsuran() {
        return angsuran;
    }

    public void setAngsuran(String angsuran) {
        this.angsuran = angsuran;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
