package com.example.a0426611017.carsurvey.Object.Personal.DetailMiniForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalSGMini;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by 0426591017 on 2/19/2018.
 */

public class ObjectDataPersonalSGDetailOrder {
    private ViewGroup container;
    private View view;
    private Context context;

    private TextView valueCustomerNameSg, valueIdTypeSg,
            valueIdNumberSg, valueFamilyCardNumSg,
            valuePersonalIdImageSg, valuePersonalFamilyCardImageSg;

    public ObjectDataPersonalSGDetailOrder(ViewGroup container, LayoutInflater inflater) {
        this.container = container;
        this.view = inflater.inflate(R.layout.fragment_data_personal_sgdetail, container, false);
        this.context = this.container.getContext();

        setField();
    }

    public View getView() {
        return view;
    }

    private void setField(){
        valueCustomerNameSg = view.findViewById(R.id.value_customer_name_sg_detail_order);
        valueIdTypeSg = view.findViewById(R.id.value_id_type_sg_detail_order);
        valueIdNumberSg = view.findViewById(R.id.value_id_number_sg_detail_order);
        valueFamilyCardNumSg = view.findViewById(R.id.value_family_card_number_sg_detail_order);
        valuePersonalIdImageSg = view.findViewById(R.id.value_id_image_sg_detail_order);
        valuePersonalFamilyCardImageSg = view.findViewById(R.id.value_family_card_image_sg_detail_order);
    }

    public void setForm(ModelPersonalSGMini modelPersonalSGMini){
        valueCustomerNameSg.setText(modelPersonalSGMini.getFullnamesg());
        valueIdTypeSg.setText(modelPersonalSGMini.getIdtypesg());
        valueIdNumberSg.setText(modelPersonalSGMini.getIdnosg());

        if(modelPersonalSGMini.getIdimgurlsg() != null){
            if(!modelPersonalSGMini.getIdimgurlsg().equals("")){
                valuePersonalIdImageSg.setText(modelPersonalSGMini.getIdimgurlsg().split("/")[6]);
            }else{
                valuePersonalIdImageSg.setText(modelPersonalSGMini.getIdimgurlsg());
            }
        }else {
            valuePersonalIdImageSg.setText(modelPersonalSGMini.getIdimgsg());
        }

        valueFamilyCardNumSg.setText(modelPersonalSGMini.getKknosg());

        if(modelPersonalSGMini.getKkimgurlsg() != null){
            if(!modelPersonalSGMini.getKkimgurlsg().equals("")){
                valuePersonalFamilyCardImageSg.setText(modelPersonalSGMini.getKkimgurlsg().split("/")[6]);
            }else{
                valuePersonalFamilyCardImageSg.setText(modelPersonalSGMini.getKkimgurlsg());
            }
        }else {
            valuePersonalFamilyCardImageSg.setText(modelPersonalSGMini.getKkimgsg());
        }

        PreviewImage previewImage = new PreviewImage();
        if (modelPersonalSGMini.getIdimgurlsg() != null)
            if (!modelPersonalSGMini.getIdimgurlsg().equals(""))
        previewImage.setPreviewImage(modelPersonalSGMini.getIdimgurlsg(), context, valuePersonalIdImageSg);

        if (modelPersonalSGMini.getKkimgurlsg() != null)
            if (!modelPersonalSGMini.getKkimgurlsg().equals(""))
        previewImage.setPreviewImage(modelPersonalSGMini.getKkimgurlsg(), context, valuePersonalFamilyCardImageSg);
    }
}
