package com.example.a0426611017.carsurvey.Fragment.Personal.FullForm;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataMainData;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.Object.Personal.FullForm.ObjectDataMainDataPending;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalOther;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class FragmentDataMainDataPending extends Fragment {
    private View view;
    private Context context;
    private String nameFile, filePath;
    private int cameraIdxButton = 0;
    private TakePicture takePicture = new TakePicture();
    private PreviewImage previewImage = new PreviewImage();
    private TextView fileNameFamilyCardImage,
            fileNameFamilyCardImageUrl;

    private ObjectDataMainDataPending objectDataMainDataPending;
    private ModelDataMainData modelDataMainData = new ModelDataMainData();
    private ModelDDLPersonal ddlPersonal = new ModelDDLPersonal();
    private FileFunction fileFunction = new FileFunction();

    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private String personalCustDataId = null;
    private String kkNo = "";
    private String kkImgUrl = "";
    private String name = "";
    private Gson gson = new Gson();
    private boolean isNew = true;

    private ResponseDetailPersonalOther responseDetailPersonalOther = new ResponseDetailPersonalOther();

    public FragmentDataMainDataPending newInstance(ModelDataMainData modelDataMainData, boolean isNew, ModelDDLPersonal ddlPersonal, String custId, String name, String kkNo, String kkImgUrl) {
        FragmentDataMainDataPending fragmentDataMainDataPending = new FragmentDataMainDataPending();
        Bundle args = new Bundle();
        args.putSerializable("modelOtherPersonal", modelDataMainData);
        args.putSerializable("ddlPersonal", ddlPersonal);
        args.putString("personalCustDataId", custId);
        args.putBoolean(Constans.KEY_IS_NEW, isNew);
        args.putString("name", name);
        args.putString("kkNoId", kkNo);
        args.putString("kkImgUrl", kkImgUrl);
        fragmentDataMainDataPending.setArguments(args);
        return fragmentDataMainDataPending;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelDataMainData = (ModelDataMainData) getArguments().getSerializable("modelOtherPersonal");
            ddlPersonal = (ModelDDLPersonal) getArguments().getSerializable("ddlPersonal");
            personalCustDataId = getArguments().getString("personalCustDataId");
            isNew = getArguments().getBoolean(Constans.KEY_IS_NEW);
            name = getArguments().getString("name");
            kkNo = getArguments().getString("kkNoId");
            kkImgUrl = getArguments().getString("kkImgUrl");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        objectDataMainDataPending = new ObjectDataMainDataPending(container, inflater, ddlPersonal);
        view = objectDataMainDataPending.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        Log.d("KK ID", "return "+ kkNo);
        Log.d("KK IMG", "return "+ kkImgUrl);

        if(modelDataMainData.isModified()){
            objectDataMainDataPending.setForm(modelDataMainData);
        }else{
            if (!isNew) {
                getDetailOther(name, personalCustDataId);
            }else{
                modelDataMainData.setKkno(kkNo);
                modelDataMainData.setKkimgurl(kkImgUrl);

                objectDataMainDataPending.setForm(modelDataMainData);
            }
        }

        fileNameFamilyCardImage = objectDataMainDataPending.getNamePersonalFamilyCardNumberImage();
        fileNameFamilyCardImageUrl = objectDataMainDataPending.getNamePersonalFamilyCardNumberImageUrl();
        Button buttonFamilyCardImage = objectDataMainDataPending.getButtonFamilyCardImage();

        callCamera(buttonFamilyCardImage, 1);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            objectDataMainDataPending.setEnabledButtonImage();
        }

        return view;
    }

    private void callCamera(Button button, int idx) {
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(v, idxButton);

            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                objectDataMainDataPending.setEnabledButtonImage();
            }
        }
    }

    public void takePicture(View view, int idx) {
        this.cameraIdxButton = idx;
        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        filePath = takePicture.getFilePath();
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Toast.makeText(context,requestCode+":"+resultCode,Toast.LENGTH_SHORT).show();
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                switch (cameraIdxButton) {
                    case 1:
                        /*fileNameFamilyCardImage.setText(nameFile);*/
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        objectDataMainDataPending.setFilePersonalFamilyCardNumberImage(nameFile);
                        objectDataMainDataPending.setFilePersonalFamilyCardNumberImageUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNameFamilyCardImage);
                        break;
                    default:
                        break;


                }
            }
        }
    }

    public ModelDataMainData getForm(){
        return objectDataMainDataPending.getForm();
    }

    public void setForm(ModelDataMainData modelDataMainData){
        objectDataMainDataPending.setForm(modelDataMainData);
    }

    public ModelDataMainData getComplete(){
        return objectDataMainDataPending.getComplete();
    }

    private void getDetailOther(final String name, final String idcustdata){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalOther> otherDetail = apiInterface.getDetailPersonalOther(name, idcustdata);
            otherDetail.enqueue(new Callback<ResponseDetailPersonalOther>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalOther> call, Response<ResponseDetailPersonalOther>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataMainData = response.body().getModelDataMainData();
                        Log.d("Result", gson.toJson(modelDataMainData));
                        Log.d("KK ID", "return call "+ kkNo);
                        Log.d("KK IMG", "return call "+ kkImgUrl);

                        modelDataMainData.setKkno(kkNo);
                        modelDataMainData.setKkimgurl(kkImgUrl);

                        setForm(modelDataMainData);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalOther = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalOther.class);
                            error = responseDetailPersonalOther.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailOther(name, idcustdata);
                                    }
                                });
                                b.show();
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.personal_other_full_edit), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailOther(name, idcustdata);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailOther(name, idcustdata);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalOther> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailOther(name, idcustdata);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailOther(name, idcustdata);
                }
            });
            b.show();
            Log.e("Error","No Internet Connectivity");
            loadingDialog.dismiss();
        }
    }

}