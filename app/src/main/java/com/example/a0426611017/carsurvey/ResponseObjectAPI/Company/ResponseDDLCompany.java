package com.example.a0426611017.carsurvey.ResponseObjectAPI.Company;

import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLCompany;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 08/02/2018.
 */

public class ResponseDDLCompany {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDDLCompany ddlCompanies;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDDLCompany getDdlCompanies() {
        return ddlCompanies;
    }

    public void setDdlCompanies(ModelDDLCompany ddlCompanies) {
        this.ddlCompanies = ddlCompanies;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
