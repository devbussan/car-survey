package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.Personal.ModelDataJobData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426591017 on 3/6/2018.
 */

public class ResponseDetailPersonalJob {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDataJobData modelDataJobData;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDataJobData getModelDataJobData() {
        return modelDataJobData;
    }

    public void setModelDataJobData(ModelDataJobData modelDataJobData) {
        this.modelDataJobData = modelDataJobData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
