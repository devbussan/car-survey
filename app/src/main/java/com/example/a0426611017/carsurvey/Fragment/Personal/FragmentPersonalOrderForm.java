package com.example.a0426611017.carsurvey.Fragment.Personal;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiPersonal;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.R;

public class FragmentPersonalOrderForm extends Fragment {

    private View view;
    private Context context;
    private String search;
    private int tab;
    private int menu;

    private ApiPersonal apiPersonal;
    private ProgressDialog progressDialog;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private String adminId = null;

    public FragmentPersonalOrderForm newInstance(String search, int tab, int menu) {
        FragmentPersonalOrderForm personalTab = new FragmentPersonalOrderForm();
        Bundle args = new Bundle();
        args.putString("search", search);
        args.putInt("tab", tab);
        args.putInt("menu", menu);
        personalTab.setArguments(args);
        return personalTab;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_company_complete_grid, container, false);
        context = container.getContext();
        search = getArguments().getString("search");
        tab = getArguments().getInt("tab");
        menu = getArguments().getInt("menu");

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);

        apiPersonal = new ApiPersonal(view, context, progressDialog);

        adminId = context.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, Context.MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefreshCompanyComplete);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                searchItem("");
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        searchItem(search);

        return view;
    }

    private void searchItem(String search){

        GridView gridView = (GridView) view.findViewById(R.id.gridView);
        TextView textViewError = view.findViewById(R.id.text_view_info_error);

        switch (tab){
            case Constans.TAB_DRAFT:
                if(apiPersonal.getListPersonal(adminId, String.valueOf(Constans.STATUS_DRAFT), tab, menu, search, gridView, textViewError, false)){
                    Log.d("Info", "CIEEEE SUKSES NIYE");
                } else {
                    Log.d("Info", "CARI ERRRORNYA BURUAN");
                }
                break;
            case Constans.TAB_PENDING:
                if(apiPersonal.getListPersonal(adminId, String.valueOf(Constans.STATUS_PENDING),tab , menu, search, gridView, textViewError, false)){
                    Log.d("Info", "CIEEEE SUKSES NIYE");
                } else {
                    Log.d("Info", "CARI ERRRORNYA BURUAN");
                }
                break;
            case Constans.TAB_RETURN_ORDER:
                if(apiPersonal.getListPersonal(adminId, String.valueOf(Constans.STATUS_RETURN_ORDER), tab, menu, search, gridView, textViewError, false)){
                    Log.d("Info", "CIEEEE SUKSES NIYE");
                } else {
                    Log.d("Info", "CARI ERRRORNYA BURUAN");
                }
                break;
            case Constans.TAB_REJECT_ORDER:
                if(apiPersonal.getListPersonal(adminId, String.valueOf(Constans.STATUS_REJECT_ORDER),tab , menu, search, gridView, textViewError, false)){
                    Log.d("Info", "CIEEEE SUKSES NIYE");
                } else {
                    Log.d("Info", "CARI ERRRORNYA BURUAN");
                }
                break;
            case Constans.TAB_CLEAR:
                if(apiPersonal.getListPersonal(adminId, String.valueOf(Constans.STATUS_CLEAR), tab , menu, search, gridView, textViewError, false)){
                    Log.d("Info", "CIEEEE SUKSES NIYE");
                } else {
                    Log.d("Info", "CARI ERRRORNYA BURUAN");
                }
                break;
            default:
                break;
        }
    }
}
