package com.example.a0426611017.carsurvey.Fragment.Survey;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLSurvey;
import com.example.a0426611017.carsurvey.Model.Survey.ModelSurvey;
import com.example.a0426611017.carsurvey.Object.Survey.ObjectSurvey;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseGetCompanySurvey;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class FragmentSurvey extends Fragment {

    private int menu;
    private View view;
    private Context context;
    private ModelDDLSurvey ddlSurvey;
    private ModelSurvey modelSurvey;
    private ObjectSurvey objectSurvey;
    private int idxButton;

    private TextView fileNameIdMotorImg, fileNameIdMotorImgUrl,
            fileNameIdMobilImg, fileNameIdMobilImgUrl;
    private Button buttonIdMotor, buttonIdMobil;
    private TakePicture takePicture = new TakePicture();
    private PreviewImage previewImage = new PreviewImage();
    private FileFunction fileFunction = new FileFunction();

    private String companyOrder="0";
    private String nameFile;
    private String name = "";

    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private ResponseGetCompanySurvey responseGetCompanySurvey;
    private Gson gson = new Gson();

    public static FragmentSurvey newInstance(int menu, ModelDDLSurvey ddlSurvey, ModelSurvey modelSurvey, String companyOrder) {
        FragmentSurvey fragment = new FragmentSurvey();
        Bundle args = new Bundle();
        args.putInt(Constans.KEY_MENU, menu);
        args.putSerializable("modelSurvey", modelSurvey);
        args.putSerializable("ddlSurvey", ddlSurvey);
        args.putString("companyOrder", companyOrder);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            menu = getArguments().getInt(Constans.KEY_MENU);
            ddlSurvey = (ModelDDLSurvey) getArguments().getSerializable("ddlSurvey");
            modelSurvey = (ModelSurvey) getArguments().getSerializable("modelSurvey");
            companyOrder = getArguments().getString("companyOrder");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectSurvey = new ObjectSurvey(container, inflater, ddlSurvey);
        view = objectSurvey.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if(menu == Constans.MENU_PERSONAL){
            objectSurvey.setFormForPersonal();
            name = Constans.PARAM_WS_PERSONAL;
        }else{
            objectSurvey.setFormForCompany();
            name = Constans.PARAM_WS_COMPANY;
        }

//        if(modelSurvey != null && modelSurvey.isModified()){
//            objectSurvey.setForm(modelSurvey);
//        }

        fileNameIdMobilImg = objectSurvey.getTextViewIdMobilImg();
        fileNameIdMobilImgUrl = objectSurvey.getTextViewIdMobilImgUrl();
        buttonIdMobil = objectSurvey.getButtonIdMobil();

        fileNameIdMotorImg = objectSurvey.getTextViewIdMotorImg();
        fileNameIdMotorImgUrl = objectSurvey.getTextViewIdMotorImgUrl();
        buttonIdMotor = objectSurvey.getButtonIdMotor();

        callCamera(buttonIdMobil, 1);
        callCamera(buttonIdMotor, 2);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            objectSurvey.setEnabledButtonImage();
        }

        if (modelSurvey.isModified()) {
            setForm(modelSurvey);
        }else{
            getDetailSurvey(companyOrder, name);
        }

        return view;
    }

    private void callCamera(Button button, int idx) {
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(idxButton);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                buttonIdMobil.setEnabled(true);
                buttonIdMotor.setEnabled(true);
            }
        }
    }

    public void takePicture(int idx) {

        this.idxButton = idx;
        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        startActivityForResult(intent, 100);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {

                switch (idxButton) {
                    case 1:
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                Toast.makeText(context, "Error : " + e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectSurvey.setTextViewIdMobilImg(nameFile);
                        objectSurvey.setTextViewIdMobilImgUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNameIdMobilImg);
                        break;
                    case 2:
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                Toast.makeText(context, "Error : " + e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectSurvey.setTextViewIdMotorImg(nameFile);
                        objectSurvey.setTextViewIdMotorImgUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNameIdMotorImg);
                        break;
                    default:
                        break;
                }

            }
        }
    }

    public ModelSurvey getForm(){
        return objectSurvey.getForm();
    }

    public void setForm(ModelSurvey modelSurvey){
        objectSurvey.setForm(modelSurvey);
    }

    public ModelSurvey getComplete(){
        return objectSurvey.getComplete();
    }

    public void getDetailSurvey(final String idCompanyOrder, final String name) {
        loadingDialog.show();
        if(InternetConnection.checkConnection(context)){
            Call<ResponseGetCompanySurvey> getDetailCompany = apiInterface.getDetailSurveyCompany(idCompanyOrder,name);
            getDetailCompany.enqueue(new Callback<ResponseGetCompanySurvey>() {
                @Override
                public void onResponse(Call<ResponseGetCompanySurvey> call, Response<ResponseGetCompanySurvey> response) {
                    if(response.isSuccessful()){
                        responseGetCompanySurvey=response.body();
                        Log.d("Response", gson.toJson(responseGetCompanySurvey));
                        if(responseGetCompanySurvey.getStatus().equals("success")){
                            setForm(responseGetCompanySurvey.getModelSurvey());
                            loadingDialog.dismiss();
                        }
                    }else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseGetCompanySurvey = gson.fromJson(response.errorBody().string(),ResponseGetCompanySurvey.class);
                            error = responseGetCompanySurvey.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailSurvey(idCompanyOrder, name);
                                    }
                                });
                                b.show();
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.fragment_survey_pending), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailSurvey(idCompanyOrder, name);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    }else {
                        Log.e("Other Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailSurvey(idCompanyOrder, name);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseGetCompanySurvey> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailSurvey(idCompanyOrder, name);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        }else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailSurvey(idCompanyOrder, name);
                }
            });
            b.show();
            Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
            loadingDialog.dismiss();
        }
    }
}
