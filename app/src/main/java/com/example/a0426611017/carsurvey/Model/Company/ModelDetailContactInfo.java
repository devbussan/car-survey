package com.example.a0426611017.carsurvey.Model.Company;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by c201017001 on 26/02/2018.
 */

public class ModelDetailContactInfo implements Serializable {

    @SerializedName("COMPANY_CONTACT_ID")
    private String contactOrderId;

    @SerializedName("COMPANY_ORDER_ID")
    private String companyOrderId;

    @SerializedName("CONTACT_NAME")
    private String contactName;

    @SerializedName("CONTACT_JOB_POS")
    private String jobPosition;

    @SerializedName("CONTACT_ID_NUMBER")
    private String idNumber;

    private String idImage;

    @SerializedName("CONTACT_NPWP")
    private String npwp;

    private String npwpImage;

    @SerializedName("CONTACT_MOBILE_PHN_1")
    private String mobilePhone1;

    @SerializedName("CONTACT_MOBILE_PHN_2")
    private String mobilePhone2;

    @SerializedName("CONTACT_PHN_1")
    private String phone1;

    @SerializedName("CONTACT_PHN_2")
    private String phone2;

    @SerializedName("CONTACT_EMAIL_1")
    private String email1;

    @SerializedName("CONTACT_EMAIL_2")
    private String email2;

    @SerializedName("CONTACT_INFO_FLAG")
    private String infoFlag;

    @SerializedName("CONTACT_ID_IMG_URL")
    private String idImageUrl;

    @SerializedName("CONTACT_NPWP_IMG_URL")
    private String npwpImageUrl;

    private boolean isModified = false;
    private boolean isComplete = false;

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getIdImageUrl() {
        return idImageUrl;
    }

    public void setIdImageUrl(String idImageUrl) {
        this.idImageUrl = idImageUrl;
    }

    public String getNpwpImageUrl() {
        return npwpImageUrl;
    }

    public void setNpwpImageUrl(String npwpImageUrl) {
        this.npwpImageUrl = npwpImageUrl;
    }

    public String getContactOrderId() {
        return contactOrderId;
    }

    public void setContactOrderId(String contactOrderId) {
        this.contactOrderId = contactOrderId;
    }

    public String getCompanyOrderId() {
        return companyOrderId;
    }

    public void setCompanyOrderId(String companyOrderId) {
        this.companyOrderId = companyOrderId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getJobPosition() {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition) {
        this.jobPosition = jobPosition;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getIdImage() {
        return idImage;
    }

    public void setIdImage(String idImage) {
        this.idImage = idImage;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getNpwpImage() {
        return npwpImage;
    }

    public void setNpwpImage(String npwpImage) {
        this.npwpImage = npwpImage;
    }

    public String getMobilePhone1() {
        return mobilePhone1;
    }

    public void setMobilePhone1(String mobilePhone1) {
        this.mobilePhone1 = mobilePhone1;
    }

    public String getMobilePhone2() {
        return mobilePhone2;
    }

    public void setMobilePhone2(String mobilePhone2) {
        this.mobilePhone2 = mobilePhone2;
    }

    public String getPhone1() {
        return phone1;
    }

    public void setPhone1(String phone1) {
        this.phone1 = phone1;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public String getEmail1() {
        return email1;
    }

    public void setEmail1(String email1) {
        this.email1 = email1;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getInfoFlag() {
        return infoFlag;
    }

    public void setInfoFlag(String infoFlag) {
        this.infoFlag = infoFlag;
    }
}
