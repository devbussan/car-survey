package com.example.a0426611017.carsurvey.Model.Personal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 0426611017 on 2/9/2018.
 */

public class ModelDataEmergencyContact implements Serializable {
    @SerializedName("CUST_DATA_ID")
    private String cust_data_id;

    @SerializedName("PERSONAL_EMERGENCY_ID")
    private String personal_emergency_id;

    @SerializedName("EMERGENCYNAME")
    private String emergencyname;

    @SerializedName("RELATION")
    private String relation;

    @SerializedName("EMERGENCYMPHN1")
    private String emergencymphn1;

    @SerializedName("EMERGENCYMPHN2")
    private String emergencymphn2;

    @SerializedName("EMERGENCYADDR")
    private String emergencyaddr;

    @SerializedName("EMERGENCYRT")
    private String emergencyrt;

    @SerializedName("EMERGENCYRW")
    private String emergencyrw;

    @SerializedName("EMERGENCYZIPCODE")
    private String emergencyzipcode;

    @SerializedName("EMERGENCYKELURAHAN")
    private String emergencykelurahan;

    @SerializedName("EMERGENCYKECAMATAN")
    private String emergencykecamatan;

    @SerializedName("EMERGENCYCITY")
    private String emergencycity;

    @SerializedName("EMERGENCYPHN")
    private String emergencyphn;

    @SerializedName("USRCRT")
    private String usrcrt;

    @SerializedName("STATUS")
    private String status;

    @SerializedName("PERSONAL_EMERGENCY")
    private String personal_emergency_return;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getCust_data_id() {
        return cust_data_id;
    }

    public void setCust_data_id(String cust_data_id) {
        this.cust_data_id = cust_data_id;
    }

    public String getPersonal_emergency_id() {
        return personal_emergency_id;
    }

    public void setPersonal_emergency_id(String personal_emergency_id) {
        this.personal_emergency_id = personal_emergency_id;
    }

    public String getEmergencyname() {
        return emergencyname;
    }

    public void setEmergencyname(String emergencyname) {
        this.emergencyname = emergencyname;
    }

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getEmergencymphn1() {
        return emergencymphn1;
    }

    public void setEmergencymphn1(String emergencymphn1) {
        this.emergencymphn1 = emergencymphn1;
    }

    public String getEmergencymphn2() {
        return emergencymphn2;
    }

    public void setEmergencymphn2(String emergencymphn2) {
        this.emergencymphn2 = emergencymphn2;
    }

    public String getEmergencyaddr() {
        return emergencyaddr;
    }

    public void setEmergencyaddr(String emergencyaddr) {
        this.emergencyaddr = emergencyaddr;
    }

    public String getEmergencyrt() {
        return emergencyrt;
    }

    public void setEmergencyrt(String emergencyrt) {
        this.emergencyrt = emergencyrt;
    }

    public String getEmergencyrw() {
        return emergencyrw;
    }

    public void setEmergencyrw(String emergencyrw) {
        this.emergencyrw = emergencyrw;
    }

    public String getEmergencyzipcode() {
        return emergencyzipcode;
    }

    public void setEmergencyzipcode(String emergencyzipcode) {
        this.emergencyzipcode = emergencyzipcode;
    }

    public String getEmergencykelurahan() {
        return emergencykelurahan;
    }

    public void setEmergencykelurahan(String emergencykelurahan) {
        this.emergencykelurahan = emergencykelurahan;
    }

    public String getEmergencykecamatan() {
        return emergencykecamatan;
    }

    public void setEmergencykecamatan(String emergencykecamatan) {
        this.emergencykecamatan = emergencykecamatan;
    }

    public String getEmergencycity() {
        return emergencycity;
    }

    public void setEmergencycity(String emergencycity) {
        this.emergencycity = emergencycity;
    }

    public String getEmergencyphn() {
        return emergencyphn;
    }

    public void setEmergencyphn(String emergencyphn) {
        this.emergencyphn = emergencyphn;
    }

    public String getUsrcrt() {
        return usrcrt;
    }

    public void setUsrcrt(String usrcrt) {
        this.usrcrt = usrcrt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getPersonal_emergency_return() {
        return personal_emergency_return;
    }

    public void setPersonal_emergency_return(String personal_emergency_return) {
        this.personal_emergency_return = personal_emergency_return;
    }
}

