package com.example.a0426611017.carsurvey.Object.Company.MiniForm;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelDetailCompanyMiniForm;

/**
 * Created by c201017001 on 12/02/2018.
 */

public class ObjcetCompanyInfo {

    private View view;
    private ViewGroup container;
    private LayoutInflater inflater;
    private Context context;

    private EditText editTextCustomerName, editTextNpwpNumber;

    public ObjcetCompanyInfo(ViewGroup container, LayoutInflater inflater) {
        this.inflater = inflater;
        this.container = container;
        this.context = container.getContext();
        this.view = this.inflater.inflate(R.layout.fragment_company_info_new_order, container, false);
        setField();
    }

    public View getView() {
        return view;
    }

    private void setField() {
        editTextCustomerName = view.findViewById(R.id.edit_text_company_name_add_new_company_info);
        editTextNpwpNumber = view.findViewById(R.id.edit_text_npwp_number_add_new_company_info);
        editTextNpwpNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 15) {
                    editTextNpwpNumber.setError("Minimum 15 digits");
                }
            }
        });
    }

    public ModelDetailCompanyMiniForm getForm() {
        ModelDetailCompanyMiniForm companyInfo = new ModelDetailCompanyMiniForm();

        companyInfo.setCompanyName(editTextCustomerName.getText().toString());
        companyInfo.setNpwpNo(editTextNpwpNumber.getText().toString());
        companyInfo.setModified(true);

        return companyInfo;
    }

    public ModelDetailCompanyMiniForm getFormComplete() {
        ModelDetailCompanyMiniForm companyInfo = getForm();

        companyInfo.setComplete(true);
        if (companyInfo.getCompanyName().length() == 0) {
            editTextCustomerName.setError("Required");
            companyInfo.setComplete(false);
        } else if (companyInfo.getCompanyName().trim().length() == 0) {
            editTextCustomerName.setError("Whitespaces only detected");
            companyInfo.setComplete(false);
        }

        if (companyInfo.getNpwpNo().length() == 0) {
            companyInfo.setComplete(false);
            editTextNpwpNumber.setError("Required");
        } else if (companyInfo.getNpwpNo().trim().length() == 0) {
            editTextNpwpNumber.setError("Whitespaces only detected");
            companyInfo.setComplete(false);
        } else if (companyInfo.getNpwpNo().trim().length() != 15) {
            editTextNpwpNumber.setError("Minimum and maximum 15 digits");
            companyInfo.setComplete(false);
        }

        return companyInfo;
    }

    public void setForm(ModelDetailCompanyMiniForm companyInfo) {
        editTextCustomerName.setText(companyInfo.getCompanyName());
        editTextNpwpNumber.setText(companyInfo.getNpwpNo());
    }

}
