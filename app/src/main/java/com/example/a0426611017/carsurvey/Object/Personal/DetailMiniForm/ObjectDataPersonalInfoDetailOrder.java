package com.example.a0426611017.carsurvey.Object.Personal.DetailMiniForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalInfoMini;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by 0426591017 on 2/19/2018.
 */

public class ObjectDataPersonalInfoDetailOrder {
    private ViewGroup container;
    private View view;
    private Context context;

    private TextView valueCustomerName, valueIdType,
            valueIdNumber, valueFamilyCardNum, valuePersonalIdImage,
            valuePersonalFamilyCardImage;

    public ObjectDataPersonalInfoDetailOrder(ViewGroup container, LayoutInflater inflater) {
        this.container = container;
        this.view = inflater.inflate(R.layout.fragment_data_personal_info_detail, container, false);
        this.context = this.container.getContext();

        setField();
    }

    public View getView() {
        return view;
    }

    private void setField(){
        valueCustomerName = view.findViewById(R.id.value_customer_name_detail_order);
        valueIdType = view.findViewById(R.id.value_id_type_detail_order);
        valueIdNumber = view.findViewById(R.id.value_id_number_detail_order);
        valueFamilyCardNum = view.findViewById(R.id.value_family_card_number_detail_order);
        valuePersonalIdImage = view.findViewById(R.id.value_id_image_detail_order);
        valuePersonalFamilyCardImage = view.findViewById(R.id.value_family_card_image_detail_order);
    }

    public void setForm(ModelPersonalInfoMini modelPersonalInfoMini){
        valueCustomerName.setText(modelPersonalInfoMini.getFullname());
        valueIdType.setText(modelPersonalInfoMini.getIdtype());
        valueIdNumber.setText(modelPersonalInfoMini.getIdno());

        if(modelPersonalInfoMini.getIdimgurl() != null){
            if(!modelPersonalInfoMini.getIdimgurl().equals("")){
                valuePersonalIdImage.setText(modelPersonalInfoMini.getIdimgurl().split("/")[6]);
            }else{
                valuePersonalIdImage.setText(modelPersonalInfoMini.getIdimgurl());
            }
        }else{
            valuePersonalIdImage.setText(modelPersonalInfoMini.getIdimg());
        }

        valueFamilyCardNum.setText(modelPersonalInfoMini.getKkno());

        if(modelPersonalInfoMini.getKkimgurl() != null){
            if(!modelPersonalInfoMini.getKkimgurl().equals("")){
                valuePersonalFamilyCardImage.setText(modelPersonalInfoMini.getKkimgurl().split("/")[6]);
            }else{
                valuePersonalFamilyCardImage.setText(modelPersonalInfoMini.getKkimgurl());
            }
        }else{
            valuePersonalFamilyCardImage.setText(modelPersonalInfoMini.getKkimg());
        }


        PreviewImage previewImage = new PreviewImage();
        if (modelPersonalInfoMini.getIdimgurl() != null)
            if (!modelPersonalInfoMini.getIdimgurl().equals(""))
                previewImage.setPreviewImage(modelPersonalInfoMini.getIdimgurl(), context, valuePersonalIdImage);

        if (modelPersonalInfoMini.getKkimgurl() != null)
            if (!modelPersonalInfoMini.getKkimgurl().equals(""))
                previewImage.setPreviewImage(modelPersonalInfoMini.getKkimgurl(), context, valuePersonalFamilyCardImage);
    }
}
