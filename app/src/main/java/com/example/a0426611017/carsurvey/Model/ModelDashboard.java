package com.example.a0426611017.carsurvey.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 13/03/2018.
 */

public class ModelDashboard {

    @SerializedName("DRAFTMINI")
    private String draftmini;

    @SerializedName("PENDINGMINI")
    private String pendingmini;

    @SerializedName("REJECTMINI")
    private String rejectmini;

    @SerializedName("RETURNMINI")
    private String returnmini;

    @SerializedName("CLEARMINI")
    private String clearmini;

    @SerializedName("CHECKHQFULL")
    private String checkhqfull;

    @SerializedName("REJECTFULL")
    private String rejectfull;

    @SerializedName("RETURNFULL")
    private String returnfull;

    @SerializedName("APPROVEFULL")
    private String approvefull;

    public String getDraftmini() {
        return draftmini;
    }

    public void setDraftmini(String draftmini) {
        this.draftmini = draftmini;
    }

    public String getPendingmini() {
        return pendingmini;
    }

    public void setPendingmini(String pendingmini) {
        this.pendingmini = pendingmini;
    }

    public String getRejectmini() {
        return rejectmini;
    }

    public void setRejectmini(String rejectmini) {
        this.rejectmini = rejectmini;
    }

    public String getReturnmini() {
        return returnmini;
    }

    public void setReturnmini(String returnmini) {
        this.returnmini = returnmini;
    }

    public String getClearmini() {
        return clearmini;
    }

    public void setClearmini(String clearmini) {
        this.clearmini = clearmini;
    }

    public String getCheckhqfull() {
        return checkhqfull;
    }

    public void setCheckhqfull(String checkhqfull) {
        this.checkhqfull = checkhqfull;
    }

    public String getRejectfull() {
        return rejectfull;
    }

    public void setRejectfull(String rejectfull) {
        this.rejectfull = rejectfull;
    }

    public String getReturnfull() {
        return returnfull;
    }

    public void setReturnfull(String returnfull) {
        this.returnfull = returnfull;
    }

    public String getApprovefull() {
        return approvefull;
    }

    public void setApprovefull(String approvefull) {
        this.approvefull = approvefull;
    }
}
