package com.example.a0426611017.carsurvey;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.ModelRequestLogin;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.ResponseLogin;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySplashScreen extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private ResponseLogin responseLogin = new ResponseLogin();
    private Gson gson = new Gson();
    private ApiInterface apiInterface;
    private String adminUsername = "";
    private String adminPassword = "";
    private FileFunction fileFunction = new FileFunction();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_splash_screen);
        ImageView imageView = findViewById(R.id.image_splash);

        fileFunction.deleteAllFileInDir();
        Log.d("Info", "Locale Default : "+Locale.getDefault());
        Locale localeDefault = new Locale("en","US");
        Locale.setDefault(localeDefault);
        Log.d("Info", "Locale Default : "+Locale.getDefault());

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(ActivitySplashScreen.this);
        progressDialog.setMessage("Try Connect to Server\n" +
                "Please Wait ...");
        progressDialog.setCancelable(false);

        Date dateNow = new Date();
        Date dateToChange;
        try {
            dateToChange = new SimpleDateFormat("yyyyMMdd").parse("20180402");
        } catch (ParseException e) {
            dateToChange = new Date();
            e.printStackTrace();
        }
        Log.d("Info", "Date Now : "+dateNow);
        Log.d("Info", "Date Change Logo : "+dateToChange);
        Log.d("Info", "Is Before Date Change : "+dateNow.before(dateToChange));
        Log.d("Info", "Is Same With Before Date Change : "+dateNow.compareTo(dateToChange));
        Log.d("Info", "Is After Date Change : "+dateNow.after(dateToChange));

        if(dateNow.compareTo(dateToChange) == 1 || dateNow.after(dateToChange)){
            imageView.setImageDrawable(getDrawable(R.drawable.logobafbaru));
        }

        SharedPreferences.Editor editor = getSharedPreferences(Constans.KeySharedPreference.DATE, MODE_PRIVATE).edit();
        editor.putInt(Constans.KEY_COMPARE_DATE, dateNow.compareTo(dateToChange));
        editor.putBoolean(Constans.KEY_AFTER_DATE, dateNow.after(dateToChange));
        editor.apply();

        Log.d("Info", "Welcome To Mobile Car Survey");
        Log.d("Info", "5 Seconds until connection reaches your device");

        String isActive = getSharedPreferences(Constans.KeySharedPreference.IS_ACTIVE, MODE_PRIVATE).getString(Constans.KEY_IS_ACTIVE, "0");
        if (isActive.equals("1")) {
            adminUsername = getSharedPreferences(Constans.KeySharedPreference.USERNAME, MODE_PRIVATE).getString(Constans.KEY_USERNAME, "");
            adminPassword = getSharedPreferences(Constans.KeySharedPreference.PASSWORD, MODE_PRIVATE).getString(Constans.KEY_PASSWORD, "");
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        }
//        else {
            new CheckingConnection(progressDialog).execute(adminUsername, adminPassword);
//        }
//        if (InternetConnection.checkConnection(ActivitySplashScreen.this)) {
//            Log.d("Info", "all connection deployed");
//
//        } else {
//
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
//        if (requestCode == 0) {
//            new CheckingConnection(progressDialog).execute(adminUsername, adminPassword);
//        } else {
//            new CheckingConnection(progressDialog).execute(adminUsername, adminPassword);
//        }
    }

    public void doLogin(String string1, String string2) {

        if (string1.equals("") || string2.equals("")) {
            Snackbar.make(findViewById(R.id.activity_login), R.string.kosong, Snackbar.LENGTH_LONG).show();
        } else {
            Log.d("Password", string2);
            ModelRequestLogin modelRequestLogin = new ModelRequestLogin();
            modelRequestLogin.setAdminUsername(string1);
            modelRequestLogin.setAdminPassword(string2);
            authLogin(modelRequestLogin);
        }
    }

    private void authLogin(final ModelRequestLogin modelRequestLogin) {
        progressDialog.show();
        if (InternetConnection.checkConnection(this)) {
            if (InternetConnection.checkConnection(this)) {
                final Call<ResponseLogin> authLogin = apiInterface.authLoginMini(modelRequestLogin);
                authLogin.enqueue(new Callback<ResponseLogin>() {
                    @Override
                    public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin>
                            response) {
                        if (response.isSuccessful()) {
                            responseLogin = response.body();

                            if (responseLogin.getStatus().equals("success")) {
                                Intent i = new Intent(ActivitySplashScreen.this, MainActivityDrawer.class);
                                SharedPreferences.Editor editor = getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, MODE_PRIVATE).edit();
                                editor.putString(Constans.KEY_ADMIN_ID, responseLogin.getModelResponseLogin().getAdminId());
                                editor.apply();
                                editor.commit();

                                editor = getSharedPreferences(Constans.KeySharedPreference.USERNAME, MODE_PRIVATE).edit();
                                editor.putString(Constans.KEY_USERNAME, modelRequestLogin.getAdminUsername());
                                editor.apply();

                                editor = getSharedPreferences(Constans.KeySharedPreference.PASSWORD, MODE_PRIVATE).edit();
                                editor.putString(Constans.KEY_PASSWORD, modelRequestLogin.getAdminPassword());
                                editor.apply();

                                editor = getSharedPreferences(Constans.KeySharedPreference.IS_ACTIVE, MODE_PRIVATE).edit();
                                editor.putString(Constans.KEY_IS_ACTIVE, "1");
                                editor.apply();

                                i.putExtra(Constans.KEY_MENU, 0);
                                startActivity(i);
                                finish();
                            }

                            progressDialog.dismiss();
                        } else if (response.code() == 404) {
                            String error = "";
                            try {
                                responseLogin = gson.fromJson(response.errorBody().string(), ResponseLogin.class);
                                error = responseLogin.getMessage();
                            } catch (IOException e) {
                                error = e.getMessage();
                                e.printStackTrace();
                            }
                            Log.e("Error", error);
                            Snackbar.make(findViewById(R.id.splash_screen), error,
                                    Snackbar.LENGTH_LONG)
                                    .show();
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                            Snackbar.make(findViewById(R.id.splash_screen), R.string.error_api,
                                    Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseLogin> call, Throwable t) {
                        Log.e("Error Retrofit", t.toString());
                        Snackbar.make(findViewById(R.id.splash_screen), t.toString(),
                                Snackbar.LENGTH_LONG)
                                .show();
                        progressDialog.dismiss();
                    }
                });
            } else {
                progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySplashScreen.this);
                builder.setTitle("Confirm Dialog");
                builder.setMessage("Cannot connect to server, try again ?");
                builder.setCancelable(false);
                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                finishAffinity();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        authLogin(modelRequestLogin);
                        Log.e("info", "YES");
                    }
                });
                builder.show();
            }
        }
    }

    private class CheckingConnection extends AsyncTask<String, Void, Boolean> {

        private ProgressDialog progressDialog;
        private String username = "";
        private String password = "";

        public CheckingConnection(ProgressDialog progressDialog) {
            this.progressDialog = progressDialog;
        }

        @Override
        protected void onPreExecute() {
            this.progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... strings) {
            Log.d("Info", "smash them");

            this.username = strings[0];
            this.password = strings[1];
            if (InternetConnection.checkConnection(ActivitySplashScreen.this)) {
                Log.d("Info", "all connection deployed");
                return true;
            } else {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                progressDialog.dismiss();
                Log.d("Error", "true");
                if (!this.username.equals("") && !this.password.equals("")) {
                    doLogin(username, password);
                } else {
                    Intent i = new Intent(ActivitySplashScreen.this, ActivityLogin.class);
                    startActivity(i);
                    finish();
                }
            } else {
                Log.d("Error", "false");
                progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySplashScreen.this);
                builder.setTitle("Confirm Dialog");
                builder.setMessage("Cannot connect to server, try again ?");
                builder.setCancelable(false);
                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                finishAffinity();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = getIntent();
                        finish();
                        startActivity(intent);
                        Log.e("info", "YES");
                    }
                });
                builder.show();
            }
        }
    }
}