package com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonal;

import com.example.a0426611017.carsurvey.R;

/**
 * Created by c201017001 on 04/01/2018.
 */

public class ObjectDataPersonalComplete {
    private View view;
    private ViewGroup container;
    private Context context;

    private TextView namePersonalIdImage, namePersonalNpwpImage,
            customerName, idNumber,
            idExpiredDate, birthPlace, dateBirthDate,
            npwp, motherMaidenName, customerModel, idType, gender;

    public ObjectDataPersonalComplete(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_data_personal_complete, viewGroup, false);

        setField();
    }

    public View getView() {
        return view;
    }

    private void setField() {
        customerModel = view.findViewById(R.id.value_customer_model);
        idType = view.findViewById(R.id.value_id_type);
        customerName = view.findViewById(R.id.value_customer_name);
        namePersonalIdImage = view.findViewById(R.id.value_id_image);
        idNumber = view.findViewById(R.id.value_id_number);
        idExpiredDate = view.findViewById(R.id.value_id_expired_date);
        gender = view.findViewById(R.id.value_gender);
        birthPlace = view.findViewById(R.id.value_birth_place);
        dateBirthDate = view.findViewById(R.id.value_birth_date);
        npwp = view.findViewById(R.id.value_npwp);
        namePersonalNpwpImage = view.findViewById(R.id.value_npwp_image);
        motherMaidenName = view.findViewById(R.id.value_mother_maiden_name);
    }

    public void setForm(final ModelDataPersonal modelDataPersonal) {
        customerModel.setText(modelDataPersonal.getCustmodel());
        idType.setText(modelDataPersonal.getIdtype());
        customerName.setText(modelDataPersonal.getCustname());

        if (modelDataPersonal.getIdimgurl() != null){
            if (!modelDataPersonal.getIdimgurl().equals("")){
                namePersonalIdImage.setText(modelDataPersonal.getIdimgurl().split("/")[6]);
            }else{
                namePersonalIdImage.setText(modelDataPersonal.getIdimgurl());
            }
        }else{
            namePersonalIdImage.setText(modelDataPersonal.getIdimg());
        }

        idNumber.setText(modelDataPersonal.getIdnumber());
        if (modelDataPersonal.getIdexpdate() != null && !modelDataPersonal.getIdexpdate().equals("")) {
            modelDataPersonal.setIdexpdate(
                    modelDataPersonal.getIdexpdate().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelDataPersonal.getIdexpdate().substring(4, 6))]
                            + " " + modelDataPersonal.getIdexpdate().substring(0, 4)
            );
        }
        idExpiredDate.setText(modelDataPersonal.getIdexpdate());

        birthPlace.setText(modelDataPersonal.getBirthplace());
        gender.setText(modelDataPersonal.getGenderid());
        if (modelDataPersonal.getBirthdate() != null && !modelDataPersonal.getBirthdate().equals("")) {
            modelDataPersonal.setBirthdate(
                    modelDataPersonal.getBirthdate().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelDataPersonal.getBirthdate().substring(4, 6))]
                            + " " + modelDataPersonal.getBirthdate().substring(0, 4)
            );
        }
        dateBirthDate.setText(modelDataPersonal.getBirthdate());
        npwp.setText(modelDataPersonal.getNpwpnum());

        if (modelDataPersonal.getNpwpimgurl() != null){
            if (!modelDataPersonal.getNpwpimgurl().equals("")){
                namePersonalNpwpImage.setText(modelDataPersonal.getNpwpimgurl().split("/")[6]);
            }else{
                namePersonalNpwpImage.setText(modelDataPersonal.getNpwpimgurl());
            }
        }else{
            namePersonalNpwpImage.setText(modelDataPersonal.getNpwpimg());
        }

        motherMaidenName.setText(modelDataPersonal.getMothermaidenname());

        PreviewImage previewImage = new PreviewImage();
        if (modelDataPersonal.getIdimgurl() != null)
            if (!modelDataPersonal.getIdimgurl().equals(""))
                previewImage.setPreviewImage(modelDataPersonal.getIdimgurl(), context, namePersonalIdImage);

        if (modelDataPersonal.getNpwpimgurl() != null)
            if (!modelDataPersonal.getNpwpimgurl().equals(""))
                previewImage.setPreviewImage(modelDataPersonal.getNpwpimgurl(), context, namePersonalNpwpImage);
    }
}
