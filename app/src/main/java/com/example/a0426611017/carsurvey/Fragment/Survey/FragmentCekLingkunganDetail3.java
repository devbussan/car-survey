package com.example.a0426611017.carsurvey.Fragment.Survey;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Survey.ModelCekLingkungan;
import com.example.a0426611017.carsurvey.Object.Survey.ObjectCekLingkunganDetail3;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseDetailSurveyEnviCompany;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCekLingkunganDetail3 extends Fragment {
    private int menu;
    private View view;
    private Context context;
    private ModelCekLingkungan modelCekLingkungan;
    private ObjectCekLingkunganDetail3 objectCekLingkunganDetail;

    private String surveyenvid;
    private String surveyid;
    private String name;
    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;
    private Gson gson = new Gson();

    private ResponseDetailSurveyEnviCompany responseDetailSurveyEnviCompany = new ResponseDetailSurveyEnviCompany();

    public static FragmentCekLingkunganDetail3 newInstance(int menu, String surveyenvid, String surveyid, String name) {
        FragmentCekLingkunganDetail3 fragment = new FragmentCekLingkunganDetail3();
        Bundle args = new Bundle();
        args.putInt(Constans.KEY_MENU, menu);
        args.putString("surveyenvid", surveyenvid);
        args.putString("surveyid",surveyid);
        args.putString("name", name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            menu = getArguments().getInt(Constans.KEY_MENU);
            surveyenvid = getArguments().getString("surveyenvid");
            surveyid = getArguments().getString("surveyid");
            name = getArguments().getString("name");
        }
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectCekLingkunganDetail = new ObjectCekLingkunganDetail3(container, inflater);
        view = objectCekLingkunganDetail.getView();
        context = container.getContext();

        if(menu == Constans.MENU_PERSONAL){
            objectCekLingkunganDetail.setFormForPersonal();
        }else{
            objectCekLingkunganDetail.setFormForCompany();
        }
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if(surveyenvid != null) {
            getDetailEnvMent(surveyenvid, surveyid, name);
        }

        return view;
    }

    public void setForm(ModelCekLingkungan modelCekLingkungan){
        objectCekLingkunganDetail.setForm(modelCekLingkungan);
    }

    private void getDetailEnvMent(final String envIdSur, final String idSurvey, String pName){
        loadingDialog.show();
        if(InternetConnection.checkConnection(context)){
            Call<ResponseDetailSurveyEnviCompany> getDetailEnvMent = apiInterface.getDetailEnviront(pName,envIdSur,idSurvey);
            getDetailEnvMent.enqueue(new Callback<ResponseDetailSurveyEnviCompany>() {
                @Override
                public void onResponse(Call<ResponseDetailSurveyEnviCompany> call, Response<ResponseDetailSurveyEnviCompany> response) {
                    if(response.isSuccessful()){
                        modelCekLingkungan = response.body().getModelCekLingkungan();
                        Log.d("Log", gson.toJson(modelCekLingkungan));
                        setForm(modelCekLingkungan);
                        loadingDialog.dismiss();

                    }else if (response.code() == 404) {
                        String error = "";
                        try {

                            responseDetailSurveyEnviCompany = gson.fromJson(response.errorBody().string(),ResponseDetailSurveyEnviCompany.class);
                            error = responseDetailSurveyEnviCompany.getMessage();
                            if(!error.equals("Data Not Found")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage("Cannot Connect to Server \n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailEnvMent(envIdSur, idSurvey, name);
                                    }
                                });
                                b.show();
                            }else{
                                Snackbar.make(view.findViewById(R.id.cek_lingkungan3), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage("Cannot Connect to Server \n" +
                                    "Please try Again ?");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailEnvMent(envIdSur, idSurvey, name);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    }else {
                        Log.e("Other Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server \n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailEnvMent(envIdSur, idSurvey, name);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailSurveyEnviCompany> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server \n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailEnvMent(envIdSur, idSurvey, name);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        }    else {
            Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server \n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailEnvMent(envIdSur, idSurvey, name);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }

    }

}
