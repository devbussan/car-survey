package com.example.a0426611017.carsurvey.Model.Personal.Mini;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426611017 on 2/26/2018.
 */

public class ModelRequestPersonalSendHq {

    @SerializedName("CMOLOCATION")
    @Expose
    private String cmoLocation;

    @SerializedName("SURVEYSTAT")
    @Expose
    private String surveyStat;

    @SerializedName("ISREADCMO")
    @Expose
    private String isReadCmo;

    @SerializedName("ISREADADMIN")
    @Expose
    private String isReadAdmin;

    @SerializedName("HQCOMMENT")
    @Expose
    private String hqComment;

    @SerializedName("HQUSER")
    @Expose
    private String hqUser;

    @SerializedName("HQRSPDTM")
    @Expose
    private String hqrPsdm;

    @SerializedName("SVYPLANDTM")
    @Expose
    private String svyPlandtm;

    @SerializedName("SVYACTUALDTM")
    @Expose
    private String svyActualdtm;

    @SerializedName("FINSUBMINTDTM")
    @Expose
    private String finSubmintdmtm;

    @SerializedName("STATUS")
    @Expose
    private String status;

    @SerializedName("USRUPD")
    @Expose
    private String usrPd;

    @SerializedName("LOG_PERSONAL")
    @Expose
    private String log_personal;

    public String getLog_personal() {
        return log_personal;
    }

    public void setLog_personal(String log_personal) {
        this.log_personal = log_personal;
    }

    public String getCmoLocation() {return cmoLocation;}

    public void setCmoLocation(String cmoLocation) {this.cmoLocation = cmoLocation;}

    public String getSurveyStat() {return surveyStat;}

    public void setSurveyStat(String surveyStat) {this.surveyStat = surveyStat;}

    public String getIsReadCmo() {return isReadCmo;}

    public void setIsReadCmo(String isReadCmo) {this.isReadCmo = isReadCmo;}

    public String getIsReadAdmin() {return isReadAdmin;}

    public void setIsReadAdmin(String isReadAdmin) {this.isReadAdmin = isReadAdmin;}

    public String getHqComment() {return hqComment;}

    public void setHqComment(String hqComment) {this.hqComment = hqComment;}

    public String getHqUser() {return hqUser;}

    public void setHqUser(String hqUser) {this.hqUser = hqUser;}

    public String getHqrPsdm() {return hqrPsdm;}

    public void setHqrPsdm(String hqrPsdm) {this.hqrPsdm = hqrPsdm;}

    public String getSvyPlandtm() {return svyPlandtm;}

    public void setSvyPlandtm(String svyPlandtm) {this.svyPlandtm = svyPlandtm;}

    public String getSvyActualdtm() {return svyActualdtm;}

    public void setSvyActualdtm(String svyActualdtm) {this.svyActualdtm = svyActualdtm;}

    public String getFinSubmintdmtm() {return finSubmintdmtm;}

    public void setFinSubmintdmtm(String finSubmintdmtm) {this.finSubmintdmtm = finSubmintdmtm;}

    public String getStatus() {return status;}

    public void setStatus(String status) {this.status = status;}

    public String getUsrPd() {return usrPd;}

    public void setUsrPd(String usrPd) {this.usrPd = usrPd;}

}
