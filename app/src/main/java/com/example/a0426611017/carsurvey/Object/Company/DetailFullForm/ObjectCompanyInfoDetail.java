package com.example.a0426611017.carsurvey.Object.Company.DetailFullForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.Model.Company.ModelCompanyInfo;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by c201017001 on 04/01/2018.
 */

public class ObjectCompanyInfoDetail {

    private ViewGroup container;
    private LayoutInflater inflater;
    private View view;
    private Context context;
    private TextView customerName, npwp, industryType, numOfEmployee, establismentDate,
            customerModel, companyType;

    public ObjectCompanyInfoDetail(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.view = inflater.inflate(R.layout.company_detail_company_info, viewGroup, false);
        this.inflater = inflater;
        findView();
    }

    public View getView() {
        return view;
    }

    private void findView() {
        customerName = view.findViewById(R.id.companyName);
        npwp = view.findViewById(R.id.npwp);

        customerModel = view.findViewById(R.id.cutomerModel);
        companyType = view.findViewById(R.id.companyType);

        industryType = view.findViewById(R.id.industryType);
        numOfEmployee = view.findViewById(R.id.numOfEmployee);
        establismentDate = view.findViewById(R.id.establismentDate);

    }

    public void setForm(ModelCompanyInfo modelCompanyInfo) {
        customerName.setText(modelCompanyInfo.getCompanyName());
        npwp.setText(modelCompanyInfo.getNpwp());
        customerModel.setText(modelCompanyInfo.getCustomerModel());
        companyType.setText(modelCompanyInfo.getCompanyType());

        industryType.setText(modelCompanyInfo.getIndustryType());
        numOfEmployee.setText(modelCompanyInfo.getNumOfEmployee());

        if (modelCompanyInfo.getEstablismentDate() != null) {
            if (!modelCompanyInfo.getEstablismentDate().equals("")) {
                modelCompanyInfo.setEstablismentDateFormat(
                        modelCompanyInfo.getEstablismentDate().substring(6, 8)
                                + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelCompanyInfo.getEstablismentDate().substring(4, 6))]
                                + " " + modelCompanyInfo.getEstablismentDate().substring(0, 4)
                );
            }
            establismentDate.setText(modelCompanyInfo.getEstablismentDateFormat());
        }

    }
}
