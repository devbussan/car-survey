package com.example.a0426611017.carsurvey.Company.DetailMiniForm;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentCompanyInfoDetailOrder;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentContactInfo2DetailOrder;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentContactInfo3DetailOrder;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentContactInfo4DetailOrder;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentContactInfo5DetailOrder;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentContactInfoDetailOrder;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelDetailCompanyMiniForm;
import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailCompanyMiniForm;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseContactOrderId;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseDetailContactInfo;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCompanyDetailOrder extends AppCompatActivity {

    private Button buttonCompanyInfo;
    private Button buttonContactInfo;
    private Button buttonContactInfo2;
    private Button buttonContactInfo3;
    private Button buttonContactInfo4;
    private Button buttonContactInfo5;

    private FragmentCompanyInfoDetailOrder fragmentCompanyInfoDetailOrder = new FragmentCompanyInfoDetailOrder();
    private FragmentContactInfoDetailOrder fragmentContactInfoDetailOrder = new FragmentContactInfoDetailOrder();
    private FragmentContactInfo2DetailOrder fragmentContactInfoDetailOrder2 = new FragmentContactInfo2DetailOrder();
    private FragmentContactInfo3DetailOrder fragmentContactInfoDetailOrder3 = new FragmentContactInfo3DetailOrder();
    private FragmentContactInfo4DetailOrder fragmentContactInfoDetailOrder4 = new FragmentContactInfo4DetailOrder();
    private FragmentContactInfo5DetailOrder fragmentContactInfoDetailOrder5 = new FragmentContactInfo5DetailOrder();

    private ModelDetailCompanyMiniForm modelDetailCompanyMiniForm = new ModelDetailCompanyMiniForm();
    private ModelDetailContactInfo modelDetailContactInfo = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo2 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo3 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo4 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo5 = new ModelDetailContactInfo();

    private ResponseContactOrderId responseContactOrderId = new ResponseContactOrderId();
    private ResponseDetailCompanyMiniForm responseDetailCompanyMiniForm = new ResponseDetailCompanyMiniForm();
    private ResponseDetailContactInfo responseDetailContactInfo = new ResponseDetailContactInfo();

    private ProgressDialog progressDialog;
    private ApiInterface apiInterface;

    private String companyOrder;
    private List<String> contactOrder = new ArrayList<>();

    private Gson gson = new Gson();
    private int menuCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.color_company));
        setContentView(R.layout.activity_company_detail_order);

        menuCategory = getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, MODE_PRIVATE).getInt(Constans.KEY_MENU_CATEGORY, 0);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        companyOrder = getIntent().getStringExtra(Constans.KeySharedPreference.COMPANY_ORDER_ID);
        Log.d("Company Order ",companyOrder);
        getContactOrderId(companyOrder);

        Button buttonBack = findViewById(R.id.button_back_detail_order_company);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("pesaaan","kembali");
                ActivityCompanyDetailOrder.super.onBackPressed();
            }
        });

        Button buttonExit = findViewById(R.id.button_exit_detail_order_company);
        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityCompanyDetailOrder.this, MainActivityDrawer.class);
                intent.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                intent.putExtra(Constans.KEY_MENU,menuCategory);
                startActivity(intent);
            }
        });

        buttonCompanyInfo = findViewById(R.id.button_company_info_detail_order);
        buttonContactInfo = findViewById(R.id.button_contact_info_detail_order);
        buttonContactInfo2 = findViewById(R.id.button_contact_info2_detail_order);
        buttonContactInfo3 = findViewById(R.id.button_contact_info3_detail_order);
        buttonContactInfo4 = findViewById(R.id.button_contact_info4_detail_order);
        buttonContactInfo5 = findViewById(R.id.button_contact_info5_detail_order);

        buttonCompanyInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCompanyInfo.setEnabled(false);
                buttonContactInfo.setEnabled(true);
                buttonContactInfo2.setEnabled(true);
                buttonContactInfo3.setEnabled(true);
                buttonContactInfo4.setEnabled(true);
                buttonContactInfo5.setEnabled(true);

                buttonCompanyInfo.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_company));

                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_white));

                fragmentCompanyInfoDetailOrder = fragmentCompanyInfoDetailOrder.newInstance(companyOrder);
                loadFragment(fragmentCompanyInfoDetailOrder);
            }
        });

        buttonContactInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCompanyInfo.setEnabled(true);
                buttonContactInfo.setEnabled(false);
                buttonContactInfo2.setEnabled(true);
                buttonContactInfo3.setEnabled(true);
                buttonContactInfo4.setEnabled(true);
                buttonContactInfo5.setEnabled(true);

                buttonCompanyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_company));

                buttonContactInfo2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_white));

                if(contactOrder.size() > 0){
                    fragmentContactInfoDetailOrder = fragmentContactInfoDetailOrder.newInstance(contactOrder.get(0), companyOrder);
                }

                loadFragment(fragmentContactInfoDetailOrder);
            }
        });

        buttonContactInfo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCompanyInfo.setEnabled(true);
                buttonContactInfo.setEnabled(true);
                buttonContactInfo2.setEnabled(false);
                buttonContactInfo3.setEnabled(true);
                buttonContactInfo4.setEnabled(true);
                buttonContactInfo5.setEnabled(true);

                buttonCompanyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo2.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_company));

                buttonContactInfo3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_white));


                if(contactOrder.size() > 1){
                    fragmentContactInfoDetailOrder2 = fragmentContactInfoDetailOrder2.newInstance(contactOrder.get(1), companyOrder);
                }

                loadFragment(fragmentContactInfoDetailOrder2);
            }
        });

        buttonContactInfo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCompanyInfo.setEnabled(true);
                buttonContactInfo.setEnabled(true);
                buttonContactInfo2.setEnabled(true);
                buttonContactInfo3.setEnabled(false);
                buttonContactInfo4.setEnabled(true);
                buttonContactInfo5.setEnabled(true);

                buttonCompanyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo3.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_company));

                buttonContactInfo4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_white));

                if(contactOrder.size() > 2){
                    fragmentContactInfoDetailOrder3=fragmentContactInfoDetailOrder3.newInstance(contactOrder.get(2), companyOrder);
                }

                loadFragment(fragmentContactInfoDetailOrder3);
            }
        });

        buttonContactInfo4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCompanyInfo.setEnabled(true);
                buttonContactInfo.setEnabled(true);
                buttonContactInfo2.setEnabled(true);
                buttonContactInfo3.setEnabled(true);
                buttonContactInfo4.setEnabled(false);
                buttonContactInfo5.setEnabled(true);

                buttonCompanyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo4.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_company));

                buttonContactInfo5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_white));

                if(contactOrder.size() > 3){
                    fragmentContactInfoDetailOrder4=fragmentContactInfoDetailOrder4.newInstance(contactOrder.get(3), companyOrder);
                }

                loadFragment(fragmentContactInfoDetailOrder4);
            }
        });

        buttonContactInfo5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCompanyInfo.setEnabled(true);
                buttonContactInfo.setEnabled(true);
                buttonContactInfo2.setEnabled(true);
                buttonContactInfo3.setEnabled(true);
                buttonContactInfo4.setEnabled(true);
                buttonContactInfo5.setEnabled(false);

                buttonCompanyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo5.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_company));

                if(contactOrder.size() > 4){
                    fragmentContactInfoDetailOrder5 = fragmentContactInfoDetailOrder5.newInstance(contactOrder.get(4), companyOrder);
                }

                loadFragment(fragmentContactInfoDetailOrder5);
            }
        });
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.frame_company_detail_order, fragment);
        fragmentTransaction.commit();

    }

    private void getContactOrderId(String companyOrderId) {
        progressDialog.show();
        if (InternetConnection.checkConnection(this)) {
            final Call<ResponseContactOrderId> contactOrderID = apiInterface.getContactOrderId(companyOrderId);
            contactOrderID.enqueue(new Callback<ResponseContactOrderId>() {
                @Override
                public void onResponse(Call<ResponseContactOrderId> call, Response<ResponseContactOrderId>
                        response) {
                    if (response.isSuccessful()) {
                        responseContactOrderId = response.body();
                        Log.d("Contact Order Id", String.valueOf(responseContactOrderId.getModelContactOrderId().getContactInfo()));
                        contactOrder = responseContactOrderId.getModelContactOrderId().getContactInfo();
                        for (int i = 0; i < contactOrder.size(); i++) {
                            switch (i) {
                                case 0:
                                    buttonContactInfo.setVisibility(View.VISIBLE);
                                    break;
                                case 1:
                                    buttonContactInfo2.setVisibility(View.VISIBLE);
                                    break;
                                case 2:
                                    buttonContactInfo3.setVisibility(View.VISIBLE);
                                    break;
                                case 3:
                                    buttonContactInfo4.setVisibility(View.VISIBLE);
                                    break;
                                case 4:
                                    buttonContactInfo5.setVisibility(View.VISIBLE);
                                    break;
                                default:
                                    break;
                            }
                        }
                        progressDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseContactOrderId = gson.fromJson(response.errorBody().string(), ResponseContactOrderId.class);
                            error = responseContactOrderId.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyDetailOrder.this);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getContactOrderId(companyOrder);
                                    }
                                });

                            }else if(!error.equals("Data Not Found")) {
                                Snackbar.make(findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyDetailOrder.this);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getContactOrderId(companyOrder);
                                }
                            });
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        progressDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyDetailOrder.this);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getContactOrderId(companyOrder);
                            }
                        });
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseContactOrderId> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyDetailOrder.this);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.toString()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getContactOrderId(companyOrder);
                        }
                    });
                    progressDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyDetailOrder.this);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet  Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getContactOrderId(companyOrder);
                }
            });
            progressDialog.dismiss();
        }
    }

}
