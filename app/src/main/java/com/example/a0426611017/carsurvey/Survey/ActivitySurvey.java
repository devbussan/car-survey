package com.example.a0426611017.carsurvey.Survey;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Company.FullForm.ActivityCompanyFullEdit;
import com.example.a0426611017.carsurvey.Finance.ActivityCompanyFinancial;
import com.example.a0426611017.carsurvey.Fragment.Survey.FragmentCekLingkungan;
import com.example.a0426611017.carsurvey.Fragment.Survey.FragmentCekLingkungan1;
import com.example.a0426611017.carsurvey.Fragment.Survey.FragmentCekLingkungan2;
import com.example.a0426611017.carsurvey.Fragment.Survey.FragmentCekLingkungan3;
import com.example.a0426611017.carsurvey.Fragment.Survey.FragmentSurvey;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiCompany;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLSurvey;
import com.example.a0426611017.carsurvey.Model.Survey.ModelCekLingkungan;
import com.example.a0426611017.carsurvey.Model.Survey.ModelSurvey;
import com.example.a0426611017.carsurvey.Personal.FullForm.ActivityPersonalSGFullEdit;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseListEnvironment;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseDDLSurvey;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySurvey extends AppCompatActivity {

    private TextView titleBar;

    private Button buttonDataSurvey, buttonCekLingkungan, buttonCekLingkungan1, buttonCekLingkungan2, buttonCekLingkungan3,
            buttonBack, buttonNext, buttonSave;

    private FloatingActionButton floatingActionButtonAdd;

    private ApiInterface apiInterface;

    private ModelDDLSurvey ddlSurvey;

    private List<String> surveyEnv = new ArrayList<>();

    private ModelSurvey modelSurvey = new ModelSurvey();
    private ModelCekLingkungan modelCekLingkungan = new ModelCekLingkungan();
    private ModelCekLingkungan modelCekLingkungan1 = new ModelCekLingkungan();
    private ModelCekLingkungan modelCekLingkungan2 = new ModelCekLingkungan();
    private ModelCekLingkungan modelCekLingkungan3 = new ModelCekLingkungan();

    private Gson gson = new Gson();

    private Fragment currentFragment;
    private FragmentSurvey fragmentSurvey = new FragmentSurvey();
    private FragmentCekLingkungan fragmentCekLingkungan = new FragmentCekLingkungan();
    private FragmentCekLingkungan1 fragmentCekLingkungan1 = new FragmentCekLingkungan1();
    private FragmentCekLingkungan2 fragmentCekLingkungan2 = new FragmentCekLingkungan2();
    private FragmentCekLingkungan3 fragmentCekLingkungan3 = new FragmentCekLingkungan3();

    private ProgressDialog loadingDialog;
    private int menu;
    private ApiCompany apiCompany;
    private String orderId;
    private String adminId;
    private String surveyId;
    private String sharedLogId = null;

    private ResponseListEnvironment responseListEnvironment = new ResponseListEnvironment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_survey);
        final View currentView = this.findViewById(android.R.id.content);
        setField();

        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        menu = getIntent().getIntExtra(Constans.KEY_MENU, 0);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        apiCompany = new ApiCompany(currentView, ActivitySurvey.this, loadingDialog);
        orderId = getIntent().getStringExtra(Constans.KeySharedPreference.ORDER_ID);
        adminId = this.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);
        surveyId = this.getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, MODE_PRIVATE).getString(Constans.KEY_SURVEY_ID, null);
        sharedLogId = this.getSharedPreferences(Constans.KeySharedPreference.LOG_ID, Context.MODE_PRIVATE).getString(Constans.KEY_LOG_ID, null);

        Log.d(menu + " Order", orderId);
        Log.d(" Admin ID", "return " + adminId);
        Log.d(" Survey ID", "return " + surveyId);
        callAPI();

        switch (menu) {
            case Constans.MENU_PERSONAL:
                setFormForPersonal();
                window.setStatusBarColor(getResources().getColor(R.color.color_personal));
                break;
            case Constans.MENU_COMPANY:
                setFormForCompany();
                window.setStatusBarColor(getResources().getColor(R.color.color_company));
                break;
            default:
                break;
        }

        buttonBack = findViewById(R.id.button_back_survey);

        buttonNext = findViewById(R.id.button_next_survey);
    }

    private void setField() {
        titleBar = findViewById(R.id.text_view_survey);

        buttonDataSurvey = findViewById(R.id.button_data_survey);
        buttonCekLingkungan = findViewById(R.id.button_cek_lingkungan);
        buttonCekLingkungan1 = findViewById(R.id.button_cek_lingkungan1);
        buttonCekLingkungan2 = findViewById(R.id.button_cek_lingkungan2);
        buttonCekLingkungan3 = findViewById(R.id.button_cek_lingkungan3);

        buttonBack = findViewById(R.id.button_back_survey);
        buttonNext = findViewById(R.id.button_next_survey);
        buttonSave = findViewById(R.id.button_save_survey);

        floatingActionButtonAdd = findViewById(R.id.floatingActionButtonAdd);
    }

    private void callAPI() {
        loadingDialog.show();
        if (InternetConnection.checkConnection(this)) {
            Call<ResponseDDLSurvey> ddlCompany = apiInterface.getDDLSurvey();
            ddlCompany.enqueue(new Callback<ResponseDDLSurvey>() {
                @Override
                public void onResponse(Call<ResponseDDLSurvey> call, Response<ResponseDDLSurvey>
                        response) {
                    if (response.isSuccessful()) {
                        setDDLSurvey(response.body().getDdlSurvey());
                    } else {
                        Snackbar.make(findViewById(R.id.layout_activity_survey), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                    }

                    loadingDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseDDLSurvey> call, Throwable t) {
                    Log.e("Error Retrofit DDL", t.toString());
                    Snackbar.make(findViewById(R.id.layout_activity_survey), t.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(R.id.layout_activity_survey), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            loadingDialog.dismiss();
        }
    }

    private void saveData(String pName, String adminId, boolean isNext) {

        List<ModelCekLingkungan> modelChekLingkunganRes = new ArrayList<>();
        ModelCekLingkungan chekLingkunganRes = new ModelCekLingkungan();

        for (int i = 0; i < 4; i++) {
            switch (i) {
                case 0:
                    chekLingkunganRes = modelCekLingkungan;
                    break;
                case 1:
                    chekLingkunganRes = modelCekLingkungan1;
                    break;
                case 2:
                    chekLingkunganRes = modelCekLingkungan2;
                    break;
                case 3:
                    chekLingkunganRes = modelCekLingkungan3;
                    break;
                default:
                    break;
            }
            if (chekLingkunganRes.isModified()) {
                if (surveyEnv.size() > i) {
                    chekLingkunganRes.setSurveyEnId(surveyEnv.get(i));
                } else {
                    chekLingkunganRes.setSurveyEnId(null);
                }
                chekLingkunganRes.setFlagSurveyEnv(String.valueOf(i+1));
                modelChekLingkunganRes.add(chekLingkunganRes);
            }
        }

        apiCompany.saveSurvey(surveyId, modelSurvey, pName, orderId, modelChekLingkunganRes, adminId, menu, isNext);

    }

    private void setFormForPersonal() {
        getListidEnvi(orderId, Constans.PARAM_WS_PERSONAL);
        titleBar.setBackgroundColor(getResources().getColor(R.color.color_personal));
        titleBar.setText("SURVEY PERSONAL FORM DATA");

        buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

        buttonBack.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonNext.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));

        floatingActionButtonAdd.setBackgroundTintList(getResources().getColorStateList(R.color.color_personal));
        floatingActionButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (buttonCekLingkungan1.getVisibility() == View.GONE) {
                    buttonCekLingkungan1.setVisibility(View.VISIBLE);
                } else if (buttonCekLingkungan2.getVisibility() == View.GONE) {
                    buttonCekLingkungan2.setVisibility(View.VISIBLE);
                } else if (buttonCekLingkungan3.getVisibility() == View.GONE) {
                    buttonCekLingkungan3.setVisibility(View.VISIBLE);
                }
            }
        });

        buttonDataSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonDataSurvey.setError(null);
                buttonDataSurvey.setEnabled(false);
                buttonCekLingkungan.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);




                if (ddlSurvey == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callAPI();
                            buttonDataSurvey.callOnClick();
                        }
                    });
                    b.show();

                } else {
                    Log.d("ddlCompanyList", gson.toJson(ddlSurvey));
                    fragmentSurvey = FragmentSurvey.newInstance(Constans.MENU_PERSONAL, ddlSurvey, modelSurvey, orderId);
                    saveForm();
                    loadFragment(fragmentSurvey);
                }

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_personal));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));


            }
        });

        buttonCekLingkungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan.setError(null);
                buttonCekLingkungan.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_personal));

                if (ddlSurvey == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callAPI();
                            buttonCekLingkungan.callOnClick();
                        }
                    });
                    b.show();

                } else {
                    String surveyEnvId = null;
                    if (surveyEnv.size() > 0) {
                        surveyEnvId = surveyEnv.get(0);
                    }

                    fragmentCekLingkungan = FragmentCekLingkungan.newInstance(
                            Constans.MENU_PERSONAL,
                            ddlSurvey,
                            modelCekLingkungan,
                            surveyEnvId,
                            surveyId,
                            orderId);
                    saveForm();

                    loadFragment(fragmentCekLingkungan);
                }
            }
        });

        buttonCekLingkungan1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan1.setError(null);
                buttonCekLingkungan1.setEnabled(false);
                buttonCekLingkungan.setEnabled(true);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_personal));


                if (ddlSurvey == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callAPI();
                            buttonCekLingkungan1.callOnClick();
                        }
                    });
                    b.show();

                } else {
                    String surveyEnvId = null;
                    if (surveyEnv.size() > 1) {
                        surveyEnvId = surveyEnv.get(1);
                    }

                    fragmentCekLingkungan1 = FragmentCekLingkungan1.newInstance(
                            Constans.MENU_PERSONAL,
                            ddlSurvey,
                            modelCekLingkungan1,
                            surveyEnvId,
                            surveyId,
                            orderId);
                    saveForm();

                    loadFragment(fragmentCekLingkungan1);
                }
            }
        });

        buttonCekLingkungan2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan2.setError(null);
                buttonCekLingkungan2.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_personal));


                if (ddlSurvey == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivitySurvey.super.onBackPressed();
                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callAPI();
                            buttonCekLingkungan2.callOnClick();
                        }
                    });
                    b.show();

                } else {
                    String surveyEnvId = null;
                    if (surveyEnv.size() > 2) {
                        surveyEnvId = surveyEnv.get(2);
                    }

                    fragmentCekLingkungan2 = FragmentCekLingkungan2.newInstance(
                            Constans.MENU_PERSONAL,
                            ddlSurvey,
                            modelCekLingkungan2,
                            surveyEnvId,
                            surveyId,
                            orderId);
                    saveForm();

                    loadFragment(fragmentCekLingkungan2);
                }
            }
        });

        buttonCekLingkungan3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan3.setError(null);
                buttonCekLingkungan3.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_personal));

                if (ddlSurvey == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callAPI();
                            buttonCekLingkungan3.callOnClick();
                        }
                    });
                    b.show();

                } else {
                    String surveyEnvId = null;
                    if (surveyEnv.size() > 3) {
                        surveyEnvId = surveyEnv.get(3);
                    }

                    fragmentCekLingkungan3 = FragmentCekLingkungan3.newInstance(
                            Constans.MENU_PERSONAL,
                            ddlSurvey,
                            modelCekLingkungan3,
                            surveyEnvId,
                            surveyId,
                            orderId);
                    saveForm();

                    loadFragment(fragmentCekLingkungan3);
                }
            }
        });
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingDialog.show();
                saveForm();
                checkCompleteSaveAndNext();
                if (modelSurvey.isComplete()
                        && modelCekLingkungan.isComplete()
                        && (!modelCekLingkungan1.isModified() || modelCekLingkungan1.isComplete())
                        && (!modelCekLingkungan2.isModified() || modelCekLingkungan2.isComplete())
                        && (!modelCekLingkungan3.isModified() || modelCekLingkungan3.isComplete())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurvey.this);
                    builder.setTitle(getString(R.string.content_question_are_you_sure_to_save));
                    builder.setMessage(getString(R.string.content_information_save_and_next));
                    builder.setNegativeButton(getString(R.string.dialog_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    loadingDialog.dismiss();
                                    Log.e("info", "NO");
                                }
                            });
                    builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            saveData(Constans.PARAM_WS_PERSONAL, adminId, true);
                        }
                    });
                    builder.show();
                    loadingDialog.dismiss();
                } else {
                    if (buttonDataSurvey.isEnabled() && !modelSurvey.isComplete()) {
                        buttonDataSurvey.setError("Not Complete");
                    }
                    if (buttonCekLingkungan.isEnabled() && !modelCekLingkungan.isComplete()) {
                        buttonCekLingkungan.setError("Not Complete");
                    }
                    if (buttonCekLingkungan1.isEnabled() &&!modelCekLingkungan1.isComplete()) {
                        buttonCekLingkungan1.setError("Not Complete");

                    }
                    if (buttonCekLingkungan2.isEnabled() && !modelCekLingkungan2.isComplete()) {
                        buttonCekLingkungan2.setError("Not Complete");

                    }
                    if (buttonCekLingkungan3.isEnabled() && !modelCekLingkungan3.isComplete()) {
                        buttonCekLingkungan3.setError("Not Complete");

                    }
                    Snackbar.make(findViewById(R.id.layout_activity_survey), "Please Fill The Data First",
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                b.setTitle(R.string.content_information_back_pressed);
                b.setMessage(getString(R.string.content_question_want_save_data)+
                        "\n"+getString(R.string.content_information_press_yes_save));
                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ActivitySurvey.this, ActivityPersonalSGFullEdit.class);
                        intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, orderId);
                        intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);

                        startActivity(intent);
                        finish();
                    }
                });
                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        buttonSave.callOnClick();
                    }
                });
                b.show();
            }
        });

        buttonSave.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurvey.this);
                builder.setTitle(getString(R.string.content_question_are_you_sure_to_save));
                builder.setMessage(getString(R.string.content_information_save_and_exit));
                builder.setNegativeButton(getString(R.string.dialog_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                loadingDialog.dismiss();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        loadingDialog.show();
                        saveForm();
                        saveData(Constans.PARAM_WS_PERSONAL, adminId, false);
                    }
                });

                builder.show();

            }
        });

        floatingActionButtonAdd.setBackgroundTintList(getResources().getColorStateList(R.color.color_personal));
        floatingActionButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (buttonCekLingkungan1.getVisibility() == View.GONE) {
                    buttonCekLingkungan1.setVisibility(View.VISIBLE);
                } else if (buttonCekLingkungan2.getVisibility() == View.GONE) {
                    buttonCekLingkungan2.setVisibility(View.VISIBLE);
                } else if (buttonCekLingkungan3.getVisibility() == View.GONE) {
                    buttonCekLingkungan3.setVisibility(View.VISIBLE);
                }
            }
        });

    }

    private void setFormForCompany() {
        getListidEnvi(orderId, Constans.PARAM_WS_COMPANY);
        titleBar.setBackgroundColor(getResources().getColor(R.color.color_company));
        titleBar.setText("SURVEY COMPANY FORM DATA");

        buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

        buttonDataSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonDataSurvey.setEnabled(false);
                buttonCekLingkungan.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);
                buttonDataSurvey.setError(null);

                if (ddlSurvey == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callAPI();
                            buttonDataSurvey.callOnClick();
                        }
                    });
                    b.show();
                } else {
                    Log.d("ddlCompanyList", gson.toJson(ddlSurvey));
                    fragmentSurvey = FragmentSurvey.newInstance(Constans.MENU_COMPANY, ddlSurvey, modelSurvey, orderId);

                    saveForm();
                    loadFragment(fragmentSurvey);

                }


                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_company));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_company));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));


            }
        });

        buttonCekLingkungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonCekLingkungan.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_company));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_company));

                buttonCekLingkungan.setError(null);

                if (ddlSurvey == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callAPI();
                            buttonCekLingkungan.callOnClick();
                        }
                    });
                    b.show();
                } else {

                    String surveyEnvId = null;
                    if (surveyEnv.size() > 0) {
                        surveyEnvId = surveyEnv.get(0);
                    }

                    fragmentCekLingkungan = FragmentCekLingkungan.newInstance(
                            Constans.MENU_COMPANY,
                            ddlSurvey,
                            modelCekLingkungan,
                            surveyEnvId,
                            surveyId,
                            orderId);
                    saveForm();

                    loadFragment(fragmentCekLingkungan);
                }
            }
        });

        buttonCekLingkungan1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonCekLingkungan1.setEnabled(false);
                buttonCekLingkungan.setEnabled(true);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_company));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_company));

                buttonCekLingkungan1.setError(null);

                if (ddlSurvey == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callAPI();
                            buttonCekLingkungan1.callOnClick();
                        }
                    });
                    b.show();
                } else {

                    String surveyEnvId = null;
                    if (surveyEnv.size() > 1) {
                        surveyEnvId = surveyEnv.get(1);
                    }

                    fragmentCekLingkungan1 = FragmentCekLingkungan1.newInstance(
                            Constans.MENU_COMPANY,
                            ddlSurvey,
                            modelCekLingkungan1,
                            surveyEnvId,
                            surveyId,
                            orderId);
                    saveForm();

                    loadFragment(fragmentCekLingkungan1);
                }
            }
        });

        buttonCekLingkungan2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonCekLingkungan2.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);
                buttonCekLingkungan2.setError(null);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_company));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_company));


                if (ddlSurvey == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callAPI();
                            buttonCekLingkungan2.callOnClick();
                        }
                    });
                    b.show();
                } else {

                    String surveyEnvId = null;
                    if (surveyEnv.size() > 2) {
                        surveyEnvId = surveyEnv.get(2);
                    }

                    fragmentCekLingkungan2 = FragmentCekLingkungan2.newInstance(
                            Constans.MENU_COMPANY,
                            ddlSurvey,
                            modelCekLingkungan2,
                            surveyEnvId,
                            surveyId,
                            orderId);
                    saveForm();

                    loadFragment(fragmentCekLingkungan2);
                }
            }
        });

        buttonCekLingkungan3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonCekLingkungan3.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan.setEnabled(true);
                buttonCekLingkungan3.setError(null);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_company));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_company));


                if (ddlSurvey == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            callAPI();
                            buttonCekLingkungan3.callOnClick();
                        }
                    });
                    b.show();
                } else {

                    String surveyEnvId = null;
                    if (surveyEnv.size() > 3) {
                        surveyEnvId = surveyEnv.get(3);
                    }

                    fragmentCekLingkungan3 = FragmentCekLingkungan3.newInstance(
                            Constans.MENU_COMPANY,
                            ddlSurvey,
                            modelCekLingkungan3,
                            surveyEnvId,
                            surveyId,
                            orderId);
                    saveForm();

                    loadFragment(fragmentCekLingkungan3);
                }
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingDialog.show();
                saveForm();
                checkCompleteSaveAndNext();

                if (modelSurvey.isComplete()
                        && modelCekLingkungan.isComplete()
                        && (!modelCekLingkungan1.isModified() || modelCekLingkungan1.isComplete())
                        && (!modelCekLingkungan2.isModified() || modelCekLingkungan2.isComplete())
                        && (!modelCekLingkungan3.isModified() || modelCekLingkungan3.isComplete())) {

                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurvey.this);
                    builder.setTitle(getString(R.string.content_question_are_you_sure_to_save));
                    builder.setMessage(getString(R.string.content_information_save_and_next));
                    builder.setNegativeButton(getString(R.string.dialog_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    loadingDialog.dismiss();
                                    Log.e("info", "NO");
                                }
                            });
                    builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int j) {
                            saveData(Constans.PARAM_WS_COMPANY, adminId, true);
                        }
                    });
                    builder.show();
                    loadingDialog.dismiss();
                } else {
                    if (buttonDataSurvey.isEnabled() && !modelSurvey.isComplete()) {
                        buttonDataSurvey.setError("Not Complete");
                    }
                    if (buttonCekLingkungan.isEnabled() && !modelCekLingkungan.isComplete()) {
                        buttonCekLingkungan.setError("Not Complete");
                    }
                    if (buttonCekLingkungan1.isEnabled() &&!modelCekLingkungan1.isComplete()) {
                        buttonCekLingkungan1.setError("Not Complete");

                    }
                    if (buttonCekLingkungan2.isEnabled() && !modelCekLingkungan2.isComplete()) {
                        buttonCekLingkungan2.setError("Not Complete");

                    }
                    if (buttonCekLingkungan3.isEnabled() && !modelCekLingkungan3.isComplete()) {
                        buttonCekLingkungan3.setError("Not Complete");

                    }
                    Snackbar.make(findViewById(R.id.layout_activity_survey), "Please Fill The Data First",
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            }
        });

        buttonSave.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurvey.this);
                builder.setTitle(getString(R.string.content_question_want_save_data));
                builder.setMessage(getString(R.string.content_information_save_and_exit));
                builder.setNegativeButton(getString(R.string.dialog_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                loadingDialog.dismiss();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        loadingDialog.show();
                        saveForm();
                        saveData(Constans.PARAM_WS_COMPANY, adminId, false);
                    }
                });

                builder.show();

            }
        });

        buttonBack.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonNext.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));

        floatingActionButtonAdd.setBackgroundTintList(getResources().getColorStateList(R.color.color_company));
        floatingActionButtonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (buttonCekLingkungan1.getVisibility() == View.GONE) {
                    buttonCekLingkungan1.setVisibility(View.VISIBLE);
                } else if (buttonCekLingkungan2.getVisibility() == View.GONE) {
                    buttonCekLingkungan2.setVisibility(View.VISIBLE);
                } else if (buttonCekLingkungan3.getVisibility() == View.GONE) {
                    buttonCekLingkungan3.setVisibility(View.VISIBLE);
                    floatingActionButtonAdd.setVisibility(View.GONE);
                }
            }
        });

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                b.setTitle(R.string.content_information_back_pressed);
                b.setMessage(getString(R.string.content_question_want_save_data)+
                        "\n"+getString(R.string.content_information_press_yes_save));
                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ActivitySurvey.this, ActivityCompanyFullEdit.class);
                        intent.putExtra(Constans.KeySharedPreference.COMPANY_ORDER_ID, orderId);
                        intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);

                        startActivity(intent);
                        finish();
                    }
                });
                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        buttonSave.callOnClick();
                    }
                });
                b.show();
            }
        });
    }

    private void saveForm() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (currentFragment.getClass() == fragmentSurvey.getClass()) {
                    modelSurvey = fragmentSurvey.getForm();
                } else if (currentFragment.getClass() == fragmentCekLingkungan.getClass()) {
                    modelCekLingkungan = fragmentCekLingkungan.getForm();
                } else if (currentFragment.getClass() == fragmentCekLingkungan1.getClass()) {
                    modelCekLingkungan1 = fragmentCekLingkungan1.getForm();
                } else if (currentFragment.getClass() == fragmentCekLingkungan2.getClass()) {
                    modelCekLingkungan2 = fragmentCekLingkungan2.getForm();
                } else if (currentFragment.getClass() == fragmentCekLingkungan3.getClass()) {
                    modelCekLingkungan3 = fragmentCekLingkungan3.getForm();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurvey.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    saveForm();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    public void checkCompleteSaveAndNext() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (modelSurvey.isModified()) {
                    modelSurvey = fragmentSurvey.getComplete();
                }
                if (modelCekLingkungan.isModified()) {
                    modelCekLingkungan = fragmentCekLingkungan.getComplete();
                }
                if (modelCekLingkungan1.isModified()) {
                    modelCekLingkungan1 = fragmentCekLingkungan1.getComplete();
                }
                if (modelCekLingkungan2.isModified()) {
                    modelCekLingkungan2 = fragmentCekLingkungan2.getComplete();
                }
                if (modelCekLingkungan3.isModified()) {
                    modelCekLingkungan3 = fragmentCekLingkungan3.getComplete();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurvey.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    checkCompleteSaveAndNext();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    private void loadFragment(Fragment fragment) {
        currentFragment = fragment;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frame_survey, fragment);
        fragmentTransaction.commit();
    }

    private void setDDLSurvey(ModelDDLSurvey ddlSurvey) {
        this.ddlSurvey = ddlSurvey;
    }

    public void getListidEnvi(final String idOrder, final String name) {
        loadingDialog.show();
        if (InternetConnection.checkConnection(this)) {
            Call<ResponseListEnvironment> getList = apiInterface.getListEnvironment(name, idOrder);
            getList.enqueue(new Callback<ResponseListEnvironment>() {
                @Override
                public void onResponse(Call<ResponseListEnvironment> call, Response<ResponseListEnvironment> response) {
                    if (response.isSuccessful()) {
                        surveyEnv = response.body().getDdlFinance().getSurveyEnv();
                        for (int i = 0; i < surveyEnv.size(); i++) {
                            if (i == 1) {
                                buttonCekLingkungan1.setVisibility(View.VISIBLE);
                            } else if (i == 2) {
                                buttonCekLingkungan2.setVisibility(View.VISIBLE);
                            } else if (i == 3) {
                                buttonCekLingkungan3.setVisibility(View.VISIBLE);
                            }
                        }
                        Log.d("surveyEnv", gson.toJson(surveyEnv));
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseListEnvironment = gson.fromJson(response.errorBody().string(), ResponseListEnvironment.class);
                            error = responseListEnvironment.getMessage();

                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                                b.setTitle(getString(R.string.content_question_internet_error));
                                b.setMessage(getString(R.string.content_information_cant_connect_server));
                                b.setCancelable(false);
                                b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getListidEnvi(idOrder, name);
                                    }
                                });
                                b.show();
                            }else
                                Snackbar.make(findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurvey.this);
                            builder.setTitle(getString(R.string.content_question_internet_error));
                            builder.setMessage(getString(R.string.content_information_cant_connect_server));
                            builder.setCancelable(false);
                            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getListidEnvi(idOrder, name);
                                    Log.e("info", "OK");
                                }
                            });
                            builder.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ActivitySurvey.this);
                        builder.setTitle(getString(R.string.content_question_internet_error));
                        builder.setMessage(getString(R.string.content_information_cant_connect_server));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getListidEnvi(idOrder, name);
                                Log.e("info", "OK");
                            }
                        });
                        builder.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseListEnvironment> call, Throwable throwable) {
                    Log.e("Error Retrofit List", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getListidEnvi(idOrder, name);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
            b.setTitle(getString(R.string.content_question_internet_error));
            b.setMessage(getString(R.string.content_information_cant_connect_server));
            b.setCancelable(false);
            b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getListidEnvi(idOrder, name);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurvey.this);
        b.setTitle(R.string.content_information_back_pressed);
        b.setMessage(getString(R.string.content_question_want_save_data)+
                "\n"+getString(R.string.content_information_press_yes_save));
        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = null;

                switch (menu) {
                    case Constans.MENU_PERSONAL:
                        intent = new Intent(ActivitySurvey.this, ActivityPersonalSGFullEdit.class);
                        intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, orderId);
                        intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);
                        break;
                    case Constans.MENU_COMPANY:
                        intent = new Intent(ActivitySurvey.this, ActivityCompanyFullEdit.class);
                        intent.putExtra(Constans.KeySharedPreference.COMPANY_ORDER_ID, orderId);
                        intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);
                        break;
                    default:
                        break;
                }

                startActivity(intent);
                finish();
            }
        });
        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                buttonSave.callOnClick();
            }
        });
        b.show();
    }
}