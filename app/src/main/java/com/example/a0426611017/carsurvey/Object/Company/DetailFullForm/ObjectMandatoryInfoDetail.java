package com.example.a0426611017.carsurvey.Object.Company.DetailFullForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Company.ModelMandatoryInfo;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by c201017001 on 04/01/2018.
 */

public class ObjectMandatoryInfoDetail {

    private ViewGroup container;
    private LayoutInflater inflater;
    private View view;
    private Context context;

    private TextView debitorScale,
            nomorAkta, nomorSiup, nomorDomisili, nomorTdp,
            dateAkta, dateSiup, dateSkDomisili, dateTdp,
            debitorGroup, counterPart,
            fileNameAkta, fileNameSiup, fileNameDomisili, fileNameTdp;

    private PreviewImage previewImage = new PreviewImage();

    public ObjectMandatoryInfoDetail(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        this.view = inflater.inflate(R.layout.company_detail_mandatory, viewGroup, false);
        this.container = viewGroup;
        this.inflater = inflater;
        findView();
    }

    public View getView() {
        return view;
    }

    private void findView() {

        debitorGroup = (TextView) view.findViewById(R.id.debitorGroup);
        nomorAkta = (TextView) view.findViewById(R.id.nomorAkta);
        nomorSiup = (TextView) view.findViewById(R.id.nomorSiup);
        nomorDomisili = (TextView) view.findViewById(R.id.nomorSkDomisili);
        nomorTdp = (TextView) view.findViewById(R.id.nomorTdp);
        dateAkta = (TextView) view.findViewById(R.id.tanggalAkta);
        dateSiup = (TextView) view.findViewById(R.id.tanggalSiup);
        dateSkDomisili = (TextView) view.findViewById(R.id.tanggalSkDomisili);
        dateTdp = (TextView) view.findViewById(R.id.tanggalTdp);
        debitorScale = (TextView) view.findViewById(R.id.debitorScale);
        counterPart = (TextView) view.findViewById(R.id.counterPart);
        fileNameAkta = (TextView) view.findViewById(R.id.nameImageAkta);
        fileNameSiup = (TextView) view.findViewById(R.id.nameImageSiup);
        fileNameDomisili = (TextView) view.findViewById(R.id.nameImageSkDomisili);
        fileNameTdp = (TextView) view.findViewById(R.id.nameImageTdp);
    }

    public void setForm(final ModelMandatoryInfo mandatoryInfo) {
        nomorAkta.setText(mandatoryInfo.getNomorAkta());
        nomorSiup.setText(mandatoryInfo.getNomorSiup());
        nomorDomisili.setText(mandatoryInfo.getNomorDomisili());
        nomorTdp.setText(mandatoryInfo.getNomorTdp());

        if (mandatoryInfo.getDateAkta() != null) {
            if(!mandatoryInfo.getDateAkta().equals("")) {
                mandatoryInfo.setDateAktaFormat(
                        mandatoryInfo.getDateAkta().substring(6, 8)
                                + " " + FormatDate.PRINT_MONTH[Integer.parseInt(mandatoryInfo.getDateAkta().substring(4, 6))]
                                + " " + mandatoryInfo.getDateAkta().substring(0, 4)
                );
            }
        }

        if (mandatoryInfo.getDateSiup() != null) {
            if(!mandatoryInfo.getDateSiup().equals("")) {
                mandatoryInfo.setDateSiupFormat(
                        mandatoryInfo.getDateSiup().substring(6, 8)
                                + " " + FormatDate.PRINT_MONTH[Integer.parseInt(mandatoryInfo.getDateSiup().substring(4, 6))]
                                + " " + mandatoryInfo.getDateSiup().substring(0, 4)
                );
            }
        }

        if (mandatoryInfo.getDateSkDomisili() != null) {
            if(!mandatoryInfo.getDateSkDomisili().equals("")) {
                mandatoryInfo.setDateSkDomisiliFormat(
                        mandatoryInfo.getDateSkDomisili().substring(6, 8)
                                + " " + FormatDate.PRINT_MONTH[Integer.parseInt(mandatoryInfo.getDateSkDomisili().substring(4, 6))]
                                + " " + mandatoryInfo.getDateSkDomisili().substring(0, 4)
                );
            }
        }

        if (mandatoryInfo.getDateTdp() != null) {
            if(!mandatoryInfo.getDateTdp().equals("")) {
                mandatoryInfo.setDateTdpFormat(
                        mandatoryInfo.getDateTdp().substring(6, 8)
                                + " " + FormatDate.PRINT_MONTH[Integer.parseInt(mandatoryInfo.getDateTdp().substring(4, 6))]
                                + " " + mandatoryInfo.getDateTdp().substring(0, 4)
                );
            }
        }

        dateAkta.setText(mandatoryInfo.getDateAktaFormat());
        dateSiup.setText(mandatoryInfo.getDateSiupFormat());
        dateSkDomisili.setText(mandatoryInfo.getDateSkDomisiliFormat());
        dateTdp.setText(mandatoryInfo.getDateTdpFormat());

        if(mandatoryInfo.getNameImageAktaUrl() != null){
            if(!mandatoryInfo.getNameImageAktaUrl().equals("")){
                fileNameAkta.setText(mandatoryInfo.getNameImageAktaUrl().split("/")[6]);
                previewImage.setPreviewImage(mandatoryInfo.getNameImageAktaUrl(), context, fileNameAkta);
            }else{
                fileNameAkta.setText(mandatoryInfo.getNameImageAktaUrl());
            }
        }else{
            fileNameAkta.setText(mandatoryInfo.getNameImageAktaUrl());
        }


        if(mandatoryInfo.getNameImageSiupUrl() != null){
            if(!mandatoryInfo.getNameImageSiupUrl().equals("")){
                fileNameSiup.setText(mandatoryInfo.getNameImageSiupUrl().split("/")[6]);
                previewImage.setPreviewImage(mandatoryInfo.getNameImageSiupUrl(), context, fileNameSiup);
            }else{
                fileNameSiup.setText(mandatoryInfo.getNameImageSiupUrl());
            }
        }else{
            fileNameSiup.setText(mandatoryInfo.getNameImageSiupUrl());
        }


        if(mandatoryInfo.getNameImageDomisiliUrl() != null){
            if(!mandatoryInfo.getNameImageDomisiliUrl().equals("")){
                fileNameDomisili.setText(mandatoryInfo.getNameImageDomisiliUrl().split("/")[6]);
                previewImage.setPreviewImage(mandatoryInfo.getNameImageDomisiliUrl(), context, fileNameDomisili);
            }else{
                fileNameDomisili.setText(mandatoryInfo.getNameImageDomisiliUrl());
            }
        }else{
            fileNameDomisili.setText(mandatoryInfo.getNameImageDomisiliUrl());
        }

        if(mandatoryInfo.getNameImageTdpUrl() != null){
            if(!mandatoryInfo.getNameImageTdpUrl().equals("")){
                fileNameTdp.setText(mandatoryInfo.getNameImageTdpUrl().split("/")[6]);
                previewImage.setPreviewImage(mandatoryInfo.getNameImageTdpUrl(), context, fileNameTdp);
            }else{
                fileNameTdp.setText(mandatoryInfo.getNameImageTdpUrl());
            }
        }else{
            fileNameTdp.setText(mandatoryInfo.getNameImageTdpUrl());
        }


        debitorGroup.setText(mandatoryInfo.getDebitorGroup());
        debitorScale.setText(mandatoryInfo.getDebitorScale());
        counterPart.setText(mandatoryInfo.getCounterPart());

    }

}
