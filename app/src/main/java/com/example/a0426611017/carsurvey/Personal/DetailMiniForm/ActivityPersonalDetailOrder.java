package com.example.a0426611017.carsurvey.Personal.DetailMiniForm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.a0426611017.carsurvey.Fragment.Personal.DetailMiniForm.FragmentDataPersonalInfoDetail;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.R;

public class ActivityPersonalDetailOrder extends AppCompatActivity implements View.OnClickListener {
    private Button buttonPersonalInfo;
    private Button buttonNext;
    private Button buttonBack;

    private String personalOrderId;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_personal_detail_order);
        window.setStatusBarColor(getResources().getColor(R.color.color_personal));

        progressDialog = new ProgressDialog(ActivityPersonalDetailOrder.this);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);

        personalOrderId = getIntent().getStringExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID);
        Log.d("Personal Order", personalOrderId);

        buttonPersonalInfo = findViewById(R.id.button_personal_info);
        buttonPersonalInfo.setOnClickListener(this);

        buttonBack = findViewById(R.id.button_back_personal_info_detail);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityPersonalDetailOrder.super.onBackPressed();
            }
        });

        buttonNext = findViewById(R.id.button_next_personal_info_detail);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityPersonalDetailOrder.this, ActivityPersonalSgDetailOrder.class);
                intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, personalOrderId);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_personal_info :
                buttonPersonalInfo.setEnabled(false);
                buttonPersonalInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonPersonalInfo.setTextColor(getResources().getColor(R.color.color_personal));

                loadFragment(FragmentDataPersonalInfoDetail.newInstance(personalOrderId));
                break;
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frame_personal_order_detail, fragment);
        fragmentTransaction.commit();
    }

}
