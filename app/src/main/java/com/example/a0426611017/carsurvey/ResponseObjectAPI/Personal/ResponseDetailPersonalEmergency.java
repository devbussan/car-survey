package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.Personal.ModelDataEmergencyContact;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426591017 on 3/6/2018.
 */

public class ResponseDetailPersonalEmergency {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDataEmergencyContact modelDataEmergency;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDataEmergencyContact getModelDataEmergency() {
        return modelDataEmergency;
    }

    public void setModelDataEmergency(ModelDataEmergencyContact modelDataEmergency) {
        this.modelDataEmergency = modelDataEmergency;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
