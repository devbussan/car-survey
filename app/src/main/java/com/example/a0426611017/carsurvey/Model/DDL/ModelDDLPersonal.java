package com.example.a0426611017.carsurvey.Model.DDL;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by c201017001 on 08/02/2018.
 */

public class ModelDDLPersonal implements Serializable{

    @SerializedName("CUSTOMER_MODEL")
    private List<String> customerModel;

    @SerializedName("ID_TYPE")
    private List<String> idType;

    @SerializedName("GENDER")
    private List<String> gender;

    @SerializedName("ADDRESS_TYPE_NAME")
    private List<String> addressTypeName;

    @SerializedName("BUILDING_LOCATION_CLASS")
    private List<String> buildingLocationClass;

    @SerializedName("BUILDING_OWNERSHIP")
    private List<String> buildingOwnership;

    @SerializedName("SALUTATION")
    private List<String> salution;

    @SerializedName("MARITAL_STATUS")
    private List<String> maritalStatus;

    @SerializedName("RELIGION")
    private List<String> religion;

    @SerializedName("EDUCATION")
    private List<String> education;

    public List<String> getCustomerModel() {
        return customerModel;
    }

    public void setCustomerModel(List<String> customerModel) {
        this.customerModel = customerModel;
    }

    public List<String> getIdType() {
        return idType;
    }

    public void setIdType(List<String> idType) {
        this.idType = idType;
    }

    public List<String> getGender() {
        return gender;
    }

    public void setGender(List<String> gender) {
        this.gender = gender;
    }

    public List<String> getAddressTypeName() {
        return addressTypeName;
    }

    public void setAddressTypeName(List<String> addressTypeName) {
        this.addressTypeName = addressTypeName;
    }

    public List<String> getBuildingLocationClass() {
        return buildingLocationClass;
    }

    public void setBuildingLocationClass(List<String> buildingLocationClass) {
        this.buildingLocationClass = buildingLocationClass;
    }

    public List<String> getBuildingOwnership() {
        return buildingOwnership;
    }

    public void setBuildingOwnership(List<String> buildingOwnership) {
        this.buildingOwnership = buildingOwnership;
    }

    public List<String> getSalution() {
        return salution;
    }

    public void setSalution(List<String> salution) {
        this.salution = salution;
    }

    public List<String> getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(List<String> maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public List<String> getReligion() {
        return religion;
    }

    public void setReligion(List<String> religion) {
        this.religion = religion;
    }

    public List<String> getEducation() {
        return education;
    }

    public void setEducation(List<String> education) {
        this.education = education;
    }
}
