package com.example.a0426611017.carsurvey.Object.Personal.FullForm;


import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Currency;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataFinancial;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by 0426611017 on 2/13/2018.
 */

public class ObjectDataFinancialPending {
    private View view;
    private Context context;
    private PreviewImage previewImage = new PreviewImage();

    private EditText monthlyFixedIncome, monthlyVariabelIncome,
            otherBusinessIncome, spouseGuarantorIncome, monthlyLivingCost,
            otherLoanInstallment;

    private TextView namePersonalMonthlyFixedIncomeImage,
            namePersonalMonthlyFixedIncomeImageUrl;

    private Button buttonMonthlyFixedIncomeImage;

    public ObjectDataFinancialPending(ViewGroup viewGroup, LayoutInflater inflater){
        this.context = viewGroup.getContext();
        ViewGroup container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_data_financial_pending, container, false);

        setField();
    }

    public View getView() {
        return view;
    }

    public TextView getNamePersonalMonthlyFixedIncomeImage() {
        return namePersonalMonthlyFixedIncomeImage;}

    public TextView getNamePersonalMonthlyFixedIncomeImageUrl() {
        return namePersonalMonthlyFixedIncomeImageUrl;
    }

    public void setFilePersonalMonthlyFixedIncomeImage(String filePersonalMonthlyFixedIncomeImage) {
        this.namePersonalMonthlyFixedIncomeImage.setText(filePersonalMonthlyFixedIncomeImage);
    }

    public void setFilePersonalMonthlyFixedIncomeImageUrl(String pathFilePersonalMonthlyFixedIncomeImage) {
        this.namePersonalMonthlyFixedIncomeImageUrl.setText(pathFilePersonalMonthlyFixedIncomeImage);
    }

    public Button getButtonMonthlyFixedIncomeImage() {
        return buttonMonthlyFixedIncomeImage;
    }

    public void setEnabledButtonImage(){
        buttonMonthlyFixedIncomeImage.setEnabled(true);
    }

    public void setField() {
        monthlyFixedIncome = view.findViewById(R.id.edit_text_monthly_fixed_income_pending);
        monthlyFixedIncome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    monthlyFixedIncome.removeTextChangedListener(this);

                    String formatted = Currency.set(monthlyFixedIncome);
                    monthlyFixedIncome.setText(formatted.replaceAll("[$]",""));
                    monthlyFixedIncome.setSelection(formatted.length()-1);
                    monthlyFixedIncome.addTextChangedListener(this);
                }
            }
        });

        namePersonalMonthlyFixedIncomeImage = view.findViewById(R.id.text_view_personal_monthly_fixed_income_image_pending);
        namePersonalMonthlyFixedIncomeImage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    namePersonalMonthlyFixedIncomeImage.setError(null);
                }
            }
        });
        namePersonalMonthlyFixedIncomeImageUrl = view.findViewById(R.id.text_view_personal_monthly_fixed_income_image_url_pending);
        buttonMonthlyFixedIncomeImage = view.findViewById(R.id.button_camera_monthly_fixed_income_image_pending);

        monthlyVariabelIncome = view.findViewById(R.id.edit_text_monthly_variable_income_pending);
        monthlyVariabelIncome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    monthlyVariabelIncome.removeTextChangedListener(this);

                    String formatted = Currency.set(monthlyVariabelIncome);
                    monthlyVariabelIncome.setText(formatted.replaceAll("[$]",""));
                    monthlyVariabelIncome.setSelection(formatted.length()-1);
                    monthlyVariabelIncome.addTextChangedListener(this);
                }
            }
        });
        otherBusinessIncome = view.findViewById(R.id.edit_text_other_business_income_pending);
        otherBusinessIncome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    otherBusinessIncome.removeTextChangedListener(this);
                    String formatted = Currency.set(otherBusinessIncome);
                    otherBusinessIncome.setText(formatted.replaceAll("[$]",""));
                    otherBusinessIncome.setSelection(formatted.length()-1);
                    otherBusinessIncome.addTextChangedListener(this);
                }
            }
        });
        spouseGuarantorIncome = view.findViewById(R.id.edit_text_spouse_guarantor_income_pending);
        spouseGuarantorIncome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    spouseGuarantorIncome.removeTextChangedListener(this);
                    String formatted = Currency.set(spouseGuarantorIncome);
                    spouseGuarantorIncome.setText(formatted.replaceAll("[$]",""));
                    spouseGuarantorIncome.setSelection(formatted.length()-1);
                    spouseGuarantorIncome.addTextChangedListener(this);
                }
            }
        });
        monthlyLivingCost = view.findViewById(R.id.edit_text_monthly_living_cost_pending);
        monthlyLivingCost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    monthlyLivingCost.removeTextChangedListener(this);
                    String formatted = Currency.set(monthlyLivingCost);
                    monthlyLivingCost.setText(formatted.replaceAll("[$]",""));
                    monthlyLivingCost.setSelection(formatted.length()-1);
                    monthlyLivingCost.addTextChangedListener(this);
                }
            }
        });
        otherLoanInstallment = view.findViewById(R.id.edit_text_other_loan_installment_pending);
        otherLoanInstallment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    otherLoanInstallment.removeTextChangedListener(this);
                    String formatted = Currency.set(otherLoanInstallment);
                    otherLoanInstallment.setText(formatted.replaceAll("[$]",""));
                    otherLoanInstallment.setSelection(formatted.length()-1);
                    otherLoanInstallment.addTextChangedListener(this);
                }
            }
        });

        buttonMonthlyFixedIncomeImage.setEnabled(false);
    }

    public void setForm(ModelDataFinancial modelDataFinancial){
        monthlyFixedIncome.setText(modelDataFinancial.getFixedincome());

        namePersonalMonthlyFixedIncomeImageUrl.setText(modelDataFinancial.getSlipimgurl());

        Log.d("Url image", namePersonalMonthlyFixedIncomeImageUrl.getText().toString());
        PreviewImage previewImage = new PreviewImage();
        String nameIdImage;
        if (modelDataFinancial.getSlipimgurl().equals("") || modelDataFinancial.getSlipimgurl() == null) {
            nameIdImage = "No selected file";
        } else {
            if (modelDataFinancial.getSlipimgurl().startsWith("http")) {
                nameIdImage = modelDataFinancial.getSlipimgurl().split("/")[6];
                previewImage.setPreviewImage(modelDataFinancial.getSlipimgurl(), context, namePersonalMonthlyFixedIncomeImage);
            } else {
                nameIdImage = modelDataFinancial.getSlipimg();
                previewImage.setPreviewImage(nameIdImage, context, namePersonalMonthlyFixedIncomeImage);
            }
        }
        namePersonalMonthlyFixedIncomeImage.setText(nameIdImage);

        monthlyVariabelIncome.setText(modelDataFinancial.getVariableincome());
        otherBusinessIncome.setText(modelDataFinancial.getOtherincome());
        spouseGuarantorIncome.setText(modelDataFinancial.getSpouseincome());
        monthlyLivingCost.setText(modelDataFinancial.getMonthlylivcost());
        otherLoanInstallment.setText(modelDataFinancial.getOtherloaninst());
    }

    public ModelDataFinancial getForm() {
        ModelDataFinancial modelDataFinancial = new ModelDataFinancial();

        modelDataFinancial.setFixedincome(monthlyFixedIncome.getText().toString().replaceAll("[,]",""));
        modelDataFinancial.setSlipimg(namePersonalMonthlyFixedIncomeImage.getText().toString());
        modelDataFinancial.setSlipimgurl(namePersonalMonthlyFixedIncomeImageUrl.getText().toString());
        modelDataFinancial.setVariableincome(monthlyVariabelIncome.getText().toString().replaceAll("[,]", ""));
        modelDataFinancial.setOtherincome(otherBusinessIncome.getText().toString().replaceAll("[,]", ""));
        modelDataFinancial.setSpouseincome(spouseGuarantorIncome.getText().toString().replaceAll("[,]", ""));
        modelDataFinancial.setMonthlylivcost(monthlyLivingCost.getText().toString().replaceAll("[,]", ""));
        modelDataFinancial.setOtherloaninst(otherLoanInstallment.getText().toString().replaceAll("[,]", ""));

        modelDataFinancial.setModified(true);

        return modelDataFinancial;
    }

    public ModelDataFinancial getComplete() {
        ModelDataFinancial modelDataFinancial = getForm();
        modelDataFinancial.setComplete(true);

        if (modelDataFinancial.getFixedincome().length() == 0) {
            monthlyFixedIncome.setError("Monthly Fixed Income is required");
            modelDataFinancial.setComplete(false);
        } else if (modelDataFinancial.getFixedincome().trim().length() == 0) {
            monthlyFixedIncome.setError("Monthly Fixed Income not allowed whitespaces only");
            modelDataFinancial.setComplete(false);
        }

        // TODO: FIELD NOT MANDATORY FROM HERE
//        if(modelDataFinancial.getSlipimg().length() == 0) {
//            namePersonalMonthlyFixedIncomeImage.setError("Building Location Image is Required");
//            modelDataFinancial.setComplete(false);
//        } else if (modelDataFinancial.getSlipimg().contains("No selected file")) {
//            namePersonalMonthlyFixedIncomeImage.setError("Monthly Variable Incoming is Required");
//            modelDataFinancial.setComplete(false);
//        } else {
//            namePersonalMonthlyFixedIncomeImage.setError(null);
//        }
//
//        if (modelDataFinancial.getVariableincome().length() == 0) {
//            monthlyVariabelIncome.setError("Monthly Variable Income is required");
//            modelDataFinancial.setComplete(false);
//        } else if (modelDataFinancial.getVariableincome().trim().length() == 0) {
//            monthlyVariabelIncome.setError("Monthly Variable Income not allowed whitespaces only");
//            modelDataFinancial.setComplete(false);
//        }
//
//        if (modelDataFinancial.getOtherincome().length() == 0) {
//            otherBusinessIncome.setError("Other Business Income is required");
//            modelDataFinancial.setComplete(false);
//        } else if (modelDataFinancial.getOtherincome().trim().length() == 0) {
//            otherBusinessIncome.setError("Other Business Income not allowed whitespaces only");
//            modelDataFinancial.setComplete(false);
//        }
//
//        if (modelDataFinancial.getSpouseincome().length() == 0) {
//            spouseGuarantorIncome.setError("Spouse Guarantor Income is required");
//            modelDataFinancial.setComplete(false);
//        } else if (modelDataFinancial.getSpouseincome().trim().length() == 0) {
//            spouseGuarantorIncome.setError("Spouse Guarantor Income not allowed whitespaces only");
//            modelDataFinancial.setComplete(false);
//        }
//
//        if (modelDataFinancial.getMonthlylivcost().length() == 0) {
//            monthlyLivingCost.setError("Monthly Living Cost is required");
//            modelDataFinancial.setComplete(false);
//        } else if (modelDataFinancial.getMonthlylivcost().trim().length() == 0) {
//            monthlyLivingCost.setError("Monthly Living Cost not allowed whitespaces only");
//            modelDataFinancial.setComplete(false);
//        }
//
//        if (modelDataFinancial.getOtherloaninst().length() == 0) {
//            otherLoanInstallment.setError("Other Loan Installment is required");
//            modelDataFinancial.setComplete(false);
//        } else if (modelDataFinancial.getOtherloaninst().trim().length() == 0) {
//            otherLoanInstallment.setError("Other Loan Installment not allowed whitespaces only");
//            modelDataFinancial.setComplete(false);
//        }

        return modelDataFinancial;
    }
}

