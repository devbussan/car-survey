package com.example.a0426611017.carsurvey.Object.Company.FullForm;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Currency;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Company.ModelAddressInfo;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLCompany;
import com.example.a0426611017.carsurvey.R;

import java.util.List;

/**
 * Created by c201017001 on 02/01/2018.
 */

public class ObjectAddressInfoFull {

    private ViewGroup container;
    private View view;
    private Context context;
    private ModelDDLCompany ddlCompany;

    private PreviewImage previewImage = new PreviewImage();

    private TextView nameFileBuildingImage, pathBiuldingImageHidden;
    private EditText address, rt, rw, zipcode, kelurahan, kecamatan, city, phone, fax,
            price, stayLength, description, notes;
    private Button buttonBuildingImage;
    private Spinner  addressType, dropdownLocationClass, dropdownOwnership;

    public ObjectAddressInfoFull(ViewGroup viewGroup, LayoutInflater inflater, ModelDDLCompany ddlCompany) {
        this.context = viewGroup.getContext();
        this.view = inflater.inflate(R.layout.fragment_company_address_info_pending, container, false);
        this.container = viewGroup;
        this.ddlCompany = ddlCompany;
        findView();
    }

    public View getView(){
        return view;
    }

    public TextView getNameFileBuildingImage() {
        return nameFileBuildingImage;
    }

    public TextView getPathBiuldingImageHidden() {
        return pathBiuldingImageHidden;
    }

    public void setNameFileBuildingImage(String nameFileBuildingImage) {
        this.nameFileBuildingImage.setText(nameFileBuildingImage);
        this.nameFileBuildingImage.setError(null);
    }

    public void setPathBiuldingImageHidden(String pathBiuldingImageHidden) {
        this.pathBiuldingImageHidden.setText(pathBiuldingImageHidden);
    }

    public Button getButtonBuildingImage() {
        return buttonBuildingImage;
    }

    public void findView() {

        address = view.findViewById(R.id.editTextAddressPending);
        rt = view.findViewById(R.id.editTextRtPending);
        rw = view.findViewById(R.id.editTextRWPending);
        zipcode = view.findViewById(R.id.editTextZipCodePending);
        kelurahan = view.findViewById(R.id.editTextKelurahanPending);
        kecamatan = view.findViewById(R.id.editTextKecamatanPending);
        city = view.findViewById(R.id.editTextCityPending);
        phone = view.findViewById(R.id.editTextPhonePending);
        fax = view.findViewById(R.id.editTextFaxPending);

        price = view.findViewById(R.id.editTextPricePending);
        stayLength = view.findViewById(R.id.editTextStayLengthPending);
        description = view.findViewById(R.id.editTextDescriptionPending);
        notes = view.findViewById(R.id.editTextNotesPending);

        nameFileBuildingImage = view.findViewById(R.id.textViewNamaFile);
        nameFileBuildingImage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    nameFileBuildingImage.setError(null);
                }
            }
        });
        pathBiuldingImageHidden = view.findViewById(R.id.textViewNameFileHidden);

        buttonBuildingImage = view.findViewById(R.id.buttonBuildingImagePennding);
        buttonBuildingImage.setEnabled(false);

        addressType = view.findViewById(R.id.spinnerAddressTypePending);
        dropdownLocationClass = view.findViewById(R.id.spinnerLocationClassPending);
        dropdownOwnership = view.findViewById(R.id.spinnerOwnershipPending);

        if (ddlCompany != null) {
            addressType.setAdapter(setAdapterItemList(ddlCompany.getAddressTypeName()));
            dropdownLocationClass.setAdapter(setAdapterItemList(ddlCompany.getBuildingLocationClass()));
            dropdownOwnership.setAdapter(setAdapterItemList(ddlCompany.getBuildingOwnerShip()));
        }

        phone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (phone.getText().toString().length() < 7) {
                    phone.setError("Minimum 7 digits");
                }
            }
        });

        zipcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (zipcode.getText().toString().length() < 5) {
                    zipcode.setError("Minimum 5 digits");
                }
            }
        });

        fax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (fax.getText().toString().length() > 0 && fax.getText().toString().length() < 7) {
                    fax.setError("Minimum 7 digits");
                }
            }
        });

        price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equals("")) {
                    price.removeTextChangedListener(this);

                    String formatted = Currency.set(price);
                    price.setText(formatted.replaceAll("[$]", ""));
                    price.setSelection(formatted.length() - 1);
                    price.addTextChangedListener(this);
                }
            }
        });
    }

    private ArrayAdapter<String> setAdapterItemList(List list){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl , list);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;

    }

    public void setForm(final ModelAddressInfo buildingInfo){

        address.setText(buildingInfo.getAddress());
        rt.setText(buildingInfo.getRt());
        rw.setText(buildingInfo.getRw());
        zipcode.setText(buildingInfo.getZipcode());

        kelurahan.setText(buildingInfo.getKelurahan());
        kecamatan.setText(buildingInfo.getKecamatan());
        city.setText(buildingInfo.getCity());
        phone.setText(buildingInfo.getPhone());

        fax.setText(buildingInfo.getFax());
        price.setText(buildingInfo.getPrice().replaceAll("[,]", ""));
        stayLength.setText(buildingInfo.getStayLength());
        description.setText(buildingInfo.getDescription());
        notes.setText(buildingInfo.getNotes());

        String fileName = "No selected file";
        if(buildingInfo.getBiuldingImageUrl() != null && !buildingInfo.getBiuldingImageUrl().equals("")){
            if(buildingInfo.getBiuldingImageUrl().startsWith("http")){
                fileName = buildingInfo.getBiuldingImageUrl().split("/")[6];
                previewImage.setPreviewImage(buildingInfo.getBiuldingImageUrl(), context, nameFileBuildingImage);
            }else{
                if(buildingInfo.getNameFileBuildingImage() != null){
                    fileName = buildingInfo.getNameFileBuildingImage();
                    previewImage.setPreviewImage(fileName, context, nameFileBuildingImage);
                }
            }

        }
        nameFileBuildingImage.setText(fileName);
        pathBiuldingImageHidden.setText(buildingInfo.getBiuldingImageUrl());

        if(ddlCompany != null){
            addressType.setSelection(ddlCompany.getAddressTypeName().indexOf(buildingInfo.getAddressType()));
            dropdownLocationClass.setSelection(ddlCompany.getBuildingLocationClass().indexOf(buildingInfo.getLocationClass()));
            dropdownOwnership.setSelection(ddlCompany.getBuildingOwnerShip().indexOf(buildingInfo.getOwnership()));
        }

    }

    public ModelAddressInfo getForm(){
        ModelAddressInfo modelAddressInfo = new ModelAddressInfo();

        modelAddressInfo.setModified(true);
        modelAddressInfo.setAddress(address.getText().toString());
        modelAddressInfo.setRt(rt.getText().toString());
        modelAddressInfo.setRw(rw.getText().toString());
        modelAddressInfo.setZipcode(zipcode.getText().toString());
        modelAddressInfo.setKelurahan(kelurahan.getText().toString());
        modelAddressInfo.setKecamatan(kecamatan.getText().toString());
        modelAddressInfo.setCity(city.getText().toString());
        modelAddressInfo.setPhone(phone.getText().toString());
        modelAddressInfo.setFax(fax.getText().toString());
        modelAddressInfo.setPrice(price.getText().toString().replaceAll("[,]", ""));
        modelAddressInfo.setStayLength(stayLength.getText().toString());
        modelAddressInfo.setDescription(description.getText().toString());
        modelAddressInfo.setNotes(notes.getText().toString());
        modelAddressInfo.setNameFileBuildingImage(nameFileBuildingImage.getText().toString());
        modelAddressInfo.setBiuldingImageUrl(pathBiuldingImageHidden.getText().toString());
        modelAddressInfo.setAddressType(addressType.getSelectedItem().toString());
        modelAddressInfo.setLocationClass(dropdownLocationClass.getSelectedItem().toString());
        modelAddressInfo.setOwnership(dropdownOwnership.getSelectedItem().toString());

        return modelAddressInfo;
    }

    public ModelAddressInfo getComplete(){
        ModelAddressInfo modelAddressInfo = getForm();

        modelAddressInfo.setModified(true);
        modelAddressInfo.setComplete(true);

        if(modelAddressInfo.getAddress().length() == 0){
            modelAddressInfo.setComplete(false);
            address.setError("Required");
        }else if(modelAddressInfo.getAddress().trim().length() == 0){
            modelAddressInfo.setComplete(false);
            address.setError("Whitespaces only detected");
        }

        if(modelAddressInfo.getRt().length() == 0){
            modelAddressInfo.setComplete(false);
            rt.setError("Required");
        }else if(modelAddressInfo.getRt().trim().length() == 0){
            modelAddressInfo.setComplete(false);
            rt.setError("Whitespaces only detected");
        }

        if(modelAddressInfo.getRw().length() == 0){
            modelAddressInfo.setComplete(false);
            rw.setError("Required");
        }else if(modelAddressInfo.getRw().trim().length() == 0){
            modelAddressInfo.setComplete(false);
            rw.setError("Whitespaces only detected");
        }

        if(modelAddressInfo.getZipcode().length() == 0){
            modelAddressInfo.setComplete(false);
            zipcode.setError("Required");
        }else if(modelAddressInfo.getZipcode().trim().length() == 0){
            modelAddressInfo.setComplete(false);
            zipcode.setError("Whitespaces only detected");
        }

        if(modelAddressInfo.getKelurahan().length() == 0){
            modelAddressInfo.setComplete(false);
            kelurahan.setError("Required");
        }else if(modelAddressInfo.getKelurahan().trim().length() == 0){
            modelAddressInfo.setComplete(false);
            kelurahan.setError("Whitespaces only detected");
        }

        if(modelAddressInfo.getKecamatan().length() == 0){
            modelAddressInfo.setComplete(false);
            kecamatan.setError("Requires");
        }else if(modelAddressInfo.getKecamatan().trim().length() == 0){
            modelAddressInfo.setComplete(false);
            kecamatan.setError("Whitespaces only detected");
        }

        if (modelAddressInfo.getCity().length() == 0){
            modelAddressInfo.setComplete(false);
            city.setError("Required");
        }else if(modelAddressInfo.getCity().trim().length() == 0){
            modelAddressInfo.setComplete(false);
            city.setError("Whitespaces onlt detected");
        }

        if (modelAddressInfo.getPhone().length() == 0){
            modelAddressInfo.setComplete(false);
            phone.setError("Required");
        }else if(modelAddressInfo.getPhone().trim().length() == 0){
            modelAddressInfo.setComplete(false);
            phone.setError("Whitespaces only detected");
        }

//        if(modelAddressInfo.getFax().length() == 0){
//            modelAddressInfo.setComplete(false);
//            fax.setError("Required");
//        }else if(modelAddressInfo.getFax().trim().length() == 0){
//            modelAddressInfo.setComplete(false);
//            fax.setError("Whitespaces only detected");
//        }

//        if (modelAddressInfo.getPrice().length() == 0){
//            modelAddressInfo.setComplete(false);
//            price.setError("Required");
//        }else if(modelAddressInfo.getPrice().trim().length() == 0){
//            modelAddressInfo.setComplete(false);
//            price.setError("Whitespaces only detected");
//        }

//        if (modelAddressInfo.getStayLength().length() == 0){
//            modelAddressInfo.setComplete(false);
//            stayLength.setError("Required");
//        } else if (modelAddressInfo.getStayLength().trim().length() == 0) {
//            modelAddressInfo.setComplete(false);
//            stayLength.setError("Whitespaces only detected");
//        }

//        if (modelAddressInfo.getDescription().length() == 0) {
//            modelAddressInfo.setComplete(false);
//            description.setError("Required");
//        } else if (modelAddressInfo.getDescription().trim().length() == 0) {
//            modelAddressInfo.setComplete(false);
//            description.setError("Whitespaces only detectec");
//        }

//        if (modelAddressInfo.getNotes().length() == 0) {
//            modelAddressInfo.setComplete(false);
//            notes.setError("Requires");
//        } else if (modelAddressInfo.getNotes().trim().length() == 0) {
//            modelAddressInfo.setComplete(false);
//            notes.setError("Whitespaces only detected");
//        }

//        if (modelAddressInfo.getNameFileBuildingImage().length() == 0) {
//            modelAddressInfo.setComplete(false);
//            nameFileBuildingImage.setError("Required");
//        } else if (modelAddressInfo.getNameFileBuildingImage().trim().length() == 0) {
//            modelAddressInfo.setComplete(false);
//            nameFileBuildingImage.setError("Whitespaces only detected");
//        } else if (modelAddressInfo.getNameFileBuildingImage().equals("No selected file")) {
//            modelAddressInfo.setComplete(false);
//            nameFileBuildingImage.setError("Required");
//        }

        return modelAddressInfo;
    }

}
