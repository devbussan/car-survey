package com.example.a0426611017.carsurvey.Model.Company;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 0414831216 on 2/26/2018.
 */

public class ModelFullFormCompany {
    @SerializedName("COMPANY_CUSTDATA_ID")
    private String companyCustId;

    @SerializedName("COMPANY_CUSTDATA_STATUS")
    private String companyCustStatus;

    @SerializedName("COMPANY_ADDR")
    private String companyAddr;

    @SerializedName("COMPANY_EVIDENCE")
    private String companyEvidence;

    public String getCompanyAddr() {
        return companyAddr;
    }

    public void setCompanyAddr(String companyAddr) {
        this.companyAddr = companyAddr;
    }

    public String getCompanyEvidence() {
        return companyEvidence;
    }

    public void setCompanyEvidence(String companyEvidence) {
        this.companyEvidence = companyEvidence;
    }

    public String getCompanyCustId() {
        return companyCustId;
    }

    public void setCompanyCustId(String companyCustId) {
        this.companyCustId = companyCustId;
    }

    public String getCompanyCustStatus() {
        return companyCustStatus;
    }

    public void setCompanyCustStatus(String companyCustStatus) {
        this.companyCustStatus = companyCustStatus;
    }
}
