package com.example.a0426611017.carsurvey.Model.Personal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 0426591017 on 2/12/2018.
 */

public class ModelDataPersonalSG implements Serializable {
    @SerializedName("PERSONAL_ORDER_ID")
    private String personal_order_id;

    @SerializedName("CUST_DATA_ID")
    private String cust_data_id;

    @SerializedName("CUSTMODEL")
    private String custmodel;

    @SerializedName("CUSTNAME")
    private String custname;

    @SerializedName("IDTYPE")
    private String idtype;

    @SerializedName("IDNUMBER")
    private String idnumber;

    private String idimg;

    @SerializedName("IDEXPDATE")
    private String idexpdate;

    private String idexpdateFormat;

    @SerializedName("IDIMGURL")
    private String idimgurl;

    @SerializedName("GENDERID")
    private String genderid;

    @SerializedName("BIRTHPLACE")
    private String birthplace;

    @SerializedName("BIRTHDATE")
    private String birthdate;

    private String birthdateFormat;

    @SerializedName("NPWP")
    private String npwpnum;

    private String npwpimg;

    @SerializedName("NPWPIMGURL")
    private String npwpimgurl;

    @SerializedName("MOTHERMAIDENNAME")
    private String mothermaidenname;

    @SerializedName("USRCRT")
    private String usrcrt;

    @SerializedName("STATUS")
    private String status;

    @SerializedName("PERSONALSG_CUSTDATA_ID")
    private String personal_custdata_return;

    @SerializedName("PERSONALSG_CUSTDATA_STATUS")
    private String personal_status_return;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getIdexpdateFormat() {
        return idexpdateFormat;
    }

    public void setIdexpdateFormat(String idexpdateFormat) {
        this.idexpdateFormat = idexpdateFormat;
    }

    public String getBirthdateFormat() {
        return birthdateFormat;
    }

    public void setBirthdateFormat(String birthdateFormat) {
        this.birthdateFormat = birthdateFormat;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getIdimg() {
        return idimg;
    }

    public void setIdimg(String idimg) {
        this.idimg = idimg;
    }

    public String getNpwpimg() {
        return npwpimg;
    }

    public void setNpwpimg(String npwpimg) {
        this.npwpimg = npwpimg;
    }

    public String getPersonal_order_id() {
        return personal_order_id;
    }

    public void setPersonal_order_id(String personal_order_id) {
        this.personal_order_id = personal_order_id;
    }

    public String getCust_data_id() {
        return cust_data_id;
    }

    public void setCust_data_id(String cust_data_id) {
        this.cust_data_id = cust_data_id;
    }

    public String getCustmodel() {
        return custmodel;
    }

    public void setCustmodel(String custmodel) {
        this.custmodel = custmodel;
    }

    public String getCustname() {
        return custname;
    }

    public void setCustname(String custname) {
        this.custname = custname;
    }

    public String getIdtype() {
        return idtype;
    }

    public void setIdtype(String idtype) {
        this.idtype = idtype;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }

    public String getIdexpdate() {
        return idexpdate;
    }

    public void setIdexpdate(String idexpdate) {
        this.idexpdate = idexpdate;
    }

    public String getIdimgurl() {
        return idimgurl;
    }

    public void setIdimgurl(String idimgurl) {
        this.idimgurl = idimgurl;
    }

    public String getGenderid() {
        return genderid;
    }

    public void setGenderid(String genderid) {
        this.genderid = genderid;
    }

    public String getBirthplace() {
        return birthplace;
    }

    public void setBirthplace(String birthplace) {
        this.birthplace = birthplace;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getNpwpnum() {
        return npwpnum;
    }

    public void setNpwpnum(String npwpnum) {
        this.npwpnum = npwpnum;
    }

    public String getNpwpimgurl() {
        return npwpimgurl;
    }

    public void setNpwpimgurl(String npwpimgurl) {
        this.npwpimgurl = npwpimgurl;
    }

    public String getMothermaidenname() {
        return mothermaidenname;
    }

    public void setMothermaidenname(String mothermaidenname) {
        this.mothermaidenname = mothermaidenname;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUsrcrt() {
        return usrcrt;
    }

    public void setUsrcrt(String usrcrt) {
        this.usrcrt = usrcrt;
    }

    public String getPersonal_custdata_return() {
        return personal_custdata_return;
    }

    public void setPersonal_custdata_return(String personal_custdata_return) {
        this.personal_custdata_return = personal_custdata_return;
    }

    public String getPersonal_status_return() {
        return personal_status_return;
    }

    public void setPersonal_status_return(String personal_status_return) {
        this.personal_status_return = personal_status_return;
    }
}
