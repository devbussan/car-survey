package com.example.a0426611017.carsurvey.Company;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.AddNew.Company.ActivityNewOrderCompany;
import com.example.a0426611017.carsurvey.Fragment.Company.FragmentCompanyFullForm;
import com.example.a0426611017.carsurvey.Fragment.Company.FragmentCompanyOrderForm;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


/**
 * Created by c201017001 on 25/01/2018.
 */

public class ActivityCompanyTab extends Fragment {

    SharedPreferences sharedpreferences;
    private View view;
    private String search;
    private int tabCurrent;
    private Context context;
    private TabLayout tabLayout;
    private Fragment previousFragment;
    private int menu;

    private TextView textPeriode;
    private String periode;

    public ActivityCompanyTab loadSearch(String search, int tabCurrent, int menu) {
        ActivityCompanyTab activityCompany = new ActivityCompanyTab();
        Bundle args = new Bundle();
        args.putString("search", search);
        args.putInt(Constans.KEY_TAB, tabCurrent);
        args.putInt("menu", menu);
        activityCompany.setArguments(args);
        return activityCompany;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = container.getContext();
        final Window window = getActivity().getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.color_company));

        view = inflater.inflate(R.layout.activity_main_tab, container, false);
        search = getArguments().getString("search");
        sharedpreferences = context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, Context.MODE_PRIVATE);

        tabCurrent = getArguments().getInt(Constans.KEY_TAB);
        if (tabCurrent == -1) {
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putInt(Constans.KEY_TAB, 0);
            editor.apply();
            editor.commit();
        }

        Log.d(Constans.KEY_TAB, "Tab Current : " + tabCurrent);

        menu = getArguments().getInt("menu", 0);

        textPeriode = view.findViewById(R.id.periode_grid);

        DateFormat dateFormat = new SimpleDateFormat("MMMM yyyy", new Locale("id"));
        DateFormat dateFormat2 = new SimpleDateFormat("MMMM yyyy", new Locale("id"));
        Date date1 = new Date();
        Log.d("Info", "Date Now : " + date1);
        String monthNow = dateFormat.format(date1);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -2);
        Date date2 = cal.getTime();
        Log.d("Info", "Date - 90 : " + date2);
        String monthYesterday = "";
        if (date2.getYear() != date1.getYear()) {
            monthYesterday = dateFormat2.format(date2);
        } else {
            monthYesterday = dateFormat2.format(date2).split(" ")[0];
        }

        periode = getString(R.string.text_dashboard_periode) + " " + monthYesterday + " - " + monthNow;
        textPeriode.setText(periode);

        ImageView actionButtonNext = view.findViewById(R.id.floatingActionGoNext);
        tabLayout = view.findViewById(R.id.tab_layout);
        switch (menu) {
            case Constans.MENU_COMPANY_FULL_FORM:
                actionButtonNext.setImageResource(R.drawable.icon_order);
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_INPUT));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_CHECKHQ));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_RETURN));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_REJECT));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_APPROVE));
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                tabLayout.setBackgroundColor(getResources().getColor(R.color.color_company));
                tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.color_personal));

//                adapter = new PagerAdapterCompanyFullForm
//                        (getFragmentManager(), tabLayout.getTabCount(), search, menu);
                break;
            case Constans.MENU_COMPANY_ORDER_FORM:
                actionButtonNext.setImageResource(R.drawable.icon_full);
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_DRAFT));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_PENDING));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_RETURN));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_REJECT));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_CLEAR));
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                tabLayout.setBackgroundColor(getResources().getColor(R.color.color_company));
                tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.color_personal));

//                adapter = new PagerAdapterCompanyOrderForm
//                        (getFragmentManager(), tabLayout.getTabCount(), search, menu);
                break;
            default:
                break;
        }

        if (tabCurrent != -1) {
            TabLayout.Tab tab = tabLayout.getTabAt(tabCurrent);
            assert tab != null;
            tab.select();
            setCurrentTab(tabCurrent);
        } else {
            setCurrentTab(0);
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabCurrent = tab.getPosition();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt(Constans.KEY_TAB, tabCurrent);
                editor.apply();
                editor.commit();
                removeFragment(previousFragment);

                setCurrentTab(tabCurrent);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        FloatingActionButton actionButtonAddNew = view.findViewById(R.id.floatingAddNew);
        actionButtonAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityNewOrderCompany.class);
//                Intent intent = new Intent(context, ActivityCompanyFullEdit.class);
                intent.putExtra(Constans.KEY_IS_NEW, true);
                startActivity(intent);
            }
        });
        actionButtonAddNew.setBackgroundColor(getResources().getColor(R.color.color_company));

        actionButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menu == Constans.MENU_COMPANY_ORDER_FORM){
                    Intent intent = new Intent(context, MainActivityDrawer.class);
                    intent.putExtra(Constans.KEY_MENU, Constans.MENU_COMPANY_FULL_FORM);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(context, MainActivityDrawer.class);
                    intent.putExtra(Constans.KEY_MENU, Constans.MENU_COMPANY_ORDER_FORM);
                    startActivity(intent);
                }
            }
        });

        return view;
    }

    private void setCurrentTab(int position) {
        if (menu == Constans.MENU_COMPANY_FULL_FORM) {
            if (position == Constans.TAB_REJECT_ORDER || position == Constans.TAB_COMPLETE) {
                textPeriode.setVisibility(View.VISIBLE);
            } else {
                textPeriode.setVisibility(View.GONE);
            }
            replaceFragment(new FragmentCompanyFullForm().newInstance(search, position, menu));
        } else {
            if (position == Constans.TAB_REJECT_ORDER) {
                textPeriode.setVisibility(View.VISIBLE);
            } else {
                textPeriode.setVisibility(View.GONE);
            }
            replaceFragment(new FragmentCompanyOrderForm().newInstance(search, position, menu));
        }
    }

    public void replaceFragment(Fragment fragment) {
        previousFragment = fragment;
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.replace(R.id.frame_container, fragment);
        ft.commit();
    }

    public void removeFragment(Fragment fragment) {
        if(fragment != null) {
            FragmentManager fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.remove(fragment);
            ft.commit();
        }
    }
}
