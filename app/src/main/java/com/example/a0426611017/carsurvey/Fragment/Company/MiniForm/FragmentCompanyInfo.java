package com.example.a0426611017.carsurvey.Fragment.Company.MiniForm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelDetailCompanyMiniForm;
import com.example.a0426611017.carsurvey.Object.Company.MiniForm.ObjcetCompanyInfo;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailCompanyMiniForm;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCompanyInfo extends Fragment {

    private Context context;
    private View view;
    private ObjcetCompanyInfo objcetCompanyInfo;

    private String compayOrderId = "0";

    private ProgressDialog progressDialog;
    private ApiInterface apiInterface;

    private Gson gson = new Gson();

    private ResponseDetailCompanyMiniForm responseDetailCompanyMiniForm = new ResponseDetailCompanyMiniForm();

    private ModelDetailCompanyMiniForm modelDetailCompanyMiniForm = new ModelDetailCompanyMiniForm();

    public FragmentCompanyInfo newInstance(ModelDetailCompanyMiniForm modelDetailCompanyMiniForm, String compayOrderId) {
        FragmentCompanyInfo fragmentCompanyInfo = new FragmentCompanyInfo();
        Bundle bundle = new Bundle();
        bundle.putSerializable("modelDetailCompanyMiniForm", modelDetailCompanyMiniForm);
        bundle.putString("compayOrderId", compayOrderId);
        fragmentCompanyInfo.setArguments(bundle);
        return fragmentCompanyInfo;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelDetailCompanyMiniForm = (ModelDetailCompanyMiniForm) getArguments().getSerializable("modelDetailCompanyMiniForm");
            compayOrderId = getArguments().getString("compayOrderId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objcetCompanyInfo = new ObjcetCompanyInfo(container, inflater);
        view = objcetCompanyInfo.getView();
        context = container.getContext();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading......");
        progressDialog.setCancelable(false);

        Log.d("compayOrderId", "asd : "+compayOrderId);

        if(modelDetailCompanyMiniForm.isModified()){
            setForm(modelDetailCompanyMiniForm);
        }else if(compayOrderId != null){
            getDetailCompanyOrder(compayOrderId);
        }

        return view;
    }

    public ModelDetailCompanyMiniForm getForm(){
        return objcetCompanyInfo.getForm();
    }

    public ModelDetailCompanyMiniForm getComplete(){
        return objcetCompanyInfo.getFormComplete();
    }

    public void setForm(ModelDetailCompanyMiniForm modelDetailCompanyMiniForm){
        objcetCompanyInfo.setForm(modelDetailCompanyMiniForm);
    }

    private void getDetailCompanyOrder(final String companyOrderId) {
        progressDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailCompanyMiniForm> contactOrderID = apiInterface.getDetailCompanyMiniForm(companyOrderId);
            contactOrderID.enqueue(new Callback<ResponseDetailCompanyMiniForm>() {
                @Override
                public void onResponse(Call<ResponseDetailCompanyMiniForm> call, Response<ResponseDetailCompanyMiniForm>
                        response) {
                    if (response.isSuccessful()) {
                        modelDetailCompanyMiniForm = response.body().getModelDetailCompanyMiniForm();
                        setForm(modelDetailCompanyMiniForm);

                        progressDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailCompanyMiniForm = gson.fromJson(response.errorBody().string(), ResponseDetailCompanyMiniForm.class);
                            error = responseDetailCompanyMiniForm.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(getString(R.string.dialog_request_time_out));
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        getString(R.string.dialog_try_again));
                                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailCompanyOrder(compayOrderId);
                                    }
                                });
                            }else if(!error.equals("Data Not Found")) {
                                Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(getString(R.string.dialog_error_server));
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    getString(R.string.dialog_try_again));
                            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailCompanyOrder(compayOrderId);
                                }
                            });
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(getString(R.string.dialog_error_server));
                        b.setCancelable(false);
                        b.setMessage(error+"\n" +
                                getString(R.string.dialog_try_again));
                        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailCompanyOrder(compayOrderId);
                            }
                        });
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(getString(R.string.dialog_error_server));
                        b.setCancelable(false);
                        b.setMessage(getString(R.string.dialog_request_time_out)+"\n" +
                                getString(R.string.dialog_try_again));
                        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailCompanyOrder(compayOrderId);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailCompanyMiniForm> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle(getString(R.string.dialog_error_server));
                    b.setCancelable(false);
                    b.setMessage(t.toString()+"\n" +
                            getString(R.string.dialog_try_again));
                    b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailCompanyOrder(companyOrderId);
                        }
                    });
                    progressDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(getString(R.string.dialog_error_server));
            b.setCancelable(false);
            b.setMessage(getString(R.string.dialog_internet_connect)+"\n" +
                    getString(R.string.dialog_try_again));
            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailCompanyOrder(compayOrderId);
                }
            });
            progressDialog.dismiss();
        }
    }

}
