package com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonalSG;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by 0426591017 on 2/15/2018.
 */

public class ObjectDataPersonalSGComplete {
    private View view;
    private ViewGroup container;
    private Context context;

    private TextView namePersonalSgIdImage, namePersonalSgNpwpImage,
            customerSgName, idNumber, idExpiredDate,
            birthPlace, dateBirthDate, npwp, motherMaidenName,
            customerModel, idType, gender;

    public ObjectDataPersonalSGComplete(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_data_personal_sg_complete, viewGroup, false);

        setField();
    }

    public View getView() {
        return view;
    }

    private void setField() {
        customerModel = view.findViewById(R.id.value_customer_model);
        idType = view.findViewById(R.id.value_id_type);
        customerSgName = view.findViewById(R.id.value_customer_name);
        namePersonalSgIdImage = view.findViewById(R.id.value_id_image);
        idNumber = view.findViewById(R.id.value_id_number);
        idExpiredDate = view.findViewById(R.id.value_id_expired_date);
        gender = view.findViewById(R.id.value_gender);
        birthPlace = view.findViewById(R.id.value_birth_place);
        dateBirthDate = view.findViewById(R.id.value_birth_date);
        npwp = view.findViewById(R.id.value_npwp);
        namePersonalSgNpwpImage = view.findViewById(R.id.value_npwp_image);
        motherMaidenName = view.findViewById(R.id.value_mother_maiden_name);
    }

    public void setForm(final ModelDataPersonalSG modelDataPersonalSg) {
        customerModel.setText(modelDataPersonalSg.getCustmodel());
        idType.setText(modelDataPersonalSg.getIdtype());
        customerSgName.setText(modelDataPersonalSg.getCustname());

        if (modelDataPersonalSg.getIdimgurl() != null){
            if (!modelDataPersonalSg.getIdimgurl().equals("")){
                namePersonalSgIdImage.setText(modelDataPersonalSg.getIdimgurl().split("/")[6]);
            }else{
                namePersonalSgIdImage.setText(modelDataPersonalSg.getIdimgurl());
            }
        }else{
            namePersonalSgIdImage.setText(modelDataPersonalSg.getIdimg());
        }

        idNumber.setText(modelDataPersonalSg.getIdnumber());
        if (modelDataPersonalSg.getIdexpdate() != null && !modelDataPersonalSg.getIdexpdate().equals("")) {
            modelDataPersonalSg.setIdexpdate(
                    modelDataPersonalSg.getIdexpdate().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelDataPersonalSg.getIdexpdate().substring(4, 6))]
                            + " " + modelDataPersonalSg.getIdexpdate().substring(0, 4)
            );
        }
        idExpiredDate.setText(modelDataPersonalSg.getIdexpdate());
        birthPlace.setText(modelDataPersonalSg.getBirthplace());
        gender.setText(modelDataPersonalSg.getGenderid());
        if (modelDataPersonalSg.getBirthdate() != null && !modelDataPersonalSg.getBirthdate().equals("")) {
            modelDataPersonalSg.setBirthdate(
                    modelDataPersonalSg.getBirthdate().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelDataPersonalSg.getBirthdate().substring(4, 6))]
                            + " " + modelDataPersonalSg.getBirthdate().substring(0, 4)
            );
        }
        dateBirthDate.setText(modelDataPersonalSg.getBirthdate());

        npwp.setText(modelDataPersonalSg.getNpwpnum());

        if (modelDataPersonalSg.getNpwpimgurl() != null){
            if (!modelDataPersonalSg.getNpwpimgurl().equals("")){
                namePersonalSgNpwpImage.setText(modelDataPersonalSg.getNpwpimgurl().split("/")[6]);
            }else{
                namePersonalSgNpwpImage.setText(modelDataPersonalSg.getNpwpimgurl());
            }
        }else{
            namePersonalSgNpwpImage.setText(modelDataPersonalSg.getNpwpimg());
        }

        motherMaidenName.setText(modelDataPersonalSg.getMothermaidenname());

        PreviewImage previewImage = new PreviewImage();
        if (modelDataPersonalSg.getIdimgurl() != null)
            if (!modelDataPersonalSg.getIdimgurl().equals(""))
                previewImage.setPreviewImage(modelDataPersonalSg.getIdimgurl(), context, namePersonalSgIdImage);

        if (modelDataPersonalSg.getNpwpimgurl() != null)
            if (modelDataPersonalSg.getNpwpimgurl().equals(""))
                previewImage.setPreviewImage(modelDataPersonalSg.getNpwpimgurl(), context, namePersonalSgNpwpImage);
    }
}
