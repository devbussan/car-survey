package com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataFinancial;
import com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm.ObjectDataFinancialComplete;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalFinancial;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDataFinancialComplete extends Fragment {
    private View view;
    private ObjectDataFinancialComplete objectDataFinancialComplete;
    private ModelDataFinancial modelDataFinancial = new ModelDataFinancial();

    private String name;
    private Context context;
    private String custid;
    private Gson gson = new Gson();
    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;

    private ResponseDetailPersonalFinancial responseDetailPersonalFinancial = new ResponseDetailPersonalFinancial();

    public FragmentDataFinancialComplete newInstance(String custid, String name){
        FragmentDataFinancialComplete fragmentDataFinancialComplete = new FragmentDataFinancialComplete();
        Bundle args = new Bundle();
        args.putString("custid", custid);
        args.putString("name", name);
        fragmentDataFinancialComplete.setArguments(args);
        return fragmentDataFinancialComplete;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            custid = getArguments().getString("custid");
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        objectDataFinancialComplete = new ObjectDataFinancialComplete(viewGroup, inflater);
        view = objectDataFinancialComplete.getView();
        context = viewGroup.getContext();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading.........");
        loadingDialog.setCancelable(false);

        getDetailFinancial(name, custid);
        return view;
    }

    public void setForm(ModelDataFinancial modelDataFinancial){
        objectDataFinancialComplete.setForm(modelDataFinancial);
    }

    private void getDetailFinancial(final String name, String idcustdata){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalFinancial> finDetail = apiInterface.getDetailPersonalFinancial(name, idcustdata);
            finDetail.enqueue(new Callback<ResponseDetailPersonalFinancial>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalFinancial> call, Response<ResponseDetailPersonalFinancial>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataFinancial = response.body().getModelDataFinancial();
                        Log.d("Result", gson.toJson(modelDataFinancial));
                        modelDataFinancial.setModified(true);
                        setForm(modelDataFinancial);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalFinancial = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalFinancial.class);
                            error = responseDetailPersonalFinancial.getMessage();
                            if(!error.equals("Data Not Found")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage("Cannot Connect to Server \n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailFinancial(name, custid);
                                    }
                                });
                                b.show();
                            }else{
                                Snackbar.make(view.findViewById(R.id.finance_detail), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage("Cannot Connect to Server \n" +
                                    "Please try Again ?");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailFinancial(name, custid);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server \n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailFinancial(name, custid);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalFinancial> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server \n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailFinancial(name, custid);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server \n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailFinancial(name, custid);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }

}