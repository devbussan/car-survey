package com.example.a0426611017.carsurvey.Fragment.Personal.MiniForm;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalInfoMini;
import com.example.a0426611017.carsurvey.Object.Personal.MiniForm.ObjectDataPersonalInfo;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseMiniFormPersonal;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class FragmentDataPersonalInfoOrder extends Fragment {
    private View view;
    private Context context;
    private int cameraIdxButton = 0;
    private String nameFile;
    private TakePicture takePicture = new TakePicture();
    private PreviewImage previewImage = new PreviewImage();
    private TextView fileNameIdImage, fileNameIdImageUrl,
            fileNamePersonalFamilyCardImage,
            fileNamePersonalFamilyCardImageUrl;

    private ObjectDataPersonalInfo objectDataPersonalInfo;
    private ModelPersonalInfoMini modelPersonalInfoMini;
    private ModelDDLPersonal ddlPersonal = new ModelDDLPersonal();
    private FileFunction fileFunction = new FileFunction();

    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private String personalOrderId = null;
    private Gson gson = new Gson();
    private boolean isNew = true;

    private ResponseMiniFormPersonal responseMiniFormPersonal = new ResponseMiniFormPersonal();

    public static FragmentDataPersonalInfoOrder newInstance(ModelPersonalInfoMini modelPersonalInfoMini, boolean isNew, ModelDDLPersonal ddlPersonal, String orderId) {
        FragmentDataPersonalInfoOrder fragmentDataPersonalInfoOrder = new FragmentDataPersonalInfoOrder();
        Bundle args = new Bundle();
        args.putSerializable("modelPersonalOrder", modelPersonalInfoMini);
        args.putSerializable("ddlPersonal", ddlPersonal);
        args.putString("personalOrderId", orderId);
        args.putBoolean(Constans.KEY_IS_NEW, isNew);
        fragmentDataPersonalInfoOrder.setArguments(args);
        return fragmentDataPersonalInfoOrder;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelPersonalInfoMini = (ModelPersonalInfoMini) getArguments().getSerializable("modelPersonalOrder");
            ddlPersonal = (ModelDDLPersonal) getArguments().getSerializable("ddlPersonal");
            personalOrderId = getArguments().getString("personalOrderId");
            isNew = getArguments().getBoolean(Constans.KEY_IS_NEW);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        objectDataPersonalInfo = new ObjectDataPersonalInfo(container, inflater, ddlPersonal);
        view = objectDataPersonalInfo.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if (modelPersonalInfoMini.isModified()) {
            objectDataPersonalInfo.setForm(modelPersonalInfoMini);
        }else{
            if (!isNew) {
                getDetailPersonalOrder(personalOrderId);
            }
        }

        fileNameIdImage = objectDataPersonalInfo.getTextViewIdImg();
        fileNameIdImageUrl = objectDataPersonalInfo.getTextViewIdImgUrl();
        Button buttonIdImage = objectDataPersonalInfo.getButtonIdImage();

        fileNamePersonalFamilyCardImage = objectDataPersonalInfo.getTextViewPersonalFamilyCardImg();
        fileNamePersonalFamilyCardImageUrl = objectDataPersonalInfo.getTextViewPersonalFamilyCardImgUrl();
        Button buttonPersonalFamilyCard = objectDataPersonalInfo.getBbuttonPersonalFamilyCard();

        callCamera(buttonIdImage,1);
        callCamera(buttonPersonalFamilyCard, 2);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            objectDataPersonalInfo.setEnabledButtonImage();
        }

        return view;
    }

    private void callCamera(Button button, int idx) {
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(v, idxButton);

            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                objectDataPersonalInfo.setEnabledButtonImage();
            }
        }
    }

    public void takePicture(View view, int idx) {
        this.cameraIdxButton = idx;
        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        startActivityForResult(intent, 100);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Toast.makeText(context,requestCode+":"+resultCode,Toast.LENGTH_SHORT).show();
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                switch (cameraIdxButton) {
                    case 1:
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();

                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectDataPersonalInfo.setTextViewIdImg(nameFile);
                        objectDataPersonalInfo.setTextViewIdImgUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNameIdImage);
                        break;
                    case 2:
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();

                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectDataPersonalInfo.setTextViewPersonalFamilyCardImg(nameFile);
                        objectDataPersonalInfo.setTextViewPersonalFamilyCardImgUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNamePersonalFamilyCardImage);
                        break;
                    default:
                        break;
                }
            }
        }
    }


    public ModelPersonalInfoMini getForm(){return objectDataPersonalInfo.getForm();
    }

    public void setForm(ModelPersonalInfoMini modelPersonalInfoMini) {
        objectDataPersonalInfo.setForm(modelPersonalInfoMini);}

    public ModelPersonalInfoMini getComplete(){
        return objectDataPersonalInfo.getComplete();
    }

    private void getDetailPersonalOrder(final String personal) {
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseMiniFormPersonal> personalOrderId = apiInterface.getDetailPersonalMiniForm(personal);
            personalOrderId.enqueue(new Callback<ResponseMiniFormPersonal>() {
                @Override
                public void onResponse(Call<ResponseMiniFormPersonal> call, Response<ResponseMiniFormPersonal> response) {
                    if (response.isSuccessful()) {
                        modelPersonalInfoMini = response.body().getModelPersonalInfoMini();

                        Log.d("MODEL MINI ", gson.toJson(response.body().getModelPersonalInfoMini()));
                        Log.d("MODEL MINI 1", "asd"+modelPersonalInfoMini.getKkimgurl()+"dsadsa"+modelPersonalInfoMini.getIdimgurl());

                        setForm(modelPersonalInfoMini);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseMiniFormPersonal = gson.fromJson(response.errorBody().string(), ResponseMiniFormPersonal.class);
                            error = responseMiniFormPersonal.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailPersonalOrder(personal);
                                    }
                                });
                                b.show();
                            } else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.fragment_data_personal_info_order), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailPersonalOrder(personal);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailPersonalOrder(personal);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseMiniFormPersonal> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailPersonalOrder(personal);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailPersonalOrder(personal);
                }
            });
            b.show();
            Log.e("Error","No Internet Connectivity");
            loadingDialog.dismiss();
        }
    }
}