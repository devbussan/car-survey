package com.example.a0426611017.carsurvey.Object.Personal.FullForm;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataMainData;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.R;

import java.util.List;

/**
 * Created by 0426611017 on 2/12/2018.
 */

public class ObjectDataMainDataPending {
    private View view;
    private Context context;
    private ViewGroup container;
    private ModelDDLPersonal ddlPersonal;
    private LayoutInflater inflater;

    private EditText numDependents, numOfResidence, familyCardNumber, kendaraanYangDimiliki, mobilePhone1, mobilePhone2;

    private TextView namePersonalFamilyCardNumberImage,
            namePersonalFamilyCardNumberImageUrl;

    private Button buttonFamilyCardImage;

    private Spinner salutation, maritalStatus, religion, education;

    public ObjectDataMainDataPending(ViewGroup viewGroup, LayoutInflater inflater, ModelDDLPersonal ddlPersonal) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.ddlPersonal = ddlPersonal;
        this.view = inflater.inflate(R.layout.fragment_data_main_data_pending, container, false);

        setField();
    }

    public View getView() {
        return view;
    }

    public TextView getNamePersonalFamilyCardNumberImage() {
        return namePersonalFamilyCardNumberImage;
    }

    public TextView getNamePersonalFamilyCardNumberImageUrl() {
        return namePersonalFamilyCardNumberImageUrl;
    }

    public void setFilePersonalFamilyCardNumberImage(String filePersonalFamilyCardNumberImage) {
        this.namePersonalFamilyCardNumberImage.setText(filePersonalFamilyCardNumberImage);
    }

    public void setFilePersonalFamilyCardNumberImageUrl(String namePersonalFamilyCardNumberImageUrl) {
        this.namePersonalFamilyCardNumberImageUrl.setText(namePersonalFamilyCardNumberImageUrl);
    }

    public Button getButtonFamilyCardImage() {

        return buttonFamilyCardImage;
    }

    public void setEnabledButtonImage() {
//        buttonFamilyCardImage.setEnabled(true);s
    }

    public void setField() {
        salutation = view.findViewById(R.id.spinner_salutation_pending);
        maritalStatus = view.findViewById(R.id.spinner_marital_status_pending);
        religion = view.findViewById(R.id.spinner_religion_pending);
        education = view.findViewById(R.id.spinner_education_pending);
        numDependents = view.findViewById(R.id.edit_text_num_dependents_pending);
        numOfResidence = view.findViewById(R.id.edit_text_num_of_residences_pending);
        familyCardNumber = view.findViewById(R.id.edit_text_family_card_number_pending);

        namePersonalFamilyCardNumberImage = view.findViewById(R.id.text_view_personal_family_card_number_image_pending);
        namePersonalFamilyCardNumberImage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    namePersonalFamilyCardNumberImage.setError(null);
                }
            }
        });
        namePersonalFamilyCardNumberImageUrl = view.findViewById(R.id.text_view_personal_family_card_number_image_url_pending);
        buttonFamilyCardImage = view.findViewById(R.id.button_camera_family_card_number_image_pending);

        kendaraanYangDimiliki = view.findViewById(R.id.edit_text_kendaraan_yang_dimiliki_pending);
        mobilePhone1 = view.findViewById(R.id.edit_text_mobile_phone_1_pending);
        mobilePhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (mobilePhone1.getText().length() < 7) {
                    mobilePhone1.setError("Minimum 7 digits");
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 7) {
                    mobilePhone1.setError("Minimum 7 digits");
                }
            }
        });

        mobilePhone2 = view.findViewById(R.id.edit_text_mobile_phone_2_pending);
        mobilePhone2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0 && s.length() < 7) {
                    mobilePhone2.setError("Minimum 7 digits");
                }
            }
        });

        if (ddlPersonal != null) {
            salutation.setAdapter(setAdapterItemList(ddlPersonal.getSalution()));
            maritalStatus.setAdapter(setAdapterItemList(ddlPersonal.getMaritalStatus()));
            religion.setAdapter(setAdapterItemList(ddlPersonal.getReligion()));
            education.setAdapter(setAdapterItemList(ddlPersonal.getEducation()));
        }

        buttonFamilyCardImage.setEnabled(false);

    }

    public void setForm(ModelDataMainData modelDataMainData) {
        if (ddlPersonal != null) {
            salutation.setSelection(ddlPersonal.getSalution().indexOf(modelDataMainData.getSalutation()));
            maritalStatus.setSelection(ddlPersonal.getMaritalStatus().indexOf(modelDataMainData.getMaritalstat()));
            religion.setSelection(ddlPersonal.getReligion().indexOf(modelDataMainData.getReligion()));
            education.setSelection(ddlPersonal.getEducation().indexOf(modelDataMainData.getEducation()));
        }

        numDependents.setText(modelDataMainData.getNumdependents());
        numOfResidence.setText(modelDataMainData.getNumofresidence());
        familyCardNumber.setText(modelDataMainData.getKkno());

        namePersonalFamilyCardNumberImageUrl.setText(modelDataMainData.getKkimgurl());

        Log.d("Url image", namePersonalFamilyCardNumberImageUrl.getText().toString());
        PreviewImage previewImage = new PreviewImage();
        String nameKkImage;
        if (modelDataMainData.getKkimgurl().equals("") || modelDataMainData.getKkimgurl() == null) {
            nameKkImage = "No selected file";
        } else {
            if (modelDataMainData.getKkimgurl().startsWith("http")) {
                nameKkImage = modelDataMainData.getKkimgurl().split("/")[6];
                previewImage.setPreviewImage(modelDataMainData.getKkimgurl(), context, namePersonalFamilyCardNumberImage);
            } else {
                nameKkImage = modelDataMainData.getKkimg();
                previewImage.setPreviewImage(nameKkImage, context, namePersonalFamilyCardNumberImage);
            }
        }
        namePersonalFamilyCardNumberImage.setText(nameKkImage);

        kendaraanYangDimiliki.setText(modelDataMainData.getOwnedvehicle());
        mobilePhone1.setText(modelDataMainData.getMobilephone1());
        mobilePhone2.setText(modelDataMainData.getMobilephone2());
    }

    public ModelDataMainData getForm() {
        ModelDataMainData modelDataMainData = new ModelDataMainData();

        modelDataMainData.setSalutation(salutation.getSelectedItem().toString());
        modelDataMainData.setMaritalstat(maritalStatus.getSelectedItem().toString());
        modelDataMainData.setReligion(religion.getSelectedItem().toString());
        modelDataMainData.setEducation(education.getSelectedItem().toString());
        modelDataMainData.setNumdependents(numDependents.getText().toString());
        modelDataMainData.setNumofresidence(numOfResidence.getText().toString());
        modelDataMainData.setKkno(familyCardNumber.getText().toString());
        modelDataMainData.setKkimg(namePersonalFamilyCardNumberImage.getText().toString());
        modelDataMainData.setKkimgurl(namePersonalFamilyCardNumberImageUrl.getText().toString());
        modelDataMainData.setOwnedvehicle(kendaraanYangDimiliki.getText().toString());
        modelDataMainData.setMobilephone1(mobilePhone1.getText().toString());
        modelDataMainData.setMobilephone2(mobilePhone2.getText().toString());

        modelDataMainData.setModified(true);

        return modelDataMainData;
    }

    private ArrayAdapter<String> setAdapterItemList(List items) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl, items);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;
    }

    public ModelDataMainData getComplete() {
        ModelDataMainData modelDataMainData = getForm();
        modelDataMainData.setComplete(true);

        // TODO: FIELD NOT MANDATORY FROM HERE
//        if (modelDataMainData.getNumdependents().length() == 0) {
//            numDependents.setError("Number Dependents is required");
//            modelDataMainData.setComplete(false);
//        }else if(modelDataMainData.getNumdependents().trim().length() == 0){
//            numDependents.setError("Number Dependents not allowed whitespaces only");
//            modelDataMainData.setComplete(false);
//        }
//
//        if (modelDataMainData.getNumofresidence().length() == 0) {
//            numOfResidence.setError("Number Of Residences is required");
//            modelDataMainData.setComplete(false);
//        }else if(modelDataMainData.getNumofresidence().trim().length() == 0){
//            numOfResidence.setError("Number Of Residences not allowed whitespaces only");
//            modelDataMainData.setComplete(false);
//        }
//
//        if (modelDataMainData.getOwnedvehicle().length() == 0) {
//            kendaraanYangDimiliki.setError("Kendaraan yang Dimiliki is required");
//            modelDataMainData.setComplete(false);
//        }else if(modelDataMainData.getOwnedvehicle().trim().length() == 0){
//            kendaraanYangDimiliki.setError("Kendaraan yang Dimiliki not allowed whitespaces only");
//            modelDataMainData.setComplete(false);
//        }

        if (modelDataMainData.getMobilephone1().length() == 0) {
            mobilePhone1.setError("Mobile Phone 1 is required");
            modelDataMainData.setComplete(false);
        }else if(modelDataMainData.getMobilephone1().trim().length() == 0){
            mobilePhone1.setError("Mobile Phone 1 not allowed whitespaces only");
            modelDataMainData.setComplete(false);
        } else if (modelDataMainData.getMobilephone1().trim().length() < 7) {
            mobilePhone1.setError("Minimum 7 digits");
            modelDataMainData.setComplete(false);
        }

        // TODO: FIELD NOT MANDATORY FROM HERE
//        if (modelDataMainData.getMobilephone2().length() == 0) {
//            mobilePhone2.setError("Mobile Phone 2 is required");
//            modelDataMainData.setComplete(false);
//        }else if(modelDataMainData.getMobilephone2().trim().length() == 0){
//            mobilePhone2.setError("Mobile Phone 2 not allowed whitespaces only");
//            modelDataMainData.setComplete(false);
//        } else if (modelDataMainData.getMobilephone2().trim().length() < 7) {
//            mobilePhone2.setError("Minimum 7 digits");
//            modelDataMainData.setComplete(false);
//        }

        return modelDataMainData;
    }

}
