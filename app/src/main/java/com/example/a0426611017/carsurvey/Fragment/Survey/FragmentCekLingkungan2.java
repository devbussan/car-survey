package com.example.a0426611017.carsurvey.Fragment.Survey;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Survey.ModelCekLingkungan;
import com.example.a0426611017.carsurvey.Object.Survey.ObjectCekLingkungan2;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseDetailSurveyEnviCompany;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLSurvey;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCekLingkungan2 extends Fragment {
    private int menu;
    private View view;
    private Context context;
    private ModelDDLSurvey ddlSurvey;
    private ModelCekLingkungan modelCekLingkungan;
    private ObjectCekLingkungan2 objectCekLingkungan2;
    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private Gson gson = new Gson();
    String idEnv = "";
    String idSurvey = "";
    String idOrder = "";
    private ResponseDetailSurveyEnviCompany responseDetailSurveyEnviCompany = new ResponseDetailSurveyEnviCompany();
    private String name;

    public static FragmentCekLingkungan2 newInstance(int menu,
                                                     ModelDDLSurvey ddlSurvey,
                                                     ModelCekLingkungan modelCekLingkungan,
                                                     String idEnv,
                                                     String idSurvey,
                                                     String idOrder) {
        FragmentCekLingkungan2 fragment = new FragmentCekLingkungan2();
        Bundle args = new Bundle();
        args.putInt(Constans.KEY_MENU, menu);
        args.putSerializable("modelCekLingkungan", modelCekLingkungan);
        args.putSerializable("ddlSurvey", ddlSurvey);
        args.putString("idEnv", idEnv);
        args.putString("idSurvey", idSurvey);
        args.putString("idOrder", idOrder);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            menu = getArguments().getInt(Constans.KEY_MENU);
            ddlSurvey = (ModelDDLSurvey) getArguments().getSerializable("ddlSurvey");
            modelCekLingkungan = (ModelCekLingkungan) getArguments().getSerializable("modelCekLingkungan");
            idEnv = getArguments().getString("idEnv");
            idSurvey = getArguments().getString("idSurvey");
            idOrder = getArguments().getString("idOrder");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectCekLingkungan2 = new ObjectCekLingkungan2(container, inflater, ddlSurvey);
        view = objectCekLingkungan2.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if(menu == Constans.MENU_PERSONAL){
            name = Constans.PARAM_WS_PERSONAL;
            objectCekLingkungan2.setFormForPersonal();
        }else{
            name = Constans.PARAM_WS_COMPANY;
            objectCekLingkungan2.setFormForCompany();
        }

        Log.d("Log", "idSurvey : "+idSurvey+" && idEnv : "+idEnv+" && idOrder : "+idOrder);

        if(modelCekLingkungan.isModified()){
            setForm(modelCekLingkungan);
        }else if((idSurvey != null && !idSurvey.equals("0"))
                && (idEnv != null && !idEnv.equals("0"))
                ){
            getDetailEnvMent(idEnv, idSurvey, name);
        }

        return view;
    }

    public ModelCekLingkungan getForm(){
        return objectCekLingkungan2.getForm();
    }

    public void setForm(ModelCekLingkungan modelCekLingkungan){
        objectCekLingkungan2.setForm(modelCekLingkungan);
    }

    public ModelCekLingkungan getComplete(){
        return objectCekLingkungan2.getComplete();
    }

    private void getDetailEnvMent(final String envIdSur, final String idSurvey, final String pName){
        loadingDialog.show();
        if(InternetConnection.checkConnection(context)){
            Call<ResponseDetailSurveyEnviCompany> getDetailEnvMent = apiInterface.getDetailEnviront(pName,envIdSur,idSurvey);
            getDetailEnvMent.enqueue(new Callback<ResponseDetailSurveyEnviCompany>() {
                @Override
                public void onResponse(Call<ResponseDetailSurveyEnviCompany> call, Response<ResponseDetailSurveyEnviCompany> response) {
                    if(response.isSuccessful()){
                        modelCekLingkungan = response.body().getModelCekLingkungan();
                        setForm(modelCekLingkungan);
                        loadingDialog.dismiss();

                    }else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailSurveyEnviCompany = gson.fromJson(response.errorBody().string(),ResponseDetailSurveyEnviCompany.class);
                            error = responseDetailSurveyEnviCompany.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailEnvMent(envIdSur, idSurvey, pName);
                                    }
                                });
                                b.show();
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.fragment_survey_env_pending), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailEnvMent(envIdSur, idSurvey, pName);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    }else {
                        Log.e("Other Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailEnvMent(envIdSur, idSurvey, pName);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailSurveyEnviCompany> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailEnvMent(envIdSur, idSurvey, pName);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailEnvMent(envIdSur, idSurvey, pName);
                }
            });
            b.show();
            Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
            loadingDialog.dismiss();
        }

    }

}
