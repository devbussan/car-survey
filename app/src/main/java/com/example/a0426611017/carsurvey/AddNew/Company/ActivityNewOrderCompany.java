package com.example.a0426611017.carsurvey.AddNew.Company;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.a0426611017.carsurvey.Finance.ActivityCompanyFinancial;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentCompanyInfo;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentContactInfo;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentContactInfo2;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentContactInfo3;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentContactInfo4;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentContactInfo5;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiCompanyOrder;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelDetailCompanyMiniForm;
import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelRequestMiniFormCompany;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseContactOrderId;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by c201017001 on 29/01/2018.
 */

public class ActivityNewOrderCompany extends AppCompatActivity {

    private Button buttonCompanyInfo;
    private Button buttonContactInfo;
    private Button buttonContactInfo2;
    private Button buttonContactInfo3;
    private Button buttonContactInfo4;
    private Button buttonContactInfo5;

    FloatingActionButton actionButtonAddContact;

    private Fragment currentFragment;

    private FragmentCompanyInfo fragmentCompanyInfo = new FragmentCompanyInfo();
    private FragmentContactInfo fragmentContactInfo = new FragmentContactInfo();
    private FragmentContactInfo2 fragmentContactInfo1 = new FragmentContactInfo2();
    private FragmentContactInfo3 fragmentContactInfo2 = new FragmentContactInfo3();
    private FragmentContactInfo4 fragmentContactInfo3 = new FragmentContactInfo4();
    private FragmentContactInfo5 fragmentContactInfo4 = new FragmentContactInfo5();

    private ModelDetailCompanyMiniForm modelDetailCompanyMiniForm = new ModelDetailCompanyMiniForm();
    private ModelDetailContactInfo modelDetailContactInfo = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo1 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo2 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo3 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo4 = new ModelDetailContactInfo();

    private ProgressDialog progressDialog;
    private ApiInterface apiInterface;
    private ResponseContactOrderId responseContactOrderId = new ResponseContactOrderId();

    private boolean isNew = true;
    private String logID = null;
    private String companyOrder;
    private List<String> contactOrder = new ArrayList<>();

    private ApiCompanyOrder apiCompanyOrder;
    private View currentView;
    private Button buttonSave;
    private Gson gson = new Gson();
    private String adminId;

    private int menuCategory = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final Window window = getWindow();
        setContentView(R.layout.activity_new_order_company);
        window.setStatusBarColor(getResources().getColor(R.color.color_company));
        currentView = this.findViewById(android.R.id.content);

        buttonCompanyInfo = findViewById(R.id.button_company_info);
        buttonContactInfo = findViewById(R.id.button_contact_info);
        buttonContactInfo2 = findViewById(R.id.button_contact_info2);
        buttonContactInfo3 = findViewById(R.id.button_contact_info3);
        buttonContactInfo4 = findViewById(R.id.button_contact_info4);
        buttonContactInfo5 = findViewById(R.id.button_contact_info5);
        isNew = getIntent().getBooleanExtra(Constans.KEY_IS_NEW, true);

        adminId = this.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, Context.MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);
        menuCategory = this.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, Context.MODE_PRIVATE).getInt(Constans.KEY_MENU_CATEGORY, 0);

        progressDialog = new ProgressDialog(ActivityNewOrderCompany.this);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (!isNew) {
            progressDialog.show();
            companyOrder = getIntent().getStringExtra(Constans.KeySharedPreference.COMPANY_ORDER_ID);
            logID = this.getSharedPreferences(Constans.KeySharedPreference.LOG_ID, MODE_PRIVATE).getString(Constans.KEY_LOG_ID, null);
            getContactOrderId(companyOrder);
            Log.d("Company Order ID", companyOrder);
        }


        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        Button buttonBack = findViewById(R.id.button_back_new_order_company);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("Check out", "kembali");
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderCompany.this);
                builder.setTitle(R.string.content_information_back_pressed);
                builder.setMessage(getString(R.string.content_question_want_save_data) +
                        "\n" + getString(R.string.content_information_press_yes_save));
                builder.setNegativeButton(getString(R.string.dialog_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Log.e("info", "NO");
                                Intent i = new Intent(ActivityNewOrderCompany.this, MainActivityDrawer.class);
                                i.putExtra(Constans.KEY_MENU, menuCategory);
                                i.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                                startActivity(i);
                                finish();
                            }
                        });
                builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.e("info", "YES");
                        Log.d("Contact Order ID", String.valueOf(contactOrder));
                        buttonSave.callOnClick();
                    }
                });
                builder.show();

            }
        });
        buttonSave = findViewById(R.id.button_save_new_order_company);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ActivityNewOrderCompany.this);
                builder.setTitle(getString(R.string.content_question_want_save_data));
                builder.setMessage(getString(R.string.content_information_save_and_exit));
                builder.setNegativeButton(getString(R.string.dialog_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                progressDialog.dismiss();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int ii) {
                        buttonSave.setEnabled(false);
                        progressDialog.show();
                        saveForm();

                        Log.d("Contact Order ID", String.valueOf(contactOrder));
                        apiCompanyOrder = new ApiCompanyOrder(currentView, ActivityNewOrderCompany.this, progressDialog);


                        final ModelRequestMiniFormCompany modelRequestMiniFormCompany = new ModelRequestMiniFormCompany();
                        modelRequestMiniFormCompany.setCompanyName(modelDetailCompanyMiniForm.getCompanyName());
                        modelRequestMiniFormCompany.setNpwpNo(modelDetailCompanyMiniForm.getNpwpNo());
                        modelRequestMiniFormCompany.setUserCreate(adminId);
                        modelRequestMiniFormCompany.setModified(modelDetailCompanyMiniForm.isModified());
                        List<ModelDetailContactInfo> modelContactInfoMinis = new ArrayList<>();
                        ModelDetailContactInfo contactInfoMiniTemp = new ModelDetailContactInfo();
                        for (int i = 0; i < 5; i++) {
                            switch (i) {
                                case 0:
                                    contactInfoMiniTemp = modelDetailContactInfo;
                                    break;
                                case 1:
                                    contactInfoMiniTemp = modelDetailContactInfo1;
                                    break;
                                case 2:
                                    contactInfoMiniTemp = modelDetailContactInfo2;
                                    break;
                                case 3:
                                    contactInfoMiniTemp = modelDetailContactInfo3;
                                    break;
                                case 4:
                                    contactInfoMiniTemp = modelDetailContactInfo4;
                                    break;
                                default:
                                    break;
                            }

                            if (contactInfoMiniTemp.isModified()) {
                                if (contactOrder.size() > i) {
                                    contactInfoMiniTemp.setContactOrderId(contactOrder.get(i));
                                } else {
                                    contactInfoMiniTemp.setContactOrderId(null);
                                }

                                contactInfoMiniTemp.setInfoFlag(String.valueOf(i + 1));
                                modelContactInfoMinis.add(contactInfoMiniTemp);
                            }
                        }

                        if (isNew) {
                            apiCompanyOrder.saveCompany(modelRequestMiniFormCompany, modelContactInfoMinis);
                        } else {
                            apiCompanyOrder.updateCompany(modelRequestMiniFormCompany, modelContactInfoMinis, companyOrder, contactOrder);
                        }
                    }
                });

                builder.show();

            }
        });


        Button buttonSendToHq = findViewById(R.id.button_sendHq_new_order_company);
        buttonSendToHq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog.show();
                saveForm();
                sendHqForm();
                if (modelDetailCompanyMiniForm.isComplete()
                        && modelDetailContactInfo.isComplete()
                        && modelDetailContactInfo1.isComplete()
                        && (!modelDetailContactInfo2.isModified() || modelDetailContactInfo2.isComplete())
                        && (!modelDetailContactInfo3.isModified() || modelDetailContactInfo3.isComplete())
                        && (!modelDetailContactInfo4.isModified() || modelDetailContactInfo4.isComplete())) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderCompany.this);
                    builder.setTitle(getString(R.string.content_question_are_you_sure));
                    builder.setMessage(getString(R.string.content_information_send_to_hq) +
                            "\n" + getString(R.string.content_information_send_to_hq_2));
                    builder.setNegativeButton(getString(R.string.dialog_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    Log.e("info", "NO");
                                    progressDialog.dismiss();
                                }
                            });
                    builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int j) {
                            saveForm();
                            Log.e("info", "YES");
                            Log.d("Contact Order ID", String.valueOf(contactOrder));
                            apiCompanyOrder = new ApiCompanyOrder(currentView, ActivityNewOrderCompany.this, progressDialog);

                            final ModelRequestMiniFormCompany modelRequestMiniFormCompany = new ModelRequestMiniFormCompany();
                            modelRequestMiniFormCompany.setCompanyName(modelDetailCompanyMiniForm.getCompanyName());
                            modelRequestMiniFormCompany.setNpwpNo(modelDetailCompanyMiniForm.getNpwpNo());
                            modelRequestMiniFormCompany.setUserCreate(adminId);
                            modelRequestMiniFormCompany.setModified(modelDetailCompanyMiniForm.isModified());
                            List<ModelDetailContactInfo> modelCompanyInfoMinis = new ArrayList<>();
                            ModelDetailContactInfo contactInfoMiniTemp = new ModelDetailContactInfo();
                            for (int i = 0; i < 5; i++) {
                                switch (i) {
                                    case 0:
                                        contactInfoMiniTemp = modelDetailContactInfo;
                                        break;
                                    case 1:
                                        contactInfoMiniTemp = modelDetailContactInfo1;
                                        break;
                                    case 2:
                                        contactInfoMiniTemp = modelDetailContactInfo2;
                                        break;
                                    case 3:
                                        contactInfoMiniTemp = modelDetailContactInfo3;
                                        break;
                                    case 4:
                                        contactInfoMiniTemp = modelDetailContactInfo4;
                                        break;
                                    default:
                                        break;
                                }

                                if (contactInfoMiniTemp.isModified()) {
                                    if (contactOrder.size() > i) {
                                        contactInfoMiniTemp.setContactOrderId(contactOrder.get(i));
                                    } else {
                                        contactInfoMiniTemp.setContactOrderId(null);
                                    }
                                    contactInfoMiniTemp.setInfoFlag(String.valueOf(i + 1));
                                    modelCompanyInfoMinis.add(contactInfoMiniTemp);
                                }

                            }
                            Log.d("Contact Info", String.valueOf(modelCompanyInfoMinis.size()));
                            if (isNew) {
                                apiCompanyOrder.saveCompany(modelRequestMiniFormCompany, modelCompanyInfoMinis, true);
                            } else {
                                apiCompanyOrder.updateCompany(modelRequestMiniFormCompany, modelCompanyInfoMinis, companyOrder, contactOrder, logID, true);
                            }
                        }
                    });
                    builder.show();
                } else {
                    if (!modelDetailCompanyMiniForm.isComplete()) {
                        if (buttonCompanyInfo.isEnabled())
                            buttonCompanyInfo.setError("Is not Complete");
                    }
                    if (!modelDetailContactInfo.isComplete()) {
                        if (buttonContactInfo.isEnabled())
                            buttonContactInfo.setError("Is not complete");
                    }
                    if (!modelDetailContactInfo1.isComplete()) {
                        if (buttonContactInfo2.isEnabled())
                            buttonContactInfo2.setError("Is not complete");
                    }
                    if (!modelDetailContactInfo2.isComplete()) {
                        if (buttonContactInfo3.isEnabled())
                            buttonContactInfo3.setError("Is not complete");
                    }
                    if (!modelDetailContactInfo3.isComplete()) {
                        if (buttonContactInfo4.isEnabled())
                            buttonContactInfo4.setError("Is not complete");
                    }
                    if (!modelDetailContactInfo4.isComplete()) {
                        if (buttonContactInfo5.isEnabled())
                            buttonContactInfo5.setError("Is not complete");
                    }
                    Snackbar.make(findViewById(R.id.myLayoutCompany), "Please Fill The Data First",
                            Snackbar.LENGTH_LONG)
                            .show();
                    progressDialog.dismiss();
                }
            }
        });

        buttonCompanyInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCompanyInfo.setError(null);

                buttonCompanyInfo.setEnabled(false);
                buttonCompanyInfo.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_company));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo2.setEnabled(true);
                buttonContactInfo2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo3.setEnabled(true);
                buttonContactInfo3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo4.setEnabled(true);
                buttonContactInfo4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo5.setEnabled(true);
                buttonContactInfo5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_white));

                fragmentCompanyInfo = fragmentCompanyInfo.newInstance(modelDetailCompanyMiniForm, companyOrder);
                saveForm();

                loadFragment(fragmentCompanyInfo);
            }
        });

        buttonContactInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContactInfo.setError(null);

                buttonCompanyInfo.setEnabled(true);
                buttonCompanyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(false);
                buttonContactInfo.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_company));

                buttonContactInfo2.setEnabled(true);
                buttonContactInfo2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo3.setEnabled(true);
                buttonContactInfo3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo4.setEnabled(true);
                buttonContactInfo4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo5.setEnabled(true);
                buttonContactInfo5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_white));

                String contactOrderId = null;
                if (!isNew) {
                    if (contactOrder.size() > 0) {
                        contactOrderId = contactOrder.get(0);
                    }
                }
                fragmentContactInfo = FragmentContactInfo.newInstance(modelDetailContactInfo, contactOrderId, companyOrder, true);

                saveForm();
                loadFragment(fragmentContactInfo);
            }
        });

        buttonContactInfo2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContactInfo2.setError(null);

                buttonCompanyInfo.setEnabled(true);
                buttonCompanyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo2.setEnabled(false);
                buttonContactInfo2.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_company));

                buttonContactInfo3.setEnabled(true);
                buttonContactInfo3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo4.setEnabled(true);
                buttonContactInfo4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo5.setEnabled(true);
                buttonContactInfo5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_white));

                String contactOrderId = null;
                if (!isNew) {
                    if (contactOrder.size() > 1) {
                        contactOrderId = contactOrder.get(1);
                    }
                }
                fragmentContactInfo1 = FragmentContactInfo2.newInstance(modelDetailContactInfo1, contactOrderId, companyOrder, true);
                saveForm();

                loadFragment(fragmentContactInfo1);
            }
        });

        buttonContactInfo3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContactInfo3.setError(null);

                buttonCompanyInfo.setEnabled(true);
                buttonCompanyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo2.setEnabled(true);
                buttonContactInfo2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo3.setEnabled(false);
                buttonContactInfo3.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_company));

                buttonContactInfo4.setEnabled(true);
                buttonContactInfo4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo5.setEnabled(true);
                buttonContactInfo5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_white));

                String contactOrderId = null;
                if (!isNew) {
                    if (contactOrder.size() > 2) {
                        contactOrderId = contactOrder.get(2);
                    }
                }
                fragmentContactInfo2 = FragmentContactInfo3.newInstance(modelDetailContactInfo2, contactOrderId, companyOrder, true);
                saveForm();

                loadFragment(fragmentContactInfo2);
            }
        });

        buttonContactInfo4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContactInfo4.setError(null);

                buttonCompanyInfo.setEnabled(true);
                buttonCompanyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo2.setEnabled(true);
                buttonContactInfo2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo3.setEnabled(true);
                buttonContactInfo3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo4.setEnabled(false);
                buttonContactInfo4.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_company));

                buttonContactInfo5.setEnabled(true);
                buttonContactInfo5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_white));

                String contactOrderId = null;
                if (!isNew) {
                    if (contactOrder.size() > 3) {
                        contactOrderId = contactOrder.get(3);
                    }
                }
                fragmentContactInfo3 = FragmentContactInfo4.newInstance(modelDetailContactInfo3, contactOrderId, companyOrder, true);

                saveForm();

                loadFragment(fragmentContactInfo3);
            }
        });

        buttonContactInfo5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContactInfo5.setError(null);

                buttonCompanyInfo.setEnabled(true);
                buttonCompanyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompanyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo2.setEnabled(true);
                buttonContactInfo2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo3.setEnabled(true);
                buttonContactInfo3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo4.setEnabled(true);
                buttonContactInfo4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContactInfo4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo5.setEnabled(false);
                buttonContactInfo5.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContactInfo5.setTextColor(getResources().getColor(R.color.color_company));

                String contactOrderId = null;
                if (!isNew) {
                    if (contactOrder.size() > 4) {
                        contactOrderId = contactOrder.get(4);
                    }
                }
                fragmentContactInfo4 = FragmentContactInfo5.newInstance(modelDetailContactInfo4, contactOrderId, companyOrder, true);

                saveForm();

                loadFragment(fragmentContactInfo4);
            }
        });

        actionButtonAddContact = findViewById(R.id.floating_button_add_contact);
        actionButtonAddContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (buttonContactInfo3.getVisibility() == View.GONE) {
                    buttonContactInfo3.setVisibility(View.VISIBLE);
                } else if (buttonContactInfo4.getVisibility() == View.GONE) {
                    buttonContactInfo4.setVisibility(View.VISIBLE);
                } else if (buttonContactInfo5.getVisibility() == View.GONE) {
                    buttonContactInfo5.setVisibility(View.VISIBLE);
                    actionButtonAddContact.setVisibility(View.GONE);
                }
            }
        });
    }

    private void saveForm() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (currentFragment.getClass() == fragmentCompanyInfo.getClass()) {
                    modelDetailCompanyMiniForm = fragmentCompanyInfo.getForm();

                } else if (currentFragment.getClass() == fragmentContactInfo.getClass()) {
                    modelDetailContactInfo = fragmentContactInfo.getForm();

                } else if (currentFragment.getClass() == fragmentContactInfo1.getClass()) {
                    modelDetailContactInfo1 = fragmentContactInfo1.getForm();

                } else if (currentFragment.getClass() == fragmentContactInfo2.getClass()) {
                    modelDetailContactInfo2 = fragmentContactInfo2.getForm();

                } else if (currentFragment.getClass() == fragmentContactInfo3.getClass()) {
                    modelDetailContactInfo3 = fragmentContactInfo3.getForm();

                } else if (currentFragment.getClass() == fragmentContactInfo4.getClass()) {
                    modelDetailContactInfo4 = fragmentContactInfo4.getForm();

                }
            }
        } else {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ActivityNewOrderCompany.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    saveForm();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    public void sendHqForm() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (modelDetailCompanyMiniForm.isModified()) {
                    modelDetailCompanyMiniForm = fragmentCompanyInfo.getComplete();
                }
                if (modelDetailContactInfo.isModified()) {
                    modelDetailContactInfo = fragmentContactInfo.getComplete();
                }
                if (modelDetailContactInfo1.isModified()) {
                    modelDetailContactInfo1 = fragmentContactInfo1.getComplete();
                }
                if (modelDetailContactInfo2.isModified()) {
                    modelDetailContactInfo2 = fragmentContactInfo2.getComplete();
                }
                if (modelDetailContactInfo3.isModified()) {
                    modelDetailContactInfo3 = fragmentContactInfo3.getComplete();
                }
                if (modelDetailContactInfo4.isModified()) {
                    modelDetailContactInfo4 = fragmentContactInfo4.getComplete();
                }

            }
        } else {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ActivityNewOrderCompany.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    sendHqForm();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        currentFragment = fragment;
        fragmentTransaction.replace(R.id.frame_company_order, fragment);
        fragmentTransaction.commit();
    }

    //    CALL API CONTACT ORDER ID
    private void getContactOrderId(final String companyOrderId) {
        progressDialog.show();
        if (InternetConnection.checkConnection(this)) {
            final Call<ResponseContactOrderId> contactOrderID = apiInterface.getContactOrderId(companyOrderId);
            contactOrderID.enqueue(new Callback<ResponseContactOrderId>() {
                @Override
                public void onResponse(Call<ResponseContactOrderId> call, Response<ResponseContactOrderId>
                        response) {
                    if (response.isSuccessful()) {
                        responseContactOrderId = response.body();
                        assert responseContactOrderId != null;
                        Log.d("Contact Order Id", String.valueOf(responseContactOrderId.getModelContactOrderId().getContactInfo()));
                        contactOrder = responseContactOrderId.getModelContactOrderId().getContactInfo();
                        for (int i = 0; i < contactOrder.size(); i++) {
                            switch (i) {
                                case 0:
                                    buttonContactInfo.setVisibility(View.VISIBLE);
                                    break;
                                case 1:
                                    buttonContactInfo2.setVisibility(View.VISIBLE);
                                    break;
                                case 2:
                                    buttonContactInfo3.setVisibility(View.VISIBLE);
                                    break;
                                case 3:
                                    buttonContactInfo4.setVisibility(View.VISIBLE);
                                    break;
                                case 4:
                                    buttonContactInfo5.setVisibility(View.VISIBLE);
                                    actionButtonAddContact.setVisibility(View.GONE);
                                    break;
                                default:
                                    break;
                            }
                        }
                        progressDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error;
                        try {
                            responseContactOrderId = gson.fromJson(response.errorBody().string(), ResponseContactOrderId.class);
                            error = responseContactOrderId.getMessage();

                            if (error.equals("Response Time Out")) {
                                AlertDialog.Builder b = new AlertDialog.Builder(ActivityNewOrderCompany.this);
                                b.setTitle(getString(R.string.content_question_internet_error));
                                b.setMessage(getString(R.string.content_information_cant_connect_server));
                                b.setCancelable(false);
                                b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getContactOrderId(companyOrderId);
                                    }
                                });
                                b.show();
                            } else
                                Snackbar.make(findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(ActivityNewOrderCompany.this);
                            b.setTitle(getString(R.string.content_question_internet_error));
                            b.setMessage(getString(R.string.content_information_cant_connect_server));
                            b.setCancelable(false);
                            b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getContactOrderId(companyOrderId);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        progressDialog.dismiss();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderCompany.this);
                        builder.setTitle(getString(R.string.content_question_internet_error));
                        builder.setMessage(getString(R.string.content_information_cant_connect_server));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getContactOrderId(companyOrderId);
                                Log.e("info", "OK");
                            }
                        });
                        builder.show();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseContactOrderId> call, Throwable t) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderCompany.this);
                    builder.setTitle(getString(R.string.content_question_internet_error));
                    builder.setMessage(getString(R.string.content_information_cant_connect_server));
                    builder.setCancelable(false);
                    builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getContactOrderId(companyOrderId);
                            Log.e("info", "OK");
                        }
                    });
                    builder.show();
                    progressDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderCompany.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getContactOrderId(companyOrderId);
                    Log.e("info", "OK");
                }
            });
            builder.show();
            progressDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderCompany.this);
        builder.setTitle(R.string.content_information_back_pressed);
        builder.setMessage(getString(R.string.content_question_want_save_data) +
                "\n" + getString(R.string.content_information_press_yes_save));
        builder.setNegativeButton(getString(R.string.dialog_no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int which) {
                        Log.e("info", "NO");
                        Intent i = new Intent(ActivityNewOrderCompany.this, MainActivityDrawer.class);
                        i.putExtra(Constans.KEY_MENU, menuCategory);
                        i.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                        startActivity(i);
                        finish();
                    }
                });
        builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Log.e("info", "YES");
                buttonSave.callOnClick();
            }
        });
        builder.show();
    }
}
