package com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey;

import com.example.a0426611017.carsurvey.Model.Survey.ModelSurvey;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0414831216 on 2/27/2018.
 */

public class ResponseSurveyFullFormCompany {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelSurvey modelSurvey;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelSurvey getModelSurvey() {
        return modelSurvey;
    }

    public void setModelSurvey(ModelSurvey modelSurvey) {
        this.modelSurvey = modelSurvey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
