package com.example.a0426611017.carsurvey.Personal.FullForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.a0426611017.carsurvey.AddNew.Personal.ActivityNewOrderPersonal;
import com.example.a0426611017.carsurvey.AddNew.Personal.ActivityNewOrderSG;
import com.example.a0426611017.carsurvey.Finance.ActivityCompanyFinancial;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataAddressPending;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataEmergencyContactPending;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataFinancialPending;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataJobDataPending;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataMainDataPending;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataPersonalSGPending;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiPersonal;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataAddress;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataEmergencyContact;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataFinancial;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataJobData;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataMainData;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonalSG;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDDLPersonal;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPersonalSGFullEdit extends AppCompatActivity implements View.OnClickListener{
    private Fragment currentFragment;

    private FragmentDataPersonalSGPending fragmentDataPersonalSGPending = new FragmentDataPersonalSGPending();
    private FragmentDataAddressPending fragmentDataAddressPending = new FragmentDataAddressPending();
    private FragmentDataMainDataPending fragmentDataMainDataPending = new FragmentDataMainDataPending();
    private FragmentDataJobDataPending fragmentDataJobDataPending = new FragmentDataJobDataPending();
    private FragmentDataEmergencyContactPending fragmentDataEmergencyContactPending = new FragmentDataEmergencyContactPending();
    private FragmentDataFinancialPending fragmentDataFinancialPending = new FragmentDataFinancialPending();

    private ModelDataPersonalSG modelDataPersonalSg = new ModelDataPersonalSG();
    private ModelDataAddress modelDataAddress = new ModelDataAddress();
    private ModelDataMainData modelDataMainData = new ModelDataMainData();
    private ModelDataJobData modelDataJobData = new ModelDataJobData();
    private ModelDataEmergencyContact modelDataEmergencyContact = new ModelDataEmergencyContact();
    private ModelDataFinancial modelDataFinancial = new ModelDataFinancial();

    private Button buttonSGInfo;
    private Button buttonContactInfo;
    private Button buttonOccupationInfo;
    private Button buttonEmergencyInfo;
    private Button buttonIncomeInfo;
    private Button buttonDataFinancial;
    private Button buttonNext;
    private Button buttonBack;
    private Button buttonSave;

    private boolean isNew = true;

    private ModelDDLPersonal ddlPersonalList;
    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private ApiPersonal apiPersonal;
    private String adminId = null;
    private String personalOrderId = null;
    private String name = "spouse";
    private String sharedLogId = null;
    private String personalSgCustDataId = null;
    private String kkSgIdNo = null;
    private String kkSgImgUrl = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_personal_sg_full_edit);
        window.setStatusBarColor(getResources().getColor(R.color.color_personal));
        final View currentView = this.findViewById(android.R.id.content);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading.........");
        loadingDialog.setCancelable(false);

        isNew = getIntent().getBooleanExtra(Constans.KEY_IS_NEW, true);

        apiPersonal = new ApiPersonal(currentView, ActivityPersonalSGFullEdit.this, loadingDialog);

        getDDLPersonal();

        adminId = this.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);
        personalOrderId = getIntent().getStringExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID);
        sharedLogId = this.getSharedPreferences(Constans.KeySharedPreference.LOG_ID, Context.MODE_PRIVATE).getString(Constans.KEY_LOG_ID, null);
        personalSgCustDataId = this.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, Context.MODE_PRIVATE).getString(Constans.KEY_SPOUSE_ID, null);
        kkSgIdNo = this.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_KK_ID, Context.MODE_PRIVATE).getString(Constans.KEY_SPOUSE_KK_ID, null);
        kkSgImgUrl = this.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_KK_IMAGE, Context.MODE_PRIVATE).getString(Constans.KEY_SPOUSE_KK_IMAGE, null);

        Log.d("Personal Order", "asd"+personalOrderId);
        Log.d("Admin Id", "asd"+adminId);
        Log.d("Log Id", "log id : "+sharedLogId);
        Log.d("KK ID SG", "return "+kkSgIdNo);
        Log.d("KK IMG SG", "return "+kkSgImgUrl);

        buttonSGInfo = findViewById(R.id.button_personal_sg_info);
        buttonSGInfo.setOnClickListener(this);

        buttonContactInfo = findViewById(R.id.button_contact_info);
        buttonContactInfo.setOnClickListener(this);

        buttonOccupationInfo = findViewById(R.id.button_occupation_info);
        buttonOccupationInfo.setOnClickListener(this);

        buttonIncomeInfo = findViewById(R.id.button_income_info);
        buttonIncomeInfo.setOnClickListener(this);

        buttonEmergencyInfo = findViewById(R.id.button_emergency_info);
        buttonEmergencyInfo.setOnClickListener(this);

        buttonDataFinancial = findViewById(R.id.button_data_financial);
        buttonDataFinancial.setOnClickListener(this);

        buttonSave = (Button) findViewById(R.id.button_save_personal);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPersonalSGFullEdit.this);
                builder.setTitle(getString(R.string.content_question_want_save_data));
                builder.setMessage(getString(R.string.content_information_save_and_exit));
                builder.setNegativeButton(getString(R.string.dialog_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                loadingDialog.dismiss();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        loadingDialog.show();
                        saveForm();

                        modelDataPersonalSg.setUsrcrt(adminId);
                        modelDataAddress.setUsrcrt(adminId);
                        modelDataMainData.setUsrcrt(adminId);
                        modelDataJobData.setUsrcrt(adminId);
                        modelDataFinancial.setUsrcrt(adminId);
                        modelDataEmergencyContact.setUsrcrt(adminId);

                        apiPersonal.saveFullFormPersonalSg(
                                modelDataPersonalSg,
                                modelDataAddress,
                                modelDataMainData,
                                modelDataJobData,
                                modelDataEmergencyContact,
                                modelDataFinancial,
                                personalOrderId,
                                name,
                                false,
                                personalSgCustDataId,
                                false,
                                sharedLogId);
                    }
                });

                builder.show();
            }
        });

        buttonNext = findViewById(R.id.button_next_personal_spouse);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                saveForm();
                checkCompleteSaveAndNext();

                if(modelDataPersonalSg.isComplete()
                        && modelDataAddress.isComplete()
                        && modelDataMainData.isComplete()
                        && modelDataJobData.isComplete()
                        && modelDataEmergencyContact.isComplete()
                        && modelDataFinancial.isComplete()
                        ){
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPersonalSGFullEdit.this);
                    builder.setTitle(getString(R.string.content_question_are_you_sure_to_save));
                    builder.setMessage(getString(R.string.content_information_save_and_next));
                    builder.setNegativeButton(getString(R.string.dialog_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    loadingDialog.dismiss();
                                    Log.e("info", "NO");
                                }
                            });
                    builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.e("info", "YES");

                            modelDataPersonalSg.setUsrcrt(adminId);
                            modelDataAddress.setUsrcrt(adminId);
                            modelDataMainData.setUsrcrt(adminId);
                            modelDataJobData.setUsrcrt(adminId);
                            modelDataEmergencyContact.setUsrcrt(adminId);
                            modelDataFinancial.setUsrcrt(adminId);

                            apiPersonal.saveFullFormPersonalSg(
                                    modelDataPersonalSg,
                                    modelDataAddress,
                                    modelDataMainData,
                                    modelDataJobData,
                                    modelDataEmergencyContact,
                                    modelDataFinancial,
                                    personalOrderId,
                                    name,
                                    true,
                                    personalSgCustDataId,
                                    true,
                                    sharedLogId);

                        }
                    });
                    builder.show();
                    loadingDialog.dismiss();
                } else {
                    if (!modelDataPersonalSg.isComplete()) {
                        if (buttonSGInfo.isEnabled())
                            buttonSGInfo.setError("Not Complete");
                    }
                    if (!modelDataAddress.isComplete()) {
                        if (buttonContactInfo.isEnabled())
                            buttonContactInfo.setError("Not Complete");
                    }
                    if (!modelDataMainData.isComplete()) {
                        if (buttonOccupationInfo.isEnabled())
                            buttonOccupationInfo.setError("Not Complete");
                    }
                    if (!modelDataJobData.isComplete()) {
                        if (buttonIncomeInfo.isEnabled())
                            buttonIncomeInfo.setError("Not Complete");
                    }
                    if (!modelDataEmergencyContact.isComplete()) {
                        if (buttonEmergencyInfo.isEnabled())
                            buttonEmergencyInfo.setError("Not Complete");
                    }
                    if (!modelDataFinancial.isComplete()) {
                        if (buttonDataFinancial.isEnabled())
                            buttonDataFinancial.setError("Not Complete");
                    }

                    Snackbar.make(findViewById(R.id.personal_sg_full_edit), "Please Fill The Data First",
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            }
        });

        buttonBack = findViewById(R.id.button_back_personal_spouse);
        buttonBack.setBackgroundColor(getResources().getColor(R.color.color_personal));
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(ActivityPersonalSGFullEdit.this);
                b.setTitle(R.string.content_information_back_pressed);
                b.setMessage(getString(R.string.content_question_want_save_data)+
                        "\n"+getString(R.string.content_information_press_yes_save));
                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ActivityPersonalSGFullEdit.this, ActivityPersonalFullEdit.class);
                        intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, personalOrderId);
                        intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);
                        startActivity(intent);
                        finish();
                    }
                });
                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        buttonSave.callOnClick();
                    }
                });
                b.show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_personal_sg_info:
                buttonSGInfo.setEnabled(false);
                buttonSGInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonSGInfo.setTextColor(getResources().getColor(R.color.color_personal));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setEnabled(true);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setEnabled(true);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setEnabled(true);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonDataFinancial.setEnabled(true);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_white));

                buttonSGInfo.setError(null);

                if(ddlPersonalList == null){
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityPersonalSGFullEdit.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLPersonal();
                            buttonSGInfo.callOnClick();
                        }
                    });
                    b.show();
                }else {

                    fragmentDataPersonalSGPending = fragmentDataPersonalSGPending.newInstance(modelDataPersonalSg, ddlPersonalList, personalOrderId, name);
                    saveForm();

                    loadFragment(fragmentDataPersonalSGPending);
                }
                break;

            case R.id.button_contact_info:
                buttonSGInfo.setEnabled(true);
                buttonSGInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonSGInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(false);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_personal));

                buttonOccupationInfo.setEnabled(true);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setEnabled(true);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setEnabled(true);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonDataFinancial.setEnabled(true);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setError(null);

                if(ddlPersonalList == null){
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityPersonalSGFullEdit.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLPersonal();
                            buttonContactInfo.callOnClick();
                        }
                    });
                    b.show();
                }else {

                    isNew = personalSgCustDataId.equals("0");

                    fragmentDataAddressPending = fragmentDataAddressPending.newInstance(modelDataAddress,
                            isNew, ddlPersonalList, personalSgCustDataId, name);
                    saveForm();

                    loadFragment(fragmentDataAddressPending);
                }
                break;

            case R.id.button_occupation_info:
                buttonSGInfo.setEnabled(true);
                buttonSGInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonSGInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setEnabled(false);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_personal));

                buttonIncomeInfo.setEnabled(true);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setEnabled(true);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonDataFinancial.setEnabled(true);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setError(null);

                if(ddlPersonalList == null){
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityPersonalSGFullEdit.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLPersonal();
                            buttonOccupationInfo.callOnClick();
                        }
                    });
                    b.show();
                }else {

                    isNew = personalSgCustDataId.equals("0");

                    fragmentDataMainDataPending = fragmentDataMainDataPending.newInstance(modelDataMainData,
                            isNew, ddlPersonalList, personalSgCustDataId, name,
                            kkSgIdNo, kkSgImgUrl);
                    saveForm();

                    loadFragment(fragmentDataMainDataPending);
                }
                break;

            case R.id.button_income_info:
                buttonSGInfo.setEnabled(true);
                buttonSGInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonSGInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setEnabled(true);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setEnabled(false);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_personal));

                buttonEmergencyInfo.setEnabled(true);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonDataFinancial.setEnabled(true);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setError(null);

                isNew = personalSgCustDataId.equals("0");

                fragmentDataJobDataPending = fragmentDataJobDataPending.newInstance(modelDataJobData, isNew, personalSgCustDataId, name);
                saveForm();

                loadFragment(fragmentDataJobDataPending);
                break;

            case R.id.button_emergency_info:
                buttonSGInfo.setEnabled(true);
                buttonSGInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonSGInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setEnabled(true);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setEnabled(true);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setEnabled(false);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_personal));

                buttonDataFinancial.setEnabled(true);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setError(null);

                isNew = personalSgCustDataId.equals("0");

                fragmentDataEmergencyContactPending = fragmentDataEmergencyContactPending.newInstance(modelDataEmergencyContact, isNew, personalSgCustDataId, name);
                saveForm();

                loadFragment(fragmentDataEmergencyContactPending);
                break;

            case R.id.button_data_financial:
                buttonSGInfo.setEnabled(true);
                buttonSGInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonSGInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setEnabled(true);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setEnabled(true);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setEnabled(true);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonDataFinancial.setEnabled(false);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_personal));

                buttonDataFinancial.setError(null);

                isNew = personalSgCustDataId.equals("0");

                fragmentDataFinancialPending = fragmentDataFinancialPending.newInstance(modelDataFinancial, isNew, personalSgCustDataId, name);
                saveForm();

                loadFragment(fragmentDataFinancialPending);
                break;
        }
    }

    private void saveForm(){
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (currentFragment.getClass() == fragmentDataPersonalSGPending.getClass()){
                    modelDataPersonalSg = fragmentDataPersonalSGPending.getForm();
                } else if (currentFragment.getClass() == fragmentDataAddressPending.getClass()){
                    modelDataAddress = fragmentDataAddressPending.getForm();
                } else if (currentFragment.getClass() == fragmentDataMainDataPending.getClass()){
                    modelDataMainData = fragmentDataMainDataPending.getForm();
                } else if (currentFragment.getClass() == fragmentDataJobDataPending.getClass()){
                    modelDataJobData = fragmentDataJobDataPending.getForm();
                }else if (currentFragment.getClass() == fragmentDataEmergencyContactPending.getClass()){
                    modelDataEmergencyContact = fragmentDataEmergencyContactPending.getForm();
                } else if (currentFragment.getClass() == fragmentDataFinancialPending.getClass()){
                    modelDataFinancial = fragmentDataFinancialPending.getForm();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPersonalSGFullEdit.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    checkCompleteSaveAndNext();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    public void checkCompleteSaveAndNext() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (modelDataPersonalSg.isModified()) {
                    modelDataPersonalSg = fragmentDataPersonalSGPending.getComplete();
                }
                if (modelDataAddress.isModified()){
                    modelDataAddress = fragmentDataAddressPending.getComplete();
                }
                if (modelDataMainData.isModified()){
                    modelDataMainData = fragmentDataMainDataPending.getComplete();
                }
                if (modelDataJobData.isModified()){
                    modelDataJobData = fragmentDataJobDataPending.getComplete();
                }
                if (modelDataEmergencyContact.isModified()){
                    modelDataEmergencyContact = fragmentDataEmergencyContactPending.getComplete();
                }
                if (modelDataFinancial.isModified()){
                    modelDataFinancial = fragmentDataFinancialPending.getComplete();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPersonalSGFullEdit.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    checkCompleteSaveAndNext();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    private void loadFragment(Fragment fragment) {
        currentFragment = fragment;
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.frame_personal_pending_sg, fragment);
        fragmentTransaction.commit();
    }

    private void setResponseDDLPersonal(ModelDDLPersonal ddlPersonalList){
        this.ddlPersonalList = ddlPersonalList;
    }

    public void getDDLPersonal(){
        loadingDialog.show();
        if (InternetConnection.checkConnection(this)) {
            Call<ResponseDDLPersonal> ddlCompany = apiInterface.getDDLPersonal();
            ddlCompany.enqueue(new Callback<ResponseDDLPersonal>() {
                @Override
                public void onResponse(Call<ResponseDDLPersonal> call, Response<ResponseDDLPersonal>
                        response) {
                    if (response.isSuccessful()) {
                        setResponseDDLPersonal(response.body().getDdlPersonal());
                        loadingDialog.dismiss();
                    } else {
                        loadingDialog.dismiss();
                        Snackbar.make(findViewById(R.id.personal_sg_full_edit), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDDLPersonal> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    Snackbar.make(findViewById(R.id.personal_sg_full_edit), t.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(R.id.personal_sg_full_edit), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            loadingDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder b = new AlertDialog.Builder(ActivityPersonalSGFullEdit.this);
        b.setTitle(R.string.content_information_back_pressed);
        b.setMessage(getString(R.string.content_question_want_save_data)+
                "\n"+getString(R.string.content_information_press_yes_save));
        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ActivityPersonalSGFullEdit.this, ActivityPersonalFullEdit.class);
                intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, personalOrderId);
                intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);
                startActivity(intent);
                finish();
            }
        });
        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                buttonSave.callOnClick();
            }
        });
        b.show();
    }
}
