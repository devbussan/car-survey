package com.example.a0426611017.carsurvey.Fragment.Financial;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Financial.ModelFinance;
import com.example.a0426611017.carsurvey.Object.Financial.ObjectFinancial;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.Model.Financial.ModelDDLFinance;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Finance.ResponseDetailFinance;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentFinancial extends Fragment {
    private int menu;
    private View view;
    private Context context;
    private ModelFinance modelFinance;
    private ObjectFinancial objectFinancial;
    private ModelDDLFinance modelDDLFinance;

    private String name = "";
    private String orderId = "0";

    private ProgressDialog progressDialog;
    private ApiInterface apiInterface;

    private Gson gson = new Gson();

    private ResponseDetailFinance responseDetailFinance = new ResponseDetailFinance();

    public static FragmentFinancial newInstance(int menu, ModelFinance modelFinance , ModelDDLFinance modelDDLFinance, String name, String orderId) {
        FragmentFinancial fragment = new FragmentFinancial();
        Bundle args = new Bundle();
        args.putInt(Constans.KEY_MENU,menu);
        args.putSerializable("modelDDLFinance",modelDDLFinance);
        args.putSerializable("modelFinance",modelFinance);
        args.putString("name", name);
        args.putString("orderId", orderId);
        fragment.setArguments(args);
        return fragment;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            menu = getArguments().getInt(Constans.KEY_MENU);
            modelFinance = (ModelFinance) getArguments().getSerializable("modelFinance");
            modelDDLFinance = (ModelDDLFinance) getArguments().getSerializable("modelDDLFinance");
            name = getArguments().getString("name");
            orderId = getArguments().getString("orderId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectFinancial = new ObjectFinancial(container, inflater,modelDDLFinance);
        view = objectFinancial.getView();
        context = container.getContext();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading......");
        progressDialog.setCancelable(false);

        if(menu == Constans.MENU_PERSONAL){
            objectFinancial.setFormForPersonal();
        }else{
            objectFinancial.setFormForCompany();

        }

      if(modelFinance.isModified()){
          objectFinancial.setForm(modelFinance);
      }else{
          getDetailFinance(name, orderId);
      }
        return view;
    }

    public ModelFinance getForm(){
        return objectFinancial.getForm();
    }

    public ModelFinance getComplete(){
        return objectFinancial.getComplete();
    };

    public void setForm(ModelFinance modelFinance){
        objectFinancial.setForm(modelFinance);
    }

    private void getDetailFinance(final String name, final String orderid){
        progressDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailFinance> detailfinance = apiInterface.getDetailFinancial(name, orderid);
            detailfinance.enqueue(new Callback<ResponseDetailFinance>() {
                @Override
                public void onResponse(Call<ResponseDetailFinance> call, Response<ResponseDetailFinance>
                        response) {
                    if (response.isSuccessful()) {
                        modelFinance = response.body().getModelFinance();
                        modelFinance.setModified(true);
                        setForm(modelFinance);
                        progressDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailFinance = gson.fromJson(response.errorBody().string(), ResponseDetailFinance.class);
                            error = responseDetailFinance.getMessage();

                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailFinance(name, orderid);
                                    }
                                });
                                b.show();
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.layout_finance), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailFinance(name, orderid);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        progressDialog.dismiss();
                    } else {
                        Log.e("Other Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailFinance(name, orderid);
                            }
                        });
                        b.show();
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailFinance> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailFinance(name, orderid);
                        }
                    });
                    b.show();
                    progressDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailFinance(name, orderid);
                }
            });
            b.show();
            progressDialog.dismiss();
        }
    }

}
