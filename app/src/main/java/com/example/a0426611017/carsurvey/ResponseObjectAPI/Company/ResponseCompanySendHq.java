package com.example.a0426611017.carsurvey.ResponseObjectAPI.Company;

import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelMiniFormCompany;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0414831216 on 2/23/2018.
 */

public class ResponseCompanySendHq {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelMiniFormCompany modelMiniFormCompany;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelMiniFormCompany getModelMiniFormCompany() {
        return modelMiniFormCompany;
    }

    public void setModelMiniFormCompany(ModelMiniFormCompany modelMiniFormCompany) {
        this.modelMiniFormCompany = modelMiniFormCompany;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
