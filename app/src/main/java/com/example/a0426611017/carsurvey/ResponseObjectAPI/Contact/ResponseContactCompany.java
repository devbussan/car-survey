package com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact;

import com.example.a0426611017.carsurvey.Model.Company.ModelContactCompany;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0414831216 on 2/19/2018.
 */

public class ResponseContactCompany{
        @SerializedName("status")
        String status;

        @SerializedName("result")
        ModelContactCompany modelContactCompany;

        @SerializedName("message")
        String message;

        public ModelContactCompany getModelContactCompany() {
                return modelContactCompany;
        }

        public void setModelContactCompany(ModelContactCompany modelContactCompany) {
                this.modelContactCompany = modelContactCompany;
        }

        public String getStatus() {
                return status;
        }

        public void setStatus(String status) {
                this.status = status;
        }

        public String getMessage() {
                return message;
        }

        public void setMessage(String message) {
                this.message = message;
        }
}
