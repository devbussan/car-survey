package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 08/02/2018.
 */

public class ResponseDDLPersonal {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDDLPersonal ddlPersonal;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDDLPersonal getDdlPersonal() {
        return ddlPersonal;
    }

    public void setDdlPersonal(ModelDDLPersonal ddlPersonal) {
        this.ddlPersonal = ddlPersonal;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
