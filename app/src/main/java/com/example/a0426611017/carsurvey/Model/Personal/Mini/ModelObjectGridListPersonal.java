package com.example.a0426611017.carsurvey.Model.Personal.Mini;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426611017 on 2/23/2018.
 */

public class ModelObjectGridListPersonal {

    @SerializedName("LISTPERSONAL")
    private ModelGridListPersonal [] modelGridListPersonal;

    public ModelGridListPersonal[] getModelGridListPersonal() { return modelGridListPersonal; }

    public void setModelGridListPersonal(ModelGridListPersonal[] modelGridListPersonal) { this.modelGridListPersonal = modelGridListPersonal; }
}
