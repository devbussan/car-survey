package com.example.a0426611017.carsurvey.MainFunction.CallApi;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.GridView;

import com.example.a0426611017.carsurvey.AddNew.Personal.ActivityNewOrderSG;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalInfoMini;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalSGMini;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelRequestPersonalSendHq;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseMiniFormPersonal;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseMiniFormPersonalSg;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponsePersonalSendHq;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by 0426611017 on 2/20/2018.
 */

public class ApiPersonalOrder {

    private ApiInterface apiInterface;
    private View view;
    private Context context;
    private ProgressDialog loadingDialog;

    private int tab = 0;
    private int menu = 0;
    private GridView gridView;
    private String search = "";
    private boolean isSuccess = true;

    private SharedPreferences sharedpreferencesMenuCategory;
    private int menuCategory = 0;
    private boolean isSubmitHq = false;
    private boolean isNew = true;
    private String sharedLogId = null;
    private String adminId = null;
    private String personalId = null;
    private Gson gson = new Gson();

    private ModelPersonalInfoMini modelPersonalInfoMini;
    private ModelPersonalSGMini modelPersonalSGMini;

    private ResponseMiniFormPersonal responseMiniFormPersonal;
    private ResponseMiniFormPersonalSg responseMiniFormPersonalSg;
    private ResponsePersonalSendHq responsePersonalSendHq;

    public ApiPersonalOrder(View view, Context context, ProgressDialog loadingDialog) {
        this.view = view;
        this.context = context;
        this.loadingDialog = loadingDialog;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sharedpreferencesMenuCategory = this.context.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, MODE_PRIVATE);
        menuCategory = sharedpreferencesMenuCategory.getInt(Constans.KEY_MENU_CATEGORY, 0);
        sharedLogId = this.context.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, MODE_PRIVATE).getString(Constans.KEY_LOG_ID, null);
    }

    private void submitHq(final ModelRequestPersonalSendHq modelRequestPersonalSendHq, String idLog, final String idPersonal) {

        Log.d("ID LOG", "return : " + idLog);
        Log.d("ID PERSONAL", "return : " + idPersonal);

        if (InternetConnection.checkConnection(context)) {
            Call<ResponsePersonalSendHq> sendHq = apiInterface.submitHq(modelRequestPersonalSendHq, idLog, idPersonal);
            sendHq.enqueue(new Callback<ResponsePersonalSendHq>() {
                @Override
                public void onResponse(Call<ResponsePersonalSendHq> call, Response<ResponsePersonalSendHq> response) {
                    if (response.isSuccessful()) {
                        responsePersonalSendHq = response.body();
                        Log.d("Response", responsePersonalSendHq.getModelRequestPersonalSendHq().getLog_personal());
                        Snackbar.make(view.findViewById(R.id.fragment_personal_sg_id), "submit HQ", Snackbar.LENGTH_LONG).show();


                        Intent i = new Intent(context, MainActivityDrawer.class);
                        i.putExtra(Constans.KEY_MENU, menuCategory);
                        i.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                        context.startActivity(i);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responsePersonalSendHq = gson.fromJson(response.errorBody().string(), ResponsePersonalSendHq.class);
                            error = responsePersonalSendHq.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Send to HQ \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    submitHq(modelRequestPersonalSendHq, sharedLogId, idPersonal);

                                }
                            });
                            b.show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Send to HQ \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    submitHq(modelRequestPersonalSendHq, sharedLogId, idPersonal);

                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Send to HQ \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                submitHq(modelRequestPersonalSendHq, sharedLogId, idPersonal);

                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponsePersonalSendHq> call, Throwable throwable) {
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Dialog");
                    b.setCancelable(false);
                    b.setMessage("Failed Send to HQ \n" +
                            "Please try Again");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            submitHq(modelRequestPersonalSendHq, sharedLogId, idPersonal);

                        }
                    });
                    b.show();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Dialog");
            b.setCancelable(false);
            b.setMessage("Failed Send to HQ \n" +
                    "Please try Again");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    submitHq(modelRequestPersonalSendHq, sharedLogId, idPersonal);

                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }

    public void saveUpdatePersonalOrder(
            ModelPersonalInfoMini modelPersonalInfoMini,
            String orderId,
            String logId) {
        this.modelPersonalInfoMini = modelPersonalInfoMini;
        this.adminId = modelPersonalInfoMini.getUsrcrt();
        this.sharedLogId = logId;

        try {
            saveUpdatePersonal(modelPersonalInfoMini, orderId);
        } catch (IOException e) {
            loadingDialog.dismiss();
            Log.e("Error Address", e.getMessage());
            Snackbar.make(view.findViewById(R.id.personal_new_order), "Info : Error upload image",
                    Snackbar.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void saveUpdatePersonal(final ModelPersonalInfoMini modelPersonalInfoMini, final String orderId) throws IOException {
        loadingDialog.show();

        Log.d("Request", gson.toJson(modelPersonalInfoMini));
        Log.d("Request", "order id : " + orderId);

        if (!modelPersonalInfoMini.getIdimgurl().startsWith("http")
                && !modelPersonalInfoMini.getIdimg().equals("")
                && !modelPersonalInfoMini.getIdimg().equals("No selected file")) {
            FileFunction fileFunction = new FileFunction();
            modelPersonalInfoMini.setIdimgurl(fileFunction.createBase64(modelPersonalInfoMini.getIdimg()));
        }

        if (!modelPersonalInfoMini.getKkimgurl().startsWith("http")
                && !modelPersonalInfoMini.getKkimg().equals("")
                && !modelPersonalInfoMini.getKkimg().equals("No selected file")) {
            FileFunction fileFunction = new FileFunction();
            modelPersonalInfoMini.setKkimgurl(fileFunction.createBase64(modelPersonalInfoMini.getKkimg()));
        }

        if (InternetConnection.checkConnection(context)) {
            Call<ResponseMiniFormPersonal> callPersonal;
            if (orderId == null) {
                callPersonal = apiInterface.saveMiniFormPersonal(modelPersonalInfoMini);
            } else {
                callPersonal = apiInterface.updateMiniFormPersonal(modelPersonalInfoMini, orderId);
            }

            callPersonal.enqueue(new Callback<ResponseMiniFormPersonal>() {
                @Override
                public void onResponse(Call<ResponseMiniFormPersonal> call, Response<ResponseMiniFormPersonal> response) {
                    if (response.isSuccessful()) {
                        responseMiniFormPersonal = response.body();
                        if (responseMiniFormPersonal.getStatus().equals("success")) {
                            String personalId = responseMiniFormPersonal.getModelPersonalInfoMini().getPersonal_order_id_return();
                            String logPersonalId = responseMiniFormPersonal.getModelPersonalInfoMini().getLog_data_id_return();

                            Log.d("Personal ID : ", personalId);

                            Intent intent = new Intent(context, ActivityNewOrderSG.class);
                            intent.putExtra(Constans.KEY_MENU, Constans.MENU_PERSONAL);
                            if (orderId == null) {
                                loadingDialog.dismiss();
                                Log.d("Log ID : ", "Log Personal Id" + logPersonalId);
                                intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, personalId);
                                intent.putExtra(Constans.KeySharedPreference.LOG_ID, logPersonalId);
                                intent.putExtra(Constans.KEY_IS_NEW, true);
                            } else {
                                loadingDialog.dismiss();
                                Log.d("Log ID : ", "Log Personal Id" + sharedLogId);
                                intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, orderId);
                                intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);
                                intent.putExtra(Constans.KEY_IS_NEW, false);
                            }

                            context.startActivity(intent);
                        } else {
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveUpdatePersonal(modelPersonalInfoMini, orderId);
                                    } catch (IOException e) {
                                        loadingDialog.dismiss();
                                        Log.e("Error Address", e.getMessage());
                                        Snackbar.make(view.findViewById(R.id.personal_new_order), "Info : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                        e.printStackTrace();
                                    }
                                }
                            });
                            b.show();
                        }

                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseMiniFormPersonal = gson.fromJson(response.errorBody().string(), ResponseMiniFormPersonal.class);
                            error = responseMiniFormPersonal.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveUpdatePersonal(modelPersonalInfoMini, orderId);
                                    } catch (IOException e) {
                                        loadingDialog.dismiss();
                                        Log.e("Error Address", e.getMessage());
                                        Snackbar.make(view.findViewById(R.id.personal_new_order), "Info : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                        e.printStackTrace();
                                    }
                                }
                            });
                            b.show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveUpdatePersonal(modelPersonalInfoMini, orderId);
                                    } catch (IOException e) {
                                        loadingDialog.dismiss();
                                        Log.e("Error Address", e.getMessage());
                                        Snackbar.make(view.findViewById(R.id.personal_new_order), "Info : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                        e.printStackTrace();
                                    }
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saved \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    saveUpdatePersonal(modelPersonalInfoMini, orderId);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address", e.getMessage());
                                    Snackbar.make(view.findViewById(R.id.personal_new_order), "Info : Error upload image",
                                            Snackbar.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseMiniFormPersonal> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Dialog");
                    b.setCancelable(false);
                    b.setMessage("Failed Saved \n" +
                            "Please try Again");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                saveUpdatePersonal(modelPersonalInfoMini, orderId);
                            } catch (IOException e) {
                                loadingDialog.dismiss();
                                Log.e("Error Address", e.getMessage());
                                Snackbar.make(view.findViewById(R.id.personal_new_order), "Info : Error upload image",
                                        Snackbar.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(context.getString(R.string.content_question_internet_error));
            b.setMessage(context.getString(R.string.content_information_cant_connect_server_save));
            b.setCancelable(false);
            b.setPositiveButton(context.getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        saveUpdatePersonal(modelPersonalInfoMini, orderId);
                    } catch (IOException e) {
                        loadingDialog.dismiss();
                        Log.e("Error Address", e.getMessage());
                        Snackbar.make(view.findViewById(R.id.personal_new_order), "Info : Error upload image",
                                Snackbar.LENGTH_LONG).show();
                        e.printStackTrace();
                    }
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }

    public void saveUpdatePersonalSgOrder(
            ModelPersonalSGMini modelPersonalSGMini,
            String orderId,
            String logId,
            boolean hqSubmit,
            boolean isNew) {
        this.modelPersonalSGMini = modelPersonalSGMini;
        this.adminId = modelPersonalSGMini.getUsrcrt();
        this.sharedLogId = logId;
        this.isSubmitHq = hqSubmit;
        this.isNew = isNew;

        try {
            saveUpdatePersonalSg(modelPersonalSGMini, orderId, isNew);
        } catch (IOException e) {
            loadingDialog.dismiss();
            Log.e("Error Address", e.getMessage());
            Snackbar.make(view.findViewById(R.id.myLayoutPersonalSg), "Info : Error upload image",
                    Snackbar.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void saveUpdatePersonalSg(final ModelPersonalSGMini modelPersonalSGMini, final String orderId, final boolean isNew) throws IOException {
        loadingDialog.show();

        Log.d("Request", gson.toJson(modelPersonalInfoMini));
        Log.d("Request", "order id : " + orderId);

        Log.d("pesan", gson.toJson(modelPersonalSGMini));

        if (!modelPersonalSGMini.getIdimgurlsg().startsWith("http")
                && !modelPersonalSGMini.getIdimgsg().equals("")
                && !modelPersonalSGMini.getIdimgsg().equals("No selected file")) {
            FileFunction fileFunction = new FileFunction();
            modelPersonalSGMini.setIdimgurlsg(fileFunction.createBase64(modelPersonalSGMini.getIdimgsg()));
        }

        if (!modelPersonalSGMini.getKkimgurlsg().startsWith("http")
                && !modelPersonalSGMini.getKkimgsg().equals("")
                && !modelPersonalSGMini.getKkimgsg().equals("No selected file")) {
            FileFunction fileFunction = new FileFunction();
            modelPersonalSGMini.setKkimgurlsg(fileFunction.createBase64(modelPersonalSGMini.getKkimgsg()));
        }

        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseMiniFormPersonalSg> ddlPersonal;
            if (isNew) {
                ddlPersonal = apiInterface.saveMiniFormPersonalSg(modelPersonalSGMini);
            } else {
                ddlPersonal = apiInterface.updateMiniFormPersonalSg(modelPersonalSGMini, orderId);
            }
            ddlPersonal.enqueue(new Callback<ResponseMiniFormPersonalSg>() {
                @Override
                public void onResponse(Call<ResponseMiniFormPersonalSg> call, Response<ResponseMiniFormPersonalSg> response) {
                    if (response.isSuccessful()) {
                        responseMiniFormPersonalSg = response.body();
                        if (responseMiniFormPersonalSg.getStatus().equals("success")) {
                            String personalId = responseMiniFormPersonalSg.getModelPersonalSGMini().getPersonal_spouse_order_rtn_id();
                            Log.d("Personal : ", "Return " + personalId);

                            Log.d("IS SUBMIT HQ : ", "Return " + isSubmitHq);
                            Log.d("LOG ID : ", "Return " + sharedLogId);
                            Log.d("ADMIN ID : ", "Return " + adminId);


                            if (isSubmitHq) {
                                ModelRequestPersonalSendHq modelRequestPersonalSendHq = new ModelRequestPersonalSendHq();
                                modelRequestPersonalSendHq.setUsrPd(adminId);
                                modelRequestPersonalSendHq.setStatus("1");
                                modelRequestPersonalSendHq.setIsReadCmo("0");
                                submitHq(modelRequestPersonalSendHq, sharedLogId, orderId);

                            } else {
                                loadingDialog.dismiss();
                                Intent i = new Intent(context, MainActivityDrawer.class);
                                i.putExtra(Constans.KEY_MENU, menuCategory);
                                i.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                                context.startActivity(i);
                            }
                        } else {
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveUpdatePersonalSg(modelPersonalSGMini, orderId, true);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            b.show();
                        }

                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseMiniFormPersonalSg = gson.fromJson(response.errorBody().string(), ResponseMiniFormPersonalSg.class);
                            error = responseMiniFormPersonalSg.getMessage();
                            if (orderId != null && responseMiniFormPersonalSg.getMessage().equals("Data Not Found")) {
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveUpdatePersonalSg(modelPersonalSGMini, orderId, true);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();

                            }

                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveUpdatePersonalSg(modelPersonalSGMini, orderId, true);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saved \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    saveUpdatePersonalSg(modelPersonalSGMini, orderId, true);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        b.show();
                    }
                    loadingDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseMiniFormPersonalSg> call, Throwable t) {

                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Dialog");
                    b.setCancelable(false);
                    b.setMessage("Failed Saved \n" +
                            "Please try Again");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            try {
                                saveUpdatePersonalSg(modelPersonalSGMini, orderId, true);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Dialog");
            b.setCancelable(false);
            b.setMessage("Failed Saved \n" +
                    "Please try Again");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        saveUpdatePersonalSg(modelPersonalSGMini, orderId, true);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }
}



