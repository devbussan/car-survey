package com.example.a0426611017.carsurvey.Fragment.Company;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiCompany;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.R;


/**
 * Created by c201017001 on 25/01/2018.
 */

public class FragmentCompanyOrderForm extends Fragment{

    private View view;
    private Context context;
    private String search;
    private int tab;
    private int menu;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ApiCompany apiCompany;
    private ProgressDialog progressDialog;

    private String adminId;

    public FragmentCompanyOrderForm newInstance(String search, int tab, int menu) {
        FragmentCompanyOrderForm companyTab = new FragmentCompanyOrderForm();
        Bundle args = new Bundle();
        args.putString("search", search);
        args.putInt("tab", tab);
        args.putInt("menu", menu);
        companyTab.setArguments(args);
        return companyTab;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_company_complete_grid, container, false);
        context = container.getContext();
        search = getArguments().getString("search");
        tab = getArguments().getInt("tab");
        menu = getArguments().getInt("menu");

        adminId = context.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, Context.MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);
        progressDialog.show();

        apiCompany = new ApiCompany(view, context, progressDialog);

        mSwipeRefreshLayout = view.findViewById(R.id.swipeToRefreshCompanyComplete);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                searchItem("");
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        searchItem(search);


        return view;
    }

//    Retrofit
    private void searchItem(String search){

        GridView gridView = view.findViewById(R.id.gridView);
        TextView textViewError = view.findViewById(R.id.text_view_info_error);
        gridView.setAdapter(null);

        switch (tab){
            case Constans.TAB_DRAFT:
                Log.d("Info", "TAB DRAFT");
                if(apiCompany.getListCompany(adminId, String.valueOf(Constans.STATUS_DRAFT), tab, menu, search, gridView, textViewError, false)){
                    Log.d("Info", "Success Get List");
                }else{
                    Log.d("Info", "Pokoknya ada yang error");
                }

                break;
            case Constans.TAB_PENDING:
                Log.d("Info", "TAB PENDING");
                if(apiCompany.getListCompany(adminId, String.valueOf(Constans.STATUS_PENDING), tab, menu, search, gridView, textViewError, false)){
                    Log.d("Info", "Success Get List");
                }else{
                    Log.d("Info", "Pokoknya ada yang error");
                }

                break;
            case Constans.TAB_RETURN_ORDER:

                Log.d("Info", "TAB RETURN ORDER");
                if(apiCompany.getListCompany(adminId, String.valueOf(Constans.STATUS_RETURN_ORDER), tab, menu, search, gridView, textViewError, false)){
                    Log.d("Info", "Success Get List");
                }else{
                    Log.d("Info", "Pokoknya ada yang error");
                }
                break;
            case Constans.TAB_REJECT_ORDER:
                Log.d("Info", "TAB REJECT ORDER");
                if(apiCompany.getListCompany(adminId, String.valueOf(Constans.STATUS_REJECT_ORDER), tab, menu, search, gridView, textViewError, false)){
                    Log.d("Info", "Success Get List");
                }else{
                    Log.d("Info", "Pokoknya ada yang error");
                }

                break;
            case Constans.TAB_CLEAR:
                Log.d("Info", "TAB CLEAR");
                if(apiCompany.getListCompany(adminId, String.valueOf(Constans.STATUS_CLEAR), tab, menu, search, gridView, textViewError, false)){
                    Log.d("Info", "Success Get List");
                }else{
                    Log.d("Info", "Pokoknya ada yang error");
                }

                break;
            default:
                break;
        }

    }

//    Volley
//    private GridVolley gridVolley;
//    private void searchItem(String search){
//
//        gridVolley = new GridVolley(view, context, progressDialog);
//        GridView gridView;
//
//        switch (tab){
//            case Constans.TAB_DRAFT:
//                Log.d("Info", "TAB DRAFT");
//                gridView = view.findViewById(R.id.gridView);
//                if(gridVolley.getListCompany(adminId, String.valueOf(Constans.STATUS_DRAFT), tab, menu, search, gridView)){
//                    Log.d("Info", "Success Get List");
//                }else{
//                    Log.d("Info", "Pokoknya ada yang error");
//                }
//
//                break;
//            case Constans.TAB_PENDING:
//                Log.d("Info", "TAB PENDING");
//                gridView = view.findViewById(R.id.gridView);
//                if(gridVolley.getListCompany(adminId, String.valueOf(Constans.STATUS_PENDING), tab, menu, search, gridView)){
//                    Log.d("Info", "Success Get List");
//                }else{
//                    Log.d("Info", "Pokoknya ada yang error");
//                }
//
//                break;
//            case Constans.TAB_RETURN_ORDER:
//
//                Log.d("Info", "TAB RETURN ORDER");
//                gridView = view.findViewById(R.id.gridView);
//                if(gridVolley.getListCompany(adminId, String.valueOf(Constans.STATUS_RETURN_ORDER), tab, menu, search, gridView)){
//                    Log.d("Info", "Success Get List");
//                }else{
//                    Log.d("Info", "Pokoknya ada yang error");
//                }
//                break;
//            case Constans.TAB_REJECT_ORDER:
//                Log.d("Info", "TAB REJECT ORDER");
//                gridView = view.findViewById(R.id.gridView);
//                if(gridVolley.getListCompany(adminId, String.valueOf(Constans.STATUS_REJECT_ORDER), tab, menu, search, gridView)){
//                    Log.d("Info", "Success Get List");
//                }else{
//                    Log.d("Info", "Pokoknya ada yang error");
//                }
//
//                break;
//            case Constans.TAB_CLEAR:
//                Log.d("Info", "TAB CLEAR");
//                gridView = view.findViewById(R.id.gridView);
//                if(gridVolley.getListCompany(adminId, String.valueOf(Constans.STATUS_CLEAR), tab, menu, search, gridView)){
//                    Log.d("Info", "Success Get List");
//                }else{
//                    Log.d("Info", "Pokoknya ada yang error");
//                }
//
//                break;
//            default:
//                break;
//        }
//
//    }


}
