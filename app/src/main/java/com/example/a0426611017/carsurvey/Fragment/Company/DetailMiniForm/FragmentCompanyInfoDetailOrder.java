package com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Object.Company.DetailMiniForm.ObjectCompanyInfoDetailOrder;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelDetailCompanyMiniForm;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailCompanyMiniForm;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCompanyInfoDetailOrder extends Fragment {
    private View view;
    private ObjectCompanyInfoDetailOrder objectCompanyInfoDetailOrder;
    private static final String ARG_PARAM2 = "param2";


    private Context context;
    private String companyOrderId = "0";

    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private Gson gson = new Gson();

    private ModelDetailCompanyMiniForm modelDetailCompanyMiniForm;
    private ResponseDetailCompanyMiniForm responseDetailCompanyMiniForm = new ResponseDetailCompanyMiniForm();


    public FragmentCompanyInfoDetailOrder newInstance(String companyOrder) {
        FragmentCompanyInfoDetailOrder fragment = new FragmentCompanyInfoDetailOrder();
        Bundle args = new Bundle();

        args.putString(ARG_PARAM2, companyOrder);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if(getArguments() !=null ){
            companyOrderId = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectCompanyInfoDetailOrder = new ObjectCompanyInfoDetailOrder(container, inflater);
        view = objectCompanyInfoDetailOrder.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);
        getDetailCompanyOrder(companyOrderId);

        return view;
    }

    private void getDetailCompanyOrder(final String companyOrderId){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailCompanyMiniForm> contactOrderID = apiInterface.getDetailCompanyMiniForm(companyOrderId);
            contactOrderID.enqueue(new Callback<ResponseDetailCompanyMiniForm>() {
                @Override
                public void onResponse(Call<ResponseDetailCompanyMiniForm> call, Response<ResponseDetailCompanyMiniForm>
                        response) {
                    if (response.isSuccessful()) {
                        modelDetailCompanyMiniForm = response.body().getModelDetailCompanyMiniForm();
                        setForm(modelDetailCompanyMiniForm);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailCompanyMiniForm = gson.fromJson(response.errorBody().string(), ResponseDetailCompanyMiniForm.class);
                            error = responseDetailCompanyMiniForm.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(getString(R.string.dialog_error_server));
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        getString(R.string.dialog_try_again));
                                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailCompanyOrder(companyOrderId);
                                    }
                                });

                            }else if(!error.equals("Data Not Found")) {
                                Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }


                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(getString(R.string.dialog_error_server));
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    getString(R.string.dialog_try_again));
                            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailCompanyOrder(companyOrderId);
                                }
                            });

                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(getString(R.string.dialog_error_server));
                        b.setCancelable(false);
                        b.setMessage( getString(R.string.dialog_response_time_out) +"\n" +
                                getString(R.string.dialog_try_again));
                        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailCompanyOrder(companyOrderId);
                            }
                        });
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailCompanyMiniForm> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle(getString(R.string.dialog_error_server));
                    b.setCancelable(false);
                    b.setMessage( t.toString());
                    b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailCompanyOrder(companyOrderId);
                        }
                    });
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(getString(R.string.dialog_error_server));
            b.setCancelable(false);
            b.setMessage(getString(R.string.dialog_internet_connect)+"\n"+getString(R.string.dialog_try_again));
            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailCompanyOrder(companyOrderId);
                }
            });
            loadingDialog.dismiss();
        }
    }

    public void setForm(ModelDetailCompanyMiniForm companyMiniForm){
        objectCompanyInfoDetailOrder.setForm(companyMiniForm);
    }


}
