package com.example.a0426611017.carsurvey.ResponseObjectAPI.Company;

import com.example.a0426611017.carsurvey.Model.Company.ModelFullFormCompany;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0414831216 on 2/26/2018.
 */

public class ResponseFullFormCompany {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelFullFormCompany modelFullFormCompany;

    @SerializedName("message")
    String message;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelFullFormCompany getModelFullFormCompany() {
        return modelFullFormCompany;
    }

    public void setModelFullFormCompany(ModelFullFormCompany modelFullFormCompany) {
        this.modelFullFormCompany = modelFullFormCompany;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
