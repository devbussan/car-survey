package com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey;

import com.example.a0426611017.carsurvey.Model.Survey.ModelCekLingkungan;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0414831216 on 3/13/2018.
 */

public class ResponseDetailSurveyEnviCompany {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelCekLingkungan modelCekLingkungan;

    @SerializedName("message")
    String message;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelCekLingkungan getModelCekLingkungan() {
        return modelCekLingkungan;
    }

    public void setModelCekLingkungan(ModelCekLingkungan modelCekLingkungan) {
        this.modelCekLingkungan = modelCekLingkungan;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
