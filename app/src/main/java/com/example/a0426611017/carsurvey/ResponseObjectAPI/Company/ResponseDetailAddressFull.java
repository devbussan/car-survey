package com.example.a0426611017.carsurvey.ResponseObjectAPI.Company;

import com.example.a0426611017.carsurvey.Model.Company.ModelAddressInfo;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 03/03/2018.
 */

public class ResponseDetailAddressFull {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelAddressInfo modelAddressInfo;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelAddressInfo getModelAddressInfo() {
        return modelAddressInfo;
    }

    public void setModelAddressInfo(ModelAddressInfo modelAddressInfo) {
        this.modelAddressInfo = modelAddressInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
