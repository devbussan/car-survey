package com.example.a0426611017.carsurvey.MainFunction.CallApi;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.ModelAddressInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelCompanyInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelMandatoryInfo;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelRequestCompanySendHq;
import com.example.a0426611017.carsurvey.Model.Company.ModelRequestContactInfoCompany;
import com.example.a0426611017.carsurvey.Model.Company.ModelRequestMiniFormCompany;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseCompanySendHq;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseFullFormCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseMiniFormCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseContactCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseContactOrderId;
import com.example.a0426611017.carsurvey.Survey.ActivitySurvey;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by c201017001 on 20/02/2018.
 */

public class ApiCompanyOrder {

    private SharedPreferences sharedpreferencesMenuCategory;
    private ApiInterface apiInterface;
    private View view;
    private Context context;
    private ProgressDialog loadingDialog;

    private boolean isConnect = false;
    private boolean isFinish = false;
    private int counter = 1;
    private boolean isSaved = false;
    private boolean isSendHq = false;
    private boolean isNext = false;

    private ModelAddressInfo modelAddressInfo = new ModelAddressInfo();
    private ModelMandatoryInfo modelMandatoryInfo = new ModelMandatoryInfo();
    private List<ModelDetailContactInfo> modelDetailContactInfos = new ArrayList<>();
    private ResponseFullFormCompany responseFullFormCompany;
    private ResponseMiniFormCompany responseMiniFormCompany;
    private ResponseContactCompany responseContactCompany;
    private ResponseContactOrderId responseContactOrderId = new ResponseContactOrderId();
    private ResponseCompanySendHq responseCompanySendHq;

    private ModelRequestCompanySendHq modelRequestCompanySendHq = new ModelRequestCompanySendHq();

    private Gson gson = new Gson();
    private int numOfContact = 0;
    private int menuCategory = 0;
    private String logId = null;
    private String companyOrderId = null;
    private String harcode;
    private String adminId;
    private String custData;
    private List<String> contactOrderId = new ArrayList<>();

    public ApiCompanyOrder(View view, Context context, ProgressDialog loadingDialog) {
        this.view = view;
        this.context = context;
        this.loadingDialog = loadingDialog;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sharedpreferencesMenuCategory = this.context.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, MODE_PRIVATE);
        menuCategory = sharedpreferencesMenuCategory.getInt(Constans.KEY_MENU_CATEGORY, 0);
    }

    public boolean isFinish() {
        return isFinish;
    }

    public void setFinish(boolean finish) {
        isFinish = finish;
    }

    public boolean isConnect() {
        return isConnect;
    }

    public ResponseCompanySendHq getResponseCompanySendHq() {
        return responseCompanySendHq;
    }

    public void setResponseCompanySendHq(ResponseCompanySendHq responseCompanySendHq) {
        this.responseCompanySendHq = responseCompanySendHq;
    }

    public ResponseMiniFormCompany getResponseMiniFormCompany() {
        return responseMiniFormCompany;
    }

    public void setResponseMiniFormCompany(ResponseMiniFormCompany responseMiniFormCompany) {
        this.responseMiniFormCompany = responseMiniFormCompany;
    }

    public ResponseContactCompany getResponseContactCompany() {
        return responseContactCompany;
    }

    public void setResponseContactCompany(ResponseContactCompany responseContactCompany) {
        this.responseContactCompany = responseContactCompany;
    }

    public ResponseContactOrderId getResponseContactOrderId() {
        return responseContactOrderId;
    }

    public void setResponseContactOrderId(ResponseContactOrderId responseContactOrderId) {
        this.responseContactOrderId = responseContactOrderId;
    }

    public void saveCompany(ModelRequestMiniFormCompany miniFormCompany, final List<ModelDetailContactInfo> modelContactInfoMini) {
        adminId = miniFormCompany.getUserCreate();
        saveOrUpdateCompanyInfo(miniFormCompany, modelContactInfoMini, null, new ArrayList<String>());
    }

    public void updateCompany(ModelRequestMiniFormCompany miniFormCompany, final List<ModelDetailContactInfo> modelContactInfoMini, String idCompany, List<String> idContact) {
        adminId = miniFormCompany.getUserCreate();
        saveOrUpdateCompanyInfo(miniFormCompany, modelContactInfoMini, idCompany, idContact);
    }

    public void saveCompany(ModelRequestMiniFormCompany miniFormCompany, final List<ModelDetailContactInfo> modelContactInfoMini, boolean isSendHq) {
        this.isSendHq = isSendHq;
        adminId = miniFormCompany.getUserCreate();
        saveOrUpdateCompanyInfo(miniFormCompany, modelContactInfoMini, null, new ArrayList<String>());
    }

    public void updateCompany(ModelRequestMiniFormCompany miniFormCompany, final List<ModelDetailContactInfo> modelContactInfoMini, String idCompany, List<String> idContact, String logId, boolean isSendHq) {
        this.logId = logId;
        this.isSendHq = isSendHq;
        adminId = miniFormCompany.getUserCreate();
        saveOrUpdateCompanyInfo(miniFormCompany, modelContactInfoMini, idCompany, idContact);
    }

    public void sendHq(ModelRequestCompanySendHq modelRequestCompanySendHq, String idLog, String idCom) {
        if (InternetConnection.checkConnection(context)) {
            Call<ResponseCompanySendHq> sendHq = apiInterface.submitHq(modelRequestCompanySendHq, idLog, idCom);
            sendHq.enqueue(new Callback<ResponseCompanySendHq>() {
                @Override
                public void onResponse(Call<ResponseCompanySendHq> call, Response<ResponseCompanySendHq> response) {
                    if (response.isSuccessful()) {
                        setResponseCompanySendHq(response.body());
                        Log.d("Response", responseCompanySendHq.getModelMiniFormCompany().getLogCompany());
                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), responseCompanySendHq.getModelMiniFormCompany().getLogCompany(),
                                Snackbar.LENGTH_LONG)
                                .show();
                        Intent intent = new Intent(context, MainActivityDrawer.class);
                        intent.putExtra(Constans.KEY_MENU, menuCategory);
                        context.startActivity(intent);

                        isConnect = true;
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        isConnect = false;
                        String error = "";
                        try {
                            responseMiniFormCompany = gson.fromJson(response.errorBody().string(), ResponseMiniFormCompany.class);
                            error = responseMiniFormCompany.getMessage();
                        } catch (IOException e) {
                            error = e.getMessage();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
                                Snackbar.LENGTH_LONG)
                                .show();
                        isConnect = false;
                        loadingDialog.dismiss();
                    } else {
                        isConnect = false;
                        Log.e("Error", String.valueOf(R.string.error_api));
                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                        isConnect = false;
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseCompanySendHq> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    Snackbar.make(view.findViewById(R.id.myLayoutCompany), " Error API " + throwable.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                }
            });
        } else {
            Log.e("Error", String.valueOf(R.string.no_connectivity));
            Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            isConnect = false;
            loadingDialog.dismiss();
        }

    }

    private void saveOrUpdateCompanyInfo(final ModelRequestMiniFormCompany miniFormCompany,
                                         final List<ModelDetailContactInfo> modelContactInfoMini,
                                         final String idCompany,
                                         final List<String> idContact) {
        numOfContact = modelContactInfoMini.size();
        companyOrderId = idCompany;
        if (miniFormCompany.isModified()) {
            if (InternetConnection.checkConnection(context)) {
                Call<ResponseMiniFormCompany> insertUpdateMiniFormCompany;
                if (idCompany == null) {
                    insertUpdateMiniFormCompany = apiInterface.saveMiniFormCompany(miniFormCompany);
                } else {
                    insertUpdateMiniFormCompany = apiInterface.editMiniFormCompany(miniFormCompany, idCompany);
                }

                insertUpdateMiniFormCompany.enqueue(new Callback<ResponseMiniFormCompany>() {
                    @Override
                    public void onResponse(Call<ResponseMiniFormCompany> call, Response<ResponseMiniFormCompany>
                            response) {
                        if (response.isSuccessful()) {
                            setResponseMiniFormCompany(response.body());
                            Snackbar.make(view.findViewById(R.id.myLayoutCompany), "success insert company",
                                    Snackbar.LENGTH_LONG)
                                    .show();
                            if (responseMiniFormCompany.getStatus().equals("success")) {
                                Log.d("Log Order", "Company Order : " + responseMiniFormCompany.getMiniFormCompanyResult().getCompanynOrderId());
                                isSaved = true;
                                if (companyOrderId == null) {
                                    logId = responseMiniFormCompany.getMiniFormCompanyResult().getLogDataID();
                                    companyOrderId = responseMiniFormCompany.getMiniFormCompanyResult().getCompanynOrderId();
                                }
                                Log.d("Num of Contact Order", String.valueOf(numOfContact));
                                if (numOfContact > 0) {
                                    for (int i = 0; i < numOfContact; i++) {
                                        ModelRequestContactInfoCompany modelRequestContactInfoCompany = null;
                                        try {
                                            modelRequestContactInfoCompany = new ModelRequestContactInfoCompany(modelContactInfoMini.get(i));
                                            modelRequestContactInfoCompany.setCompanyOrderId(companyOrderId);
                                            Log.d("CompanyOrder",companyOrderId);
                                            modelRequestContactInfoCompany.setUsrCrt(adminId);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                        Log.d("Request Contact " + (i + 1), gson.toJson(modelRequestContactInfoCompany));
                                        saveOrUpdateContact(modelRequestContactInfoCompany, i + 1, companyOrderId, modelContactInfoMini.get(i).getContactOrderId());
                                    }
                                } else {
                                    loadingDialog.dismiss();
                                    Intent intent = new Intent(context, MainActivityDrawer.class);
                                    intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                                    intent.putExtra(Constans.KEY_MENU, menuCategory);
                                    context.startActivity(intent);
                                }
                            } else {
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveOrUpdateCompanyInfo(miniFormCompany, modelContactInfoMini, idCompany, idContact);
                                    }
                                });
                                b.show();
                            }


                            isConnect = true;
                        } else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";
                            try {
                                responseMiniFormCompany = gson.fromJson(response.errorBody().string(), ResponseMiniFormCompany.class);
                                error = responseMiniFormCompany.getMessage();
                                if (error.equals("Response Time Out")) {
                                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                                    b.setTitle("Error Dialog");
                                    b.setCancelable(false);
                                    b.setMessage("Failed Saved \n" +
                                            "Please try Again");
                                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            saveOrUpdateCompanyInfo(miniFormCompany, modelContactInfoMini, idCompany, idContact);
                                        }
                                    });
                                    b.show();
                                }else{
                                    Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
                                            Snackbar.LENGTH_LONG)
                                            .show();
                                }
                            } catch (IOException e) {
                                error = e.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveOrUpdateCompanyInfo(miniFormCompany, modelContactInfoMini, idCompany, idContact);
                                    }
                                });
                                b.show();
                                e.printStackTrace();
                            }
                            Log.e("Error", error);
                            loadingDialog.dismiss();
                        } else {
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    saveOrUpdateCompanyInfo(miniFormCompany, modelContactInfoMini, idCompany, idContact);
                                }
                            });
                            b.show();
                            loadingDialog.dismiss();
                        }


                    }

                    @Override
                    public void onFailure(Call<ResponseMiniFormCompany> call, Throwable t) {
                        Log.e("Error Retrofit", t.toString());
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saved \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                saveOrUpdateCompanyInfo(miniFormCompany, modelContactInfoMini, idCompany, idContact);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                });
            } else {
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Error Dialog");
                b.setCancelable(false);
                b.setMessage("Failed Saved \n" +
                        "Please try Again");
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveOrUpdateCompanyInfo(miniFormCompany, modelContactInfoMini, idCompany, idContact);
                    }
                });
                b.show();
                loadingDialog.dismiss();
            }
        } else {
            isSaved = true;
            if (numOfContact > 0) {
                for (int i = 0; i < numOfContact; i++) {
                    ModelRequestContactInfoCompany modelRequestContactInfoCompany = null;
                    try {
                        modelRequestContactInfoCompany = new ModelRequestContactInfoCompany(modelContactInfoMini.get(i));
                        modelRequestContactInfoCompany.setCompanyOrderId(companyOrderId);
                        modelRequestContactInfoCompany.setUsrCrt(adminId);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.d("Request Contact " + (i + 1), gson.toJson(modelRequestContactInfoCompany));
                    saveOrUpdateContact(modelRequestContactInfoCompany, i + 1, companyOrderId, modelContactInfoMini.get(i).getContactOrderId());
                }
            } else {
                Intent intent = new Intent(context, MainActivityDrawer.class);
                intent.putExtra(Constans.KEY_MENU, menuCategory);
                intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                context.startActivity(intent);
                loadingDialog.dismiss();
            }
        }
    }

    public void saveOrUpdateContact(final ModelRequestContactInfoCompany miniFormContactCompany, final int idx, final String idCompany, final String idContact) {
        Log.d("Request", gson.toJson(miniFormContactCompany));
        Log.d("Request", gson.toJson(idContact));
        if (InternetConnection.checkConnection(context)) {
            Call<ResponseContactCompany> cntCompany;
            if (idContact == null) {
                cntCompany = apiInterface.saveContactCompany(miniFormContactCompany);

            } else {
                cntCompany = apiInterface.editContactCompany(miniFormContactCompany, idCompany, idContact);
            }
            cntCompany.enqueue(new Callback<ResponseContactCompany>() {
                @Override
                public void onResponse(Call<ResponseContactCompany> call, Response<ResponseContactCompany> response) {
                    if (response.isSuccessful()) {
                        setResponseContactCompany(response.body());
                        Log.d("Response Contact Info " + idx, responseContactCompany.getModelContactCompany().getContactOrderId());
                        if (idx == numOfContact) {
                            if (isSendHq) {
                                Log.d("Debug", "Send HQ");
                                Log.d("Log Id", logId);
                                loadingDialog.dismiss();
                                ModelRequestCompanySendHq objectSendHq = new ModelRequestCompanySendHq();
                                objectSendHq.setUsrPd(adminId);
                                objectSendHq.setStaTus("1");
                                objectSendHq.setIsReadcmo("0");
                                sendHq(objectSendHq, logId, idCompany);
                            } else if (isNext) {
                                loadingDialog.dismiss();
                                Intent intent = new Intent(context, ActivitySurvey.class);
                                intent.putExtra(Constans.KEY_MENU, Constans.MENU_COMPANY);
                                intent.putExtra(Constans.KeySharedPreference.ORDER_ID, companyOrderId);
                                context.startActivity(intent);
                            } else {
                                loadingDialog.dismiss();
                                Intent intent = new Intent(context, MainActivityDrawer.class);
                                intent.putExtra(Constans.KEY_MENU, menuCategory);
                                intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                                context.startActivity(intent);
                            }

                        }
                        isSaved = true;
                        counter++;
                    } else if (response.code() == 404) {
                        isConnect = false;
                        String error = "";
                        try {
                            responseContactCompany = gson.fromJson(response.errorBody().string(), ResponseContactCompany.class);
                            error = responseContactCompany.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveOrUpdateContact(miniFormContactCompany, idx, idCompany, idContact);
                                    }
                                });
                                b.show();
                            }else{
                                Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    saveOrUpdateContact(miniFormContactCompany, idx, idCompany, idContact);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saved \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                saveOrUpdateContact(miniFormContactCompany, idx, idCompany, idContact);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }

                }

                @Override
                public void onFailure(Call<ResponseContactCompany> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Dialog");
                    b.setCancelable(false);
                    b.setMessage("Failed Saved \n" +
                            "Please try Again");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            saveOrUpdateContact(miniFormContactCompany, idx, idCompany, idContact);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });

        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Dialog");
            b.setCancelable(false);
            b.setMessage("Failed Saved \n" +
                    "Please try Again");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    saveOrUpdateContact(miniFormContactCompany, idx, idCompany, idContact);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }

    }

    public void saveFullForm(ModelCompanyInfo modelCompanyInfo,
                             String idCompanyOrder,
                             ModelAddressInfo modelAddressInfo,
                             ModelMandatoryInfo modelMandatoryInfo,
                             List<ModelDetailContactInfo> modelDetailContactInfos,
                             List<String> contactOrderId,
                             String custData,
                             boolean isNext,
                             String logid) {

        this.modelAddressInfo = modelAddressInfo;
        this.modelMandatoryInfo = modelMandatoryInfo;
        this.modelDetailContactInfos = modelDetailContactInfos;
        this.companyOrderId = idCompanyOrder;
        this.contactOrderId = contactOrderId;
        this.adminId = modelCompanyInfo.getUserCreate();
        this.custData = custData;
        this.isNext = isNext;
        this.logId = logid;

        saveFullFormCompany(modelCompanyInfo, idCompanyOrder, logid);
//        saveFullFormEvidence(this.modelMandatoryInfo, "16");
    }

    public void saveFullFormCompany(final ModelCompanyInfo modelCompanyInfo, final String idCompanyOrder, final String logid) {
        loadingDialog.show();
        if (modelCompanyInfo.isModified()) {
            if (InternetConnection.checkConnection(context)) {
                Call<ResponseFullFormCompany> saveFullFormCompany = apiInterface.saveFullFormCompany(modelCompanyInfo, idCompanyOrder, logid);
                saveFullFormCompany.enqueue(new Callback<ResponseFullFormCompany>() {
                    @Override
                    public void onResponse(Call<ResponseFullFormCompany> call, Response<ResponseFullFormCompany> response) {
                        if (response.isSuccessful()) {
                            responseFullFormCompany = response.body();
                            if (responseFullFormCompany.getStatus().equals("success")) {
                                Log.d("Status Company Full", responseFullFormCompany.getModelFullFormCompany().getCompanyCustStatus());
                                Log.d("Status Company Full", responseFullFormCompany.getModelFullFormCompany().getCompanyCustId());
                                if(custData.equals("0")){
                                    custData = responseFullFormCompany.getModelFullFormCompany().getCompanyCustId();
                                }
//                            saveFullFormAddress(modelAddressInfo,responseFullFormCompany.getModelFullFormCompany().getCompanyCustId());
                                try {
                                    saveFullFormAddress(modelAddressInfo, custData);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address", e.getMessage());
                                    Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Address : Error upload image",
                                            Snackbar.LENGTH_LONG)
                                            .show();
                                    e.printStackTrace();
                                }

                            } else {
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFullFormCompany(modelCompanyInfo, idCompanyOrder, logid);
                                    }
                                });
                                b.show();
                                loadingDialog.dismiss();
                            }

                        } else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";
                            try {
                                responseFullFormCompany = gson.fromJson(response.errorBody().string(), ResponseFullFormCompany.class);
                                error = responseFullFormCompany.getMessage();
                                if(error.equals("Response Time Out")){
                                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                                    b.setTitle("Error Dialog");
                                    b.setCancelable(false);
                                    b.setMessage("Failed Saved \n" +
                                            "Please try Again");
                                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            saveFullFormCompany(modelCompanyInfo, idCompanyOrder, logid);
                                        }
                                    });
                                    b.show();
                                    loadingDialog.dismiss();
                                }else{
                                    Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
                                            Snackbar.LENGTH_LONG)
                                            .show();
                                }
                            } catch (IOException e) {
                                error = e.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFullFormCompany(modelCompanyInfo, idCompanyOrder, logid);
                                    }
                                });
                                b.show();
                                loadingDialog.dismiss();
                                e.printStackTrace();
                            }

                            Log.e("Error Company Full", error);
                            loadingDialog.dismiss();
                        } else {
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    saveFullFormCompany(modelCompanyInfo, idCompanyOrder, logid);
                                }
                            });
                            b.show();
                            Log.e("Error Company Full", String.valueOf(R.string.error_api));
                            loadingDialog.dismiss();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseFullFormCompany> call, Throwable throwable) {
                        Log.e("Retrofit Company Full", throwable.toString());
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saved \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                saveFullFormCompany(modelCompanyInfo, idCompanyOrder, logid);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                });
            } else {
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Error Dialog");
                b.setCancelable(false);
                b.setMessage("Failed Saved \n" +
                        "Please try Again");
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveFullFormCompany(modelCompanyInfo, idCompanyOrder, logid);
                    }
                });
                b.show();
                Log.e("Connection Company Full", String.valueOf(R.string.no_connectivity));
                loadingDialog.dismiss();
            }
        } else {
            try {
                saveFullFormAddress(modelAddressInfo, custData);
            } catch (IOException e) {
                loadingDialog.dismiss();
                Log.e("Error Address", e.getMessage());
                Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Address : Error upload image",
                        Snackbar.LENGTH_LONG)
                        .show();
                e.printStackTrace();
            }
        }
    }

    public void saveFullFormAddress(ModelAddressInfo modelAddressInfo, final String idCustData) throws IOException {
        loadingDialog.show();
        Log.d("modelAddressInfo", gson.toJson(modelAddressInfo));
        if (modelAddressInfo.isModified()) {
            if (!modelAddressInfo.getBiuldingImageUrl().startsWith("http")
                    && !modelAddressInfo.getNameFileBuildingImage().equals("")
                    && !modelAddressInfo.getNameFileBuildingImage().equals("No selected file")) {
                FileFunction fileFunction = new FileFunction();
                modelAddressInfo.setBiuldingImageUrl(fileFunction.createBase64(modelAddressInfo.getNameFileBuildingImage()));
            }
            if (InternetConnection.checkConnection(context)) {
                Call<ResponseFullFormCompany> saveFullFormCompany = apiInterface.saveFullFormAddress(modelAddressInfo, idCustData);
                saveFullFormCompany.enqueue(new Callback<ResponseFullFormCompany>() {
                    @Override
                    public void onResponse(Call<ResponseFullFormCompany> call, Response<ResponseFullFormCompany> response) {
                        if (response.isSuccessful()) {
                            responseFullFormCompany = response.body();
                            if (responseFullFormCompany.getStatus().equals("success")) {
                                Log.d("Status Address Full", responseFullFormCompany.getModelFullFormCompany().getCompanyAddr());
                                try {
                                    saveFullFormEvidence(modelMandatoryInfo, idCustData);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Evidence", e.getMessage());
                                    Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Evidence : Error upload image",
                                            Snackbar.LENGTH_LONG)
                                            .show();
                                    e.printStackTrace();
                                }

                            } else {
                                Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Address Gagal Simpan",
                                        Snackbar.LENGTH_LONG)
                                        .show();
                                loadingDialog.dismiss();
                            }
                        } else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";
                            try {
                                responseFullFormCompany = gson.fromJson(response.errorBody().string(), ResponseFullFormCompany.class);
                                error = responseFullFormCompany.getMessage();
                            } catch (IOException e) {
                                error = e.getMessage();
                                e.printStackTrace();
                            }
                            Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Address : " + error,
                                    Snackbar.LENGTH_LONG)
                                    .show();
                            Log.e("Error Address", error);
                            loadingDialog.dismiss();
                        } else {
                            isConnect = false;
                            Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Company : " + String.valueOf(R.string.error_api),
                                    Snackbar.LENGTH_LONG)
                                    .show();
                            Log.e("Other Error Address", "Address : " + String.valueOf(R.string.error_api));
                            loadingDialog.dismiss();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseFullFormCompany> call, Throwable throwable) {
                        Log.e("Error Retrofit Address", throwable.toString());
                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Address : " + throwable.toString(),
                                Snackbar.LENGTH_LONG)
                                .show();
                        isConnect = false;
                        loadingDialog.dismiss();
                    }
                });
            } else {
                Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Address : " + String.valueOf(R.string.no_connectivity),
                        Snackbar.LENGTH_LONG)
                        .show();
                Log.e("Err Connection Address", "Address : " + String.valueOf(R.string.no_connectivity));
                isConnect = false;
                loadingDialog.dismiss();
            }
        } else {
            try {
                saveFullFormEvidence(modelMandatoryInfo, idCustData);
            } catch (IOException e) {
                loadingDialog.dismiss();
                Log.e("Error Evidence", e.getMessage());
                Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Evidence : Error upload image",
                        Snackbar.LENGTH_LONG)
                        .show();
                e.printStackTrace();
            }
        }
    }

    public void saveFullFormEvidence(ModelMandatoryInfo modelMandatoryInfo, String idCustData) throws IOException {
        numOfContact = modelDetailContactInfos.size();
        Log.d("evidence",gson.toJson(modelMandatoryInfo));
        loadingDialog.show();
        if (modelMandatoryInfo.isModified()) {
            if (!modelMandatoryInfo.getNameImageAktaUrl().startsWith("http")
                    && !modelMandatoryInfo.getNameImageAkta().equals("No selected file")) {
                FileFunction fileFunction = new FileFunction();
                modelMandatoryInfo.setNameImageAktaUrl(fileFunction.createBase64(modelMandatoryInfo.getNameImageAkta()));
            }
            if (!modelMandatoryInfo.getNameImageSiupUrl().startsWith("http")
                    && !modelMandatoryInfo.getNameImageSiup().equals("No selected file")) {
                FileFunction fileFunction = new FileFunction();
                modelMandatoryInfo.setNameImageSiupUrl(fileFunction.createBase64(modelMandatoryInfo.getNameImageSiup()));
            }
            if (!modelMandatoryInfo.getNameImageDomisiliUrl().startsWith("http")
                    && !modelMandatoryInfo.getNameImageDomisili().equals("No selected file")) {
                FileFunction fileFunction = new FileFunction();
                modelMandatoryInfo.setNameImageDomisiliUrl(fileFunction.createBase64(modelMandatoryInfo.getNameImageDomisili()));
            }
            if (!modelMandatoryInfo.getNameImageTdpUrl().startsWith("http")
                    && !modelMandatoryInfo.getNameImageTdp().equals("No selected file")) {
                FileFunction fileFunction = new FileFunction();
                modelMandatoryInfo.setNameImageTdpUrl(fileFunction.createBase64(modelMandatoryInfo.getNameImageTdp()));
            }
            if (InternetConnection.checkConnection(context)) {
                Call<ResponseFullFormCompany> saveFullFormCompany = apiInterface.saveFullFormEvidence(modelMandatoryInfo, idCustData);
                saveFullFormCompany.enqueue(new Callback<ResponseFullFormCompany>() {
                    @Override
                    public void onResponse(Call<ResponseFullFormCompany> call, Response<ResponseFullFormCompany> response) {
                        if (response.isSuccessful()) {
                            responseFullFormCompany = response.body();
                            if (responseFullFormCompany.getStatus().equals("success")) {
                                Log.d("Status Evidence", responseFullFormCompany.getModelFullFormCompany().getCompanyEvidence());
                                isSaved = true;
                                if (numOfContact > 0) {
                                    for (int i = 0; i < numOfContact; i++) {
                                        ModelRequestContactInfoCompany modelRequestContactInfoCompany = null;
                                        try {
                                            modelRequestContactInfoCompany = new ModelRequestContactInfoCompany(modelDetailContactInfos.get(i));
                                            modelRequestContactInfoCompany.setCompanyOrderId(companyOrderId);
                                            modelRequestContactInfoCompany.setUsrCrt(adminId);
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }

                                        Log.d("Request Contact " + (i + 1), gson.toJson(modelRequestContactInfoCompany));

//                                        if (contactOrderId.size() > i) {
                                            saveOrUpdateContact(modelRequestContactInfoCompany, i + 1, companyOrderId, modelDetailContactInfos.get(i).getContactOrderId());
//                                        } else {
//                                            saveOrUpdateContact(modelRequestContactInfoCompany, i + 1, companyOrderId, null);
//                                        }
//                                if(success){
//
//                                }
                                    }
                                } else {
                                    if (isNext) {
                                        loadingDialog.dismiss();
                                        Intent intent = new Intent(context, ActivitySurvey.class);
//                                    Intent intent = new Intent(context, ActivityCompanyFinancial.class);
                                        intent.putExtra(Constans.KEY_MENU, Constans.MENU_COMPANY);
                                        intent.putExtra(Constans.KeySharedPreference.ORDER_ID, companyOrderId);
                                        context.startActivity(intent);
                                    } else {
                                        loadingDialog.dismiss();
                                        Intent intent = new Intent(context, MainActivityDrawer.class);
                                        intent.putExtra(Constans.KEY_MENU, menuCategory);
                                        intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                                        context.startActivity(intent);
                                    }
                                }
                            } else {
                                Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Evidence Gagal Simpan",
                                        Snackbar.LENGTH_LONG)
                                        .show();
                                loadingDialog.dismiss();
                            }
                        } else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";
                            try {
                                responseFullFormCompany = gson.fromJson(response.errorBody().string(), ResponseFullFormCompany.class);
                                error = responseFullFormCompany.getMessage();
                            } catch (IOException e) {
                                error = e.getMessage();
                                e.printStackTrace();
                            }
                            Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Evidence : " + error,
                                    Snackbar.LENGTH_LONG)
                                    .show();
                            Log.e("Error Evidence", error);
                            loadingDialog.dismiss();
                        } else {
                            isConnect = false;
                            Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Evidence : " + String.valueOf(R.string.error_api),
                                    Snackbar.LENGTH_LONG)
                                    .show();
                            Log.e("Other Error Evidence", String.valueOf(R.string.error_api));
                            loadingDialog.dismiss();
                        }

                    }

                    @Override
                    public void onFailure(Call<ResponseFullFormCompany> call, Throwable throwable) {
                        Log.e("Error Retrofit Evidence", throwable.toString());
                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Evidence : " + throwable.toString(),
                                Snackbar.LENGTH_LONG)
                                .show();
                        isConnect = false;
                        loadingDialog.dismiss();
                    }
                });
            } else {
                Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Evidence : " + String.valueOf(R.string.no_connectivity),
                        Snackbar.LENGTH_LONG)
                        .show();
                Log.e("Err Connection Evidence", String.valueOf(R.string.no_connectivity));
                isConnect = false;
                loadingDialog.dismiss();
            }
        } else {
            isSaved = true;
            if (numOfContact > 0) {
                for (int i = 0; i < numOfContact; i++) {
                    ModelRequestContactInfoCompany modelRequestContactInfoCompany = null;
                    try {
                        modelRequestContactInfoCompany = new ModelRequestContactInfoCompany(modelDetailContactInfos.get(i));
                        modelRequestContactInfoCompany.setCompanyOrderId(companyOrderId);
                        modelRequestContactInfoCompany.setUsrCrt(adminId);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    Log.d("Request Contact " + (i + 1), gson.toJson(modelRequestContactInfoCompany));

//                    if (contactOrderId.size() > i) {
                        saveOrUpdateContact(modelRequestContactInfoCompany, i + 1, companyOrderId, modelDetailContactInfos.get(i).getContactOrderId());
//                    } else {
//                        saveOrUpdateContact(modelRequestContactInfoCompany, i + 1, companyOrderId, null);
//                    }
//                                if(success){
//
//                                }
                }
            } else {
                if (isNext) {
                    loadingDialog.dismiss();
                    Intent intent = new Intent(context, ActivitySurvey.class);
//                                    Intent intent = new Intent(context, ActivityCompanyFinancial.class);
                    intent.putExtra(Constans.KEY_MENU, Constans.MENU_COMPANY);
                    intent.putExtra(Constans.KeySharedPreference.ORDER_ID, companyOrderId);
                    context.startActivity(intent);
                } else {
                    loadingDialog.dismiss();
                    Intent intent = new Intent(context, MainActivityDrawer.class);
                    intent.putExtra(Constans.KEY_MENU, menuCategory);
                    intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                    context.startActivity(intent);
                }
            }
        }
    }
    //    TODO REMOVED
//    public void getContactOrderId(String companyOrderId){
//        if (InternetConnection.checkConnection(context)) {
//            Call<ResponseContactOrderId> contactOrderID = apiInterface.getContactOrderId(companyOrderId);
//            contactOrderID.enqueue(new Callback<ResponseContactOrderId>() {
//                @Override
//                public void onResponse(Call<ResponseContactOrderId> call, Response<ResponseContactOrderId>
//                        response) {
//                    if (response.isSuccessful()) {
//                        setResponseContactOrderId(response.body());
//                        loadingDialog.dismiss();
//                        isConnect = true;
//                        setFinish(true);
//                        isFinish = true;
//                    } else if(response.code() == 404){
//                        String error = "";
//                        try {
//                            responseContactOrderId = gson.fromJson(response.errorBody().string(), ResponseContactOrderId.class);
//                            error = responseContactOrderId.getMessage();
//                        } catch (IOException e) {
//                            error = e.getMessage();
//                            e.printStackTrace();
//                        }
//                        setFinish(true);
//                        Log.e("Error", error);
//                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                        isConnect = false;
//                        loadingDialog.dismiss();
//                    }else{
//                        setFinish(true);
//                        loadingDialog.dismiss();
//                        isConnect = false;
//                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.error_api,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseContactOrderId> call, Throwable t) {
//                    Log.e("Error Retrofit", t.toString());
//                    Snackbar.make(view.findViewById(R.id.myLayoutCompany), t.toString(),
//                            Snackbar.LENGTH_LONG)
//                            .show();
//                    setFinish(true);
//                    loadingDialog.dismiss();
//                }
//            });
//        } else {
//            Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.no_connectivity,
//                    Snackbar.LENGTH_LONG)
//                    .show();
//            setFinish(true);
//            loadingDialog.dismiss();
//        }
//    }

//    public void updateCompanyInfo(ModelRequestMiniFormCompany miniFormCompany, final List<ModelContactInfoMini> modelContactInfoMini, String companyOrderId) {
//        numOfContact = modelContactInfoMini.size();
//        if (InternetConnection.checkConnection(context)) {
//            Call<ResponseMiniFormCompany> updCompny = apiInterface.editMiniFormCompany(miniFormCompany, companyOrderId);
//            updCompny.enqueue(new Callback<ResponseMiniFormCompany>() {
//                @Override
//                public void onResponse(Call<ResponseMiniFormCompany> call, Response<ResponseMiniFormCompany> response) {
//                    if (response.isSuccessful()) {
//                        setResponseMiniFormCompany(response.body());
//                        if (responseMiniFormCompany.getStatus().equals("success")) {
//                            for (int i = 0; i < numOfContact; i++) {
//                                ModelRequestContactInfoCompany objectRequestContactInfoCompany = null;
//                                try {
//                                    objectRequestContactInfoCompany = new ModelRequestContactInfoCompany(modelContactInfoMini.get(i));
//                                    objectRequestContactInfoCompany.setUsrCrt("6");
//                                    objectRequestContactInfoCompany.setCntInfoFlag(String.valueOf(i + 1));
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//
//                                Log.d("Request Contact " + (i + 1), gson.toJson(objectRequestContactInfoCompany));
//                                updContactInfo(objectRequestContactInfoCompany, i + 1);
////                                if(success){
////
////                                }
//                            }
//                        }else {
//                            loadingDialog.dismiss();
//                            Snackbar.make(view.findViewById(R.id.myLayoutCompany),"Error teuing  dimana, papay we ku maneh",Snackbar.LENGTH_LONG).show();
//                        }
//                        //Snackbar.make(view.findViewById(R.id.myLayoutCompany),responseMiniFormCompany.getMiniFormCompanyResult().getCompanyInfoOrder(),Snackbar.LENGTH_LONG).show();
//                        //loadingDialog.dismiss();
//                    } else if (response.code() == 404) {
//                        isConnect = false;
//                        String error = "";
//                        try {
//                            responseMiniFormCompany = gson.fromJson(response.errorBody().string(), ResponseMiniFormCompany.class);
//                            error = responseMiniFormCompany.getMessage();
//                        } catch (IOException e) {
//                            error = e.getMessage();
//                            e.printStackTrace();
//                        }
//                        Log.e("Error", error);
//                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                        isConnect = false;
//                        loadingDialog.dismiss();
//                    } else {
//                        isConnect = false;
//                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.error_api,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                        isConnect = false;
//                        loadingDialog.dismiss();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseMiniFormCompany> call, Throwable throwable) {
//                    Log.e("Error Retrofit", throwable.toString());
//                    Snackbar.make(view.findViewById(R.id.myLayoutCompany), throwable.toString(),
//                            Snackbar.LENGTH_LONG)
//                            .show();
//                    isConnect = false;
//                    loadingDialog.dismiss();
//                }
//            });
//        }
//    }

//    TODO REMOVED
//    private void updContactInfo(ModelRequestContactInfoCompany objectRequestContactInfoCompany, final int i) {
//        if (InternetConnection.checkConnection(context)) {
//            Call<ResponseContactCompany> updcontact = apiInterface.editContactCompany(objectRequestContactInfoCompany, "111", "48");
//            updcontact.enqueue(new Callback<ResponseContactCompany>() {
//                @Override
//                public void onResponse(Call<ResponseContactCompany> call, Response<ResponseContactCompany> response) {
//                    if (response.isSuccessful()) {
//                        setResponseContactCompany(response.body());
//                        if (i == numOfContact) {
//                            Log.d("Pesaaaaan",responseContactCompany.getStatus());
//                            Snackbar.make(view.findViewById(R.id.myLayoutCompany), responseContactCompany.getModelContactCompany().getContactOrderId(),
//                                    Snackbar.LENGTH_LONG)
//                                    .show();
//                            loadingDialog.dismiss();
//                            Intent intent = new Intent(context, MainActivityDrawer.class);
//                            intent.putExtra(Constans.KEY_MENU, menuCategory);
//                            context.startActivity(intent);
//
//                        }
//                    } else if (response.code() == 404) {
//                        isConnect = false;
//                        String error = "";
//                        try {
//                            responseContactCompany = gson.fromJson(response.errorBody().string(), ResponseContactCompany.class);
//                            error = responseContactCompany.getMessage();
//                        } catch (IOException e) {
//                            error = e.getMessage();
//                            e.printStackTrace();
//                        }
//                        Log.e("Error", error);
//                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                        isConnect = false;
//                        loadingDialog.dismiss();
//                    } else {
//                        isConnect = false;
//                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.error_api,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                        loadingDialog.dismiss();
//                    }
//                    isConnect = true;
//                    loadingDialog.dismiss();
//                }
//
//                @Override
//                public void onFailure(Call<ResponseContactCompany> call, Throwable throwable) {
//
//                }
//            });
//        } else {
//            Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.no_connectivity,
//                    Snackbar.LENGTH_LONG)
//                    .show();
//            isConnect = false;
//            loadingDialog.dismiss();
//        }
//    }


//    TODO REMOVED
//    public boolean updateCompanyOrder(String companyOrderID, ModelRequestMiniFormCompany miniFormCompany, final List<ModelContactInfoMini> modelContactInfoMini) {
//        numOfContact = modelContactInfoMini.size();
//        if (InternetConnection.checkConnection(context)) {
//            Call<ResponseMiniFormCompany> ddlCompany = apiInterface.saveMiniFormCompany(miniFormCompany);
//            ddlCompany.enqueue(new Callback<ResponseMiniFormCompany>() {
//                @Override
//                public void onResponse(Call<ResponseMiniFormCompany> call, Response<ResponseMiniFormCompany>
//                        response) {
//                    if (response.isSuccessful()) {
//                        setResponseMiniFormCompany(response.body());
//
//                        if (responseMiniFormCompany.getStatus().equals("success")) {
//                            Log.d("Log Order", "Company Order : " + responseMiniFormCompany.getMiniFormCompanyResult().getCompanynOrderId());
////                            Toast.makeText(context, "Company Order : "+responseMiniFormCompany.getMiniFormCompanyResult().getCompanynOrderId(), Toast.LENGTH_LONG).show();
//                            for (int i = 0; i < numOfContact; i++) {
//                                ModelRequestContactInfoCompany objectRequestContactInfoCompany = null;
//                                try {
//                                    objectRequestContactInfoCompany = new ModelRequestContactInfoCompany(modelContactInfoMini.get(i));
//                                    objectRequestContactInfoCompany.setCompanyOrderId(responseMiniFormCompany.getMiniFormCompanyResult().getCompanynOrderId());
//                                    objectRequestContactInfoCompany.setUsrCrt("6");
//                                    objectRequestContactInfoCompany.setCntInfoFlag(String.valueOf(i + 1));
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//
//                                Log.d("Request Contact " + (i + 1), gson.toJson(objectRequestContactInfoCompany));
////                                saveContactInfo(objectRequestContactInfoCompany, i + 1);
////                                if(success){
////
////                                }
//                            }
//                            isTrue = true;
//                        } else {
//                            isTrue = false;
//                        }
//
//
//                        isConnect = true;
//                    } else if (response.code() == 404) {
//                        isConnect = false;
//                        String error = "";
//                        try {
//                            responseMiniFormCompany = gson.fromJson(response.errorBody().string(), ResponseMiniFormCompany.class);
//                            error = responseMiniFormCompany.getMessage();
//                        } catch (IOException e) {
//                            error = e.getMessage();
//                            e.printStackTrace();
//                        }
//                        Log.e("Error", error);
//                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                        isTrue = false;
//                        loadingDialog.dismiss();
//                    } else {
//                        isConnect = false;
//                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.error_api,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                        isTrue = false;
//                        loadingDialog.dismiss();
//                    }
//
//
//                }
//
//                @Override
//                public void onFailure(Call<ResponseMiniFormCompany> call, Throwable t) {
//                    Log.e("Error Retrofit", t.toString());
//                    Snackbar.make(view.findViewById(R.id.myLayoutCompany), "Company Order : " + t.toString(),
//                            Snackbar.LENGTH_LONG)
//                            .show();
//                    isTrue = false;
//                    isConnect = false;
//                    loadingDialog.dismiss();
//                }
//            });
//        } else {
//            Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.no_connectivity,
//                    Snackbar.LENGTH_LONG)
//                    .show();
//            isConnect = false;
//            loadingDialog.dismiss();
//            isTrue = false;
//        }
//        return isTrue;
//    }
}
