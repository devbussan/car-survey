package com.example.a0426611017.carsurvey.ResponseObjectAPI.Finance;

import com.example.a0426611017.carsurvey.Model.Financial.ModelFinance;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0414831216 on 3/10/2018.
 */

public class ResponseFinancial {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelFinance modelFinance;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelFinance getModelFinance() {
        return modelFinance;
    }

    public void setModelFinance(ModelFinance modelFinance) {
        this.modelFinance = modelFinance;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
