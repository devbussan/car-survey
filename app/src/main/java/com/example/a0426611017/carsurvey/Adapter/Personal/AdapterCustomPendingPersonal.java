package com.example.a0426611017.carsurvey.Adapter.Personal;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.ActivityBanding;
import com.example.a0426611017.carsurvey.AddNew.Personal.ActivityNewOrderPersonal;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelGridListPersonal;
import com.example.a0426611017.carsurvey.Personal.DetailFullForm.ActivityPersonalDetail;
import com.example.a0426611017.carsurvey.Personal.DetailMiniForm.ActivityPersonalDetailOrder;
import com.example.a0426611017.carsurvey.Personal.FullForm.ActivityPersonalFullEdit;
import com.example.a0426611017.carsurvey.R;

import java.util.List;

/**
 * Created by 0426611017 on 12/14/2017.
 */

public class AdapterCustomPendingPersonal extends BaseAdapter {
    private static List<ModelGridListPersonal> listpersonalPending;
    private LayoutInflater  mInflater;
    private Context context;
    private int tabCurrent;
    private int menu;

    public AdapterCustomPendingPersonal (Context ActivityListPersonalPending, List<ModelGridListPersonal> results, int tab, int menu) {
        listpersonalPending = results;
        context = ActivityListPersonalPending;
        mInflater = LayoutInflater.from(ActivityListPersonalPending);
        this.tabCurrent = tab;
        this.menu = menu;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        AdapterCustomPendingPersonal.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_list_row_pending, null);
            holder = new AdapterCustomPendingPersonal.ViewHolder();
            holder.personalDatePendingTextField = convertView.findViewById(R.id.date);
            holder.personalNamePendingTextField = convertView.findViewById(R.id.name);
            holder.personalNo_idPendingTextField = convertView.findViewById(R.id.no_id);
            holder.buttonDetail = convertView.findViewById(R.id.buttonDetail);
            holder.buttonEdit = convertView.findViewById(R.id.buttonEdit);
            holder.personal_comment = convertView.findViewById(R.id.comment);
            holder.buttonViewMoreComment = convertView.findViewById(R.id.btn_comment);


            if(tabCurrent == 3){
                holder.buttonEdit.setText("Banding");
            }else{
                holder.buttonEdit.setText("Edit");
            }

            convertView.setTag(holder);

        } else {
            holder = (AdapterCustomPendingPersonal.ViewHolder) convertView.getTag();
        }

        final String personalDate = listpersonalPending.get(position).getDateCreate();
        final String personalName = listpersonalPending.get(position).getFullName();
        final String personalNo_id = listpersonalPending.get(position).getIdNo();

        holder.personalDatePendingTextField.setText(personalDate);
        holder.personalNamePendingTextField.setText(personalName);
        holder.personalNo_idPendingTextField.setText(personalNo_id);

        if(menu == Constans.MENU_PERSONAL_ORDER_FORM){
            if(tabCurrent == Constans.TAB_RETURN_ORDER || tabCurrent == Constans.TAB_REJECT_ORDER  ) {
                if(listpersonalPending.get(position).getComMent().equals("-")){
                    holder.buttonViewMoreComment.setVisibility(View.GONE);
                    holder.personal_comment.setText("");
                }
                else{
                    holder.personal_comment.setText(listpersonalPending.get(position).getComMent());
                    holder.personal_comment.setTextColor(context.getResources().getColor(R.color.color_company));
                }
            }else if(tabCurrent == Constans.TAB_DRAFT || tabCurrent == Constans.TAB_CLEAR  ){
                holder.personal_comment.setText("");
                holder.buttonViewMoreComment.setVisibility(View.GONE);
            }
        }


        if(menu == Constans.MENU_PERSONAL_FULL_FORM) {
            if(tabCurrent == Constans.TAB_RETURN_ORDER || tabCurrent == Constans.TAB_REJECT_ORDER ||
                    tabCurrent == Constans.TAB_CHECK_HQ) {
                if(listpersonalPending.get(position).getComMent().equals("-")){
                    holder.personal_comment.setText("");
                    holder.buttonViewMoreComment.setVisibility(View.GONE);
                }
                else{
                    holder.personal_comment.setText(listpersonalPending.get(position).getComMent());
                    holder.personal_comment.setTextColor(context.getResources().getColor(R.color.color_company));
                }
            }else if(tabCurrent == Constans.TAB_DRAFT || tabCurrent == Constans.TAB_CLEAR  ){
                holder.personal_comment.setText("");
                holder.buttonViewMoreComment.setVisibility(View.GONE);
            }
        }




        holder.buttonDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                SharedPreferences.Editor editor;
                if(tabCurrent == Constans.TAB_CLEAR || menu == Constans.MENU_PERSONAL_FULL_FORM ){
                    intent = new Intent(context,ActivityPersonalDetail.class);
                    editor = context.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_CUST_DATA_ID, listpersonalPending.get(position).getCustDataId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SURVEY_ID, listpersonalPending.get(position).getSurveyId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_ID, listpersonalPending.get(position).getGuarantorCustId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.PERSONAL_KK_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_PERSONAL_KK_ID, listpersonalPending.get(position).getKkNo());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.PERSONAL_KK_IMAGE, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_PERSONAL_KK_IMAGE, listpersonalPending.get(position).getKkImgUrl());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_KK_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_KK_ID, listpersonalPending.get(position).getKkNoSG());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_KK_IMAGE, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_KK_IMAGE, listpersonalPending.get(position).getkkImgURLSG());
                    editor.apply();
                }else{
                    intent = new Intent(context,ActivityPersonalDetailOrder.class);
                }
                intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, listpersonalPending.get(position).getPersonalOrderId());
                context.startActivity(intent);
            }
        });

        holder.buttonViewMoreComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("testing","kumaha atuh");
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom_dialog);
                TextView contentText = dialog.findViewById(R.id.text_view_comment_message);
                TextView headerText = dialog.findViewById(R.id.text_view_comment_title);
                final ImageView close = dialog.findViewById(R.id.btn_close_comment);
                TextView commentName = dialog.findViewById(R.id.text_view_comment_name);
                commentName.setText(listpersonalPending.get(position).getFullName());
                contentText.setText(listpersonalPending.get(position).getComMent());
                headerText.setText("Comment");
                close.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                          dialog.dismiss();
                    }
                });

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.create();
                dialog.show();

            }
        });

        holder.buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                SharedPreferences.Editor editor;
                if(tabCurrent == 3) {
                    intent = new Intent(context, ActivityBanding.class);
                    intent.putExtra(Constans.KEY_MENU, Constans.MENU_PERSONAL);

                }else if(tabCurrent == Constans.TAB_CLEAR || menu == Constans.MENU_PERSONAL_FULL_FORM){
                    intent = new Intent(context, ActivityPersonalFullEdit.class);
                    editor = context.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_CUST_DATA_ID, listpersonalPending.get(position).getCustDataId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SURVEY_ID, listpersonalPending.get(position).getSurveyId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_ID, listpersonalPending.get(position).getGuarantorCustId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.PERSONAL_KK_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_PERSONAL_KK_ID, listpersonalPending.get(position).getKkNo());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.PERSONAL_KK_IMAGE, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_PERSONAL_KK_IMAGE, listpersonalPending.get(position).getKkImgUrl());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_KK_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_KK_ID, listpersonalPending.get(position).getKkNoSG());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_KK_IMAGE, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_KK_IMAGE, listpersonalPending.get(position).getkkImgURLSG());
                    editor.apply();
                }else{
                    intent = new Intent(context, ActivityNewOrderPersonal.class);
                }

                editor = context.getSharedPreferences(Constans.KeySharedPreference.LOG_ID, Context.MODE_PRIVATE).edit();
                editor.putString(Constans.KEY_LOG_ID, listpersonalPending.get(position).getLogDataId());
                editor.apply();
                editor.commit();
                intent.putExtra(Constans.KEY_IS_NEW, false);
                intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, listpersonalPending.get(position).getPersonalOrderId());
                context.startActivity(intent);

            }
        });
        return convertView;
    }

    static class ViewHolder {
        TextView personalDatePendingTextField;
        TextView personalNamePendingTextField;
        TextView personalNo_idPendingTextField;
        TextView personal_comment;
        Button buttonDetail;
        Button buttonEdit;
        Button buttonViewMoreComment;
    }

    @Override
    public int getCount() {return listpersonalPending.size();}

    @Override
    public Object getItem(int arg0) {return listpersonalPending.get(arg0);}

    @Override
    public long getItemId(int arg0) {return 0;}

}
