package com.example.a0426611017.carsurvey.Model.DDL;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by c201017001 on 08/02/2018.
 */

public class ModelDDLCompany implements Serializable{

    @SerializedName("COMPANY_MODEL")
    private List<String> companyModel;

    @SerializedName("COMPANY_TYPE")
    private List<String> companyType;

    @SerializedName("DEBITOR_BUSINESS_SCALE")
    private List<String> debitorBusinessScale;

    @SerializedName("ADDRESS_TYPE_NAME")
    private List<String> addressTypeName;

    @SerializedName("BUILDING_LOCATION_CLASS")
    private List<String> buildingLocationClass;

    @SerializedName("BUILDING_OWNERSHIP")
    private List<String> buildingOwnerShip;

    public List<String> getCompanyModel() {
        return companyModel;
    }

    public void setCompanyModel(List<String> companyModel) {
        this.companyModel = companyModel;
    }

    public List<String> getCompanyType() {
        return companyType;
    }

    public void setCompanyType(List<String> companyType) {
        this.companyType = companyType;
    }

    public List<String> getDebitorBusinessScale() {
        return debitorBusinessScale;
    }

    public void setDebitorBusinessScale(List<String> debitorBusinessScale) {
        this.debitorBusinessScale = debitorBusinessScale;
    }

    public List<String> getAddressTypeName() {
        return addressTypeName;
    }

    public void setAddressTypeName(List<String> addressTypeName) {
        this.addressTypeName = addressTypeName;
    }

    public List<String> getBuildingLocationClass() {
        return buildingLocationClass;
    }

    public void setBuildingLocationClass(List<String> buildingLocationClass) {
        this.buildingLocationClass = buildingLocationClass;
    }

    public List<String> getBuildingOwnerShip() {
        return buildingOwnerShip;
    }

    public void setBuildingOwnerShip(List<String> buildingOwnerShip) {
        this.buildingOwnerShip = buildingOwnerShip;
    }
}
