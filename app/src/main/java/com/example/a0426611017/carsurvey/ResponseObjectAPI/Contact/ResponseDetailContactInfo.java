package com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact;

import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 26/02/2018.
 */

public class ResponseDetailContactInfo {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDetailContactInfo modelDetailContactInfo;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDetailContactInfo getModelDetailContactInfo() {
        return modelDetailContactInfo;
    }

    public void setModelDetailContactInfo(ModelDetailContactInfo modelDetailContactInfo) {
        this.modelDetailContactInfo = modelDetailContactInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
