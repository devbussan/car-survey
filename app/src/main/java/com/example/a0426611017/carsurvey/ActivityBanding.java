package com.example.a0426611017.carsurvey;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiBanding;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.ModelBanding;
import com.google.gson.Gson;

public class ActivityBanding extends AppCompatActivity implements View.OnClickListener {

    private EditText editTextComment;
    private TextView textViewComment;
    private Button buttonSave, buttonBack;

    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private ApiBanding apiBanding;
    private View currentView;
    private ModelBanding modelBanding = new ModelBanding();

    private String adminId = null;
    private String sharedLogId = null;
    private String statusBanding = null;
    private String OrderId = null;
    private String name = null;
    private int menuCategory = 0;
    private int menu;

    private Gson gson = new Gson();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_banding);
        Window window = getWindow();
        currentView = this.findViewById(android.R.id.content);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading.........");
        loadingDialog.setCancelable(false);

        apiBanding = new ApiBanding(currentView, ActivityBanding.this, loadingDialog);

        adminId = this.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);
        menu = getIntent().getIntExtra(Constans.KEY_MENU, 0);
        sharedLogId = this.getSharedPreferences(Constans.KeySharedPreference.LOG_ID, Context.MODE_PRIVATE).getString(Constans.KEY_LOG_ID, null);
        menuCategory = this.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, Context.MODE_PRIVATE).getInt(Constans.KEY_MENU_CATEGORY, 0);

        switch (menu) {
            case Constans.MENU_PERSONAL:
                window.setStatusBarColor(getResources().getColor(R.color.color_personal));
                OrderId = getIntent().getStringExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID);
                name = "personal";
                break;
            case Constans.MENU_COMPANY:
                window.setStatusBarColor(getResources().getColor(R.color.color_company));
                OrderId = getIntent().getStringExtra(Constans.KeySharedPreference.COMPANY_ORDER_ID);
                name = "company";
                break;
        }


        editTextComment = findViewById(R.id.edit_text_comment);
        textViewComment = findViewById(R.id.text_view_comment);
        buttonSave = findViewById(R.id.button_send_banding);
        buttonBack = findViewById(R.id.button_back_banding);
        if(menu == Constans.MENU_PERSONAL){
            textViewComment.setTextColor(getResources().getColor(R.color.color_personal));
            buttonSave.setTextColor(getResources().getColor(R.color.color_white));
            buttonSave.setBackground(getDrawable(R.drawable.ripple_effect_all_personal_button));
            buttonBack.setTextColor(getResources().getColor(R.color.color_white));
            buttonBack.setBackground(getDrawable(R.drawable.ripple_effect_all_personal_button));
            editTextComment.setBackground(getDrawable(R.drawable.border_comment_banding_personal));
        } else if (menu == Constans.MENU_COMPANY){
            textViewComment.setTextColor(getResources().getColor(R.color.color_company));
            buttonSave.setTextColor(getResources().getColor(R.color.color_white));
            buttonSave.setBackground(getDrawable(R.drawable.ripple_effect_all_company_button));
            buttonBack.setTextColor(getResources().getColor(R.color.color_white));
            buttonBack.setBackground(getDrawable(R.drawable.ripple_effect_all_company_button));
            editTextComment.setBackground(getDrawable(R.drawable.border_comment_banding_company));
        }
        buttonSave.setOnClickListener(this);

        buttonBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_send_banding:
                loadingDialog.show();
                modelBanding = getComplete();
                if (InternetConnection.checkConnection(this)) {
                    Log.e("info", "return "+modelBanding.isComplete());
                    if(modelBanding.isComplete()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityBanding.this);
                        builder.setTitle(getString(R.string.content_question_are_you_sure_to_save));
                        builder.setMessage(getString(R.string.content_information_save_and_next));
                        builder.setNegativeButton(getString(R.string.dialog_no),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        loadingDialog.dismiss();
                                        Log.e("info", "NO");
                                    }
                                });
                        builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Log.e("info", "YES");

                                if (menuCategory == Constans.MENU_COMPANY_ORDER_FORM ||
                                        menuCategory == Constans.MENU_PERSONAL_ORDER_FORM){
                                    statusBanding = String.valueOf(Constans.STATUS_BANDING_ORDER);
                                }else if (menuCategory == Constans.MENU_COMPANY_FULL_FORM ||
                                        menuCategory == Constans.MENU_PERSONAL_FULL_FORM){
                                    statusBanding = String.valueOf(Constans.STATUS_BANDING_FULL);
                                }

                                modelBanding.setCmocrt(adminId);
                                modelBanding.setProcessid(statusBanding);

                                apiBanding.sendCommentBanding(modelBanding,
                                        name,
                                        sharedLogId, OrderId
                                );
                            }
                        });
                        builder.show();
                        loadingDialog.dismiss();
                    } else {
                        if(!modelBanding.isComplete()){
                            Snackbar.make(findViewById(R.id.layout_banding), "Please Fill The Data First",
                                    Snackbar.LENGTH_LONG)
                                    .show();
                            loadingDialog.dismiss();
                        }
                    }
                } else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityBanding.this);
                    builder.setTitle(getString(R.string.content_question_internet_error));
                    builder.setMessage(getString(R.string.content_information_cant_connect_server));
                    builder.setCancelable(false);
                    builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            buttonSave.callOnClick();
                            Log.e("info", "OK");
                        }
                    });
                    builder.show();
                }
                break;
            case R.id.button_back_banding:
                AlertDialog.Builder b = new AlertDialog.Builder(ActivityBanding.this);
                b.setTitle(getString(R.string.content_question_are_you_sure));
                b.setMessage(getString(R.string.content_information_data_will_lost));
                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ActivityBanding.super.onBackPressed();
                        finish();
                    }
                });
                b.show();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder b = new AlertDialog.Builder(ActivityBanding.this);
        b.setTitle(getString(R.string.content_question_are_you_sure));
        b.setMessage(getString(R.string.content_information_data_will_lost));
        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                ActivityBanding.super.onBackPressed();
                finish();
            }
        });
        b.show();
    }

    public void setForm(ModelBanding modelBanding) {
        editTextComment.setText(modelBanding.getComment());
    }

    public ModelBanding getForm(){
        ModelBanding modelBanding = new ModelBanding();

        modelBanding.setComment(editTextComment.getText().toString());

        return modelBanding;
    }

    public ModelBanding getComplete() {
        ModelBanding modelBanding = getForm();

        modelBanding.setComplete(true);
        if (modelBanding.getComment().length() == 0) {
            editTextComment.setError("Comment is Required");
            modelBanding.setComplete(false);
        } else if (modelBanding.getComment().trim().length() == 0) {
            editTextComment.setError("Comment not allowed whitespaces only");
            modelBanding.setComplete(false);
        }

        return modelBanding;
    }
}
