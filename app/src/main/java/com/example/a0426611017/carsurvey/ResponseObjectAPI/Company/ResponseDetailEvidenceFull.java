package com.example.a0426611017.carsurvey.ResponseObjectAPI.Company;

import com.example.a0426611017.carsurvey.Model.Company.ModelMandatoryInfo;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 03/03/2018.
 */

public class ResponseDetailEvidenceFull {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelMandatoryInfo modelMandatoryInfo;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelMandatoryInfo getModelMandatoryInfo() {
        return modelMandatoryInfo;
    }

    public void setModelMandatoryInfo(ModelMandatoryInfo modelMandatoryInfo) {
        this.modelMandatoryInfo = modelMandatoryInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
