package com.example.a0426611017.carsurvey.Company.FullForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.a0426611017.carsurvey.Finance.ActivityCompanyFinancial;
import com.example.a0426611017.carsurvey.Fragment.Company.FullForm.FragmentAddressInfoPending;
import com.example.a0426611017.carsurvey.Fragment.Company.FullForm.FragmentCompanyInfoPending;
import com.example.a0426611017.carsurvey.Fragment.Company.FullForm.FragmentMandatoryPending;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentContactInfo;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentContactInfo2;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentContactInfo3;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentContactInfo4;
import com.example.a0426611017.carsurvey.Fragment.Company.MiniForm.FragmentContactInfo5;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiCompanyOrder;
import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiDDL;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.ModelAddressInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelCompanyInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelMandatoryInfo;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLCompany;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDDLCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseContactOrderId;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCompanyFullEdit extends AppCompatActivity {
    Button buttonCompany, buttonAddress, buttonMandatory,
            buttonContact, buttonContact2, buttonContact3, buttonContact4, buttonContact5,
            buttonNext, buttonBack, buttonSave;

    private boolean isConnect = false;

    private Fragment currentFragment;

    private ApiInterface apiInterface;

    private FragmentCompanyInfoPending fragmentCompanyInfoPending = new FragmentCompanyInfoPending();
    private FragmentAddressInfoPending fragmentAddressInfoPending = new FragmentAddressInfoPending();
    private FragmentMandatoryPending fragmentMandatoryPending = new FragmentMandatoryPending();
    private FragmentContactInfo fragmentContactInfo = new FragmentContactInfo();
    private FragmentContactInfo2 fragmentContactInfo2 = new FragmentContactInfo2();
    private FragmentContactInfo3 fragmentContactInfo3 = new FragmentContactInfo3();
    private FragmentContactInfo4 fragmentContactInfo4 = new FragmentContactInfo4();
    private FragmentContactInfo5 fragmentContactInfo5 = new FragmentContactInfo5();

    private ModelCompanyInfo modelCompanyInfo = new ModelCompanyInfo();
    private ModelAddressInfo modelAddressInfo = new ModelAddressInfo();
    private ModelDetailContactInfo modelDetailContactInfo = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo2 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo3 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo4 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo5 = new ModelDetailContactInfo();
    private ModelMandatoryInfo modelMandatoryInfo = new ModelMandatoryInfo();
    private boolean isNew = true;

    private ResponseContactOrderId responseContactOrderId = new ResponseContactOrderId();
    private ModelDDLCompany ddlCompanyList = null;
    private ProgressDialog loadingDialog;

    private ApiDDL apiDDL;
    private ApiCompanyOrder apiCompanyOrder;

    private List<String> contactOrder = new ArrayList<>();

    private Gson gson = new Gson();

    private String adminId;
    private String companyOrderId;
    private String custDataId = "0";
    private String sharedLogId = null;

    private int menuCategory = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_company_full_edit);
        window.setStatusBarColor(getResources().getColor(R.color.color_company));
        final View currentView = this.findViewById(android.R.id.content);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        apiCompanyOrder = new ApiCompanyOrder(currentView, ActivityCompanyFullEdit.this, loadingDialog);
        adminId = this.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);
        sharedLogId = this.getSharedPreferences(Constans.KeySharedPreference.LOG_ID, Context.MODE_PRIVATE).getString(Constans.KEY_LOG_ID, null);
        custDataId = this.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, MODE_PRIVATE).getString(Constans.KEY_CUST_DATA_ID, null);
        menuCategory = this.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, Context.MODE_PRIVATE).getInt(Constans.KEY_MENU_CATEGORY, 0);

        companyOrderId = getIntent().getStringExtra(Constans.KeySharedPreference.COMPANY_ORDER_ID);

        getContactOrderId(companyOrderId);
        getDDLCompany();

        Log.d("Company Order", companyOrderId);
        Log.d("Admin Id", adminId);

        isNew = getIntent().getBooleanExtra(Constans.KEY_IS_NEW, true);

        buttonCompany = (Button) findViewById(R.id.button_company_pending);
        buttonCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buttonCompany.setError(null);

                buttonCompany.setEnabled(false);
                buttonAddress.setEnabled(true);
                buttonMandatory.setEnabled(true);
                buttonContact.setEnabled(true);
                buttonContact2.setEnabled(true);
                buttonContact3.setEnabled(true);
                buttonContact4.setEnabled(true);
                buttonContact5.setEnabled(true);

                buttonCompany.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonCompany.setTextColor(getResources().getColor(R.color.color_company));

                buttonAddress.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonAddress.setTextColor(getResources().getColor(R.color.color_white));

                buttonMandatory.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonMandatory.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact5.setTextColor(getResources().getColor(R.color.color_white));

                if (ddlCompanyList == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLCompany();
                            buttonCompany.callOnClick();
                        }
                    });
                    b.show();
                } else {
                    Log.d("ddlCompanyList", gson.toJson(ddlCompanyList));

                    fragmentCompanyInfoPending = fragmentCompanyInfoPending.newInstance(modelCompanyInfo, ddlCompanyList, companyOrderId);
                    saveForm();
                    loadFragment(fragmentCompanyInfoPending);
                }
            }
        });

        buttonAddress = (Button) findViewById(R.id.button_address_pending);
        buttonAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAddress.setError(null);

                buttonCompany.setEnabled(true);
                buttonAddress.setEnabled(false);
                buttonMandatory.setEnabled(true);
                buttonContact.setEnabled(true);
                buttonContact2.setEnabled(true);
                buttonContact3.setEnabled(true);
                buttonContact4.setEnabled(true);
                buttonContact5.setEnabled(true);

                buttonCompany.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompany.setTextColor(getResources().getColor(R.color.color_white));

                buttonAddress.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonAddress.setTextColor(getResources().getColor(R.color.color_company));

                buttonMandatory.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonMandatory.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact5.setTextColor(getResources().getColor(R.color.color_white));

                if (ddlCompanyList == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLCompany();
                            buttonAddress.callOnClick();
                        }
                    });
                    b.show();
                } else {

                    fragmentAddressInfoPending = fragmentAddressInfoPending.newInstance(modelAddressInfo, ddlCompanyList, custDataId);
                    saveForm();
                    loadFragment(fragmentAddressInfoPending);
                }


            }
        });

        buttonMandatory = (Button) findViewById(R.id.button_mandatory_pending);
        buttonMandatory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonMandatory.setError(null);

                buttonCompany.setEnabled(true);
                buttonAddress.setEnabled(true);
                buttonMandatory.setEnabled(false);
                buttonContact.setEnabled(true);
                buttonContact2.setEnabled(true);
                buttonContact3.setEnabled(true);
                buttonContact4.setEnabled(true);
                buttonContact5.setEnabled(true);

                buttonCompany.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompany.setTextColor(getResources().getColor(R.color.color_white));

                buttonAddress.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonAddress.setTextColor(getResources().getColor(R.color.color_white));

                buttonMandatory.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonMandatory.setTextColor(getResources().getColor(R.color.color_company));

                buttonContact.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact5.setTextColor(getResources().getColor(R.color.color_white));

                if (ddlCompanyList == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLCompany();
                            buttonMandatory.callOnClick();
                        }
                    });
                    b.show();
                } else {
                    fragmentMandatoryPending = fragmentMandatoryPending.newInstance(modelMandatoryInfo, ddlCompanyList, custDataId);
                    saveForm();
                    loadFragment(fragmentMandatoryPending);
                }


            }
        });

        buttonContact = (Button) findViewById(R.id.button_contact_pending);
        buttonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContact.setError(null);

                buttonCompany.setEnabled(true);
                buttonAddress.setEnabled(true);
                buttonMandatory.setEnabled(true);
                buttonContact.setEnabled(false);
                buttonContact2.setEnabled(true);
                buttonContact3.setEnabled(true);
                buttonContact4.setEnabled(true);
                buttonContact5.setEnabled(true);

                buttonCompany.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompany.setTextColor(getResources().getColor(R.color.color_white));

                buttonAddress.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonAddress.setTextColor(getResources().getColor(R.color.color_white));

                buttonMandatory.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonMandatory.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContact.setTextColor(getResources().getColor(R.color.color_company));

                buttonContact2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact5.setTextColor(getResources().getColor(R.color.color_white));

                String contactOrderId = null;
                if (contactOrder.size() > 0) {
                    contactOrderId = contactOrder.get(0);
                }
                fragmentContactInfo = fragmentContactInfo.newInstance(modelDetailContactInfo, contactOrderId, companyOrderId, false);
                saveForm();
                loadFragment(fragmentContactInfo);


            }
        });

        buttonContact2 = (Button) findViewById(R.id.button_contact2_pending);
        buttonContact2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContact2.setError(null);

                buttonCompany.setEnabled(true);
                buttonAddress.setEnabled(true);
                buttonMandatory.setEnabled(true);
                buttonContact.setEnabled(true);
                buttonContact2.setEnabled(false);
                buttonContact3.setEnabled(true);
                buttonContact4.setEnabled(true);
                buttonContact5.setEnabled(true);

                buttonCompany.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompany.setTextColor(getResources().getColor(R.color.color_white));

                buttonAddress.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonAddress.setTextColor(getResources().getColor(R.color.color_white));

                buttonMandatory.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonMandatory.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact2.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContact2.setTextColor(getResources().getColor(R.color.color_company));

                buttonContact3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact5.setTextColor(getResources().getColor(R.color.color_white));


                String contactOrderId = null;
                if (contactOrder.size() > 1) {
                    contactOrderId = contactOrder.get(1);
                }
                fragmentContactInfo2 = fragmentContactInfo2.newInstance(modelDetailContactInfo2, contactOrderId, companyOrderId, false);
                saveForm();
                loadFragment(fragmentContactInfo2);


            }
        });

        buttonContact3 = (Button) findViewById(R.id.button_contact3_pending);
        buttonContact3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContact3.setError(null);

                buttonCompany.setEnabled(true);
                buttonAddress.setEnabled(true);
                buttonMandatory.setEnabled(true);
                buttonContact.setEnabled(true);
                buttonContact2.setEnabled(true);
                buttonContact3.setEnabled(false);
                buttonContact4.setEnabled(true);
                buttonContact5.setEnabled(true);

                buttonCompany.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompany.setTextColor(getResources().getColor(R.color.color_white));

                buttonAddress.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonAddress.setTextColor(getResources().getColor(R.color.color_white));

                buttonMandatory.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonMandatory.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact3.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContact3.setTextColor(getResources().getColor(R.color.color_company));

                buttonContact4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact5.setTextColor(getResources().getColor(R.color.color_white));



                String contactOrderId = null;
                if (contactOrder.size() > 2) {
                    contactOrderId = contactOrder.get(2);
                }
                fragmentContactInfo3 = fragmentContactInfo3.newInstance(modelDetailContactInfo3, contactOrderId, companyOrderId, false);
                saveForm();
                loadFragment(fragmentContactInfo3);


            }
        });

        buttonContact4 = (Button) findViewById(R.id.button_contact4_pending);
        buttonContact4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContact4.setError(null);

                buttonCompany.setEnabled(true);
                buttonAddress.setEnabled(true);
                buttonMandatory.setEnabled(true);
                buttonContact.setEnabled(true);
                buttonContact2.setEnabled(true);
                buttonContact3.setEnabled(true);
                buttonContact4.setEnabled(false);
                buttonContact5.setEnabled(true);

                buttonCompany.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompany.setTextColor(getResources().getColor(R.color.color_white));

                buttonAddress.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonAddress.setTextColor(getResources().getColor(R.color.color_white));

                buttonMandatory.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonMandatory.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact4.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContact4.setTextColor(getResources().getColor(R.color.color_company));

                buttonContact5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact5.setTextColor(getResources().getColor(R.color.color_white));


                String contactOrderId = null;
                if (contactOrder.size() > 3) {
                    contactOrderId = contactOrder.get(3);
                }
                fragmentContactInfo4 = fragmentContactInfo4.newInstance(modelDetailContactInfo4, contactOrderId, companyOrderId, false);
                saveForm();
                loadFragment(fragmentContactInfo4);


            }
        });

        buttonContact5 = (Button) findViewById(R.id.button_contact5_pending);
        buttonContact5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContact5.setError(null);

                buttonCompany.setEnabled(true);
                buttonAddress.setEnabled(true);
                buttonMandatory.setEnabled(true);
                buttonContact.setEnabled(true);
                buttonContact2.setEnabled(true);
                buttonContact3.setEnabled(true);
                buttonContact4.setEnabled(true);
                buttonContact5.setEnabled(false);

                buttonCompany.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompany.setTextColor(getResources().getColor(R.color.color_white));

                buttonAddress.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonAddress.setTextColor(getResources().getColor(R.color.color_white));

                buttonMandatory.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonMandatory.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact2.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact3.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact4.setTextColor(getResources().getColor(R.color.color_white));

                buttonContact5.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContact5.setTextColor(getResources().getColor(R.color.color_company));


                String contactOrderId = null;
                if (contactOrder.size() > 4) {
                    contactOrderId = contactOrder.get(4);
                }
                fragmentContactInfo5 = fragmentContactInfo5.newInstance(modelDetailContactInfo5, contactOrderId, companyOrderId, false);
                saveForm();
                loadFragment(fragmentContactInfo5);

            }
        });

        buttonSave = findViewById(R.id.button_save_company);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                builder.setTitle(getString(R.string.content_question_want_save_data));
                builder.setMessage(getString(R.string.content_information_save_and_exit));
                builder.setNegativeButton(getString(R.string.dialog_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                loadingDialog.dismiss();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        loadingDialog.show();
                        saveForm();

                        modelCompanyInfo.setUserCreate(adminId);
                        modelAddressInfo.setUserCreate(adminId);
                        modelMandatoryInfo.setUserCreate(adminId);

                        List<ModelDetailContactInfo> modelContactInfoMinis = new ArrayList<>();
                        ModelDetailContactInfo contactInfoMiniTemp = new ModelDetailContactInfo();

                        for (int i = 0; i < 5; i++) {
                            switch (i) {
                                case 0:
                                    contactInfoMiniTemp = modelDetailContactInfo;
                                    break;
                                case 1:
                                    contactInfoMiniTemp = modelDetailContactInfo2;
                                    break;
                                case 2:
                                    contactInfoMiniTemp = modelDetailContactInfo3;
                                    break;
                                case 3:
                                    contactInfoMiniTemp = modelDetailContactInfo4;
                                    break;
                                case 4:
                                    contactInfoMiniTemp = modelDetailContactInfo5;
                                    break;
                                default:
                                    break;
                            }

                            if (contactInfoMiniTemp.isModified()) {
                                if(contactOrder.size() > i){
                                    contactInfoMiniTemp.setContactOrderId(contactOrder.get(i));
                                }else{
                                    contactInfoMiniTemp.setContactOrderId(null);
                                }

                                contactInfoMiniTemp.setInfoFlag(String.valueOf(i+1));
                                modelContactInfoMinis.add(contactInfoMiniTemp);
                            }
                        }

                        apiCompanyOrder.saveFullForm(modelCompanyInfo, companyOrderId,
                                modelAddressInfo,
                                modelMandatoryInfo,
                                modelContactInfoMinis,
                                contactOrder,
                                custDataId,
                                false,
                                sharedLogId);
                    }
                });
                builder.show();

            }
        });

        buttonNext = findViewById(R.id.button_next_company);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                saveForm();
                checkIsComplete();

                if (modelCompanyInfo.isComplete()
                        && modelAddressInfo.isComplete()
                        && modelMandatoryInfo.isComplete()
                        && modelDetailContactInfo.isComplete()
                        && modelDetailContactInfo2.isComplete()
                        ) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                    builder.setTitle(getString(R.string.content_question_are_you_sure_to_save));
                    builder.setMessage(getString(R.string.content_information_save_and_next));
                    builder.setNegativeButton(getString(R.string.dialog_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    loadingDialog.dismiss();
                                    Log.e("info", "NO");
                                }
                            });
                    builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int j) {
                            modelCompanyInfo.setUserCreate(adminId);
                            modelAddressInfo.setUserCreate(adminId);
                            modelMandatoryInfo.setUserCreate(adminId);

                            List<ModelDetailContactInfo> modelContactInfoMinis = new ArrayList<>();
                            ModelDetailContactInfo contactInfoMiniTemp = new ModelDetailContactInfo();

                            for (int i = 0; i < 5; i++) {
                                switch (i) {
                                    case 0:
                                        contactInfoMiniTemp = modelDetailContactInfo;
                                        break;
                                    case 1:
                                        contactInfoMiniTemp = modelDetailContactInfo2;
                                        break;
                                    case 2:
                                        contactInfoMiniTemp = modelDetailContactInfo3;
                                        break;
                                    case 3:
                                        contactInfoMiniTemp = modelDetailContactInfo4;
                                        break;
                                    case 4:
                                        contactInfoMiniTemp = modelDetailContactInfo5;
                                        break;
                                    default:
                                        break;
                                }

                                if (contactInfoMiniTemp.isModified()) {
                                    if(contactOrder.size() > i){
                                        contactInfoMiniTemp.setContactOrderId(contactOrder.get(i));
                                    }else{
                                        contactInfoMiniTemp.setContactOrderId(null);
                                    }

                                    contactInfoMiniTemp.setInfoFlag(String.valueOf(i+1));
                                    modelContactInfoMinis.add(contactInfoMiniTemp);
                                }
                            }

                            apiCompanyOrder.saveFullForm(modelCompanyInfo, companyOrderId,
                                    modelAddressInfo,
                                    modelMandatoryInfo,
                                    modelContactInfoMinis,
                                    contactOrder,
                                    custDataId,
                                    true,
                                    sharedLogId);
                        }
                    });
                    builder.show();
                    loadingDialog.dismiss();
                } else {
                    if (!modelCompanyInfo.isComplete()) {
                        if (buttonCompany.isEnabled())
                            buttonCompany.setError("Not Complete");
                    }
                    if (!modelAddressInfo.isComplete()) {
                        if (buttonAddress.isEnabled())
                            buttonAddress.setError("Not Complete");
                    }
                    if (!modelMandatoryInfo.isComplete()) {
                        if (buttonMandatory.isEnabled())
                            buttonMandatory.setError("Not Complete");
                    }
                    if (!modelDetailContactInfo.isComplete()) {
                        if (buttonContact.isEnabled())
                            buttonContact.setError("Not Complete");
                    }
                    if (!modelDetailContactInfo2.isComplete()) {
                        if (buttonContact2.isEnabled())
                            buttonContact2.setError("Not Complete");
                    }
                    if (!modelDetailContactInfo3.isComplete()) {
                        if (buttonContact3.isEnabled())
                            buttonContact3.setError("Not Complete");
                    }
                    if (!modelDetailContactInfo4.isComplete()) {
                        if (buttonContact4.isEnabled())
                            buttonContact4.setError("Not Complete");
                    }
                    if (!modelDetailContactInfo5.isComplete()) {
                        if (buttonContact5.isEnabled())
                            buttonContact5.setError("Not Complete");
                    }
                    loadingDialog.dismiss();
                    Snackbar.make(findViewById(R.id.myLayoutCompany), "Please fill all form",
                            Snackbar.LENGTH_LONG)
                            .show();
                }
            }
        });

        buttonBack = findViewById(R.id.button_back_company);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                b.setTitle(R.string.content_information_back_pressed);
                b.setMessage(getString(R.string.content_question_want_save_data)+
                        "\n"+getString(R.string.content_information_press_yes_save));
                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompanyFullEdit.super.onBackPressed();

                        Intent i = new Intent(ActivityCompanyFullEdit.this, MainActivityDrawer.class);
                        i.putExtra(Constans.KEY_MENU, menuCategory);
                        i.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                        startActivity(i);
                        finish();
                    }
                });
                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        buttonSave.callOnClick();
                    }
                });
                b.show();
            }
        });
    }

    private void saveForm() {
        if (InternetConnection.checkConnection(this)) {
            isConnect = true;
            if (currentFragment != null) {
                if (currentFragment.getClass() == fragmentCompanyInfoPending.getClass()) {

                    modelCompanyInfo = fragmentCompanyInfoPending.getForm();

                } else if (currentFragment.getClass() == fragmentAddressInfoPending.getClass()) {

                    modelAddressInfo = fragmentAddressInfoPending.getForm();

                } else if (currentFragment.getClass() == fragmentMandatoryPending.getClass()) {

                    modelMandatoryInfo = fragmentMandatoryPending.getForm();

                } else if (currentFragment.getClass() == fragmentContactInfo.getClass()) {

                    modelDetailContactInfo = fragmentContactInfo.getForm();

                } else if (currentFragment.getClass() == fragmentContactInfo2.getClass()) {

                    modelDetailContactInfo2 = fragmentContactInfo2.getForm();

                } else if (currentFragment.getClass() == fragmentContactInfo3.getClass()) {

                    modelDetailContactInfo3 = fragmentContactInfo3.getForm();

                } else if (currentFragment.getClass() == fragmentContactInfo4.getClass()) {

                    modelDetailContactInfo4 = fragmentContactInfo4.getForm();

                } else if (currentFragment.getClass() == fragmentContactInfo5.getClass()) {

                    modelDetailContactInfo5 = fragmentContactInfo5.getForm();

                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    saveForm();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    private void checkIsComplete() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (modelCompanyInfo.isModified()) {
                    modelCompanyInfo = fragmentCompanyInfoPending.getComplete();
                }
                if (modelAddressInfo.isModified()) {
                    modelAddressInfo = fragmentAddressInfoPending.getComplete();
                }
                if (modelMandatoryInfo.isModified()) {
                    modelMandatoryInfo = fragmentMandatoryPending.getComplete();

                }
                if (modelDetailContactInfo.isModified()) {
                    modelDetailContactInfo = fragmentContactInfo.getComplete();

                }
                if (modelDetailContactInfo2.isModified()) {
                    modelDetailContactInfo2 = fragmentContactInfo2.getComplete();

                }
                if (modelDetailContactInfo3.isModified()) {

                    modelDetailContactInfo3 = fragmentContactInfo3.getComplete();

                }
                if (modelDetailContactInfo4.isModified()) {

                    modelDetailContactInfo4 = fragmentContactInfo4.getComplete();

                }
                if (modelDetailContactInfo5.isModified()) {

                    modelDetailContactInfo5 = fragmentContactInfo5.getComplete();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    checkIsComplete();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    private void setDDL(ModelDDLCompany ddlCompanyList) {
        this.ddlCompanyList = ddlCompanyList;
    }

    private void getContactOrderId(final String companyOrderId) {
        loadingDialog.show();
        if (InternetConnection.checkConnection(this)) {
            final Call<ResponseContactOrderId> contactOrderID = apiInterface.getContactOrderId(companyOrderId);
            contactOrderID.enqueue(new Callback<ResponseContactOrderId>() {
                @Override
                public void onResponse(Call<ResponseContactOrderId> call, Response<ResponseContactOrderId>
                        response) {
                    if (response.isSuccessful()) {
                        responseContactOrderId = response.body();
                        Log.d("Contact Order Id", String.valueOf(responseContactOrderId.getModelContactOrderId().getContactInfo()));
                        contactOrder = responseContactOrderId.getModelContactOrderId().getContactInfo();
                        for (int i = 0; i < contactOrder.size(); i++) {
                            switch (i) {
                                case 0:
                                    buttonContact.setVisibility(View.VISIBLE);
                                    break;
                                case 1:
                                    buttonContact2.setVisibility(View.VISIBLE);
                                    break;
                                case 2:
                                    buttonContact3.setVisibility(View.VISIBLE);
                                    break;
                                case 3:
                                    buttonContact4.setVisibility(View.VISIBLE);
                                    break;
                                case 4:
                                    buttonContact5.setVisibility(View.VISIBLE);
                                    break;
                                default:
                                    break;
                            }
                        }
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseContactOrderId = gson.fromJson(response.errorBody().string(), ResponseContactOrderId.class);
                            error = responseContactOrderId.getMessage();

                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                                b.setTitle(getString(R.string.content_question_internet_error));
                                b.setMessage(getString(R.string.content_information_cant_connect_server));
                                b.setCancelable(false);
                                b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        getContactOrderId(companyOrderId);
                                    }
                                });
                                b.show();
                            }else
                                Snackbar.make(findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                            builder.setTitle(getString(R.string.content_question_internet_error));
                            builder.setMessage(getString(R.string.content_information_cant_connect_server));
                            builder.setCancelable(false);
                            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getContactOrderId(companyOrderId);
                                    Log.e("info", "OK");
                                }
                            });
                            builder.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                        builder.setTitle(getString(R.string.content_question_internet_error));
                        builder.setMessage(getString(R.string.content_information_cant_connect_server));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getContactOrderId(companyOrderId);
                                Log.e("info", "OK");
                            }
                        });
                        builder.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseContactOrderId> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                    builder.setTitle(getString(R.string.content_question_internet_error));
                    builder.setMessage(getString(R.string.content_information_cant_connect_server));
                    builder.setCancelable(false);
                    builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            getContactOrderId(companyOrderId);
                            Log.e("info", "OK");
                        }
                    });
                    builder.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    getContactOrderId(companyOrderId);
                    Log.e("info", "OK");
                }
            });
            builder.show();
            loadingDialog.dismiss();
        }
    }

    public void getDDLCompany() {
        loadingDialog.show();
        if (InternetConnection.checkConnection(this)) {
            Call<ResponseDDLCompany> ddlCompany = apiInterface.getDDLCompany();
            ddlCompany.enqueue(new Callback<ResponseDDLCompany>() {
                @Override
                public void onResponse(Call<ResponseDDLCompany> call, Response<ResponseDDLCompany>
                        response) {
                    if (response.isSuccessful()) {
                        setDDL(response.body().getDdlCompanies());
                        loadingDialog.dismiss();
                    } else {
                        Snackbar.make(findViewById(R.id.myLayoutCompany), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDDLCompany> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    Snackbar.make(findViewById(R.id.myLayoutCompany), t.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(R.id.myLayoutCompany), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            loadingDialog.dismiss();
        }
    }

    private void loadFragment(Fragment fragment) {
        currentFragment = fragment;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frame_company_pending, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        buttonBack = findViewById(R.id.button_back_company);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyFullEdit.this);
                b.setTitle(R.string.content_information_back_pressed);
                b.setMessage(getString(R.string.content_question_want_save_data)+
                        "\n"+getString(R.string.content_information_press_yes_save));
                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(ActivityCompanyFullEdit.this, MainActivityDrawer.class);
                        i.putExtra(Constans.KEY_MENU, menuCategory);
                        i.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                        startActivity(i);
                        finish();
                    }
                });
                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        buttonSave.callOnClick();
                    }
                });
                b.show();
            }
        });
    }
}
