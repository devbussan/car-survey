package com.example.a0426611017.carsurvey.Model.Personal.Mini;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 0426591017 on 2/19/2018.
 */

public class ModelPersonalSGMini implements Serializable {
    @SerializedName("PERSONALORDERID")
    private String personalorderid;

    @SerializedName("PERSONAL_SG_ID")
    private String personal_sg_id;

    @SerializedName("FULLNAMESG")
    private String fullnamesg;

    @SerializedName("IDTYPESG")
    private String idtypesg;

    @SerializedName("IDNOSG")
    private String idnosg;

    @SerializedName("IDIMGURLSG")
    private String idimgurlsg;

    private String idimgsg;

    @SerializedName("KKNOSG")
    private String kknosg;

    @SerializedName("KKIMGURLSG")
    private String kkimgurlsg;

    private String kkimgsg;

    @SerializedName("USRCRT")
    private String usrcrt;

    @SerializedName("PERSONAL_SPOUSE_ORDER_RTN_ID")
    private String personal_spouse_order_rtn_id;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getIdimgsg() {
        return idimgsg;
    }

    public void setIdimgsg(String idimgsg) {
        this.idimgsg = idimgsg;
    }

    public String getKkimgsg() {
        return kkimgsg;
    }

    public void setKkimgsg(String kkimgsg) {
        this.kkimgsg = kkimgsg;
    }

    public String getPersonalorderid() {
        return personalorderid;
    }

    public void setPersonalorderid(String personalorderid) {
        this.personalorderid = personalorderid;
    }

    public String getPersonal_sg_id() {
        return personal_sg_id;
    }

    public void setPersonal_sg_id(String personal_sg_id) {
        this.personal_sg_id = personal_sg_id;
    }

    public String getFullnamesg() {
        return fullnamesg;
    }

    public void setFullnamesg(String fullnamesg) {
        this.fullnamesg = fullnamesg;
    }

    public String getIdtypesg() {
        return idtypesg;
    }

    public void setIdtypesg(String idtypesg) {
        this.idtypesg = idtypesg;
    }

    public String getIdnosg() {
        return idnosg;
    }

    public void setIdnosg(String idnosg) {
        this.idnosg = idnosg;
    }

    public String getIdimgurlsg() {
        return idimgurlsg;
    }

    public void setIdimgurlsg(String idimgurlsg) {
        this.idimgurlsg = idimgurlsg;
    }

    public String getKknosg() {
        return kknosg;
    }

    public void setKknosg(String kknosg) {
        this.kknosg = kknosg;
    }

    public String getKkimgurlsg() {
        return kkimgurlsg;
    }

    public void setKkimgurlsg(String kkimgurlsg) {
        this.kkimgurlsg = kkimgurlsg;
    }

    public String getUsrcrt() {
        return usrcrt;
    }

    public void setUsrcrt(String usrcrt) {
        this.usrcrt = usrcrt;
    }

    public String getPersonal_spouse_order_rtn_id() {
        return personal_spouse_order_rtn_id;
    }

    public void setPersonal_spouse_order_rtn_id(String personal_spouse_order_rtn_id) {
        this.personal_spouse_order_rtn_id = personal_spouse_order_rtn_id;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
