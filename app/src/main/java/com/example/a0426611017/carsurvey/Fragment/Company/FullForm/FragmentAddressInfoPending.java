package com.example.a0426611017.carsurvey.Fragment.Company.FullForm;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Model.Company.ModelAddressInfo;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLCompany;
import com.example.a0426611017.carsurvey.Object.Company.FullForm.ObjectAddressInfoFull;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailAddressFull;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by c201017001 on 20/12/2017.
 */

public class FragmentAddressInfoPending extends Fragment {
    private View view;
    private Context context;
    private String nameFile;
    private Button buttonBuildingImage;
    private TextView textViewNameFile;
    private ObjectAddressInfoFull buildingInfo;
    private PreviewImage previewImage = new PreviewImage();
    private FileFunction fileFunction = new FileFunction();
    private TakePicture takePicture = new TakePicture();
    private String custDataId = "0";

    private ModelAddressInfo modelAddressInfo = new ModelAddressInfo();
    private ModelDDLCompany ddlCompany;

    private ResponseDetailAddressFull responseDetailAddressFull = new ResponseDetailAddressFull();

    private Gson gson = new Gson();

    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;

    public FragmentAddressInfoPending newInstance(ModelAddressInfo buildingInfo, ModelDDLCompany ddlCompany, String custDataId) {
        FragmentAddressInfoPending fragmentAddressInfoPending = new FragmentAddressInfoPending();
        Bundle args = new Bundle();
        args.putSerializable("buildingInfo", buildingInfo);
        args.putSerializable("ddlCompany", ddlCompany);
        args.putString("custDataId", custDataId);
        fragmentAddressInfoPending.setArguments(args);
        return fragmentAddressInfoPending;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelAddressInfo = (ModelAddressInfo) getArguments().getSerializable("buildingInfo");
            ddlCompany = (ModelDDLCompany) getArguments().getSerializable("ddlCompany");
            custDataId = getArguments().getString("custDataId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        buildingInfo = new ObjectAddressInfoFull(container, inflater, ddlCompany);
        view = buildingInfo.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if(modelAddressInfo.isModified()){
            buildingInfo.setForm(modelAddressInfo);
        }else if(custDataId != null && !custDataId.equals("0")){
            getDetailAddressFull(custDataId);
        }

        textViewNameFile = buildingInfo.getNameFileBuildingImage();
        buttonBuildingImage = buildingInfo.getButtonBuildingImage();

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }else{
            buttonBuildingImage.setEnabled(true);
        }
//
        callCamera(buttonBuildingImage);


        return view;
    }

    private void callCamera(Button button){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(v);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                buttonBuildingImage.setEnabled(true);
            }
        }
    }

    public void takePicture(View view) {

        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        startActivityForResult(intent, 100);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {

                if(data != null){
                    try {

                        File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                        nameFile = file.getName();

                    } catch (IOException e) {
                        Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                        e.printStackTrace();
                    }

                }
                buildingInfo.setNameFileBuildingImage(nameFile);
                buildingInfo.setPathBiuldingImageHidden(nameFile);

                previewImage.setPreviewImage(nameFile, context, textViewNameFile);
            }
        }
    }

    public ModelAddressInfo getForm(){
        return buildingInfo.getForm();
    }

    public void setForm(ModelAddressInfo modelAddressInfo){
        buildingInfo.setForm(modelAddressInfo);
    }

    public ModelAddressInfo getComplete(){
        return buildingInfo.getComplete();
    }

    private void getDetailAddressFull(final String custDataId) {
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailAddressFull> addressDetail = apiInterface.getDetailFullAddress(custDataId);
            addressDetail.enqueue(new Callback<ResponseDetailAddressFull>() {
                @Override
                public void onResponse(Call<ResponseDetailAddressFull> call, Response<ResponseDetailAddressFull>
                        response) {
                    if (response.isSuccessful()) {
                        modelAddressInfo = response.body().getModelAddressInfo();
                        modelAddressInfo.setModified(true);
                        setForm(modelAddressInfo);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailAddressFull = gson.fromJson(response.errorBody().string(), ResponseDetailAddressFull.class);
                            error = responseDetailAddressFull.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder a = new AlertDialog.Builder(context);
                                a.setTitle(getString(R.string.dialog_error_server));
                                a.setCancelable(false);
                                a.setMessage(error+"\n" + getString(R.string.dialog_try_again));
                                a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailAddressFull(custDataId);
                                    }
                                });

                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();

                            android.app.AlertDialog.Builder a = new android.app.AlertDialog.Builder(context);
                            a.setTitle(getString(R.string.dialog_error_server));
                            a.setCancelable(false);
                            a.setMessage(error+"\n" +
                                    getString(R.string.dialog_try_again));
                            a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailAddressFull(custDataId);
                                }
                            });
                            a.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder a = new AlertDialog.Builder(context);
                        a.setTitle(getString(R.string.dialog_error_server));
                        a.setCancelable(false);
                        a.setMessage("Service response time out\n" +
                                getString(R.string.dialog_try_again));
                        a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailAddressFull(custDataId);
                            }
                        });
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailAddressFull> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder a = new AlertDialog.Builder(context);
                    a.setTitle(getString(R.string.dialog_error_server));
                    a.setCancelable(false);
                    a.setMessage(t.getMessage() +"\n" +
                            getString(R.string.dialog_try_again));
                    a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailAddressFull(custDataId);
                        }
                    });
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder a = new AlertDialog.Builder(context);
            a.setTitle(getString(R.string.dialog_error_server));
            a.setCancelable(false);
            a.setMessage(getString(R.string.dialog_internet_connect)+"\n"+
                    getString(R.string.dialog_try_again));
            a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailAddressFull(custDataId);
                }
            });
            loadingDialog.dismiss();
        }
    }

}
