package com.example.a0426611017.carsurvey.MainFunction.CallApi;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Adapter.Personal.AdapterCustomCompletePersonal;
import com.example.a0426611017.carsurvey.Adapter.Personal.AdapterCustomPendingPersonal;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataAddress;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataEmergencyContact;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataFinancial;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataJobData;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataMainData;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonal;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonalSG;
import com.example.a0426611017.carsurvey.Personal.FullForm.ActivityPersonalSGFullEdit;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelGridListPersonal;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalAddr;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalEmergency;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalFinancial;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalInfo;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalJob;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalOther;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalSgInfo;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseGridListPersonal;
import com.example.a0426611017.carsurvey.Survey.ActivitySurvey;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by c201017001 on 20/02/2018.
 */

public class ApiPersonal {

    private ApiInterface apiInterface;
    private View view;
    private Context context;
    private ProgressDialog loadingDialog;

    private int tab = 0;
    private int menu = 0;
    private GridView gridView;
    private TextView textViewError;
    private String search = "";
    private boolean isSuccess = true;

    private SharedPreferences sharedpreferencesMenuCategory;
    private int menuCategory = 0;
    private boolean isNextBtn = false;
    private boolean isConnect = false;
    private boolean isBtnFromSg = false;
    private boolean isFull = false;
    private String adminId = null;
    private String custDataId = null;
    private String personalOrderId = null;
    private String sharedLogId = null;
    private String logId = null;
    public Gson gson = new Gson();

    private ResponseGridListPersonal responseGridListPersonal = new ResponseGridListPersonal();

    private static List<ModelGridListPersonal> listPersonalGrid = new ArrayList<>();

    private ModelDataPersonal modelDataPersonal;
    private ModelDataPersonalSG modelDataPersonalSG;
    private ModelDataAddress modelDataAddress;
    private ModelDataMainData modelDataMainData;
    private ModelDataJobData modelDataJobData;
    private ModelDataEmergencyContact modelDataEmergencyContact;
    private ModelDataFinancial modelDataFinancial;

    private ResponseDetailPersonalInfo responseDetailPersonalInfo;
    private ResponseDetailPersonalSgInfo responseDetailPersonalSgInfo;
    private ResponseDetailPersonalAddr responseDetailPersonalAddr;
    private ResponseDetailPersonalOther responseDetailPersonalOther;
    private ResponseDetailPersonalJob responseDetailPersonalJob;
    private ResponseDetailPersonalEmergency responseDetailPersonalEmergency;
    private ResponseDetailPersonalFinancial responseDetailPersonalFinancial;

    public ApiPersonal(View view, Context context, ProgressDialog loadingDialog) {
        this.view = view;
        this.context = context;
        this.loadingDialog = loadingDialog;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sharedpreferencesMenuCategory = this.context.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, MODE_PRIVATE);
        menuCategory = sharedpreferencesMenuCategory.getInt(Constans.KEY_MENU_CATEGORY, 0);
        sharedLogId = this.context.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, MODE_PRIVATE).getString(Constans.KEY_LOG_ID, null);
    }

    public boolean isConnect() {
        return isConnect;
    }

    public boolean getListPersonal(String userId,
                                   String statusId,
                                   int tab,
                                   int menu,
                                   String search,
                                   GridView gridView,
                                   TextView textViewError,
                                   boolean isFull) {

        this.tab = tab;
        this.menu = menu;
        this.search = search;
        this.gridView = gridView;
        this.isFull = isFull;
        this.textViewError = textViewError;
        return getListPersonal(userId, statusId, search);
    }

    public boolean getListPersonal(String userId, String statusId, final String search) {
        loadingDialog.show();
        listPersonalGrid.clear();
        if (InternetConnection.checkConnection(context)) {
            Call<ResponseGridListPersonal> ddlPersonal;
            if (isFull) {
                ddlPersonal = apiInterface.getListDataPersonalFull(userId, statusId);
            }else{
                ddlPersonal = apiInterface.getListDataPersonal(userId, statusId);
            }
            ddlPersonal.enqueue(new Callback<ResponseGridListPersonal>() {
                @Override
                public void onResponse(Call<ResponseGridListPersonal> call, Response<ResponseGridListPersonal> response) {
                    if (response.isSuccessful()) {
                        responseGridListPersonal = response.body();
                        Log.d("Result", gson.toJson(responseGridListPersonal.getModelObjectGridListPersonal().getModelGridListPersonal()));
                        if (createGridList(responseGridListPersonal)){
                            textViewError.setText(null);

                            Log.d("Info Grid List","Success Create Grid List Personal");
                        }
                        isConnect = true;
                    } else if (response.code() == 404) {
                        isConnect = false;
                        String error = "";
                        try {
                            responseGridListPersonal = gson.fromJson(response.errorBody().string(), ResponseGridListPersonal.class);
                            error = responseGridListPersonal.getMessage();

                            if(!error.equals("Data Not Found")){
                                error = error.concat("\n Please Refresh by Pulling Down");
                            }
                        } catch (IOException e) {
                            Log.e("Error", e.getMessage());
                            error = "Something went wrong \n Please Refresh by Pulling Down";
                            e.printStackTrace();
                        }
                        textViewError.setText(error);
                        gridView.setAdapter(null);
                        isSuccess = false;
                        loadingDialog.dismiss();
                    } else {
                        isConnect = false;

                        textViewError.setText(context.getString(R.string.error_api).concat("\n" +
                                "Please Refresh by Pulling Down"));
                        gridView.setAdapter(null);
                        isSuccess = false;
                        loadingDialog.dismiss();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<ResponseGridListPersonal> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    isConnect = false;
                    isSuccess = false;
                    textViewError.setText(t.toString().concat("\n" +
                            "Please Refresh by Pulling Down"));
                    gridView.setAdapter(null);
                    loadingDialog.dismiss();
                }
            });
        } else {
            gridView.setAdapter(null);
            isConnect = false;
            isSuccess = false;
            textViewError.setText(context.getString(R.string.content_question_internet_error).concat("\n" +
                    "Please Refresh by Pulling Down"));
            loadingDialog.dismiss();
        }
        return isSuccess;
    }

    private boolean createGridList(ResponseGridListPersonal responseGridListPersonal){
        if(tab == 0 || tab == 2 || tab == 3){
            if (!search.equals("")) {
                listPersonalGrid.clear();
                for (ModelGridListPersonal modelGridListPersonal: responseGridListPersonal.getModelObjectGridListPersonal().getModelGridListPersonal()) {
                    if (modelGridListPersonal.getFullName().contains(search)
                            || modelGridListPersonal.getDateCreate().contains(search)
                            || modelGridListPersonal.getIdNo().contains(search)) {
                        listPersonalGrid.add(modelGridListPersonal);
                    }
                }
                gridView.setAdapter(new AdapterCustomPendingPersonal(context, listPersonalGrid, tab, menu));
            } else {
                gridView.setAdapter(new AdapterCustomPendingPersonal(context, Arrays.asList(responseGridListPersonal.getModelObjectGridListPersonal().getModelGridListPersonal()), tab, menu));
            }
        }else{
            if (!search.equals("")) {
                listPersonalGrid.clear();
                for (ModelGridListPersonal modelGridListPersonal : responseGridListPersonal.getModelObjectGridListPersonal().getModelGridListPersonal()) {
                    if (modelGridListPersonal.getFullName().contains(search)
                            || modelGridListPersonal.getDateCreate().contains(search)
                            || modelGridListPersonal.getIdNo().contains(search)) {
                        listPersonalGrid.add(modelGridListPersonal);
                    }
                }
                gridView.setAdapter(new AdapterCustomCompletePersonal(context, listPersonalGrid, tab, menu));
            } else {
                gridView.setAdapter(new AdapterCustomCompletePersonal(context, Arrays.asList(responseGridListPersonal.getModelObjectGridListPersonal().getModelGridListPersonal()), tab, menu));
            }
        }
        loadingDialog.dismiss();
        return true;
    }

    public void saveFullFormPersonal(
                            ModelDataPersonal modelDataPersonal,
                            ModelDataAddress modelDataAddress,
                            ModelDataMainData modelDataMainData,
                            ModelDataJobData modelDataJobData,
                            ModelDataEmergencyContact modelDataEmergencyContact,
                            ModelDataFinancial modelDataFinancial,
                            String orderid,
                            String name,
                            boolean isNextBtnd,
                            String custId,
                            String logid){
        this.modelDataPersonal = modelDataPersonal;
        this.modelDataAddress = modelDataAddress;
        this.modelDataMainData = modelDataMainData;
        this.modelDataJobData = modelDataJobData;
        this.modelDataEmergencyContact = modelDataEmergencyContact;
        this.modelDataFinancial = modelDataFinancial;
        this.adminId = modelDataPersonal.getUsrcrt();
        this.isNextBtn = isNextBtnd;
        this.custDataId = custId;
        this.personalOrderId = orderid;
        this.logId = logid;

        try {
            saveFullPersonalInfo(modelDataPersonal, name, orderid, logid);
        } catch (IOException e) {
            loadingDialog.dismiss();
            Log.e("Error Address",e.getMessage());
            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                    Snackbar.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void saveFullPersonalInfo(final ModelDataPersonal modelDataPersonal, final String name,
                                      final String orderid, final String logid) throws IOException{
        loadingDialog.show();

        Log.d("Request", gson.toJson(modelDataPersonal));
        Log.d("Request", name + " " + orderid);

        if (!custDataId.equals('-') && modelDataPersonal.isModified()) {

            if(!modelDataPersonal.getIdimgurl().startsWith("http")
                    && !modelDataPersonal.getIdimg().equals("")
                    && !modelDataPersonal.getIdimg().equals("No selected file")){
                FileFunction fileFunction = new FileFunction();
                modelDataPersonal.setIdimgurl(fileFunction.createBase64(modelDataPersonal.getIdimg()));
            }

            if(!modelDataPersonal.getNpwpimgurl().startsWith("http")
                    && !modelDataPersonal.getNpwpimg().equals("")
                    && !modelDataPersonal.getNpwpimg().equals("No selected file")){
                FileFunction fileFunction = new FileFunction();
                modelDataPersonal.setNpwpimgurl(fileFunction.createBase64(modelDataPersonal.getNpwpimg()));
            }

            if (InternetConnection.checkConnection(context)) {
                Call<ResponseDetailPersonalInfo> saveInfo = apiInterface.savePersonalInfo(modelDataPersonal, name, orderid, logid);
                saveInfo.enqueue(new Callback<ResponseDetailPersonalInfo>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseDetailPersonalInfo> call, Response<ResponseDetailPersonalInfo> response) {
                        if (response.isSuccessful()) {
                            responseDetailPersonalInfo = response.body();
                            if (responseDetailPersonalInfo.getStatus().equals("success")) {
                                String custId = responseDetailPersonalInfo.getModelDataPersonal().getPersonal_custdata_return();
                                Log.d("Status", custId);

                                if (custId.equals("-")){
                                    custId = custDataId;
                                }

                                Log.d("CUST DATA ID", "RETURN " + custId);
                                Log.d("NAME", "RETURN " + name);

                                try {
                                    saveFullPersonalAddr(modelDataAddress, name, custId);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address",e.getMessage());
                                    Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image",
                                            Snackbar.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }

                            }else{
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Personal Info \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalInfo(modelDataPersonal, name, orderid, logid);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address",e.getMessage());
                                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                                    Snackbar.LENGTH_LONG).show();
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                                loadingDialog.dismiss();
                            }
                        } else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";

                            try {
                                responseDetailPersonalInfo = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalInfo.class);
                                error = responseDetailPersonalInfo.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Personal Info \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalInfo(modelDataPersonal, name, orderid, logid);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address",e.getMessage());
                                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                                    Snackbar.LENGTH_LONG).show();
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                            } catch (IOException e) {
                                error = e.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Personal Info \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalInfo(modelDataPersonal, name, orderid, logid);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address",e.getMessage());
                                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                                    Snackbar.LENGTH_LONG).show();
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                                e.printStackTrace();
                            }

                            Log.e("Error", error);
                        } else {
                            isConnect = false;
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saves Personal Info \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveFullPersonalInfo(modelDataPersonal, name, orderid, logid);
                                    } catch (IOException e) {
                                        loadingDialog.dismiss();
                                        Log.e("Error Address",e.getMessage());
                                        Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                        e.printStackTrace();
                                    }
                                }
                            });
                            b.show();
                            loadingDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseDetailPersonalInfo> call, @NonNull Throwable throwable) {
                        Log.e("Error Retrofit", throwable.toString());
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saves Personal Info \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    saveFullPersonalInfo(modelDataPersonal, name, orderid, logid);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address",e.getMessage());
                                    Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                            Snackbar.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                });
            } else {
                Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Error Dialog");
                b.setCancelable(false);
                b.setMessage("Failed Saves Personal Info \n" +
                        "Please try Again");
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            saveFullPersonalInfo(modelDataPersonal, name, orderid, logid);
                        } catch (IOException e) {
                            loadingDialog.dismiss();
                            Log.e("Error Address",e.getMessage());
                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                    Snackbar.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                });
                b.show();
                loadingDialog.dismiss();
            }

        }else if (!custDataId.equals('-') && !modelDataPersonal.isModified()){
            Log.d("Cust Data - Is Modified", "true - false");

            try {
                saveFullPersonalAddr(modelDataAddress, name, custDataId);
            } catch (IOException e) {
                loadingDialog.dismiss();
                Log.e("Error Address",e.getMessage());
                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image",
                        Snackbar.LENGTH_LONG).show();
                e.printStackTrace();
            }

        }else{
            Log.d("Is Modified", "false");
            Snackbar.make(view.findViewById(R.id.personal_full_edit), "You Must Fill Data Personal First, Please Click Data Personal Button",
                    Snackbar.LENGTH_LONG).show();
            isConnect = false;
            loadingDialog.dismiss();
        }
    }

    public void saveFullFormPersonalSg(
                            ModelDataPersonalSG modelDataPersonalSG,
                            ModelDataAddress modelDataAddress,
                            ModelDataMainData modelDataMainData,
                            ModelDataJobData modelDataJobData,
                            ModelDataEmergencyContact modelDataEmergencyContact,
                            ModelDataFinancial modelDataFinancial,
                            String orderid,
                            String name,
                            boolean isNextBtn,
                            String custId,
                            boolean isSg,
                            String logid){
        this.modelDataPersonalSG = modelDataPersonalSG;
        this.modelDataAddress = modelDataAddress;
        this.modelDataMainData = modelDataMainData;
        this.modelDataJobData = modelDataJobData;
        this.modelDataEmergencyContact = modelDataEmergencyContact;
        this.modelDataFinancial = modelDataFinancial;
        this.personalOrderId = orderid;
        this.adminId = modelDataPersonalSG.getUsrcrt();
        this.isNextBtn = isNextBtn;
        this.custDataId = custId;
        this.isBtnFromSg = isSg;
        this.logId = logid;

        try {
            saveFullPersonalSgInfo(modelDataPersonalSG, name, orderid, logid);
        } catch (IOException e) {
            loadingDialog.dismiss();
            Log.e("Error Address",e.getMessage());
            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                    Snackbar.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private void saveFullPersonalSgInfo(final ModelDataPersonalSG modelDataPersonalSG, final String name,
                                        final String orderid, final String logid) throws IOException{
        loadingDialog.show();

        Log.d("Request", gson.toJson(modelDataPersonalSG));
        Log.d("Request", name+" "+orderid);

        if (!custDataId.equals('-') && modelDataPersonalSG.isModified()) {

            if(!modelDataPersonalSG.getIdimgurl().startsWith("http")
                    && !modelDataPersonalSG.getIdimg().equals("")
                    && !modelDataPersonalSG.getIdimg().equals("No selected file")){
                FileFunction fileFunction = new FileFunction();
                modelDataPersonalSG.setIdimgurl(fileFunction.createBase64(modelDataPersonalSG.getIdimg()));
            }

            if(!modelDataPersonalSG.getNpwpimgurl().startsWith("http")
                    && !modelDataPersonalSG.getNpwpimg().equals("")
                    && !modelDataPersonalSG.getNpwpimg().equals("No selected file")){
                FileFunction fileFunction = new FileFunction();
                modelDataPersonalSG.setNpwpimgurl(fileFunction.createBase64(modelDataPersonalSG.getNpwpimg()));
            }

            if (InternetConnection.checkConnection(context)){
                Call<ResponseDetailPersonalSgInfo> saveInfo = apiInterface.savePersonalSgInfo(modelDataPersonalSG, name,orderid, logid);
                saveInfo.enqueue(new Callback<ResponseDetailPersonalSgInfo>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseDetailPersonalSgInfo> call, Response<ResponseDetailPersonalSgInfo> response) {
                        if(response.isSuccessful()){
                            responseDetailPersonalSgInfo = response.body();
                            if(responseDetailPersonalSgInfo.getStatus().equals("success")){
                                String custId = responseDetailPersonalSgInfo.getModelDataPersonalSG().getPersonal_custdata_return();
                                Log.d("Status", custId);

                                if (custId.equals("-")){
                                    custId = custDataId;
                                }

                                Log.d("CUST DATA ID", "RETURN " + custId);
                                Log.d("NAME", "RETURN " + name);

                                try {
                                    saveFullPersonalAddr(modelDataAddress, name, custId);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address",e.getMessage());
                                    Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Address Data : Error upload image",
                                            Snackbar.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }
                            }else{
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved Personal Info \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalSgInfo(modelDataPersonalSG, name, orderid, logid);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address",e.getMessage());
                                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                                    Snackbar.LENGTH_LONG).show();
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                                loadingDialog.dismiss();
                            }

                        }else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";

                            try {
                                responseDetailPersonalSgInfo = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalSgInfo.class);
                                error = responseDetailPersonalSgInfo.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved Personal Info \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalSgInfo(modelDataPersonalSG, name, orderid, logid);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address",e.getMessage());
                                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                                    Snackbar.LENGTH_LONG).show();
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                            } catch (IOException e) {
                                error = e.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved Personal Info \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalSgInfo(modelDataPersonalSG, name, orderid, logid);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address",e.getMessage());
                                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                                    Snackbar.LENGTH_LONG).show();
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                                e.printStackTrace();
                            }

                            Log.e("Error", error);
                            isSuccess = false;
                            loadingDialog.dismiss();
                        }else {
                            isConnect = false;
                            Log.e("Other Error", String.valueOf(R.string.error_api));
                            isSuccess = false;
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved Personal Info \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveFullPersonalSgInfo(modelDataPersonalSG, name, orderid, logid);
                                    } catch (IOException e) {
                                        loadingDialog.dismiss();
                                        Log.e("Error Address",e.getMessage());
                                        Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                        e.printStackTrace();
                                    }
                                }
                            });
                            b.show();
                            loadingDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseDetailPersonalSgInfo> call, @NonNull Throwable throwable) {
                        Log.e("Error Retrofit", throwable.toString());
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saved Personal Info \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    saveFullPersonalSgInfo(modelDataPersonalSG, name, orderid, logid);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address",e.getMessage());
                                    Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                            Snackbar.LENGTH_LONG).show();
                                    e.printStackTrace();
                                }
                            }
                        });
                        b.show();
                        isConnect = false;
                        loadingDialog.dismiss();
                    }
                });
            }else {
                Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
                isConnect = false;
                isSuccess = false;
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Error Dialog");
                b.setCancelable(false);
                b.setMessage("Failed Saved Personal Info \n" +
                        "Please try Again");
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            saveFullPersonalSgInfo(modelDataPersonalSG, name, orderid, logid);
                        } catch (IOException e) {
                            loadingDialog.dismiss();
                            Log.e("Error Address",e.getMessage());
                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Info : Error upload image",
                                    Snackbar.LENGTH_LONG).show();
                            e.printStackTrace();
                        }
                    }
                });
                b.show();
                loadingDialog.dismiss();
            }

        }else if (!custDataId.equals('-') && !modelDataPersonalSG.isModified()){
            Log.d("Cust Data - Is Modified", "true - false");

            try {
                saveFullPersonalAddr(modelDataAddress, name, custDataId);
            } catch (IOException e) {
                loadingDialog.dismiss();
                Log.e("Error Address",e.getMessage());
                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image",
                        Snackbar.LENGTH_LONG).show();
                e.printStackTrace();
            }

        }else{
            Log.d("Is Modified", "false");
            Snackbar.make(view.findViewById(R.id.personal_full_edit), "You Must Fill Data Spouse / Guarantor First, Please Click Data Personal Button",
                    Snackbar.LENGTH_LONG).show();
            isConnect = false;
            loadingDialog.dismiss();
        }
    }


    private void saveFullPersonalAddr(final ModelDataAddress modelDataAddress, final String name, final String cusId) throws IOException{
        loadingDialog.show();

        Log.d("Request", gson.toJson(modelDataAddress));
        Log.d("Request", name+" "+cusId);

        if (modelDataAddress.isModified()) {

            if(!modelDataAddress.getBldimgurl().startsWith("http")
                    && !modelDataAddress.getBldimg().equals("")
                    && !modelDataAddress.getBldimg().equals("No selected file")){
                FileFunction fileFunction = new FileFunction();
                modelDataAddress.setBldimgurl(fileFunction.createBase64(modelDataAddress.getBldimg()));
            }

            if (InternetConnection.checkConnection(context)) {
                Call<ResponseDetailPersonalAddr> saveInfo = apiInterface.savePersonalAddr(modelDataAddress, name, cusId);
                saveInfo.enqueue(new Callback<ResponseDetailPersonalAddr>() {
                    @Override
                    public void onResponse(Call<ResponseDetailPersonalAddr> call, Response<ResponseDetailPersonalAddr> response) {
                        if (response.isSuccessful()) {
                            responseDetailPersonalAddr = response.body();
                            if (responseDetailPersonalAddr.getStatus().equals("success")) {
                                Log.d("Status",responseDetailPersonalAddr.getModelDataAddress().getPersonal_addr_return());

                                try {
                                    saveFullPersonalOther(modelDataMainData, name, cusId);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address", e.getMessage());

                                    if (name.equals("personal")) {
                                        Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                    } else {
                                        Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Address Data : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                    }
                                    e.printStackTrace();
                                }

                            } else {
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Address Info \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalAddr(modelDataAddress, name, custDataId);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address",e.getMessage());
                                            if (name.equals("personal")) {
                                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                                            } else {
                                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                                            }
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                                loadingDialog.dismiss();
                            }
                        } else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";

                            try {
                                responseDetailPersonalAddr = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalAddr.class);
                                error = responseDetailPersonalAddr.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Address Info \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalAddr(modelDataAddress, name, custDataId);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address",e.getMessage());
                                            if (name.equals("personal")) {
                                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                                            } else {
                                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                                            }
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();

                            } catch (IOException e) {
                                error = e.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Address Info \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalAddr(modelDataAddress, name, custDataId);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address",e.getMessage());
                                            if (name.equals("personal")) {
                                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                                            } else {
                                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                                            }
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                                e.printStackTrace();
                            }

                            Log.e("Error", error);
                            isSuccess = false;
                            loadingDialog.dismiss();
                        } else {
                            isConnect = false;
                            Log.e("Other Error", String.valueOf(R.string.error_api));
                            isSuccess = false;

                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saves Address Info \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveFullPersonalAddr(modelDataAddress, name, custDataId);
                                    } catch (IOException e) {
                                        loadingDialog.dismiss();
                                        Log.e("Error Address",e.getMessage());
                                        if (name.equals("personal")) {
                                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                                        } else {
                                            Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                                        }
                                        e.printStackTrace();
                                    }
                                }
                            });
                            b.show();
                            loadingDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDetailPersonalAddr> call, Throwable throwable) {
                        Log.e("Error Retrofit", throwable.toString());

                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saves Address Info \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    saveFullPersonalAddr(modelDataAddress, name, custDataId);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address",e.getMessage());
                                    if (name.equals("personal")) {
                                        Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                                    } else {
                                        Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                                    }
                                    e.printStackTrace();
                                }
                            }
                        });
                        b.show();

                        isConnect = false;
                        loadingDialog.dismiss();
                    }
                });
            } else {
                Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Error Dialog");
                b.setCancelable(false);
                b.setMessage("Failed Saves Address Info \n" +
                        "Please try Again");
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            saveFullPersonalAddr(modelDataAddress, name, custDataId);
                        } catch (IOException e) {
                            loadingDialog.dismiss();
                            Log.e("Error Address",e.getMessage());
                            if (name.equals("personal")) {
                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                            } else {
                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Address Data : Error upload image", Snackbar.LENGTH_LONG).show();
                            }
                            e.printStackTrace();
                        }
                    }
                });
                b.show();
                loadingDialog.dismiss();
            }

        }else{
            Log.d("Is Modified", "false");
            try {
                saveFullPersonalOther(modelDataMainData, name, cusId);
            } catch (IOException e) {
                loadingDialog.dismiss();
                Log.e("Error Address", e.getMessage());

                if (name.equals("personal")) {
                    Snackbar.make(view.findViewById(R.id.personal_full_edit), "Address Data : Error upload image",
                            Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Address Data : Error upload image",
                            Snackbar.LENGTH_LONG).show();
                }
                e.printStackTrace();
            }
        }
    }

    private void saveFullPersonalOther(final ModelDataMainData modelDataMainData, final String name, final String cusId) throws IOException{
        loadingDialog.show();

        Log.d("Request", gson.toJson(modelDataMainData));
        Log.d("Request", name+" "+cusId);

        if (modelDataMainData.isModified()) {

            if(!modelDataMainData.getKkimgurl().startsWith("http")
                    && !modelDataMainData.getKkimg().equals("")
                    && !modelDataMainData.getKkimg().equals("No selected file")){
                FileFunction fileFunction = new FileFunction();
                modelDataMainData.setKkimgurl(fileFunction.createBase64(modelDataMainData.getKkimg()));
            }

            if (InternetConnection.checkConnection(context)){
                Call<ResponseDetailPersonalOther> saveInfo = apiInterface.savePersonalOther(modelDataMainData, name, cusId);
                saveInfo.enqueue(new Callback<ResponseDetailPersonalOther>() {
                    @Override
                    public void onResponse(Call<ResponseDetailPersonalOther> call, Response<ResponseDetailPersonalOther> response) {
                        if(response.isSuccessful()){
                            responseDetailPersonalOther = response.body();
                            if(responseDetailPersonalOther.getStatus().equals("success")){
                                Log.d("Status",responseDetailPersonalOther.getModelDataMainData().getPersonal_other_return());

                                saveFullPersonalJob(modelDataJobData, name, cusId);

                            } else {
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved Main Data \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalOther(modelDataMainData, name, cusId);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address", e.getMessage());

                                            if (name.equals("personal")) {
                                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Main Data : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            } else {
                                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Main Data : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            }
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                                loadingDialog.dismiss();
                            }
                        }else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";

                            try {
                                responseDetailPersonalOther = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalOther.class);
                                error = responseDetailPersonalOther.getMessage();

                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved Main Data \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalOther(modelDataMainData, name, cusId);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address", e.getMessage());

                                            if (name.equals("personal")) {
                                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Main Data : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            } else {
                                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Main Data : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            }
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                            } catch (IOException e) {
                                error = e.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved Main Data \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalOther(modelDataMainData, name, cusId);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address", e.getMessage());

                                            if (name.equals("personal")) {
                                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Main Data : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            } else {
                                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Main Data : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            }
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                                e.printStackTrace();
                            }

                            Log.e("Error", error);
                            isSuccess = false;
                            loadingDialog.dismiss();
                        }else {
                            isConnect = false;
                            Log.e("Other Error", String.valueOf(R.string.error_api));
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved Main Data \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveFullPersonalOther(modelDataMainData, name, cusId);
                                    } catch (IOException e) {
                                        loadingDialog.dismiss();
                                        Log.e("Error Address", e.getMessage());

                                        if (name.equals("personal")) {
                                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Main Data : Error upload image",
                                                    Snackbar.LENGTH_LONG).show();
                                        } else {
                                            Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Main Data : Error upload image",
                                                    Snackbar.LENGTH_LONG).show();
                                        }
                                        e.printStackTrace();
                                    }
                                }
                            });
                            b.show();
                            loadingDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDetailPersonalOther> call, Throwable throwable) {
                        Log.e("Error Retrofit", throwable.toString());

                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saved Main Data \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    saveFullPersonalOther(modelDataMainData, name, cusId);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address", e.getMessage());

                                    if (name.equals("personal")) {
                                        Snackbar.make(view.findViewById(R.id.personal_full_edit), "Main Data : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                    } else {
                                        Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Main Data : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                    }
                                    e.printStackTrace();
                                }
                            }
                        });
                        b.show();

                        isConnect = false;
                        loadingDialog.dismiss();
                    }
                });
            }else {
                Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Error Dialog");
                b.setCancelable(false);
                b.setMessage("Failed Saved Main Data \n" +
                        "Please try Again");
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            saveFullPersonalOther(modelDataMainData, name, cusId);
                        } catch (IOException e) {
                            loadingDialog.dismiss();
                            Log.e("Error Address", e.getMessage());

                            if (name.equals("personal")) {
                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Main Data : Error upload image",
                                        Snackbar.LENGTH_LONG).show();
                            } else {
                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Main Data : Error upload image",
                                        Snackbar.LENGTH_LONG).show();
                            }
                            e.printStackTrace();
                        }
                    }
                });
                b.show();
                loadingDialog.dismiss();
            }

        }else{
            Log.d("Is Modified", "false");
            saveFullPersonalJob(modelDataJobData, name, cusId);
        }
    }

    public void saveFullPersonalJob(final ModelDataJobData modelDataJobData, final String name, final String cusId) {
        loadingDialog.show();

        Log.d("Request", gson.toJson(modelDataJobData));
        Log.d("Request", name+" "+cusId);

        if (modelDataJobData.isModified()) {

            if (InternetConnection.checkConnection(context)){
                Call<ResponseDetailPersonalJob> saveInfo = apiInterface.savePersonalJob(modelDataJobData, name, cusId);
                saveInfo.enqueue(new Callback<ResponseDetailPersonalJob>() {
                    @Override
                    public void onResponse(Call<ResponseDetailPersonalJob> call, Response<ResponseDetailPersonalJob> response) {
                        if(response.isSuccessful()){
                            responseDetailPersonalJob = response.body();
                            if(responseDetailPersonalJob.getStatus().equals("success")){
                                Log.d("Status",responseDetailPersonalJob.getModelDataJobData().getPersonal_job_return());

                                saveFullPersonalEmergency(modelDataEmergencyContact, name, cusId);
                            } else {
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Job Data \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFullPersonalJob(modelDataJobData, name, cusId);
                                    }
                                });
                                b.show();
                                loadingDialog.dismiss();
                            }

                        }else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";
                            try {
                                responseDetailPersonalJob = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalJob.class);
                                error = responseDetailPersonalJob.getMessage();

                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Job Data \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFullPersonalJob(modelDataJobData, name, cusId);
                                    }
                                });
                                b.show();
                            } catch (IOException e) {
                                error = e.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Job Data \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFullPersonalJob(modelDataJobData, name, cusId);
                                    }
                                });
                                b.show();
                                e.printStackTrace();
                            }
                            Log.e("Error", error);
                            isSuccess = false;
                            loadingDialog.dismiss();
                        }else {
                            isConnect = false;
                            Log.e("Other Error", String.valueOf(R.string.error_api));
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saves Job Data \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    saveFullPersonalJob(modelDataJobData, name, cusId);
                                }
                            });
                            b.show();
                            loadingDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseDetailPersonalJob> call, @NonNull Throwable throwable) {
                        Log.e("Error Retrofit", throwable.toString());
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saves Job Data \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                saveFullPersonalJob(modelDataJobData, name, cusId);
                            }
                        });
                        b.show();
                        isConnect = false;
                        loadingDialog.dismiss();
                    }
                });
            }else {
                Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Error Dialog");
                b.setCancelable(false);
                b.setMessage("Failed Saves Job Data \n" +
                        "Please try Again");
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveFullPersonalJob(modelDataJobData, name, cusId);
                    }
                });
                b.show();
                loadingDialog.dismiss();
            }

        }else{
            Log.d("Is Modified", "false");
            saveFullPersonalEmergency(modelDataEmergencyContact, name, cusId);
        }
    }

    private void saveFullPersonalEmergency(final ModelDataEmergencyContact modelDataEmergencyContact, final String name, final String cusId){
        loadingDialog.show();

        Log.d("Request", gson.toJson(modelDataEmergencyContact));
        Log.d("Request", name+" "+cusId);

        if (modelDataEmergencyContact.isModified()) {

            if (InternetConnection.checkConnection(context)){
                Call<ResponseDetailPersonalEmergency> saveInfo = apiInterface.savePersonalEmergency(modelDataEmergencyContact, name,cusId);
                saveInfo.enqueue(new Callback<ResponseDetailPersonalEmergency>() {
                    @Override
                    public void onResponse(Call<ResponseDetailPersonalEmergency> call, Response<ResponseDetailPersonalEmergency> response) {
                        if(response.isSuccessful()){
                            responseDetailPersonalEmergency = response.body();
                            if(responseDetailPersonalEmergency.getStatus().equals("success")){
                                Log.d("Status",responseDetailPersonalEmergency.getModelDataEmergency().getPersonal_emergency_return());

                                try {
                                    saveFullPersonalFinancial(modelDataFinancial, name, cusId);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address", e.getMessage());

                                    if (name.equals("personal")) {
                                        Snackbar.make(view.findViewById(R.id.personal_full_edit), "Personal Emergency Data : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                    } else {
                                        Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Personal Emergency Data  : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                    }
                                    e.printStackTrace();
                                }

                            } else {
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Emergency Contact \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFullPersonalEmergency(modelDataEmergencyContact, name, cusId);
                                    }
                                });
                                b.show();
                                loadingDialog.dismiss();
                            }

                        }else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";
                            try {
                                responseDetailPersonalEmergency = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalEmergency.class);
                                error = responseDetailPersonalEmergency.getMessage();

                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Emergency Contact \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFullPersonalEmergency(modelDataEmergencyContact, name, cusId);
                                    }
                                });
                                b.show();
                            } catch (IOException e) {
                                error = e.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saves Emergency Contact \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFullPersonalEmergency(modelDataEmergencyContact, name, cusId);
                                    }
                                });
                                b.show();
                                e.printStackTrace();
                            }
                            Log.e("Error", error);
                            isSuccess = false;
                            loadingDialog.dismiss();
                        }else {
                            isConnect = false;
                            Log.e("Other Error", String.valueOf(R.string.error_api));
                            isSuccess = false;
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saves Emergency Contact \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    saveFullPersonalEmergency(modelDataEmergencyContact, name, cusId);
                                }
                            });
                            b.show();
                            loadingDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDetailPersonalEmergency> call, Throwable throwable) {
                        Log.e("Error Retrofit", throwable.toString());
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saves Emergency Contact \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                saveFullPersonalEmergency(modelDataEmergencyContact, name, cusId);
                            }
                        });
                        b.show();
                        isConnect = false;
                        loadingDialog.dismiss();
                    }
                });
            }else {
                Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Error Dialog");
                b.setCancelable(false);
                b.setMessage("Failed Saves Emergency Contact \n" +
                        "Please try Again");
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveFullPersonalEmergency(modelDataEmergencyContact, name, cusId);
                    }
                });
                b.show();
                loadingDialog.dismiss();
            }

        }else{
            Log.d("Is Modified", "false");
            try {
                saveFullPersonalFinancial(modelDataFinancial, name, cusId);
            } catch (IOException e) {
                loadingDialog.dismiss();
                Log.e("Error Address", e.getMessage());

                if (name.equals("personal")) {
                    Snackbar.make(view.findViewById(R.id.personal_full_edit), "Personal Emergency Data : Error upload image",
                            Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Personal Emergency Data  : Error upload image",
                            Snackbar.LENGTH_LONG).show();
                }
                e.printStackTrace();
            }
        }
    }

    private void saveFullPersonalFinancial(final ModelDataFinancial modelDataFinancial, final String name, final String cusId) throws IOException{
        loadingDialog.show();

        Log.d("Request", gson.toJson(modelDataFinancial));
        Log.d("Request", name+" "+cusId);

        if (modelDataFinancial.isModified()) {

            if(!modelDataFinancial.getSlipimgurl().startsWith("http")
                    && !modelDataFinancial.getSlipimg().equals("")
                    && !modelDataFinancial.getSlipimg().equals("No selected file")){
                FileFunction fileFunction = new FileFunction();
                modelDataFinancial.setSlipimgurl(fileFunction.createBase64(modelDataFinancial.getSlipimg()));
            }

            if (InternetConnection.checkConnection(context)){
                Call<ResponseDetailPersonalFinancial> saveInfo = apiInterface.savePersonalFinancial(modelDataFinancial, name, cusId);
                saveInfo.enqueue(new Callback<ResponseDetailPersonalFinancial>() {
                    @Override
                    public void onResponse(Call<ResponseDetailPersonalFinancial> call, Response<ResponseDetailPersonalFinancial> response) {
                        if(response.isSuccessful()){
                            responseDetailPersonalFinancial = response.body();
                            if(responseDetailPersonalFinancial.getStatus().equals("success")){
                                Log.d("Status",responseDetailPersonalFinancial.getModelDataFinancial().getPersonal_financial_return());
                                Log.d("IS NEXT BTN", "return"+isNextBtn);
                                if(isNextBtn && isBtnFromSg) {
                                    Log.d("PERSONAL_ORDER_ID", "return "+personalOrderId);
                                    Log.d("LOG_ID", "return "+sharedLogId);

                                    Intent intent = new Intent(context, ActivitySurvey.class);
                                    intent.putExtra(Constans.KEY_MENU, Constans.MENU_PERSONAL);
                                    intent.putExtra(Constans.KeySharedPreference.ORDER_ID, personalOrderId);
                                    intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);
                                    context.startActivity(intent);

                                    loadingDialog.dismiss();

                                }else if (isNextBtn){
                                    Log.d("PERSONAL_ORDER_ID", "return "+personalOrderId);
                                    Log.d("LOG_ID", "return "+sharedLogId);

                                    Intent intent = new Intent(context, ActivityPersonalSGFullEdit.class);
                                    intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, personalOrderId);
                                    intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);
                                    context.startActivity(intent);

                                    loadingDialog.dismiss();
                                }else{
                                    Intent intent = new Intent(context, MainActivityDrawer.class);
                                    intent.putExtra(Constans.KEY_MENU, menuCategory);
                                    intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                                    context.startActivity(intent);

                                    loadingDialog.dismiss();
                                }
                                loadingDialog.dismiss();
                            }else{
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved Finance Data \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalFinancial(modelDataFinancial, name, cusId);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address", e.getMessage());

                                            if (name.equals("personal")) {
                                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Personal Emergency Data : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            } else {
                                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Personal Emergency Data  : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            }
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                            }
                        }else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";
                            try {
                                responseDetailPersonalFinancial = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalFinancial.class);
                                error = responseDetailPersonalFinancial.getMessage();

                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved Finance Data \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalFinancial(modelDataFinancial, name, cusId);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address", e.getMessage());

                                            if (name.equals("personal")) {
                                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Personal Emergency Data : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            } else {
                                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Personal Emergency Data  : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            }
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                            } catch (IOException e) {
                                error = e.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved Finance Data \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveFullPersonalFinancial(modelDataFinancial, name, cusId);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Log.e("Error Address", e.getMessage());

                                            if (name.equals("personal")) {
                                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Personal Emergency Data : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            } else {
                                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Personal Emergency Data  : Error upload image",
                                                        Snackbar.LENGTH_LONG).show();
                                            }
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                                e.printStackTrace();
                            }
                            Log.e("Error", error);
                            isSuccess = false;
                            loadingDialog.dismiss();
                        }else {
                            isConnect = false;
                            Log.e("Other Error", String.valueOf(R.string.error_api));
                            isSuccess = false;
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved Finance Data \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        saveFullPersonalFinancial(modelDataFinancial, name, cusId);
                                    } catch (IOException e) {
                                        loadingDialog.dismiss();
                                        Log.e("Error Address", e.getMessage());

                                        if (name.equals("personal")) {
                                            Snackbar.make(view.findViewById(R.id.personal_full_edit), "Personal Emergency Data : Error upload image",
                                                    Snackbar.LENGTH_LONG).show();
                                        } else {
                                            Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Personal Emergency Data  : Error upload image",
                                                    Snackbar.LENGTH_LONG).show();
                                        }
                                        e.printStackTrace();
                                    }
                                }
                            });
                            b.show();
                            loadingDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseDetailPersonalFinancial> call, Throwable throwable) {
                        Log.e("Error Retrofit", throwable.toString());
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saved Finance Data \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    saveFullPersonalFinancial(modelDataFinancial, name, cusId);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Log.e("Error Address", e.getMessage());

                                    if (name.equals("personal")) {
                                        Snackbar.make(view.findViewById(R.id.personal_full_edit), "Personal Emergency Data : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                    } else {
                                        Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Personal Emergency Data  : Error upload image",
                                                Snackbar.LENGTH_LONG).show();
                                    }
                                    e.printStackTrace();
                                }
                            }
                        });
                        b.show();
                        isConnect = false;
                        loadingDialog.dismiss();
                    }
                });
            }else {
                Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle("Error Dialog");
                b.setCancelable(false);
                b.setMessage("Failed Saved Finance Data \n" +
                        "Please try Again");
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            saveFullPersonalFinancial(modelDataFinancial, name, cusId);
                        } catch (IOException e) {
                            loadingDialog.dismiss();
                            Log.e("Error Address", e.getMessage());

                            if (name.equals("personal")) {
                                Snackbar.make(view.findViewById(R.id.personal_full_edit), "Personal Emergency Data : Error upload image",
                                        Snackbar.LENGTH_LONG).show();
                            } else {
                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), "Personal Emergency Data  : Error upload image",
                                        Snackbar.LENGTH_LONG).show();
                            }
                            e.printStackTrace();
                        }
                    }
                });
                b.show();
                loadingDialog.dismiss();
            }

        }else{
            Log.d("Is Modified", "false");
            loadingDialog.dismiss();
            Intent intent = new Intent(context, MainActivityDrawer.class);
            intent.putExtra(Constans.KEY_MENU, menuCategory);
            intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
            context.startActivity(intent);
        }
    }
}