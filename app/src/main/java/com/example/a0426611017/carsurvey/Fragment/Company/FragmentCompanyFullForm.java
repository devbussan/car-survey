package com.example.a0426611017.carsurvey.Fragment.Company;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiCompany;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.R;


/**
 * Created by c201017001 on 25/01/2018.
 */

public class FragmentCompanyFullForm extends Fragment{

    private View view;
    private int tab;
    private int menu;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private SwipeRefreshLayout mSwipeRefreshLayoutError;

    private ApiCompany apiCompany;

    private String adminId;

    public FragmentCompanyFullForm newInstance(String search, int tab, int menu) {
        FragmentCompanyFullForm companyTab = new FragmentCompanyFullForm();
        Bundle args = new Bundle();
        args.putString("search", search);
        args.putInt("tab", tab);
        args.putInt("menu", menu);
        companyTab.setArguments(args);
        return companyTab;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_company_complete_grid, container, false);
        Context context = container.getContext();
        String search = getArguments().getString("search");
        tab = getArguments().getInt("tab");
        menu = getArguments().getInt("menu");

        adminId = context.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, Context.MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);

        apiCompany = new ApiCompany(view, context, progressDialog);

        mSwipeRefreshLayout = view.findViewById(R.id.swipeToRefreshCompanyComplete);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                searchItem("");
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        searchItem(search);


        return view;
    }

    private void searchItem(String search) {

        GridView gridView = view.findViewById(R.id.gridView);
        TextView textViewError = view.findViewById(R.id.text_view_info_error);
        gridView.setAdapter(null);

        switch (tab) {
            case Constans.TAB_CHECK_HQ:
                Log.d("Info", "TAB CHECK HQ");
                if (apiCompany.getListCompany(adminId, String.valueOf(Constans.STATUS_CHECK_HQ), tab, menu, search, gridView, textViewError, true)) {
                    Log.d("Info", "Success Get List");
                } else {
                    Log.d("Info", "Pokoknya ada yang error");
                }

                break;
            case Constans.TAB_RETURN_FULL:
                Log.d("Info", "TAB RETURN FULL");
                if (apiCompany.getListCompany(adminId, String.valueOf(Constans.STATUS_RETURN_FULL), tab, menu, search, gridView, textViewError, true)) {
                    Log.d("Info", "Success Get List");
                } else {
                    Log.d("Info", "Pokoknya ada yang error");
                }

                break;
            case Constans.TAB_REJECT_FULL:
                Log.d("Info", "TAB REJECT FULL");
                if (apiCompany.getListCompany(adminId, String.valueOf(Constans.STATUS_REJECT_FULL), tab, menu, search, gridView, textViewError, true)) {
                    Log.d("Info", "Success Get List");
                } else {
                    Log.d("Info", "Pokoknya ada yang error");
                }
                break;
            case Constans.TAB_COMPLETE:
                Log.d("Info", "TAB COMPLETE");
                if (apiCompany.getListCompany(adminId, String.valueOf(Constans.STATUS_COMPLETE), tab, menu, search, gridView, textViewError, true)) {
                    Log.d("Info", "Success Get List");
                } else {
                    Log.d("Info", "Pokoknya ada yang error");
                }
                break;
            case Constans.TAB_INPUT_FULL:
                Log.d("Info", "TAB INPUT FULL");
                if (apiCompany.getListCompany(adminId, String.valueOf(Constans.STATUS_INPUT_FULL), tab, menu, search, gridView, textViewError, true)) {
                    Log.d("Info", "Success Get List");
                } else {
                    Log.d("Info", "Pokoknya ada yang error");
                }
            default:
                break;
        }

    }

}
