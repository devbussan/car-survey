package com.example.a0426611017.carsurvey.Adapter.Company;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Company.DetailFullForm.ActivityCompanyDetail;
import com.example.a0426611017.carsurvey.Company.DetailMiniForm.ActivityCompanyDetailOrder;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelGridListCompany;

import java.util.List;

public class AdapterCustomCompleteCompany extends BaseAdapter {
    private static List<ModelGridListCompany> listcompanyGrid;
    private LayoutInflater mInflater;
    private Context context;

    private int tabCurrent;
    private int menu;

    public AdapterCustomCompleteCompany(Context ActivityListCompanyComplete, List<ModelGridListCompany> results, int tab, int menu) {
        context = ActivityListCompanyComplete;
        listcompanyGrid = results;
        mInflater = LayoutInflater.from(ActivityListCompanyComplete);
        tabCurrent = tab;
        this.menu = menu;
    }

    @SuppressLint({"SetTextI18n", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        AdapterCustomCompleteCompany.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_list_row, null);
            holder = new AdapterCustomCompleteCompany.ViewHolder();
            holder.companyDateTextField = convertView.findViewById(R.id.date);
            holder.companyNameTextField = convertView.findViewById(R.id.name);
            holder.companyNo_idTextField = convertView.findViewById(R.id.no_id);
            holder.companyComment = convertView.findViewById(R.id.comment);
            holder.buttonDetail = convertView.findViewById(R.id.buttonDetail);
            holder.buttonViewMoreComment = convertView.findViewById(R.id.btn_comment);

            convertView.setTag(holder);
        } else {
            holder = (AdapterCustomCompleteCompany.ViewHolder) convertView.getTag();

        }

        if(menu == Constans.MENU_COMPANY_ORDER_FORM){
            if(tabCurrent == Constans.TAB_PENDING){
                if(listcompanyGrid.get(position).getComMent().equals("-")){
                    holder.companyComment.setText("");
                    holder.buttonViewMoreComment.setVisibility(View.GONE);
                }else{
                    holder.companyComment.setText(listcompanyGrid.get(position).getComMent());
                    holder.companyComment.setTextColor(context.getResources().getColor(R.color.color_company));
                }
            }

            if(tabCurrent == Constans.TAB_CLEAR ){
                holder.companyComment.setText("");
                holder.buttonViewMoreComment.setVisibility(View.GONE);
            }
        }

        if(menu == Constans.MENU_COMPANY_FULL_FORM){
            if(tabCurrent == Constans.TAB_DRAFT || tabCurrent == Constans.TAB_CLEAR){
                holder.companyComment.setText("");
                holder.buttonViewMoreComment.setVisibility(View.GONE);
            }

            if(tabCurrent == Constans.TAB_CHECK_HQ){
                if(listcompanyGrid.get(position).getComMent().equals("-")){
                    holder.companyComment.setText("");
                    holder.buttonViewMoreComment.setVisibility(View.GONE);
                }            else{
                    holder.companyComment.setText(listcompanyGrid.get(position).getComMent());
                    holder.companyComment.setTextColor(context.getResources().getColor(R.color.color_company));
                }
            }
        }

        holder.companyDateTextField.setText(
                listcompanyGrid.get(position).getDateCreate().split("-")[2]
                        + " " + FormatDate.PRINT_MONTH[Integer.parseInt(listcompanyGrid.get(position).getDateCreate().split("-")[1])]
                        + " " + listcompanyGrid.get(position).getDateCreate().split("-")[0]
        );
        holder.companyNameTextField.setText(listcompanyGrid.get(position).getCompanyName());
        holder.companyNo_idTextField.setText(listcompanyGrid.get(position).getNpwpNo());

        holder.buttonViewMoreComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom_dialog);
                TextView headerText = dialog.findViewById(R.id.text_view_comment_title);
                TextView contextText = dialog.findViewById(R.id.text_view_comment_message);
                ImageView close = dialog.findViewById(R.id.btn_close_comment);
                TextView commentName = dialog.findViewById(R.id.text_view_comment_name);
                commentName.setText(listcompanyGrid.get(position).getCompanyName());
                headerText.setText("Comment");
                contextText.setText(listcompanyGrid.get(position).getComMent());
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.create();
                dialog.show();
            }
        });

        holder.buttonDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                SharedPreferences.Editor editor;
                if((tabCurrent == Constans.TAB_COMPLETE && menu == Constans.MENU_COMPANY_FULL_FORM) ||  menu == Constans.MENU_COMPANY_FULL_FORM){
                    intent = new Intent(context,ActivityCompanyDetail.class);
                    editor = context.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_CUST_DATA_ID, listcompanyGrid.get(position).getCustDataId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SURVEY_ID, listcompanyGrid.get(position).getSurveyId());
                    editor.apply();
                }else if(tabCurrent == Constans.TAB_CLEAR && menu == Constans.MENU_COMPANY_ORDER_FORM){
                    intent = new Intent(context,ActivityCompanyDetailOrder.class);
                    editor = context.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_CUST_DATA_ID, listcompanyGrid.get(position).getCustDataId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SURVEY_ID, listcompanyGrid.get(position).getSurveyId());
                    editor.apply();
                }else{
                    intent = new Intent(context,ActivityCompanyDetailOrder.class);
                }
                intent.putExtra(Constans.KeySharedPreference.COMPANY_ORDER_ID, listcompanyGrid.get(position).getCompanyOrderId());
                context.startActivity(intent);

            }
        });
        return convertView;
    }

    static class ViewHolder {
        TextView companyDateTextField;
        TextView companyNameTextField;
        TextView companyNo_idTextField;
        TextView companyComment;
        Button buttonDetail;
        Button buttonViewMoreComment;
    }

    @Override
    public int getCount() {return listcompanyGrid.size();}

    @Override
    public Object getItem(int arg0) { return listcompanyGrid.get(arg0); }

    @Override
    public long getItemId(int arg0) {return arg0;}

    }
