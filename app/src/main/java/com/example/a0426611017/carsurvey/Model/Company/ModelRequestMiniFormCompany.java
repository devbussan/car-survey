package com.example.a0426611017.carsurvey.Model.Company;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 09/02/2018.
 */

public class ModelRequestMiniFormCompany {

    @SerializedName("COMPANYNAME")
    @Expose
    String companyName;

    @SerializedName("NPWPNO")
    @Expose
    String npwpNo;

    @SerializedName("USRCRT")
    @Expose
    String userCreate;

    private boolean isModified = false;

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getNpwpNo() {
        return npwpNo;
    }

    public void setNpwpNo(String npwpNo) {
        this.npwpNo = npwpNo;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }
}
