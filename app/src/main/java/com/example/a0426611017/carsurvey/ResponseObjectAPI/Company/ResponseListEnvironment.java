package com.example.a0426611017.carsurvey.ResponseObjectAPI.Company;

import com.example.a0426611017.carsurvey.Model.Financial.ModelDDLFinance;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0414831216 on 3/12/2018.
 */

public class ResponseListEnvironment {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDDLFinance ddlFinance;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDDLFinance getDdlFinance() {
        return ddlFinance;
    }

    public void setDdlFinance(ModelDDLFinance ddlFinance) {
        this.ddlFinance = ddlFinance;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
