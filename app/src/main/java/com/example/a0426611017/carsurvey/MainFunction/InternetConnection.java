package com.example.a0426611017.carsurvey.MainFunction;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Response;

/**
 * Created by c201017001 on 08/02/2018.
 */

public class InternetConnection {

    public static boolean checkConnection(Context context) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Response resp = null;
        int code = 400;
//        OkHttpClient client = new OkHttpClient.Builder()
//                .connectTimeout(1, TimeUnit.MINUTES)
//                .readTimeout(30, TimeUnit.SECONDS)
//                .build();
//        try {
//            Request req = new Request.Builder().url(Constans.DOMAIN)
//                    .get()
//                    .build();
//            resp =
//                    client
//                    .newCall(req).execute();
//            code = resp.code();
//            Log.d("Response Code",""+code);
//        }
//        catch (Exception th) {
//            th.printStackTrace();
//            Log.d("Error",th.getMessage());
//        }
//        finally {
//            if (resp != null) {
//                try {
//                    resp.close();
//                }
//                catch (Exception th) {
//                    th.printStackTrace();
//                    Log.d("Error",th.getMessage());
//                }
//            }
//        }
        HttpURLConnection urlc = null;
        try {
//            URL url = new URL(Constans.DOMAIN);
            URL url = new URL(Constans.DOMAIN);
            urlc = (HttpURLConnection) url.openConnection();
            urlc.setRequestProperty("User-Agent", "test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setRequestMethod("GET");
            urlc.setDoInput(true);
            urlc.setReadTimeout(60000);
            urlc.setConnectTimeout(60000); // mTimeout is one minutes
            urlc.connect();
            Log.d("Info", "Response Code : "+urlc.getResponseCode());
            code = urlc.getResponseCode();
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("warning", "Error checking internet connection", e);
            return false;
        } finally {
            if(urlc != null){
                try {
                    urlc.getInputStream().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return code == 200;


    }

    public static Map<String, String> getImei(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        assert connMgr != null;
        NetworkInfo activeNetworkInfo = connMgr.getActiveNetworkInfo();
        Map ret = new HashMap();
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if(activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI){
            ret.put("isConnect", "1");
            ret.put("idx", 0);
        }else{
            if (connectivity != null) {
                NetworkInfo[] info = connectivity.getAllNetworkInfo();
                if (info != null) {
                    for (int i = 0; i < info.length; i++) {
                        if (info[i].isConnected()) {
                            ret.put("isConnect", "1");
                            ret.put("idx", i);
                        }
                    }
                }else{
                    ret.put("isConnect", "0");
                }

            }else{
                ret.put("isConnect", "0");
            }
        }
        return ret;
    }

}
