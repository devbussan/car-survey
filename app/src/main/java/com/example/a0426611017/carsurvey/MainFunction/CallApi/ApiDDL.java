package com.example.a0426611017.carsurvey.MainFunction.CallApi;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDDLCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseListEnvironment;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseSurveyEnvFullFormCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Finance.ResponseDDLFinance;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDDLPersonal;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by c201017001 on 20/02/2018.
 */

public class ApiDDL {

    private ApiInterface apiInterface;
    private View view;
    private Context context;
    private ProgressDialog loadingDialog;

    private boolean isConnect = false;
    private ResponseDDLCompany responseDDLCompany;
    private ResponseDDLPersonal responseDDLPersonal;
    private ResponseDDLFinance responseDDLFinance;
    private ResponseSurveyEnvFullFormCompany responseSurveyEnvFullFormCompany;
    private ResponseListEnvironment responseListEnvironment;

    public ApiDDL(View view, Context context, ProgressDialog loadingDialog) {
        this.view = view;
        this.context = context;
        this.loadingDialog = loadingDialog;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public ResponseDDLCompany getResponseDDLCompany() {
        return responseDDLCompany;
    }

    public void setResponseDDLCompany(ResponseDDLCompany responseDDLCompany) {
        this.responseDDLCompany = responseDDLCompany;
    }

    public ResponseDDLPersonal getResponseDDLPersonal() {
        return responseDDLPersonal;
    }

    public void setResponseDDLPersonal(ResponseDDLPersonal responseDDLPersonal) {
        this.responseDDLPersonal = responseDDLPersonal;
    }

    public ApiInterface getApiInterface() {
        return apiInterface;
    }

    public void setApiInterface(ApiInterface apiInterface) {
        this.apiInterface = apiInterface;
    }

    public ResponseDDLFinance getResponseDDLFinance() {
        return responseDDLFinance;
    }

    public void setResponseDDLFinance(ResponseDDLFinance responseDDLFinance) {
        this.responseDDLFinance = responseDDLFinance;
    }

    public ResponseSurveyEnvFullFormCompany getResponseSurveyEnvFullFormCompany() {
        return responseSurveyEnvFullFormCompany;
    }

    public void setResponseSurveyEnvFullFormCompany(ResponseSurveyEnvFullFormCompany responseSurveyEnvFullFormCompany) {
        this.responseSurveyEnvFullFormCompany = responseSurveyEnvFullFormCompany;
    }

    public ResponseListEnvironment getResponseListEnvironment() {
        return responseListEnvironment;
    }

    public void setResponseListEnvironment(ResponseListEnvironment responseListEnvironment) {
        this.responseListEnvironment = responseListEnvironment;
    }

    public boolean isConnect() {
        return isConnect;
    }

    public void getDDLCompany(){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            Call<ResponseDDLCompany> ddlCompany = apiInterface.getDDLCompany();
            ddlCompany.enqueue(new Callback<ResponseDDLCompany>() {
                @Override
                public void onResponse(Call<ResponseDDLCompany> call, Response<ResponseDDLCompany>
                        response) {
                    if (response.isSuccessful()) {
                        setResponseDDLCompany(response.body());
                        loadingDialog.dismiss();
                        isConnect = true;
                    } else {
                        isConnect = false;
                        Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDDLCompany> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    Snackbar.make(view.findViewById(R.id.myLayoutCompany), t.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                    isConnect = false;
                    loadingDialog.dismiss();
                }

            });
        } else {
            Snackbar.make(view.findViewById(R.id.myLayoutCompany), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            isConnect = false;
            loadingDialog.dismiss();
        }
    }

//    public void getDDLPersonal(){
//        loadingDialog.show();
//        if (InternetConnection.checkConnection(context)) {
//            Call<ResponseDDLPersonal> ddlCompany = apiInterface.getDDLPersonal();
//            ddlCompany.enqueue(new Callback<ResponseDDLPersonal>() {
//                @Override
//                public void onResponse(Call<ResponseDDLPersonal> call, Response<ResponseDDLPersonal>
//                        response) {
//                    if (response.isSuccessful()) {
//                        setResponseDDLPersonal(response.body());
//                        loadingDialog.dismiss();
//                        isConnect = true;
//                    } else {
//                        isConnect = false;
//                        loadingDialog.dismiss();
//                        Snackbar.make(view.findViewById(R.id.personal_full_edit), R.string.error_api,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseDDLPersonal> call, Throwable t) {
//                    Log.e("Error Retrofit", t.toString());
//                    Snackbar.make(view.findViewById(R.id.personal_full_edit), t.toString(),
//                            Snackbar.LENGTH_LONG)
//                            .show();
//                    isConnect = false;
//                    loadingDialog.dismiss();
//                }
//            });
//        } else {
//            Snackbar.make(view.findViewById(R.id.personal_full_edit), R.string.no_connectivity,
//                    Snackbar.LENGTH_LONG)
//                    .show();
//            loadingDialog.dismiss();
//            isConnect = false;
//        }
//    }

//    public void getDDLFinance (){
//        if (InternetConnection.checkConnection(context)) {
//            Call<ResponseDDLFinance> ddlFinanceCall = apiInterface.getDDLFinance();
//            ddlFinanceCall.enqueue(new Callback<ResponseDDLFinance>() {
//                @Override
//                public void onResponse(Call<ResponseDDLFinance> call, Response<ResponseDDLFinance>
//                        response) {
//                    if (response.isSuccessful()) {
//                        setResponseDDLFinance(response.body());
//                        isConnect = true;
//                    } else {
//                        isConnect = false;
//                        Snackbar.make(view.findViewById(R.id.layout_finance), R.string.error_api,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                    }
//                    loadingDialog.dismiss();
//                }
//
//                @Override
//                public void onFailure(Call<ResponseDDLFinance> call, Throwable t) {
//                    Log.e("Error Retrofit", t.toString());
//                    isConnect = false;
//                    Snackbar.make(view.findViewById(R.id.layout_finance), t.toString(),
//                            Snackbar.LENGTH_LONG)
//                            .show();
//                    loadingDialog.dismiss();
//                }
//            });
//        } else {
//            isConnect = false;
//            Snackbar.make(view.findViewById(R.id.layout_finance), R.string.no_connectivity,
//                    Snackbar.LENGTH_LONG)
//                    .show();
//            loadingDialog.dismiss();
//        }
//    }
}
