package com.example.a0426611017.carsurvey.Model.Personal.Mini;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426611017 on 2/23/2018.
 */

public class ModelGridListPersonal {

    @SerializedName("PERSONAL_ORDER_H_ID")
    private String personalOrderId;

    @SerializedName("FULLNAME")
    private String fullName;

    @SerializedName("ID_TYPE")
    private String idType;

    @SerializedName("ID_NO")
    private String idNo;

    @SerializedName("ID_IMG_URL")
    private String idImgUrl;

    @SerializedName("KK_NO")
    private String kkNo;

    @SerializedName("KK_IMG_URL")
    private String kkImgUrl;

    @SerializedName("SURVEY_STAT")
    private String surveyStat;

    @SerializedName("CMO_ID")
    private String cmoId;

    @SerializedName("ADMIN_NAME")
    private String adminName;

    @SerializedName("DTM_CRT")
    private String dateCreate;

    @SerializedName("IS_READ")
    private String isRead;

    @SerializedName("LOG_DATA_ID")
    private String logDataId;

    @SerializedName("PERSONAL_CUST_DATA_ID")
    private String custDataId;

    @SerializedName("GUARANTOR_CUST_DATA_ID")
    private String guarantorCustId;

    @SerializedName("PERSONAL_SURVEY_ID")
    private String surveyId;

    @SerializedName("KK_NO_SG")
    private String kkNoSG;

    @SerializedName("KK_IMG_URL_SG")
    private String kkImgURLSG;

    @SerializedName("COMMENT")
    private String comMent;



    public String getKkNoSG() {
        return kkNoSG;
    }

    public void setKkNoSG(String kkNoSG) {
        this.kkNoSG = kkNoSG;
    }

    public String getkkImgURLSG() {
        return kkImgURLSG;
    }

    public void setkkImgURLSG(String kkImgURLSG) {
        this.kkImgURLSG = kkImgURLSG;
    }

    public String getCustDataId() {
        return custDataId;
    }

    public void setCustDataId(String custDataId) {
        this.custDataId = custDataId;
    }

    public String getGuarantorCustId() {
        return guarantorCustId;
    }

    public void setGuarantorCustId(String guarantorCustId) {
        this.guarantorCustId = guarantorCustId;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getPersonalOrderId() {return personalOrderId;}

    public void setPersonalOrderId(String personalOrderId) {this.personalOrderId = personalOrderId;}

    public String getFullName() {return fullName;}

    public void setFullName(String fullName) {this.fullName = fullName;}

    public String getIdType() {return idType;}

    public void setIdType(String idType) {this.idType = idType;}

    public String getIdNo() {return idNo;}

    public void setIdNo(String idNo) {this.idNo = idNo;}

    public String getIdImgUrl() {return idImgUrl;}

    public void setIdImgUrl(String idImgUrl) {this.idImgUrl = idImgUrl;}

    public String getKkNo() {return kkNo;}

    public void setKkNo(String kkNo) {this.kkNo = kkNo;}

    public String getKkImgUrl() {return kkImgUrl;}

    public void setKkImgUrl(String kkImgUrl) {this.kkImgUrl = kkImgUrl;}

    public String getSurveyStat() {return surveyStat;}

    public void setSurveyStat(String surveyStat) {this.surveyStat = surveyStat;}

    public String getCmoId() {return cmoId;}

    public void setCmoId(String cmoId) {this.cmoId = cmoId;}

    public String getAdminName() {return adminName;}

    public void setAdminName(String adminName) {this.adminName = adminName;}

    public String getDateCreate() {return dateCreate;}

    public void setDateCreate(String dateCreate) {this.dateCreate = dateCreate;}

    public String getIsRead() {return isRead;}

    public void setIsRead(String isRead) {this.isRead = isRead;}

    public String getLogDataId() {return logDataId;}

    public void setLogDataId(String logDataId) {this.logDataId = logDataId;}

    public String getComMent() {
        return comMent;
    }

    public void setComMent(String comMent) {
        this.comMent = comMent;
    }
}
