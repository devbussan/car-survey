package com.example.a0426611017.carsurvey.MainFunction.CallApi;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelRequestCompanySendHq;
import com.example.a0426611017.carsurvey.Model.ModelBanding;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelRequestPersonalSendHq;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseCompanySendHq;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponsePersonalSendHq;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.ResponseBanding;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by 0426591017 on 3/21/2018.
 */

public class ApiBanding {
    private View view;
    private Context context;
    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;
    private SharedPreferences sharedpreferencesMenuCategory;
    private int menuCategory = 0;

    private String sharedLogId = null;
    private String sharedOrderId = null;

    private Gson gson = new Gson();
    private ResponseBanding responseBanding = new ResponseBanding();
    private ResponsePersonalSendHq responsePersonalSendHq  = new ResponsePersonalSendHq();
    private ResponseCompanySendHq responseCompanySendHq = new ResponseCompanySendHq();

    public ApiBanding(View view, Context context, ProgressDialog loadingDialog) {
        this.view = view;
        this.context = context;
        this.loadingDialog = loadingDialog;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sharedpreferencesMenuCategory = this.context.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, MODE_PRIVATE);
        menuCategory = sharedpreferencesMenuCategory.getInt(Constans.KEY_MENU_CATEGORY, 0);
    }

    public void sendCommentBanding(final ModelBanding modelBanding, final String name, final String logid, final String orderid){
        loadingDialog.show();
        Log.d("Request", gson.toJson(modelBanding));
        Log.d("Request", name+" "+logid);

        this.sharedLogId = logid;
        this.sharedOrderId = orderid;

        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseBanding> callBanding;
            callBanding = apiInterface.saveComment(modelBanding, name, logid);
            callBanding.enqueue(new Callback<ResponseBanding>() {
                @Override
                public void onResponse(Call<ResponseBanding> call, Response<ResponseBanding> response) {
                    if (response.isSuccessful()) {
                        responseBanding = response.body();
                        if (responseBanding.getStatus().equals("success")) {
                            String responses = responseBanding.getModelBanding().getLogDetailsReturn();

                            if (name.equals("personal")) {
                                ModelRequestPersonalSendHq modelRequestPersonalSendHq = new ModelRequestPersonalSendHq();
                                modelRequestPersonalSendHq.setUsrPd(modelBanding.getCmocrt());

                                if (modelBanding.getProcessid().equals("13")){
                                    modelRequestPersonalSendHq.setStatus("1");
                                }else{
                                    modelRequestPersonalSendHq.setStatus("5");
                                }

                                modelRequestPersonalSendHq.setIsReadCmo("0");
                                sendHq(modelRequestPersonalSendHq, sharedLogId, sharedOrderId);
                            } else if (name.equals("company")) {
                                ModelRequestCompanySendHq modelRequestCompanySendHq = new ModelRequestCompanySendHq();
                                modelRequestCompanySendHq.setUsrPd(modelBanding.getCmocrt());

                                if (modelBanding.getProcessid().equals("13")) {
                                    modelRequestCompanySendHq.setStaTus("1");
                                }else{
                                    modelRequestCompanySendHq.setStaTus("5");
                                }

                                modelRequestCompanySendHq.setIsReadcmo("0");
                                sendHq(modelRequestCompanySendHq, sharedLogId, sharedOrderId);
                            }

                            loadingDialog.dismiss();
                        }

                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseBanding = gson.fromJson(response.errorBody().string(), ResponseBanding.class);
                            error = responseBanding.getMessage();

                            if (logid != null && responseBanding.getMessage().equals("Data Not Found")) {
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Dialog");
                                b.setCancelable(false);
                                b.setMessage("Failed Saved \n" +
                                        "Please try Again");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        sendCommentBanding(modelBanding, name, logid, orderid);
                                    }
                                });
                                b.show();
                                loadingDialog.dismiss();
                            }

                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Saved \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sendCommentBanding(modelBanding, name, logid, orderid);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                            loadingDialog.dismiss();
                        }
                        Log.e("Error", error);
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Saved \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sendCommentBanding(modelBanding, name, logid, orderid);
                            }
                        });
                        b.show();
                    }
                    loadingDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseBanding> call, Throwable t) {

                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Dialog");
                    b.setCancelable(false);
                    b.setMessage("Failed Saved \n" +
                            "Please try Again");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendCommentBanding(modelBanding, name, logid, orderid);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Dialog");
            b.setCancelable(false);
            b.setMessage("Failed Saved \n" +
                    "Please try Again");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendCommentBanding(modelBanding, name, logid, orderid);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }

    public void sendHq(final ModelRequestCompanySendHq modelRequestCompanySendHq, final String idLog, final String idOrder) {
        Log.d("Request", gson.toJson(modelRequestCompanySendHq));
        Log.d("Log", "" + idLog);
        Log.d("idOrder", "" + idOrder);
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseCompanySendHq> sendHq = apiInterface.submitHq(modelRequestCompanySendHq, idLog, idOrder);
            sendHq.enqueue(new Callback<ResponseCompanySendHq>() {
                @Override
                public void onResponse(Call<ResponseCompanySendHq> call, Response<ResponseCompanySendHq> response) {
                    if (response.isSuccessful()) {
                        responseCompanySendHq = response.body();
                        Log.d("Response", responseCompanySendHq.getModelMiniFormCompany().getLogCompany());
                        Snackbar.make(view.findViewById(R.id.layout_banding), responseCompanySendHq.getModelMiniFormCompany().getLogCompany(),
                                Snackbar.LENGTH_LONG)
                                .show();
                        Intent i = new Intent(context, MainActivityDrawer.class);
                        i.putExtra(Constans.KEY_MENU, menuCategory);
                        i.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                        context.startActivity(i);

                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseCompanySendHq = gson.fromJson(response.errorBody().string(), ResponseCompanySendHq.class);
                            error = responseCompanySendHq.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed saved Financial data \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sendHq(modelRequestCompanySendHq, idLog, idOrder);
                                }
                            });
                            b.show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Send to HQ \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sendHq(modelRequestCompanySendHq, idLog, idOrder);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        Log.e("Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Send to HQ \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sendHq(modelRequestCompanySendHq, idLog, idOrder);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseCompanySendHq> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Dialog");
                    b.setCancelable(false);
                    b.setMessage("Failed Send to HQ \n" +
                            "Please try Again");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendHq(modelRequestCompanySendHq, idLog, idOrder);
                        }
                    });
                    b.show();
                }
            });
        } else {
            Log.e("Error", String.valueOf(R.string.no_connectivity));
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Dialog");
            b.setCancelable(false);
            b.setMessage("Failed Send to HQ \n" +
                    "Please try Again");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendHq(modelRequestCompanySendHq, idLog, idOrder);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }

    }

    public void sendHq(final ModelRequestPersonalSendHq modelRequestPersonalSendHq, final String idLog, final String idOrder) {
        Log.d("Request", gson.toJson(modelRequestPersonalSendHq));
        Log.d("Log", "" + idLog);
        Log.d("idOrder", "" + idOrder);
        if (InternetConnection.checkConnection(context)) {
            Call<ResponsePersonalSendHq> sendHq = apiInterface.submitHq(modelRequestPersonalSendHq, idLog, idOrder);
            sendHq.enqueue(new Callback<ResponsePersonalSendHq>() {
                @Override
                public void onResponse(Call<ResponsePersonalSendHq> call, Response<ResponsePersonalSendHq> response) {
                    if (response.isSuccessful()) {
                        responsePersonalSendHq = response.body();
                        Log.d("Response", responsePersonalSendHq.getModelRequestPersonalSendHq().getLog_personal());
                        Snackbar.make(view.findViewById(R.id.layout_banding), responsePersonalSendHq.getModelRequestPersonalSendHq().getLog_personal(),
                                Snackbar.LENGTH_LONG)
                                .show();

                        Intent i = new Intent(context, MainActivityDrawer.class);
                        i.putExtra(Constans.KEY_MENU, menuCategory);
                        i.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                        context.startActivity(i);

                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responsePersonalSendHq = gson.fromJson(response.errorBody().string(), ResponsePersonalSendHq.class);
                            error = responsePersonalSendHq.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Send to HQ \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sendHq(modelRequestPersonalSendHq, idLog, idOrder);
                                }
                            });
                            b.show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Dialog");
                            b.setCancelable(false);
                            b.setMessage("Failed Send to HQ \n" +
                                    "Please try Again");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sendHq(modelRequestPersonalSendHq, idLog, idOrder);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        Log.e("Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Failed Send to HQ \n" +
                                "Please try Again");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sendHq(modelRequestPersonalSendHq, idLog, idOrder);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponsePersonalSendHq> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Dialog");
                    b.setCancelable(false);
                    b.setMessage("Failed Send to HQ \n" +
                            "Please try Again");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendHq(modelRequestPersonalSendHq, idLog, idOrder);
                        }
                    });
                    b.show();
                }
            });
        } else {
            Log.e("Error", String.valueOf(R.string.no_connectivity));
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Dialog");
            b.setCancelable(false);
            b.setMessage("Failed Send to HQ \n" +
                    "Please try Again");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendHq(modelRequestPersonalSendHq, idLog, idOrder);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }

    }
}
