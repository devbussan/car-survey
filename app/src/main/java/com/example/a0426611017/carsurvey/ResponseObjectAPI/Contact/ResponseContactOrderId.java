package com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact;

import com.example.a0426611017.carsurvey.Model.Company.ModelContactOrderId;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 23/02/2018.
 */

public class ResponseContactOrderId {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelContactOrderId modelContactOrderId;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelContactOrderId getModelContactOrderId() {
        return modelContactOrderId;
    }

    public void setModelContactOrderId(ModelContactOrderId modelContactOrderId) {
        this.modelContactOrderId = modelContactOrderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
