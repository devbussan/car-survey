package com.example.a0426611017.carsurvey.Finance;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Fragment.Financial.FragmentFinancial;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiCompany;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Financial.ModelDDLFinance;
import com.example.a0426611017.carsurvey.Model.Financial.ModelFinance;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Finance.ResponseDDLFinance;
import com.example.a0426611017.carsurvey.Survey.ActivitySurvey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCompanyFinancial extends AppCompatActivity {
    private Fragment currentFragment;
    private TextView titleBar;
    private ApiInterface apiInterface;
    private Button buttonDataFinancial,
            buttonBack, buttonSave, buttonKonfirm;
    private ModelDDLFinance modelDDLFinance;
    private ModelFinance modelFinance = new ModelFinance();
    private ProgressDialog loadingDialog;
    private FragmentFinancial fragmentFinancial = new FragmentFinancial();
    private ApiCompany apiCompany;
    private String orderid;
    private String adminId;
    private String logId;
    private int menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_company_financial);
        final View currentView = this.findViewById(android.R.id.content);

        setfield();
        setFormForCompany();

        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        apiCompany = new ApiCompany(currentView, this, loadingDialog);
        getDDLFinance();

        orderid = getIntent().getStringExtra(Constans.KeySharedPreference.ORDER_ID);
        adminId = this.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);
        logId = this.getSharedPreferences(Constans.KeySharedPreference.LOG_ID, MODE_PRIVATE).getString(Constans.KEY_LOG_ID, null);
        menu = getIntent().getIntExtra(Constans.KEY_MENU, 0);

        if (menu == Constans.MENU_PERSONAL) {
            setFormForPersonal();
            window.setStatusBarColor(getResources().getColor(R.color.color_personal));
        } else if (menu == Constans.MENU_COMPANY) {
            setFormForCompany();
            window.setStatusBarColor(getResources().getColor(R.color.color_company));
        }

        buttonBack = findViewById(R.id.button_back_financial);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyFinancial.this);
                b.setTitle(R.string.content_information_back_pressed);
                b.setMessage(getString(R.string.content_question_want_save_data)+
                        "\n"+getString(R.string.content_information_press_yes_save));
                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(ActivityCompanyFinancial.this, ActivitySurvey.class);
                        i.putExtra(Constans.KeySharedPreference.ORDER_ID, orderid);
                        i.putExtra(Constans.KEY_MENU, menu);
                        startActivity(i);
                        finish();
                    }
                });
                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        buttonSave.callOnClick();
                    }
                });
                b.show();
            }
        });

    }

    private void setfield() {
        titleBar = findViewById(R.id.text_view_financial);
        buttonDataFinancial = findViewById(R.id.button_data_financial);
        buttonBack = findViewById(R.id.button_back_financial);
        buttonSave = findViewById(R.id.button_save_financial);
        buttonKonfirm = findViewById(R.id.button_submit_financial);
    }

    private void setFormForCompany() {
        titleBar.setText("COMPANY FINANCIAL FORM DATA");
        titleBar.setBackgroundColor(getResources().getColor(R.color.color_company));
        buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonBack.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonSave.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonKonfirm.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));

        buttonSave.setOnClickListener(new View.OnClickListener() {
            //            Handler handler = new Handler(){
//                public  void handleMessage(Message msg){
//                    super.handleMessage(msg);
//                    progressDialog.incrementProgressBy(2);
//                }
//            };
            @Override
            public void onClick(View view) {
                loadingDialog.show();
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFinancial.this);
                builder.setTitle(getString(R.string.content_question_want_save_data));
                builder.setMessage(getString(R.string.content_information_save_and_exit));
                builder.setNegativeButton(getString(R.string.dialog_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                loadingDialog.dismiss();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        saveForm();
                        modelFinance.setUserCreate(adminId);
                        apiCompany.saveFinance(modelFinance, orderid, Constans.PARAM_WS_COMPANY);
                    }
                });
                builder.show();
            }
        });

        buttonDataFinancial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                buttonDataFinancial.setEnabled(false);
                buttonDataFinancial.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_company));

                if (modelDDLFinance == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyFinancial.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLFinance();
                            buttonDataFinancial.callOnClick();
                        }
                    });
                    b.show();
                } else {
                    fragmentFinancial = FragmentFinancial.newInstance(Constans.MENU_COMPANY, modelFinance, modelDDLFinance, Constans.PARAM_WS_COMPANY, orderid);

                    loadFragment(fragmentFinancial);
                }
            }
        });

        buttonKonfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingDialog.show();
                saveForm();
                saveComplete();
                if (modelFinance.isComplete()) {
                    modelFinance.setUserCreate(adminId);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFinancial.this);
                    builder.setTitle(getString(R.string.content_question_are_you_sure_to_save));
                    builder.setMessage(getString(R.string.content_information_save_and_next));
                    builder.setNegativeButton(getString(R.string.dialog_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    loadingDialog.dismiss();
                                    Log.e("info", "NO");
                                }
                            });
                    builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            apiCompany.saveFinance(modelFinance, orderid, Constans.PARAM_WS_COMPANY, adminId, logId, true);
                        }
                    });
                    builder.show();
                    loadingDialog.dismiss();
                } else {
                    if (buttonDataFinancial.isEnabled() && !modelFinance.isComplete()) {
                        buttonDataFinancial.setError("Not Complete");
                    }
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void setFormForPersonal() {
        titleBar.setText("PERSONAL FINANCIAL FORM DATA");
        titleBar.setBackgroundColor(getResources().getColor(R.color.color_personal));
        buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonBack.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonSave.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonKonfirm.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));

        buttonSave.setOnClickListener(new View.OnClickListener() {
            //            Handler handler = new Handler(){
//                public  void handleMessage(Message msg){
//                    super.handleMessage(msg);
//                    progressDialog.incrementProgressBy(2);
//                }
//            };
            @Override
            public void onClick(View view) {
                loadingDialog.show();
                saveForm();
                modelFinance.setUserCreate(adminId);
                apiCompany.saveFinance(modelFinance, orderid, Constans.PARAM_WS_PERSONAL);
            }
        });

        buttonDataFinancial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                buttonDataFinancial.setEnabled(false);
                buttonDataFinancial.setBackground(getDrawable(R.drawable.disabled_button_background_personal));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_personal));

                if (modelDDLFinance == null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyFinancial.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLFinance();
                            buttonDataFinancial.callOnClick();
                        }
                    });
                    b.show();
                } else {

                    fragmentFinancial = FragmentFinancial.newInstance(Constans.MENU_PERSONAL, modelFinance, modelDDLFinance, Constans.PARAM_WS_PERSONAL, orderid);

                    loadFragment(fragmentFinancial);
                }
            }
        });

        buttonKonfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveForm();
                saveComplete();
                if (modelFinance.isComplete()) {
                    modelFinance.setUserCreate(adminId);
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFinancial.this);
                    builder.setTitle(getString(R.string.content_question_are_you_sure_to_save));
                    builder.setMessage(getString(R.string.content_information_save_and_next));
                    builder.setNegativeButton(getString(R.string.dialog_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    loadingDialog.dismiss();
                                    Log.e("info", "NO");
                                }
                            });
                    builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            apiCompany.saveFinance(modelFinance, orderid, Constans.PARAM_WS_PERSONAL, adminId, logId, true);
                        }
                    });
                    builder.show();
                    loadingDialog.dismiss();
                } else {
                    if (buttonDataFinancial.isEnabled() && !modelFinance.isComplete()) {
                        buttonDataFinancial.setError("Not Complete");
                    }
                    loadingDialog.dismiss();
                }
            }
        });
    }

    private void saveForm() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                modelFinance = fragmentFinancial.getForm();
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFinancial.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    saveForm();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    private void saveComplete() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (modelFinance.isModified()) {
                    modelFinance = fragmentFinancial.getComplete();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityCompanyFinancial.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    saveForm();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    private void loadFragment(Fragment fragment) {
        currentFragment = fragment;
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frame_financial, fragment);
        fragmentTransaction.commit();
    }

    private void setDDL(ModelDDLFinance modelDDLFinance) {
        this.modelDDLFinance = modelDDLFinance;
    }

    public void getDDLFinance() {
        if (InternetConnection.checkConnection(this)) {
            Call<ResponseDDLFinance> ddlFinanceCall = apiInterface.getDDLFinance();
            ddlFinanceCall.enqueue(new Callback<ResponseDDLFinance>() {
                @Override
                public void onResponse(Call<ResponseDDLFinance> call, Response<ResponseDDLFinance>
                        response) {
                    if (response.isSuccessful()) {
                        setDDL(response.body().getDdlFinance());

                    } else {
                        Snackbar.make(findViewById(R.id.layout_finance), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                    }
                    loadingDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseDDLFinance> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());

                    Snackbar.make(findViewById(R.id.layout_finance), t.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(R.id.layout_finance), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            loadingDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder b = new AlertDialog.Builder(ActivityCompanyFinancial.this);
        b.setTitle(R.string.content_information_back_pressed);
        b.setMessage(getString(R.string.content_question_want_save_data)+
                "\n"+getString(R.string.content_information_press_yes_save));
        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(ActivityCompanyFinancial.this, ActivitySurvey.class);
                i.putExtra(Constans.KeySharedPreference.ORDER_ID, orderid);
                i.putExtra(Constans.KEY_MENU, menu);
                startActivity(i);
                finish();
            }
        });
        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                buttonSave.callOnClick();
            }
        });
        b.show();
    }
}



