package com.example.a0426611017.carsurvey.MainFunction;

import com.example.a0426611017.carsurvey.Model.Financial.ModelFinance;
import com.example.a0426611017.carsurvey.Model.Company.ModelAddressInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelCompanyInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelMandatoryInfo;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelRequestCompanySendHq;
import com.example.a0426611017.carsurvey.Model.Company.ModelRequestContactInfoCompany;
import com.example.a0426611017.carsurvey.Model.Company.ModelRequestMiniFormCompany;
import com.example.a0426611017.carsurvey.Model.ModelBanding;
import com.example.a0426611017.carsurvey.Model.Survey.ModelCekLingkungan;
import com.example.a0426611017.carsurvey.Model.Survey.ModelSurvey;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalInfoMini;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalSGMini;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataAddress;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataEmergencyContact;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataFinancial;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataJobData;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataMainData;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonal;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonalSG;
import com.example.a0426611017.carsurvey.Model.ModelRequestLogin;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelRequestPersonalSendHq;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseCompanySendHq;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDDLCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailCompanyMiniForm;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseDetailContactInfo;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.ResponseBanding;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseDetailSurveyEnviCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Finance.ResponseFinancial;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseFullFormCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseGetCompanySurvey;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseGridListCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseListEnvironment;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseMiniFormCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseSurveyEnvFullFormCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseSurveyFullFormCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseContactCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseContactOrderId;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailAddressFull;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailCompanyFull;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailEvidenceFull;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Finance.ResponseDDLFinance;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDDLPersonal;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalAddr;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalEmergency;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalFinancial;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalInfo;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalJob;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalOther;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalSgInfo;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseGridListPersonal;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseMiniFormPersonal;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseMiniFormPersonalSg;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponsePersonalSendHq;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.ResponseDashboard;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Finance.ResponseDetailFinance;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.ResponseLogin;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseDDLSurvey;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

/**
 * Created by c201017001 on 08/02/2018.
 */

public interface ApiInterface {

    @GET("refmaster/cpnyfull")
    Call<ResponseDDLCompany> getDDLCompany();

    @GET("refmaster/psfull")
    Call<ResponseDDLPersonal> getDDLPersonal();

    @GET("refmaster/srvfull")
    Call<ResponseDDLSurvey> getDDLSurvey();

    @POST("companyorder")
    Call<ResponseMiniFormCompany> saveMiniFormCompany(@Body ModelRequestMiniFormCompany miniFormCompany);

    @GET("companyorder/{userId}/status/{statusId}")
    Call<ResponseGridListCompany> getListDataCompany(@Path("userId") String userId, @Path("statusId") String statusId);

    @GET("companyfull/{userId}/status/{statusId}")
    Call<ResponseGridListCompany> getListDataCompanyFull(@Path("userId") String userId, @Path("statusId") String statusId);

    /*
     * Mini Form Personal
     */

    @POST("personalorder")
    Call<ResponseMiniFormPersonal> saveMiniFormPersonal(@Body ModelPersonalInfoMini modelPersonalInfoMini);

    @POST("spouseorder")
    Call<ResponseMiniFormPersonalSg> saveMiniFormPersonalSg(@Body ModelPersonalSGMini modelPersonalSGMini);

    @PUT("personalorder/{id}")
    Call<ResponseMiniFormPersonal> updateMiniFormPersonal(@Body ModelPersonalInfoMini modelPersonalInfoMini, @Path("id") String personalOrderid);

    @PUT("spouseorder/{id}")
    Call<ResponseMiniFormPersonalSg> updateMiniFormPersonalSg(@Body ModelPersonalSGMini modelPersonalSGMini, @Path("id") String personalOrderid);

    @GET("personalorder/{id}")
    Call<ResponseMiniFormPersonal> getDetailPersonalMiniForm(@Path("id") String personalOrderId);

    @GET("spouseorder/{id}")
    Call<ResponseMiniFormPersonalSg> getDetailPersonalSgMiniForm(@Path("id") String personalOrderId);

    /*
     * End Mini Form Personal
     */

    @GET("personalorder/{userId}/status/{statusId}")
    Call<ResponseGridListPersonal> getListDataPersonal(@Path("userId") String userId, @Path("statusId") String statusId);

    @GET("personalfull/{userId}/status/{statusId}")
    Call<ResponseGridListPersonal> getListDataPersonalFull(@Path("userId") String userId, @Path("statusId") String statusId);

    @GET("refmaster/finfull")
    Call<ResponseDDLFinance> getDDLFinance();

    @POST("contactorder")
    Call<ResponseContactCompany> saveContactCompany(@Body ModelRequestContactInfoCompany modelRequestContactInfoCompany);

    @PUT("companyorder/{id}")
    Call<ResponseMiniFormCompany> editMiniFormCompany(@Body ModelRequestMiniFormCompany miniFormCompany, @Path("id")String idCompanyOrder );

    @PUT("contactorder/{idcontact}/company/{id}")
    Call<ResponseContactCompany> editContactCompany(@Body ModelRequestContactInfoCompany modelRequestContactInfoCompany, @Path("id")String idCompanyOrder, @Path("idcontact") String idContact );

    @GET("contactorder/list/{id}")
    Call<ResponseContactOrderId> getContactOrderId(@Path("id") String idCompanyOrder);

    @PUT("companylog/{idlog}/company/{idcom}")
    Call<ResponseCompanySendHq> submitHq (@Body ModelRequestCompanySendHq modelRequestCompanySendHq, @Path("idlog") String idLog, @Path("idcom") String idCom );

    @PUT("personallog/{idlog}/personal/{idpersonal}")
    Call<ResponsePersonalSendHq> submitHq(@Body ModelRequestPersonalSendHq modelRequestPersonalSendHq, @Path("idlog") String idLog, @Path("idpersonal") String idPersonal);

    @GET("companyorder/{id}")
    Call<ResponseDetailCompanyMiniForm> getDetailCompanyMiniForm(@Path("id") String companyOrderID);

    @GET("contactorder/{id}/company/{idcompany}")
    Call<ResponseDetailContactInfo> getDetailContactInfo(@Path("id") String contactOrderId, @Path("idcompany") String companyOrderId);

    @PUT("companyfull/{idCompanyOrder}/logid/{logid}")
    Call<ResponseFullFormCompany> saveFullFormCompany(@Body ModelCompanyInfo modelCompanyInfo, @Path("idCompanyOrder") String idCompanyOrder, @Path("logid") String logid);

    @PUT("companyfull/addr/{idCustData}")
    Call<ResponseFullFormCompany> saveFullFormAddress(@Body ModelAddressInfo modelAddressInfo, @Path("idCustData") String idCustData);

    @PUT("companyfull/evidence/{idCustData}")
    Call<ResponseFullFormCompany> saveFullFormEvidence(@Body ModelMandatoryInfo modelMandatoryInfo, @Path("idCustData") String idCustData);

    @POST("auth")
    Call<ResponseLogin> authLogin(@Body ModelRequestLogin modelRequestLogin);

    @POST("authmini")
    Call<ResponseLogin> authLoginMini(@Body ModelRequestLogin modelRequestLogin);

    @PUT("surveyfull/{name}/survey/{idComCusdata}")
    Call<ResponseSurveyFullFormCompany> saveSurveyFullFormCompany(@Body ModelSurvey modelSurvey, @Path("name") String name, @Path("idComCusdata")String icComCust );

    @PUT("surveyfull/{name}/environtment/{id}")
    Call<ResponseSurveyEnvFullFormCompany> saveSurveyEnvFormCompany(@Body ModelCekLingkungan modelCekLingkungan, @Path("name")String nama, @Path("id")String idsurvey );

    @GET("companyfull/{id}")
    Call<ResponseDetailCompanyFull> getDetailFullCompany(@Path("id") String companyOrderid);

    @GET("companyfull/addr/{id}")
    Call<ResponseDetailAddressFull> getDetailFullAddress(@Path("id") String custDataId);

    @GET("companyfull/evidence/{id}")
    Call<ResponseDetailEvidenceFull> getDetailFullEvidence(@Path("id") String custDataId);

    @PUT("surveyfull/{name}/financial/{idorder}")
    Call<ResponseFinancial> saveFinance(@Path("name") String name, @Path("idorder") String idorder, @Body ModelFinance modelFinance);

    @GET("surveyfull/{name}/survey/{idOrder}")
    Call<ResponseGetCompanySurvey> getDetailSurveyCompany(@Path("idOrder") String idOrder,@Path("name") String name);

    @GET("surveyfull/{name}/list/idenv/{idorder}")
    Call<ResponseListEnvironment> getListEnvironment(@Path("name") String Name, @Path("idorder") String idCOm);

    @GET("surveyfull/{name}/environtment/{envIdSur}/survey/{idSurvey}")
    Call<ResponseDetailSurveyEnviCompany> getDetailEnviront (@Path("name") String name,@Path("envIdSur") String envIdSurv,@Path("idSurvey") String idSurvey);

    @GET("surveyfull/{name}/financial/{orderid}")
    Call<ResponseDetailFinance> getDetailFinancial(@Path("name") String name, @Path("orderid") String companyorder);

    /*
     * FOR PUT PERSONAL DATA
     */

    @PUT("personalfull/{name}/info/{idorder}/logid/{logid}")
    Call<ResponseDetailPersonalInfo> savePersonalInfo(@Body ModelDataPersonal modelDataPersonal, @Path("name") String name, @Path("idorder") String idOrder, @Path("logid") String logid);

    @PUT("personalfull/{name}/info/{idorder}/logid/{logid}")
    Call<ResponseDetailPersonalSgInfo> savePersonalSgInfo(@Body ModelDataPersonalSG modelDataPersonalSG, @Path("name") String name, @Path("idorder") String idOrder, @Path("logid") String logid);

    @PUT("personalfull/{name}/addr/{idcustdata}")
    Call<ResponseDetailPersonalAddr> savePersonalAddr(@Body ModelDataAddress modelDataAddress, @Path("name") String name, @Path("idcustdata") String idCustData);

    @PUT("personalfull/{name}/other/{idcustdata}")
    Call<ResponseDetailPersonalOther> savePersonalOther(@Body ModelDataMainData modelDataMainData, @Path("name") String name, @Path("idcustdata") String idCustData);

    @PUT("personalfull/{name}/job/{idcustdata}")
    Call<ResponseDetailPersonalJob> savePersonalJob(@Body ModelDataJobData modelDataJobData, @Path("name") String name, @Path("idcustdata") String idCustData);

    @PUT("personalfull/{name}/emergency/{idcustdata}")
    Call<ResponseDetailPersonalEmergency> savePersonalEmergency(@Body ModelDataEmergencyContact modelDataEmergencyContact, @Path("name") String name, @Path("idcustdata") String idCustData);

    @PUT("personalfull/{name}/fin/{idcustdata}")
    Call<ResponseDetailPersonalFinancial> savePersonalFinancial(@Body ModelDataFinancial modelDataFinancial, @Path("name") String name, @Path("idcustdata") String idCustData);

    /*
     * FOR GET PERSONAL DATA
     */

    @GET("personalfull/{name}/info/{idorder}")
    Call<ResponseDetailPersonalInfo> getDetailPersonalInfo(@Path("name") String name, @Path("idorder") String idOrder);

    @GET("personalfull/{name}/info/{idorder}")
    Call<ResponseDetailPersonalSgInfo> getDetailPersonalSgInfo(@Path("name") String name, @Path("idorder") String idOrder);

    @GET("personalfull/{name}/addr/{idcustdata}")
    Call<ResponseDetailPersonalAddr> getDetailPersonalAddr(@Path("name") String name, @Path("idcustdata") String idCustData);

    @GET("personalfull/{name}/other/{idcustdata}")
    Call<ResponseDetailPersonalOther> getDetailPersonalOther(@Path("name") String name, @Path("idcustdata") String idCustData);

    @GET("personalfull/{name}/job/{idcustdata}")
    Call<ResponseDetailPersonalJob> getDetailPersonalJob(@Path("name") String name, @Path("idcustdata") String idCustData);

    @GET("personalfull/{name}/emergency/{idcustdata}")
    Call<ResponseDetailPersonalEmergency> getDetailPersonalEmergency(@Path("name") String name, @Path("idcustdata") String idCustData);

    @GET("personalfull/{name}/fin/{idcustdata}")
    Call<ResponseDetailPersonalFinancial> getDetailPersonalFinancial(@Path("name") String name, @Path("idcustdata") String idCustData);

    /*
     * FOR BANDING
     */

    @PUT("log/{name}/logid/{logid}")
    Call<ResponseBanding> saveComment(@Body ModelBanding modelBanding, @Path("name") String name, @Path("logid") String logid);

    @GET("dashboard/{name}/userid/{userid}")
    Call<ResponseDashboard> getDashboard(@Path("name") String name, @Path("userid") String userid);
}

