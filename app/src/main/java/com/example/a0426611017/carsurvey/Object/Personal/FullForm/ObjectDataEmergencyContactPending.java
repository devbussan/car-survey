package com.example.a0426611017.carsurvey.Object.Personal.FullForm;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.a0426611017.carsurvey.Model.Personal.ModelDataEmergencyContact;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by 0426611017 on 2/9/2018.
 */

public class ObjectDataEmergencyContactPending {
    private View view;

    private EditText emergencyContactName, customerRelationship,
            emergencyMobilePhone1, emergencyMobilePhone2,
            emergencyContactAddress, emergencyRt, emergencyRw,
            emergencyZipCode, emergencyKelurahan, emergencyKecamatan,
            emergencyCity, emergencyPhone1;

    public ObjectDataEmergencyContactPending(ViewGroup viewGroup, LayoutInflater inflater) {
        Context context = viewGroup.getContext();
        ViewGroup container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_data_emergency_contact_pending, container, false);

        setField();
    }

    public View getView() {
        return view;
    }

    public void setField() {
        emergencyContactName = view.findViewById(R.id.edit_text_emergency_contact_name_pending);
        customerRelationship = view.findViewById(R.id.edit_text_customer_relationship_pending);
        emergencyMobilePhone1 = view.findViewById(R.id.edit_text_emergency_mobile_phone_1_pending);
        emergencyMobilePhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (emergencyMobilePhone1.getText().length() < 7) {
                    emergencyMobilePhone1.setError("Minimum 7 digits");
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 7) {
                    emergencyMobilePhone1.setError("Minimum 7 digits");
                }
            }
        });

        emergencyMobilePhone2 = view.findViewById(R.id.edit_text_emergency_mobile_phone_2_pending);
        emergencyMobilePhone2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0 && s.length() < 7) {
                    emergencyMobilePhone2.setError("Minimum 7 digits");
                }
            }
        });

        emergencyContactAddress = view.findViewById(R.id.edit_text_emergency_contact_address_pending);
        emergencyRt = view.findViewById(R.id.edit_text_emergency_rt_pending);
        emergencyRw = view.findViewById(R.id.edit_text_emergency_rw_pending);
        emergencyZipCode = view.findViewById(R.id.edit_text_emergency_zip_code_pending);
        emergencyKelurahan = view.findViewById(R.id.edit_text_emergency_kelurahan_pending);
        emergencyKecamatan = view.findViewById(R.id.edit_text_emergency_kecamatan_pending);
        emergencyCity = view.findViewById(R.id.edit_text_emergency_city_pending);
        emergencyPhone1 = view.findViewById(R.id.edit_text_emergency_phone_1_pending);
        emergencyPhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (emergencyPhone1.getText().length() < 15) {
                    emergencyPhone1.setError("Minimum 7 digits");
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 7) {
                    emergencyPhone1.setError("Minimum 7 digits");
                }
            }
        });
    }

    public void setForm(ModelDataEmergencyContact modelDataEmergencyContact) {
        emergencyContactName.setText(modelDataEmergencyContact.getEmergencyname());
        customerRelationship.setText(modelDataEmergencyContact.getRelation());
        emergencyMobilePhone1.setText(modelDataEmergencyContact.getEmergencymphn1());

        emergencyMobilePhone2.setText(modelDataEmergencyContact.getEmergencymphn2());

        emergencyContactAddress.setText(modelDataEmergencyContact.getEmergencyaddr());
        emergencyRt.setText(modelDataEmergencyContact.getEmergencyrt());
        emergencyRw.setText(modelDataEmergencyContact.getEmergencyrw());
        emergencyZipCode.setText(modelDataEmergencyContact.getEmergencyzipcode());
        emergencyKelurahan.setText(modelDataEmergencyContact.getEmergencykelurahan());
        emergencyKecamatan.setText(modelDataEmergencyContact.getEmergencykecamatan());
        emergencyCity.setText(modelDataEmergencyContact.getEmergencycity());
        emergencyPhone1.setText(modelDataEmergencyContact.getEmergencyphn());
    }

    public ModelDataEmergencyContact getForm() {
        ModelDataEmergencyContact modelDataEmergencyContact = new ModelDataEmergencyContact();

        modelDataEmergencyContact.setEmergencyname(emergencyContactName.getText().toString());
        modelDataEmergencyContact.setRelation(customerRelationship.getText().toString());
        modelDataEmergencyContact.setEmergencymphn1(emergencyMobilePhone1.getText().toString());
        modelDataEmergencyContact.setEmergencymphn2(emergencyMobilePhone2.getText().toString());
        modelDataEmergencyContact.setEmergencyaddr(emergencyContactAddress.getText().toString());
        modelDataEmergencyContact.setEmergencyrt(emergencyRt.getText().toString());
        modelDataEmergencyContact.setEmergencyrw(emergencyRw.getText().toString());
        modelDataEmergencyContact.setEmergencyzipcode(emergencyZipCode.getText().toString());
        modelDataEmergencyContact.setEmergencykelurahan(emergencyKelurahan.getText().toString());
        modelDataEmergencyContact.setEmergencykecamatan(emergencyKecamatan.getText().toString());
        modelDataEmergencyContact.setEmergencycity(emergencyCity.getText().toString());
        modelDataEmergencyContact.setEmergencyphn(emergencyPhone1.getText().toString());

        modelDataEmergencyContact.setModified(true);

        return modelDataEmergencyContact;
    }

    public ModelDataEmergencyContact getComplete() {
        ModelDataEmergencyContact modelDataEmergencyContact = getForm();
        modelDataEmergencyContact.setComplete(true);

        if (modelDataEmergencyContact.getEmergencyname().length() == 0) {
            emergencyContactName.setError("Emeregency Contact Name is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencyname().trim().length() == 0) {
            emergencyContactName.setError("Emeregency Contact Name not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getRelation().length() == 0) {
            customerRelationship.setError("Customer Relationship is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getRelation().trim().length() == 0) {
            customerRelationship.setError("Customer Relationship not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getEmergencymphn1().length() == 0) {
            emergencyMobilePhone1.setError("Emergency Mobile Phone 1 is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencymphn1().trim().length() == 0) {
            emergencyMobilePhone1.setError("Emergency Mobile Phone not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencymphn1().trim().length() < 7) {
            emergencyMobilePhone1.setError("Minimum 7 digits");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getEmergencymphn2().length() > 0 && modelDataEmergencyContact.getEmergencymphn2().trim().length() < 7) {
            emergencyMobilePhone2.setError("Minimum 7 digits");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getEmergencyaddr().length() == 0) {
            emergencyContactAddress.setError("Emergency Contact Address is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencyaddr().trim().length() == 0) {
            emergencyContactAddress.setError("Emergency Contact Address not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getEmergencyrt().length() == 0) {
            emergencyRt.setError("Emergency RT is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencyrt().trim().length() == 0) {
            emergencyRt.setError("Emergency RT not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getEmergencyrw().length() == 0) {
            emergencyRw.setError("Emergency RW is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencyrw().trim().length() == 0) {
            emergencyRw.setError("Emergency RW not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getEmergencyzipcode().length() == 0) {
            emergencyZipCode.setError("Emergency Zip Code is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencyzipcode().trim().length() == 0) {
            emergencyZipCode.setError("Emergency Zip Code not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getEmergencykelurahan().length() == 0) {
            emergencyKelurahan.setError("Emergency Kelurahan is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencykelurahan().trim().length() == 0) {
            emergencyKelurahan.setError("Emergency Kelurahan not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getEmergencykecamatan().length() == 0) {
            emergencyKecamatan.setError("Emergency Kecamatan is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencykecamatan().trim().length() == 0) {
            emergencyKecamatan.setError("Emergency Kecamatan not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getEmergencycity().length() == 0) {
            emergencyCity.setError("Emergency City is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencycity().trim().length() == 0) {
            emergencyCity.setError("Emergency City not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        }

        if (modelDataEmergencyContact.getEmergencyphn().length() == 0) {
            emergencyPhone1.setError("Emergency Phone 1 is required");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencyphn().trim().length() == 0) {
            emergencyPhone1.setError("Emergency Phone 1 not allowed whitespaces only");
            modelDataEmergencyContact.setComplete(false);
        } else if (modelDataEmergencyContact.getEmergencyphn().trim().length() < 7) {
            emergencyPhone1.setError("Minimum 7 digits");
            modelDataEmergencyContact.setComplete(false);
        }

        return modelDataEmergencyContact;
    }
}
