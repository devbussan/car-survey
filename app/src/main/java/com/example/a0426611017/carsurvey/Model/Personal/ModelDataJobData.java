package com.example.a0426611017.carsurvey.Model.Personal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 0426611017 on 2/9/2018.
 */

public class ModelDataJobData implements Serializable {
    @SerializedName("CUST_DATA_ID")
    private String cust_data_id;

    @SerializedName("PERSONAL_JOB_ID")
    private String personal_job_id;

    @SerializedName("PROFFESIONNAME")
    private String proffesionname;

    @SerializedName("JOBPOS")
    private String jobpos;

    @SerializedName("JOBSTAT")
    private String jobstat;

    @SerializedName("COMPANYNAME")
    private String companyname;

    @SerializedName("INDUSTRYTYPE")
    private String industrytype;

    @SerializedName("ESTABLISHMENTDTM")
    private String establishmentdtm;

    @SerializedName("JOBADDR")
    private String jobaddr;

    @SerializedName("JOBRT")
    private String jobrt;

    @SerializedName("JOBRW")
    private String jobrw;

    @SerializedName("JOBZIPODE")
    private String jobzipode;

    @SerializedName("JOBKELURAHAN")
    private String jobkelurahan;

    @SerializedName("JOBKECAMATAN")
    private String jobkecamatan;

    @SerializedName("JOBCITY")
    private String jobcity;

    @SerializedName("JOBPHONE")
    private String jobphone;

    @SerializedName("USRCRT")
    private String usrcrt;

    @SerializedName("STATUS")
    private String status;

    @SerializedName("PERSONAL_JOB")
    private String personal_job_return;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getCust_data_id() {
        return cust_data_id;
    }

    public void setCust_data_id(String cust_data_id) {
        this.cust_data_id = cust_data_id;
    }

    public String getPersonal_job_id() {
        return personal_job_id;
    }

    public void setPersonal_job_id(String personal_job_id) {
        this.personal_job_id = personal_job_id;
    }

    public String getProffesionname() {
        return proffesionname;
    }

    public void setProffesionname(String proffesionname) {
        this.proffesionname = proffesionname;
    }

    public String getJobpos() {
        return jobpos;
    }

    public void setJobpos(String jobpos) {
        this.jobpos = jobpos;
    }

    public String getJobstat() {
        return jobstat;
    }

    public void setJobstat(String jobstat) {
        this.jobstat = jobstat;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getIndustrytype() {
        return industrytype;
    }

    public void setIndustrytype(String industrytype) {
        this.industrytype = industrytype;
    }

    public String getEstablishmentdtm() {
        return establishmentdtm;
    }

    public void setEstablishmentdtm(String establishmentdtm) {
        this.establishmentdtm = establishmentdtm;
    }

    public String getJobaddr() {
        return jobaddr;
    }

    public void setJobaddr(String jobaddr) {
        this.jobaddr = jobaddr;
    }

    public String getJobrt() {
        return jobrt;
    }

    public void setJobrt(String jobrt) {
        this.jobrt = jobrt;
    }

    public String getJobrw() {
        return jobrw;
    }

    public void setJobrw(String jobrw) {
        this.jobrw = jobrw;
    }

    public String getJobzipode() {
        return jobzipode;
    }

    public void setJobzipode(String jobzipode) {
        this.jobzipode = jobzipode;
    }

    public String getJobkelurahan() {
        return jobkelurahan;
    }

    public void setJobkelurahan(String jobkelurahan) {
        this.jobkelurahan = jobkelurahan;
    }

    public String getJobkecamatan() {
        return jobkecamatan;
    }

    public void setJobkecamatan(String jobkecamatan) {
        this.jobkecamatan = jobkecamatan;
    }

    public String getJobcity() {
        return jobcity;
    }

    public void setJobcity(String jobcity) {
        this.jobcity = jobcity;
    }

    public String getJobphone() {
        return jobphone;
    }

    public void setJobphone(String jobphone) {
        this.jobphone = jobphone;
    }

    public String getUsrcrt() {
        return usrcrt;
    }

    public void setUsrcrt(String usrcrt) {
        this.usrcrt = usrcrt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getPersonal_job_return() {
        return personal_job_return;
    }

    public void setPersonal_job_return(String personal_job_return) {
        this.personal_job_return = personal_job_return;
    }
}

