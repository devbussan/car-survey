package com.example.a0426611017.carsurvey.Fragment.Personal.FullForm;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataFinancial;
import com.example.a0426611017.carsurvey.Object.Personal.FullForm.ObjectDataFinancialPending;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalFinancial;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class FragmentDataFinancialPending extends Fragment {
    private View view;
    private Context context;
    private String nameFile, filePath;
    private int cameraIdxButton = 0;
    private TakePicture takePicture = new TakePicture();
    private PreviewImage previewImage = new PreviewImage();
    private TextView fileNameMonthlyFixedIncomeImage,
            fileNameMonthlyFixedIncomeImageUrl;

    private ObjectDataFinancialPending objectDataFinancialPending;
    private ModelDataFinancial modelDataFinancial = new ModelDataFinancial();
    private FileFunction fileFunction = new FileFunction();

    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private String personalCustDataId = null;
    private String name = "";
    private Gson gson = new Gson();
    private boolean isNew = true;

    private ResponseDetailPersonalFinancial responseDetailPersonalFinancial = new ResponseDetailPersonalFinancial();

    public FragmentDataFinancialPending newInstance(ModelDataFinancial modelDataFinancial, boolean isNew, String custId, String name) {
        FragmentDataFinancialPending fragmentDataFinancialPending = new FragmentDataFinancialPending();
        Bundle args = new Bundle();
        args.putSerializable("modeFinPersonal", modelDataFinancial);
        args.putString("personalCustDataId", custId);
        args.putBoolean(Constans.KEY_IS_NEW, isNew);
        args.putString("name", name);
        fragmentDataFinancialPending.setArguments(args);
        return fragmentDataFinancialPending;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelDataFinancial = (ModelDataFinancial) getArguments().getSerializable("modeFinPersonal");
            personalCustDataId = getArguments().getString("personalCustDataId");
            isNew = getArguments().getBoolean(Constans.KEY_IS_NEW);
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        objectDataFinancialPending = new ObjectDataFinancialPending(container, inflater);
        view = objectDataFinancialPending.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if(modelDataFinancial.isModified()){
            objectDataFinancialPending.setForm(modelDataFinancial);
        }else{
            if (!isNew) {
                getDetailFinancial(name, personalCustDataId);
            }
        }

        fileNameMonthlyFixedIncomeImage = objectDataFinancialPending.getNamePersonalMonthlyFixedIncomeImage();
        fileNameMonthlyFixedIncomeImageUrl = objectDataFinancialPending.getNamePersonalMonthlyFixedIncomeImageUrl();
        Button buttonMonthlyFixedIncomeImage = objectDataFinancialPending.getButtonMonthlyFixedIncomeImage();

        callCamera(buttonMonthlyFixedIncomeImage, 1);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            objectDataFinancialPending.setEnabledButtonImage();
        }

        return view;
    }

    private void callCamera(Button button, int idx) {
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(v, idxButton);

            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                objectDataFinancialPending.setEnabledButtonImage();
            }
        }
    }

    public void takePicture(View view, int idx) {
        this.cameraIdxButton = idx;
        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        filePath = takePicture.getFilePath();
        startActivityForResult(intent, 100);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                switch (cameraIdxButton) {
                    case 1:
                     /*fileNameMonthlyFixedIncomeImage.setText(nameFile);*/
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        objectDataFinancialPending.setFilePersonalMonthlyFixedIncomeImage(nameFile);
                        objectDataFinancialPending.setFilePersonalMonthlyFixedIncomeImageUrl(filePath);
                        previewImage.setPreviewImage(nameFile, context, fileNameMonthlyFixedIncomeImage);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public ModelDataFinancial getForm(){
        return objectDataFinancialPending.getForm();
    }

    public void setForm(ModelDataFinancial modelDataFinancial){
        objectDataFinancialPending.setForm(modelDataFinancial);
    }

    public ModelDataFinancial getComplete(){
        return objectDataFinancialPending.getComplete();
    }

    private void getDetailFinancial(final String name, final String idcustdata){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalFinancial> finDetail = apiInterface.getDetailPersonalFinancial(name, idcustdata);
            finDetail.enqueue(new Callback<ResponseDetailPersonalFinancial>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalFinancial> call, Response<ResponseDetailPersonalFinancial>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataFinancial = response.body().getModelDataFinancial();
                        Log.d("Result", gson.toJson(modelDataFinancial));

                        setForm(modelDataFinancial);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalFinancial = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalFinancial.class);
                            error = responseDetailPersonalFinancial.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailFinancial(name, idcustdata);
                                    }
                                });
                                b.show();
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.personal_fin_full_edit), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailFinancial(name, idcustdata);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailFinancial(name, idcustdata);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalFinancial> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailFinancial(name, idcustdata);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailFinancial(name, idcustdata);
                }
            });
            b.show();
            Log.e("Error","No Internet Connectivity");
            loadingDialog.dismiss();
        }
    }
}

