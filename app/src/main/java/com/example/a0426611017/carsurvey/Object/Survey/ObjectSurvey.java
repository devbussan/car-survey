package com.example.a0426611017.carsurvey.Object.Survey;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Currency;
import com.example.a0426611017.carsurvey.MainFunction.DatePicker;
import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLSurvey;
import com.example.a0426611017.carsurvey.Model.Survey.ModelSurvey;
import com.example.a0426611017.carsurvey.R;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by c201017001 on 12/02/2018.
 */

public class ObjectSurvey {

    private View view;
    private ViewGroup container;
    private LayoutInflater inflater;
    private Context context;
    private ModelDDLSurvey ddlSurvey;
    private DatePicker datePicker = new DatePicker();
    private PreviewImage previewImage = new PreviewImage();

    private TextView labelPersonalId, labelTanggalTerima, labelTanggalSurvey, labelAlamatSurvey, labelLokasiSurvey,
            labelSourceIncoming, labelNamaSourceIncoming, labelNamaPemberiInformasi, labelNamaSourceStatus,
            labelNamaSourceStatusKet, labelPengajuanCalonDebitur, labelTipeRumah, labelLokasiRumah,
            labelHargaSewaRumah, labelJenisBangunan, labelKondisiBangunanRumah, labelKetersediaanGarasi,
            labelLuasBangunan, labelLuasTanah, labelLamaTinggal, labelNamaRekListrik, labelNamaRekListrikStatus,
            labelAnggotaOrmas, labelJumlahMobil, labelStatusKepemilikanMobil, labelStatusKepemilikanMobilImage,
            labelJumlahMotor, labelStatusKepemilikanMotor, labelStatusKepemilikanMotorImage, labelKarakterKonsumen,
            labelJumlahTanggungan, labelTotalPenghasilan, labelLamaKerja, labelPosisi, labelNamaAtasanLangsung,
            labelNoTeleponAtasanLangsung, labelBiayaRutinBulanan, labelAngsuranTagihanLain,
            labelStatusKepemilikanMobilImage_url, labelStatusKepemilikanMotorImage_url;

    private EditText editTextTanggalTerima, editTextTanggalSurvey, editTextAlamatSurvey,
            editTextNamaSourceIncoming, editTextNamaPemberiInformasi, editTextNamaSourceStatusKet,
            editTextHargaSewaRumah, editTextLuasBangunan, editTextLuasTanah, editTextLamaTinggal, editTextNamaRekListrik,
            editTextJumlahMobil, editTextJumlahMotor, editTextJumlahTanggungan,
            editTextTotalPenghasilan, editTextLamaKerja, editTextPosisi, editTextNamaAtasanLangsung,
            editTextNoTeleponAtasanLangsung, editTextBiayaRutinTiapBulan, editTextAngsuranTagianLain,
            editTextNamaSourceStatusSurvey, edit_text_company_order_hid;


    private Spinner spinnerLokasiSurvey, spinnerSourceIncoming, spinnerPengajuanCalonDebitur, spinnerTipeRumah,
            spinnerLokasiRumah, spinnerJenisBangunan, spinnerKondisiBangunan, spinnerKetersediaanGarasi, spinnerRekListrikStatus,
            spinnerAnggotaOrmas, spinnerStatusKepemilikanMobil, spinnerStatusKepemilikanMotor, spinnerKarakterKonsumen;

    private TextView textViewIdMobilImg, textViewIdMotorImg,
            textViewIdMotorImgUrl, textViewIdMobilImgUrl;

    private Button buttonIdMotor, buttonIdMobil;

    public ObjectSurvey(ViewGroup container, LayoutInflater inflater, ModelDDLSurvey ddlSurvey) {
        this.container = container;
        this.ddlSurvey = ddlSurvey;
        this.context = container.getContext();
        this.view = inflater.inflate(R.layout.fragment_survey, container, false);

        setField();
        setFieldLabel();
    }

    public View getView() {
        return view;
    }

    public TextView getTextViewIdMobilImg() {
        return textViewIdMobilImg;
    }

    public void setTextViewIdMobilImg(String textViewIdMobilImg) {
        this.textViewIdMobilImg.setText(textViewIdMobilImg);
    }

    public TextView getTextViewIdMotorImg() {
        return textViewIdMotorImg;
    }

    public void setTextViewIdMotorImg(String textViewIdMotorImg) {
        this.textViewIdMotorImg.setText(textViewIdMotorImg);
    }

    public TextView getTextViewIdMotorImgUrl() {
        return textViewIdMotorImgUrl;
    }

    public void setTextViewIdMotorImgUrl(String textViewIdMotorImgUrl) {
        this.textViewIdMotorImgUrl.setText(textViewIdMotorImgUrl);
    }

    public TextView getTextViewIdMobilImgUrl() {
        return textViewIdMobilImgUrl;
    }

    public void setTextViewIdMobilImgUrl(String textViewIdMobilImgUrl) {
        this.textViewIdMobilImgUrl.setText(textViewIdMobilImgUrl);
    }

    public Button getButtonIdMobil() {
        return buttonIdMobil;
    }

    public Button getButtonIdMotor() {
        return buttonIdMotor;
    }

    public void setEnabledButtonImage() {
        buttonIdMobil.setEnabled(true);
        buttonIdMotor.setEnabled(true);
    }

    private void setField() {
        editTextTanggalTerima = view.findViewById(R.id.edit_text_tanggal_terima_order_survey);
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, editTextTanggalTerima, context);
        editTextTanggalSurvey = view.findViewById(R.id.edit_text_tanggal_survey);
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, editTextTanggalSurvey, context);

        editTextAlamatSurvey = view.findViewById(R.id.edit_text_alamat_survey);
        spinnerLokasiSurvey = view.findViewById(R.id.spinner_lokasi_survey);
        spinnerSourceIncoming = view.findViewById(R.id.spinner_source_incoming_survey);
        editTextNamaSourceIncoming = view.findViewById(R.id.edit_text_nama_source_incoming_survey);
        editTextNamaPemberiInformasi = view.findViewById(R.id.edit_text_nama_pemberi_informasi_survey);
        editTextNamaSourceStatusSurvey = view.findViewById(R.id.edit_text_nama_source_status_survey);
        editTextNamaSourceStatusKet = view.findViewById(R.id.edit_text_nama_source_status_ket_survey);

        spinnerPengajuanCalonDebitur = view.findViewById(R.id.spinner_pengajuan_calon_debitur_survey);
        spinnerTipeRumah = view.findViewById(R.id.spinner_tipe_rumah_survey);
        spinnerLokasiRumah = view.findViewById(R.id.spinner_lokasi_rumah_survey);
        editTextHargaSewaRumah = view.findViewById(R.id.edit_text_harga_sewa_rumah_survey);
        editTextHargaSewaRumah.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    editTextHargaSewaRumah.removeTextChangedListener(this);

                    String formatted = Currency.set(editTextHargaSewaRumah);
                    editTextHargaSewaRumah.setText(formatted.replaceAll("[$]",""));
                    editTextHargaSewaRumah.setSelection(formatted.length()-1);
                    editTextHargaSewaRumah.addTextChangedListener(this);
                }
            }
        });
        spinnerJenisBangunan = view.findViewById(R.id.spinner_jenis_bangunan_survey);
        spinnerKondisiBangunan = view.findViewById(R.id.spinner_kondisi_bangunan_rumah_survey);
        spinnerKetersediaanGarasi = view.findViewById(R.id.spinner_ketersediaan_garasi_survey);
        editTextLuasBangunan = view.findViewById(R.id.edit_text_luas_bangunan_survey);
        editTextLuasTanah = view.findViewById(R.id.edit_text_luas_tanah_survey);
        editTextLamaTinggal = view.findViewById(R.id.edit_text_lama_tinggal_survey);

        editTextNamaRekListrik = view.findViewById(R.id.edit_text_nama_rek_listrik_survey);
        spinnerRekListrikStatus = view.findViewById(R.id.spinner_nama_rek_listrik_status_survey);
        spinnerAnggotaOrmas = view.findViewById(R.id.spinner_anggota_ormas_survey);
        editTextJumlahMobil = view.findViewById(R.id.edit_text_jumlah_mobil_survey);

        spinnerStatusKepemilikanMobil = view.findViewById(R.id.spinner_status_kepemilikan_mobil_survey);

        textViewIdMobilImg = view.findViewById(R.id.text_view_nama_image_status_kepemilikan_mobil_survey);
        textViewIdMobilImg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    textViewIdMobilImg.setError(null);
                }
            }
        });
        textViewIdMobilImgUrl = view.findViewById(R.id.text_view_nama_image_status_kepemilikan_mobil_survey_url);

        editTextJumlahMotor = view.findViewById(R.id.edit_text_jumlah_motor_survey);

        spinnerStatusKepemilikanMotor = view.findViewById(R.id.spinner_status_kepemilikan_motor_survey);

        textViewIdMotorImg = view.findViewById(R.id.text_view_nama_image_status_kepemilikan_motor_survey);
        textViewIdMotorImg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    textViewIdMotorImg.setError(null);
                }
            }
        });
        textViewIdMotorImgUrl = view.findViewById(R.id.text_view_nama_image_status_kepemilikan_motor_survey_url);

        spinnerKarakterKonsumen = view.findViewById(R.id.spinner_karakter_konsumen_survey);
        editTextJumlahTanggungan = view.findViewById(R.id.edit_text_jumlah_tanggungan_survey);
        editTextTotalPenghasilan = view.findViewById(R.id.edit_text_total_penghasilan_survey);
        editTextTotalPenghasilan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    editTextTotalPenghasilan.removeTextChangedListener(this);

                    String formatted = Currency.set(editTextTotalPenghasilan);
                    editTextTotalPenghasilan.setText(formatted.replaceAll("[$]",""));
                    editTextTotalPenghasilan.setSelection(formatted.length()-1);
                    editTextTotalPenghasilan.addTextChangedListener(this);
                }
            }
        });
        editTextLamaKerja = view.findViewById(R.id.edit_text_lama_kerja_survey);
        editTextPosisi = view.findViewById(R.id.edit_text_posisi_survey);
        editTextNamaAtasanLangsung = view.findViewById(R.id.edit_text_nama_atasan_langsung_survey);
        editTextNoTeleponAtasanLangsung = view.findViewById(R.id.edit_text_no_telepon_atasan_langsung_survey);
        editTextNoTeleponAtasanLangsung.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 7) {
                    editTextNoTeleponAtasanLangsung.setError("Minimum 7 digits");
                }
            }
        });
        editTextBiayaRutinTiapBulan = view.findViewById(R.id.edit_text_biaya_rutin_bulanan_survey);
        editTextBiayaRutinTiapBulan.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    editTextBiayaRutinTiapBulan.removeTextChangedListener(this);

                    String formatted = Currency.set(editTextBiayaRutinTiapBulan);
                    editTextBiayaRutinTiapBulan.setText(formatted.replaceAll("[$]",""));
                    editTextBiayaRutinTiapBulan.setSelection(formatted.length()-1);
                    editTextBiayaRutinTiapBulan.addTextChangedListener(this);
                }
            }
        });

        editTextAngsuranTagianLain = view.findViewById(R.id.edit_text_angsuran_tagihan_lain_survey);
        editTextAngsuranTagianLain.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")) {
                    editTextAngsuranTagianLain.removeTextChangedListener(this);

                    String formatted = Currency.set(editTextAngsuranTagianLain);
                    editTextAngsuranTagianLain.setText(formatted.replaceAll("[$]",""));
                    editTextAngsuranTagianLain.setSelection(formatted.length()-1);
                    editTextAngsuranTagianLain.addTextChangedListener(this);
                }
            }
        });

        if (ddlSurvey != null) {
            spinnerLokasiSurvey.setAdapter(setAdapterItemList(ddlSurvey.getLokasiSurvey()));
            spinnerSourceIncoming.setAdapter(setAdapterItemList(ddlSurvey.getSourceIncoming()));
            spinnerPengajuanCalonDebitur.setAdapter(setAdapterItemList(ddlSurvey.getPengajuanCalonDebitur()));
            spinnerTipeRumah.setAdapter(setAdapterItemList(ddlSurvey.getTipeRumah()));
            spinnerLokasiRumah.setAdapter(setAdapterItemList(ddlSurvey.getLokasiRumah()));
            spinnerJenisBangunan.setAdapter(setAdapterItemList(ddlSurvey.getJenisBangunan()));
            spinnerKondisiBangunan.setAdapter(setAdapterItemList(ddlSurvey.getKondisiBangunan()));
            spinnerKetersediaanGarasi.setAdapter(setAdapterItemList(ddlSurvey.getKetersediaanGarasi()));
            spinnerRekListrikStatus.setAdapter(setAdapterItemList(ddlSurvey.getrListrikHubunganKonsumen()));
            spinnerAnggotaOrmas.setAdapter(setAdapterItemList(ddlSurvey.getAnggotaOrmas()));
            spinnerStatusKepemilikanMobil.setAdapter(setAdapterItemList(ddlSurvey.getStatusKepemilikanMobil()));
            spinnerStatusKepemilikanMotor.setAdapter(setAdapterItemList(ddlSurvey.getStatusKepemilikanMotor()));
            spinnerKarakterKonsumen.setAdapter(setAdapterItemList(ddlSurvey.getKarakterKonsumen()));
        }

        buttonIdMobil = view.findViewById(R.id.button_camera_status_kepemilikan_mobil_image_survey);
        buttonIdMotor = view.findViewById(R.id.button_camera_status_kepemilikan_motor_image_survey);

        edit_text_company_order_hid = view.findViewById(R.id.company_order_id);

        editTextJumlahMotor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (editTextJumlahMotor.getText().toString().equals("0")) {
                    buttonIdMotor.setEnabled(false);
                    spinnerStatusKepemilikanMotor.setSelection(ddlSurvey.getStatusKepemilikanMotor().indexOf("TIDAK PUNYA"));
                    spinnerStatusKepemilikanMotor.setEnabled(false);
                    spinnerStatusKepemilikanMotor.setClickable(false);
                } else {
                    buttonIdMotor.setEnabled(true);
                    spinnerStatusKepemilikanMotor.setEnabled(true);
                    spinnerStatusKepemilikanMotor.setClickable(true);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editTextJumlahMotor.getText().toString().equals("0")) {
                    buttonIdMotor.setEnabled(false);
                    spinnerStatusKepemilikanMotor.setSelection(ddlSurvey.getStatusKepemilikanMotor().indexOf("TIDAK PUNYA"));
                    spinnerStatusKepemilikanMotor.setEnabled(false);
                    spinnerStatusKepemilikanMotor.setClickable(false);
                } else {
                    buttonIdMotor.setEnabled(true);
                    spinnerStatusKepemilikanMotor.setEnabled(true);
                    spinnerStatusKepemilikanMotor.setClickable(true);
                }
            }
        });

        editTextJumlahMobil.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (editTextJumlahMobil.getText().toString().equals("0")) {
                    buttonIdMobil.setEnabled(false);
                    spinnerStatusKepemilikanMobil.setSelection(ddlSurvey.getStatusKepemilikanMobil().indexOf("TIDAK PUNYA"));
                    spinnerStatusKepemilikanMobil.setEnabled(false);
                    spinnerStatusKepemilikanMobil.setClickable(false);
                } else {
                    buttonIdMobil.setEnabled(true);
                    spinnerStatusKepemilikanMobil.setEnabled(true);
                    spinnerStatusKepemilikanMobil.setClickable(true);
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (editTextJumlahMobil.getText().toString().equals("0")) {
                    buttonIdMobil.setEnabled(false);
                    spinnerStatusKepemilikanMobil.setSelection(ddlSurvey.getStatusKepemilikanMobil().indexOf("TIDAK PUNYA"));
                    spinnerStatusKepemilikanMobil.setEnabled(false);
                    spinnerStatusKepemilikanMobil.setClickable(false);
                } else {
                    buttonIdMobil.setEnabled(true);
                    spinnerStatusKepemilikanMobil.setEnabled(true);
                    spinnerStatusKepemilikanMobil.setClickable(true);
                }
            }
        });
    }

    public ModelSurvey getForm() {
        ModelSurvey modelSurvey = new ModelSurvey();

        modelSurvey.setDtmorder(editTextTanggalTerima.getText().toString());

        if (modelSurvey.getDtmorder() != null) {
            if (!modelSurvey.getDtmorder().equals("")) {
                modelSurvey.setDtmorder(
                        modelSurvey.getDtmorder().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(modelSurvey.getDtmorder().split(" ")[1])),
                                2,
                                "0"
                        )
                                + modelSurvey.getDtmorder().split(" ")[0]
                );
            }
        }

        modelSurvey.setSurveydate(editTextTanggalSurvey.getText().toString());

        if (modelSurvey.getSurveydate() != null) {
            if (!modelSurvey.getSurveydate().equals("")) {
                modelSurvey.setSurveydate(
                        modelSurvey.getSurveydate().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(modelSurvey.getSurveydate().split(" ")[1])),
                                2,
                                "0"
                        )
                                + modelSurvey.getSurveydate().split(" ")[0]
                );
            }
        }

        modelSurvey.setSurveyaddr(editTextAlamatSurvey.getText().toString());
        modelSurvey.setSurveylocation(spinnerLokasiSurvey.getSelectedItem().toString());
        modelSurvey.setSrcincoming(spinnerSourceIncoming.getSelectedItem().toString());
        modelSurvey.setSrcincomingname(editTextNamaSourceIncoming.getText().toString());
        modelSurvey.setInforsrcname(editTextNamaPemberiInformasi.getText().toString());
        modelSurvey.setSrcnamestat(editTextNamaSourceStatusSurvey.getText().toString());
        modelSurvey.setSrcstatket(editTextNamaSourceStatusKet.getText().toString());

        modelSurvey.setClndebitur(spinnerPengajuanCalonDebitur.getSelectedItem().toString());
        modelSurvey.setHometype(spinnerTipeRumah.getSelectedItem().toString());
        modelSurvey.setHomeloc(spinnerLokasiRumah.getSelectedItem().toString());
        modelSurvey.setRentprice(editTextHargaSewaRumah.getText().toString().replaceAll("[,]", ""));

        modelSurvey.setBldtype(spinnerJenisBangunan.getSelectedItem().toString());
        modelSurvey.setBldcond(spinnerKondisiBangunan.getSelectedItem().toString());
        modelSurvey.setGaragestat(spinnerKetersediaanGarasi.getSelectedItem().toString());
        modelSurvey.setBldarea(editTextLuasBangunan.getText().toString());
        modelSurvey.setLuasbld(editTextLuasTanah.getText().toString());
        modelSurvey.setStaylength(editTextLamaTinggal.getText().toString());
        modelSurvey.setElctrybillname(editTextNamaRekListrik.getText().toString());
        modelSurvey.setElctrybillstat(spinnerRekListrikStatus.getSelectedItem().toString());
        modelSurvey.setIsormas(spinnerAnggotaOrmas.getSelectedItem().toString());

        modelSurvey.setNumofcar(editTextJumlahMobil.getText().toString());
        modelSurvey.setCarstat(spinnerStatusKepemilikanMobil.getSelectedItem().toString());
        modelSurvey.setCarimgurl(textViewIdMobilImgUrl.getText().toString());
        modelSurvey.setCarimg(textViewIdMobilImg.getText().toString());

        modelSurvey.setNumofmotorcycle(editTextJumlahMotor.getText().toString());
        modelSurvey.setMotorcyclestat(spinnerStatusKepemilikanMotor.getSelectedItem().toString());
        modelSurvey.setMotorcycleimgurl(textViewIdMotorImgUrl.getText().toString());
        modelSurvey.setMotorcycleimg(textViewIdMotorImg.getText().toString());

        modelSurvey.setCharactercust(spinnerKarakterKonsumen.getSelectedItem().toString());
        modelSurvey.setJmltanggungan(editTextJumlahTanggungan.getText().toString());
        modelSurvey.setTotalincome(editTextTotalPenghasilan.getText().toString().replaceAll("[,]", ""));
        modelSurvey.setLamakerja(editTextLamaKerja.getText().toString());
        modelSurvey.setPosition(editTextPosisi.getText().toString());
        modelSurvey.setAtasan(editTextNamaAtasanLangsung.getText().toString());
        modelSurvey.setAtasanphone(editTextNoTeleponAtasanLangsung.getText().toString());
        modelSurvey.setBiayarutin(editTextBiayaRutinTiapBulan.getText().toString().replaceAll("[,]", ""));
        modelSurvey.setTagihanlain(editTextAngsuranTagianLain.getText().toString().replaceAll("[,]", ""));
        modelSurvey.setOrderid(edit_text_company_order_hid.getText().toString());

        modelSurvey.setModified(true);

        return modelSurvey;
    }

    public void setForm(ModelSurvey modelSurvey) {
        Log.d("DDL SURVEY", ddlSurvey.getJenisBangunan().toString());

        if (ddlSurvey != null) {
            spinnerLokasiSurvey.setSelection(ddlSurvey.getLokasiSurvey().indexOf(modelSurvey.getSurveylocation()));
            spinnerSourceIncoming.setSelection(ddlSurvey.getSourceIncoming().indexOf(modelSurvey.getSrcincoming()));
            spinnerPengajuanCalonDebitur.setSelection(ddlSurvey.getPengajuanCalonDebitur().indexOf(modelSurvey.getClndebitur()));
            spinnerTipeRumah.setSelection(ddlSurvey.getTipeRumah().indexOf(modelSurvey.getHometype()));
            spinnerLokasiRumah.setSelection(ddlSurvey.getLokasiRumah().indexOf(modelSurvey.getHomeloc()));
            spinnerJenisBangunan.setSelection(ddlSurvey.getJenisBangunan().indexOf(modelSurvey.getBldtype()));
            spinnerKondisiBangunan.setSelection(ddlSurvey.getKondisiBangunan().indexOf(modelSurvey.getBldcond()));
            spinnerKetersediaanGarasi.setSelection(ddlSurvey.getKetersediaanGarasi().indexOf(modelSurvey.getGaragestat()));
            spinnerRekListrikStatus.setSelection(ddlSurvey.getrListrikHubunganKonsumen().indexOf(modelSurvey.getElctrybillstat()));
            spinnerAnggotaOrmas.setSelection(ddlSurvey.getAnggotaOrmas().indexOf(modelSurvey.getIsormas()));
            spinnerStatusKepemilikanMobil.setSelection(ddlSurvey.getStatusKepemilikanMobil().indexOf(modelSurvey.getCarstat()));
            spinnerStatusKepemilikanMotor.setSelection(ddlSurvey.getStatusKepemilikanMotor().indexOf(modelSurvey.getMotorcyclestat()));
            spinnerKarakterKonsumen.setSelection(ddlSurvey.getKarakterKonsumen().indexOf(modelSurvey.getCharactercust()));
        }

        if (modelSurvey.getDtmorder() != null && !modelSurvey.getDtmorder().equals("")) {
            modelSurvey.setDtmorder(
                    modelSurvey.getDtmorder().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelSurvey.getDtmorder().substring(4, 6))]
                            + " " + modelSurvey.getDtmorder().substring(0, 4)
            );
        }
        editTextTanggalTerima.setText(modelSurvey.getDtmorder());

        if (modelSurvey.getSurveydate() != null && !modelSurvey.getSurveydate().equals("")) {
            modelSurvey.setSurveydate(
                    modelSurvey.getSurveydate().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelSurvey.getSurveydate().substring(4, 6))]
                            + " " + modelSurvey.getSurveydate().substring(0, 4)
            );
        }
        editTextTanggalSurvey.setText(modelSurvey.getSurveydate());

        editTextAlamatSurvey.setText(modelSurvey.getSurveyaddr());
        editTextNamaSourceIncoming.setText(modelSurvey.getSrcincomingname());
        editTextNamaPemberiInformasi.setText(modelSurvey.getInforsrcname());
        editTextNamaSourceStatusSurvey.setText(modelSurvey.getSrcnamestat());
        editTextNamaSourceStatusKet.setText(modelSurvey.getSrcstatket());

        editTextHargaSewaRumah.setText(modelSurvey.getRentprice());
        editTextLuasBangunan.setText(modelSurvey.getBldarea());
        editTextLuasTanah.setText(modelSurvey.getLuasbld());
        editTextLamaTinggal.setText(modelSurvey.getStaylength());
        editTextNamaRekListrik.setText(modelSurvey.getElctrybillname());

        textViewIdMobilImgUrl.setText(modelSurvey.getCarimgurl());

        Log.d("Url image mobil", textViewIdMobilImgUrl.getText().toString());
        PreviewImage previewImage = new PreviewImage();
        String nameIdImage;
        if (modelSurvey.getCarimgurl().equals("") || modelSurvey.getCarimgurl() == null) {
            nameIdImage = "No selected file";
        } else {
            if (modelSurvey.getCarimgurl().startsWith("http")) {
                nameIdImage = modelSurvey.getCarimgurl().split("/")[6];
                previewImage.setPreviewImage(modelSurvey.getCarimgurl(), context, textViewIdMobilImg);
            } else {
                nameIdImage = modelSurvey.getCarimg();
                previewImage.setPreviewImage(nameIdImage, context, textViewIdMobilImg);
            }
        }
        textViewIdMobilImg.setText(nameIdImage);

        editTextJumlahMobil.setText(modelSurvey.getNumofcar());

        textViewIdMotorImgUrl.setText(modelSurvey.getMotorcycleimgurl());

        Log.d("Url image motor", textViewIdMotorImgUrl.getText().toString());
        PreviewImage previewImageMotor = new PreviewImage();
        String nameIdImageMotor;
        if (modelSurvey.getMotorcycleimgurl().equals("") || modelSurvey.getMotorcycleimgurl() == null) {
            nameIdImageMotor = "No selected file";
        } else {
            if (modelSurvey.getMotorcycleimgurl().startsWith("http")) {
                nameIdImageMotor = modelSurvey.getMotorcycleimgurl().split("/")[6];
                previewImageMotor.setPreviewImage(modelSurvey.getMotorcycleimgurl(), context, textViewIdMotorImg);
            } else {
                nameIdImageMotor = modelSurvey.getMotorcycleimg();
                previewImageMotor.setPreviewImage(nameIdImageMotor, context, textViewIdMotorImg);
            }
        }
        textViewIdMotorImg.setText(nameIdImageMotor);

        editTextJumlahTanggungan.setText(modelSurvey.getJmltanggungan());
        editTextTotalPenghasilan.setText(modelSurvey.getTotalincome());
        editTextLamaKerja.setText(modelSurvey.getLamakerja());
        editTextPosisi.setText(modelSurvey.getPosition());
        editTextNamaAtasanLangsung.setText(modelSurvey.getAtasan());
        editTextNoTeleponAtasanLangsung.setText(modelSurvey.getAtasanphone());

        editTextBiayaRutinTiapBulan.setText(modelSurvey.getBiayarutin());
        editTextAngsuranTagianLain.setText(modelSurvey.getTagihanlain());
        textViewIdMotorImgUrl.setText(modelSurvey.getMotorcycleimgurl());
        textViewIdMobilImgUrl.setText(modelSurvey.getCarimgurl());

        edit_text_company_order_hid.setText(modelSurvey.getOrderid());

        editTextJumlahMotor.setText(modelSurvey.getNumofmotorcycle());

        if (editTextJumlahMobil.getText().toString().equals("0")) {
            buttonIdMobil.setEnabled(false);
            spinnerStatusKepemilikanMobil.setSelection(ddlSurvey.getStatusKepemilikanMobil().indexOf("TIDAK PUNYA"));
            spinnerStatusKepemilikanMobil.setEnabled(false);
            spinnerStatusKepemilikanMobil.setClickable(false);
        } else {
            buttonIdMobil.setEnabled(true);
            spinnerStatusKepemilikanMobil.setEnabled(true);
            spinnerStatusKepemilikanMobil.setClickable(true);
        }

        if (editTextJumlahMotor.getText().toString().equals("0")) {
            buttonIdMotor.setEnabled(false);
            spinnerStatusKepemilikanMotor.setSelection(ddlSurvey.getStatusKepemilikanMotor().indexOf("TIDAK PUNYA"));
            spinnerStatusKepemilikanMotor.setEnabled(false);
            spinnerStatusKepemilikanMotor.setClickable(false);
        } else {
            buttonIdMotor.setEnabled(true);
            spinnerStatusKepemilikanMotor.setEnabled(true);
            spinnerStatusKepemilikanMotor.setClickable(true);
        }
    }

    private void setFieldLabel() {
        labelPersonalId = view.findViewById(R.id.label_personal_id_survey);
        labelTanggalTerima = view.findViewById(R.id.label_tanggal_terima_order_survey);
        labelTanggalSurvey = view.findViewById(R.id.label_tanggal_survey);
        labelAlamatSurvey = view.findViewById(R.id.label_alamat_survey);
        labelLokasiSurvey = view.findViewById(R.id.label_lokasi_survey);
        labelSourceIncoming = view.findViewById(R.id.label_source_incoming_survey);
        labelNamaSourceIncoming = view.findViewById(R.id.label_nama_source_incoming_survey);
        labelNamaPemberiInformasi = view.findViewById(R.id.label_nama_pemberi_informasi_survey);
        labelNamaSourceStatus = view.findViewById(R.id.label_nama_source_status_survey);
        labelNamaSourceStatusKet = view.findViewById(R.id.label_nama_source_status_ket_survey);
        labelPengajuanCalonDebitur = view.findViewById(R.id.label_pengajuan_calon_debitur_survey);
        labelTipeRumah = view.findViewById(R.id.label_tipe_rumah_survey);
        labelLokasiRumah = view.findViewById(R.id.label_lokasi_rumah_survey);
        labelHargaSewaRumah = view.findViewById(R.id.label_harga_sewa_rumah_survey);
        labelJenisBangunan = view.findViewById(R.id.label_jenis_bangunan_survey);
        labelKondisiBangunanRumah = view.findViewById(R.id.label_kondisi_bangunan_rumah_survey);
        labelKetersediaanGarasi = view.findViewById(R.id.label_ketersediaan_garasi_survey);
        labelLuasBangunan = view.findViewById(R.id.label_luas_bangunan_survey);
        labelLuasTanah = view.findViewById(R.id.label_luas_tanah_survey);
        labelLamaTinggal = view.findViewById(R.id.label_lama_tinggal_survey);
        labelNamaRekListrik = view.findViewById(R.id.label_nama_rek_listrik_survey);
        labelNamaRekListrikStatus = view.findViewById(R.id.label_nama_rek_listrik_status_survey);
        labelAnggotaOrmas = view.findViewById(R.id.label_anggota_ormas_survey);
        labelJumlahMobil = view.findViewById(R.id.label_jumlah_mobil_survey);
        labelStatusKepemilikanMobil = view.findViewById(R.id.label_status_kepemilikan_mobil_survey);
        labelStatusKepemilikanMobilImage = view.findViewById(R.id.label_status_kepemilikan_mobil_image_survey);
        labelJumlahMotor = view.findViewById(R.id.label_jumlah_motor_survey);
        labelStatusKepemilikanMotor = view.findViewById(R.id.label_status_kepemilikan_motor_survey);
        labelStatusKepemilikanMotorImage = view.findViewById(R.id.label_status_kepemilikan_motor_image_survey);
        labelKarakterKonsumen = view.findViewById(R.id.label_karakter_konsumen_survey);
        labelJumlahTanggungan = view.findViewById(R.id.label_jumlah_tanggungan_survey);
        labelTotalPenghasilan = view.findViewById(R.id.label_total_penghasilan_survey);
        labelLamaKerja = view.findViewById(R.id.label_lama_kerja_survey);
        labelPosisi = view.findViewById(R.id.label_posisi_survey);
        labelNamaAtasanLangsung = view.findViewById(R.id.label_nama_atasan_langsung_survey);
        labelNoTeleponAtasanLangsung = view.findViewById(R.id.label_no_telepon_atasan_langsung_survey);
        labelBiayaRutinBulanan = view.findViewById(R.id.label_biaya_rutin_bulanan_survey);
        labelAngsuranTagihanLain = view.findViewById(R.id.label_angsuran_tagihan_lain_survey);
    }

    public void setFormForPersonal() {
        labelPersonalId.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelTanggalTerima.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelTanggalSurvey.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelAlamatSurvey.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLokasiSurvey.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelSourceIncoming.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaSourceIncoming.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaPemberiInformasi.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaSourceStatus.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaSourceStatusKet.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelPengajuanCalonDebitur.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelTipeRumah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLokasiRumah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelHargaSewaRumah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelJenisBangunan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKondisiBangunanRumah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKetersediaanGarasi.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLuasBangunan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLuasTanah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLamaTinggal.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaRekListrik.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaRekListrikStatus.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelAnggotaOrmas.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelJumlahMobil.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelStatusKepemilikanMobil.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelStatusKepemilikanMobilImage.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelJumlahMotor.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelStatusKepemilikanMotor.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelStatusKepemilikanMotorImage.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKarakterKonsumen.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelJumlahTanggungan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelTotalPenghasilan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLamaKerja.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelPosisi.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaAtasanLangsung.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNoTeleponAtasanLangsung.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelBiayaRutinBulanan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelAngsuranTagihanLain.setTextColor(context.getResources().getColor(R.color.color_personal));
    }

    public void setFormForCompany() {
        labelPersonalId.setTextColor(context.getResources().getColor(R.color.color_company));
        labelTanggalTerima.setTextColor(context.getResources().getColor(R.color.color_company));
        labelTanggalSurvey.setTextColor(context.getResources().getColor(R.color.color_company));
        labelAlamatSurvey.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLokasiSurvey.setTextColor(context.getResources().getColor(R.color.color_company));
        labelSourceIncoming.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaSourceIncoming.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaPemberiInformasi.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaSourceStatus.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaSourceStatusKet.setTextColor(context.getResources().getColor(R.color.color_company));
        labelPengajuanCalonDebitur.setTextColor(context.getResources().getColor(R.color.color_company));
        labelTipeRumah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLokasiRumah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelHargaSewaRumah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelJenisBangunan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKondisiBangunanRumah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKetersediaanGarasi.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLuasBangunan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLuasTanah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLamaTinggal.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaRekListrik.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaRekListrikStatus.setTextColor(context.getResources().getColor(R.color.color_company));
        labelAnggotaOrmas.setTextColor(context.getResources().getColor(R.color.color_company));
        labelJumlahMobil.setTextColor(context.getResources().getColor(R.color.color_company));
        labelStatusKepemilikanMobil.setTextColor(context.getResources().getColor(R.color.color_company));
        labelStatusKepemilikanMobilImage.setTextColor(context.getResources().getColor(R.color.color_company));
        labelJumlahMotor.setTextColor(context.getResources().getColor(R.color.color_company));
        labelStatusKepemilikanMotor.setTextColor(context.getResources().getColor(R.color.color_company));
        labelStatusKepemilikanMotorImage.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKarakterKonsumen.setTextColor(context.getResources().getColor(R.color.color_company));
        labelJumlahTanggungan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelTotalPenghasilan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLamaKerja.setTextColor(context.getResources().getColor(R.color.color_company));
        labelPosisi.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaAtasanLangsung.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNoTeleponAtasanLangsung.setTextColor(context.getResources().getColor(R.color.color_company));
        labelBiayaRutinBulanan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelAngsuranTagihanLain.setTextColor(context.getResources().getColor(R.color.color_company));
    }

    private ArrayAdapter<String> setAdapterItemList(List list) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl, list);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;
    }

    public ModelSurvey getComplete() {
        ModelSurvey modelSurvey = getForm();
        modelSurvey.setComplete(true);

        if (modelSurvey.getDtmorder().length() == 0) {
            editTextTanggalTerima.setError("Order Date is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getDtmorder().trim().length() == 0) {
            editTextTanggalTerima.setError("rder Date not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getSurveydate().length() == 0) {
            editTextTanggalSurvey.setError("Survey Date is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getSurveydate().trim().length() == 0) {
            editTextTanggalSurvey.setError("Survey Date not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getSurveyaddr().length() == 0) {
            editTextAlamatSurvey.setError("Survey Address is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getSurveyaddr().trim().length() == 0) {
            editTextAlamatSurvey.setError("Survey Address not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getSrcincomingname().length() == 0) {
            editTextNamaSourceIncoming.setError("Survey Incoming Name is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getSrcincomingname().trim().length() == 0) {
            editTextNamaSourceIncoming.setError("Survey Incoming Name not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getInforsrcname().length() == 0) {
            editTextNamaPemberiInformasi.setError("Survey Incoming Source Name is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getInforsrcname().trim().length() == 0) {
            editTextNamaPemberiInformasi.setError("Survey Incoming Source Name not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getSrcnamestat().length() == 0) {
            editTextNamaSourceStatusSurvey.setError("Survey Source Status is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getSrcnamestat().trim().length() == 0) {
            editTextNamaSourceStatusSurvey.setError("Survey Source Status not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

//        if (modelSurvey.getSrcstatket().length() == 0) {
//            editTextNamaSourceStatusKet.setError("Survey Source Status Ket is required");
//            modelSurvey.setComplete(false);
//        } else if (modelSurvey.getSrcstatket().trim().length() == 0) {
//            editTextNamaSourceStatusKet.setError("Survey Source Status Ket not allowed whitespaces only");
//            modelSurvey.setComplete(false);
//        }

        if (modelSurvey.getBldarea().length() == 0) {
            editTextLuasBangunan.setError("Luas Bangunan is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getBldarea().trim().length() == 0) {
            editTextLuasBangunan.setError("Luas Bangunan not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getLuasbld().length() == 0) {
            editTextLuasTanah.setError("Luas Tanah is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getLuasbld().trim().length() == 0) {
            editTextLuasTanah.setError("Luas Tanah not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getStaylength().length() == 0) {
            editTextLamaTinggal.setError("Lama Tinggal is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getStaylength().trim().length() == 0) {
            editTextLamaTinggal.setError("Lama Tinggal not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getElctrybillname().length() == 0) {
            editTextNamaRekListrik.setError("Electrycity Bill Name is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getElctrybillname().trim().length() == 0) {
            editTextNamaRekListrik.setError("Electrycity Bill Name not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getNumofcar().length() == 0) {
            editTextJumlahMobil.setError("Num of Car is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getNumofcar().trim().length() == 0) {
            editTextJumlahMobil.setError("Num of Car not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (!modelSurvey.getNumofcar().equals("0")) {
            if (modelSurvey.getCarimg().length() == 0) {
                textViewIdMobilImg.setError("CAR Image Image is Required");
                modelSurvey.setComplete(false);
            } else if (modelSurvey.getCarimg().equals("No selected file")) {
                textViewIdMobilImg.setError("CAR Image is Required");
                modelSurvey.setComplete(false);
            } else {
                textViewIdMobilImg.setError(null);
            }
        }else{
            textViewIdMobilImg.setError(null);
        }


        if (modelSurvey.getNumofmotorcycle().length() == 0) {
            editTextJumlahMotor.setError("Num of Car is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getNumofmotorcycle().trim().length() == 0) {
            editTextJumlahMotor.setError("Num of Car not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }


        if (!modelSurvey.getNumofmotorcycle().equals("0")) {
            if (modelSurvey.getMotorcycleimg().length() == 0) {
                textViewIdMotorImg.setError("Motorcycle Image Image is Required");
                modelSurvey.setComplete(false);
            } else if (modelSurvey.getMotorcycleimg().equals("No selected file")) {
                textViewIdMotorImg.setError("Motorcycle Image is Required");
                modelSurvey.setComplete(false);
            } else {
                textViewIdMotorImg.setError(null);
            }
        }else{
            textViewIdMotorImg.setError(null);
        }

        if (modelSurvey.getJmltanggungan().length() == 0) {
            editTextJumlahTanggungan.setError("Jumlah Tanggungan is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getJmltanggungan().trim().length() == 0) {
            editTextJumlahTanggungan.setError("Jumlah Tanggungan not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getTotalincome().length() == 0) {
            editTextTotalPenghasilan.setError("Total Income is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getTotalincome().trim().length() == 0) {
            editTextTotalPenghasilan.setError("Total Income not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getLamakerja().length() == 0) {
            editTextLamaKerja.setError("Lama Bekerja is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getLamakerja().trim().length() == 0) {
            editTextLamaKerja.setError("Lama Bekerja not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getPosition().length() == 0) {
            editTextPosisi.setError("Job Position is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getPosition().trim().length() == 0) {
            editTextPosisi.setError("Job Position not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        if(modelSurvey.getAtasanphone().length() > 0 && modelSurvey.getAtasanphone().length() < 7){
            editTextNoTeleponAtasanLangsung.setError("Minimum 7 digits");
            modelSurvey.setComplete(false);
        }

        if (modelSurvey.getBiayarutin().length() == 0) {
            editTextBiayaRutinTiapBulan.setError("Biaya Bulanan is required");
            modelSurvey.setComplete(false);
        } else if (modelSurvey.getBiayarutin().trim().length() == 0) {
            editTextBiayaRutinTiapBulan.setError("Biaya Bulanan not allowed whitespaces only");
            modelSurvey.setComplete(false);
        }

        return modelSurvey;
    }
}
