package com.example.a0426611017.carsurvey.Object.Survey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Currency;
import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLSurvey;
import com.example.a0426611017.carsurvey.Model.Survey.ModelSurvey;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by c201017001 on 12/02/2018.
 */

public class ObjectSurveyDetail {

    private View view;
    private ViewGroup container;
    private LayoutInflater inflater;
    private Context context;
    private ModelDDLSurvey ddlSurvey;

    private TextView labelPersonalId, labelTanggalTerima, labelTanggalSurvey, labelAlamatSurvey, labelLokasiSurvey,
            labelSourceIncoming, labelNamaSourceIncoming, labelNamaPemberiInformasi, labelNamaSourceStatus,
            labelNamaSourceStatusKet, labelPengajuanCalonDebitur, labelTipeRumah, labelLokasiRumah,
            labelHargaSewaRumah, labelJenisBangunan, labelKondisiBangunanRumah, labelKetersediaanGarasi,
            labelLuasBangunan, labelLuasTanah, labelLamaTinggal, labelNamaRekListrik, labelNamaRekListrikStatus,
            labelAnggotaOrmas, labelJumlahMobil, labelStatusKepemilikanMobil, labelStatusKepemilikanMobilImage,
            labelJumlahMotor, labelStatusKepemilikanMotor, labelStatusKepemilikanMotorImage, labelKarakterKonsumen,
            labelJumlahTanggungan, labelTotalPenghasilan, labelLamaKerja, labelPosisi, labelNamaAtasanLangsung,
            labelNoTeleponAtasanLangsung, labelBiayaRutinBulanan, labelAngsuranTagihanLain;

    private TextView valueNamaSourceStatus, valueTanggalTerima, valueTanggalSurvey, valueAlamatSurvey,
            valueNamaSourceIncoming, valueNamaPemberiInformasi, valueNamaSourceStatusKet,
            valueHargaSewaRumah, valueLuasBangunan, valueLuasTanah, valueLamaTinggal, valueNamaRekListrik,
            valueJumlahMobil, valueJumlahMotor, valueJumlahTanggungan,
            valueTotalPenghasilan, valueLamaKerja, valuePosisi, valueNamaAtasanLangsung,
            valueNoTeleponAtasanLangsung, valueBiayaRutinTiapBulan, valueAngsuranTagianLain,
            valueLokasiSurvey, valueSourceIncoming, valuePengajuanCalonDebitur, valueTipeRumah,
            valueLokasiRumah, valueJenisBangunan, valueKondisiBangunan, valueKetersediaanGarasi, valueRekListrikStatus,
            valueAnggotaOrmas, valueStatusKepemilikanMobil, valueStatusKepemilikanMotor, valueKarakterKonsumen;

    private TextView fileNameImageKepemilikanMobil, fileNameImageKepemilikanMotor;

    public ObjectSurveyDetail(ViewGroup container, LayoutInflater inflater) {
        this.container = container;
        this.inflater = inflater;
        this.context = container.getContext();
        this.view = this.inflater.inflate(R.layout.fragment_survey_detail, container, false);
        setField();
        setFieldLabel();
    }

    public View getView() {
        return view;
    }

    public void setForm(ModelSurvey modelSurvey){
        String dateFormat = "";
        if(modelSurvey.getDtmorder() != null){
            if(!modelSurvey.getDtmorder().equals("")){
                dateFormat = modelSurvey.getDtmorder().substring(6,8)
                        .concat(" ")
                        .concat(FormatDate.PRINT_MONTH[Integer.parseInt(modelSurvey.getDtmorder().substring(4,6))])
                        .concat(" ")
                        .concat(modelSurvey.getDtmorder().substring(0,4));
            }
        }
        valueTanggalTerima.setText(dateFormat);
        dateFormat = "";
        if(modelSurvey.getSurveydate() != null){
            if(!modelSurvey.getSurveydate().equals("")){
                dateFormat = modelSurvey.getSurveydate().substring(6,8)
                        .concat(" ")
                        .concat(FormatDate.PRINT_MONTH[Integer.parseInt(modelSurvey.getSurveydate().substring(4,6))])
                        .concat(" ")
                        .concat(modelSurvey.getSurveydate().substring(0,4));
            }
        }
        valueTanggalSurvey.setText(dateFormat);
        valueAlamatSurvey.setText(modelSurvey.getSurveyaddr());
        valueNamaSourceIncoming.setText(modelSurvey.getSrcincomingname());
        valueNamaPemberiInformasi.setText(modelSurvey.getInforsrcname());
        valueNamaSourceStatusKet.setText(modelSurvey.getSrcstatket());
        valueNamaSourceStatus.setText(modelSurvey.getSrcnamestat());
        valueHargaSewaRumah.setText(modelSurvey.getRentprice());
            if(!valueHargaSewaRumah.getText().toString().equals("")){
                String formatted = Currency.set(valueHargaSewaRumah);
                valueHargaSewaRumah.setText(formatted.replaceAll("[$]",""));
            }
        valueLuasBangunan.setText(modelSurvey.getBldarea());
        valueLuasTanah.setText(modelSurvey.getLuasbld());
        valueLamaTinggal.setText(modelSurvey.getStaylength());
        valueNamaRekListrik.setText(modelSurvey.getElctrybillname());
        valueJumlahMobil.setText(modelSurvey.getNumofcar());
        valueJumlahMotor.setText(modelSurvey.getNumofmotorcycle());
        valueJumlahTanggungan.setText(modelSurvey.getJmltanggungan());
        valueTotalPenghasilan.setText(modelSurvey.getTotalincome());

            if(!valueTotalPenghasilan.getText().toString().equals("")){
                String formatted = Currency.set(valueTotalPenghasilan);
                valueTotalPenghasilan.setText(formatted.replaceAll("[$]",""));
            }

        valueLamaKerja.setText(modelSurvey.getLamakerja());
        valuePosisi.setText(modelSurvey.getPosition());
        valueNamaAtasanLangsung.setText(modelSurvey.getAtasan());
        valueNoTeleponAtasanLangsung.setText(modelSurvey.getAtasanphone());
        valueBiayaRutinTiapBulan.setText(modelSurvey.getBiayarutin());
            if(!valueBiayaRutinTiapBulan.getText().toString().equals("")){
                String formatted = Currency.set(valueBiayaRutinTiapBulan);
                valueBiayaRutinTiapBulan.setText(formatted.replaceAll("[$]",""));
            }

        valueAngsuranTagianLain.setText(modelSurvey.getTagihanlain());
            if(!valueAngsuranTagianLain.getText().toString().equals("")){
                String formatted = Currency.set(valueAngsuranTagianLain);
                valueAngsuranTagianLain.setText(formatted.replaceAll("[$]",""));
            }


        valueLokasiSurvey.setText(modelSurvey.getSurveylocation());
        valueSourceIncoming.setText(modelSurvey.getSrcincoming());
        valuePengajuanCalonDebitur.setText(modelSurvey.getClndebitur());
        valueTipeRumah.setText(modelSurvey.getHometype());
        valueLokasiRumah.setText(modelSurvey.getHomeloc());
        valueJenisBangunan.setText(modelSurvey.getBldtype());
        valueKondisiBangunan.setText(modelSurvey.getBldcond());
        valueKetersediaanGarasi.setText(modelSurvey.getGaragestat());
        valueRekListrikStatus.setText(modelSurvey.getElctrybillstat());
        valueAnggotaOrmas.setText(modelSurvey.getIsormas());
        valueStatusKepemilikanMobil.setText(modelSurvey.getCarstat());
        valueStatusKepemilikanMotor.setText(modelSurvey.getMotorcyclestat());
        valueKarakterKonsumen.setText(modelSurvey.getCharactercust());

        String fileNameKepemilikanMobil = modelSurvey.getCarimgurl();
        String fileNameKepemilikanMotor = modelSurvey.getMotorcycleimgurl();
        if(fileNameKepemilikanMobil != null && !fileNameKepemilikanMobil.equals("")){
            fileNameKepemilikanMobil = fileNameKepemilikanMobil.split("/")[6];
        }
        if(fileNameKepemilikanMotor != null && !fileNameKepemilikanMotor.equals("")){
            fileNameKepemilikanMotor = fileNameKepemilikanMotor.split("/")[6];
        }
        fileNameImageKepemilikanMobil.setText(fileNameKepemilikanMobil);
        fileNameImageKepemilikanMotor.setText(fileNameKepemilikanMotor);
    }

    private void setField(){
        valueTanggalTerima = view.findViewById(R.id.value_tanggal_terima_order_survey);
        valueTanggalSurvey = view.findViewById(R.id.value_tanggal_survey);
        valueAlamatSurvey = view.findViewById(R.id.value_alamat_survey);
        valueNamaSourceIncoming = view.findViewById(R.id.value_nama_source_incoming_survey);
        valueNamaPemberiInformasi = view.findViewById(R.id.value_nama_pemberi_informasi_survey);
        valueNamaSourceStatusKet = view.findViewById(R.id.value_nama_source_status_ket_survey);
        valueNamaSourceStatus = view.findViewById(R.id.value_nama_source_status_survey);
        valueHargaSewaRumah = view.findViewById(R.id.value_harga_sewa_rumah_survey);
        valueLuasBangunan = view.findViewById(R.id.value_luas_bangunan_survey);
        valueLuasTanah = view.findViewById(R.id.value_luas_tanah_survey);
        valueLamaTinggal = view.findViewById(R.id.value_lama_tinggal_survey);
        valueNamaRekListrik = view.findViewById(R.id.value_nama_rek_listrik_survey);
        valueJumlahMobil = view.findViewById(R.id.value_jumlah_mobil_survey);
        valueJumlahMotor = view.findViewById(R.id.value_jumlah_motor_survey);
        valueJumlahTanggungan = view.findViewById(R.id.value_jumlah_tanggungan_survey);
        valueTotalPenghasilan = view.findViewById(R.id.value_total_penghasilan_survey);
        valueLamaKerja = view.findViewById(R.id.value_lama_kerja_survey);
        valuePosisi = view.findViewById(R.id.value_posisi_survey);
        valueNamaAtasanLangsung = view.findViewById(R.id.value_nama_atasan_langsung_survey);
        valueNoTeleponAtasanLangsung = view.findViewById(R.id.value_no_telepon_atasan_langsung_survey);
        valueBiayaRutinTiapBulan = view.findViewById(R.id.value_biaya_rutin_bulanan_survey);

        valueAngsuranTagianLain = view.findViewById(R.id.value_angsuran_tagihan_lain_survey);
        valueLokasiSurvey = view.findViewById(R.id.value_lokasi_survey);
        valueSourceIncoming = view.findViewById(R.id.value_source_incoming_survey);
        valuePengajuanCalonDebitur = view.findViewById(R.id.value_pengajuan_calon_debitur_survey);
        valueTipeRumah = view.findViewById(R.id.value_tipe_rumah_survey);
        valueLokasiRumah = view.findViewById(R.id.value_lokasi_rumah_survey);
        valueJenisBangunan = view.findViewById(R.id.value_jenis_bangunan_survey);
        valueKondisiBangunan = view.findViewById(R.id.value_kondisi_bangunan_rumah_survey);
        valueKetersediaanGarasi = view.findViewById(R.id.value_ketersediaan_garasi_survey);
        valueRekListrikStatus = view.findViewById(R.id.value_nama_rek_listrik_status_survey);
        valueAnggotaOrmas = view.findViewById(R.id.value_anggota_ormas_survey);
        valueStatusKepemilikanMobil = view.findViewById(R.id.value_status_kepemilikan_mobil_survey);
        valueStatusKepemilikanMotor = view.findViewById(R.id.value_status_kepemilikan_motor_survey);
        valueKarakterKonsumen = view.findViewById(R.id.value_karakter_konsumen_survey);

        fileNameImageKepemilikanMobil = view.findViewById(R.id.value_status_kepemilikan_mobil_image_survey);
        fileNameImageKepemilikanMotor = view.findViewById(R.id.value_status_kepemilikan_motor_image_survey);
    }

    private void setFieldLabel(){
        labelTanggalTerima = view.findViewById(R.id.label_tanggal_terima_order_survey);
        labelTanggalSurvey = view.findViewById(R.id.label_tanggal_survey);
        labelAlamatSurvey = view.findViewById(R.id.label_alamat_survey);
        labelLokasiSurvey = view.findViewById(R.id.label_lokasi_survey);
        labelSourceIncoming = view.findViewById(R.id.label_source_incoming_survey);
        labelNamaSourceIncoming = view.findViewById(R.id.label_nama_source_incoming_survey);
        labelNamaPemberiInformasi = view.findViewById(R.id.label_nama_pemberi_informasi_survey);
        labelNamaSourceStatus = view.findViewById(R.id.label_nama_source_status_survey);
        labelNamaSourceStatusKet = view.findViewById(R.id.label_nama_source_status_ket_survey);
        labelPengajuanCalonDebitur = view.findViewById(R.id.label_pengajuan_calon_debitur_survey);
        labelTipeRumah = view.findViewById(R.id.label_tipe_rumah_survey);
        labelLokasiRumah = view.findViewById(R.id.label_lokasi_rumah_survey);
        labelHargaSewaRumah = view.findViewById(R.id.label_harga_sewa_rumah_survey);
        labelJenisBangunan = view.findViewById(R.id.label_jenis_bangunan_survey);
        labelKondisiBangunanRumah = view.findViewById(R.id.label_kondisi_bangunan_rumah_survey);
        labelKetersediaanGarasi = view.findViewById(R.id.label_ketersediaan_garasi_survey);
        labelLuasBangunan = view.findViewById(R.id.label_luas_bangunan_survey);
        labelLuasTanah = view.findViewById(R.id.label_luas_tanah_survey);
        labelLamaTinggal = view.findViewById(R.id.label_lama_tinggal_survey);
        labelNamaRekListrik = view.findViewById(R.id.label_nama_rek_listrik_survey);
        labelNamaRekListrikStatus = view.findViewById(R.id.label_nama_rek_listrik_status_survey);
        labelAnggotaOrmas = view.findViewById(R.id.label_anggota_ormas_survey);
        labelJumlahMobil = view.findViewById(R.id.label_jumlah_mobil_survey);
        labelStatusKepemilikanMobil = view.findViewById(R.id.label_status_kepemilikan_mobil_survey);
        labelStatusKepemilikanMobilImage = view.findViewById(R.id.label_status_kepemilikan_mobil_image_survey);
        labelJumlahMotor = view.findViewById(R.id.label_jumlah_motor_survey);
        labelStatusKepemilikanMotor = view.findViewById(R.id.label_status_kepemilikan_motor_survey);
        labelStatusKepemilikanMotorImage = view.findViewById(R.id.label_status_kepemilikan_motor_image_survey);
        labelKarakterKonsumen = view.findViewById(R.id.label_karakter_konsumen_survey);
        labelJumlahTanggungan = view.findViewById(R.id.label_jumlah_tanggungan_survey);
        labelTotalPenghasilan = view.findViewById(R.id.label_total_penghasilan_survey);
        labelLamaKerja = view.findViewById(R.id.label_lama_kerja_survey);
        labelPosisi = view.findViewById(R.id.label_posisi_survey);
        labelNamaAtasanLangsung = view.findViewById(R.id.label_nama_atasan_langsung_survey);
        labelNoTeleponAtasanLangsung = view.findViewById(R.id.label_no_telepon_atasan_langsung_survey);
        labelBiayaRutinBulanan = view.findViewById(R.id.label_biaya_rutin_bulanan_survey);
        labelAngsuranTagihanLain = view.findViewById(R.id.label_angsuran_tagihan_lain_survey);
    }

    public void setFormForPersonal(){
        labelTanggalTerima.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelTanggalSurvey.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelAlamatSurvey.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLokasiSurvey.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelSourceIncoming.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaSourceIncoming.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaPemberiInformasi.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaSourceStatus.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaSourceStatusKet.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelPengajuanCalonDebitur.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelTipeRumah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLokasiRumah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelHargaSewaRumah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelJenisBangunan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKondisiBangunanRumah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKetersediaanGarasi.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLuasBangunan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLuasTanah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLamaTinggal.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaRekListrik.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaRekListrikStatus.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelAnggotaOrmas.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelJumlahMobil.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelStatusKepemilikanMobil.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelStatusKepemilikanMobilImage.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelJumlahMotor.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelStatusKepemilikanMotor.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelStatusKepemilikanMotorImage.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKarakterKonsumen.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelJumlahTanggungan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelTotalPenghasilan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLamaKerja.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelPosisi.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaAtasanLangsung.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNoTeleponAtasanLangsung.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelBiayaRutinBulanan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelAngsuranTagihanLain.setTextColor(context.getResources().getColor(R.color.color_personal));
    }

    public void setFormForCompany(){
        labelTanggalTerima.setTextColor(context.getResources().getColor(R.color.color_company));
        labelTanggalSurvey.setTextColor(context.getResources().getColor(R.color.color_company));
        labelAlamatSurvey.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLokasiSurvey.setTextColor(context.getResources().getColor(R.color.color_company));
        labelSourceIncoming.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaSourceIncoming.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaPemberiInformasi.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaSourceStatus.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaSourceStatusKet.setTextColor(context.getResources().getColor(R.color.color_company));
        labelPengajuanCalonDebitur.setTextColor(context.getResources().getColor(R.color.color_company));
        labelTipeRumah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLokasiRumah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelHargaSewaRumah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelJenisBangunan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKondisiBangunanRumah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKetersediaanGarasi.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLuasBangunan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLuasTanah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLamaTinggal.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaRekListrik.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaRekListrikStatus.setTextColor(context.getResources().getColor(R.color.color_company));
        labelAnggotaOrmas.setTextColor(context.getResources().getColor(R.color.color_company));
        labelJumlahMobil.setTextColor(context.getResources().getColor(R.color.color_company));
        labelStatusKepemilikanMobil.setTextColor(context.getResources().getColor(R.color.color_company));
        labelStatusKepemilikanMobilImage.setTextColor(context.getResources().getColor(R.color.color_company));
        labelJumlahMotor.setTextColor(context.getResources().getColor(R.color.color_company));
        labelStatusKepemilikanMotor.setTextColor(context.getResources().getColor(R.color.color_company));
        labelStatusKepemilikanMotorImage.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKarakterKonsumen.setTextColor(context.getResources().getColor(R.color.color_company));
        labelJumlahTanggungan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelTotalPenghasilan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLamaKerja.setTextColor(context.getResources().getColor(R.color.color_company));
        labelPosisi.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaAtasanLangsung.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNoTeleponAtasanLangsung.setTextColor(context.getResources().getColor(R.color.color_company));
        labelBiayaRutinBulanan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelAngsuranTagihanLain.setTextColor(context.getResources().getColor(R.color.color_company));
    }

}
