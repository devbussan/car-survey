package com.example.a0426611017.carsurvey.Model.Company.Mini;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0414831216 on 2/23/2018.
 */

public class ModelRequestCompanySendHq {
    @SerializedName("CMOLOCATION")
    @Expose
     String cmpLocation;

    @SerializedName("SURVEYSTAT")
    @Expose
    String surVeystat;

    @SerializedName("ISREADCMO")
    @Expose
    String isReadcmo;

    @SerializedName("ISREADADMIN")
    @Expose
    String isReadAdmin;

    @SerializedName("HQCOMMENT")
    @Expose
    String hqComment;

    @SerializedName("HQUSER")
    @Expose
    String hqUser;

    @SerializedName("HQRSPDTM")
    @Expose
    String hqrPsdm;

    @SerializedName("SVYPLANDTM")
    @Expose
    String svyPlandtm;

    @SerializedName("SVYACTUALDTM")
    @Expose
    String svyActualdtm;

    @SerializedName("FINSUBMINTDTM")
    @Expose
    String finSubmintdmtm;


    @SerializedName("STATUS")
    @Expose
    String staTus;

    @SerializedName("USRUPD")
    @Expose
    String usrPd;


    public String getCmpLocation() {
        return cmpLocation;
    }

    public void setCmpLocation(String cmpLocation) {
        this.cmpLocation = cmpLocation;
    }

    public String getSurVeystat() {
        return surVeystat;
    }

    public void setSurVeystat(String surVeystat) {
        this.surVeystat = surVeystat;
    }

    public String getIsReadcmo() {
        return isReadcmo;
    }

    public void setIsReadcmo(String isReadcmo) {
        this.isReadcmo = isReadcmo;
    }

    public String getIsReadAdmin() {
        return isReadAdmin;
    }

    public void setIsReadAdmin(String isReadAdmin) {
        this.isReadAdmin = isReadAdmin;
    }

    public String getHqComment() {
        return hqComment;
    }

    public void setHqComment(String hqComment) {
        this.hqComment = hqComment;
    }

    public String getHqUser() {
        return hqUser;
    }

    public void setHqUser(String hqUser) {
        this.hqUser = hqUser;
    }

    public String getHqrPsdm() {
        return hqrPsdm;
    }

    public void setHqrPsdm(String hqrPsdm) {
        this.hqrPsdm = hqrPsdm;
    }

    public String getSvyPlandtm() {
        return svyPlandtm;
    }

    public void setSvyPlandtm(String svyPlandtm) {
        this.svyPlandtm = svyPlandtm;
    }

    public String getSvyActualdtm() {
        return svyActualdtm;
    }

    public void setSvyActualdtm(String svyActualdtm) {
        this.svyActualdtm = svyActualdtm;
    }

    public String getFinSubmintdmtm() {
        return finSubmintdmtm;
    }

    public void setFinSubmintdmtm(String finSubmintdmtm) {
        this.finSubmintdmtm = finSubmintdmtm;
    }

    public String getStaTus() {
        return staTus;
    }

    public void setStaTus(String staTus) {
        this.staTus = staTus;
    }

    public String getUsrPd() {
        return usrPd;
    }

    public void setUsrPd(String usrPd) {
        this.usrPd = usrPd;
    }
}
