package com.example.a0426611017.carsurvey;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Company.ActivityCompanyTab;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.Personal.ActivityPersonalTab;

/**
 * Created by c201017001 on 24/01/2018.
 */

public class MainActivityDrawer extends AppCompatActivity {

    public static DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private SearchView searchView;
    private MenuItem menu_item;

    private Fragment fragment;
    private ActionBarDrawerToggle mDrawerToggle;

    SharedPreferences sharedpreferencesTab;
    SharedPreferences sharedpreferencesMenuCategory;
    private int tabCurrent;
    private int menu;
    private int tabChosen;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_drawer);
        sharedpreferencesMenuCategory = getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, Context.MODE_PRIVATE);
//        searchView = new SearchView(this);
        mToolbar = findViewById(R.id.action_bar);
        final Window window = getWindow();

        ImageView imageViewLogoLogin = findViewById(R.id.logo_baf_drawer);

        int mustChange = getSharedPreferences(Constans.KeySharedPreference.DATE, MODE_PRIVATE).getInt(Constans.KEY_COMPARE_DATE, -1);
        boolean isTimetoChange = getSharedPreferences(Constans.KeySharedPreference.DATE, MODE_PRIVATE).getBoolean(Constans.KEY_AFTER_DATE, false);

        String nameCmo = getSharedPreferences(Constans.KeySharedPreference.ADMIN_NAME, MODE_PRIVATE).getString(Constans.KEY_ADMIN_NAME, "anonymous");
        String nikCmo = getSharedPreferences(Constans.KeySharedPreference.ADMIN_NIK, MODE_PRIVATE).getString(Constans.KEY_ADMIN_NIK, "00000000");

        if(mustChange == 1 || isTimetoChange){
            imageViewLogoLogin.setImageDrawable(getDrawable(R.drawable.logobafbaru));
        }

        menu = getIntent().getExtras().getInt(Constans.KEY_MENU, 0);
        SharedPreferences.Editor sharedCategory = sharedpreferencesMenuCategory.edit();
        sharedCategory.putInt(Constans.KEY_MENU_CATEGORY,menu);
        sharedCategory.apply();
        Log.d("menu category", "ini menu "+sharedpreferencesMenuCategory.getInt(Constans.KEY_MENU_CATEGORY, 0));
        if(menu == 0){
            loadFragment(new FragmentDashboardTab());
            window.setStatusBarColor(getResources().getColor(R.color.color_grey));
            mToolbar.setBackgroundColor(getResources().getColor(R.color.color_grey));
        }else if (menu == Constans.MENU_COMPANY_ORDER_FORM || menu == Constans.MENU_COMPANY_FULL_FORM){
            window.setStatusBarColor(getResources().getColor(R.color.color_company));
            mToolbar.setBackgroundColor(getResources().getColor(R.color.color_company));
            tabChosen = getIntent().getIntExtra(Constans.KEY_CURRENT_TAB, -1);
            loadFragment(new ActivityCompanyTab().loadSearch("", tabChosen, menu));
        }else if (menu == Constans.MENU_PERSONAL_ORDER_FORM || menu == Constans.MENU_PERSONAL_FULL_FORM){
            window.setStatusBarColor(getResources().getColor(R.color.color_personal));
            mToolbar.setBackgroundColor(getResources().getColor(R.color.color_personal));
            tabChosen = getIntent().getIntExtra(Constans.KEY_CURRENT_TAB, -1);
            loadFragment(new ActivityPersonalTab().loadSearch("", tabChosen, menu));
        }

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        sharedpreferencesTab = getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, Context.MODE_PRIVATE);

        final LinearLayout layoutPersonal = findViewById(R.id.linePersonal);
        final LinearLayout layoutCompany = findViewById(R.id.lineCompany);
        final LinearLayout layoutDashboard = findViewById(R.id.lineDashboard);
        final LinearLayout layoutLogout = findViewById(R.id.lineLogout);

        TextView textViewNameCmo = findViewById(R.id.textViewName);
        textViewNameCmo.setText(nameCmo.toUpperCase());

        TextView textViewNikCmo = findViewById(R.id.textViewNik);
        textViewNikCmo.setText(nikCmo);

        mDrawerLayout= findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerOpened(final View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        layoutPersonal.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                window.setStatusBarColor(getResources().getColor(R.color.color_personal));
                mToolbar.setBackgroundColor(Color.parseColor("#3E60AA"));
                mDrawerLayout.closeDrawer(GravityCompat.START);
//                loadFragment(new ActivityPersonalTab().loadSearch("", -1, menu));
                loadFragment(new FragmentMenuCategory().newInstance(Constans.MENU_PERSONAL));

            }
        });

        layoutCompany.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                window.setStatusBarColor(getResources().getColor(R.color.color_company));
                mToolbar.setBackgroundColor(Color.parseColor("#C13A41"));
                mDrawerLayout.closeDrawer(GravityCompat.START);
//                invalidateOptionsMenu();
                menu_item.setVisible(false);
//                loadFragment(new ActivityCompanyTab().loadSearch("", -1));
                menu = 0;
                loadFragment(new FragmentMenuCategory().newInstance(Constans.MENU_COMPANY));
            }
        });

        layoutDashboard.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View v) {
                window.setStatusBarColor(getResources().getColor(R.color.color_grey));
                mToolbar.setBackgroundColor(getResources().getColor(R.color.color_grey));
                mDrawerLayout.closeDrawer(GravityCompat.START);
//                invalidateOptionsMenu();
                menu_item.setVisible(false);
//                loadFragment(new ActivityCompanyTab().loadSearch("", -1));
                menu = 0;
                loadFragment(new FragmentDashboardTab());
            }
        });


        layoutLogout.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mToolbar.setBackgroundColor(getResources().getColor(R.color.color_black));
                menu_item.setVisible(false);
                menu = 0;
                AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivityDrawer.this);
                builder1.setMessage("Are you sure want Logout ?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                SharedPreferences.Editor editor = getSharedPreferences(Constans.KeySharedPreference.IS_ACTIVE, MODE_PRIVATE).edit();
                                editor.putString(Constans.KEY_IS_ACTIVE, "0");
                                editor.apply();
                                editor.commit();
                                Intent i = new Intent(MainActivityDrawer.this, ActivityLogin.class);
                                startActivity(i);
                                finishAffinity();
                            }
                        });

                builder1.setNegativeButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        if(this.menu != 0){
        getMenuInflater().inflate(R.menu.menu_main, menu);

        menu_item = menu.findItem(R.id.action_search);

        menu_item.setVisible(false);
        if(this.menu != 0){
            menu_item.setVisible(true);
        }


        searchView = (SearchView) menu_item.getActionView();
        searchView.setFocusable(false);
        searchView.setQueryHint("Search");
        searchView.setBackgroundColor(Color.WHITE);
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {


            @Override
            public boolean onQueryTextSubmit(String s) {
                if (fragment.getClass() == ActivityCompanyTab.class){
                    tabCurrent = sharedpreferencesTab.getInt(Constans.KEY_TAB, -1);
                    loadFragment(new ActivityCompanyTab().loadSearch(s, tabCurrent, MainActivityDrawer.this.menu));
                }else if (fragment.getClass() == ActivityPersonalTab.class){
                    tabCurrent = sharedpreferencesTab.getInt(Constans.KEY_TAB, -1);
                    loadFragment(new ActivityPersonalTab().loadSearch(s, tabCurrent, MainActivityDrawer.this.menu));
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
//                if(s.equals("")){
//                    if (fragment.getClass() == new ActivityCompanyTab().getClass()){
//                        tabCurrent = sharedpreferencesTab.getInt(ActivityCompanyTab.tabKey, -1);
//                        loadFragment(new ActivityCompanyTab().loadSearch(s, tabCurrent, MainActivityDrawer.this.menu));
//                    }else if (fragment.getClass() == new ActivityPersonalTab().getClass()){
//                        tabCurrent = sharedpreferencesPersonal.getInt(ActivityPersonalTab.tabKey, -1);
//                        loadFragment(new ActivityPersonalTab().loadSearch(s, tabCurrent,MainActivityDrawer.this.menu));
//                    }
//                }
                return false;
            }
        });
//        }


        return true;
    }

    public void loadFragment(Fragment fragment) {
// create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
// create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
// replace the FrameLayout with new Fragment
        this.fragment = fragment;
        fragmentTransaction.replace(R.id.content_drawer, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onBackPressed() {
        if(this.menu == 0){
            if (doubleBackToExitPressedOnce) {
//                MainActivityDrawer.super.onBackPressed();
//                finish();
            }

            this.doubleBackToExitPressedOnce = true;
//            Toast.makeText(this, "Please click Back Button once again to exit the application ", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 3000);
        }else if(this.menu == Constans.MENU_COMPANY_FULL_FORM || this.menu == Constans.MENU_COMPANY_ORDER_FORM){
            menu = 0;
            loadFragment(new FragmentMenuCategory().newInstance(Constans.MENU_COMPANY));
        }else if (this.menu == Constans.MENU_PERSONAL_FULL_FORM || this.menu == Constans.MENU_PERSONAL_ORDER_FORM){
            menu = 0;
            loadFragment(new FragmentMenuCategory().newInstance(Constans.MENU_PERSONAL));
        }

    }
}