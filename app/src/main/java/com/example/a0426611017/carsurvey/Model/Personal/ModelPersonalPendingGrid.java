package com.example.a0426611017.carsurvey.Model.Personal;

/**
 * Created by 0426611017 on 12/14/2017.
 */

public class ModelPersonalPendingGrid {

    private String personalDatePending;
    private String personalNamePending;
    private String personalNo_idPending;
    private String personalPhone_numberPending;

    //personal date pending
    public String getPersonalDatePending() {return personalDatePending;}
    public void setPersonalDatePending (String personalDatePending) {this.personalDatePending = personalDatePending;}

    //personal name pending
    public String getPersonalNamePending() {return personalNamePending;}
    public void setPersonalNamePending (String personalNamePending) {this.personalNamePending = personalNamePending;}

    //personal No_id pending
    public String getPersonalNo_idPending() {return personalNo_idPending;}
    public void setPersonalNo_idPending (String personalNo_idPending) {this.personalNo_idPending = personalNo_idPending;}

    //personal Phone_number pending
    public String getpersonalPhone_numberPending() {return personalPhone_numberPending;}
    public void setPersonalPhone_numberPending (String personalPhone_numberPending) {this.personalPhone_numberPending = personalPhone_numberPending;}
}
