package com.example.a0426611017.carsurvey.Model.Company.Mini;

import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 09/02/2018.
 */

public class ModelMiniFormCompany {

    @SerializedName("COMPANY_ORDER_ID")
    private String companynOrderId;

    @SerializedName("LOG_DATA_ID")
    private String logDataID;

    @SerializedName("COMPANY_INFO_ORDER")
    private String companyInfoOrder;

    @SerializedName("LOG_COMPANY")
     private String logCompany;

    public String getLogCompany() {
        return logCompany;
    }

    public void setLogCompany(String logCompany) {
        this.logCompany = logCompany;
    }

    public String getCompanyInfoOrder() {
        return companyInfoOrder;
    }

    public void setCompanyInfoOrder(String companyInfoOrder) {
        this.companyInfoOrder = companyInfoOrder;
    }

    public String getCompanynOrderId() {
        return companynOrderId;
    }

    public void setCompanynOrderId(String companynOrderId) {
        this.companynOrderId = companynOrderId;
    }

    public String getLogDataID() {  
        return logDataID;
    }

    public void setLogDataID(String logDataID) {
        this.logDataID = logDataID;
    }
}
