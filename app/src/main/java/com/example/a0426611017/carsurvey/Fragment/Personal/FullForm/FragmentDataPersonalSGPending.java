package com.example.a0426611017.carsurvey.Fragment.Personal.FullForm;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonalSG;
import com.example.a0426611017.carsurvey.Object.Personal.FullForm.ObjectDataPersonalSGPending;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalSgInfo;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by 0426591017 on 2/15/2018.
 */

public class FragmentDataPersonalSGPending extends Fragment {
    private View view;
    private Context context;
    private int cameraIdxButton = 0;
    private String nameFile;
    private TakePicture takePicture = new TakePicture();
    private PreviewImage previewImage = new PreviewImage();

    private TextView fileNameIdImage, fileNameIdImageUrl,
            fileNamePersonalNpwp, fileNamePersonalNpwpUrl;

    private ObjectDataPersonalSGPending objectDataPersonalSGPending;
    private ModelDataPersonalSG modelDataPersonalSg = new ModelDataPersonalSG();
    private ModelDDLPersonal ddlPersonal = new ModelDDLPersonal();
    private FileFunction fileFunction = new FileFunction();

    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private String personalOrderId = null;
    private String name = null;
    private Gson gson = new Gson();

    private ResponseDetailPersonalSgInfo responseDetailPersonalSgInfo = new ResponseDetailPersonalSgInfo();

    public FragmentDataPersonalSGPending newInstance(ModelDataPersonalSG modelDataPersonalSG, ModelDDLPersonal ddlPersonal, String orderId, String name) {
        FragmentDataPersonalSGPending fragmentDataPersonalSGPending = new FragmentDataPersonalSGPending();
        Bundle args = new Bundle();
        args.putSerializable("modelDataPersonalSg", modelDataPersonalSG);
        args.putSerializable("ddlPersonal", ddlPersonal);
        args.putString("personalOrderId", orderId);
        args.putString("name", name);
        fragmentDataPersonalSGPending.setArguments(args);
        return fragmentDataPersonalSGPending;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelDataPersonalSg = (ModelDataPersonalSG) getArguments().getSerializable("modelDataPersonalSg");
            ddlPersonal = (ModelDDLPersonal) getArguments().getSerializable("ddlPersonal");
            personalOrderId = getArguments().getString("personalOrderId");
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        objectDataPersonalSGPending = new ObjectDataPersonalSGPending(container, inflater, ddlPersonal);
        view = objectDataPersonalSGPending.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if(modelDataPersonalSg.isModified()){
            objectDataPersonalSGPending.setForm(modelDataPersonalSg);
        }else{
            getDetailInfo(name, personalOrderId);
        }

        fileNameIdImage = objectDataPersonalSGPending.getTextViewPersonalIdImageSg();
        fileNameIdImageUrl = objectDataPersonalSGPending.getTextViewPersonalIdImageSgUrl();
        Button buttonIdImage = objectDataPersonalSGPending.getButtonIdImageSg();

        fileNamePersonalNpwp = objectDataPersonalSGPending.getTextViewPersonalNpwpSg();
        fileNamePersonalNpwpUrl = objectDataPersonalSGPending.getTextViewPersonalNpwpSgUrl();
        Button buttonPersonalNpwp = objectDataPersonalSGPending.getButtonPersonalNpwpSg();

        callCamera(buttonIdImage,1);
        callCamera(buttonPersonalNpwp, 2);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            objectDataPersonalSGPending.setEnabledButtonImage();
        }

        return view;
    }

    private void callCamera(Button button, int idx) {
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(v, idxButton);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                objectDataPersonalSGPending.setEnabledButtonImage();
            }
        }
    }

    public void takePicture(View view, int idx) {
        this.cameraIdxButton = idx;
        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                switch (cameraIdxButton) {
                    case 1:
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectDataPersonalSGPending.setFilePersonalIdImageSg(nameFile);
                        objectDataPersonalSGPending.setFilePersonalIdImageSgUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNameIdImage);
                        break;
                    case 2:
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectDataPersonalSGPending.setFilePersonalNpwpSg(nameFile);
                        objectDataPersonalSGPending.setFilePersonalNpwpSgUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNamePersonalNpwp);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    public ModelDataPersonalSG getForm(){
        return objectDataPersonalSGPending.getForm();
    }

    public void setForm(ModelDataPersonalSG modelDataPersonalSG){
        objectDataPersonalSGPending.setForm(modelDataPersonalSG);
    }

    public ModelDataPersonalSG getComplete(){
        return objectDataPersonalSGPending.getComplete();
    }

    private void getDetailInfo(final String name, final String idOrder){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalSgInfo> infoSgDetail = apiInterface.getDetailPersonalSgInfo(name, idOrder);
            infoSgDetail.enqueue(new Callback<ResponseDetailPersonalSgInfo>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalSgInfo> call, Response<ResponseDetailPersonalSgInfo>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataPersonalSg = response.body().getModelDataPersonalSG();
                        Log.d("Result", gson.toJson(modelDataPersonalSg));

                        setForm(modelDataPersonalSg);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalSgInfo = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalSgInfo.class);
                            error = responseDetailPersonalSgInfo.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailInfo(name, idOrder);
                                    }
                                });
                                b.show();
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.personal_sg_full_edit), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailInfo(name, idOrder);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        Log.e("Error","Service response time out");
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailInfo(name, idOrder);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalSgInfo> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailInfo(name, idOrder);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailInfo(name, idOrder);
                }
            });
            b.show();
            Log.e("Error","No Internet Connectivity");
            loadingDialog.dismiss();
        }
    }
}
