package com.example.a0426611017.carsurvey.MainFunction;

import android.widget.EditText;
import android.widget.TextView;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by c201017001 on 27/03/2018.
 */

public class Currency {
    public static String set(EditText editText){
        String cleanString = editText.getText().toString().replaceAll("[,]", "");

        double parsed = Double.parseDouble(cleanString);
        NumberFormat df = NumberFormat.getCurrencyInstance(new Locale("en", "US"));

        df.setMaximumFractionDigits(0);
        return df.format((parsed));
    }

    public static String set(TextView textView){
        String cleanString = textView.getText().toString().replaceAll("[,]", "");

        double parsed = Double.parseDouble(cleanString);
        NumberFormat df = NumberFormat.getCurrencyInstance(new Locale("en", "US"));

        df.setMaximumFractionDigits(0);
        return df.format((parsed));
    }

    public static String set(String string){
        String cleanString = string.replaceAll("[,]", "");

        double parsed = Double.parseDouble(cleanString);
        NumberFormat df = NumberFormat.getCurrencyInstance(new Locale("en", "US"));

        df.setMaximumFractionDigits(0);
        return df.format((parsed));
    }
}

