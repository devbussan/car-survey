package com.example.a0426611017.carsurvey.Adapter.Company;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.ActivityBanding;
import com.example.a0426611017.carsurvey.AddNew.Company.ActivityNewOrderCompany;
import com.example.a0426611017.carsurvey.Company.DetailFullForm.ActivityCompanyDetail;
import com.example.a0426611017.carsurvey.Company.DetailMiniForm.ActivityCompanyDetailOrder;
import com.example.a0426611017.carsurvey.Company.FullForm.ActivityCompanyFullEdit;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelGridListCompany;
import com.example.a0426611017.carsurvey.R;

import java.util.List;

public class AdapterCustomPendingCompany extends BaseAdapter {
    private static List<ModelGridListCompany> listcompanyPending;
    private LayoutInflater mInflater;
    private Context context;
    private int tabCurrent;
    private int menu;

    public AdapterCustomPendingCompany(Context ActivityListCompanyPending, List<ModelGridListCompany> results, int tab, int menu) {
        listcompanyPending = results;
        context = ActivityListCompanyPending;
        mInflater = LayoutInflater.from(ActivityListCompanyPending);
        this.tabCurrent = tab;
        this.menu = menu;
    }

    @SuppressLint({"SetTextI18n", "InflateParams"})
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final AdapterCustomPendingCompany.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_list_row_pending, null);
            holder = new AdapterCustomPendingCompany.ViewHolder();
            holder.companyDatePendingTextField = convertView.findViewById(R.id.date);
            holder.companyNamePendingTextField = convertView.findViewById(R.id.name);
            holder.companyNo_idPendingTextField = convertView.findViewById(R.id.no_id);
            holder.companyComment = convertView.findViewById(R.id.comment);
            holder.buttonDetail = convertView.findViewById(R.id.buttonDetail);
            holder.buttonEdit = convertView.findViewById(R.id.buttonEdit);
            holder.buttonViewMoreComment = convertView.findViewById(R.id.btn_comment);

            if(tabCurrent == 3){
                holder.buttonEdit.setText("Banding");
            }else{
                holder.buttonEdit.setText("Edit");
            }
            convertView.setTag(holder);

        } else {
            holder = (AdapterCustomPendingCompany.ViewHolder) convertView.getTag();
        }

        final String companyDate = listcompanyPending.get(position).getDateCreate();
        final String companyName = listcompanyPending.get(position).getCompanyName();
        final String companyNo_id = listcompanyPending.get(position).getNpwpNo();


        if(menu == Constans.MENU_COMPANY_ORDER_FORM){
            if(tabCurrent == Constans.TAB_REJECT_ORDER || tabCurrent == Constans.TAB_RETURN_ORDER){
                if(listcompanyPending.get(position).getComMent().equals("-")){
                    holder.companyComment.setText("");
                    holder.buttonViewMoreComment.setVisibility(View.GONE);
                }else{
                    holder.companyComment.setText(listcompanyPending.get(position).getComMent());
                    holder.companyComment.setTextColor(context.getResources().getColor(R.color.color_company));
                }
            }else{
                holder.companyComment.setText("");
                holder.buttonViewMoreComment.setVisibility(View.GONE);
            }
        }

        if(menu == Constans.MENU_COMPANY_FULL_FORM){
            if(tabCurrent == Constans.TAB_REJECT_ORDER || tabCurrent == Constans.TAB_RETURN_ORDER||
                    tabCurrent == Constans.TAB_CHECK_HQ  ){
                if(listcompanyPending.get(position).getComMent().equals("-")){
                    holder.companyComment.setText("");
                    holder.buttonViewMoreComment.setVisibility(View.GONE);
                }else{
                    holder.companyComment.setText(listcompanyPending.get(position).getComMent());
                    holder.companyComment.setTextColor(context.getResources().getColor(R.color.color_company));
                }
            }else {
                holder.companyComment.setText("");
                holder.buttonViewMoreComment.setVisibility(View.GONE);
            }
        }


        holder.companyDatePendingTextField.setText(
                companyDate.split("-")[2]
                        + " " + FormatDate.PRINT_MONTH[Integer.parseInt(companyDate.split("-")[1])]
                        + " " + companyDate.split("-")[0]
        );
        holder.companyNamePendingTextField.setText(companyName);
        holder.companyNo_idPendingTextField.setText(companyNo_id);
        holder.buttonDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                SharedPreferences.Editor editor;
                if (tabCurrent == Constans.TAB_CLEAR || menu == Constans.MENU_COMPANY_FULL_FORM) {
                    intent = new Intent(context, ActivityCompanyDetail.class);
                    editor = context.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_CUST_DATA_ID, listcompanyPending.get(position).getCustDataId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SURVEY_ID, listcompanyPending.get(position).getSurveyId());
                    editor.apply();
                } else {
                    intent = new Intent(context, ActivityCompanyDetailOrder.class);
                }
                intent.putExtra(Constans.KeySharedPreference.COMPANY_ORDER_ID, listcompanyPending.get(position).getCompanyOrderId());
                context.startActivity(intent);

            }
        });

        holder.buttonViewMoreComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom_dialog);
                TextView headerText = dialog.findViewById(R.id.text_view_comment_title);
                TextView contextText = dialog.findViewById(R.id.text_view_comment_message);
                ImageView close = dialog.findViewById(R.id.btn_close_comment);
                TextView commentName = dialog.findViewById(R.id.text_view_comment_name);
                commentName.setText(listcompanyPending.get(position).getCompanyName());
                headerText.setText("Comment");
                contextText.setText(listcompanyPending.get(position).getComMent());
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.create();
                dialog.show();

            }
        });

        holder.buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                SharedPreferences.Editor editor;
                if(tabCurrent == 3){
                    intent = new Intent(context, ActivityBanding.class);
                    intent.putExtra(Constans.KEY_MENU, Constans.MENU_COMPANY);

                }else if (tabCurrent == Constans.TAB_CLEAR || menu == Constans.MENU_COMPANY_FULL_FORM) {
                    intent = new Intent(context, ActivityCompanyFullEdit.class);
                    editor = context.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_CUST_DATA_ID, listcompanyPending.get(position).getCustDataId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SURVEY_ID, listcompanyPending.get(position).getSurveyId());
                    editor.apply();
                } else {
                    intent = new Intent(context, ActivityNewOrderCompany.class);
                }
                editor = context.getSharedPreferences(Constans.KeySharedPreference.LOG_ID, Context.MODE_PRIVATE).edit();
                editor.putString(Constans.KEY_LOG_ID, listcompanyPending.get(position).getLogDataID());
                editor.apply();
                intent.putExtra(Constans.KEY_IS_NEW, false);
                intent.putExtra(Constans.KeySharedPreference.COMPANY_ORDER_ID, listcompanyPending.get(position).getCompanyOrderId());
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    static class ViewHolder {
        TextView companyDatePendingTextField;
        TextView companyNamePendingTextField;
        TextView companyNo_idPendingTextField;
        TextView companyComment;
        Button buttonDetail;
        Button buttonEdit;
        Button buttonViewMoreComment;
    }

    @Override
    public int getCount() {
        return listcompanyPending.size();
    }

    @Override
    public Object getItem(int arg0) {
        return listcompanyPending.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return 0;
    }

}
