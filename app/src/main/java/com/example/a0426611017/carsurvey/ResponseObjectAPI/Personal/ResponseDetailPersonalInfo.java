package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.Personal.ModelDataAddress;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonal;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426591017 on 3/6/2018.
 */

public class ResponseDetailPersonalInfo {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDataPersonal modelDataPersonal;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDataPersonal getModelDataPersonal() {
        return modelDataPersonal;
    }

    public void setModelDataPersonal(ModelDataPersonal modelDataPersonal) {
        this.modelDataPersonal = modelDataPersonal;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
