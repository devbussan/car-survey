package com.example.a0426611017.carsurvey.ResponseObjectAPI.Company;

import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelObjectGridListCompany;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 22/02/2018.
 */

public class ResponseGridListCompany {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelObjectGridListCompany modelObjectGridListCompany;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelObjectGridListCompany getModelObjectGridListCompany() {
        return modelObjectGridListCompany;
    }

    public void setModelObjectGridListCompany(ModelObjectGridListCompany modelObjectGridListCompany) {
        this.modelObjectGridListCompany = modelObjectGridListCompany;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
