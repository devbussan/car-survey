package com.example.a0426611017.carsurvey.Model.Company;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by c201017001 on 03/01/2018.
 */

public class ModelAddressInfo implements Serializable {

    @SerializedName("COMPANY_CUST_ADDR_ID")
    private String companyCustAddrId;

    @SerializedName("COMPANY_CUST_DATA_ID")
    private String companyCustDataId;

    @SerializedName("STATUS")
    private String status;

    @SerializedName("COMPANYADDR")
    private String address;

    @SerializedName("COMPANYRT")
    private String rt;

    @SerializedName("COMPANYRW")
    private String rw;

    @SerializedName("ZIPCODE")
    private String zipcode;

    @SerializedName("KELURAHAN")
    private String kelurahan;

    @SerializedName("KECAMATAN")
    private String kecamatan;

    @SerializedName("CITY")
    private String city;

    @SerializedName("PHONE")
    private String phone;

    @SerializedName("FAX")
    private String fax;

    @SerializedName("ADDRESSTYPENAME")
    private String addressType;

    private String nameFileBuildingImage;

    @SerializedName("BLDIMGURL")
    private String biuldingImageUrl;

    @SerializedName("BLDPRICEEST")
    private String price;

    @SerializedName("STAYLENGTH")
    private String stayLength;

    @SerializedName("DIRECTIONDESC")
    private String description;

    @SerializedName("NOTES")
    private String notes;

    @SerializedName("BLDLOCCLASS")
    private String locationClass;

    @SerializedName("BLDOWNER")
    private String ownership;

    @SerializedName("USRCRT")
    private String userCreate;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getCompanyCustAddrId() {
        return companyCustAddrId;
    }

    public void setCompanyCustAddrId(String companyCustAddrId) {
        this.companyCustAddrId = companyCustAddrId;
    }

    public String getCompanyCustDataId() {
        return companyCustDataId;
    }

    public void setCompanyCustDataId(String companyCustDataId) {
        this.companyCustDataId = companyCustDataId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRt() {
        return rt;
    }

    public void setRt(String rt) {
        this.rt = rt;
    }

    public String getRw() {
        return rw;
    }

    public void setRw(String rw) {
        this.rw = rw;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getNameFileBuildingImage() {
        return nameFileBuildingImage;
    }

    public void setNameFileBuildingImage(String nameFileBuildingImage) {
        this.nameFileBuildingImage = nameFileBuildingImage;
    }

    public String getBiuldingImageUrl() {
        return biuldingImageUrl;
    }

    public void setBiuldingImageUrl(String biuldingImageUrl) {
        this.biuldingImageUrl = biuldingImageUrl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getStayLength() {
        return stayLength;
    }

    public void setStayLength(String stayLength) {
        this.stayLength = stayLength;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getLocationClass() {
        return locationClass;
    }

    public void setLocationClass(String locationClass) {
        this.locationClass = locationClass;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
