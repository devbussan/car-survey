package com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataEmergencyContact;
import com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm.ObjectDataEmergencyContactComplete;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalEmergency;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDataEmergencyContactComplete extends Fragment {
    private View view;
    private ObjectDataEmergencyContactComplete objectDataEmergencyContactComplete;
    private ModelDataEmergencyContact modelDataEmergencyContact = new ModelDataEmergencyContact();

    private String name;
    private Context context;
    private String custid;
    private Gson gson = new Gson();
    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;

    private ResponseDetailPersonalEmergency responseDetailPersonalEmergency = new ResponseDetailPersonalEmergency();

    public FragmentDataEmergencyContactComplete newInstance(String custid, String name){
        FragmentDataEmergencyContactComplete fragmentDataEmergencyContactComplete = new FragmentDataEmergencyContactComplete();
        Bundle args = new Bundle();
        args.putString("custid", custid);
        args.putString("name", name);
        fragmentDataEmergencyContactComplete.setArguments(args);
        return fragmentDataEmergencyContactComplete;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            custid = getArguments().getString("custid");
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        objectDataEmergencyContactComplete = new ObjectDataEmergencyContactComplete(viewGroup, inflater);
        view = objectDataEmergencyContactComplete.getView();
        context = viewGroup.getContext();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading.........");
        loadingDialog.setCancelable(false);

        getDetailEmergency(name, custid);

        return view;
    }

    public void setForm(ModelDataEmergencyContact modelDataEmergencyContact){
        objectDataEmergencyContactComplete.setForm(modelDataEmergencyContact);
    }

    private void getDetailEmergency(final String name, String idcustdata){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalEmergency> emergencyDetail = apiInterface.getDetailPersonalEmergency(name, idcustdata);
            emergencyDetail.enqueue(new Callback<ResponseDetailPersonalEmergency>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalEmergency> call, Response<ResponseDetailPersonalEmergency>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataEmergencyContact = response.body().getModelDataEmergency();
                        modelDataEmergencyContact.setModified(true);
                        setForm(modelDataEmergencyContact);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalEmergency = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalEmergency.class);
                            error = responseDetailPersonalEmergency.getMessage();
                            if(!error.equals("Data Not Found")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage("Cannot Connect to Server \n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailEmergency(name, custid);
                                    }
                                });
                                b.show();

                            }else{
                                Snackbar.make(view.findViewById(R.id.emergency_detail), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage("Cannot Connect to Server \n" +
                                    "Please try Again ?");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailEmergency(name, custid);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server \n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailEmergency(name, custid);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalEmergency> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server \n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailEmergency(name, custid);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server \n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailEmergency(name, custid);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }
}
