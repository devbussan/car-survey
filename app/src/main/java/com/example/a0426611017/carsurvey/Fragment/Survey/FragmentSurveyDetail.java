package com.example.a0426611017.carsurvey.Fragment.Survey;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Survey.ModelSurvey;
import com.example.a0426611017.carsurvey.Object.Survey.ObjectSurveyDetail;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseGetCompanySurvey;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentSurveyDetail extends Fragment {

    private int menu;
    private View view;
    private Context context;
    private ObjectSurveyDetail objectSurveyDetail;

    private String orderid;
    private String name;
    private Gson gson = new Gson();
    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;
    private ResponseGetCompanySurvey responseGetCompanySurvey = new ResponseGetCompanySurvey();

    public static FragmentSurveyDetail newInstance(int menu, String name, String orderid) {
        FragmentSurveyDetail fragment = new FragmentSurveyDetail();
        Bundle args = new Bundle();
        args.putInt(Constans.KEY_MENU, menu);
        args.putString("name", name);
        args.putString("orderid", orderid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            menu = getArguments().getInt(Constans.KEY_MENU);
            orderid = getArguments().getString("orderid");
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectSurveyDetail = new ObjectSurveyDetail(container, inflater);
        view = objectSurveyDetail.getView();

        if(menu == Constans.MENU_PERSONAL){
            objectSurveyDetail.setFormForPersonal();
        }else{
            objectSurveyDetail.setFormForCompany();
        }

        context = container.getContext();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        getDetailSurvey(orderid, name);

        return view;


    }

    public void setForm(ModelSurvey modelSurvey){
        objectSurveyDetail.setForm(modelSurvey);
    }

    public void getDetailSurvey(String idOrder, final String name) {
        loadingDialog.show();
        if(InternetConnection.checkConnection(context)){
            Call<ResponseGetCompanySurvey> getDetailCompany = apiInterface.getDetailSurveyCompany(idOrder,name);
            getDetailCompany.enqueue(new Callback<ResponseGetCompanySurvey>() {
                @Override
                public void onResponse(Call<ResponseGetCompanySurvey> call, Response<ResponseGetCompanySurvey> response) {
                    if(response.isSuccessful()){
                        responseGetCompanySurvey=response.body();
                        if(responseGetCompanySurvey.getStatus().equals("success")){
                            setForm(responseGetCompanySurvey.getModelSurvey());
                            loadingDialog.dismiss();
                        }
                        loadingDialog.dismiss();
                    }else if (response.code() == 404) {

                        String error = "";
                        try {

                            responseGetCompanySurvey = gson.fromJson(response.errorBody().string(),ResponseGetCompanySurvey.class);
                            error = responseGetCompanySurvey.getMessage();
                            if(!error.equals("Data Not Found")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage("Cannot Connect to Server \n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailSurvey(orderid, name);
                                    }
                                });
                                b.show();
                            }else{
                                Snackbar.make(view.findViewById(R.id.survey_detail), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage("Cannot Connect to Server \n" +
                                    "Please try Again ?");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailSurvey(orderid, name);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    }else {
                        Log.e("Other Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server \n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailSurvey(orderid, name);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<ResponseGetCompanySurvey> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server \n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailSurvey(orderid, name);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        }else {
            Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server \n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailSurvey(orderid, name);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }

}
