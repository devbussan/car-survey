package com.example.a0426611017.carsurvey.Company.DetailFullForm;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.a0426611017.carsurvey.Fragment.Company.DetailFullForm.FragmentAddressInfo;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailFullForm.FragmentCompanyInfo;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailFullForm.FragmentMandatory;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentContactInfo2DetailOrder;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentContactInfo3DetailOrder;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentContactInfo4DetailOrder;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentContactInfo5DetailOrder;
import com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm.FragmentContactInfoDetailOrder;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.ModelAddressInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelCompanyInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelMandatoryInfo;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseDetailContactInfo;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseContactOrderId;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailAddressFull;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailCompanyFull;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailEvidenceFull;
import com.example.a0426611017.carsurvey.Survey.ActivitySurveyDetail;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityCompanyDetail extends AppCompatActivity {

    Button buttonCompany, buttonAddress, buttonMandatory,
            buttonContact, buttonContact2, buttonContact3, buttonContact4, buttonContact5,
            buttonNext, buttonBack;

    private FragmentCompanyInfo fragmentCompanyInfo = new FragmentCompanyInfo();
    private FragmentAddressInfo fragmentAddressInfo = new FragmentAddressInfo();
    private FragmentMandatory fragmentMandatory = new FragmentMandatory();
    private FragmentContactInfoDetailOrder fragmentContactInfoDetailOrder = new FragmentContactInfoDetailOrder();
    private FragmentContactInfo2DetailOrder fragmentContactInfoDetailOrder2 = new FragmentContactInfo2DetailOrder();
    private FragmentContactInfo3DetailOrder fragmentContactInfoDetailOrder3 = new FragmentContactInfo3DetailOrder();
    private FragmentContactInfo4DetailOrder fragmentContactInfoDetailOrder4 = new FragmentContactInfo4DetailOrder();
    private FragmentContactInfo5DetailOrder fragmentContactInfoDetailOrder5 = new FragmentContactInfo5DetailOrder();


    private ModelCompanyInfo modelCompanyInfo = new ModelCompanyInfo();
    private ModelAddressInfo modelAddressInfo = new ModelAddressInfo();
    private ModelDetailContactInfo modelDetailContactInfo = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo2 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo3 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo4 = new ModelDetailContactInfo();
    private ModelDetailContactInfo modelDetailContactInfo5 = new ModelDetailContactInfo();
    private ModelMandatoryInfo modelMandatoryInfo = new ModelMandatoryInfo();

    private ResponseContactOrderId responseContactOrderId = new ResponseContactOrderId();
    private ResponseDetailContactInfo responseDetailContactInfo = new ResponseDetailContactInfo();
    private ResponseDetailCompanyFull responseDetailCompanyFull = new ResponseDetailCompanyFull();

    private List<String> contactOrder = new ArrayList<>();

    private Gson gson = new Gson();

    private String companyOrderId;

    private ProgressDialog loadingDialog;

    private ApiInterface apiInterface;

    private Fragment currenFragment = new Fragment();
    private String custDataId = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_company_detail);
        window.setStatusBarColor(getResources().getColor(R.color.color_company));
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        buttonCompany = findViewById(R.id.button_company);
        buttonAddress = findViewById(R.id.button_address);
        buttonMandatory = findViewById(R.id.button_mandatory);
        buttonContact = findViewById(R.id.button_contact);
        buttonContact2 = findViewById(R.id.button_contact2);
        buttonContact3 = findViewById(R.id.button_contact3);
        buttonContact4 = findViewById(R.id.button_contact4);
        buttonContact5 = findViewById(R.id.button_contact5);

        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        companyOrderId = getIntent().getStringExtra(Constans.KeySharedPreference.COMPANY_ORDER_ID);
        custDataId = this.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, MODE_PRIVATE).getString(Constans.KEY_CUST_DATA_ID, null);

        getContactOrderId(companyOrderId);

        buttonCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCompany.setEnabled(false);
                buttonCompany.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonCompany.setTextColor(getResources().getColor(R.color.color_company));

                setButton();

                fragmentCompanyInfo = fragmentCompanyInfo.newInstance(modelCompanyInfo,companyOrderId);

                loadFragment(fragmentCompanyInfo);
            }
        });

        buttonAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonAddress.setEnabled(false);
                buttonAddress.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonAddress.setTextColor(getResources().getColor(R.color.color_company));

                setButton();
                fragmentAddressInfo=fragmentAddressInfo.newInstance(modelAddressInfo,custDataId);

                loadFragment(fragmentAddressInfo);
            }
        });

        buttonMandatory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonMandatory.setEnabled(false);
                buttonMandatory.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonMandatory.setTextColor(getResources().getColor(R.color.color_company));

                setButton();

                fragmentMandatory=fragmentMandatory.newInstance(modelMandatoryInfo,custDataId);

                loadFragment(fragmentMandatory);
            }
        });

        buttonContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContact.setEnabled(false);
                buttonContact.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContact.setTextColor(getResources().getColor(R.color.color_company));

                setButton();

                if(contactOrder.size() > 0){
                    fragmentContactInfoDetailOrder = fragmentContactInfoDetailOrder.newInstance(contactOrder.get(0), companyOrderId);
                    //getDetailContact(contactOrder.get(0), companyOrderId, 1);
                }

                loadFragment(fragmentContactInfoDetailOrder);
            }
        });

        buttonContact2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContact2.setEnabled(false);
                buttonContact2.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContact2.setTextColor(getResources().getColor(R.color.color_company));

                setButton();

                if(contactOrder.size() > 1){
                    fragmentContactInfoDetailOrder2=fragmentContactInfoDetailOrder2.newInstance(contactOrder.get(1), companyOrderId);
//                    getDetailContact(contactOrder.get(1), companyOrderId, 2);
                }

                loadFragment(fragmentContactInfoDetailOrder2);
            }
        });

        buttonContact3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContact3.setEnabled(false);
                buttonContact3.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContact3.setTextColor(getResources().getColor(R.color.color_company));

                setButton();

                if(contactOrder.size() > 2){
                    fragmentContactInfoDetailOrder3=fragmentContactInfoDetailOrder3.newInstance(contactOrder.get(2), companyOrderId);
//                    getDetailContact(contactOrder.get(2), companyOrderId, 3);
                }

                loadFragment(fragmentContactInfoDetailOrder3);
            }
        });

        buttonContact4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContact4.setEnabled(false);
                buttonContact4.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContact4.setTextColor(getResources().getColor(R.color.color_company));

                setButton();

                if(contactOrder.size() > 3){
                    fragmentContactInfoDetailOrder4=fragmentContactInfoDetailOrder4.newInstance(contactOrder.get(3), companyOrderId);
                    //getDetailContact(contactOrder.get(3), companyOrderId, 4);
                }

                loadFragment(fragmentContactInfoDetailOrder4);
            }
        });

        buttonContact5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonContact5.setEnabled(false);
                buttonContact5.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonContact5.setTextColor(getResources().getColor(R.color.color_company));

                setButton();

                if(contactOrder.size() > 4){
                    fragmentContactInfoDetailOrder5=fragmentContactInfoDetailOrder5.newInstance(contactOrder.get(4), companyOrderId);
//                    getDetailContact(contactOrder.get(4), companyOrderId, 5);
                }

                loadFragment(fragmentContactInfoDetailOrder5);
            }
        });

        buttonNext = findViewById(R.id.button_next_company_detail);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityCompanyDetail.this, ActivitySurveyDetail.class);
                intent.putExtra(Constans.KEY_MENU, Constans.MENU_COMPANY);
                intent.putExtra(Constans.KeySharedPreference.ORDER_ID, companyOrderId);
                startActivity(intent);
//                Toast.makeText(ActivityCompanyDetail.this, "Button Next Under Maintenis", Toast.LENGTH_SHORT).show();
            }
        });

        buttonBack = findViewById(R.id.button_back_company_detail);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompanyDetail.super.onBackPressed();
            }
        });

    }

    private void setButton(){
        if (currenFragment != null){
            if(currenFragment.getClass() == FragmentCompanyInfo.class){
                buttonCompany.setEnabled(true);
                buttonCompany.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCompany.setTextColor(getResources().getColor(R.color.color_white));
            }else if(currenFragment.getClass() == FragmentAddressInfo.class){
                buttonAddress.setEnabled(true);
                buttonAddress.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonAddress.setTextColor(getResources().getColor(R.color.color_white));
            }else if(currenFragment.getClass() == FragmentMandatory.class){
                buttonMandatory.setEnabled(true);
                buttonMandatory.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonMandatory.setTextColor(getResources().getColor(R.color.color_white));
            }else if(currenFragment.getClass() == FragmentContactInfoDetailOrder.class){
                buttonContact.setEnabled(true);
                buttonContact.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact.setTextColor(getResources().getColor(R.color.color_white));
            }else if(currenFragment.getClass() == FragmentContactInfo2DetailOrder.class){
                buttonContact2.setEnabled(true);
                buttonContact2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact2.setTextColor(getResources().getColor(R.color.color_white));
            }else if(currenFragment.getClass() == FragmentContactInfo3DetailOrder.class){
                buttonContact3.setEnabled(true);
                buttonContact3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact3.setTextColor(getResources().getColor(R.color.color_white));
            }else if(currenFragment.getClass() == FragmentContactInfo4DetailOrder.class){
                buttonContact4.setEnabled(true);
                buttonContact4.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact4.setTextColor(getResources().getColor(R.color.color_white));
            }else if(currenFragment.getClass() == FragmentContactInfo5DetailOrder.class){
                buttonContact5.setEnabled(true);
                buttonContact5.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonContact5.setTextColor(getResources().getColor(R.color.color_white));
            }
        }
    }

    private void loadFragment(Fragment fragment) {
// create a FragmentManager
        FragmentManager fm = getSupportFragmentManager();
// create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        currenFragment= fragment;
        fragmentTransaction.replace(R.id.frame_company_complete, fragment);
        fragmentTransaction.commit(); // save the changes
    }

    private void getContactOrderId(String companyOrderId) {
        loadingDialog.show();
        if (InternetConnection.checkConnection(this)) {
            final Call<ResponseContactOrderId> contactOrderID = apiInterface.getContactOrderId(companyOrderId);
            contactOrderID.enqueue(new Callback<ResponseContactOrderId>() {
                @Override
                public void onResponse(@NonNull Call<ResponseContactOrderId> call, @NonNull Response<ResponseContactOrderId>
                        response) {
                    if (response.isSuccessful()) {
                        responseContactOrderId = response.body();
                        assert responseContactOrderId != null;
                        Log.d("Contact Order Id", String.valueOf(responseContactOrderId.getModelContactOrderId().getContactInfo()));
                        contactOrder = responseContactOrderId.getModelContactOrderId().getContactInfo();
                        for (int i = 0; i < contactOrder.size(); i++) {
                            switch (i) {
                                case 0:
                                    buttonContact.setVisibility(View.VISIBLE);
                                    break;
                                case 1:
                                    buttonContact2.setVisibility(View.VISIBLE);
                                    break;
                                case 2:
                                    buttonContact3.setVisibility(View.VISIBLE);
                                    break;
                                case 3:
                                    buttonContact4.setVisibility(View.VISIBLE);
                                    break;
                                case 4:
                                    buttonContact5.setVisibility(View.VISIBLE);
                                    break;
                                default:
                                    break;
                            }
                        }
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseContactOrderId = gson.fromJson(response.errorBody().string(), ResponseContactOrderId.class);
                            error = responseContactOrderId.getMessage();
                        } catch (IOException e) {
                            error = e.getMessage();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        Snackbar.make(findViewById(R.id.layout_company_detail), error,
                                Snackbar.LENGTH_LONG)
                                .show();
                        loadingDialog.dismiss();
                    } else {
                        loadingDialog.dismiss();
                        Snackbar.make(findViewById(R.id.layout_company_detail), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseContactOrderId> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    Snackbar.make(findViewById(R.id.layout_company_detail), t.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(R.id.layout_company_detail), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            loadingDialog.dismiss();
        }
    }

    private void getDetailContact(String contactOrderId, String companyOrderId, final int idx) {
        loadingDialog.show();
        if (InternetConnection.checkConnection(this)) {
            final Call<ResponseDetailContactInfo> contactDetail = apiInterface.getDetailContactInfo(contactOrderId, companyOrderId);
            contactDetail.enqueue(new Callback<ResponseDetailContactInfo>() {
                @Override
                public void onResponse(Call<ResponseDetailContactInfo> call, Response<ResponseDetailContactInfo>
                        response) {
                    if (response.isSuccessful()) {
                        switch (idx) {
                            case 1:
                                modelDetailContactInfo = response.body().getModelDetailContactInfo();
                                modelDetailContactInfo.setModified(true);
                                fragmentContactInfoDetailOrder.setForm(modelDetailContactInfo);
                                break;
                            case 2:
                                modelDetailContactInfo2 = response.body().getModelDetailContactInfo();
                                fragmentContactInfoDetailOrder2.setForm(modelDetailContactInfo2);
                                modelDetailContactInfo2.setModified(true);
                                break;
                            case 3:
                                modelDetailContactInfo3 = response.body().getModelDetailContactInfo();
                                fragmentContactInfoDetailOrder3.setForm(modelDetailContactInfo3);
                                modelDetailContactInfo3.setModified(true);
                                break;
                            case 4:
                                modelDetailContactInfo4 = response.body().getModelDetailContactInfo();
                                fragmentContactInfoDetailOrder4.setForm(modelDetailContactInfo4);
                                modelDetailContactInfo4.setModified(true);
                                break;
                            case 5:
                                modelDetailContactInfo5 = response.body().getModelDetailContactInfo();
                                fragmentContactInfoDetailOrder5.setForm(modelDetailContactInfo5);
                                modelDetailContactInfo5.setModified(true);
                                break;
                            default:
                                break;
                        }

                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailContactInfo = gson.fromJson(response.errorBody().string(), ResponseDetailContactInfo.class);
                            error = responseDetailContactInfo.getMessage();
                        } catch (IOException e) {
                            error = e.getMessage();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        Snackbar.make(findViewById(R.id.layout_company_detail), error,
                                Snackbar.LENGTH_LONG)
                                .show();
                        loadingDialog.dismiss();
                    } else {
                        loadingDialog.dismiss();
                        Snackbar.make(findViewById(R.id.layout_company_detail), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailContactInfo> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    Snackbar.make(findViewById(R.id.layout_company_detail), t.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(R.id.layout_company_detail), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            loadingDialog.dismiss();
        }
    }

//    private void getDetailCompanyFull(String companyOrderid){
//        loadingDialog.show();
//        if (InternetConnection.checkConnection(this)) {
//            final Call<ResponseDetailCompanyFull> detailcompany = apiInterface.getDetailFullCompany(companyOrderid);
//            detailcompany.enqueue(new Callback<ResponseDetailCompanyFull>() {
//                @Override
//                public void onResponse(Call<ResponseDetailCompanyFull> call, Response<ResponseDetailCompanyFull>
//                        response) {
//                    if (response.isSuccessful()) {
//                        modelCompanyInfo = response.body().getModelCompanyInfo();
//                        modelCompanyInfo.setModified(true);
//                        fragmentCompanyInfo.setForm(modelCompanyInfo);
//                        loadingDialog.dismiss();
//                    } else if (response.code() == 404) {
//                        String error = "";
//                        try {
//                            responseDetailCompanyFull = gson.fromJson(response.errorBody().string(), ResponseDetailCompanyFull.class);
//                            error = responseDetailCompanyFull.getMessage();
//                        } catch (IOException e) {
//                            error = e.getMessage();
//                            e.printStackTrace();
//                        }
//                        Log.e("Error", error);
//                        Snackbar.make(findViewById(R.id.layout_company_detail), error,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                        loadingDialog.dismiss();
//                    } else {
//                        loadingDialog.dismiss();
//                        Snackbar.make(findViewById(R.id.layout_company_detail), R.string.error_api,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseDetailCompanyFull> call, Throwable t) {
//                    Log.e("Error Retrofit", t.toString());
//                    Snackbar.make(findViewById(R.id.layout_company_detail), t.toString(),
//                            Snackbar.LENGTH_LONG)
//                            .show();
//                    loadingDialog.dismiss();
//                }
//            });
//        } else {
//            Snackbar.make(findViewById(R.id.layout_company_detail), R.string.no_connectivity,
//                    Snackbar.LENGTH_LONG)
//                    .show();
//            loadingDialog.dismiss();
//        }
//    }

//    private void getDetailAddressFull(String custDataId){
//        loadingDialog.show();
//        if (InternetConnection.checkConnection(this)) {
//            final Call<ResponseDetailAddressFull> addressDetail = apiInterface.getDetailFullAddress(custDataId);
//            addressDetail.enqueue(new Callback<ResponseDetailAddressFull>() {
//                @Override
//                public void onResponse(@NonNull Call<ResponseDetailAddressFull> call, @NonNull Response<ResponseDetailAddressFull>
//                        response) {
//                    if (response.isSuccessful()) {
//                        modelAddressInfo = response.body().getModelAddressInfo();
//                        modelAddressInfo.setModified(true);
//                        fragmentAddressInfo.setForm(modelAddressInfo);
//                        loadingDialog.dismiss();
//                    } else if (response.code() == 404) {
//                        String error = "";
//                        try {
//                            responseDetailCompanyFull = gson.fromJson(response.errorBody().string(), ResponseDetailCompanyFull.class);
//                            error = responseDetailCompanyFull.getMessage();
//                        } catch (IOException e) {
//                            error = e.getMessage();
//                            e.printStackTrace();
//                        }
//                        Log.e("Error", error);
//                        Snackbar.make(findViewById(R.id.layout_company_detail), error,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                        loadingDialog.dismiss();
//                    } else {
//                        loadingDialog.dismiss();
//                        Snackbar.make(findViewById(R.id.layout_company_detail), R.string.error_api,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseDetailAddressFull> call, Throwable t) {
//                    Log.e("Error Retrofit", t.toString());
//                    Snackbar.make(findViewById(R.id.layout_company_detail), t.toString(),
//                            Snackbar.LENGTH_LONG)
//                            .show();
//                    loadingDialog.dismiss();
//                }
//            });
//        } else {
//            Snackbar.make(findViewById(R.id.layout_company_detail), R.string.no_connectivity,
//                    Snackbar.LENGTH_LONG)
//                    .show();
//            loadingDialog.dismiss();
//        }
//    }

//    private void getDetailEvidenceFull(String custDataId){
//        loadingDialog.show();
//        if (InternetConnection.checkConnection(this)) {
//            final Call<ResponseDetailEvidenceFull> evidenceDetail = apiInterface.getDetailFullEvidence(custDataId);
//            evidenceDetail.enqueue(new Callback<ResponseDetailEvidenceFull>() {
//                @Override
//                public void onResponse(Call<ResponseDetailEvidenceFull> call, Response<ResponseDetailEvidenceFull>
//                        response) {
//                    if (response.isSuccessful()) {
//                        modelMandatoryInfo = response.body().getModelMandatoryInfo();
//                        modelMandatoryInfo.setModified(true);
//                        fragmentMandatory.setForm(modelMandatoryInfo);
//                        loadingDialog.dismiss();
//                    } else if (response.code() == 404) {
//                        String error = "";
//                        try {
//                            responseDetailCompanyFull = gson.fromJson(response.errorBody().string(), ResponseDetailCompanyFull.class);
//                            error = responseDetailCompanyFull.getMessage();
//                        } catch (IOException e) {
//                            error = e.getMessage();
//                            e.printStackTrace();
//                        }
//                        Log.e("Error", error);
//                        Snackbar.make(findViewById(R.id.layout_company_detail), error,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                        loadingDialog.dismiss();
//                    } else {
//                        loadingDialog.dismiss();
//                        Snackbar.make(findViewById(R.id.layout_company_detail), R.string.error_api,
//                                Snackbar.LENGTH_LONG)
//                                .show();
//                    }
//                }
//
//                @Override
//                public void onFailure(Call<ResponseDetailEvidenceFull> call, Throwable t) {
//                    Log.e("Error Retrofit", t.toString());
//                    Snackbar.make(findViewById(R.id.layout_company_detail), t.toString(),
//                            Snackbar.LENGTH_LONG)
//                            .show();
//                    loadingDialog.dismiss();
//                }
//            });
//        } else {
//            Snackbar.make(findViewById(R.id.layout_company_detail), R.string.no_connectivity,
//                    Snackbar.LENGTH_LONG)
//                    .show();
//            loadingDialog.dismiss();
//        }
 //   }

}
