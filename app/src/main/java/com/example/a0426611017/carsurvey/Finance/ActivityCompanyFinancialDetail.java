package com.example.a0426611017.carsurvey.Finance;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Fragment.Financial.FragmentFinancialDetail;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.Model.Financial.ModelFinance;
import com.example.a0426611017.carsurvey.R;

public class ActivityCompanyFinancialDetail extends AppCompatActivity {
    private TextView titleBar;
    private Button buttonDataFinancial,
            buttonBack, buttonExit;
    private ModelFinance modelFinance = new ModelFinance();
    private FragmentFinancialDetail fragmentFinancialDetail = new FragmentFinancialDetail();
    private String orderid;
    private int menuCategory;
    int menu;
    //private FragmentFinancial fragmentFinancial = new FragmentFinancial();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_company_financial_detail);
        setfield();
        //setFormForCompany();
        menuCategory = getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, MODE_PRIVATE).getInt(Constans.KEY_MENU_CATEGORY, 0);
        orderid = getIntent().getStringExtra(Constans.KeySharedPreference.ORDER_ID);
        menu = getIntent().getIntExtra(Constans.KEY_MENU, 0);
        switch (menu){
            case Constans.MENU_PERSONAL:
                window.setStatusBarColor(getResources().getColor(R.color.color_personal));
                setFormForPersonal();
                break;
            case Constans.MENU_COMPANY:
                window.setStatusBarColor(getResources().getColor(R.color.color_company));
                setFormForCompany();

                break;
            default:
                break;
        }


    }

    private void setFormForPersonal() {
        titleBar.setText("DATA FINANCIAL");
        titleBar.setBackgroundColor(getResources().getColor(R.color.color_personal));
        buttonBack.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonExit.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompanyFinancialDetail.super.onBackPressed();
            }
        });

        buttonDataFinancial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                buttonDataFinancial.setEnabled(false);
                buttonDataFinancial.setBackground(getDrawable(R.drawable.disabled_button_background_personal));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_personal));

                loadFragment(FragmentFinancialDetail.newInstance(menu, orderid, Constans.PARAM_WS_PERSONAL));
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityCompanyFinancialDetail.this, MainActivityDrawer.class);
                intent.putExtra(Constans.KEY_MENU,menuCategory);
                intent.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                startActivity(intent);
            }
        });
    }

    private void setFormForCompany() {
        titleBar.setText("DATA FINANCIAL");
        titleBar.setBackgroundColor(getResources().getColor(R.color.color_company));
        buttonBack.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonExit.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));

        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompanyFinancialDetail.super.onBackPressed();
            }
        });

        buttonDataFinancial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                buttonDataFinancial.setEnabled(false);
                buttonDataFinancial.setBackground(getDrawable(R.drawable.disabled_button_background_company));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_company));

                loadFragment( FragmentFinancialDetail.newInstance(menu, orderid, Constans.PARAM_WS_COMPANY));
            }
        });

        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityCompanyFinancialDetail.this, MainActivityDrawer.class);
                intent.putExtra(Constans.KEY_MENU,menuCategory);
                intent.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                startActivity(intent);
            }
        });
    }


    private void loadFragment (Fragment fragment){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frame_financial_detail, fragment);
        fragmentTransaction.commit();
    }


    private void setfield() {
        titleBar = findViewById(R.id.text_view_financial);
        buttonBack = findViewById(R.id.button_back_financial);
        buttonDataFinancial = findViewById(R.id.button_data_financial);
        buttonExit = findViewById(R.id.button_exit_financial);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
