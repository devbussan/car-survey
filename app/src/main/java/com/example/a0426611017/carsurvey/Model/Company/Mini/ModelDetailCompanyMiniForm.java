package com.example.a0426611017.carsurvey.Model.Company.Mini;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by c201017001 on 26/02/2018.
 */

public class ModelDetailCompanyMiniForm implements Serializable {
    @SerializedName("COMPANY_ORDER_ID")
    String companyOrderID;

    @SerializedName("COMPANY_NAME")
    String companyName;

    @SerializedName("NPWP_NO")
    String npwpNo;

    @SerializedName("LOGDATACMPID")
    String logDataCompanyID;

    private boolean isModified = false;
    private boolean isComplete = false;

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getCompanyOrderID() {
        return companyOrderID;
    }

    public void setCompanyOrderID(String companyOrderID) {
        this.companyOrderID = companyOrderID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getNpwpNo() {
        return npwpNo;
    }

    public void setNpwpNo(String npwpNo) {
        this.npwpNo = npwpNo;
    }

    public String getLogDataCompanyID() {
        return logDataCompanyID;
    }

    public void setLogDataCompanyID(String logDataCompanyID) {
        this.logDataCompanyID = logDataCompanyID;
    }
}
