package com.example.a0426611017.carsurvey.Fragment.Company.DetailFullForm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.ModelCompanyInfo;
import com.example.a0426611017.carsurvey.Object.Company.DetailFullForm.ObjectCompanyInfoDetail;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailCompanyFull;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCompanyInfo extends Fragment {
    private static final String ARG_PARAM1 = "param1";

    private Context context;
    private View view;
    private ObjectCompanyInfoDetail objectCompanyInfo;

    private String companyOrderId="0";
    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;
    private Gson gson = new Gson();

    private ModelCompanyInfo modelCompanyInfo = new ModelCompanyInfo();
    private ResponseDetailCompanyFull responseDetailCompanyFull = new ResponseDetailCompanyFull();

    public FragmentCompanyInfo newInstance(ModelCompanyInfo modelCompanyInfo,String companyOrderId) {
        FragmentCompanyInfo fragmentCompanyInfo = new FragmentCompanyInfo();
        Bundle args = new Bundle();
        args.putSerializable("modelCompanyInfo", modelCompanyInfo);
        args.putString(ARG_PARAM1,companyOrderId);
        fragmentCompanyInfo.setArguments(args);
        return fragmentCompanyInfo;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            companyOrderId = getArguments().getString(ARG_PARAM1);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                Bundle savedInstanceState) {
        objectCompanyInfo = new ObjectCompanyInfoDetail(container, inflater);
        view = objectCompanyInfo.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        getDetailCompanyFull(companyOrderId);

        return view;
    }

    public void setForm(ModelCompanyInfo modelCompanyInfo){
        objectCompanyInfo.setForm(modelCompanyInfo);
    }


    private void getDetailCompanyFull(String companyOrderid){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailCompanyFull> detailcompany = apiInterface.getDetailFullCompany(companyOrderid);
            detailcompany.enqueue(new Callback<ResponseDetailCompanyFull>() {
                @Override
                public void onResponse(Call<ResponseDetailCompanyFull> call, Response<ResponseDetailCompanyFull>
                        response) {
                    if (response.isSuccessful()) {
                        modelCompanyInfo = response.body().getModelCompanyInfo();
                        modelCompanyInfo.setModified(true);
                        setForm(modelCompanyInfo);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailCompanyFull = gson.fromJson(response.errorBody().string(), ResponseDetailCompanyFull.class);
                            error = responseDetailCompanyFull.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailCompanyFull(companyOrderId);
                                    }
                                });
                            }else if(!error.equals("Data Not Found")) {
                                Snackbar.make(view.findViewById(R.id.layout_company_detail), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailCompanyFull(companyOrderId);
                                }
                            });
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage(error+"\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailCompanyFull(companyOrderId);
                            }
                        });
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage(R.string.error_api+"\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailCompanyFull(companyOrderId);
                            }
                        });
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailCompanyFull> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());

                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.toString()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailCompanyFull(companyOrderId);
                        }
                    });
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage(R.string.no_connectivity+"\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailCompanyFull(companyOrderId);
                }
            });
            loadingDialog.dismiss();
        }
    }

}
