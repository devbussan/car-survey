package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelRequestPersonalSendHq;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426611017 on 2/26/2018.
 */

public class ResponsePersonalSendHq {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelRequestPersonalSendHq modelRequestPersonalSendHq;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelRequestPersonalSendHq getModelRequestPersonalSendHq() {
        return modelRequestPersonalSendHq;
    }

    public void setModelRequestPersonalSendHq(ModelRequestPersonalSendHq modelRequestPersonalSendHq) {
        this.modelRequestPersonalSendHq = modelRequestPersonalSendHq;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
