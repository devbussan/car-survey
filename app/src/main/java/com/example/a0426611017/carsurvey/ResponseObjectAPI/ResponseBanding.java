package com.example.a0426611017.carsurvey.ResponseObjectAPI;

import com.example.a0426611017.carsurvey.Model.ModelBanding;
import com.example.a0426611017.carsurvey.Model.ModelDashboard;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426591017 on 3/21/2018.
 */

public class ResponseBanding {
    @SerializedName("status")
    private String status;

    @SerializedName("result")
    private ModelBanding modelBanding;

    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelBanding getModelBanding() {
        return modelBanding;
    }

    public void setModelBanding(ModelBanding modelBanding) {
        this.modelBanding = modelBanding;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
