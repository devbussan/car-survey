package com.example.a0426611017.carsurvey.Fragment.Company.FullForm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.ModelCompanyInfo;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLCompany;
import com.example.a0426611017.carsurvey.Object.Company.FullForm.ObjectCompanyInfoFull;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailCompanyFull;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentCompanyInfoPending extends Fragment {

    private ObjectCompanyInfoFull customerInfo;
    private View view;
    private Context context;
    private ModelCompanyInfo modelCompanyInfo = new ModelCompanyInfo();
    private ModelDDLCompany ddlCompany;
    private String orderId = "0";

    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;

    private ResponseDetailCompanyFull responseDetailCompanyFull = new ResponseDetailCompanyFull();

    private Gson gson = new Gson();

    public FragmentCompanyInfoPending newInstance(ModelCompanyInfo modelCompanyInfo, ModelDDLCompany ddlCompany, String orderId) {
        FragmentCompanyInfoPending fragmentCompanyInfoPending = new FragmentCompanyInfoPending();
        Bundle args = new Bundle();
        args.putSerializable("modelCompanyInfo", modelCompanyInfo);
        args.putSerializable("ddlCompany", ddlCompany);
        args.putString("orderId", orderId);
        fragmentCompanyInfoPending.setArguments(args);
        return fragmentCompanyInfoPending;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelCompanyInfo = (ModelCompanyInfo) getArguments().getSerializable("modelCompanyInfo");
            ddlCompany = (ModelDDLCompany) getArguments().getSerializable("ddlCompany");
            orderId = getArguments().getString("orderId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        customerInfo = new ObjectCompanyInfoFull(container, inflater, ddlCompany);
        view = customerInfo.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if (modelCompanyInfo.isModified()) {
            customerInfo.setForm(modelCompanyInfo);
        }else{
            getDetailCompanyFull(orderId);
        }

        return view;
    }

    public void setForm(ModelCompanyInfo modelCompanyInfo){
        customerInfo.setForm(modelCompanyInfo);
    }

    public ModelCompanyInfo getForm(){
        return customerInfo.getForm();
    }

    public ModelCompanyInfo getComplete(){
        return customerInfo.getComplete();
    }

    private void getDetailCompanyFull(String companyOrderid) {
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            Call<ResponseDetailCompanyFull> detailcompany = apiInterface.getDetailFullCompany(companyOrderid);
            detailcompany.enqueue(new Callback<ResponseDetailCompanyFull>() {
                @Override
                public void onResponse(Call<ResponseDetailCompanyFull> call, Response<ResponseDetailCompanyFull>
                        response) {
                    if (response.isSuccessful()) {
                        modelCompanyInfo = response.body().getModelCompanyInfo();
                        modelCompanyInfo.setModified(true);
                        setForm(modelCompanyInfo);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {

                        String error = "";
                        try {
                            responseDetailCompanyFull = gson.fromJson(response.errorBody().string(), ResponseDetailCompanyFull.class);
                            error = responseDetailCompanyFull.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder a = new AlertDialog.Builder(context);
                                a.setTitle(getString(R.string.dialog_error_server));
                                a.setCancelable(false);
                                a.setMessage(error+"\n" + getString(R.string.dialog_try_again));
                                a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailCompanyFull(orderId);
                                    }
                                });
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            android.app.AlertDialog.Builder a = new android.app.AlertDialog.Builder(context);
                            a.setTitle(getString(R.string.dialog_error_server));
                            a.setCancelable(false);
                            a.setMessage(error+"\n" +
                                    getString(R.string.dialog_try_again));
                            a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailCompanyFull(orderId);
                                }
                            });
                            a.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder a = new AlertDialog.Builder(context);
                        a.setTitle(getString(R.string.dialog_error_server));
                        a.setCancelable(false);
                        a.setMessage("Service response time out\n" +
                                getString(R.string.dialog_try_again));
                        a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailCompanyFull(orderId);
                            }
                        });
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailCompanyFull> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    android.app.AlertDialog.Builder a = new android.app.AlertDialog.Builder(context);
                    a.setTitle(getString(R.string.dialog_error_server));
                    a.setCancelable(false);
                    a.setMessage(t.getMessage()+"\n" +
                            getString(R.string.dialog_try_again));
                    a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailCompanyFull(orderId);
                        }
                    });
                    a.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            android.app.AlertDialog.Builder b = new android.app.AlertDialog.Builder(context);
            b.setTitle(getString(R.string.dialog_error_server));
            b.setCancelable(false);
            b.setMessage(getString(R.string.dialog_internet_connect)+"\n" +
                            getString(R.string.dialog_try_again));
            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailCompanyFull(orderId);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }

}
