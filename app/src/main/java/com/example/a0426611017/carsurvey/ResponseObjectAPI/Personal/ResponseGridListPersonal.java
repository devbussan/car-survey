package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelObjectGridListPersonal;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426611017 on 2/23/2018.
 */

public class ResponseGridListPersonal {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelObjectGridListPersonal modelObjectGridListPersonal;

    @SerializedName("message")
    String message;

    public String getStatus() { return status;}

    public void setStatus(String status) {this.status = status;}

    public ModelObjectGridListPersonal getModelObjectGridListPersonal() { return modelObjectGridListPersonal;}

    public void setModelObjectGridListPersonal(ModelObjectGridListPersonal modelObjectGridListPersonal) { this.modelObjectGridListPersonal = modelObjectGridListPersonal;}

    public String getMessage() { return message; }

    public void setMessage(String message) { this.message = message; }

}
