package com.example.a0426611017.carsurvey.Model.Personal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 0426611017 on 2/9/2018.
 */

public class ModelDataMainData implements Serializable {
    @SerializedName("CUST_DATA_ID")
    private String cust_data_id;

    @SerializedName("PERSONAL_OTHER_ID")
    private String personal_other_id;

    @SerializedName("SALUTATION")
    private String salutation;

    @SerializedName("MARITALSTAT")
    private String maritalstat;

    @SerializedName("RELIGION")
    private String religion;

    @SerializedName("EDUCATION")
    private String education;

    @SerializedName("NUMDEPENDENTS")
    private String numdependents;

    @SerializedName("NUMOFRESIDENCE")
    private String numofresidence;

    @SerializedName("KKNO")
    private String kkno;

    @SerializedName("KKIMGURL")
    private String kkimgurl;

    private String kkimg;

    @SerializedName("OWNEDVEHICLE")
    private String ownedvehicle;

    @SerializedName("MOBILEPHONE1")
    private String mobilephone1;

    @SerializedName("MOBILEPHONE2")
    private String mobilephone2;

    @SerializedName("USRCRT")
    private String usrcrt;

    @SerializedName("STATUS")
    private String status;

    @SerializedName("PERSONAL_OTHER_INFO")
    private String personal_other_return;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getCust_data_id() {
        return cust_data_id;
    }

    public void setCust_data_id(String cust_data_id) {
        this.cust_data_id = cust_data_id;
    }

    public String getPersonal_other_id() {
        return personal_other_id;
    }

    public void setPersonal_other_id(String personal_other_id) {
        this.personal_other_id = personal_other_id;
    }

    public String getSalutation() {
        return salutation;
    }

    public void setSalutation(String salutation) {
        this.salutation = salutation;
    }

    public String getMaritalstat() {
        return maritalstat;
    }

    public void setMaritalstat(String maritalstat) {
        this.maritalstat = maritalstat;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getNumdependents() {
        return numdependents;
    }

    public void setNumdependents(String numdependents) {
        this.numdependents = numdependents;
    }

    public String getNumofresidence() {
        return numofresidence;
    }

    public void setNumofresidence(String numofresidence) {
        this.numofresidence = numofresidence;
    }

    public String getKkno() {
        return kkno;
    }

    public void setKkno(String kkno) {
        this.kkno = kkno;
    }

    public String getKkimgurl() {
        return kkimgurl;
    }

    public void setKkimgurl(String kkimgurl) {
        this.kkimgurl = kkimgurl;
    }

    public String getKkimg() {
        return kkimg;
    }

    public void setKkimg(String kkimg) {
        this.kkimg = kkimg;
    }

    public String getOwnedvehicle() {
        return ownedvehicle;
    }

    public void setOwnedvehicle(String ownedvehicle) {
        this.ownedvehicle = ownedvehicle;
    }

    public String getMobilephone1() {
        return mobilephone1;
    }

    public void setMobilephone1(String mobilephone1) {
        this.mobilephone1 = mobilephone1;
    }

    public String getMobilephone2() {
        return mobilephone2;
    }

    public void setMobilephone2(String mobilephone2) {
        this.mobilephone2 = mobilephone2;
    }

    public String getUsrcrt() {
        return usrcrt;
    }

    public void setUsrcrt(String usrcrt) {
        this.usrcrt = usrcrt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getPersonal_other_return() {
        return personal_other_return;
    }

    public void setPersonal_other_return(String personal_other_return) {
        this.personal_other_return = personal_other_return;
    }
}
