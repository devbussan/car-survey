package com.example.a0426611017.carsurvey.Object.Company.MiniForm;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;

/**
 * Created by c201017001 on 12/02/2018.
 */

public class ObjectContactInfo4 {

    private View view;
    private ViewGroup container;
    private LayoutInflater inflater;
    private Context context;

    private EditText editTextName, editTextJobPosition, editTextIdNum, editTextIdNpwp,
            editTextMobilePhone1, editTextMobilePhone2, editTextPhone1, editTextPhone2,
            editTextEmail1, editTextEmail2;

    private TextView textViewIdImage, textViewIdNpwpImage,
            textViewIdImageUrl, textViewIdNpwpImageUrl;

    private Button buttonIdImage, buttonIdNpwpIamge;

    public ObjectContactInfo4(ViewGroup container, LayoutInflater inflater) {
        this.container = container;
        this.inflater = inflater;
        this.context = this.container.getContext();
        this.view = this.inflater.inflate(R.layout.fragment_contact_info4_new_order, container, false);
        setField();
    }

    public View getView() {
        return view;
    }

    public TextView getTextViewIdImage() {
        return textViewIdImage;
    }

    public TextView getTextViewIdNpwpImage() {
        return textViewIdNpwpImage;
    }

    public TextView getTextViewIdImageUrl() {
        return textViewIdImageUrl;
    }

    public TextView getTextViewIdNpwpImageUrl() {
        return textViewIdNpwpImageUrl;
    }

    public Button getButtonIdImage() {
        return buttonIdImage;
    }

    public Button getButtonIdNpwpIamge() {
        return buttonIdNpwpIamge;
    }

    public void setTextViewIdImage(String fileNameIdImage) {
        this.textViewIdImage.setText(fileNameIdImage);
    }

    public void setTextViewIdNpwpImage(String fileNameIdNpwpImage) {
        this.textViewIdNpwpImage.setText(fileNameIdNpwpImage);
    }

    public void setTextViewIdImageUrl(String urlIdImageUrl) {
        this.textViewIdImageUrl.setText(urlIdImageUrl);
    }

    public void setTextViewIdNpwpImageUrl(String urlIdNpwpImageUrl) {
        this.textViewIdNpwpImageUrl.setText(urlIdNpwpImageUrl);
    }

    private void setField(){
        editTextName = view.findViewById(R.id.edit_text_name_add_new_contact_info4);
        editTextJobPosition = view.findViewById(R.id.edit_text_job_position_add_new_contact_info4);
        editTextIdNum = view.findViewById(R.id.edit_text_id_num_add_new_contact_info4);
        editTextIdNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() < 16){
                    editTextIdNum.setError("Minimum and Maximum 16 digits");
                }
            }
        });

        editTextIdNpwp = view.findViewById(R.id.edit_text_id_npwp_add_new_contact_info4);
        editTextIdNpwp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 15) {
                    editTextIdNpwp.setError("Minimum and Maximum 15 digits");
                }
            }
        });

        editTextMobilePhone1 = view.findViewById(R.id.edit_text_mobile_phone1_add_new_contact_info4);
        editTextMobilePhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() < 7){
                    editTextMobilePhone1.setError("Minimum 7 digits");
                }
            }
        });
        editTextMobilePhone2 = view.findViewById(R.id.edit_text_mobile_phone2_add_new_contact_info4);
        editTextPhone1 = view.findViewById(R.id.edit_text_phone1_add_new_contact_info4);
        editTextPhone1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 7) {
                    editTextPhone1.setError("Minimum 7 digits");
                }
            }
        });

        editTextPhone2 = view.findViewById(R.id.edit_text_phone2_add_new_contact_info4);
        editTextEmail1 = view.findViewById(R.id.edit_text_email1_add_new_contact_info4);
        editTextEmail1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!Constans.VALID_EMAIL_ADDRESS.matcher(s.toString()).find()){
                    editTextEmail1.setError("Is not Email Pattern");
                }
            }
        });
        editTextEmail2 = view.findViewById(R.id.edit_text_email2_add_new_contact_info4);
        editTextEmail2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() > 0 && !Constans.VALID_EMAIL_ADDRESS.matcher(s.toString()).find()){
                    editTextEmail2.setError("Is not Email Pattern");
                }
            }
        });

        textViewIdImage = view.findViewById(R.id.text_view_id_image_add_new_contact_info4);
        textViewIdImage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    textViewIdImage.setError(null);
                }
            }
        });
        textViewIdNpwpImage = view.findViewById(R.id.text_view_id_npwp_image_add_new_contact_info4);
        textViewIdNpwpImage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    textViewIdNpwpImage.setError(null);
                }
            }
        });

        textViewIdImageUrl = view.findViewById(R.id.text_view_id_image_url_add_new_contact_info4);
        textViewIdNpwpImageUrl = view.findViewById(R.id.text_view_id_npwp_image_url_add_new_contact_info4);

        buttonIdImage = view.findViewById(R.id.button_camera_id_image_add_new_contact_info4);
        buttonIdNpwpIamge = view.findViewById(R.id.button_camera_id_npwp_image_add_new_contact_info4);
    }

    public void setForm(ModelDetailContactInfo contactInfo){
        editTextName.setText(contactInfo.getContactName());
        editTextJobPosition.setText(contactInfo.getJobPosition());
        editTextIdNum.setText(contactInfo.getIdNumber());
        editTextIdNpwp.setText(contactInfo.getNpwp());
        editTextMobilePhone1.setText(contactInfo.getMobilePhone1());
        editTextMobilePhone2.setText(contactInfo.getMobilePhone2());
        editTextPhone1.setText(contactInfo.getPhone1());
        editTextPhone2.setText(contactInfo.getPhone2());
        editTextEmail1.setText(contactInfo.getEmail1());
        editTextEmail2.setText(contactInfo.getEmail2());

        textViewIdImageUrl.setText(contactInfo.getIdImageUrl());
        textViewIdNpwpImageUrl.setText(contactInfo.getNpwpImageUrl());

        Log.d("Url image", textViewIdImageUrl.getText().toString());
        PreviewImage previewImage = new PreviewImage();
        String nameIdImage = "";
        if (contactInfo.getIdImageUrl().equals("") || contactInfo.getIdImageUrl() == null) {
            nameIdImage = "No selected file";
        } else {

            if (contactInfo.getIdImageUrl().startsWith("http")) {
                nameIdImage = contactInfo.getIdImageUrl().split("/")[6];
                previewImage.setPreviewImage(contactInfo.getIdImageUrl(), context, textViewIdImage);
            } else {
                nameIdImage = contactInfo.getIdImage();
                previewImage.setPreviewImage(nameIdImage, context, textViewIdImage);
            }


        }
        textViewIdImage.setText(nameIdImage);


        String nameIdNpwpImage = "";
        if (contactInfo.getNpwpImageUrl().equals("") || contactInfo.getNpwpImageUrl() == null) {
            nameIdNpwpImage = "No selected file";
        } else {
            if (contactInfo.getNpwpImageUrl().startsWith("http")) {
                nameIdNpwpImage = contactInfo.getNpwpImageUrl().split("/")[6];
                previewImage.setPreviewImage(contactInfo.getNpwpImageUrl(), context, textViewIdNpwpImage);
            } else {
                nameIdNpwpImage = contactInfo.getNpwpImage();
                previewImage.setPreviewImage(nameIdNpwpImage, context, textViewIdNpwpImage);
            }
        }
        textViewIdNpwpImage.setText(nameIdNpwpImage);
    }

    public ModelDetailContactInfo getForm(){
        ModelDetailContactInfo contactInfo = new ModelDetailContactInfo();

        contactInfo.setContactName(editTextName.getText().toString());
        contactInfo.setJobPosition(editTextJobPosition.getText().toString());
        contactInfo.setIdNumber(editTextIdNum.getText().toString());
        contactInfo.setNpwp(editTextIdNpwp.getText().toString());
        contactInfo.setMobilePhone1(editTextMobilePhone1.getText().toString());
        contactInfo.setMobilePhone2(editTextMobilePhone2.getText().toString());
        contactInfo.setPhone1(editTextPhone1.getText().toString());
        contactInfo.setPhone2(editTextPhone2.getText().toString());
        contactInfo.setEmail1(editTextEmail1.getText().toString());
        contactInfo.setEmail2(editTextEmail2.getText().toString());
        contactInfo.setIdImage(textViewIdImage.getText().toString());
        contactInfo.setNpwpImage(textViewIdNpwpImage.getText().toString());
        contactInfo.setIdImageUrl(textViewIdImageUrl.getText().toString());
        contactInfo.setNpwpImageUrl(textViewIdNpwpImageUrl.getText().toString());

        contactInfo.setModified(true);

        return contactInfo;
    }

    public ModelDetailContactInfo getComplete(){
        ModelDetailContactInfo contactInfo = getForm();

        contactInfo.setModified(true);
        contactInfo.setComplete(true);

        if(contactInfo.getContactName().length() == 0){
            editTextName.setError("Required");
            contactInfo.setComplete(false);
        }else if(contactInfo.getContactName().trim().length() == 0){
            editTextName.setError("Whitespaces only detected");
            contactInfo.setComplete(false);
        }

        if(contactInfo.getJobPosition().length() == 0){
            editTextJobPosition.setError("Required");
            contactInfo.setComplete(false);
        }else if(contactInfo.getJobPosition().trim().length() == 0){
            editTextJobPosition.setError("Whitespaces only detected");
            contactInfo.setComplete(false);
        }

        if(contactInfo.getIdNumber().length() == 0){
            editTextIdNum.setError("Required");
            contactInfo.setComplete(false);
        }else if(contactInfo.getIdNumber().trim().length() == 0){
            editTextIdNum.setError("Whitespaces only detected");
            contactInfo.setComplete(false);
        }else if(contactInfo.getIdNumber().trim().length() != 16){
            editTextIdNum.setError("Minimum and Maximum 16 digits");
            contactInfo.setComplete(false);
        }

        if(contactInfo.getNpwp().length() == 0){
            editTextIdNpwp.setError("Required");
            contactInfo.setComplete(false);
        }else if(contactInfo.getNpwp().trim().length() == 0){
            editTextIdNpwp.setError("Whitespaces only detected");
            contactInfo.setComplete(false);
        }else if(contactInfo.getNpwp().trim().length() != 15){
            editTextIdNpwp.setError("Minimum and Maximum 15 digits");
            contactInfo.setComplete(false);
        }

        if(contactInfo.getMobilePhone1().length() == 0){
            editTextMobilePhone1.setError("Required");
            contactInfo.setComplete(false);
        }else if(contactInfo.getMobilePhone1().trim().length() == 0){
            editTextMobilePhone1.setError("Whitespaces only detected");
            contactInfo.setComplete(false);
        }else if(contactInfo.getMobilePhone1().trim().length() < 7){
            editTextMobilePhone1.setError("Minimum 7 digits");
            contactInfo.setComplete(false);
        }

        if (contactInfo.getPhone1().trim().length() == 0){
            editTextPhone1.setError("Required");
            contactInfo.setComplete(false);
        }else if(contactInfo.getPhone1().trim().length() == 0){
            editTextPhone1.setError("Whitespaces only detected");
            contactInfo.setComplete(false);
        }else if(contactInfo.getPhone1().trim().length() < 7){
            editTextPhone1.setError("Minimum 7 digits");
            contactInfo.setComplete(false);
        }

        if(contactInfo.getEmail1().length() == 0){
            editTextEmail1.setError("Required");
            contactInfo.setComplete(false);
        }else if(contactInfo.getEmail1().trim().length() == 0){
            editTextEmail1.setError("Whitespaces only detected");
            contactInfo.setComplete(false);
        }else if(!Constans.VALID_EMAIL_ADDRESS.matcher(contactInfo.getEmail1()).find()){
            editTextEmail1.setError("Is not Email Pattern");
            contactInfo.setComplete(false);
        }

        if(contactInfo.getIdImage().length() == 0){
            textViewIdImage.setError("Required");
            contactInfo.setComplete(false);
        }else if(contactInfo.getIdImage().equals("No selected file")){
            textViewIdImage.setError("Required");
            contactInfo.setComplete(false);
        }

        if(contactInfo.getNpwpImage().length() == 0){
            textViewIdNpwpImage.setError("Required");
            contactInfo.setComplete(false);
        }else if(contactInfo.getNpwpImage().equals("No selected file")){
            textViewIdNpwpImage.setError("Required");
            contactInfo.setComplete(false);
        }

        return contactInfo;
    }

    public void setDisabled(){
        editTextName.setEnabled(false);
        editTextJobPosition.setEnabled(false);
        editTextIdNum.setEnabled(false);

        editTextIdNpwp.setEnabled(false);

        editTextMobilePhone1.setEnabled(false);
        editTextMobilePhone2.setEnabled(false);
        editTextPhone1.setEnabled(false);

        editTextPhone2.setEnabled(false);
        editTextEmail1.setEnabled(false);
        editTextEmail2.setEnabled(false);

        textViewIdImage.setEnabled(false);
        textViewIdNpwpImage.setEnabled(false);

        textViewIdImageUrl.setEnabled(false);
        textViewIdNpwpImageUrl.setEnabled(false);

        buttonIdImage.setEnabled(false);
        buttonIdNpwpIamge.setEnabled(false);
    }
}
