package com.example.a0426611017.carsurvey.ResponseObjectAPI.Company;

import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelMiniFormCompany;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 09/02/2018.
 */

public class ResponseMiniFormCompany {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelMiniFormCompany miniFormCompanyResult;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelMiniFormCompany getMiniFormCompanyResult() {
        return miniFormCompanyResult;
    }

    public void setMiniFormCompanyResult(ModelMiniFormCompany miniFormCompanyResult) {
        this.miniFormCompanyResult = miniFormCompanyResult;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
