package com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey;

import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLSurvey;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 08/02/2018.
 */

public class ResponseDDLSurvey {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDDLSurvey ddlSurvey;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDDLSurvey getDdlSurvey() {
        return ddlSurvey;
    }

    public void setDdlSurvey(ModelDDLSurvey ddlSurvey) {
        this.ddlSurvey = ddlSurvey;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
