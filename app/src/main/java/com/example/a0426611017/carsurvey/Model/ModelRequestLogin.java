package com.example.a0426611017.carsurvey.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 28/02/2018.
 */

public class ModelRequestLogin {
    @SerializedName("ADMIN_USERNAME")
    private String adminUsername;

    @SerializedName("ADMIN_PASSWORD")
    private String adminPassword;

    @SerializedName("IMEI")
    private String adminImei;

    public String getAdminImei() {
        return adminImei;
    }

    public void setAdminImei(String adminImei) {
        this.adminImei = adminImei;
    }

    public String getAdminUsername() {
        return adminUsername;
    }

    public void setAdminUsername(String adminUsername) {
        this.adminUsername = adminUsername;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public void setAdminPassword(String adminPassword) {
        this.adminPassword = adminPassword;
    }
}
