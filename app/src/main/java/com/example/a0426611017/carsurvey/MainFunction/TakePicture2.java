package com.example.a0426611017.carsurvey.MainFunction;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.view.SurfaceView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by c201017001 on 10/01/2018.
 */

public class TakePicture2 {

    private FileFunction fileFunction = new FileFunction();
    private String nameFile;
    private String filePath;
    private Context context;

    public String getNameFile() {
        return nameFile;
    }

    public String getFilePath() {
        return filePath;
    }

    public Intent getPickImageIntent(Context context) {
        Intent chooserIntent = null;

        List<Intent> intentList = new ArrayList<>();

        Intent pickIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePhotoIntent.putExtra("return-data", true);
        takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(fileFunction.createFilePictureBAF()));
        intentList = addIntentsToList(context, intentList, pickIntent);
        intentList = addIntentsToList(context, intentList, takePhotoIntent);

        if (intentList.size() > 0) {
            chooserIntent = Intent.createChooser(intentList.remove(intentList.size() - 1),
                    "Select Chooser");
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentList.toArray(new Parcelable[]{}));
        }

        nameFile = fileFunction.getNameFile();
        filePath = fileFunction.getFilePath();

        return chooserIntent;
    }

    private List<Intent> addIntentsToList(Context context, List<Intent> list, Intent intent) {
        List<ResolveInfo> resInfo = context.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo resolveInfo : resInfo) {
            String packageName = resolveInfo.activityInfo.packageName;
            Intent targetedIntent = new Intent(intent);
            targetedIntent.setPackage(packageName);
            list.add(targetedIntent);
        }
        return list;
    }

    public void getPickImage(Context context) throws IOException {
        fileFunction.createFilePictureBAF();
        Camera camera = Camera.open(0);
        Camera.Parameters parameters = camera.getParameters();
        parameters.setPictureFormat(ImageFormat.JPEG);
        camera.setParameters(parameters);
        SurfaceView mview = new SurfaceView(context);
        camera.setPreviewDisplay(mview.getHolder());
        camera.setPreviewDisplay(null);
        camera.startPreview();
        camera.takePicture(null,null,photoCallback);
        camera.stopPreview();
    }

    private Camera.PictureCallback photoCallback=new Camera.PictureCallback() {
        public void onPictureTaken(byte[] data, Camera camera) {

            try {
                File file = fileFunction.createFilePictureBAF();
                FileOutputStream out = new FileOutputStream(file);
                out.write(data);
                out.flush();
                out.close();
            } catch (Exception e)
            {
                e.printStackTrace();
            }

        }
    };

}
