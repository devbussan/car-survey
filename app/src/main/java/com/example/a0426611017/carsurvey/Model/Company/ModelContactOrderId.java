package com.example.a0426611017.carsurvey.Model.Company;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by c201017001 on 23/02/2018.
 */

public class ModelContactOrderId {

    @SerializedName("CONTACT_INFO")
    private List<String> contactInfo;

    public List<String> getContactInfo() {
        return contactInfo;
    }

    public void setContactInfo(List<String> contactInfo) {
        this.contactInfo = contactInfo;
    }
}
