package com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataJobData;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by 0426611017 on 2/12/2018.
 */

public class ObjectDataJobDataComplete {
    private ViewGroup container;
    private View view;
    private Context context;

    private TextView
            profesionName, jobPosition, jobStatus, namaPerusahaan,
            industryTypeName, employmentEstablishment, jobAddress,
            jobRt, jobRw, jobZipCode, jobKelurahan, jobKecamatan,
            jobCity, jobPhone1;

    public ObjectDataJobDataComplete(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_data_job_data_complete, viewGroup, false);

        setField();
    }

    public View getView() {
        return view;
    }

    private void setField() {
        profesionName = view.findViewById(R.id.value_profession_name);
        jobPosition = view.findViewById(R.id.value_job_position);
        jobStatus = view.findViewById(R.id.value_job_status);
        namaPerusahaan = view.findViewById(R.id.value_nama_perusahaan);
        industryTypeName = view.findViewById(R.id.value_industry_type_name);
        employmentEstablishment = view.findViewById(R.id.value_employment_establishment_date);
        jobAddress = view.findViewById(R.id.value_job_address);
        jobRt = view.findViewById(R.id.value_job_rt);
        jobRw = view.findViewById(R.id.value_job_rw);
        jobZipCode = view.findViewById(R.id.value_job_zip_code);
        jobKelurahan = view.findViewById(R.id.value_job_kelurahan);
        jobKecamatan = view.findViewById(R.id.value_job_kecamatan);
        jobCity = view.findViewById(R.id.value_job_city);
        jobPhone1 = view.findViewById(R.id.value_job_phone_1);
    }

    public void setForm(final ModelDataJobData modelDataJobData) {
        profesionName.setText(modelDataJobData.getProffesionname());
        jobPosition.setText(modelDataJobData.getJobpos());
        jobStatus.setText(modelDataJobData.getJobstat());
        namaPerusahaan.setText(modelDataJobData.getCompanyname());
        industryTypeName.setText(modelDataJobData.getIndustrytype());
        if (modelDataJobData.getEstablishmentdtm() != null && !modelDataJobData.getEstablishmentdtm().equals("")) {
            modelDataJobData.setEstablishmentdtm(
                    modelDataJobData.getEstablishmentdtm().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelDataJobData.getEstablishmentdtm().substring(4, 6))]
                            + " " + modelDataJobData.getEstablishmentdtm().substring(0, 4)
            );
        }
        employmentEstablishment.setText(modelDataJobData.getEstablishmentdtm());
        jobAddress.setText(modelDataJobData.getJobaddr());
        jobRt.setText(modelDataJobData.getJobrt());
        jobRw.setText(modelDataJobData.getJobrw());
        jobZipCode.setText(modelDataJobData.getJobzipode());
        jobKelurahan.setText(modelDataJobData.getJobkelurahan());
        jobKecamatan.setText(modelDataJobData.getJobkecamatan());
        jobCity.setText(modelDataJobData.getJobcity());
        jobPhone1.setText(modelDataJobData.getJobphone());
    }
}

