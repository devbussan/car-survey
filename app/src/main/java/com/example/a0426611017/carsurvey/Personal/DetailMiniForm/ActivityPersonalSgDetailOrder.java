package com.example.a0426611017.carsurvey.Personal.DetailMiniForm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.a0426611017.carsurvey.Fragment.Personal.DetailMiniForm.FragmentDataPersonalSGDetail;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.R;

public class ActivityPersonalSgDetailOrder extends AppCompatActivity implements View.OnClickListener{

    private Button buttonPersonalSg;
    private Button buttonExit;
    private Button buttonBack;

    private String personalOrderId;

    private ProgressDialog progressDialog;

    private SharedPreferences sharedpreferencesMenuCategory;
    private int menuCategory = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_personal_sg_detail_order);
        window.setStatusBarColor(getResources().getColor(R.color.color_personal));

        progressDialog = new ProgressDialog(ActivityPersonalSgDetailOrder.this);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);

        personalOrderId = getIntent().getStringExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID);
        Log.d("Personal Order", personalOrderId);

        buttonPersonalSg = findViewById(R.id.button_personal_sg_detail);
        buttonPersonalSg.setOnClickListener(this);

        sharedpreferencesMenuCategory = this.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, Context.MODE_PRIVATE);
        menuCategory = sharedpreferencesMenuCategory.getInt(Constans.KEY_MENU_CATEGORY, 0);

        buttonExit = findViewById(R.id.button_exit_personal_sg_detail);
        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityPersonalSgDetailOrder.this, MainActivityDrawer.class);
                intent.putExtra(Constans.KEY_MENU, menuCategory);
                intent.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                startActivity(intent);
            }
        });

        buttonBack = findViewById(R.id.button_back_personal_sg_detail);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityPersonalSgDetailOrder.super.onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_personal_sg_detail :
                buttonPersonalSg.setEnabled(false);
                buttonPersonalSg.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonPersonalSg.setTextColor(getResources().getColor(R.color.color_personal));

                loadFragment(FragmentDataPersonalSGDetail.newInstance(personalOrderId));
                break;
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frame_personal_sg_order_detail, fragment);
        fragmentTransaction.commit();
    }


}
