package com.example.a0426611017.carsurvey.Model.Company;

import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;

/**
 * Created by 0414831216 on 2/19/2018.
 */

public class ModelRequestContactInfoCompany {
    @SerializedName("COMPANYORDERID")
    @Expose
    String companyOrderId;

    @SerializedName("CNTNAME")
    @Expose
    String cntName;

    @SerializedName("CNTJOBPOS")
    @Expose
    String cntJobPos;

    @SerializedName("CNTIDNUM")
    @Expose
    String cntIdnum;

    @SerializedName("CNTIDURL")
    @Expose
    String cntIdUrl;

    @SerializedName("CNTNPWPURL")
    @Expose
    String cntNpwpUrl;

    @SerializedName("CNTNPWP")
    @Expose
    String cntNpwp;

    @SerializedName("CNTMBLPHN1")
    @Expose
    String cntMblHn1;

    @SerializedName("CNTMBLPHN2")
    @Expose
    String cntMblHn2;

    @SerializedName("CNTPHN1")
    @Expose
    String cntPhn1;

    @SerializedName("CNTPHN2")
    @Expose
    String cntPhn2;

    @SerializedName("CNTEMAIL1")
    @Expose
    String cntEmail1;

    @SerializedName("CNTEMAIL2")
    @Expose
    String cntEmail2;

    @SerializedName("CNTINFOFLAG")
    @Expose
    String cntInfoFlag;

    @SerializedName("USRCRT")
    @Expose
    String usrCrt;

    private boolean isModified = false;


    public ModelRequestContactInfoCompany(ModelDetailContactInfo modelContactInfoMini) throws IOException {
        FileFunction fileFunction = new FileFunction();
        this.cntName = modelContactInfoMini.getContactName();
        this.cntJobPos = modelContactInfoMini.getJobPosition();
        this.cntIdnum = modelContactInfoMini.getIdNumber();
        if (modelContactInfoMini.getIdImageUrl().startsWith("http")) {
            this.cntIdUrl = modelContactInfoMini.getIdImageUrl();
        } else if (!modelContactInfoMini.getIdImage().equals("No selected file")) {
            this.cntIdUrl = fileFunction.createBase64(modelContactInfoMini.getIdImage());

        }

        if (modelContactInfoMini.getNpwpImageUrl().startsWith("http")) {
            this.cntNpwpUrl = modelContactInfoMini.getNpwpImageUrl();
        } else if (!modelContactInfoMini.getNpwpImage().equals("No selected file")) {
            this.cntNpwpUrl = fileFunction.createBase64(modelContactInfoMini.getNpwpImage());

        }

        this.cntNpwp = modelContactInfoMini.getNpwp();
        this.cntMblHn1 = modelContactInfoMini.getMobilePhone1();
        this.cntMblHn2 = modelContactInfoMini.getMobilePhone2();
        this.cntPhn1 = modelContactInfoMini.getPhone1();
        this.cntPhn2 = modelContactInfoMini.getPhone2();
        this.cntEmail1 = modelContactInfoMini.getEmail1();
        this.cntEmail2 = modelContactInfoMini.getEmail2();
        this.cntInfoFlag = modelContactInfoMini.getInfoFlag();

        this.isModified = modelContactInfoMini.isModified();
        //this.cntInfoFlag = modelContactInfoMini.;
        //this.usrCrt;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public String getCompanyOrderId() {
        return companyOrderId;
    }

    public void setCompanyOrderId(String companyOrderId) {
        this.companyOrderId = companyOrderId;
    }

    public String getCntName() {
        return cntName;
    }

    public void setCntName(String cntName) {
        this.cntName = cntName;
    }

    public String getCntJobPos() {
        return cntJobPos;
    }

    public void setCntJobPos(String cntJobPos) {
        this.cntJobPos = cntJobPos;
    }

    public String getCntIdnum() {
        return cntIdnum;
    }

    public void setCntIdnum(String cntIdnum) {
        this.cntIdnum = cntIdnum;
    }

    public String getCntIdUrl() {
        return cntIdUrl;
    }

    public void setCntIdUrl(String cntIdUrl) {
        this.cntIdUrl = cntIdUrl;
    }

    public String getCntNpwpUrl() {
        return cntNpwpUrl;
    }

    public void setCntNpwpUrl(String cntNpwpUrl) {
        this.cntNpwpUrl = cntNpwpUrl;
    }

    public String getCntNpwp() {
        return cntNpwp;
    }

    public void setCntNpwp(String cntNpwp) {
        this.cntNpwp = cntNpwp;
    }

    public String getCntMblHn1() {
        return cntMblHn1;
    }

    public void setCntMblHn1(String cntMblHn1) {
        this.cntMblHn1 = cntMblHn1;
    }

    public String getCntMblHn2() {
        return cntMblHn2;
    }

    public void setCntMblHn2(String cntMblHn2) {
        this.cntMblHn2 = cntMblHn2;
    }

    public String getCntPhn1() {
        return cntPhn1;
    }

    public void setCntPhn1(String cntPhn1) {
        this.cntPhn1 = cntPhn1;
    }

    public String getCntPhn2() {
        return cntPhn2;
    }

    public void setCntPhn2(String cntPhn2) {
        this.cntPhn2 = cntPhn2;
    }

    public String getCntEmail1() {
        return cntEmail1;
    }

    public void setCntEmail1(String cntEmail1) {
        this.cntEmail1 = cntEmail1;
    }

    public String getCntEmail2() {
        return cntEmail2;
    }

    public void setCntEmail2(String cntEmail2) {
        this.cntEmail2 = cntEmail2;
    }

    public String getCntInfoFlag() {
        return cntInfoFlag;
    }

    public void setCntInfoFlag(String cntInfoFlag) {
        this.cntInfoFlag = cntInfoFlag;
    }

    public String getUsrCrt() {
        return usrCrt;
    }

    public void setUsrCrt(String usrCrt) {
        this.usrCrt = usrCrt;
    }


}

