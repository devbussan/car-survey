package com.example.a0426611017.carsurvey.Model.Financial;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by 0414831216 on 2/13/2018.
 */

public class ModelDDLFinance implements Serializable {
    @SerializedName("PAYMENT_FREQ")
    private List<String> payment_freq;

    @SerializedName("ASURANSI")
    private List<String> asurnsi;

    @SerializedName("PEMBAYARAN")
    private List<String> pembayaran;

    @SerializedName("CUSTOMER_NOTIFICATION")
    private List<String> customer_notofikasi;

    @SerializedName("SURVEY_ENV")
    private List<String> surveyEnv;

    public List<String> getPayment_freq() {
        return payment_freq;
    }

    public void setPayment_freq(List<String> payment_freq) {
        this.payment_freq = payment_freq;
    }

    public List<String> getAsurnsi() {
        return asurnsi;
    }

    public void setAsurnsi(List<String> asurnsi) {
        this.asurnsi = asurnsi;
    }

    public List<String> getPembayaran() {
        return pembayaran;
    }

    public void setPembayaran(List<String> pembayaran) {
        this.pembayaran = pembayaran;
    }

    public List<String> getCustomer_notofikasi() {
        return customer_notofikasi;
    }

    public void setCustomer_notofikasi(List<String> customer_notofikasi) {
        this.customer_notofikasi = customer_notofikasi;
    }

    public List<String> getSurveyEnv() {
        return surveyEnv;
    }

    public void setSurveyEnv(List<String> surveyEnv) {
        this.surveyEnv = surveyEnv;
    }
}
