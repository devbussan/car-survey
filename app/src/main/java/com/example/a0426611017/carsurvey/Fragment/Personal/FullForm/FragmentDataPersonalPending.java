package com.example.a0426611017.carsurvey.Fragment.Personal.FullForm;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonal;
import com.example.a0426611017.carsurvey.Object.Personal.FullForm.ObjectDataPersonalPending;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalInfo;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class FragmentDataPersonalPending extends Fragment {
    private View view;
    private Context context;
    private int cameraIdxButton = 0;
    private String nameFile;
    private TakePicture takePicture = new TakePicture();
    private PreviewImage previewImage = new PreviewImage();
    private TextView fileNameIdImage, fileNamePersonalNpwp,
            fileNameIdImageUrl, fileNamePersonalNpwpUrl;

    private ObjectDataPersonalPending objectDataPersonalPending;
    private ModelDataPersonal modelDataPersonal = new ModelDataPersonal();
    private ModelDDLPersonal ddlPersonal = new ModelDDLPersonal();
    private FileFunction fileFunction = new FileFunction();


    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private String personalOrderId = null;
    private String name = null;
    private Gson gson = new Gson();
    private ResponseDetailPersonalInfo responseDetailPersonalInfo = new ResponseDetailPersonalInfo();

    public FragmentDataPersonalPending newInstance(ModelDataPersonal modelDataPersonal, ModelDDLPersonal ddlPersonal, String orderId) {
        FragmentDataPersonalPending fragmentDataPersonalPending = new FragmentDataPersonalPending();
        Bundle args = new Bundle();
        args.putSerializable("modelInfoPersonal", modelDataPersonal);
        args.putSerializable("ddlPersonal", ddlPersonal);
        args.putString("personalOrderId", orderId);
        fragmentDataPersonalPending.setArguments(args);
        return fragmentDataPersonalPending;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelDataPersonal = (ModelDataPersonal) getArguments().getSerializable("modelInfoPersonal");
            ddlPersonal = (ModelDDLPersonal) getArguments().getSerializable("ddlPersonal");
            personalOrderId = getArguments().getString("personalOrderId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        objectDataPersonalPending = new ObjectDataPersonalPending(container, inflater, ddlPersonal);
        view = objectDataPersonalPending.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        name = "personal";
        if(modelDataPersonal.isModified()){
            objectDataPersonalPending.setForm(modelDataPersonal);
        }else{
            getDetailInfo(name, personalOrderId);
        }

        fileNameIdImage = objectDataPersonalPending.getNamePersonalIdImage();
        fileNameIdImageUrl = objectDataPersonalPending.getNamePersonalIdImageUrl();
        Button buttonIdImage = objectDataPersonalPending.getButtonIdImage();

        fileNamePersonalNpwp = objectDataPersonalPending.getNamePersonalNpwpImage();
        fileNamePersonalNpwpUrl = objectDataPersonalPending.getNamePersonalNpwpImageUrl();
        Button buttonPersonalNpwp = objectDataPersonalPending.getButtonPersonalNpwp();

        callCamera(buttonIdImage,1);
        callCamera(buttonPersonalNpwp, 2);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            objectDataPersonalPending.setEnabledButtonImage();
        }

        return view;
    }

    private void callCamera(Button button, int idx) {
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(v, idxButton);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                objectDataPersonalPending.setEnabledButtonImage();
            }
        }
    }

    public void takePicture(View view, int idx) {
        this.cameraIdxButton = idx;
        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                switch (cameraIdxButton) {
                    case 1:
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectDataPersonalPending.setFilePersonalIdImage(nameFile);
                        objectDataPersonalPending.setNamePersonalIdImageUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNameIdImage);
                        break;
                    case 2:
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectDataPersonalPending.setFilePersonalNpwp(nameFile);
                        objectDataPersonalPending.setNamePersonalNpwpImageUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNamePersonalNpwp);
                        break;

                    default:
                        break;
                }
            }
        }
    }

    public ModelDataPersonal getForm(){
        return objectDataPersonalPending.getForm();
    }

    public void setForm(ModelDataPersonal modelDataPersonal){
        objectDataPersonalPending.setForm(modelDataPersonal);
    }

    public ModelDataPersonal getComplete(){
        return objectDataPersonalPending.getComplete();
    }

    private void getDetailInfo(final String name, final String idOrder){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalInfo> infoDetail = apiInterface.getDetailPersonalInfo(name, idOrder);
            infoDetail.enqueue(new Callback<ResponseDetailPersonalInfo>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalInfo> call, Response<ResponseDetailPersonalInfo>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataPersonal = response.body().getModelDataPersonal();
                        Log.d("Result", gson.toJson(modelDataPersonal));

                        setForm(modelDataPersonal);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalInfo = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalInfo.class);
                            error = responseDetailPersonalInfo.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailInfo(name, idOrder);
                                    }
                                });
                                b.show();
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.fragment_data_personal_pending), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailInfo(name, idOrder);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        Log.e("Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailInfo(name, idOrder);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalInfo> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailInfo(name, idOrder);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailInfo(name, idOrder);
                }
            });
            b.show();
            Log.e("Error", String.valueOf(R.string.no_connectivity));
            loadingDialog.dismiss();
        }
    }
}