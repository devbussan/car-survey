package com.example.a0426611017.carsurvey.Object.Personal.MiniForm;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalSGMini;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;

import java.util.List;

/**
 * Created by 0426591017 on 2/19/2018.
 */

public class ObjectDataPersonalSg {
    private View view;
    private Context context;
    private ViewGroup container;
    private ModelDDLPersonal ddlPersonal;

    private EditText customerNameSg, idNumberSg, familyCardNumSg;

    private TextView textViewIdImgSg, textViewFamilyCardImgSg,
                    textViewIdImgSgUrl, textViewFamilyCardImgSgUrl;

    private Button buttonIdImageSg, buttonPersonalFamilyCardSg;

    private Spinner IdTypeSg;

    public ObjectDataPersonalSg(ViewGroup viewGroup, LayoutInflater inflater, ModelDDLPersonal ddlPersonal) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.ddlPersonal = ddlPersonal;
        this.view = inflater.inflate(R.layout.fragment_data_personal_sgorder, container, false);

        setField();
    }

    public View getView() {
        return view;
    }

    public TextView getTextViewIdImgSg() {
        return textViewIdImgSg;
    }

    public TextView getTextViewFamilyCardImgSg() {
        return textViewFamilyCardImgSg;
    }

    public TextView getTextViewIdImgSgUrl() {
        return textViewIdImgSgUrl;
    }

    public TextView getTextViewFamilyCardImgSgUrl() {
        return textViewFamilyCardImgSgUrl;
    }

    public void setTextViewIdImgSg(String textViewIdImgSg){
        this.textViewIdImgSg.setText(textViewIdImgSg);
    }

    public void setTextViewFamilyCardImgSg(String fileFamilyCardImgSg) {
        this.textViewFamilyCardImgSg.setText(fileFamilyCardImgSg);
    }

    public void setTextViewIdImgSgUrl(String textViewIdImgSgUrl) {
        this.textViewIdImgSgUrl.setText(textViewIdImgSgUrl);
    }

    public void setTextViewFamilyCardImgSgUrl(String textViewFamilyCardImgSgUrl) {
        this.textViewFamilyCardImgSgUrl.setText(textViewFamilyCardImgSgUrl);
    }

    public Button getButtonIdImageSg() {
        return buttonIdImageSg;
    }

    public Button getButtonPersonalFamilyCardSg() {
        return buttonPersonalFamilyCardSg;
    }

    public void setEnabledButtonImage(){
        buttonIdImageSg.setEnabled(true);
        buttonPersonalFamilyCardSg.setEnabled(true);
    }

    public void setField() {
        customerNameSg = (EditText) view.findViewById(R.id.edit_text_customer_name_sg_add_new);
        IdTypeSg = (Spinner) view.findViewById(R.id.spinner_id_type_sg_add_new);

        idNumberSg = (EditText) view.findViewById(R.id.edit_text_id_number_sg_add_new);
        idNumberSg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 16){
                    idNumberSg.setError("Minimum 16 digit");
                }
            }
        });

        textViewIdImgSg = (TextView) view.findViewById(R.id.text_view_id_image_sg_add_new);
        textViewIdImgSg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    textViewIdImgSg.setError(null);
                }
            }
        });
        textViewIdImgSgUrl = (TextView) view.findViewById(R.id.text_view_id_image_sg_url_add_new);

        familyCardNumSg = (EditText) view.findViewById(R.id.edit_text_family_card_number_sg_add_new);
        familyCardNumSg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 16){
                    familyCardNumSg.setError("Minimum 16 digit");
                }
            }
        });

        textViewFamilyCardImgSg = (TextView) view.findViewById(R.id.text_view_familiy_card_number_image_sg_add_new);
        textViewFamilyCardImgSg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    textViewFamilyCardImgSg.setError(null);
                }
            }
        });
        textViewFamilyCardImgSgUrl = (TextView) view.findViewById(R.id.text_view_family_card_number_image_sg_url_add_new);

        buttonIdImageSg = (Button) view.findViewById(R.id.button_camera_id_image_sg_add_new);
        buttonPersonalFamilyCardSg = (Button) view.findViewById(R.id.button_camera_family_card_number_image_sg_add_new);

        if(ddlPersonal != null){
            IdTypeSg.setAdapter(setAdapterItemList(ddlPersonal.getIdType()));
        }

        buttonIdImageSg.setEnabled(false);
        buttonPersonalFamilyCardSg.setEnabled(false);
    }

    public void setForm(ModelPersonalSGMini modelPersonalSGMini) {
        Log.d("DDL PERSONAL", ddlPersonal.getCustomerModel().toString());
        if (ddlPersonal != null) {
            IdTypeSg.setSelection(ddlPersonal.getIdType().indexOf(modelPersonalSGMini.getIdtypesg()));
        }

        customerNameSg.setText(modelPersonalSGMini.getFullnamesg());
        idNumberSg.setText(modelPersonalSGMini.getIdnosg());

        textViewIdImgSgUrl.setText(modelPersonalSGMini.getIdimgurlsg());
        Log.d("Url image", textViewIdImgSgUrl.getText().toString());
        PreviewImage previewImage = new PreviewImage();
        String nameIdImageSg = "";
        if (modelPersonalSGMini.getIdimgurlsg().equals("") || modelPersonalSGMini.getIdimgurlsg() == null) {
            nameIdImageSg = "No selected file";
        } else {

            if (modelPersonalSGMini.getIdimgurlsg().startsWith("http")) {
                nameIdImageSg = modelPersonalSGMini.getIdimgurlsg().split("/")[6];
                previewImage.setPreviewImage(modelPersonalSGMini.getIdimgurlsg(), context, textViewIdImgSg);
            } else {
                nameIdImageSg = modelPersonalSGMini.getIdimgsg();
                previewImage.setPreviewImage(nameIdImageSg, context, textViewIdImgSg);
            }


        }
        textViewIdImgSg.setText(nameIdImageSg);


        familyCardNumSg.setText(modelPersonalSGMini.getKknosg());

        textViewFamilyCardImgSgUrl.setText(modelPersonalSGMini.getKkimgurlsg());
        Log.d("Url image", textViewIdImgSgUrl.getText().toString());
        PreviewImage previewFamilyCardImageSg = new PreviewImage();
        String namePersonalFamilyCardImage = "";
        if (modelPersonalSGMini.getKkimgurlsg().equals("") || modelPersonalSGMini.getKkimgurlsg() == null) {
            namePersonalFamilyCardImage = "No selected file";
        } else {
            if (modelPersonalSGMini.getKkimgurlsg().startsWith("http")) {
                namePersonalFamilyCardImage = modelPersonalSGMini.getKkimgurlsg().split("/")[6];
                previewFamilyCardImageSg.setPreviewImage(modelPersonalSGMini.getKkimgurlsg(), context, textViewFamilyCardImgSg);
            } else {
                namePersonalFamilyCardImage = modelPersonalSGMini.getKkimgsg();
                previewFamilyCardImageSg.setPreviewImage(namePersonalFamilyCardImage, context, textViewFamilyCardImgSg);
            }
        }
        textViewFamilyCardImgSg.setText(namePersonalFamilyCardImage);

    }

    public ModelPersonalSGMini getForm(){
        ModelPersonalSGMini modelPersonalSGMini = new ModelPersonalSGMini();

        modelPersonalSGMini.setFullnamesg(customerNameSg.getText().toString());
        modelPersonalSGMini.setIdtypesg(IdTypeSg.getSelectedItem().toString());
        modelPersonalSGMini.setIdnosg(idNumberSg.getText().toString());
        modelPersonalSGMini.setIdimgsg(textViewIdImgSg.getText().toString());
        modelPersonalSGMini.setIdimgurlsg(textViewIdImgSgUrl.getText().toString());
        modelPersonalSGMini.setKknosg(familyCardNumSg.getText().toString());
        modelPersonalSGMini.setKkimgsg(textViewFamilyCardImgSg.getText().toString());
        modelPersonalSGMini.setKkimgurlsg(textViewFamilyCardImgSgUrl.getText().toString());
        modelPersonalSGMini.setModified(true);

        return modelPersonalSGMini;
    }

    private ArrayAdapter<String> setAdapterItemList(List items) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl, items);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;
    }

    public ModelPersonalSGMini getComplete() {
        ModelPersonalSGMini modelPersonalSGMini = getForm();

        modelPersonalSGMini.setComplete(true);
        if (modelPersonalSGMini.getFullnamesg().length() == 0) {
            customerNameSg.setError("Required");
            modelPersonalSGMini.setComplete(false);
        } else if (modelPersonalSGMini.getFullnamesg().trim().length() == 0) {
            customerNameSg.setError("Whitespaces only detected");
            modelPersonalSGMini.setComplete(false);
        }

        if (modelPersonalSGMini.getIdnosg().length() == 0) {
            idNumberSg.setError("Required");
            modelPersonalSGMini.setComplete(false);
        } else if (modelPersonalSGMini.getIdnosg().trim().length() == 0) {
            idNumberSg.setError("Whitespaces only detected");
            modelPersonalSGMini.setComplete(false);
        } else if (modelPersonalSGMini.getIdnosg().trim().length() != 16) {
            idNumberSg.setError("Minimum and maximum 16 digits");
            modelPersonalSGMini.setComplete(false);
        }

        if(modelPersonalSGMini.getIdimgsg().length() == 0){
            textViewIdImgSg.setError("Required");
            modelPersonalSGMini.setComplete(false);
        }else if(modelPersonalSGMini.getIdimgsg().equals("No selected file")){
            textViewIdImgSg.setError("Required");
            modelPersonalSGMini.setComplete(false);
        }else {
            textViewIdImgSg.setError(null);
        }

        if (modelPersonalSGMini.getKknosg().length() == 0) {
            familyCardNumSg.setError("Required");
            modelPersonalSGMini.setComplete(false);
        } else if (modelPersonalSGMini.getKknosg().trim().length() == 0) {
            familyCardNumSg.setError("Whitespaces only detected");
            modelPersonalSGMini.setComplete(false);
        } else if (modelPersonalSGMini.getKknosg().trim().length() != 16) {
            familyCardNumSg.setError("Minimum and maximum 16 digits");
            modelPersonalSGMini.setComplete(false);
        }

        if(modelPersonalSGMini.getKkimgsg().length() == 0){
            textViewFamilyCardImgSg.setError("Required");
            modelPersonalSGMini.setComplete(false);
        }else if(modelPersonalSGMini.getKkimgsg().equals("No selected file")){
            textViewFamilyCardImgSg.setError("Required");
            modelPersonalSGMini.setComplete(false);
        }else{
            textViewFamilyCardImgSg.setError(null);
        }

        return modelPersonalSGMini;
    }
}
