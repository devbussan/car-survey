package com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Currency;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataFinancial;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by 0426611017 on 2/14/2018.
 */

public class ObjectDataFinancialComplete {
    private ViewGroup container;
    private View view;
    private Context context;

    private TextView monthlyFixedIncome, monthlyVariabelIncome,
            otherBusinessIncome, spouseGuarantorIncome, monthlyLivingCost,
            otherLoanInstallment;

    private TextView namePersonalMonthlyFixedIncomeImage;

    public ObjectDataFinancialComplete(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_data_financial_complete, viewGroup, false);

        setField();
    }

    public View getView() {
        return view;
    }

    private void setField() {

        monthlyFixedIncome = view.findViewById(R.id.value_monthly_fixed_income);
        namePersonalMonthlyFixedIncomeImage = view.findViewById(R.id.value_monthly_fixed_income_image);
        monthlyVariabelIncome = view.findViewById(R.id.value_monthly_variable_income);
        otherBusinessIncome = view.findViewById(R.id.value_other_business_income);
        spouseGuarantorIncome = view.findViewById(R.id.value_spouse_guarantor_income);
        monthlyLivingCost = view.findViewById(R.id.value_monthly_living_cost);
        otherLoanInstallment = view.findViewById(R.id.value_other_loan_installment);
    }

    public void setForm(final ModelDataFinancial modelDataFinancial){
        monthlyFixedIncome.setText(modelDataFinancial.getFixedincome());
        if(!monthlyFixedIncome.getText().toString().equals("")){
            if(!monthlyFixedIncome.getText().toString().equals("")){
                String formatted = Currency.set(monthlyFixedIncome);
                monthlyFixedIncome.setText(formatted.replaceAll("[$]",""));
            }
        }

        if (modelDataFinancial.getSlipimgurl() != null){
            if (!modelDataFinancial.getSlipimgurl().equals("")){
                namePersonalMonthlyFixedIncomeImage.setText(modelDataFinancial.getSlipimgurl().split("/")[6]);
            }else{
                namePersonalMonthlyFixedIncomeImage.setText(modelDataFinancial.getSlipimgurl());
            }
        }else{
            namePersonalMonthlyFixedIncomeImage.setText(modelDataFinancial.getSlipimg());
        }

        monthlyVariabelIncome.setText(modelDataFinancial.getVariableincome());
        if(!monthlyVariabelIncome.getText().toString().equals("")){
            if(!monthlyVariabelIncome.getText().toString().equals("")){
                String formatted = Currency.set(monthlyVariabelIncome);
                monthlyVariabelIncome.setText(formatted.replaceAll("[$]",""));
            }
        }
        otherBusinessIncome.setText(modelDataFinancial.getOtherincome());
        if(!otherBusinessIncome.getText().toString().equals("")){
            if(!otherBusinessIncome.getText().toString().equals("")){
                String formatted = Currency.set(otherBusinessIncome);
                otherBusinessIncome.setText(formatted.replaceAll("[$]",""));
            }
        }
        spouseGuarantorIncome.setText(modelDataFinancial.getSpouseincome());
        if(!spouseGuarantorIncome.getText().toString().equals("")){
            if(!spouseGuarantorIncome.getText().toString().equals("")){
                String formatted = Currency.set(spouseGuarantorIncome);
                spouseGuarantorIncome.setText(formatted.replaceAll("[$]",""));
            }
        }
        monthlyLivingCost.setText(modelDataFinancial.getMonthlylivcost());
        if(!monthlyLivingCost.getText().toString().equals("")){
            if(!monthlyLivingCost.getText().toString().equals("")){
                String formatted = Currency.set(monthlyLivingCost);
                monthlyLivingCost.setText(formatted.replaceAll("[$]",""));
            }
        }
        otherLoanInstallment.setText(modelDataFinancial.getOtherloaninst());
        if(!otherLoanInstallment.getText().toString().equals("")){
            if(!otherLoanInstallment.getText().toString().equals("")){
                String formatted = Currency.set(otherLoanInstallment);
                otherLoanInstallment.setText(formatted.replaceAll("[$]",""));
            }
        }

        PreviewImage previewImage = new PreviewImage();
        if (modelDataFinancial.getSlipimgurl() != null)
            if (!modelDataFinancial.getSlipimgurl().equals(""))
                previewImage.setPreviewImage(modelDataFinancial.getSlipimgurl(), context, namePersonalMonthlyFixedIncomeImage);
    }

}
