package com.example.a0426611017.carsurvey.Fragment.Personal.MiniForm;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalSGMini;
import com.example.a0426611017.carsurvey.Object.Personal.MiniForm.ObjectDataPersonalSg;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseMiniFormPersonalSg;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class FragmentDataPersonalSGOrder extends Fragment {
    private View view;
    private Context context;
    private int cameraIdxButton = 0;
    private String nameFile;
    private TakePicture takePicture = new TakePicture();
    private PreviewImage previewImage = new PreviewImage();
    private TextView fileNameIdImageSg, fileNameIdImageSgUrl,
            fileNamePersonalFamilyCardImageSg,
            fileNamePersonalFamilyCardImageSgUrl;

    private ObjectDataPersonalSg objectDataPersonalSg;
    private ModelPersonalSGMini modelPersonalSGMini;
    private ModelDDLPersonal ddlPersonal = new ModelDDLPersonal();
    private FileFunction fileFunction = new FileFunction();

    private ApiInterface apiInterface;
    private String personalOrderId = null;
    private ProgressDialog loadingDialog;
    private boolean isNew = true;

    private ResponseMiniFormPersonalSg responseMiniFormPersonalSg = new ResponseMiniFormPersonalSg();

    private Gson gson = new Gson();

    public static FragmentDataPersonalSGOrder newInstance(ModelPersonalSGMini modelPersonalSGMini, boolean isNew, ModelDDLPersonal ddlPersonal, String orderId) {
        FragmentDataPersonalSGOrder fragmentDataPersonalSGOrder = new FragmentDataPersonalSGOrder();
        Bundle args = new Bundle();
        args.putSerializable("modelPersonalSGOrder", modelPersonalSGMini);
        args.putSerializable("ddlPersonal", ddlPersonal);
        args.putString("personalOrderId", orderId);
        args.putBoolean(Constans.KEY_IS_NEW, isNew);
        fragmentDataPersonalSGOrder.setArguments(args);
        return fragmentDataPersonalSGOrder;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if(getArguments() !=null){
            modelPersonalSGMini = (ModelPersonalSGMini) getArguments().getSerializable("modelPersonalSGOrder");
            ddlPersonal = (ModelDDLPersonal) getArguments().getSerializable("ddlPersonal");
            personalOrderId = getArguments().getString("personalOrderId");
            isNew = getArguments().getBoolean(Constans.KEY_IS_NEW);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectDataPersonalSg = new ObjectDataPersonalSg(container, inflater, ddlPersonal);
        view = objectDataPersonalSg.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if (modelPersonalSGMini.isModified()) {
            objectDataPersonalSg.setForm(modelPersonalSGMini);
        }else{
            if (!isNew) {

                Log.d("pesan","persoanal id   "+personalOrderId);
                getDetailPersonalSgOrder(personalOrderId);
            }
        }

        fileNameIdImageSg = objectDataPersonalSg.getTextViewIdImgSg();
        fileNameIdImageSgUrl = objectDataPersonalSg.getTextViewIdImgSgUrl();
        Button buttonIdImage = objectDataPersonalSg.getButtonIdImageSg();

        fileNamePersonalFamilyCardImageSg = objectDataPersonalSg.getTextViewFamilyCardImgSg();
        fileNamePersonalFamilyCardImageSgUrl = objectDataPersonalSg.getTextViewFamilyCardImgSgUrl();
        Button buttonPersonalFamilyCard = objectDataPersonalSg.getButtonPersonalFamilyCardSg();

        callCamera(buttonIdImage,1);
        callCamera(buttonPersonalFamilyCard, 2);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
        } else {
            objectDataPersonalSg.setEnabledButtonImage();
        }

        return view;
    }

    private void callCamera(Button button, int idx) {
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(v, idxButton);

            }
        });
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                objectDataPersonalSg.setEnabledButtonImage();
            }
        }
    }

    public void takePicture(View view, int idx) {
        this.cameraIdxButton = idx;
        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                switch (cameraIdxButton) {
                    case 1:
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();

                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectDataPersonalSg.setTextViewIdImgSg(nameFile);
                        objectDataPersonalSg.setTextViewIdImgSgUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNameIdImageSg);
                        break;
                    case 2:
                        if (data != null) {
                            try {
                               File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();

                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectDataPersonalSg.setTextViewFamilyCardImgSg(nameFile);
                        objectDataPersonalSg.setTextViewFamilyCardImgSgUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNamePersonalFamilyCardImageSg);
                        break;
                    default:
                        break;
                }
            }
        }
    }


    public ModelPersonalSGMini getForm(){return objectDataPersonalSg.getForm();
    }

    public void setForm(ModelPersonalSGMini modelPersonalSGMini) {objectDataPersonalSg.setForm(modelPersonalSGMini);}

    public ModelPersonalSGMini getComplete(){
        return objectDataPersonalSg.getComplete();
    }

    private void getDetailPersonalSgOrder(final String personal) {
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseMiniFormPersonalSg> personalOrderId = apiInterface.getDetailPersonalSgMiniForm(personal);
            personalOrderId.enqueue(new Callback<ResponseMiniFormPersonalSg>() {
                @Override
                public void onResponse(Call<ResponseMiniFormPersonalSg> call, Response<ResponseMiniFormPersonalSg> response) {
                    if (response.isSuccessful()) {
                        modelPersonalSGMini = response.body().getModelPersonalSGMini();
                        setForm(modelPersonalSGMini);

                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseMiniFormPersonalSg = gson.fromJson(response.errorBody().string(), ResponseMiniFormPersonalSg.class);
                            error = responseMiniFormPersonalSg.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailPersonalSgOrder(personal);
                                    }
                                });
                                b.show();
                            } else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.fragment_personal_sg_id), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailPersonalSgOrder(personal);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        Log.e("Error","Service response time out");
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailPersonalSgOrder(personal);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }
                @Override
                public void onFailure(Call<ResponseMiniFormPersonalSg> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailPersonalSgOrder(personal);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailPersonalSgOrder(personal);
                }
            });
            b.show();
            Log.e("Error","No Internet Connectivity");
            loadingDialog.dismiss();
        }
    }
}
