package com.example.a0426611017.carsurvey.Model.Company;

import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 20/02/2018.
 */

public class ModelContactCompany {

    @SerializedName("COMPANY_CONTACT_ORDER")
    private String contactOrderId;

    @SerializedName("COMPANY_INFO_ORDER")
    private String contactInfoOrder;

    public String getContactInfoOrder() {
        return contactInfoOrder;
    }

    public void setContactInfoOrder(String contactInfoOrder) {
        this.contactInfoOrder = contactInfoOrder;
    }

    public String getContactOrderId() {
        return contactOrderId;
    }

    public void setContactOrderId(String contactOrderId) {
        this.contactOrderId = contactOrderId;
    }
}
