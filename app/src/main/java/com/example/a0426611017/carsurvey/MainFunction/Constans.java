package com.example.a0426611017.carsurvey.MainFunction;

import java.util.regex.Pattern;

/**
 * Created by c201017001 on 25/01/2018.
 */

public class Constans {
    public final static String [] status = {
            "DRAFT",
            "PENDING",
            "CLEAR",
            "RETURN ORDER",
            "REJECT ORDER",
            "CHECK HQ",
            "RETURN FULL",
            "REJECT FULL",
            "COMPLETE",
            "INPUT FULL"
    };

    public final static int STATUS_DRAFT = 0;
    public final static int STATUS_PENDING = 1;
    public final static int STATUS_CLEAR = 2;
    public final static int STATUS_RETURN_ORDER = 3;
    public final static int STATUS_REJECT_ORDER = 4;
    public final static int STATUS_CHECK_HQ = 5;
    public final static int STATUS_RETURN_FULL = 7;
    public final static int STATUS_REJECT_FULL = 8;
    public final static int STATUS_COMPLETE = 6;
    public final static int STATUS_INPUT_FULL = 2;
    public final static int STATUS_BANDING_ORDER = 13;
    public final static int STATUS_BANDING_FULL = 15;

    //    ORDER FORM
    public final static int TAB_DRAFT = 0;          //Detail - Edit
    public final static int TAB_PENDING = 1;        //Detail
    public final static int TAB_RETURN_ORDER = 2;   //Detail - Edit
    public final static int TAB_REJECT_ORDER = 3;   //Detail - Banding
    public final static int TAB_CLEAR = 4;          //Detail

    public final static String TAB_TEXT_DRAFT="DRAFT";
    public final static String TAB_TEXT_PENDING="PENDING";
    public final static String TAB_TEXT_CLEAR="CLEAR";

    //    FULL ORDER
    public final static int TAB_INPUT_FULL=0;       //Detail - Edit
    public final static int TAB_CHECK_HQ = 1;       //Detail
    public final static int TAB_RETURN_FULL = 2;    //Detail - Edit
    public final static int TAB_REJECT_FULL = 3;    //Detail - Banding
    public final static int TAB_COMPLETE = 4;       //Detail

    public final static String TAB_TEXT_INPUT="INPUT FULL";
    public final static String TAB_TEXT_CHECKHQ="CHECK HQ";
    public final static String TAB_TEXT_RETURN="RETURN";
    public final static String TAB_TEXT_REJECT="REJECT";
    public final static String TAB_TEXT_APPROVE="APPROVE";

    public final static String KEY_MENU = "keyMenu";
    public final static String KEY_MENU_CATEGORY = "keyMenuCategory";
    public final static int MENU_PERSONAL = 1;
    public final static int MENU_PERSONAL_ORDER_FORM = 2;
    public final static int MENU_PERSONAL_FULL_FORM = 3;

    public final static int MENU_COMPANY = 4;
    public final static int MENU_COMPANY_ORDER_FORM = 5;
    public final static int MENU_COMPANY_FULL_FORM = 6;

    public final static String KEY_IS_NEW = "isNew";
    public final static String KEY_CURRENT_TAB = "keyTabCurrent";
    public final static String KEY_TAB = "keyTab";
    public final static String KEY_LOG_ID = "keyLogID";
    public final static String KEY_CUST_DATA_ID = "keyCustDataID";
    public final static String KEY_SURVEY_ID = "keySurveyID";
    public final static String KEY_SPOUSE_ID = "keySpouse";
    public final static String KEY_PERSONAL_KK_ID = "keyPersonalKK";
    public final static String KEY_PERSONAL_KK_IMAGE = "keyPersonalKKImage";
    public final static String KEY_SPOUSE_KK_ID = "keySpouseKK";
    public final static String KEY_SPOUSE_KK_IMAGE = "keySpouseKKImage";

    public final static String PARAM_WS_COMPANY ="company";
    public final static String PARAM_WS_PERSONAL ="personal";

    public final static Pattern VALID_EMAIL_ADDRESS =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    // public final static String DOMAIN = "http://192.168.250.14:8987";         // Locals DEV
    // public final static String DOMAIN = "http://apicarsurvey.baf.id:8987";    // Public DEV
    // public final static String DOMAIN = "http://192.168.250.27:8987/";        // Locals PROD
    public final static String DOMAIN = "http://apimobile.baf.id:8987";          // Public PROD

    public final static String URL_IMAGE_THUMB = "/android/images/thumb/";

    //    SharedPreference
    public final static String KEY_ADMIN_ID = "keyAdminId";
    public final static String KEY_USERNAME = "keyUserName";
    public final static String KEY_PASSWORD = "keyPassWord";
    public final static String KEY_IS_ACTIVE = "keyIsActive";
    public final static String KEY_AFTER_DATE = "keyAfterDate";
    public final static String KEY_COMPARE_DATE = "keyCompareDate";
    public final static String KEY_ADMIN_NAME = "keyAdminName";
    public final static String KEY_ADMIN_NIK = "keyAdminNik";


    public final static int TAB_PERSONAL_DASHBOARD = 0;
    public final static int TAB_COMPANY_DASHBOARD = 1;


    public static class KeySharedPreference{
        public final static String ORDER_ID = "sharedPreferenceOrderId";
        public final static String PERSONAL_TAB = "sharedPreferencePersonalTab";
        public final static String CURRENT_TAB = "sharedPreferenceCurrentTab";
        public final static String MENU_CATEGORY = "sharedPreferenceMenuCategory";
        public final static String COMPANY_ORDER_ID = "sharedPreferenceCompanyOrderID";
        public final static String PERSONAL_ORDER_ID = "sharedPreferencePersonalOrderID";
        public final static String ADMIN_ID = "sharedPreferenceAdminId";
        public final static String LOG_ID = "sharedPreferenceLogId";
        public final static String CUST_DATA_ID = "sharedPreferenceCustDataId";
        public final static String SURVEY_ID = "sharedPreferenceSurveyId";
        public final static String SPOUSE_ID = "sharedPreferenceSpouseId";
        public final static String PERSONAL_KK_ID = "sharedPreferencePersonalKKid";
        public final static String PERSONAL_KK_IMAGE = "sharedPreferencePersonalKKImage";
        public final static String SPOUSE_KK_ID = "sharedPreferenceSpouseKKid";
        public final static String SPOUSE_KK_IMAGE = "sharedPreferenceSpouseKKImage";
        public final static String IS_ACTIVE = "sharedPreferenceIsActive";
        public final static String USERNAME = "sharedPreferenceUserName";
        public final static String PASSWORD = "sharedPreferencePassWord";
        public final static String DATE = "sharedPreferenceDate";
        public final static String ADMIN_NAME = "sharedPreferenceAdminName";
        public final static String ADMIN_NIK = "sharedPreferenceAdminNik";
    }
}

