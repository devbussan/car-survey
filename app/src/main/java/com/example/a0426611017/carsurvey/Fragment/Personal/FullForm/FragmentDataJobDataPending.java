package com.example.a0426611017.carsurvey.Fragment.Personal.FullForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataJobData;
import com.example.a0426611017.carsurvey.Object.Personal.FullForm.ObjectDataJobDataPending;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalJob;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDataJobDataPending extends Fragment {
    private View view;
    private Context context;

    private ObjectDataJobDataPending objectDataJobDataPending;
    private ModelDataJobData modelDataJobData = new ModelDataJobData();

    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private String personalCustDataId = null;
    private String name = "";
    private Gson gson = new Gson();
    private boolean isNew = true;

    private ResponseDetailPersonalJob responseDetailPersonalJob = new ResponseDetailPersonalJob();

    public FragmentDataJobDataPending newInstance(ModelDataJobData modelDataJobData, boolean isNew, String custId, String name) {
        FragmentDataJobDataPending fragmentDataJobDataPending = new FragmentDataJobDataPending();
        Bundle args = new Bundle();
        args.putSerializable("modelJobPersonal", modelDataJobData);
        args.putString("personalCustDataId", custId);
        args.putBoolean(Constans.KEY_IS_NEW, isNew);
        args.putString("name", name);
        fragmentDataJobDataPending.setArguments(args);
        return fragmentDataJobDataPending;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelDataJobData = (ModelDataJobData) getArguments().getSerializable("modelJobPersonal");
            personalCustDataId = getArguments().getString("personalCustDataId");
            isNew = getArguments().getBoolean(Constans.KEY_IS_NEW);
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        objectDataJobDataPending = new ObjectDataJobDataPending(container, inflater);
        view = objectDataJobDataPending.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if(modelDataJobData.isModified()){
            objectDataJobDataPending.setForm(modelDataJobData);
        }else{
            if (!isNew) {
                getDetailJob(name, personalCustDataId);
            }
        }

        return view;
    }

    public ModelDataJobData getForm(){
        return objectDataJobDataPending.getForm();
    }

    public void setForm(ModelDataJobData modelDataJobData){
        objectDataJobDataPending.setForm(modelDataJobData);
    }

    public ModelDataJobData getComplete(){
        return objectDataJobDataPending.getComplete();
    }

    private void getDetailJob(final String name, final String idcustdata){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalJob> jobDetail = apiInterface.getDetailPersonalJob(name, idcustdata);
            jobDetail.enqueue(new Callback<ResponseDetailPersonalJob>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalJob> call, Response<ResponseDetailPersonalJob>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataJobData = response.body().getModelDataJobData();
                        Log.d("Result", gson.toJson(modelDataJobData));

                        setForm(modelDataJobData);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalJob = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalJob.class);
                            error = responseDetailPersonalJob.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailJob(name, idcustdata);
                                    }
                                });
                                b.show();
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.personal_jobs_full_edit), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailJob(name, idcustdata);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailJob(name, idcustdata);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalJob> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailJob(name, idcustdata);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailJob(name, idcustdata);
                }
            });
            b.show();
            Log.e("Error","No Internet Connectivity");
            loadingDialog.dismiss();
        }
    }

}