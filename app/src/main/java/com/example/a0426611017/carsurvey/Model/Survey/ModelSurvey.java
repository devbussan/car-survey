package com.example.a0426611017.carsurvey.Model.Survey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by c201017001 on 08/02/2018.
 */

public class ModelSurvey implements Serializable{
//    private String tanggalTerima1;
//
//    private String tanggalSurvey1;

    @SerializedName("DTMORDER")
    private String dtmorder;

    @SerializedName("SURVEYDATE")
    private String surveydate;

    @SerializedName("SURVEYADDR")
    private String surveyaddr;

    @SerializedName("SURVEYLOCATION")
    private String surveylocation;

    @SerializedName("SRCINCOMING")
    private String srcincoming;

    @SerializedName("SRCINCOMINGNAME")
    private String srcincomingname;

    @SerializedName("INFORSRCNAME")
    private String inforsrcname;

    @SerializedName("SRCNAMESTAT")
    private String srcnamestat;

    @SerializedName("SRCSTATKET")
    private String srcstatket;

    @SerializedName("CLNDEBITUR")
    private String clndebitur;

    @SerializedName("HOMETYPE")
    private String hometype;

    @SerializedName("HOMELOC")
    private String homeloc;

    @SerializedName("RENTPRICE")
    private String rentprice;

    @SerializedName("BLDTYPE")
    private String bldtype;

    @SerializedName("BLDCOND")
    private String bldcond;

    @SerializedName("GARAGESTAT")
    private String garagestat;

    @SerializedName("BLDAREA")
    private String bldarea;

    @SerializedName("LUASBLD")
    private String luasbld;

    @SerializedName("STAYLENGTH")
    private String staylength;

    @SerializedName("ELCTRYBILLNAME")
    private String elctrybillname;

    @SerializedName("ELCTRYBILLSTAT")
    private String elctrybillstat;

    @SerializedName("ISORMAS")
    private String isormas;

    @SerializedName("NUMOFCAR")
    private String numofcar ;

    @SerializedName("CARSTAT")
    private String carstat ;

    @SerializedName("CARIMGURL")
    private String carimgurl ;

    private String carimg ;

    @SerializedName("NUMOFMOTORCYCLE")
    private String numofmotorcycle ;

    @SerializedName("MOTORCYCLESTAT")
    private String motorcyclestat ;

    @SerializedName("MOTORCYCLEIMGURL")
    private String motorcycleimgurl ;

    private String motorcycleimg ;

    @SerializedName("CHARACTERCUST")
    private String charactercust;

    @SerializedName("JMLTANGGUNGAN")
    private String jmltanggungan;

    @SerializedName("TOTALINCOME")
    private String totalincome ;

    @SerializedName("LAMAKERJA")
    private String lamakerja;

    @SerializedName("POSITION")
    private String position;

    @SerializedName("ATASAN")
    private String atasan;

    @SerializedName("ATASANPHONE")
    private String atasanphone;

    @SerializedName("BIAYARUTIN")
    private String biayarutin;

    @SerializedName("TAGIHANLAIN")
    private String tagihanlain;

    @SerializedName("USRCRT")
    private String usrcrt;

    @SerializedName("ORDER_ID")
    private String orderid;

    @SerializedName("SURVEY_ID")
    private String surveyid;

    @SerializedName("SURVEY_STATUS")
    private String companySurveyStatus;

    private boolean isModified = false;

    private boolean isComplete = false;

//    public String getTanggalTerima1() {
//        return tanggalTerima1;
//    }
//
//    public void setTanggalTerima1(String tanggalTerima1) {
//        this.tanggalTerima1 = tanggalTerima1;
//    }
//
//    public String getTanggalSurvey1() {
//        return tanggalSurvey1;
//    }
//
//    public void setTanggalSurvey1(String tanggalSurvey1) {
//        this.tanggalSurvey1 = tanggalSurvey1;
//    }

    public String getDtmorder() {
        return dtmorder;
    }

    public void setDtmorder(String dtmorder) {
        this.dtmorder = dtmorder;
    }

    public String getSurveydate() {
        return surveydate;
    }

    public void setSurveydate(String surveydate) {
        this.surveydate = surveydate;
    }

    public String getSurveyaddr() {
        return surveyaddr;
    }

    public void setSurveyaddr(String surveyaddr) {
        this.surveyaddr = surveyaddr;
    }

    public String getSurveylocation() {
        return surveylocation;
    }

    public void setSurveylocation(String surveylocation) {
        this.surveylocation = surveylocation;
    }

    public String getSrcincoming() {
        return srcincoming;
    }

    public void setSrcincoming(String srcincoming) {
        this.srcincoming = srcincoming;
    }

    public String getSrcincomingname() {
        return srcincomingname;
    }

    public void setSrcincomingname(String srcincomingname) {
        this.srcincomingname = srcincomingname;
    }

    public String getInforsrcname() {
        return inforsrcname;
    }

    public void setInforsrcname(String inforsrcname) {
        this.inforsrcname = inforsrcname;
    }

    public String getSrcnamestat() {
        return srcnamestat;
    }

    public void setSrcnamestat(String srcnamestat) {
        this.srcnamestat = srcnamestat;
    }

    public String getSrcstatket() {
        return srcstatket;
    }

    public void setSrcstatket(String srcstatket) {
        this.srcstatket = srcstatket;
    }

    public String getClndebitur() {
        return clndebitur;
    }

    public void setClndebitur(String clndebitur) {
        this.clndebitur = clndebitur;
    }

    public String getHometype() {
        return hometype;
    }

    public void setHometype(String hometype) {
        this.hometype = hometype;
    }

    public String getHomeloc() {
        return homeloc;
    }

    public void setHomeloc(String homeloc) {
        this.homeloc = homeloc;
    }

    public String getRentprice() {
        return rentprice;
    }

    public void setRentprice(String rentprice) {
        this.rentprice = rentprice;
    }

    public String getBldtype() {
        return bldtype;
    }

    public void setBldtype(String bldtype) {
        this.bldtype = bldtype;
    }

    public String getBldcond() {
        return bldcond;
    }

    public void setBldcond(String bldcond) {
        this.bldcond = bldcond;
    }

    public String getGaragestat() {
        return garagestat;
    }

    public void setGaragestat(String garagestat) {
        this.garagestat = garagestat;
    }

    public String getBldarea() {
        return bldarea;
    }

    public void setBldarea(String bldarea) {
        this.bldarea = bldarea;
    }

    public String getLuasbld() {
        return luasbld;
    }

    public void setLuasbld(String luasbld) {
        this.luasbld = luasbld;
    }

    public String getStaylength() {
        return staylength;
    }

    public void setStaylength(String staylength) {
        this.staylength = staylength;
    }

    public String getElctrybillname() {
        return elctrybillname;
    }

    public void setElctrybillname(String elctrybillname) {
        this.elctrybillname = elctrybillname;
    }

    public String getElctrybillstat() {
        return elctrybillstat;
    }

    public void setElctrybillstat(String elctrybillstat) {
        this.elctrybillstat = elctrybillstat;
    }

    public String getIsormas() {
        return isormas;
    }

    public void setIsormas(String isormas) {
        this.isormas = isormas;
    }

    public String getNumofcar() {
        return numofcar;
    }

    public void setNumofcar(String numofcar) {
        this.numofcar = numofcar;
    }

    public String getCarstat() {
        return carstat;
    }

    public void setCarstat(String carstat) {
        this.carstat = carstat;
    }

    public String getCarimgurl() {
        return carimgurl;
    }

    public void setCarimgurl(String carimgurl) {
        this.carimgurl = carimgurl;
    }

    public String getNumofmotorcycle() {
        return numofmotorcycle;
    }

    public void setNumofmotorcycle(String numofmotorcycle) {
        this.numofmotorcycle = numofmotorcycle;
    }

    public String getMotorcyclestat() {
        return motorcyclestat;
    }

    public void setMotorcyclestat(String motorcyclestat) {
        this.motorcyclestat = motorcyclestat;
    }

    public String getMotorcycleimgurl() {
        return motorcycleimgurl;
    }

    public void setMotorcycleimgurl(String motorcycleimgurl) {
        this.motorcycleimgurl = motorcycleimgurl;
    }

    public String getCharactercust() {
        return charactercust;
    }

    public void setCharactercust(String charactercust) {
        this.charactercust = charactercust;
    }

    public String getJmltanggungan() {
        return jmltanggungan;
    }

    public void setJmltanggungan(String jmltanggungan) {
        this.jmltanggungan = jmltanggungan;
    }

    public String getTotalincome() {
        return totalincome;
    }

    public void setTotalincome(String totalincome) {
        this.totalincome = totalincome;
    }

    public String getLamakerja() {
        return lamakerja;
    }

    public void setLamakerja(String lamakerja) {
        this.lamakerja = lamakerja;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAtasan() {
        return atasan;
    }

    public void setAtasan(String atasan) {
        this.atasan = atasan;
    }

    public String getAtasanphone() {
        return atasanphone;
    }

    public void setAtasanphone(String atasanphone) {
        this.atasanphone = atasanphone;
    }

    public String getBiayarutin() {
        return biayarutin;
    }

    public void setBiayarutin(String biayarutin) {
        this.biayarutin = biayarutin;
    }

    public String getTagihanlain() {
        return tagihanlain;
    }

    public void setTagihanlain(String tagihanlain) {
        this.tagihanlain = tagihanlain;
    }

    public String getUsrcrt() {
        return usrcrt;
    }

    public void setUsrcrt(String usrcrt) {
        this.usrcrt = usrcrt;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getSurveyid() {
        return surveyid;
    }

    public void setSurveyid(String surveyid) {
        this.surveyid = surveyid;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public String getCarimg() {
        return carimg;
    }

    public void setCarimg(String carimg) {
        this.carimg = carimg;
    }

    public String getMotorcycleimg() {
        return motorcycleimg;
    }

    public void setMotorcycleimg(String motorcycleimg) {
        this.motorcycleimg = motorcycleimg;
    }

    public String getCompanySurveyStatus() {
        return companySurveyStatus;
    }

    public void setCompanySurveyStatus(String companySurveyStatus) {
        this.companySurveyStatus = companySurveyStatus;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
