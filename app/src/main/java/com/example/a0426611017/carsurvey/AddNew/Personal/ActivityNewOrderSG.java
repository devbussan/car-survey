package com.example.a0426611017.carsurvey.AddNew.Personal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.a0426611017.carsurvey.Finance.ActivityCompanyFinancial;
import com.example.a0426611017.carsurvey.Fragment.Personal.MiniForm.FragmentDataPersonalSGOrder;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiPersonalOrder;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalSGMini;
import com.example.a0426611017.carsurvey.Personal.FullForm.ActivityPersonalFullEdit;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDDLPersonal;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNewOrderSG extends AppCompatActivity implements View.OnClickListener{
    private Fragment currentFragment;

    private FragmentDataPersonalSGOrder fragmentDataPersonalSGOrder = new FragmentDataPersonalSGOrder();

    private ModelPersonalSGMini modelPersonalSGMini = new ModelPersonalSGMini();

    private Button buttonPersonalInfoSg;
    private Button buttonSave;
    private Button buttonSubmitToHq;
    private Button buttonBack;

    private boolean isNew = true;

    private ModelDDLPersonal ddlPersonalList = null;
    private ProgressDialog loadingDialog;
    private ApiPersonalOrder apiPersonalOrder;
    private ApiInterface apiInterface;
    private String adminId = null;
    private String personalOrderId = null;
    private String sharedLogId = null;

    private View currentView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_personal_sg);
        currentView = this.findViewById(android.R.id.content);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading.........");
        loadingDialog.setCancelable(false);

        isNew = getIntent().getBooleanExtra(Constans.KEY_IS_NEW, true);

        getDDLPersonal();

        apiPersonalOrder = new ApiPersonalOrder(currentView, ActivityNewOrderSG.this, loadingDialog);
        adminId = this.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);
        personalOrderId = getIntent().getStringExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID);
        sharedLogId = getIntent().getStringExtra(Constans.KeySharedPreference.LOG_ID);

        Log.d("Personal Order", personalOrderId);
        Log.d("Admin Id", adminId);
        Log.d("Log Id", "log id : "+sharedLogId);

        buttonPersonalInfoSg = findViewById(R.id.button_personal_sg);
        buttonPersonalInfoSg.setOnClickListener(this);

        buttonSave = findViewById(R.id.button_save_personal_sg);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderSG.this);
                builder.setTitle(getString(R.string.content_question_want_save_data));
                builder.setMessage(getString(R.string.content_information_save_and_exit));
                builder.setNegativeButton(getString(R.string.dialog_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                loadingDialog.dismiss();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        loadingDialog.show();
                        saveForm();

                        modelPersonalSGMini.setUsrcrt(adminId);

                        if (isNew){
                            modelPersonalSGMini.setPersonalorderid(personalOrderId);
                            Log.d("check out","insert baru");
                            apiPersonalOrder.saveUpdatePersonalSgOrder(modelPersonalSGMini,
                                    personalOrderId,
                                    sharedLogId,
                                    false,
                                    true
                            );

                        } else {
                            modelPersonalSGMini.setPersonalorderid(personalOrderId);
                            Log.d("Check","update cooy");
                            apiPersonalOrder.saveUpdatePersonalSgOrder(modelPersonalSGMini,
                                    personalOrderId,
                                    sharedLogId,
                                    false,
                                    false
                            );
                        }
                    }
                });
                builder.show();
            }
        });

        buttonSubmitToHq = findViewById(R.id.button_submit_personal_sg);
        buttonSubmitToHq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                saveForm();
                checkCompleteSendHQForm();

                if(modelPersonalSGMini.isComplete()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderSG.this);
                    builder.setTitle(getString(R.string.content_question_are_you_sure));
                    builder.setMessage(getString(R.string.content_information_send_to_hq)+
                            "\n"+getString(R.string.content_information_send_to_hq_2));
                    builder.setNegativeButton(getString(R.string.dialog_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {

                                    loadingDialog.dismiss();
                                    Log.e("info", "NO");
                                }
                            });
                    builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.e("info", "YES");
                            modelPersonalSGMini.setUsrcrt(adminId);

                            if (isNew) {
                                modelPersonalSGMini.setPersonalorderid(personalOrderId);
                                apiPersonalOrder.saveUpdatePersonalSgOrder(modelPersonalSGMini,
                                        personalOrderId,
                                        sharedLogId,
                                        true,
                                        true
                                );
                                Log.d("pesan", "asup ka new");
                            } else {
                                modelPersonalSGMini.setPersonalorderid(personalOrderId);
                                apiPersonalOrder.saveUpdatePersonalSgOrder(modelPersonalSGMini,
                                        personalOrderId,
                                        sharedLogId,
                                        true,
                                        false

                                );
                                Log.d("pesan","asup ka edit");

                            }

                        }
                    });
                    builder.show();
                    loadingDialog.dismiss();
                } else {
                    if(!modelPersonalSGMini.isComplete()){
                        if (buttonPersonalInfoSg.isEnabled()) {
                            buttonPersonalInfoSg.setError("Is not Complete");
                        }
                        Snackbar.make(findViewById(R.id.myLayoutPersonalSg), "Please Fill The Data First",
                                Snackbar.LENGTH_LONG)
                                .show();
                        loadingDialog.dismiss();
                    }
                }
            }
        });

        buttonBack = findViewById(R.id.button_back_personal_sg);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentFragment != null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityNewOrderSG.this);
                    b.setTitle(R.string.content_information_back_pressed);
                    b.setMessage(getString(R.string.content_question_want_save_data)+
                            "\n"+getString(R.string.content_information_press_yes_save));

                    b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(ActivityNewOrderSG.this, ActivityNewOrderPersonal.class);
                            intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, personalOrderId);
                            intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);
                            intent.putExtra(Constans.KEY_IS_NEW, false);
                            startActivity(intent);
                            finish();
                        }
                    });
                    b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            buttonSave.callOnClick();
                        }
                    });
                    b.show();
                } else {
                    Intent intent = new Intent(ActivityNewOrderSG.this, ActivityNewOrderPersonal.class);
                    intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, personalOrderId);
                    intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);
                    intent.putExtra(Constans.KEY_IS_NEW, false);
                    startActivity(intent);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_personal_sg :
                buttonPersonalInfoSg.setEnabled(false);
                buttonPersonalInfoSg.setBackground(getDrawable(R.drawable.disabled_button_background_personal));
                buttonPersonalInfoSg.setTextColor(getResources().getColor(R.color.color_personal));
                buttonPersonalInfoSg.setError(null);

                if(ddlPersonalList == null){
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityNewOrderSG.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLPersonal();
                            buttonPersonalInfoSg.callOnClick();
                        }
                    });
                    b.show();
                }else {
                    fragmentDataPersonalSGOrder = fragmentDataPersonalSGOrder.newInstance(modelPersonalSGMini, isNew, ddlPersonalList, personalOrderId);
                    saveForm();

                    loadFragment(fragmentDataPersonalSGOrder);
                }
                break;
        }
    }

    private void saveForm() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (currentFragment.getClass() == fragmentDataPersonalSGOrder.getClass()){
                    modelPersonalSGMini = fragmentDataPersonalSGOrder.getForm();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderSG.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    saveForm();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    public void checkCompleteSendHQForm() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (modelPersonalSGMini.isModified()) {
                    modelPersonalSGMini = fragmentDataPersonalSGOrder.getComplete();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderSG.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    checkCompleteSendHQForm();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    private void loadFragment(Fragment fragment) {
        currentFragment = fragment;
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.frame_personal_sg_order, fragment);
        fragmentTransaction.commit();

    }

    private void setResponseDDLPersonal(ModelDDLPersonal ddlPersonalList){
        this.ddlPersonalList = ddlPersonalList;
    }

    public void getDDLPersonal(){
        loadingDialog.show();
        if (InternetConnection.checkConnection(this)) {
            Call<ResponseDDLPersonal> ddlCompany = apiInterface.getDDLPersonal();
            ddlCompany.enqueue(new Callback<ResponseDDLPersonal>() {
                @Override
                public void onResponse(Call<ResponseDDLPersonal> call, Response<ResponseDDLPersonal>
                        response) {
                    if (response.isSuccessful()) {
                        setResponseDDLPersonal(response.body().getDdlPersonal());
                        loadingDialog.dismiss();
                    } else {
                        loadingDialog.dismiss();
                        Snackbar.make(findViewById(R.id.myLayoutPersonalSg), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDDLPersonal> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    Snackbar.make(findViewById(R.id.myLayoutPersonalSg), t.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(R.id.myLayoutPersonalSg), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            loadingDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder b = new AlertDialog.Builder(ActivityNewOrderSG.this);
        b.setTitle(R.string.content_information_back_pressed);
        b.setMessage(getString(R.string.content_question_want_save_data)+
                "\n"+getString(R.string.content_information_press_yes_save));
        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ActivityNewOrderSG.this, ActivityNewOrderPersonal.class);
                intent.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, personalOrderId);
                intent.putExtra(Constans.KeySharedPreference.LOG_ID, sharedLogId);
                intent.putExtra(Constans.KEY_IS_NEW, false);
                startActivity(intent);
                finish();
            }
        });
        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                buttonSave.callOnClick();
            }
        });
        b.show();
    }
}
