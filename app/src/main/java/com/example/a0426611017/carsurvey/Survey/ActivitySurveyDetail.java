package com.example.a0426611017.carsurvey.Survey;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Finance.ActivityCompanyFinancialDetail;
import com.example.a0426611017.carsurvey.Fragment.Survey.FragmentCekLingkunganDetail;
import com.example.a0426611017.carsurvey.Fragment.Survey.FragmentCekLingkunganDetail1;
import com.example.a0426611017.carsurvey.Fragment.Survey.FragmentCekLingkunganDetail2;
import com.example.a0426611017.carsurvey.Fragment.Survey.FragmentCekLingkunganDetail3;
import com.example.a0426611017.carsurvey.Fragment.Survey.FragmentSurveyDetail;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseListEnvironment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivitySurveyDetail extends AppCompatActivity {

    private TextView titleBar;

    private Button buttonDataSurvey, buttonCekLingkungan, buttonCekLingkungan1, buttonCekLingkungan2, buttonCekLingkungan3,
            buttonBack, buttonNext;

    private ApiInterface apiInterface;

    private FragmentSurveyDetail fragmentSurveyDetail = new FragmentSurveyDetail();
    private FragmentCekLingkunganDetail fragmentCekLingkunganDetail = new FragmentCekLingkunganDetail();
    private FragmentCekLingkunganDetail1 fragmentCekLingkunganDetail1 = new FragmentCekLingkunganDetail1();
    private FragmentCekLingkunganDetail2 fragmentCekLingkunganDetail2 = new FragmentCekLingkunganDetail2();
    private FragmentCekLingkunganDetail3 fragmentCekLingkunganDetail3 = new FragmentCekLingkunganDetail3();

    private ProgressDialog loadingDialog;

    private List<String> surveyEnv = new ArrayList<>();
    private String orderId;

    private String surveyId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_survey_detail);
        setField();

        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);


        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        orderId = getIntent().getStringExtra(Constans.KeySharedPreference.ORDER_ID);
        surveyId = this.getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, MODE_PRIVATE).getString(Constans.KEY_SURVEY_ID, null);
        int menu = getIntent().getIntExtra(Constans.KEY_MENU, 0);

        switch (menu) {
            case Constans.MENU_PERSONAL:
                setFormForPersonal();
                window.setStatusBarColor(getResources().getColor(R.color.color_personal));
                break;
            case Constans.MENU_COMPANY:
                setFormForCompany();
                window.setStatusBarColor(getResources().getColor(R.color.color_company));
                break;
            default:
                break;
        }

        buttonBack = findViewById(R.id.button_back_survey_detail);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivitySurveyDetail.super.onBackPressed();
            }
        });


        buttonNext = findViewById(R.id.button_next_survey_detail);


    }

    private void setField() {
        titleBar = findViewById(R.id.text_view_survey_detail);

        buttonDataSurvey = findViewById(R.id.button_data_survey_detail);
        buttonCekLingkungan = findViewById(R.id.button_cek_lingkungan_detail);
        buttonCekLingkungan1 = findViewById(R.id.button_cek_lingkungan_detail1);
        buttonCekLingkungan1.setVisibility(View.GONE);
        buttonCekLingkungan2 = findViewById(R.id.button_cek_lingkungan_detail2);
        buttonCekLingkungan2.setVisibility(View.GONE);
        buttonCekLingkungan3 = findViewById(R.id.button_cek_lingkungan_detail3);
        buttonCekLingkungan3.setVisibility(View.GONE);

        buttonBack = findViewById(R.id.button_back_survey_detail);
        buttonNext = findViewById(R.id.button_next_survey_detail);
    }

    private void setFormForPersonal() {
        getListidEnvi(orderId, Constans.PARAM_WS_PERSONAL);
        titleBar.setBackgroundColor(getResources().getColor(R.color.color_personal));
        titleBar.setText(getString(R.string.header_survey_personal_form));

        buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

        buttonNext.setText(getString(R.string.btn_next));
        buttonDataSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonDataSurvey.setEnabled(false);
                buttonCekLingkungan.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_personal));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                loadFragment(FragmentSurveyDetail.newInstance(Constans.MENU_PERSONAL, Constans.PARAM_WS_PERSONAL, orderId));
            }
        });

        buttonCekLingkungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_personal));


                String idsurveyenv = null;
                if (surveyEnv != null
                        && surveyEnv.size() > 0) {
                    idsurveyenv = surveyEnv.get(0);
                }


                fragmentCekLingkunganDetail = FragmentCekLingkunganDetail.newInstance(Constans.MENU_PERSONAL, idsurveyenv, surveyId, Constans.PARAM_WS_PERSONAL);

                loadFragment(fragmentCekLingkunganDetail);
            }
        });

        buttonCekLingkungan1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan1.setEnabled(false);
                buttonCekLingkungan.setEnabled(true);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_personal));


                String idsurveyenv = null;
                if (surveyEnv != null
                        && surveyEnv.size() > 1) {
                    idsurveyenv = surveyEnv.get(1);
                }


                fragmentCekLingkunganDetail1 = FragmentCekLingkunganDetail1.newInstance(Constans.MENU_PERSONAL, idsurveyenv, surveyId, Constans.PARAM_WS_PERSONAL);

                loadFragment(fragmentCekLingkunganDetail1);
            }
        });

        buttonCekLingkungan2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan2.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_personal));


                String idsurveyenv = null;
                if (surveyEnv != null
                        && surveyEnv.size() > 2) {
                    idsurveyenv = surveyEnv.get(2);
                }


                fragmentCekLingkunganDetail2 = FragmentCekLingkunganDetail2.newInstance(Constans.MENU_PERSONAL, idsurveyenv, surveyId, Constans.PARAM_WS_PERSONAL);

                loadFragment(fragmentCekLingkunganDetail2);
            }
        });

        buttonCekLingkungan3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan3.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_personal));


                String idsurveyenv = null;
                if (surveyEnv != null
                        && surveyEnv.size() > 3) {
                    idsurveyenv = surveyEnv.get(3);
                }


                fragmentCekLingkunganDetail3 = FragmentCekLingkunganDetail3.newInstance(Constans.MENU_PERSONAL, idsurveyenv, surveyId, Constans.PARAM_WS_PERSONAL);

                loadFragment(fragmentCekLingkunganDetail3);
            }
        });

        buttonBack.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
        buttonNext.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivitySurveyDetail.this, ActivityCompanyFinancialDetail.class);
                intent.putExtra(Constans.KEY_MENU, Constans.MENU_PERSONAL);
                intent.putExtra(Constans.KeySharedPreference.ORDER_ID, orderId);
                startActivity(intent);
            }
        });


    }

    private void setFormForCompany() {
        getListidEnvi(orderId, Constans.PARAM_WS_COMPANY);
        titleBar.setBackgroundColor(getResources().getColor(R.color.color_company));
        titleBar.setText(getString(R.string.header_survey_company_form));

        buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

        buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

        buttonNext.setText(getString(R.string.btn_next));

        buttonDataSurvey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonDataSurvey.setEnabled(false);
                buttonCekLingkungan.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_company));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_company));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                fragmentSurveyDetail = FragmentSurveyDetail.newInstance(Constans.MENU_COMPANY, Constans.PARAM_WS_COMPANY, orderId);

                loadFragment(fragmentSurveyDetail);
            }
        });

        buttonCekLingkungan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_company));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_company));

                String idsurveyenv = null;
                if (surveyEnv != null
                        && surveyEnv.size() > 0) {
                    idsurveyenv = surveyEnv.get(0);
                }


                fragmentCekLingkunganDetail = FragmentCekLingkunganDetail.newInstance(Constans.MENU_COMPANY, idsurveyenv, surveyId, Constans.PARAM_WS_COMPANY);

                loadFragment(fragmentCekLingkunganDetail);
            }
        });

        buttonCekLingkungan1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan1.setEnabled(false);
                buttonCekLingkungan.setEnabled(true);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_company));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_company));


                String idsurveyenv = null;
                if (surveyEnv != null
                        && surveyEnv.size() > 1) {
                    idsurveyenv = surveyEnv.get(1);
                }

                fragmentCekLingkunganDetail1 = FragmentCekLingkunganDetail1.newInstance(Constans.MENU_COMPANY, idsurveyenv, surveyId, Constans.PARAM_WS_COMPANY);

                loadFragment(fragmentCekLingkunganDetail1);
            }
        });

        buttonCekLingkungan2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan2.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan3.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_company));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_company));


                String idsurveyenv = null;
                if (surveyEnv != null
                        && surveyEnv.size() > 2) {
                    idsurveyenv = surveyEnv.get(2);
                }


                fragmentCekLingkunganDetail2 = FragmentCekLingkunganDetail2.newInstance(Constans.MENU_COMPANY, idsurveyenv, surveyId, Constans.PARAM_WS_COMPANY);

                loadFragment(fragmentCekLingkunganDetail2);
            }
        });

        buttonCekLingkungan3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonCekLingkungan3.setEnabled(false);
                buttonDataSurvey.setEnabled(true);
                buttonCekLingkungan2.setEnabled(true);
                buttonCekLingkungan1.setEnabled(true);
                buttonCekLingkungan.setEnabled(true);

                buttonDataSurvey.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonDataSurvey.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan1.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan1.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan2.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
                buttonCekLingkungan2.setTextColor(getResources().getColor(R.color.color_white));

                buttonCekLingkungan3.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_company));
                buttonCekLingkungan3.setTextColor(getResources().getColor(R.color.color_company));


                String idsurveyenv = null;
                if (surveyEnv != null
                        && surveyEnv.size() > 3) {
                    idsurveyenv = surveyEnv.get(3);
                }


                fragmentCekLingkunganDetail3 = FragmentCekLingkunganDetail3.newInstance(Constans.MENU_COMPANY, idsurveyenv, surveyId, Constans.PARAM_WS_COMPANY);

                loadFragment(fragmentCekLingkunganDetail3);
            }
        });

        buttonBack.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));
        buttonNext.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_company_button));

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivitySurveyDetail.this, ActivityCompanyFinancialDetail.class);
                intent.putExtra(Constans.KEY_MENU, Constans.MENU_COMPANY);
                intent.putExtra(Constans.KeySharedPreference.ORDER_ID, orderId);
                startActivity(intent);
            }
        });

    }

    public void getListidEnvi(final String idOrder, final String name) {
        if (InternetConnection.checkConnection(this)) {
            Call<ResponseListEnvironment> getList = apiInterface.getListEnvironment(name, idOrder);
            getList.enqueue(new Callback<ResponseListEnvironment>() {
                @Override
                public void onResponse(Call<ResponseListEnvironment> call, Response<ResponseListEnvironment> response) {
                    if (response.isSuccessful()) {
                        surveyEnv = response.body().getDdlFinance().getSurveyEnv();
                        for (int i = 0; i < surveyEnv.size(); i++) {
                            if (i == 1) {
                                buttonCekLingkungan1.setVisibility(View.VISIBLE);
                            } else if (i == 2) {
                                buttonCekLingkungan2.setVisibility(View.VISIBLE);
                            } else if (i == 3) {
                                buttonCekLingkungan3.setVisibility(View.VISIBLE);
                            }
                        }
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurveyDetail.this);
                        b.setTitle("Error Dialog");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server \n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getListidEnvi(idOrder, name);
                            }
                        });
                        b.show();
                    }
                    loadingDialog.dismiss();
                }

                @Override
                public void onFailure(Call<ResponseListEnvironment> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurveyDetail.this);
                    b.setTitle("Error Dialog");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server \n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getListidEnvi(idOrder, name);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(ActivitySurveyDetail.this);
            b.setTitle("Error Dialog");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server \n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getListidEnvi(idOrder, name);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }

    private void loadFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frame_survey_detail, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
