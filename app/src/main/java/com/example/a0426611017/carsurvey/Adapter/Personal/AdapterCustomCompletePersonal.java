package com.example.a0426611017.carsurvey.Adapter.Personal;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelGridListPersonal;
import com.example.a0426611017.carsurvey.Personal.DetailFullForm.ActivityPersonalDetail;
import com.example.a0426611017.carsurvey.Personal.DetailMiniForm.ActivityPersonalDetailOrder;
import com.example.a0426611017.carsurvey.R;

import java.util.List;

/**
 * Created by 0426611017 on 12/13/2017.
 */

public class AdapterCustomCompletePersonal extends BaseAdapter {
    private static List<ModelGridListPersonal> listPersonalGrid;
    private LayoutInflater mInflater;
    private Context context;

    private int tabCurrent;
    private int menu;

    public AdapterCustomCompletePersonal(Context ActivityListPersonalComplete, List<ModelGridListPersonal> results, int tab, int menu) {
        context = ActivityListPersonalComplete;
        listPersonalGrid = results;
        mInflater=LayoutInflater.from(ActivityListPersonalComplete);
        tabCurrent = tab;
        this.menu = menu;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        AdapterCustomCompletePersonal.ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_list_row, null);
            holder = new AdapterCustomCompletePersonal.ViewHolder();
            holder.personalDateTextField = convertView.findViewById(R.id.date);
            holder.personalNameTextField = convertView.findViewById(R.id.name);
            holder.personalNo_idTextField = convertView.findViewById(R.id.no_id);
            holder.personal_comment = convertView.findViewById(R.id.comment);
            holder.buttonDetail = convertView.findViewById(R.id.buttonDetail);
            holder.buttonViewMoreComment = convertView.findViewById(R.id.btn_comment);

            convertView.setTag(holder);
        } else {
            holder = (AdapterCustomCompletePersonal.ViewHolder) convertView.getTag();
        }


        holder.personalDateTextField.setText(listPersonalGrid.get(position).getDateCreate());
        holder.personalNameTextField.setText(listPersonalGrid.get(position).getFullName());
        holder.personalNo_idTextField.setText(listPersonalGrid.get(position).getIdNo());


        if(menu == Constans.MENU_PERSONAL_ORDER_FORM){
            if(tabCurrent == Constans.TAB_PENDING ) {
                if(listPersonalGrid.get(position).getComMent().equals("-")){
                    holder.buttonViewMoreComment.setVisibility(View.GONE);
                    holder.personal_comment.setText("");
                }
                else{
                    holder.personal_comment.setText(listPersonalGrid.get(position).getComMent());
                    holder.personal_comment.setTextColor(context.getResources().getColor(R.color.color_company));
                }
            }else {
                holder.personal_comment.setText("");
                holder.buttonViewMoreComment.setVisibility(View.GONE);
            }
        }


        if(menu == Constans.MENU_PERSONAL_FULL_FORM){
            if(tabCurrent == Constans.TAB_CHECK_HQ) {
                if(listPersonalGrid.get(position).getComMent().equals("-")){
                    holder.personal_comment.setText("");
                    holder.buttonViewMoreComment.setVisibility(View.GONE);
                }
                else{
                    holder.personal_comment.setText(listPersonalGrid.get(position).getComMent());
                    holder.personal_comment.setTextColor(context.getResources().getColor(R.color.color_company));
                }
            }else {
                holder.personal_comment.setText("");
                holder.buttonViewMoreComment.setVisibility(View.GONE);
            }
        }

        holder.buttonViewMoreComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.custom_dialog);
                TextView headerText = dialog.findViewById(R.id.text_view_comment_title);
                TextView contentText = dialog.findViewById(R.id.text_view_comment_message);
                ImageView close = dialog.findViewById(R.id.btn_close_comment);
                TextView commentName = dialog.findViewById(R.id.text_view_comment_name);
                commentName.setText(listPersonalGrid.get(position).getFullName());
                headerText.setText("Comment");
                contentText.setText(listPersonalGrid.get(position).getComMent());
                close.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.setCancelable(true);
                dialog.setCanceledOnTouchOutside(true);
                dialog.create();
                dialog.show();
            }
        });

        holder.buttonDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                Intent i;
                SharedPreferences.Editor editor;
                if((tabCurrent == Constans.TAB_CLEAR && menu == Constans.MENU_PERSONAL_FULL_FORM) || menu == Constans.MENU_PERSONAL_FULL_FORM ){
                    i = new Intent(context,ActivityPersonalDetail.class);
                    editor = context.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_CUST_DATA_ID, listPersonalGrid.get(position).getCustDataId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SURVEY_ID, listPersonalGrid.get(position).getSurveyId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_ID, listPersonalGrid.get(position).getGuarantorCustId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.PERSONAL_KK_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_PERSONAL_KK_ID, listPersonalGrid.get(position).getKkNo());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.PERSONAL_KK_IMAGE, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_PERSONAL_KK_IMAGE, listPersonalGrid.get(position).getKkImgUrl());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_KK_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_KK_ID, listPersonalGrid.get(position).getKkNoSG());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_KK_IMAGE, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_KK_IMAGE, listPersonalGrid.get(position).getkkImgURLSG());
                    editor.apply();
                }else if(tabCurrent == Constans.TAB_CLEAR && menu == Constans.MENU_PERSONAL_ORDER_FORM){
                    i = new Intent(context,ActivityPersonalDetailOrder.class);
                    editor = context.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_CUST_DATA_ID, listPersonalGrid.get(position).getCustDataId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SURVEY_ID, listPersonalGrid.get(position).getSurveyId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_ID, listPersonalGrid.get(position).getGuarantorCustId());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.PERSONAL_KK_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_PERSONAL_KK_ID, listPersonalGrid.get(position).getKkNo());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.PERSONAL_KK_IMAGE, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_PERSONAL_KK_IMAGE, listPersonalGrid.get(position).getKkImgUrl());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_KK_ID, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_KK_ID, listPersonalGrid.get(position).getKkNoSG());
                    editor.apply();

                    editor = context.getSharedPreferences(Constans.KeySharedPreference.SPOUSE_KK_IMAGE, Context.MODE_PRIVATE).edit();
                    editor.putString(Constans.KEY_SPOUSE_KK_IMAGE, listPersonalGrid.get(position).getkkImgURLSG());
                    editor.apply();
                }else{
                    i = new Intent(context,ActivityPersonalDetailOrder.class);
                }
                i.putExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID, listPersonalGrid.get(position).getPersonalOrderId());
                context.startActivity(i);
            }
        });
        return convertView;
    }

    public class ViewHolder {
        TextView personalDateTextField;
        TextView personalNameTextField;
        TextView personalNo_idTextField;
        TextView personal_comment;
        Button buttonDetail;
        Button buttonViewMoreComment;
    }

    @Override
    public int getCount() {return listPersonalGrid.size();
    }

    @Override
    public Object getItem(int arg0) {
        return listPersonalGrid.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }


}
