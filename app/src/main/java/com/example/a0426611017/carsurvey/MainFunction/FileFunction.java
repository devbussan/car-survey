package com.example.a0426611017.carsurvey.MainFunction;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by c201017001 on 10/01/2018.
 */

public class FileFunction {

    private String nameFile;
    private String filePath;
    private String base64;
    private File defaultPathPictureBAF = new File(Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_PICTURES), "BAF");

    public File getDefaultPathPictureBAF() {
        return defaultPathPictureBAF;
    }

    public String getNameFile() {
        return nameFile;
    }

    public String getBase64() {
        return base64;
    }

    public String getFilePath() {
        return filePath;
    }

    File getFilePictureBAF(String fileName) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "BAF");

        return new File(mediaStorageDir + File.separator + fileName);
    }

    File createFilePictureBAF() {
        Log.i("Directory", "Path : " + defaultPathPictureBAF.getPath() + " || Absolute Path :" + defaultPathPictureBAF.getAbsolutePath());
        if (!defaultPathPictureBAF.exists()) {
            if (!defaultPathPictureBAF.mkdirs()) {
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        nameFile = "IMG_" + timeStamp + ".jpg";
        filePath = defaultPathPictureBAF.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg";
//        base64 = createBase64(filePath);
        return new File(filePath);
    }

    private String getRealPathFromURI(Uri contentUri, Context context) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            ;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    public File copyFileFromUriToDirPictureBAF(Uri uri, Context context) throws IOException {

        File srcFile = new File(getRealPathFromURI(uri, context));
        return this.copyFileToDirPictureBAF(srcFile);
    }

    private File copyFileToDirPictureBAF(File srcFile) throws IOException {

        File ret = createFilePictureBAF();
        FileUtils.copyFile(srcFile, ret);
        return ret;
    }

    public String createBase64(String fileFullPath) throws IOException {
        File file = new File(defaultPathPictureBAF.getPath() + File.separator + fileFullPath);
        byte[] byteArrayImage = new byte[(int) file.length()];
        FileInputStream fis = new FileInputStream(file);
        fis.read(byteArrayImage);
        fis.close();
        return Base64.encodeToString(byteArrayImage, Base64.DEFAULT);
    }

    public boolean deleteAllFileInDir() {
        if(new File(defaultPathPictureBAF.getPath()).exists()) {
            try {
                FileUtils.cleanDirectory(new File(defaultPathPictureBAF.getPath()));
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;

    }

}
