package com.example.a0426611017.carsurvey.Personal;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.AddNew.Personal.ActivityNewOrderPersonal;
import com.example.a0426611017.carsurvey.Fragment.Personal.FragmentDataPersonalFullForm;
import com.example.a0426611017.carsurvey.Fragment.Personal.FragmentPersonalOrderForm;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ActivityPersonalTab extends Fragment {

    private View view;
    private String search;
    private int tabCurrent;
    private int menu;
    private Context context;

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private PagerAdapter adapter;

    private TextView textPeriode;

    SharedPreferences sharedpreferences;
    private Constans constans = new Constans();

    private String periode;

    public ActivityPersonalTab loadSearch(String search, int tabCurrent, int menu) {
        ActivityPersonalTab activityPersonal = new ActivityPersonalTab();
        Bundle args = new Bundle();
        args.putString("search", search);
        args.putInt(Constans.KEY_TAB, tabCurrent);
        args.putInt("menu", menu);
        activityPersonal.setArguments(args);
        return activityPersonal;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = container.getContext();
        view = inflater.inflate(R.layout.activity_main_tab, container, false);
        search = getArguments().getString("search");
        sharedpreferences = context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, Context.MODE_PRIVATE);

        tabCurrent = getArguments().getInt(Constans.KEY_TAB);
        if(tabCurrent == -1){
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putInt(Constans.KEY_TAB, 0);
            editor.apply();
            editor.commit();
        }

        Log.d(Constans.KEY_TAB, "Tab Current : "+tabCurrent);

        menu = getArguments().getInt("menu", 0);

        viewPager = view.findViewById(R.id.pager);
        textPeriode = view.findViewById(R.id.periode_grid);

        DateFormat dateFormat = new SimpleDateFormat("MMMM yyyy", new Locale("id"));
        DateFormat dateFormat2 = new SimpleDateFormat("MMMM yyyy", new Locale("id"));
        Date date1 = new Date();
        Log.d("Info", "Date Now : "+date1);
        String monthNow = dateFormat.format(date1);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -2);
        Date date2 = cal.getTime();
        Log.d("Info", "Date - 90 : "+date2);
        String monthYesterday = "";
        if(date2.getYear() != date1.getYear()){
            monthYesterday = dateFormat2.format(date2);
        }else{
            monthYesterday = dateFormat2.format(date2).split(" ")[0];
        }

        periode = getString(R.string.text_dashboard_periode) + " " + monthYesterday + " - " + monthNow;
        textPeriode.setText(periode);

        ImageView actionButtonNext = view.findViewById(R.id.floatingActionGoNext);
        tabLayout = view.findViewById(R.id.tab_layout);
        switch (menu){
            case Constans.MENU_PERSONAL_FULL_FORM:
                actionButtonNext.setImageResource(R.drawable.icon_order);
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_INPUT));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_CHECKHQ));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_RETURN));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_REJECT));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_APPROVE));
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                tabLayout.setBackgroundColor(getResources().getColor(R.color.color_personal));
                tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.color_company));

                break;
            case Constans.MENU_PERSONAL_ORDER_FORM:
                actionButtonNext.setImageResource(R.drawable.icon_full);
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_DRAFT));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_PENDING));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_RETURN));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_REJECT));
                tabLayout.addTab(tabLayout.newTab().setText(Constans.TAB_TEXT_CLEAR));
                tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                tabLayout.setBackgroundColor(getResources().getColor(R.color.color_personal));
                tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.color_company));
                Log.d("testing","tab order form");
                break;
            default:
                break;
        }

        if(tabCurrent != -1){
            TabLayout.Tab tab = tabLayout.getTabAt(tabCurrent);
            tab.select();
            setCurrentTab(tabCurrent);
        }else{
            setCurrentTab(0);
        }

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabCurrent = tab.getPosition();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putInt(Constans.KEY_TAB, tabCurrent);
                editor.apply();
                editor.commit();
                setCurrentTab(tabCurrent);

            }


            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        FloatingActionButton actionButtonAddNew = view.findViewById(R.id.floatingAddNew);
        actionButtonAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ActivityNewOrderPersonal.class);
                intent.putExtra(Constans.KEY_IS_NEW, true);
                startActivity(intent);
            }
        });
        actionButtonAddNew.setBackgroundColor(getResources().getColor(R.color.color_personal));

        actionButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(menu == Constans.MENU_PERSONAL_ORDER_FORM){
                    Intent intent = new Intent(context, MainActivityDrawer.class);
                    intent.putExtra(Constans.KEY_MENU, Constans.MENU_PERSONAL_FULL_FORM);
                    startActivity(intent);
                }else {
                    Intent intent = new Intent(context, MainActivityDrawer.class);
                    intent.putExtra(Constans.KEY_MENU, Constans.MENU_PERSONAL_ORDER_FORM);
                    startActivity(intent);
                }
            }
        });

        return view;
    }

    private void setCurrentTab(int position){
        if(menu == Constans.MENU_PERSONAL_FULL_FORM){
            if(position == Constans.TAB_REJECT_ORDER || position == Constans.TAB_COMPLETE){
                textPeriode.setVisibility(View.VISIBLE);
            }else{
                textPeriode.setVisibility(View.GONE);
            }
            replaceFragment(new FragmentDataPersonalFullForm().newInstance(search, position, menu));
        }else{
            if(position == Constans.TAB_REJECT_FULL){
                textPeriode.setVisibility(View.VISIBLE);
            }else{
                textPeriode.setVisibility(View.GONE);
            }
            replaceFragment(new FragmentPersonalOrderForm().newInstance(search, position, menu));
        }
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }
}
