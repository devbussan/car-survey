package com.example.a0426611017.carsurvey.Object.Company.DetailMiniForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;

/**
 * Created by c201017001 on 14/02/2018.
 */

public class ObjectContactInfo4DetailOrder {

    private View view;
    private ViewGroup container;
    private LayoutInflater inflater;
    private Context context;

    private TextView valueName, valueJobPosition, valueIdNum, valueIdImage, valueIdNpwp, valueIdNpwpImage,
            valueMobilePhone1, valueMobilePhone2, valuePhone1, valuePhone2,
            valueEmail1, valueEmail2;
    
    public ObjectContactInfo4DetailOrder(ViewGroup container, LayoutInflater inflater) {
        this.container = container;
        this.inflater = inflater;
        this.view = this.inflater.inflate(R.layout.fragment_contact_info4_detail_order, container, false);
        this.context = this.container.getContext();
        setField();
    }

    public View getView() {
        return view;
    }

    private void setField(){
        valueName = view.findViewById(R.id.value_name_detail_order_contact_info4);
        valueJobPosition = view.findViewById(R.id.value_job_position_detail_order_contact_info4);
        valueIdNum = view.findViewById(R.id.value_id_num_detail_order_contact_info4);
        valueIdImage = view.findViewById(R.id.value_id_image_detail_order_contact_info4);
        valueIdNpwp = view.findViewById(R.id.value_id_npwp_detail_order_contact_info4);
        valueIdNpwpImage = view.findViewById(R.id.value_id_npwp_image_detail_order_contact_info4);
        valueMobilePhone1 = view.findViewById(R.id.value_mobile_phone1_detail_order_contact_info4);
        valueMobilePhone2 = view.findViewById(R.id.value_mobile_phone2_detail_order_contact_info4);
        valuePhone1 = view.findViewById(R.id.value_phone1_detail_order_contact_info4);
        valuePhone2 = view.findViewById(R.id.value_phone2_detail_order_contact_info4);
        valueEmail1 = view.findViewById(R.id.value_email1_detail_order_contact_info4);
        valueEmail2 = view.findViewById(R.id.value_email2_detail_order_contact_info4);
    }

    public void setForm(ModelDetailContactInfo contactInfoMini){
        valueName.setText(contactInfoMini.getContactName());
        valueJobPosition.setText(contactInfoMini.getJobPosition());
        valueIdNum.setText(contactInfoMini.getIdNumber());
        if(contactInfoMini.getIdImageUrl() != null){
            if(!contactInfoMini.getIdImageUrl().equals("")){
                valueIdImage.setText(contactInfoMini.getIdImageUrl().split("/")[6]);
            }else{
                valueIdImage.setText(contactInfoMini.getIdImageUrl());
            }
        }else{
            valueIdImage.setText(contactInfoMini.getIdImageUrl());
        }

        valueIdNpwp.setText(contactInfoMini.getNpwp());

        if(contactInfoMini.getNpwpImageUrl() != null){
            if(!contactInfoMini.getNpwpImageUrl().equals("")){
                valueIdNpwpImage.setText(contactInfoMini.getNpwpImageUrl().split("/")[6]);
            }else{
                valueIdNpwpImage.setText(contactInfoMini.getNpwpImageUrl());
            }
        }else{
            valueIdNpwpImage.setText(contactInfoMini.getNpwpImageUrl());
        }
        valueMobilePhone1.setText(contactInfoMini.getMobilePhone1());
        valueMobilePhone2.setText(contactInfoMini.getMobilePhone2());
        valuePhone1.setText(contactInfoMini.getPhone1());
        valuePhone2.setText(contactInfoMini.getPhone2());
        valueEmail1.setText(contactInfoMini.getEmail1());
        valueEmail2.setText(contactInfoMini.getEmail2());

        PreviewImage previewImage = new PreviewImage();
        previewImage.setPreviewImage(contactInfoMini.getIdImageUrl(), context, valueIdImage);
        previewImage.setPreviewImage(contactInfoMini.getNpwpImageUrl(), context, valueIdNpwpImage);
    }
}
