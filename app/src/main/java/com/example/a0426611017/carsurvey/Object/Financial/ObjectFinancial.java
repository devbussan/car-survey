package com.example.a0426611017.carsurvey.Object.Financial;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Currency;
import com.example.a0426611017.carsurvey.Model.Financial.ModelDDLFinance;
import com.example.a0426611017.carsurvey.Model.Financial.ModelFinance;
import com.example.a0426611017.carsurvey.R;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by 0414831216 on 2/12/2018.
 */

public class ObjectFinancial {

    private LayoutInflater inflater;
    private View view;
    private Context context;
    private ModelDDLFinance modelDDLFinance;

    private TextView labelNamaDealer, labelMerek, labelTypePembiayaan, labelSeri, labelWarna,
            labelAsetCondition, labelNamaBpkb, labelBpkbCityIssued, labelOtr, labelCashback,
            labelDpPercent, labelDpAmount, labelTenor, labelPaymentRequest, labelAngsuran,
            labelAsuransi, labelRate, labelAdmin, labelProvisi, labelPembayaran, labelCustNotifikasi;

    private EditText EditTextNamaDealer, EditTextMerek, EditTextTypePembiayaan, EditTextSeri,
            EditTextWarna, EditTextAsetCondition, EditTextNamaBpkb, EditTextBpkbCityIssued,
            EditTextOtr, EditTextCashback, EditTextDpPercent, EditTextDpAmount, EditTextTenor,
            EditTextAngsuran, EditTextRate, EditTextAdmin,
            EditTextProvisi, EditTextPembiayaanId;

    private Spinner SpinnerPaymentRequest, SpinnerAsuransi, SpinnerPembayaran, SpinnerCustNotifikasi;

    private DecimalFormat decimalFormat = new DecimalFormat(".##");

    public ObjectFinancial(ViewGroup container, LayoutInflater inflater, ModelDDLFinance modelDDLFinance) {

        this.inflater = inflater;
        this.context = container.getContext();
        this.modelDDLFinance = modelDDLFinance;
        this.view = this.inflater.inflate(R.layout.fragment_financial, container, false);
        setField();
        setFieldLabel();
    }


    public View getView() {
        return view;
    }

    private void setFieldLabel() {
        labelNamaDealer = view.findViewById(R.id.label_nama_dealer);
        labelMerek = view.findViewById(R.id.label_merek);
        labelTypePembiayaan = view.findViewById(R.id.label_type_pembiayaan);
        labelSeri = view.findViewById(R.id.label_seri);
        labelWarna = view.findViewById(R.id.label_warna);
        labelAsetCondition = view.findViewById(R.id.label_aset_condition);
        labelNamaBpkb = view.findViewById(R.id.label_nama_bpkb);
        labelBpkbCityIssued = view.findViewById(R.id.label_bpkb_city_issued);
        labelOtr = view.findViewById(R.id.label_otr);
        labelCashback = view.findViewById(R.id.label_cashback);
        labelDpPercent = view.findViewById(R.id.label_dp_percent);
        labelDpAmount = view.findViewById(R.id.label_dp_amount);
        labelTenor = view.findViewById(R.id.label_tenor);
        labelPaymentRequest = view.findViewById(R.id.label_payment_request);
        labelAngsuran = view.findViewById(R.id.label_angsuran);
        labelAsuransi = view.findViewById(R.id.label_asuransi);
        labelRate = view.findViewById(R.id.label_rate);
        labelAdmin = view.findViewById(R.id.label_admin);
        labelProvisi = view.findViewById(R.id.label_provisi);
        labelPembayaran = view.findViewById(R.id.label_pembayaran);
        labelCustNotifikasi = view.findViewById(R.id.label_cust_notifikasi);
    }

    public void setField() {
        EditTextNamaDealer = view.findViewById(R.id.edit_text_nama_dealer);
        EditTextMerek = view.findViewById(R.id.edit_text_merek);
        EditTextTypePembiayaan = view.findViewById(R.id.edit_text_type_pembiayaan);
        EditTextSeri = view.findViewById(R.id.edit_text_seri);
        EditTextWarna = view.findViewById(R.id.edit_text_warna);
        EditTextAsetCondition = view.findViewById(R.id.edit_text_aset_condition);
        EditTextNamaBpkb = view.findViewById(R.id.edit_text_nama_bpkb);
        EditTextBpkbCityIssued = view.findViewById(R.id.edit_text_bpkb_city_issued);
        EditTextOtr = view.findViewById(R.id.edit_text_otr);
        EditTextOtr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                if (!EditTextDpPercent.getText().toString().equals("") && !s.toString().equals("")) {
                if (!s.toString().equals("")) {
                    EditTextOtr.removeTextChangedListener(this);

                    String formatted = Currency.set(EditTextOtr);
                    EditTextOtr.setText(formatted.replaceAll("[$]",""));
                    EditTextOtr.setSelection(formatted.length()-1);
                    EditTextOtr.addTextChangedListener(this);

                    if(!EditTextDpPercent.getText().toString().equals("")){
                        double assetPrice = Double.parseDouble(EditTextOtr.getText().toString().replaceAll("[,]",""));
                        double dpPercent = Double.parseDouble(EditTextDpPercent.getText().toString());
                        double result = assetPrice * (dpPercent / 100);

                        Log.d("Info","assetPrice : "+assetPrice);
                        Log.d("Info","dpPercent: "+dpPercent);
                        Log.d("Info","result : "+result);

                        formatted = Currency.set(String.valueOf(result));
                        EditTextDpAmount.setText(formatted.replaceAll("[$]",""));
                    }

                } else {
                    EditTextDpAmount.setText("0");
                }
            }
        });
        EditTextCashback = view.findViewById(R.id.edit_text_cashback);
        EditTextCashback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")){
                    EditTextCashback.removeTextChangedListener(this);
                    String formatted = Currency.set(EditTextCashback);
                    EditTextCashback.setText(formatted.replaceAll("[$]",""));
                    EditTextCashback.setSelection(formatted.length()-1);
                    EditTextCashback.addTextChangedListener(this);
                }
            }
        });
        EditTextDpPercent = view.findViewById(R.id.edit_text_dp_percent);
        EditTextDpPercent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
//                if (!EditTextOtr.getText().toString().equals("") && !s.toString().equals("")) {
                if (!s.toString().equals("")) {
                    double assetPrice = Double.parseDouble(EditTextOtr.getText().toString().replaceAll("[,]",""));
                    double dpPercent = Double.parseDouble(EditTextDpPercent.getText().toString());
                    double result = assetPrice * (dpPercent / 100);

                    Log.d("Info","assetPrice : "+assetPrice);
                    Log.d("Info","dpPercent: "+dpPercent);
                    Log.d("Info","result : "+result);

                    String formatted = Currency.set(String.valueOf(result));
                    EditTextDpAmount.setText(formatted.replaceAll("[$]",""));
                } else {
                    EditTextDpAmount.setText("0");
                }
            }
        });
        EditTextDpAmount = view.findViewById(R.id.edit_text_dp_amount);
        EditTextDpAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")){
                    EditTextDpAmount.removeTextChangedListener(this);

                    String formatted = Currency.set(EditTextDpAmount);
                    EditTextDpAmount.setText(formatted.replaceAll("[$]",""));
                    EditTextDpAmount.setSelection(formatted.length()-1);
                    EditTextDpAmount.addTextChangedListener(this);
                }
            }
        });
        EditTextTenor = view.findViewById(R.id.edit_text_tenor);
        EditTextAngsuran = view.findViewById(R.id.edit_text_angsuran);
        EditTextAngsuran.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")){
                    EditTextAngsuran.removeTextChangedListener(this);

                    String formatted = Currency.set(EditTextAngsuran);
                    EditTextAngsuran.setText(formatted.replaceAll("[$]",""));
                    EditTextAngsuran.setSelection(formatted.length()-1);
                    EditTextAngsuran.addTextChangedListener(this);
                }
            }
        });
        EditTextRate = view.findViewById(R.id.edit_text_rate);
        EditTextAdmin = view.findViewById(R.id.edit_text_admin);
        EditTextAdmin.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")){
                    EditTextAdmin.removeTextChangedListener(this);

                    String formatted = Currency.set(EditTextAdmin);
                    EditTextAdmin.setText(formatted.replaceAll("[$]",""));
                    EditTextAdmin.setSelection(formatted.length()-1);
                    EditTextAdmin.addTextChangedListener(this);
                }
            }
        });
        EditTextProvisi = view.findViewById(R.id.edit_text_provisi);
        EditTextPembiayaanId = view.findViewById(R.id.edit_text_pembiayaan_id);
        SpinnerPaymentRequest = view.findViewById(R.id.spinner_payment_request);
        SpinnerAsuransi = view.findViewById(R.id.spinner_asuransi);
        SpinnerPembayaran = view.findViewById(R.id.spinner_pembayaran);
        SpinnerCustNotifikasi = view.findViewById(R.id.spinner_cust_notifikasi);
        if (modelDDLFinance != null) {
            SpinnerPaymentRequest.setAdapter(setAdapterItemList(modelDDLFinance.getPayment_freq()));
            SpinnerAsuransi.setAdapter(setAdapterItemList(modelDDLFinance.getAsurnsi()));
            SpinnerPembayaran.setAdapter(setAdapterItemList(modelDDLFinance.getPembayaran()));
            SpinnerCustNotifikasi.setAdapter(setAdapterItemList(modelDDLFinance.getCustomer_notofikasi()));
        }

    }

    public void setFormForCompany() {
        labelNamaDealer.setTextColor(context.getResources().getColor(R.color.color_company));
        labelMerek.setTextColor(context.getResources().getColor(R.color.color_company));
        labelTypePembiayaan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelSeri.setTextColor(context.getResources().getColor(R.color.color_company));
        labelWarna.setTextColor(context.getResources().getColor(R.color.color_company));
        labelAsetCondition.setTextColor(context.getResources().getColor(R.color.color_company));
        labelNamaBpkb.setTextColor(context.getResources().getColor(R.color.color_company));
        labelBpkbCityIssued.setTextColor(context.getResources().getColor(R.color.color_company));
        labelOtr.setTextColor(context.getResources().getColor(R.color.color_company));
        labelCashback.setTextColor(context.getResources().getColor(R.color.color_company));
        labelDpPercent.setTextColor(context.getResources().getColor(R.color.color_company));
        labelDpAmount.setTextColor(context.getResources().getColor(R.color.color_company));
        labelTenor.setTextColor(context.getResources().getColor(R.color.color_company));
        labelPaymentRequest.setTextColor(context.getResources().getColor(R.color.color_company));
        labelAngsuran.setTextColor(context.getResources().getColor(R.color.color_company));
        labelAsuransi.setTextColor(context.getResources().getColor(R.color.color_company));
        labelRate.setTextColor(context.getResources().getColor(R.color.color_company));
        labelAdmin.setTextColor(context.getResources().getColor(R.color.color_company));
        labelProvisi.setTextColor(context.getResources().getColor(R.color.color_company));
        labelPembayaran.setTextColor(context.getResources().getColor(R.color.color_company));
        labelCustNotifikasi.setTextColor(context.getResources().getColor(R.color.color_company));
    }

    public void setFormForPersonal() {
        labelNamaDealer.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelMerek.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelTypePembiayaan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelSeri.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelWarna.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelAsetCondition.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelNamaBpkb.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelBpkbCityIssued.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelOtr.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelCashback.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelDpPercent.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelDpAmount.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelTenor.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelPaymentRequest.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelAngsuran.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelAsuransi.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelRate.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelAdmin.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelProvisi.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelPembayaran.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelCustNotifikasi.setTextColor(context.getResources().getColor(R.color.color_personal));
    }

    private ArrayAdapter<String> setAdapterItemList(List list) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl, list);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;

    }

    public ModelFinance getForm() {
        ModelFinance modelFinance = new ModelFinance();

        modelFinance.setNamaDealer(EditTextNamaDealer.getText().toString());

        modelFinance.setMerk(EditTextMerek.getText().toString());

        modelFinance.setTypePembiayaan(EditTextTypePembiayaan.getText().toString());

        modelFinance.setSerial(EditTextSeri.getText().toString());


        modelFinance.setWarna(EditTextWarna.getText().toString());

        modelFinance.setAsetCondition(EditTextAsetCondition.getText().toString());

        modelFinance.setNamaBpkb(EditTextNamaBpkb.getText().toString());

        modelFinance.setBpkbCityIssued(EditTextBpkbCityIssued.getText().toString());

        modelFinance.setOtr(EditTextOtr.getText().toString().replaceAll("[,]", ""));

        modelFinance.setCashback(EditTextCashback.getText().toString().replaceAll("[,]", ""));

        modelFinance.setDpPercent(EditTextDpPercent.getText().toString());

        modelFinance.setDpAmount(EditTextDpAmount.getText().toString().replaceAll("[,]", ""));

        modelFinance.setTenor(EditTextTenor.getText().toString());

        modelFinance.setAngsuran(EditTextAngsuran.getText().toString().replaceAll("[,]", ""));

        modelFinance.setRate(EditTextRate.getText().toString());

        modelFinance.setPaymentRequest(SpinnerPaymentRequest.getSelectedItem().toString());
        modelFinance.setAsuransi(SpinnerAsuransi.getSelectedItem().toString());
        modelFinance.setPembayaran(SpinnerPembayaran.getSelectedItem().toString());
        modelFinance.setCustNotifikasi(SpinnerCustNotifikasi.getSelectedItem().toString());
        modelFinance.setAdmin(EditTextAdmin.getText().toString());
        modelFinance.setProvisi(EditTextProvisi.getText().toString());
        modelFinance.setModified(true);
        return modelFinance;
    }

    public void setForm(ModelFinance modelFinance) {
        EditTextNamaDealer.setText(modelFinance.getNamaDealer());
        EditTextMerek.setText(modelFinance.getMerk());
        EditTextTypePembiayaan.setText(modelFinance.getTypePembiayaan());
        EditTextSeri.setText(modelFinance.getSerial());
        EditTextWarna.setText(modelFinance.getWarna());
        EditTextAsetCondition.setText(modelFinance.getAsetCondition());
        EditTextNamaBpkb.setText(modelFinance.getNamaBpkb());
        EditTextBpkbCityIssued.setText(modelFinance.getBpkbCityIssued());
        EditTextOtr.setText(modelFinance.getOtr());
        EditTextCashback.setText(modelFinance.getCashback());
        EditTextDpPercent.setText(modelFinance.getDpPercent().split("[.]")[0]);
        EditTextDpAmount.setText(modelFinance.getDpAmount());
        EditTextTenor.setText(modelFinance.getTenor());
        EditTextAngsuran.setText(modelFinance.getAngsuran());
        EditTextRate.setText(modelFinance.getRate());
        EditTextAdmin.setText(modelFinance.getAdmin());
        EditTextProvisi.setText(modelFinance.getProvisi());
        EditTextPembiayaanId.setText(modelFinance.getPembiayaanId());
        if (modelDDLFinance != null) {
            SpinnerPaymentRequest.setSelection(modelDDLFinance.getPayment_freq().indexOf(modelFinance.getPaymentRequest()));
            SpinnerAsuransi.setSelection(modelDDLFinance.getAsurnsi().indexOf(modelFinance.getAsuransi()));
            SpinnerPembayaran.setSelection(modelDDLFinance.getPembayaran().indexOf(modelFinance.getPembayaran()));
            SpinnerCustNotifikasi.setSelection(modelDDLFinance.getCustomer_notofikasi().indexOf(modelFinance.getCustNotifikasi()));
        }

    }

    public ModelFinance getComplete() {
        ModelFinance modelFinance = getForm();

        modelFinance.setComplete(true);
        if (EditTextNamaDealer.getText().toString().length() == 0) {
            EditTextNamaDealer.setError("Required ");
            modelFinance.setComplete(false);
        } else if (EditTextNamaDealer.getText().toString().trim().length() == 0) {
            EditTextNamaDealer.setError("Whitespaces only detected");

        }

        if (EditTextMerek.getText().toString().length() == 0) {
            EditTextMerek.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextMerek.getText().toString().trim().length() == 0) {
            EditTextMerek.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextTypePembiayaan.getText().toString().length() == 0) {
            EditTextTypePembiayaan.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextTypePembiayaan.getText().toString().trim().length() == 0) {
            EditTextTypePembiayaan.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextSeri.getText().toString().length() == 0) {
            EditTextSeri.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextSeri.getText().toString().trim().length() == 0) {
            EditTextSeri.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }


        if (EditTextWarna.getText().toString().length() == 0) {
            EditTextSeri.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextWarna.getText().toString().trim().length() == 0) {
            EditTextWarna.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextAsetCondition.getText().toString().length() == 0) {
            EditTextAsetCondition.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextAsetCondition.getText().toString().trim().length() == 0) {
            EditTextAsetCondition.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextNamaBpkb.getText().toString().length() == 0) {
            EditTextNamaBpkb.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextNamaBpkb.getText().toString().trim().length() == 0) {
            EditTextNamaBpkb.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextBpkbCityIssued.getText().toString().length() == 0) {
            EditTextBpkbCityIssued.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextBpkbCityIssued.getText().toString().trim().length() == 0) {
            EditTextBpkbCityIssued.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextOtr.getText().toString().length() == 0) {
            EditTextOtr.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextOtr.getText().toString().trim().length() == 0) {
            EditTextOtr.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextCashback.getText().toString().length() == 0) {
            EditTextCashback.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextCashback.getText().toString().trim().length() == 0) {
            EditTextCashback.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextDpPercent.getText().toString().length() == 0) {
            EditTextDpPercent.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextDpPercent.getText().toString().trim().length() == 0) {
            EditTextDpPercent.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextDpAmount.getText().toString().length() == 0) {
            EditTextDpAmount.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextDpAmount.getText().toString().trim().length() == 0) {
            EditTextDpAmount.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextTenor.getText().toString().length() == 0) {
            EditTextTenor.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextTenor.getText().toString().trim().length() == 0) {
            EditTextTenor.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextAngsuran.getText().toString().length() == 0) {
            EditTextAngsuran.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextAngsuran.getText().toString().trim().length() == 0) {
            EditTextAngsuran.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }

        if (EditTextRate.getText().toString().length() == 0) {
            EditTextRate.setError("Required");
            modelFinance.setComplete(false);
        } else if (EditTextRate.getText().toString().trim().length() == 0) {
            EditTextRate.setError("Whitespaces only detected");
            modelFinance.setComplete(false);
        }
        return modelFinance;
    }


}
