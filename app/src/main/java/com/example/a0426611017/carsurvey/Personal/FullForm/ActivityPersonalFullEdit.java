package com.example.a0426611017.carsurvey.Personal.FullForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.a0426611017.carsurvey.AddNew.Personal.ActivityNewOrderPersonal;
import com.example.a0426611017.carsurvey.Finance.ActivityCompanyFinancial;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataAddressPending;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataEmergencyContactPending;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataFinancialPending;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataJobDataPending;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataMainDataPending;
import com.example.a0426611017.carsurvey.Fragment.Personal.FullForm.FragmentDataPersonalPending;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiPersonal;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataAddress;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataEmergencyContact;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataFinancial;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataJobData;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataMainData;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonal;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDDLPersonal;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityPersonalFullEdit extends FragmentActivity implements View.OnClickListener{
    private Fragment currentFragment;

    private FragmentDataPersonalPending fragmentDataPersonalPending = new FragmentDataPersonalPending();
    private FragmentDataAddressPending fragmentDataAddressPending   = new FragmentDataAddressPending();
    private FragmentDataMainDataPending fragmentDataMainDataPending = new FragmentDataMainDataPending();
    private FragmentDataJobDataPending fragmentDataJobDataPending = new FragmentDataJobDataPending();
    private FragmentDataEmergencyContactPending fragmentDataEmergencyContactPending = new FragmentDataEmergencyContactPending();
    private FragmentDataFinancialPending fragmentDataFinancialPending = new FragmentDataFinancialPending();

    private ModelDataPersonal modelDataPersonal = new ModelDataPersonal();
    private ModelDataAddress modelDataAddress   = new ModelDataAddress();
    private ModelDataMainData modelDataMainData = new ModelDataMainData();
    private ModelDataJobData modelDataJobData   = new ModelDataJobData();
    private ModelDataEmergencyContact modelDataEmergencyContact = new ModelDataEmergencyContact();
    private ModelDataFinancial modelDataFinancial = new ModelDataFinancial();

    private Button buttonPersonalInfo;
    private Button buttonContactInfo;
    private Button buttonOccupationInfo;
    private Button buttonEmergencyInfo;
    private Button buttonIncomeInfo;
    private Button buttonDataFinancial;
    private Button buttonSave;
    private Button buttonNext;
    private Button buttonBack;

    private boolean isNew = true;

    private ModelDDLPersonal ddlPersonalList;
    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private ApiPersonal apiPersonal;
    private String adminId = null;
    private String personalOrderId = null;
    private String name = "personal";
    private String sharedLogId = null;
    private String personalCustDataId = null;
    private String kkIdNo = null;
    private String kkImgUrl = null;

    private View currentView;
    private int menuCategory = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_personal_full_edit);
        window.setStatusBarColor(getResources().getColor(R.color.color_personal));
        currentView = this.findViewById(android.R.id.content);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading.........");
        loadingDialog.setCancelable(false);

        isNew = getIntent().getBooleanExtra(Constans.KEY_IS_NEW, true);

        apiPersonal = new ApiPersonal(currentView, ActivityPersonalFullEdit.this, loadingDialog);
        getDDLPersonal();

        adminId = this.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);
        personalOrderId = getIntent().getStringExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID);
        sharedLogId = this.getSharedPreferences(Constans.KeySharedPreference.LOG_ID, Context.MODE_PRIVATE).getString(Constans.KEY_LOG_ID, null);
        personalCustDataId = this.getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, Context.MODE_PRIVATE).getString(Constans.KEY_CUST_DATA_ID, null);
        kkIdNo = this.getSharedPreferences(Constans.KeySharedPreference.PERSONAL_KK_ID, Context.MODE_PRIVATE).getString(Constans.KEY_PERSONAL_KK_ID, null);
        kkImgUrl = this.getSharedPreferences(Constans.KeySharedPreference.PERSONAL_KK_IMAGE, Context.MODE_PRIVATE).getString(Constans.KEY_PERSONAL_KK_IMAGE, null);
        menuCategory = this.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, Context.MODE_PRIVATE).getInt(Constans.KEY_MENU_CATEGORY, 0);

        Log.d("Personal Order", "return"+personalOrderId);
        Log.d("Admin Id", "return"+adminId);
        Log.d("Log Id", "return"+sharedLogId);
        Log.d("Cust Data Id", "return"+personalCustDataId);
        Log.d("KK ID", "return "+kkIdNo);
        Log.d("KK IMG", "return "+kkImgUrl);

        buttonPersonalInfo = (Button) findViewById(R.id.button_personal_info);
        buttonPersonalInfo.setOnClickListener(this);

        buttonContactInfo = (Button) findViewById(R.id.button_contact_info);
        buttonContactInfo.setOnClickListener(this);

        buttonOccupationInfo = (Button) findViewById(R.id.button_occupation_info);
        buttonOccupationInfo.setOnClickListener(this);

        buttonIncomeInfo = (Button) findViewById(R.id.button_income_info);
        buttonIncomeInfo.setOnClickListener(this);

        buttonEmergencyInfo = (Button) findViewById(R.id.button_emergency_info);
        buttonEmergencyInfo.setOnClickListener(this);

        buttonDataFinancial = (Button) findViewById(R.id.button_data_financial);
        buttonDataFinancial.setOnClickListener(this);

        buttonSave = (Button) findViewById(R.id.button_save_personal);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPersonalFullEdit.this);
                builder.setTitle(getString(R.string.content_question_want_save_data));
                builder.setMessage(getString(R.string.content_information_save_and_exit));
                builder.setNegativeButton(getString(R.string.dialog_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                loadingDialog.dismiss();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int j) {
                        loadingDialog.show();
                        saveForm();

                        modelDataPersonal.setUsrcrt(adminId);
                        modelDataAddress.setUsrcrt(adminId);
                        modelDataMainData.setUsrcrt(adminId);
                        modelDataJobData.setUsrcrt(adminId);
                        modelDataFinancial.setUsrcrt(adminId);
                        modelDataEmergencyContact.setUsrcrt(adminId);

                        apiPersonal.saveFullFormPersonal(
                                modelDataPersonal,
                                modelDataAddress,
                                modelDataMainData,
                                modelDataJobData,
                                modelDataEmergencyContact,
                                modelDataFinancial,
                                personalOrderId,
                                name,
                                false,
                                personalCustDataId,
                                sharedLogId);
                    }
                });

                builder.show();
            }
        });

        buttonNext = (Button) findViewById(R.id.button_next_personal);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                saveForm();
                checkCompleteSaveAndNext();

                if(modelDataPersonal.isComplete()
                        && modelDataAddress.isComplete()
                        && modelDataMainData.isComplete()
                        && modelDataJobData.isComplete()
                        && modelDataEmergencyContact.isComplete()
                        && modelDataFinancial.isComplete()
                        ){
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPersonalFullEdit.this);
                    builder.setTitle(getString(R.string.content_question_are_you_sure_to_save));
                    builder.setMessage(getString(R.string.content_information_save_and_next));
                    builder.setNegativeButton(getString(R.string.dialog_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    loadingDialog.dismiss();
                                    Log.e("info", "NO");
                                }
                            });
                    builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.e("info", "YES");

                            modelDataPersonal.setUsrcrt(adminId);
                            modelDataAddress.setUsrcrt(adminId);
                            modelDataMainData.setUsrcrt(adminId);
                            modelDataJobData.setUsrcrt(adminId);
                            modelDataEmergencyContact.setUsrcrt(adminId);
                            modelDataFinancial.setUsrcrt(adminId);

                            apiPersonal.saveFullFormPersonal(
                                    modelDataPersonal,
                                    modelDataAddress,
                                    modelDataMainData,
                                    modelDataJobData,
                                    modelDataEmergencyContact,
                                    modelDataFinancial,
                                    personalOrderId,
                                    name,
                                    true,
                                    personalCustDataId,
                                    sharedLogId);
                        }
                    });
                    builder.show();
                    loadingDialog.dismiss();
                } else {
                    if (!modelDataPersonal.isComplete()) {
                        if (buttonPersonalInfo.isEnabled())
                            buttonPersonalInfo.setError("Not Complete");
                    }
                    if (!modelDataAddress.isComplete()) {
                        if (buttonContactInfo.isEnabled())
                            buttonContactInfo.setError("Not Complete");
                    }
                    if (!modelDataMainData.isComplete()) {
                        if (buttonOccupationInfo.isEnabled())
                            buttonOccupationInfo.setError("Not Complete");
                    }
                    if (!modelDataJobData.isComplete()) {
                        if (buttonIncomeInfo.isEnabled())
                            buttonIncomeInfo.setError("Not Complete");
                    }
                    if (!modelDataEmergencyContact.isComplete()) {
                        if (buttonEmergencyInfo.isEnabled())
                            buttonEmergencyInfo.setError("Not Complete");
                    }
                    if (!modelDataFinancial.isComplete()) {
                        if (buttonDataFinancial.isEnabled())
                            buttonDataFinancial.setError("Not Complete");
                    }

                    Snackbar.make(findViewById(R.id.personal_full_edit), "Please Fill The Data First",
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            }
        });

        buttonBack = (Button) findViewById(R.id.button_back_personal);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder b = new AlertDialog.Builder(ActivityPersonalFullEdit.this);
                b.setTitle(R.string.content_information_back_pressed);
                b.setMessage(getString(R.string.content_question_want_save_data)+
                        "\n"+getString(R.string.content_information_press_yes_save));
                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(ActivityPersonalFullEdit.this, MainActivityDrawer.class);
                        intent.putExtra(Constans.KEY_MENU, menuCategory);
                        intent.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                        startActivity(intent);
                        finish();
                    }
                });
                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        buttonSave.callOnClick();
                    }
                });
                b.show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_personal_info:
                buttonPersonalInfo.setEnabled(false);
                buttonPersonalInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonPersonalInfo.setTextColor(getResources().getColor(R.color.color_personal));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setEnabled(true);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setEnabled(true);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setEnabled(true);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonDataFinancial.setEnabled(true);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_white));

                buttonPersonalInfo.setError(null);

                if(ddlPersonalList == null){
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityPersonalFullEdit.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLPersonal();
                            buttonPersonalInfo.callOnClick();
                        }
                    });
                    b.show();
                }else {

                    fragmentDataPersonalPending = fragmentDataPersonalPending.newInstance(modelDataPersonal, ddlPersonalList, personalOrderId);
                    saveForm();

                    loadFragment(fragmentDataPersonalPending);
                }
                break;

            case R.id.button_contact_info:
                buttonPersonalInfo.setEnabled(true);
                buttonPersonalInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonPersonalInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(false);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_personal));

                buttonOccupationInfo.setEnabled(true);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setEnabled(true);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setEnabled(true);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonDataFinancial.setEnabled(true);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setError(null);

                if(ddlPersonalList == null){
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityPersonalFullEdit.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLPersonal();
                            buttonContactInfo.callOnClick();
                        }
                    });
                    b.show();
                }else {

                    isNew = personalCustDataId.equals("0");

                    fragmentDataAddressPending = fragmentDataAddressPending.newInstance(modelDataAddress, isNew, ddlPersonalList, personalCustDataId, name);
                    saveForm();

                    loadFragment(fragmentDataAddressPending);
                }
                break;

            case R.id.button_occupation_info:
                buttonPersonalInfo.setEnabled(true);
                buttonPersonalInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonPersonalInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setEnabled(false);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_personal));

                buttonIncomeInfo.setEnabled(true);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setEnabled(true);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonDataFinancial.setEnabled(true);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setError(null);

                if(ddlPersonalList == null){
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityPersonalFullEdit.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLPersonal();
                            buttonOccupationInfo.callOnClick();
                        }
                    });
                    b.show();
                }else {

                    isNew = personalCustDataId.equals("0");

                    fragmentDataMainDataPending = fragmentDataMainDataPending.newInstance(modelDataMainData,
                            isNew, ddlPersonalList, personalCustDataId, name,
                            kkIdNo, kkImgUrl);
                    saveForm();

                    loadFragment(fragmentDataMainDataPending);
                }
                break;

            case R.id.button_income_info:
                buttonPersonalInfo.setEnabled(true);
                buttonPersonalInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonPersonalInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setEnabled(true);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setEnabled(false);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_personal));

                buttonEmergencyInfo.setEnabled(true);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonDataFinancial.setEnabled(true);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setError(null);

                isNew = personalCustDataId.equals("0");

                fragmentDataJobDataPending = fragmentDataJobDataPending.newInstance(modelDataJobData, isNew, personalCustDataId, name);
                saveForm();

                loadFragment(fragmentDataJobDataPending);
                break;

            case R.id.button_emergency_info:
                buttonPersonalInfo.setEnabled(true);
                buttonPersonalInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonPersonalInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setEnabled(true);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setEnabled(true);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setEnabled(false);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_personal));

                buttonDataFinancial.setEnabled(true);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setError(null);

                isNew = personalCustDataId.equals("0");

                fragmentDataEmergencyContactPending = fragmentDataEmergencyContactPending.newInstance(modelDataEmergencyContact, isNew, personalCustDataId, name);
                saveForm();

                loadFragment(fragmentDataEmergencyContactPending);
                break;

            case R.id.button_data_financial:
                buttonPersonalInfo.setEnabled(true);
                buttonPersonalInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonPersonalInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonContactInfo.setEnabled(true);
                buttonContactInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonContactInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonOccupationInfo.setEnabled(true);
                buttonOccupationInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonOccupationInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonIncomeInfo.setEnabled(true);
                buttonIncomeInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonIncomeInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonEmergencyInfo.setEnabled(true);
                buttonEmergencyInfo.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                buttonEmergencyInfo.setTextColor(getResources().getColor(R.color.color_white));

                buttonDataFinancial.setEnabled(false);
                buttonDataFinancial.setBackground(getResources().getDrawable(R.drawable.disabled_button_background_personal));
                buttonDataFinancial.setTextColor(getResources().getColor(R.color.color_personal));

                buttonDataFinancial.setError(null);

                isNew = personalCustDataId.equals("0");

                fragmentDataFinancialPending = fragmentDataFinancialPending.newInstance(modelDataFinancial, isNew, personalCustDataId, name);
                saveForm();

                loadFragment(fragmentDataFinancialPending);
                break;
        }
    }

    private void saveForm(){
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (currentFragment.getClass() == fragmentDataPersonalPending.getClass()){
                    modelDataPersonal = fragmentDataPersonalPending.getForm();
                } else if (currentFragment.getClass() == fragmentDataAddressPending.getClass()){
                    modelDataAddress = fragmentDataAddressPending.getForm();
                } else if (currentFragment.getClass() == fragmentDataMainDataPending.getClass()){
                    modelDataMainData = fragmentDataMainDataPending.getForm();
                } else if (currentFragment.getClass() == fragmentDataJobDataPending.getClass()){
                    modelDataJobData = fragmentDataJobDataPending.getForm();
                }else if (currentFragment.getClass() == fragmentDataEmergencyContactPending.getClass()){
                    modelDataEmergencyContact = fragmentDataEmergencyContactPending.getForm();
                } else if (currentFragment.getClass() == fragmentDataFinancialPending.getClass()){
                    modelDataFinancial = fragmentDataFinancialPending.getForm();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPersonalFullEdit.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    checkCompleteSaveAndNext();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    public void checkCompleteSaveAndNext() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (modelDataPersonal.isModified()) {
                    modelDataPersonal = fragmentDataPersonalPending.getComplete();
                }
                if (modelDataAddress.isModified()){
                    modelDataAddress = fragmentDataAddressPending.getComplete();
                }
                if (modelDataMainData.isModified()){
                    modelDataMainData = fragmentDataMainDataPending.getComplete();
                }
                if (modelDataJobData.isModified()){
                    modelDataJobData = fragmentDataJobDataPending.getComplete();
                }if (modelDataEmergencyContact.isModified()){
                    modelDataEmergencyContact = fragmentDataEmergencyContactPending.getComplete();
                }
                if (modelDataFinancial.isModified()){
                    modelDataFinancial = fragmentDataFinancialPending.getComplete();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityPersonalFullEdit.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    checkCompleteSaveAndNext();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    private void loadFragment(Fragment fragment) {
        currentFragment = fragment;
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.frame_personal_pending, fragment);
        fragmentTransaction.commit();
    }

    private void setResponseDDLPersonal(ModelDDLPersonal ddlPersonalList){
        this.ddlPersonalList = ddlPersonalList;
    }

    public void getDDLPersonal(){
        loadingDialog.show();
        if (InternetConnection.checkConnection(this)) {
            Call<ResponseDDLPersonal> ddlCompany = apiInterface.getDDLPersonal();
            ddlCompany.enqueue(new Callback<ResponseDDLPersonal>() {
                @Override
                public void onResponse(Call<ResponseDDLPersonal> call, Response<ResponseDDLPersonal>
                        response) {
                    if (response.isSuccessful()) {
                        setResponseDDLPersonal(response.body().getDdlPersonal());
                        loadingDialog.dismiss();
                    } else {
                        loadingDialog.dismiss();
                        Snackbar.make(findViewById(R.id.personal_full_edit), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDDLPersonal> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    Snackbar.make(findViewById(R.id.personal_full_edit), t.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(R.id.personal_full_edit), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            loadingDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder b = new AlertDialog.Builder(ActivityPersonalFullEdit.this);
        b.setTitle(R.string.content_information_back_pressed);
        b.setMessage(getString(R.string.content_question_want_save_data)+
                "\n"+getString(R.string.content_information_press_yes_save));
        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(ActivityPersonalFullEdit.this, MainActivityDrawer.class);
                intent.putExtra(Constans.KEY_MENU, menuCategory);
                intent.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                startActivity(intent);
                finish();
            }
        });
        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                buttonSave.callOnClick();
            }
        });
        b.show();
    }
}
