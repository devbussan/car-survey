package com.example.a0426611017.carsurvey.Personal.DetailFullForm;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm.FragmentDataAddressComplete;
import com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm.FragmentDataEmergencyContactComplete;
import com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm.FragmentDataFinancialComplete;
import com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm.FragmentDataJobDataComplete;
import com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm.FragmentDataMainDataComplete;
import com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm.FragmentDataPersonalSGComplete;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Survey.ActivitySurveyDetail;

/**
 * Created by 0426591017 on 2/15/2018.
 */

public class ActivityPersonalSGDetail extends FragmentActivity implements View.OnClickListener {
    private Fragment currentFragment;
    private Button btn_personal_sg;
    private Button btn_data_address;
    private Button btn_data_main_data;
    private Button btn_data_job_data;
    private Button btn_data_emergency_data;
    private Button btn_data_financial;

    private FragmentDataPersonalSGComplete fragmentDataPersonalSGComplete = new FragmentDataPersonalSGComplete();
    private FragmentDataAddressComplete fragmentDataAddressComplete = new FragmentDataAddressComplete();
    private FragmentDataMainDataComplete fragmentDataMainDataComplete = new FragmentDataMainDataComplete();
    private FragmentDataJobDataComplete fragmentDataJobDataComplete = new FragmentDataJobDataComplete();
    private FragmentDataEmergencyContactComplete fragmentDataEmergencyContactComplete = new FragmentDataEmergencyContactComplete();
    private FragmentDataFinancialComplete fragmentDataFinancialComplete = new FragmentDataFinancialComplete();


    private String custid;
    private String orderid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        setContentView(R.layout.activity_personal_sg_detail);
        window.setStatusBarColor(getResources().getColor(R.color.color_personal));

        orderid = getIntent().getStringExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID);
        custid = getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, MODE_PRIVATE).getString(Constans.KEY_SPOUSE_ID, "0");

        btn_personal_sg = findViewById(R.id.button_data_personal_sg);
        btn_personal_sg.setOnClickListener(this);

        btn_data_address = findViewById(R.id.button_data_address);
        btn_data_address.setOnClickListener(this);

        btn_data_main_data = findViewById(R.id.button_data_main_data);
        btn_data_main_data.setOnClickListener(this);

        btn_data_job_data = findViewById(R.id.button_data_job_data);
        btn_data_job_data.setOnClickListener(this);

        btn_data_emergency_data = findViewById(R.id.button_data_emergency_contact);
        btn_data_emergency_data.setOnClickListener(this);

        btn_data_financial = findViewById(R.id.button_data_financial);
        btn_data_financial.setOnClickListener(this);

        Button btn_next = findViewById(R.id.button_next_personal_spouse);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityPersonalSGDetail.this, ActivitySurveyDetail.class);
                intent.putExtra(Constans.KEY_MENU, Constans.MENU_PERSONAL);
                intent.putExtra(Constans.KeySharedPreference.ORDER_ID, orderid);
                startActivity(intent);
            }
        });

        Button btn_back = findViewById(R.id.button_back_personal_spouse);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityPersonalSGDetail.super.onBackPressed();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_data_personal_sg:
                btn_personal_sg.setEnabled(false);
                btn_personal_sg.setBackground(getDrawable(R.drawable.disabled_button_background_personal));
                btn_personal_sg.setTextColor(getResources().getColor(R.color.color_personal));

                fragmentDataPersonalSGComplete = fragmentDataPersonalSGComplete.newInstance(orderid);
                setButton();

                loadFragment(fragmentDataPersonalSGComplete);
                break;

            case R.id.button_data_address:
                btn_data_address.setEnabled(false);
                btn_data_address.setBackground(getDrawable(R.drawable.disabled_button_background_personal));
                btn_data_address.setTextColor(getResources().getColor(R.color.color_personal));

                fragmentDataAddressComplete = fragmentDataAddressComplete.newInstance(custid, "spouse");
                setButton();

                loadFragment(fragmentDataAddressComplete);
                break;

            case R.id.button_data_main_data:
                btn_data_main_data.setEnabled(false);
                btn_data_main_data.setBackground(getDrawable(R.drawable.disabled_button_background_personal));
                btn_data_main_data.setTextColor(getResources().getColor(R.color.color_personal));


                fragmentDataMainDataComplete = fragmentDataMainDataComplete.newInstance(custid, "spouse");
                setButton();

                loadFragment(fragmentDataMainDataComplete);
                break;

            case R.id.button_data_job_data:
                btn_data_job_data.setEnabled(false);
                btn_data_job_data.setBackground(getDrawable(R.drawable.disabled_button_background_personal));
                btn_data_job_data.setTextColor(getResources().getColor(R.color.color_personal));

                fragmentDataJobDataComplete = fragmentDataJobDataComplete.newInstance(custid, "spouse");
                setButton();

                loadFragment(fragmentDataJobDataComplete);
                break;

            case R.id.button_data_emergency_contact:
                btn_data_emergency_data.setEnabled(false);
                btn_data_emergency_data.setBackground(getDrawable(R.drawable.disabled_button_background_personal));
                btn_data_emergency_data.setTextColor(getResources().getColor(R.color.color_personal));

                fragmentDataEmergencyContactComplete = fragmentDataEmergencyContactComplete.newInstance(custid, "spouse");
                setButton();

                loadFragment(fragmentDataEmergencyContactComplete);
                break;

            case R.id.button_data_financial:
                btn_data_financial.setEnabled(false);
                btn_data_financial.setBackground(getDrawable(R.drawable.disabled_button_background_personal));
                btn_data_financial.setTextColor(getResources().getColor(R.color.color_personal));

                fragmentDataFinancialComplete = fragmentDataFinancialComplete.newInstance(custid, "spouse");
                setButton();

                loadFragment(fragmentDataFinancialComplete);
                break;
        }
    }

    private void loadFragment(Fragment fragment) {
        currentFragment = fragment;
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.frame_personal_sg_complete, fragment);
        fragmentTransaction.commit();
    }

    private void setButton(){
        if (currentFragment != null) {
            if (currentFragment.getClass() == fragmentDataPersonalSGComplete.getClass()) {
                btn_personal_sg.setEnabled(true);
                btn_personal_sg.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                btn_personal_sg.setTextColor(getResources().getColor(R.color.color_white));
            }else if (currentFragment.getClass() == fragmentDataAddressComplete.getClass()) {
                btn_data_address.setEnabled(true);
                btn_data_address.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                btn_data_address.setTextColor(getResources().getColor(R.color.color_white));
            }else if (currentFragment.getClass() == fragmentDataMainDataComplete.getClass()) {
                btn_data_main_data.setEnabled(true);
                btn_data_main_data.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                btn_data_main_data.setTextColor(getResources().getColor(R.color.color_white));
            }else if (currentFragment.getClass() == fragmentDataJobDataComplete.getClass()) {
                btn_data_job_data.setEnabled(true);
                btn_data_job_data.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                btn_data_job_data.setTextColor(getResources().getColor(R.color.color_white));
            }else if (currentFragment.getClass() == fragmentDataEmergencyContactComplete.getClass()) {
                btn_data_emergency_data.setEnabled(true);
                btn_data_emergency_data.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                btn_data_emergency_data.setTextColor(getResources().getColor(R.color.color_white));
            }else if (currentFragment.getClass() == fragmentDataFinancialComplete.getClass()) {
                btn_data_financial.setEnabled(true);
                btn_data_financial.setBackground(getResources().getDrawable(R.drawable.ripple_effect_all_personal_button));
                btn_data_financial.setTextColor(getResources().getColor(R.color.color_white));
            }
        }
    }

}
