package com.example.a0426611017.carsurvey.Model.Company;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by c201017001 on 03/01/2018.
 */

public class ModelCompanyInfo implements Serializable {

    @SerializedName("COMPANY_ORDER_ID")
    private String companyOrderId;

    @SerializedName("COMPANY_CUST_DATA_ID")
    private String companyCustDataId;

    @SerializedName("STATUS")
    private String status;

    @SerializedName("USRCRT")
    private String userCreate;

    @SerializedName("COMPANYNAME")
    private String companyName;

    @SerializedName("NPWPNO")
    private String npwp;

    @SerializedName("COMPANYMODEL")
    private String customerModel;

    @SerializedName("COMPANYTYPE")
    private String companyType;

    @SerializedName("INDUSTRYTYPE")
    private String industryType;

    @SerializedName("NUMOFEMP")
    private String numOfEmployee;

    @SerializedName("ESTABLISMENTDATE")
    private String establismentDate;

    private String establismentDateFormat;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getCompanyOrderId() {
        return companyOrderId;
    }

    public void setCompanyOrderId(String companyOrderId) {
        this.companyOrderId = companyOrderId;
    }

    public String getCompanyCustDataId() {
        return companyCustDataId;
    }

    public void setCompanyCustDataId(String companyCustDataId) {
        this.companyCustDataId = companyCustDataId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getNpwp() {
        return npwp;
    }

    public void setNpwp(String npwp) {
        this.npwp = npwp;
    }

    public String getCustomerModel() {
        return customerModel;
    }

    public void setCustomerModel(String customerModel) {
        this.customerModel = customerModel;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public String getNumOfEmployee() {
        return numOfEmployee;
    }

    public void setNumOfEmployee(String numOfEmployee) {
        this.numOfEmployee = numOfEmployee;
    }

    public String getEstablismentDate() {
        return establismentDate;
    }

    public void setEstablismentDate(String establismentDate) {
        this.establismentDate = establismentDate;
    }

    public String getEstablismentDateFormat() {
        return establismentDateFormat;
    }

    public void setEstablismentDateFormat(String establismentDateFormat) {
        this.establismentDateFormat = establismentDateFormat;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}

