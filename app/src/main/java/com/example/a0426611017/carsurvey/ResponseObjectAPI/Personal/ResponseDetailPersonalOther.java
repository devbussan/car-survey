package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.Personal.ModelDataMainData;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426591017 on 3/6/2018.
 */

public class ResponseDetailPersonalOther {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDataMainData modelDataMainData;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDataMainData getModelDataMainData() {
        return modelDataMainData;
    }

    public void setModelDataMainData(ModelDataMainData modelDataMainData) {
        this.modelDataMainData = modelDataMainData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
