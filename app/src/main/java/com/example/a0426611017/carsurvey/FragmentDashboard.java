package com.example.a0426611017.carsurvey;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.ResponseDashboard;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class FragmentDashboard extends Fragment {

    private Context context;
    private View view;

    private SwipeRefreshLayout swipeRefreshLayout;

    private TextView textTitleDashboard, textPeriode;
    private TextView textViewOrder, textViewFull;
    private TextView textViewDraft, textViewPending, textViewReturn, textViewReject, textViewClear;
    private TextView textViewInputFull, textViewCheckHq, textViewReturnFull, textViewRejectFull, textViewApprove;
    private TextView textViewDashDraft, textViewDashPending, textViewDashReturn, textViewDashReject, textViewDashClear;
    private TextView textViewDashInputFull, textViewDashCheckHqFull, textViewDashReturnFull, textViewDashRejectFull, textViewDashApproveFull;

    private LinearLayout wrapDraftOrder, wrapPendingOrder, wrapReturnOrder, wrapRejectOrder, wrapClearOrder;
    private LinearLayout wrapInputFullOrder, wrapCheckHqFullOrder, wrapReturnFullOrder, wrapRejectFullOrder, wrapApproveFullOrder;

    private int tab = 0;

    private ResponseDashboard responseDashboard = new ResponseDashboard();

    private ProgressDialog progressDialog;
    private ApiInterface apiInterface;

    private Gson gson = new Gson();

    private String adminId = "0";
    private String periode;

    public static FragmentDashboard newInstance(int position) {
        FragmentDashboard fragment = new FragmentDashboard();
        Bundle bundle = new Bundle();
        bundle.putInt("tab", position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        context = container.getContext();
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading......");
        progressDialog.setCancelable(false);

        adminId = context.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);

        swipeRefreshLayout = view.findViewById(R.id.swipeToRefreshDahsboard);

        textPeriode = view.findViewById(R.id.periode_dashboard);

        textTitleDashboard = view.findViewById(R.id.label_dashboard);
        textViewDraft = view.findViewById(R.id.draft_number);
        textViewPending = view.findViewById(R.id.pending_number);
        textViewReturn = view.findViewById(R.id.return_number);
        textViewReject = view.findViewById(R.id.reject_number);
        textViewClear = view.findViewById(R.id.clear_number);

        textViewInputFull = view.findViewById(R.id.input_full_number);
        textViewCheckHq = view.findViewById(R.id.check_hq_full_number);
        textViewReturnFull = view.findViewById(R.id.return_full_number);
        textViewRejectFull = view.findViewById(R.id.reject_full_number);
        textViewApprove = view.findViewById(R.id.approve_full_number);

        textViewDashDraft = view.findViewById(R.id.dashboard_draft);
        textViewDashPending = view.findViewById(R.id.dashboard_pending);
        textViewDashReturn = view.findViewById(R.id.dashboard_return);
        textViewDashReject = view.findViewById(R.id.dashboard_reject);
        textViewDashClear = view.findViewById(R.id.dashboard_clear);

        textViewDashInputFull = view.findViewById(R.id.dashboard_input_full);
        textViewDashCheckHqFull = view.findViewById(R.id.dashboard_check_hq_full);
        textViewDashReturnFull = view.findViewById(R.id.dashboard_return_full);
        textViewDashRejectFull = view.findViewById(R.id.dashboard_reject_full);
        textViewDashApproveFull = view.findViewById(R.id.dashboard_approve_full);

        textViewOrder = view.findViewById(R.id.text_view_order_form);
        textViewFull = view.findViewById(R.id.text_view_full_form);

        wrapDraftOrder = view.findViewById(R.id.wrap_draft_order);
        wrapPendingOrder = view.findViewById(R.id.wrap_pending_order);
        wrapReturnOrder = view.findViewById(R.id.wrap_return_order);
        wrapRejectOrder = view.findViewById(R.id.wrap_reject_order);
        wrapClearOrder = view.findViewById(R.id.wrap_clear_order);

        wrapInputFullOrder = view.findViewById(R.id.wrap_input_full_order);
        wrapCheckHqFullOrder = view.findViewById(R.id.wrap_check_hq_full_order);
        wrapReturnFullOrder = view.findViewById(R.id.wrap_return_full_order);
        wrapRejectFullOrder = view.findViewById(R.id.wrap_reject_full_order);
        wrapApproveFullOrder = view.findViewById(R.id.wrap_approve_full_order);



        if(getArguments() != null){

            tab = getArguments().getInt("tab");
        }

        DateFormat dateFormat = new SimpleDateFormat("MMMM yyyy", new Locale("id"));
        Date date = new Date();
        periode = getString(R.string.text_dashboard_periode) + " " + dateFormat.format(date);
        textPeriode.setText(periode);

        if(tab == Constans.TAB_PERSONAL_DASHBOARD){
            textTitleDashboard.setText(getString(R.string.text_dashboard_personal));
            textTitleDashboard.setTextColor(getResources().getColor(R.color.color_personal));

            textViewOrder.setBackground(getResources().getDrawable(R.drawable.background_order_solid_personal));
            textViewFull.setBackground(getResources().getDrawable(R.drawable.background_order_solid_personal));

            wrapDraftOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_personal_form));
            textViewDraft.setTextColor(getResources().getColor(R.color.color_personal));
            textViewDashDraft.setTextColor(getResources().getColor(R.color.color_personal));

            wrapPendingOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_personal_form));
            textViewPending.setTextColor(getResources().getColor(R.color.color_personal));
            textViewDashPending.setTextColor(getResources().getColor(R.color.color_personal));

            wrapReturnOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_personal_form));
            textViewReturn.setTextColor(getResources().getColor(R.color.color_personal));
            textViewDashReturn.setTextColor(getResources().getColor(R.color.color_personal));

            wrapRejectOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_personal_form));
            textViewReject.setTextColor(getResources().getColor(R.color.color_personal));
            textViewDashReject.setTextColor(getResources().getColor(R.color.color_personal));

            wrapClearOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_personal_form));
            textViewClear.setTextColor(getResources().getColor(R.color.color_personal));
            textViewDashClear.setTextColor(getResources().getColor(R.color.color_personal));


            wrapInputFullOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_personal_form));
            textViewInputFull.setTextColor(getResources().getColor(R.color.color_personal));
            textViewDashInputFull.setTextColor(getResources().getColor(R.color.color_personal));

            wrapCheckHqFullOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_personal_form));
            textViewCheckHq.setTextColor(getResources().getColor(R.color.color_personal));
            textViewDashCheckHqFull.setTextColor(getResources().getColor(R.color.color_personal));

            wrapReturnFullOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_personal_form));
            textViewReturnFull.setTextColor(getResources().getColor(R.color.color_personal));
            textViewDashReturnFull.setTextColor(getResources().getColor(R.color.color_personal));

            wrapRejectFullOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_personal_form));
            textViewRejectFull.setTextColor(getResources().getColor(R.color.color_personal));
            textViewDashRejectFull.setTextColor(getResources().getColor(R.color.color_personal));

            wrapApproveFullOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_personal_form));
            textViewApprove.setTextColor(getResources().getColor(R.color.color_personal));
            textViewDashApproveFull.setTextColor(getResources().getColor(R.color.color_personal));

            getDashboard(Constans.PARAM_WS_PERSONAL, adminId);
            Log.d("pesaaan",Constans.PARAM_WS_PERSONAL + " : " + adminId);
            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getDashboard(Constans.PARAM_WS_PERSONAL, adminId);
                    swipeRefreshLayout.setRefreshing(false);
                }
            });

            wrapDraftOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_ORDER_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_DRAFT);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapPendingOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_ORDER_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_PENDING);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapReturnOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_ORDER_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_RETURN_ORDER);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapRejectOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_ORDER_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_REJECT_ORDER);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapClearOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_ORDER_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_CLEAR);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapInputFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_INPUT_FULL);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapCheckHqFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_CHECK_HQ);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapReturnFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_RETURN_FULL);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapRejectFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_REJECT_FULL);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapApproveFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_COMPLETE);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });


            wrapInputFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_INPUT_FULL);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapCheckHqFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_CHECK_HQ);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapReturnFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_RETURN_FULL);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapRejectFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_REJECT_FULL);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapApproveFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_COMPLETE);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

        } else {
            textTitleDashboard.setText(getString(R.string.text_dashboard_company));
            textTitleDashboard.setTextColor(getResources().getColor(R.color.color_red_soft));

            textViewOrder.setBackground(getResources().getDrawable(R.drawable.background_order_solid_company));
            textViewFull.setBackground(getResources().getDrawable(R.drawable.background_order_solid_company));

            wrapDraftOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_company_form));
            textViewDraft.setTextColor(getResources().getColor(R.color.color_company));
            textViewDashDraft.setTextColor(getResources().getColor(R.color.color_company));

            wrapPendingOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_company_form));
            textViewPending.setTextColor(getResources().getColor(R.color.color_company));
            textViewDashPending.setTextColor(getResources().getColor(R.color.color_company));

            wrapReturnOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_company_form));
            textViewReturn.setTextColor(getResources().getColor(R.color.color_company));
            textViewDashReturn.setTextColor(getResources().getColor(R.color.color_company));

            wrapRejectOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_company_form));
            textViewReject.setTextColor(getResources().getColor(R.color.color_company));
            textViewDashReject.setTextColor(getResources().getColor(R.color.color_company));

            wrapClearOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_company_form));
            textViewClear.setTextColor(getResources().getColor(R.color.color_company));
            textViewDashClear.setTextColor(getResources().getColor(R.color.color_company));

            wrapInputFullOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_company_form));
            textViewInputFull.setTextColor(getResources().getColor(R.color.color_company));
            textViewDashInputFull.setTextColor(getResources().getColor(R.color.color_company));

            wrapCheckHqFullOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_company_form));
            textViewCheckHq.setTextColor(getResources().getColor(R.color.color_company));
            textViewDashCheckHqFull.setTextColor(getResources().getColor(R.color.color_company));

            wrapReturnFullOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_company_form));
            textViewReturnFull.setTextColor(getResources().getColor(R.color.color_company));
            textViewDashReturnFull.setTextColor(getResources().getColor(R.color.color_company));

            wrapRejectFullOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_company_form));
            textViewRejectFull.setTextColor(getResources().getColor(R.color.color_company));
            textViewDashRejectFull.setTextColor(getResources().getColor(R.color.color_company));

            wrapApproveFullOrder.setBackground(getResources().getDrawable(R.drawable.ripple_effect_order_company_form));
            textViewApprove.setTextColor(getResources().getColor(R.color.color_company));
            textViewDashApproveFull.setTextColor(getResources().getColor(R.color.color_company));

            getDashboard(Constans.PARAM_WS_COMPANY, adminId);

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    getDashboard(Constans.PARAM_WS_COMPANY, adminId);
                    swipeRefreshLayout.setRefreshing(false);
                }
            });

            wrapDraftOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_ORDER_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_DRAFT);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapPendingOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_ORDER_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_PENDING);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapReturnOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_ORDER_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_RETURN_ORDER);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapRejectOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_ORDER_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_REJECT_ORDER);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapClearOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_ORDER_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_CLEAR);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapInputFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_INPUT_FULL);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapCheckHqFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_CHECK_HQ);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapReturnFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_RETURN_FULL);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapRejectFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_REJECT_FULL);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            wrapApproveFullOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_FULL_FORM);
                    Intent.putExtra(Constans.KEY_CURRENT_TAB,Constans.TAB_COMPLETE);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

        }

        return view;

    }

    private void getDashboard(final String name, final String userid){
        Log.d("pesan",name+"  :   "+userid);
        progressDialog.show();
        if (InternetConnection.checkConnection(context)) {
            Call<ResponseDashboard> dashboard = apiInterface.getDashboard(name, userid);
            dashboard.enqueue(new Callback<ResponseDashboard>() {
                @Override
                public void onResponse(Call<ResponseDashboard> call, Response<ResponseDashboard>
                        response) {
                    if (response.isSuccessful()) {
                        responseDashboard = response.body();
                        Log.d("pesaan","masuk lah");
                        if(responseDashboard.getStatus().equals("success")) {
                            textViewDraft.setText(responseDashboard.getModelDashboard().getDraftmini());
                            textViewPending.setText(responseDashboard.getModelDashboard().getPendingmini());
                            textViewReturn.setText(responseDashboard.getModelDashboard().getReturnmini());
                            textViewReject.setText(responseDashboard.getModelDashboard().getRejectmini());
                            textViewClear.setText(responseDashboard.getModelDashboard().getClearmini());
                            textViewInputFull.setText(responseDashboard.getModelDashboard().getClearmini());
                            textViewCheckHq.setText(responseDashboard.getModelDashboard().getCheckhqfull());
                            textViewReturnFull.setText(responseDashboard.getModelDashboard().getReturnfull());
                            textViewRejectFull.setText(responseDashboard.getModelDashboard().getRejectfull());
                            textViewApprove.setText(responseDashboard.getModelDashboard().getApprovefull());
                        }
                        progressDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            Log.d("ErrorBody",response.errorBody().string());
//                            responseDashboard = gson.fromJson(response.errorBody().string(), ResponseDashboard.class);
                            error = responseDashboard.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(getString(R.string.dialog_error_server));
                                b.setCancelable(false);
                                b.setMessage(getString(R.string.dialog_response_time_out)+"\n" +
                                        getString(R.string.dialog_try_again));
                                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDashboard(name, userid);
                                    }
                                });
                                b.show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(getString(R.string.dialog_error_server));
                            b.setCancelable(false);
                            b.setMessage(getString(R.string.dialog_response_time_out)+"\n" +
                                    getString(R.string.dialog_try_again));
                            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDashboard(name, userid);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(getString(R.string.dialog_error_server));
                        b.setCancelable(false);
                        b.setMessage(getString(R.string.dialog_response_time_out)+"\n" +
                                getString(R.string.dialog_try_again));
                        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDashboard(name, userid);
                            }
                        });
                        b.show();
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(getString(R.string.dialog_error_server));
                        b.setCancelable(false);
                        b.setMessage(getString(R.string.dialog_response_time_out)+"\n" +
                                getString(R.string.dialog_try_again));
                        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDashboard(name, userid);
                            }
                        });
                        b.show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDashboard> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle(getString(R.string.dialog_error_server));
                    b.setCancelable(false);
                    b.setMessage(getString(R.string.dialog_response_time_out)+"\n" +
                            getString(R.string.dialog_try_again));
                    b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDashboard(name, userid);
                        }
                    });
                    b.show();
                    progressDialog.dismiss();
                }
            });
        } else {
            Log.d("pesaaaan","Eroooooooooooooooooooooooooooor");
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(getString(R.string.dialog_error_server));
            b.setCancelable(false);
            b.setMessage(getString(R.string.dialog_internet_connect)+"\n" +
                    getString(R.string.dialog_try_again));
            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDashboard(name, userid);
                }
            });
            b.show();
            progressDialog.dismiss();

        }
    }

}
