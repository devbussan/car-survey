package com.example.a0426611017.carsurvey.Fragment.Personal.FullForm;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataAddress;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.Object.Personal.FullForm.ObjectDataAddressPending;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalAddr;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class FragmentDataAddressPending extends Fragment {
    private View view;
    private Context context;
    private String nameFile, filePath;
    private int cameraIdxButton = 0;
    private TakePicture takePicture = new TakePicture();
    private PreviewImage previewImage = new PreviewImage();
    private TextView fileNamePersonalBuildingImageLocation,
            fileNamePersonalBuildingImageLocationUrl;

    private ObjectDataAddressPending objectDataAddressPending;
    private ModelDataAddress modelDataAddress = new ModelDataAddress();
    private ModelDDLPersonal ddlPersonal = new ModelDDLPersonal();
    private FileFunction fileFunction = new FileFunction();

    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private String personalCustDataId = null;
    private String name = "";
    private Gson gson = new Gson();
    private boolean isNew = true;

    private ResponseDetailPersonalAddr responseDetailPersonalAddr = new ResponseDetailPersonalAddr();

    public FragmentDataAddressPending newInstance(ModelDataAddress modelDataAddress, boolean isNew, ModelDDLPersonal ddlPersonal, String custId, String name) {
        FragmentDataAddressPending fragmentDataAddressPending = new FragmentDataAddressPending();
        Bundle args = new Bundle();
        args.putSerializable("modelAddrPersonal", modelDataAddress);
        args.putSerializable("ddlPersonal", ddlPersonal);
        args.putString("personalCustDataId", custId);
        args.putBoolean(Constans.KEY_IS_NEW, isNew);
        args.putString("name", name);
        fragmentDataAddressPending.setArguments(args);
        return fragmentDataAddressPending;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelDataAddress = (ModelDataAddress) getArguments().getSerializable("modelAddrPersonal");
            ddlPersonal = (ModelDDLPersonal) getArguments().getSerializable("ddlPersonal");
            personalCustDataId = getArguments().getString("personalCustDataId");
            isNew = getArguments().getBoolean(Constans.KEY_IS_NEW);
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        objectDataAddressPending = new ObjectDataAddressPending(container, inflater, ddlPersonal);
        view = objectDataAddressPending.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if(modelDataAddress.isModified()){
            objectDataAddressPending.setForm(modelDataAddress);
        }else{
            if (!isNew) {
                getDetailAddr(name, personalCustDataId);
            }
        }

        fileNamePersonalBuildingImageLocation = objectDataAddressPending.getNamePersonalBuildingLocationImage();
        fileNamePersonalBuildingImageLocationUrl = objectDataAddressPending.getNamePersonalBuildingLocationImageUrl();
        Button buttonPersonalBuildingImageLocation = objectDataAddressPending.getButtonPersonalBuildingImageLocation();

        callCamera(buttonPersonalBuildingImageLocation, 1);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }else{
            objectDataAddressPending.setEnabledButtonImage();
        }

        return view;
    }

    private void callCamera(Button button, int idx){
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(v, idxButton);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                objectDataAddressPending.setEnabledButtonImage();
            }
        }
    }

    public void takePicture(View view, int idx) {
        this.cameraIdxButton = idx;
        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        filePath = takePicture.getFilePath();
        startActivityForResult(intent, 100);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                switch (cameraIdxButton){
                    case 1:
                        if (data != null) {
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectDataAddressPending.setNamePersonalBuildingImageLocation(nameFile);
                        objectDataAddressPending.setNamePersonalBuildingLocationImageUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNamePersonalBuildingImageLocation);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public ModelDataAddress getForm(){
        return objectDataAddressPending.getForm();
    }

    public void setForm(ModelDataAddress modelDataAddress){
        objectDataAddressPending.setForm(modelDataAddress);
    }

    public ModelDataAddress getComplete(){
        return objectDataAddressPending.getComplete();
    }

    private void getDetailAddr(final String name, final String idcustdata){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalAddr> addrDetail = apiInterface.getDetailPersonalAddr(name, idcustdata);
            addrDetail.enqueue(new Callback<ResponseDetailPersonalAddr>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalAddr> call, Response<ResponseDetailPersonalAddr>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataAddress = response.body().getModelDataAddress();
                        Log.d("Result", gson.toJson(modelDataAddress));

                        setForm(modelDataAddress);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalAddr = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalAddr.class);
                            error = responseDetailPersonalAddr.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailAddr(name, idcustdata);
                                    }
                                });
                                b.show();
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.personal_addr_full_edit), error,
                                        Snackbar.LENGTH_LONG)
                                    .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailAddr(name, idcustdata);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailAddr(name, idcustdata);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalAddr> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailAddr(name, idcustdata);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailAddr(name, idcustdata);
                }
            });
            b.show();
            Log.e("Error","No Internet Connectivity");
            loadingDialog.dismiss();
        }
    }
}