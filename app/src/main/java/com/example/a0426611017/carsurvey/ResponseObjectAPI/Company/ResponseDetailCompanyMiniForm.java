package com.example.a0426611017.carsurvey.ResponseObjectAPI.Company;

import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelDetailCompanyMiniForm;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 26/02/2018.
 */

public class ResponseDetailCompanyMiniForm {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDetailCompanyMiniForm modelDetailCompanyMiniForm;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDetailCompanyMiniForm getModelDetailCompanyMiniForm() {
        return modelDetailCompanyMiniForm;
    }

    public void setModelDetailCompanyMiniForm(ModelDetailCompanyMiniForm modelDetailCompanyMiniForm) {
        this.modelDetailCompanyMiniForm = modelDetailCompanyMiniForm;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
