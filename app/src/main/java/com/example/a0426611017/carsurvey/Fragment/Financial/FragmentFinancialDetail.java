package com.example.a0426611017.carsurvey.Fragment.Financial;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Financial.ModelFinance;
import com.example.a0426611017.carsurvey.Object.Financial.ObjectFinanceDetail;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Finance.ResponseDetailFinance;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentFinancialDetail extends Fragment {
    private int menu;
    private View view;
    private ObjectFinanceDetail objectFinanceDetail;

    private String orderid;
    private String name;
    private Context context;
    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private Gson gson = new Gson();
    private ResponseDetailFinance responseDetailFinance = new ResponseDetailFinance();

    public static FragmentFinancialDetail newInstance(int menu, String orderid, String name) {
        FragmentFinancialDetail fragment = new FragmentFinancialDetail();
        Bundle args = new Bundle();
        args.putInt(Constans.KEY_MENU,menu);
        args.putString("orderid", orderid);
        args.putString("name", name);
        fragment.setArguments(args);
        return fragment;
    }


   public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            menu = getArguments().getInt(Constans.KEY_MENU);
            orderid = getArguments().getString("orderid");
            name = getArguments().getString("name");
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectFinanceDetail = new ObjectFinanceDetail(container, inflater);
        view = objectFinanceDetail.getView();

        if(menu == Constans.MENU_PERSONAL){
            objectFinanceDetail.setFormForPersonal();
        }else{
            objectFinanceDetail.setFormForCompany();
        }

        context = container.getContext();
        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        getDetailFinance(name, orderid);

        return view;
    }

    public void setForm(ModelFinance modelFinance){
        objectFinanceDetail.setForm(modelFinance);
    }


    private void getDetailFinance(final String name, final String orderid){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailFinance> detailfinance = apiInterface.getDetailFinancial(name, orderid);
            detailfinance.enqueue(new Callback<ResponseDetailFinance>() {
                @Override
                public void onResponse(Call<ResponseDetailFinance> call, Response<ResponseDetailFinance>
                        response) {
                    if (response.isSuccessful()) {
                        setForm(response.body().getModelFinance());
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailFinance = gson.fromJson(response.errorBody().string(), ResponseDetailFinance.class);
                            error = responseDetailFinance.getMessage();
                            if(!error.equals("Data Not Found")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage("Cannot Connect to Server\n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailFinance(name, orderid);
                                    }
                                });
                                b.show();
                            }else{
                                Snackbar.make(view.findViewById(R.id.detail_financial), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage("Cannot Connect to Server\n" +
                                    "Please try Again ?");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailFinance(name, orderid);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server\n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailFinance(name, orderid);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailFinance> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server\n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailFinance(name, orderid);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server\n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailFinance(name, orderid);
                }
            });
            b.show();
            Log.d("Error Connection", getString(R.string.content_question_internet_error));
            loadingDialog.dismiss();
        }
    }
}
