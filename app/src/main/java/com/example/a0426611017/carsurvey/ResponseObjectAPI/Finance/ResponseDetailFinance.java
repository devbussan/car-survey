package com.example.a0426611017.carsurvey.ResponseObjectAPI.Finance;

import com.example.a0426611017.carsurvey.Model.Financial.ModelFinance;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 03/03/2018.
 */

public class ResponseDetailFinance {

    @SerializedName("status")
    private String status;

    @SerializedName("result")
    private ModelFinance modelFinance;

    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelFinance getModelFinance() {
        return modelFinance;
    }

    public void setModelFinance(ModelFinance modelFinance) {
        this.modelFinance = modelFinance;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
