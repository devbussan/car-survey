package com.example.a0426611017.carsurvey.Object.Survey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Model.Survey.ModelCekLingkungan;
import com.example.a0426611017.carsurvey.R;

import java.util.List;

/**
 * Created by c201017001 on 12/02/2018.
 */

public class ObjectCekLingkunganDetail1 {

    private View view;
    private ViewGroup container;
    private LayoutInflater inflater;
    private Context context;

    private TextView labelNamaInforman, labelHubunganDenganKonsumen, labelHubunganLainLain, labelCalonDebiturDikenal,
            labelKarakterDebitur, labelKarakterPasangan, labelHubunganDebiturDenganPasangan, labelInformasiPositif,
            labelInformasiNegatif, labelInformasiKepemilikanRumah, labelInformasiPekerjaan, labelInformasiKepemilikanAsset,
            labelLamaTinggalKonsumen, labelKeteranganLain;

    private TextView valueNamaInforman, valueHubunganLainLain, valueCalonDebiturDikenal,
            valueKarakterDebitur, valueKarakterPasangan, valueHubunganDebiturDenganPasangan, valueInformasiPositif,
            valueInformasiNegatif, valueInformasiKepemilikanRumah, valueInformasiPekerjaan, valueInformasiKepemilikanAsset,
            valueLamaTinggalKonsumen, valueKeteranganLain, valueHubunganDenganKonsumen;

    public ObjectCekLingkunganDetail1(ViewGroup container, LayoutInflater inflater) {
        this.container = container;
        this.inflater = inflater;
        this.context = container.getContext();
        this.view = this.inflater.inflate(R.layout.fragment_cek_lingkungan_detail1, container, false);
        setField();
        setFieldLabel();
    }

    public View getView() {
        return view;
    }

    private void setField(){
        valueNamaInforman = view.findViewById(R.id.value_nama_informan);
        valueHubunganDenganKonsumen = view.findViewById(R.id.value_hubungan_dengan_konsumen);
        valueHubunganLainLain = view.findViewById(R.id.value_hubungan_lain_lain);
        valueCalonDebiturDikenal = view.findViewById(R.id.value_calon_debitur_dikenal);
        valueKarakterDebitur = view.findViewById(R.id.value_karakter_debitur);
        valueKarakterPasangan = view.findViewById(R.id.value_karakter_pasangan);
        valueHubunganDebiturDenganPasangan = view.findViewById(R.id.value_hubungan_debitur_dengan_pasangan);
        valueInformasiPositif = view.findViewById(R.id.value_informasi_positif);
        valueInformasiNegatif = view.findViewById(R.id.value_informasi_negatif);
        valueInformasiKepemilikanRumah = view.findViewById(R.id.value_informasi_kepemilikan_rumah);
        valueInformasiPekerjaan = view.findViewById(R.id.value_informasi_pekerjaan);
        valueInformasiKepemilikanAsset = view.findViewById(R.id.value_informasi_kepemilikan_asset);
        valueLamaTinggalKonsumen = view.findViewById(R.id.value_lama_tinggal_konsumen);
        valueKeteranganLain = view.findViewById(R.id.value_keterangan_lain);
    }

    public void setForm(ModelCekLingkungan modelCekLingkungan){

        valueNamaInforman.setText(modelCekLingkungan.getInforName());
        valueHubunganLainLain.setText(modelCekLingkungan.getOtherRelation());
        valueCalonDebiturDikenal.setText(modelCekLingkungan.getClnDebDiKenal());
        valueKarakterDebitur.setText(modelCekLingkungan.getCharacterCust());
        valueKarakterPasangan.setText(modelCekLingkungan.getCharacterSpouse());
        valueHubunganDebiturDenganPasangan.setText(modelCekLingkungan.getRltnWithSpouse());
        valueInformasiPositif.setText(modelCekLingkungan.getPositiveInfo());
        valueInformasiNegatif.setText(modelCekLingkungan.getNegativeInfo());
        valueInformasiKepemilikanRumah.setText(modelCekLingkungan.getHomeStat());
        valueInformasiPekerjaan.setText(modelCekLingkungan.getWorkStat());
        valueInformasiKepemilikanAsset.setText(modelCekLingkungan.getInfoAsset());
        valueLamaTinggalKonsumen.setText(modelCekLingkungan.getLamaTinggal());
        valueKeteranganLain.setText(modelCekLingkungan.getOtherInfo());
        valueHubunganDenganKonsumen.setText(modelCekLingkungan.getRelationWithCust());

    }

    private void setFieldLabel(){
        labelNamaInforman = view.findViewById(R.id.label_nama_informan);
        labelHubunganDenganKonsumen = view.findViewById(R.id.label_hubungan_dengan_konsumen);
        labelHubunganLainLain = view.findViewById(R.id.label_hubungan_lain_lain);
        labelCalonDebiturDikenal = view.findViewById(R.id.label_calon_debitur_dikenal);
        labelKarakterDebitur = view.findViewById(R.id.label_karakter_debitur);
        labelKarakterPasangan = view.findViewById(R.id.label_karakter_pasangan);
        labelHubunganDebiturDenganPasangan = view.findViewById(R.id.label_hubungan_debitur_dengan_pasangan);
        labelInformasiPositif = view.findViewById(R.id.label_informasi_positif);
        labelInformasiNegatif = view.findViewById(R.id.label_informasi_negatif);
        labelInformasiKepemilikanRumah = view.findViewById(R.id.label_informasi_kepemilikan_rumah);
        labelInformasiPekerjaan = view.findViewById(R.id.label_informasi_pekerjaan);
        labelInformasiKepemilikanAsset = view.findViewById(R.id.label_informasi_kepemilikan_asset);
        labelLamaTinggalKonsumen = view.findViewById(R.id.label_lama_tinggal_konsumen);
        labelKeteranganLain = view.findViewById(R.id.label_keterangan_lain);
    }

    public void setFormForPersonal(){
        labelNamaInforman.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelHubunganDenganKonsumen.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelHubunganLainLain.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelCalonDebiturDikenal.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKarakterDebitur.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKarakterPasangan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelHubunganDebiturDenganPasangan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelInformasiPositif.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelInformasiNegatif.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelInformasiKepemilikanRumah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelInformasiPekerjaan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelInformasiKepemilikanAsset.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLamaTinggalKonsumen.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKeteranganLain.setTextColor(context.getResources().getColor(R.color.color_personal));
    }

    public void setFormForCompany(){
        labelNamaInforman.setTextColor(context.getResources().getColor(R.color.color_company));
        labelHubunganDenganKonsumen.setTextColor(context.getResources().getColor(R.color.color_company));
        labelHubunganLainLain.setTextColor(context.getResources().getColor(R.color.color_company));
        labelCalonDebiturDikenal.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKarakterDebitur.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKarakterPasangan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelHubunganDebiturDenganPasangan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelInformasiPositif.setTextColor(context.getResources().getColor(R.color.color_company));
        labelInformasiNegatif.setTextColor(context.getResources().getColor(R.color.color_company));
        labelInformasiKepemilikanRumah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelInformasiPekerjaan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelInformasiKepemilikanAsset.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLamaTinggalKonsumen.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKeteranganLain.setTextColor(context.getResources().getColor(R.color.color_company));
    }

    private ArrayAdapter<String> setAdapterItemList(List list){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl , list);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;

    }
}
