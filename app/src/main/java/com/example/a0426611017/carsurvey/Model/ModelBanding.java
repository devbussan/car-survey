package com.example.a0426611017.carsurvey.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426591017 on 3/21/2018.
 */

public class ModelBanding {
    @SerializedName("PROCESSID")
    private String processid;

    @SerializedName("CMOCRT")
    private String cmocrt;

    @SerializedName("ADMINCRT")
    private String admincrt;

    @SerializedName("COMMENT")
    private String comment;

    @SerializedName("LOG_DETAILS")
    private String logDetailsReturn;

    private boolean isComplete = false;

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getLogDetailsReturn() {
        return logDetailsReturn;
    }

    public void setLogDetailsReturn(String logDetailsReturn) {
        this.logDetailsReturn = logDetailsReturn;
    }

    public String getProcessid() {
        return processid;
    }

    public void setProcessid(String processid) {
        this.processid = processid;
    }

    public String getCmocrt() {
        return cmocrt;
    }

    public void setCmocrt(String cmocrt) {
        this.cmocrt = cmocrt;
    }

    public String getAdmincrt() {
        return admincrt;
    }

    public void setAdmincrt(String admincrt) {
        this.admincrt = admincrt;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
