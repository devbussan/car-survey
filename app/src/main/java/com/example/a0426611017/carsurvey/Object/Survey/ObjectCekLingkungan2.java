package com.example.a0426611017.carsurvey.Object.Survey;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Model.Survey.ModelCekLingkungan;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLSurvey;

import java.util.List;

/**
 * Created by 0414831216 on 2/28/2018.
 */

public class ObjectCekLingkungan2 {
    private View view;
    private ViewGroup container;
    private LayoutInflater inflater;
    private Context context;
    private ModelDDLSurvey ddlSurvey;

    private TextView labelNamaInforman, labelHubunganDenganKonsumen, labelHubunganLainLain, labelCalonDebiturDikenal,
            labelKarakterDebitur, labelKarakterPasangan, labelHubunganDebiturDenganPasangan, labelInformasiPositif,
            labelInformasiNegatif, labelInformasiKepemilikanRumah, labelInformasiPekerjaan, labelInformasiKepemilikanAsset,
            labelLamaTinggalKonsumen, labelKeteranganLain;

    private EditText editTextNamaInforman, editTextHubunganLainLain, editTextCalonDebiturDikenal,
            editTextKarakterDebitur, editTextKarakterPasangan, editTextHubunganDebiturDenganPasangan, editTextInformasiPositif,
            editTextInformasiNegatif, editTextInformasiKepemilikanRumah, editTextInformasiPekerjaan, editTextInformasiKepemilikanAsset,
            editTextLamaTinggalKonsumen, editTextKeteranganLain;

    private Spinner spinnerHubunganDenganKonsumen;

    public ObjectCekLingkungan2(ViewGroup container, LayoutInflater inflater, ModelDDLSurvey ddlSurvey) {
        this.container = container;
        this.inflater = inflater;
        this.ddlSurvey = ddlSurvey;
        this.context = container.getContext();
        this.view = this.inflater.inflate(R.layout.fragment_cek_lingkungan2, container, false);
        setField();
        setFieldLabel();
    }

    public View getView() {
        return view;
    }

    private void setField(){
        editTextNamaInforman = view.findViewById(R.id.edit_text_nama_informan);
        spinnerHubunganDenganKonsumen = view.findViewById(R.id.spinner_hubungan_dengan_konsumen);
        editTextHubunganLainLain = view.findViewById(R.id.edit_text_hubungan_lain_lain);
        editTextCalonDebiturDikenal = view.findViewById(R.id.edit_text_calon_debitur_dikenal);
        editTextKarakterDebitur = view.findViewById(R.id.edit_text_karakter_debitur);
        editTextKarakterPasangan = view.findViewById(R.id.edit_text_karakter_pasangan);
        editTextHubunganDebiturDenganPasangan = view.findViewById(R.id.edit_text_hubungan_debitur_dengan_pasangan);
        editTextInformasiPositif = view.findViewById(R.id.edit_text_informasi_positif);
        editTextInformasiNegatif = view.findViewById(R.id.edit_text_informasi_negatif);
        editTextInformasiKepemilikanRumah = view.findViewById(R.id.edit_text_informasi_kepemilikan_rumah);
        editTextInformasiPekerjaan = view.findViewById(R.id.edit_text_informasi_pekerjaan);
        editTextInformasiKepemilikanAsset = view.findViewById(R.id.edit_text_informasi_kepemilikan_asset);
        editTextLamaTinggalKonsumen = view.findViewById(R.id.edit_text_lama_tinggal_konsumen);
        editTextKeteranganLain = view.findViewById(R.id.edit_text_keterangan_lain);
        if(ddlSurvey != null){
            spinnerHubunganDenganKonsumen.setAdapter(setAdapterItemList(ddlSurvey.getSourceHubunganKonsumen()));
        }
    }

    public void setForm(ModelCekLingkungan modelCekLingkungan){
        editTextNamaInforman.setText(modelCekLingkungan.getInforName());
        editTextHubunganLainLain.setText(modelCekLingkungan.getOtherRelation());
        editTextCalonDebiturDikenal.setText(modelCekLingkungan.getClnDebDiKenal());
        editTextKarakterDebitur.setText(modelCekLingkungan.getCharacterCust());
        editTextKarakterPasangan.setText(modelCekLingkungan.getCharacterSpouse());
        editTextHubunganDebiturDenganPasangan.setText(modelCekLingkungan.getRltnWithSpouse());
        editTextInformasiPositif.setText(modelCekLingkungan.getPositiveInfo());
        editTextInformasiNegatif.setText(modelCekLingkungan.getNegativeInfo());
        editTextInformasiKepemilikanRumah.setText(modelCekLingkungan.getHomeStat());
        editTextInformasiPekerjaan.setText(modelCekLingkungan.getWorkStat());
        editTextInformasiKepemilikanAsset.setText(modelCekLingkungan.getInfoAsset());
        editTextLamaTinggalKonsumen.setText(modelCekLingkungan.getLamaTinggal());
        editTextKeteranganLain.setText(modelCekLingkungan.getOtherInfo());
        if(ddlSurvey != null){
            spinnerHubunganDenganKonsumen.setSelection(ddlSurvey.getSourceHubunganKonsumen().indexOf(modelCekLingkungan.getRelationWithCust ()));
        }
    }

    public ModelCekLingkungan getForm(){
        ModelCekLingkungan modelCekLingkungan = new ModelCekLingkungan();

        modelCekLingkungan.setInforName(editTextNamaInforman.getText().toString());
        modelCekLingkungan.setRelationWithCust(spinnerHubunganDenganKonsumen.getSelectedItem().toString());
        modelCekLingkungan.setOtherRelation(editTextHubunganLainLain.getText().toString());
        modelCekLingkungan.setClnDebDiKenal(editTextCalonDebiturDikenal.getText().toString());
        modelCekLingkungan.setCharacterCust(editTextKarakterDebitur.getText().toString());
        modelCekLingkungan.setCharacterSpouse(editTextKarakterPasangan.getText().toString());
        modelCekLingkungan.setRltnWithSpouse(editTextHubunganDebiturDenganPasangan.getText().toString());
        modelCekLingkungan.setPositiveInfo(editTextInformasiPositif.getText().toString());
        modelCekLingkungan.setNegativeInfo(editTextInformasiNegatif.getText().toString());
        modelCekLingkungan.setHomeStat(editTextInformasiKepemilikanRumah.getText().toString());
        modelCekLingkungan.setWorkStat(editTextInformasiPekerjaan.getText().toString());
        modelCekLingkungan.setInfoAsset(editTextInformasiKepemilikanAsset.getText().toString());
        modelCekLingkungan.setLamaTinggal(editTextLamaTinggalKonsumen.getText().toString());
        modelCekLingkungan.setOtherInfo(editTextKeteranganLain.getText().toString());
        modelCekLingkungan.setModified(true);


        return modelCekLingkungan;
    }

    private void setFieldLabel(){
        labelNamaInforman = view.findViewById(R.id.label_nama_informan);
        labelHubunganDenganKonsumen = view.findViewById(R.id.label_hubungan_dengan_konsumen);
        labelHubunganLainLain = view.findViewById(R.id.label_hubungan_lain_lain);
        labelCalonDebiturDikenal = view.findViewById(R.id.label_calon_debitur_dikenal);
        labelKarakterDebitur = view.findViewById(R.id.label_karakter_debitur);
        labelKarakterPasangan = view.findViewById(R.id.label_karakter_pasangan);
        labelHubunganDebiturDenganPasangan = view.findViewById(R.id.label_hubungan_debitur_dengan_pasangan);
        labelInformasiPositif = view.findViewById(R.id.label_informasi_positif);
        labelInformasiNegatif = view.findViewById(R.id.label_informasi_negatif);
        labelInformasiKepemilikanRumah = view.findViewById(R.id.label_informasi_kepemilikan_rumah);
        labelInformasiPekerjaan = view.findViewById(R.id.label_informasi_pekerjaan);
        labelInformasiKepemilikanAsset = view.findViewById(R.id.label_informasi_kepemilikan_asset);
        labelLamaTinggalKonsumen = view.findViewById(R.id.label_lama_tinggal_konsumen);
        labelKeteranganLain = view.findViewById(R.id.label_keterangan_lain);
    }

    public void setFormForPersonal(){
        labelNamaInforman.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelHubunganDenganKonsumen.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelHubunganLainLain.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelCalonDebiturDikenal.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKarakterDebitur.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKarakterPasangan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelHubunganDebiturDenganPasangan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelInformasiPositif.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelInformasiNegatif.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelInformasiKepemilikanRumah.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelInformasiPekerjaan.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelInformasiKepemilikanAsset.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelLamaTinggalKonsumen.setTextColor(context.getResources().getColor(R.color.color_personal));
        labelKeteranganLain.setTextColor(context.getResources().getColor(R.color.color_personal));
    }

    public void setFormForCompany(){
        labelNamaInforman.setTextColor(context.getResources().getColor(R.color.color_company));
        labelHubunganDenganKonsumen.setTextColor(context.getResources().getColor(R.color.color_company));
        labelHubunganLainLain.setTextColor(context.getResources().getColor(R.color.color_company));
        labelCalonDebiturDikenal.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKarakterDebitur.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKarakterPasangan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelHubunganDebiturDenganPasangan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelInformasiPositif.setTextColor(context.getResources().getColor(R.color.color_company));
        labelInformasiNegatif.setTextColor(context.getResources().getColor(R.color.color_company));
        labelInformasiKepemilikanRumah.setTextColor(context.getResources().getColor(R.color.color_company));
        labelInformasiPekerjaan.setTextColor(context.getResources().getColor(R.color.color_company));
        labelInformasiKepemilikanAsset.setTextColor(context.getResources().getColor(R.color.color_company));
        labelLamaTinggalKonsumen.setTextColor(context.getResources().getColor(R.color.color_company));
        labelKeteranganLain.setTextColor(context.getResources().getColor(R.color.color_company));
    }

    public ModelCekLingkungan getComplete() {
        ModelCekLingkungan modelCekLingkungan = getForm();
        modelCekLingkungan.setComplete(true);

        if (modelCekLingkungan.getInforName().length() == 0) {
            editTextNamaInforman.setError("Informan Name is required");
            modelCekLingkungan.setComplete(false);
        } else if (modelCekLingkungan.getInforName().trim().length() == 0) {
            editTextNamaInforman.setError("Informan Name not allowed whitespaces only");
            modelCekLingkungan.setComplete(false);
        }

//        if (modelCekLingkungan.getOtherRelation().length() == 0) {
//            editTextHubunganLainLain.setError("Informan Relationship is required");
//            modelCekLingkungan.setComplete(false);
//        } else if (modelCekLingkungan.getOtherRelation().trim().length() == 0) {
//            editTextHubunganLainLain.setError("Informan Relationship not allowed whitespaces only");
//            modelCekLingkungan.setComplete(false);
//        }

        if (modelCekLingkungan.getClnDebDiKenal().length() == 0) {
            editTextCalonDebiturDikenal.setError("Calon debitur di Kenal is required");
            modelCekLingkungan.setComplete(false);
        } else if (modelCekLingkungan.getClnDebDiKenal().trim().length() == 0) {
            editTextCalonDebiturDikenal.setError("Calon debitur di Kenal not allowed whitespaces only");
            modelCekLingkungan.setComplete(false);
        }

        if (modelCekLingkungan.getCharacterCust().length() == 0) {
            editTextKarakterDebitur.setError("Karakter debitur is required");
            modelCekLingkungan.setComplete(false);
        } else if (modelCekLingkungan.getCharacterCust().trim().length() == 0) {
            editTextKarakterDebitur.setError("Karakter debitur not allowed whitespaces only");
            modelCekLingkungan.setComplete(false);
        }

        if (modelCekLingkungan.getCharacterSpouse().length() == 0) {
            editTextKarakterPasangan.setError("Karakter pasangan debitur is required");
            modelCekLingkungan.setComplete(false);
        } else if (modelCekLingkungan.getCharacterSpouse().trim().length() == 0) {
            editTextKarakterPasangan.setError("Karakter pasangan debitur not allowed whitespaces only");
            modelCekLingkungan.setComplete(false);
        }

        if (modelCekLingkungan.getRltnWithSpouse().length() == 0) {
            editTextHubunganDebiturDenganPasangan.setError("Hubungan pasangan dengan debitur is required");
            modelCekLingkungan.setComplete(false);
        } else if (modelCekLingkungan.getRltnWithSpouse().trim().length() == 0) {
            editTextHubunganDebiturDenganPasangan.setError("Hubungan pasangan dengan debitur not allowed whitespaces only");
            modelCekLingkungan.setComplete(false);
        }

        if (modelCekLingkungan.getHomeStat().length() == 0) {
            editTextInformasiKepemilikanRumah.setError("Home Status is required");
            modelCekLingkungan.setComplete(false);
        } else if (modelCekLingkungan.getHomeStat().trim().length() == 0) {
            editTextInformasiKepemilikanRumah.setError("Home Status not allowed whitespaces only");
            modelCekLingkungan.setComplete(false);
        }

        if (modelCekLingkungan.getWorkStat().length() == 0) {
            editTextInformasiPekerjaan.setError("Work Status is required");
            modelCekLingkungan.setComplete(false);
        } else if (modelCekLingkungan.getWorkStat().trim().length() == 0) {
            editTextInformasiPekerjaan.setError("Work Status not allowed whitespaces only");
            modelCekLingkungan.setComplete(false);
        }

        return modelCekLingkungan;
    }

    private ArrayAdapter<String> setAdapterItemList(List list){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl , list);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;

    }
}
