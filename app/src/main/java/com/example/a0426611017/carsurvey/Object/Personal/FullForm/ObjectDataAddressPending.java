package com.example.a0426611017.carsurvey.Object.Personal.FullForm;

/**
 * Created by 0426611017 on 2/12/2018.
 */

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Currency;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataAddress;
import com.example.a0426611017.carsurvey.R;

import java.util.List;

/**
 * Created by 0426611017 on 2/9/2018.
 */

public class ObjectDataAddressPending {
    private View view;
    private Context context;
    private ViewGroup container;
    private ModelDDLPersonal ddlPersonal;

    private EditText personalAddress, personalRt, personalRw,
            personalZipCode, personalKelurahan, personalKecamatan,
            personalCity, personalPhone, personalFax,
            buildingPriceEstimates, buildingStayLength,
            directionDescription, notes;

    private TextView namePersonalBuildingLocationImage,
            namePersonalBuildingLocationImageUrl;

    private Button buttonPersonalBuildingImageLocation;

    private Spinner addressTypeName, buildingLocationClass, buildingOwnership;

    public ObjectDataAddressPending(ViewGroup viewGroup, LayoutInflater inflater, ModelDDLPersonal ddlPersonal) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.ddlPersonal = ddlPersonal;
        this.view = inflater.inflate(R.layout.fragment_data_address_pending, container, false);

        setField();
    }

    public View getView() {
        return view;
    }

    public TextView getNamePersonalBuildingLocationImage() {
        return namePersonalBuildingLocationImage;
    }

    public TextView getNamePersonalBuildingLocationImageUrl() {
        return namePersonalBuildingLocationImageUrl;
    }

    public void setNamePersonalBuildingImageLocation(String namePersonalBuildingLocationImageUrl) {
        this.namePersonalBuildingLocationImage.setText(namePersonalBuildingLocationImageUrl);
    }

    public void setNamePersonalBuildingLocationImageUrl(String namePersonalBuildingLocationImageUrl) {
        this.namePersonalBuildingLocationImageUrl.setText(namePersonalBuildingLocationImageUrl);
    }

    public Button getButtonPersonalBuildingImageLocation() {
        return buttonPersonalBuildingImageLocation;
    }

    public void setEnabledButtonImage() {
        buttonPersonalBuildingImageLocation.setEnabled(true);
    }

    public void setField() {
        addressTypeName = view.findViewById(R.id.spinner_address_type_name_pending);
        personalAddress = view.findViewById(R.id.edit_text_personal_address_pending);
        personalRt = view.findViewById(R.id.edit_text_personal_rt_pending);
        personalRw = view.findViewById(R.id.edit_text_personal_rw_pending);
        personalZipCode = view.findViewById(R.id.edit_text_personal_zip_code_pending);
        personalKelurahan = view.findViewById(R.id.edit_text_personal_kelurahan_pending);
        personalKecamatan = view.findViewById(R.id.edit_text_personal_kecamatan_pending);
        personalCity = view.findViewById(R.id.edit_text_personal_city_pending);
        personalPhone = view.findViewById(R.id.edit_text_personal_phone_pending);
        personalPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (personalPhone.getText().length() < 7) {
                    personalPhone.setError("Minimum 7 digits");
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 7) {
                    personalPhone.setError("Minimum 7 digits");
                }
            }
        });

        personalFax = view.findViewById(R.id.edit_text_personal_fax_pending);
        personalFax.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (personalFax.getText().length() < 7) {
                    personalFax.setError("Minimum 7 digits");
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 7) {
                    personalFax.setError("Minimum 7 digits");
                }
            }
        });

        buildingLocationClass = view.findViewById(R.id.spinner_building_location_class_pending);

        namePersonalBuildingLocationImage = view.findViewById(R.id.text_view_personal_building_location_image_pending);
        namePersonalBuildingLocationImage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    namePersonalBuildingLocationImage.setError(null);
                }
            }
        });
        namePersonalBuildingLocationImageUrl = view.findViewById(R.id.text_view_personal_building_location_image_url_pending);
        buttonPersonalBuildingImageLocation = (Button) view.findViewById(R.id.button_camera_building_location_image_pending);

        buildingOwnership = view.findViewById(R.id.spinner_building_ownership_pending);
        buildingPriceEstimates = view.findViewById(R.id.edit_text_building_price_estimates_pending);
        buildingPriceEstimates.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("")){
                    buildingPriceEstimates.removeTextChangedListener(this);

                    String formatted = Currency.set(buildingPriceEstimates);
                    buildingPriceEstimates.setText(formatted.replaceAll("[$]",""));
                    buildingPriceEstimates.setSelection(formatted.length()-1);
                    buildingPriceEstimates.addTextChangedListener(this);
                }
            }
        });
        buildingStayLength = view.findViewById(R.id.edit_text_building_stay_length_pending);
        directionDescription = view.findViewById(R.id.edit_text_direction_description_pending);
        notes = view.findViewById(R.id.edit_text_notes_pending);

        if (ddlPersonal != null) {
            addressTypeName.setAdapter(setAdapterItemList(ddlPersonal.getAddressTypeName()));
            buildingLocationClass.setAdapter(setAdapterItemList(ddlPersonal.getBuildingLocationClass()));
            buildingOwnership.setAdapter(setAdapterItemList(ddlPersonal.getBuildingOwnership()));
        }

        buttonPersonalBuildingImageLocation.setEnabled(false);
    }

    public void setForm(ModelDataAddress modelDataAddress) {
        if (ddlPersonal != null) {
            addressTypeName.setSelection(ddlPersonal.getAddressTypeName().indexOf(modelDataAddress.getAddresstypename()));
            buildingLocationClass.setSelection(ddlPersonal.getBuildingLocationClass().indexOf(modelDataAddress.getBldlocclass()));
            buildingOwnership.setSelection(ddlPersonal.getBuildingOwnership().indexOf(modelDataAddress.getBldowner()));
        }

        personalAddress.setText(modelDataAddress.getCustaddr());
        personalRt.setText(modelDataAddress.getCustrt());
        personalRw.setText(modelDataAddress.getCustrw());
        personalZipCode.setText(modelDataAddress.getZipcode());
        personalKelurahan.setText(modelDataAddress.getKelurahan());
        personalKecamatan.setText(modelDataAddress.getKecamatan());
        personalCity.setText(modelDataAddress.getCity());
        personalPhone.setText(modelDataAddress.getPhone());

        personalFax.setText(modelDataAddress.getFax());
        namePersonalBuildingLocationImageUrl.setText(modelDataAddress.getBldimgurl());

        Log.d("Url image", namePersonalBuildingLocationImageUrl.getText().toString());
        PreviewImage previewImage = new PreviewImage();
        String nameIdImage;
        if (modelDataAddress.getBldimgurl().equals("") || modelDataAddress.getBldimgurl() == null) {
            nameIdImage = "No selected file";
        } else {
            if (modelDataAddress.getBldimgurl().startsWith("http")) {
                nameIdImage = modelDataAddress.getBldimgurl().split("/")[6];
                previewImage.setPreviewImage(modelDataAddress.getBldimgurl(), context, namePersonalBuildingLocationImage);
            } else {
                nameIdImage = modelDataAddress.getBldimg();
                previewImage.setPreviewImage(nameIdImage, context, namePersonalBuildingLocationImage);
            }
        }
        namePersonalBuildingLocationImage.setText(nameIdImage);

        buildingPriceEstimates.setText(modelDataAddress.getBldpriceest());
        buildingStayLength.setText(modelDataAddress.getStaylength());
        directionDescription.setText(modelDataAddress.getDirectiondesc());
        notes.setText(modelDataAddress.getNotes());

    }

    public ModelDataAddress getForm() {
        ModelDataAddress modelDataAddress = new ModelDataAddress();

        modelDataAddress.setAddresstypename(addressTypeName.getSelectedItem().toString());
        modelDataAddress.setCustaddr(personalAddress.getText().toString());
        modelDataAddress.setCustrt(personalRt.getText().toString());
        modelDataAddress.setCustrw(personalRw.getText().toString());
        modelDataAddress.setZipcode(personalZipCode.getText().toString());
        modelDataAddress.setKelurahan(personalKelurahan.getText().toString());
        modelDataAddress.setKecamatan(personalKecamatan.getText().toString());
        modelDataAddress.setCity(personalCity.getText().toString());
        modelDataAddress.setPhone(personalPhone.getText().toString());
        modelDataAddress.setFax(personalFax.getText().toString());
        modelDataAddress.setBldlocclass(buildingLocationClass.getSelectedItem().toString());
        modelDataAddress.setBldimg(namePersonalBuildingLocationImage.getText().toString());
        modelDataAddress.setBldimgurl(namePersonalBuildingLocationImageUrl.getText().toString());
        modelDataAddress.setBldowner(buildingOwnership.getSelectedItem().toString());
        modelDataAddress.setBldpriceest(buildingPriceEstimates.getText().toString().replaceAll("[,]", ""));
        modelDataAddress.setStaylength(buildingStayLength.getText().toString());
        modelDataAddress.setDirectiondesc(directionDescription.getText().toString());
        modelDataAddress.setNotes(notes.getText().toString());

        modelDataAddress.setModified(true);

        return modelDataAddress;
    }

    private ArrayAdapter<String> setAdapterItemList(List items) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl, items);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;
    }

    public ModelDataAddress getComplete() {
        ModelDataAddress modelDataAddress = getForm();
        modelDataAddress.setComplete(true);

        if (modelDataAddress.getCustaddr().length() == 0) {
            personalAddress.setError("Personal Address is required");
            modelDataAddress.setComplete(false);
        }else if(modelDataAddress.getCustaddr().trim().length() == 0){
            personalAddress.setError("Personal Address not allowed whitespaces only");
            modelDataAddress.setComplete(false);
        }

        if (modelDataAddress.getCustrt().length() == 0) {
            personalRt.setError("Personal RT is required");
            modelDataAddress.setComplete(false);
        }else if(modelDataAddress.getCustrt().trim().length() == 0){
            personalRt.setError("Personal RT not allowed whitespaces only");
            modelDataAddress.setComplete(false);
        }

        if (modelDataAddress.getCustrw().length() == 0) {
            personalRw.setError("Personal RW is required");
            modelDataAddress.setComplete(false);
        }else if(modelDataAddress.getCustrw().trim().length() == 0){
            personalRw.setError("Personal RW not allowed whitespaces only");
            modelDataAddress.setComplete(false);
        }

        if (modelDataAddress.getZipcode().length() == 0) {
            personalZipCode.setError("Personal Zip Code is required");
            modelDataAddress.setComplete(false);
        }else if(modelDataAddress.getZipcode().trim().length() == 0){
            personalZipCode.setError("Personal Zip Code not allowed whitespaces only");
            modelDataAddress.setComplete(false);
        }

        if (modelDataAddress.getKelurahan().length() == 0) {
            personalKelurahan.setError("Personal Kelurahan is required");
            modelDataAddress.setComplete(false);
        }else if(modelDataAddress.getKelurahan().trim().length() == 0){
            personalKelurahan.setError("Personal Kelurahan not allowed whitespaces only");
            modelDataAddress.setComplete(false);
        }

        if (modelDataAddress.getKecamatan().length() == 0) {
            personalKecamatan.setError("Personal Kecamatan is required");
            modelDataAddress.setComplete(false);
        }else if(modelDataAddress.getKecamatan().trim().length() == 0){
            personalKecamatan.setError("Personal Kecamatan not allowed whitespaces only");
            modelDataAddress.setComplete(false);
        }

        if (modelDataAddress.getCity().length() == 0) {
            personalCity.setError("Personal City is required");
            modelDataAddress.setComplete(false);
        }else if(modelDataAddress.getCity().trim().length() == 0){
            personalCity.setError("Personal City not allowed whitespaces only");
            modelDataAddress.setComplete(false);
        }

        if (modelDataAddress.getPhone().length() == 0) {
            personalPhone.setError("Personal Phone is required");
            modelDataAddress.setComplete(false);
        }else if (modelDataAddress.getPhone().trim().length() == 0){
            personalPhone.setError("Personal Phone not allowed whitespaces only");
            modelDataAddress.setComplete(false);
        } else if (modelDataAddress.getPhone().trim().length() < 7) {
            personalPhone.setError("Minimum 7 digits");
            modelDataAddress.setComplete(false);
        }

        // TODO: FIELD NOT MANDATORY FROM HERE
//        if (modelDataAddress.getFax().length() == 0) {
//            personalFax.setError("Personal Fax is required");
//            modelDataAddress.setComplete(false);
//        }else if(modelDataAddress.getFax().trim().length() == 0){
//            personalFax.setError("Personal Fax not allowed whitespaces only");
//            modelDataAddress.setComplete(false);
//        }
//
//        if(modelDataAddress.getBldimg().length() == 0){
//            namePersonalBuildingLocationImage.setError("Building Location Image is Required");
//            modelDataAddress.setComplete(false);
//        }else if(modelDataAddress.getBldimg().equals("No selected file")){
//            namePersonalBuildingLocationImage.setError("Building Location Image is Required");
//            modelDataAddress.setComplete(false);
//        }else {
//            namePersonalBuildingLocationImage.setError(null);
//        }
//
//        if (modelDataAddress.getBldpriceest().length() == 0) {
//            buildingPriceEstimates.setError("Building Price Estimates is required");
//            modelDataAddress.setComplete(false);
//        }else if(modelDataAddress.getBldpriceest().trim().length() == 0){
//            buildingPriceEstimates.setError("Building Price Estimates not allowed whitespaces only");
//            modelDataAddress.setComplete(false);
//        }
//
//        if (modelDataAddress.getStaylength().length() == 0) {
//            buildingStayLength.setError("Building Stay Length is required");
//            modelDataAddress.setComplete(false);
//        }else if(modelDataAddress.getStaylength().trim().length() == 0){
//            buildingStayLength.setError("Building Stay Length not allowed whitespaces only");
//            modelDataAddress.setComplete(false);
//        }
//
//        if (modelDataAddress.getDirectiondesc().length() == 0) {
//            directionDescription.setError("Direction Description is required");
//            modelDataAddress.setComplete(false);
//        }else if(modelDataAddress.getDirectiondesc().trim().length() == 0){
//            directionDescription.setError("Direction Description not allowed whitespaces only");
//            modelDataAddress.setComplete(false);
//        }
//
//        if (modelDataAddress.getNotes().length() == 0) {
//            notes.setError("Notes is required");
//            modelDataAddress.setComplete(false);
//        }else if(modelDataAddress.getNotes().trim().length() == 0){
//            notes.setError("Notes not allowed whitespaces only");
//            modelDataAddress.setComplete(false);
//        }

        return modelDataAddress;
    }
}




