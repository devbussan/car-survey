package com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataAddress;
import com.example.a0426611017.carsurvey.R;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by 0426611017 on 2/12/2018.
 */

public class ObjectDataAddressComplete {
    private ViewGroup container;
    private View view;
    private Context context;

    private TextView personalAddress, personalRt,
            personalRw, personalZipCode, personalKelurahan,
            personalKecamatan, personalCity, personalPhone, personalFax,
            buildingPriceEstimates,
            buildingStayLength, directionDescription, notes,
            addressTypeName, buildingLocationClass, buildingOwnership,
            namePersonalBuildingLocationImage;

    public ObjectDataAddressComplete(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_data_address_complete, viewGroup, false);

        setField();
    }

    public View getView() {
        return view;
    }

    private void setField() {
        addressTypeName = view.findViewById(R.id.value_address_type_name);
        personalAddress = view.findViewById(R.id.value_personal_address);
        personalRt = view.findViewById(R.id.value_personal_rt);
        personalRw = view.findViewById(R.id.value_personal_rw);
        personalZipCode = view.findViewById(R.id.value_personal_zip_code);
        personalKelurahan = view.findViewById(R.id.value_personal_kelurahan);
        personalKecamatan = view.findViewById(R.id.value_personal_kecamatan);
        personalCity = view.findViewById(R.id.value_personal_city);
        personalPhone = view.findViewById(R.id.value_personal_phone);
        personalFax = view.findViewById(R.id.value_personal_fax);
        buildingLocationClass = view.findViewById(R.id.value_building_location_class);
        namePersonalBuildingLocationImage = view.findViewById(R.id.value_building_location_image);
        buildingOwnership = view.findViewById(R.id.value_building_ownership);
        buildingPriceEstimates = view.findViewById(R.id.value_building_price_estimates);
        buildingStayLength = view.findViewById(R.id.value_building_stay_length);
        directionDescription = view.findViewById(R.id.value_direction_description);
        notes = view.findViewById(R.id.value_notes);
    }

    public void setForm(final ModelDataAddress modelDataAddress) {
        addressTypeName.setText(modelDataAddress.getAddresstypename());
        personalAddress.setText(modelDataAddress.getCustaddr());
        personalRt.setText(modelDataAddress.getCustrt());
        personalRw.setText(modelDataAddress.getCustrw());
        personalZipCode.setText(modelDataAddress.getZipcode());
        personalKelurahan.setText(modelDataAddress.getKelurahan());
        personalKecamatan.setText(modelDataAddress.getKecamatan());
        personalCity.setText(modelDataAddress.getCity());
        personalPhone.setText(modelDataAddress.getPhone());
        personalFax.setText(modelDataAddress.getFax());
        buildingLocationClass.setText(modelDataAddress.getBldlocclass());

        if (modelDataAddress.getBldimgurl() != null){
            if (!modelDataAddress.getBldimgurl().equals("")){
                namePersonalBuildingLocationImage.setText(modelDataAddress.getBldimgurl().split("/")[6]);
            }else{
                namePersonalBuildingLocationImage.setText(modelDataAddress.getBldimgurl());
            }
        }else{
            namePersonalBuildingLocationImage.setText(modelDataAddress.getBldimg());
        }

        buildingOwnership.setText(modelDataAddress.getBldowner());
        buildingPriceEstimates.setText(modelDataAddress.getBldpriceest());
        if(!buildingPriceEstimates.getText().toString().equals("")){
            if(!buildingPriceEstimates.getText().toString().equals("")){
                String cleanString = buildingPriceEstimates.getText().toString().replaceAll("[,.]", "");

                double parsed = Double.parseDouble(cleanString);
                String formatted = NumberFormat.getCurrencyInstance(new Locale("en", "US")).format((parsed / 100));

                buildingPriceEstimates.setText(formatted.replaceAll("[$]",""));
            }
        }
        buildingStayLength.setText(modelDataAddress.getStaylength());
        directionDescription.setText(modelDataAddress.getDirectiondesc());
        notes.setText(modelDataAddress.getNotes());

        PreviewImage previewImage = new PreviewImage();
        if (modelDataAddress.getBldimgurl() != null)
            if (!modelDataAddress.getBldimgurl().equals(""))
                previewImage.setPreviewImage(modelDataAddress.getBldimgurl(), context, namePersonalBuildingLocationImage);
    }
}

