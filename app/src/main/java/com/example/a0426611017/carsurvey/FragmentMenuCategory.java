package com.example.a0426611017.carsurvey;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.a0426611017.carsurvey.MainFunction.Constans;

/**
 * Created by c201017001 on 14/02/2018.
 */

public class FragmentMenuCategory extends Fragment{

    private View view;
    private Context context;
    private int menu = 0;

    public FragmentMenuCategory newInstance(int menu){
        FragmentMenuCategory fragmentMenuCategory = new FragmentMenuCategory();

        Bundle args = new Bundle();
        args.putInt("menu", menu);

        fragmentMenuCategory.setArguments(args);
        return fragmentMenuCategory;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.activity_menu_category, container, false);
        context = container.getContext();

        ImageView imageViewOrder = view.findViewById(R.id.order_logo);
        ImageView imageViewFull = view.findViewById(R.id.full_logo);

        menu = getArguments().getInt("menu", 0);

        if(menu == Constans.MENU_COMPANY){
            imageViewOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_ORDER_FORM);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            imageViewFull.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_COMPANY_FULL_FORM);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });
        }else if (menu == Constans.MENU_PERSONAL){
            imageViewOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_ORDER_FORM);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });

            imageViewFull.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent Intent = new Intent(context, MainActivityDrawer.class);
                    Intent.putExtra(Constans.KEY_MENU,Constans.MENU_PERSONAL_FULL_FORM);
                    startActivity(Intent);
                    getActivity().finish();
                }
            });
        }


        return view;

    }

}
