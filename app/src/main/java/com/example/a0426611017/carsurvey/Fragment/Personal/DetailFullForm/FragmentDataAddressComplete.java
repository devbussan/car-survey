package com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataAddress;
import com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm.ObjectDataAddressComplete;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalAddr;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentDataAddressComplete extends Fragment {
    private View view;
    private ObjectDataAddressComplete objectDataAddressComplete;
    private ModelDataAddress modelDataAddress = new ModelDataAddress();

    private String name;
    private Context context;
    private String custid;
    private Gson gson = new Gson();
    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;

    private ResponseDetailPersonalAddr responseDetailPersonalAddr = new ResponseDetailPersonalAddr();

    public FragmentDataAddressComplete newInstance(String custid, String name){
        FragmentDataAddressComplete fragmentDataAddressComplete = new FragmentDataAddressComplete();
        Bundle args = new Bundle();
        args.putString("custid",custid);
        args.putString("name", name);
        fragmentDataAddressComplete.setArguments(args);
        return fragmentDataAddressComplete;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            custid = getArguments().getString("custid");
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        objectDataAddressComplete = new ObjectDataAddressComplete(viewGroup, inflater);
        view = objectDataAddressComplete.getView();
        context = viewGroup.getContext();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading.........");
        loadingDialog.setCancelable(false);

        getDetailAddr(name, custid);

        return view;
    }

    public void setForm(ModelDataAddress modelDataAddress){
        objectDataAddressComplete.setForm(modelDataAddress);
    }

    private void getDetailAddr(final String name, String idcustdata){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalAddr> addrDetail = apiInterface.getDetailPersonalAddr(name, idcustdata);
            addrDetail.enqueue(new Callback<ResponseDetailPersonalAddr>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalAddr> call, Response<ResponseDetailPersonalAddr>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataAddress = response.body().getModelDataAddress();
                        Log.d("Result", gson.toJson(modelDataAddress));
                        modelDataAddress.setModified(true);
                        setForm(modelDataAddress);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalAddr = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalAddr.class);
                            error = responseDetailPersonalAddr.getMessage();
                            if(!error.equals("Data Not Found")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage("Cannot Connect to Server \n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailAddr(name, custid);
                                    }
                                });
                                b.show();
                            }else{
                                Snackbar.make(view.findViewById(R.id.address_detail), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage("Cannot Connect to Server \n" +
                                    "Please try Again ?");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailAddr(name, custid);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server \n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailAddr(name, custid);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalAddr> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server \n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailAddr(name, custid);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server \n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailAddr(name, custid);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }
}