package com.example.a0426611017.carsurvey.ResponseObjectAPI.Company;

import com.example.a0426611017.carsurvey.Model.Company.ModelCompanyInfo;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 03/03/2018.
 */

public class ResponseDetailCompanyFull {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelCompanyInfo modelCompanyInfo;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelCompanyInfo getModelCompanyInfo() {
        return modelCompanyInfo;
    }

    public void setModelCompanyInfo(ModelCompanyInfo modelCompanyInfo) {
        this.modelCompanyInfo = modelCompanyInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
