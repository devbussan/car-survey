package com.example.a0426611017.carsurvey.AddNew.Personal;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.a0426611017.carsurvey.Fragment.Personal.MiniForm.FragmentDataPersonalInfoOrder;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiPersonalOrder;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalInfoMini;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDDLPersonal;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNewOrderPersonal extends AppCompatActivity implements View.OnClickListener{
    private Fragment currentFragment;

    private FragmentDataPersonalInfoOrder fragmentDataPersonalInfoOrder = new FragmentDataPersonalInfoOrder();

    private ModelPersonalInfoMini modelPersonalInfoMini = new ModelPersonalInfoMini();

    private Button buttonPersonalInfo;
    private Button buttonNext;
    private Button buttonBack;

    private boolean isNew = true;

    private ModelDDLPersonal ddlPersonalList = null;
    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private ApiPersonalOrder apiPersonalOrder;
    private String adminId = null;
    private String personalOrderId = null;
    private String sharedLogId = null;
    private int menuCategory = 0;

    private View currentView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_order_personal);
        currentView = this.findViewById(android.R.id.content);

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(this);
        loadingDialog.setMessage("Loading.........");
        loadingDialog.setCancelable(false);

        isNew = getIntent().getBooleanExtra(Constans.KEY_IS_NEW, true);

        getDDLPersonal();

        apiPersonalOrder = new ApiPersonalOrder(currentView, ActivityNewOrderPersonal.this, loadingDialog);
        adminId = this.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);
        menuCategory = this.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, Context.MODE_PRIVATE).getInt(Constans.KEY_MENU_CATEGORY, 0);

        if (!isNew) {
            loadingDialog.show();
            personalOrderId = getIntent().getStringExtra(Constans.KeySharedPreference.PERSONAL_ORDER_ID);
            sharedLogId = this.getSharedPreferences(Constans.KeySharedPreference.LOG_ID, Context.MODE_PRIVATE).getString(Constans.KEY_LOG_ID, null);
            Log.d("Personal Order", personalOrderId);
            Log.d("Admin Id", adminId);
            Log.d("Log Id", "log id : "+sharedLogId);
        }

        buttonPersonalInfo = findViewById(R.id.button_personal_info);
        buttonPersonalInfo.setOnClickListener(this);
        buttonNext = findViewById(R.id.button_next_personal_order);
        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingDialog.show();
                saveForm();
                checkCompleteSaveAndNext();

                if(modelPersonalInfoMini.isComplete()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderPersonal.this);
                    builder.setTitle(getString(R.string.content_question_are_you_sure_to_save));
                    builder.setMessage(getString(R.string.content_information_save_and_next));
                    builder.setNegativeButton(getString(R.string.dialog_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    loadingDialog.dismiss();
                                    Log.e("info", "NO");
                                }
                            });
                    builder.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Log.e("info", "YES");

                            modelPersonalInfoMini.setUsrcrt(adminId);

                            apiPersonalOrder.saveUpdatePersonalOrder(modelPersonalInfoMini,
                                    personalOrderId,
                                    sharedLogId
                            );
                        }
                    });
                    builder.show();
                    loadingDialog.dismiss();
                } else {
                    if(!modelPersonalInfoMini.isComplete()){
                        if (buttonPersonalInfo.isEnabled())
                            buttonPersonalInfo.setError("Is not Complete");

                        Snackbar.make(findViewById(R.id.personal_new_order), "Please Fill The Data First",
                                Snackbar.LENGTH_LONG)
                                .show();
                        loadingDialog.dismiss();
                    }
                }
            }
        });

        buttonBack = findViewById(R.id.button_back_new_order_personal);
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (currentFragment != null) {
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityNewOrderPersonal.this);
                    b.setTitle(getString(R.string.content_question_are_you_sure));
                    b.setMessage(getString(R.string.content_information_data_will_lost));
                    b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent i = new Intent(ActivityNewOrderPersonal.this, MainActivityDrawer.class);
                            i.putExtra(Constans.KEY_MENU, menuCategory);
                            i.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                            startActivity(i);
                            finish();
                        }
                    });
                    b.show();
                } else {
                    ActivityNewOrderPersonal.super.onBackPressed();
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_personal_info :
                buttonPersonalInfo.setEnabled(false);
                buttonPersonalInfo.setBackground(getDrawable(R.drawable.disabled_button_background_personal));
                buttonPersonalInfo.setTextColor(getResources().getColor(R.color.color_personal));
                buttonPersonalInfo.setError(null);

                if(ddlPersonalList == null){
                    AlertDialog.Builder b = new AlertDialog.Builder(ActivityNewOrderPersonal.this);
                    b.setTitle(getString(R.string.content_question_internet_error));
                    b.setMessage(getString(R.string.content_information_cant_connect_server));
                    b.setCancelable(false);
                    b.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDDLPersonal();
                            buttonPersonalInfo.callOnClick();
                        }
                    });
                    b.show();
                }else {

                    fragmentDataPersonalInfoOrder = FragmentDataPersonalInfoOrder.newInstance(modelPersonalInfoMini, isNew, ddlPersonalList, personalOrderId);
                    saveForm();

                    loadFragment(fragmentDataPersonalInfoOrder);
                }
                break;
        }
    }

    private void saveForm() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (currentFragment.getClass() == fragmentDataPersonalInfoOrder.getClass()){
                    modelPersonalInfoMini = fragmentDataPersonalInfoOrder.getForm();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderPersonal.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    saveForm();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    public void checkCompleteSaveAndNext() {
        if (InternetConnection.checkConnection(this)) {
            if (currentFragment != null) {
                if (modelPersonalInfoMini.isModified()) {
                    Log.d("cek save next","check get complete");
                    modelPersonalInfoMini = fragmentDataPersonalInfoOrder.getComplete();
                }
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(ActivityNewOrderPersonal.this);
            builder.setTitle(getString(R.string.content_question_internet_error));
            builder.setMessage(getString(R.string.content_information_cant_connect_server));
            builder.setCancelable(false);
            builder.setPositiveButton(getString(R.string.dialog_try_again), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    checkCompleteSaveAndNext();
                    Log.e("info", "OK");
                }
            });
            builder.show();
        }
    }

    private void loadFragment(Fragment fragment) {
        currentFragment = fragment;
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fm.beginTransaction();

        fragmentTransaction.replace(R.id.frame_personal_order, fragment);
        fragmentTransaction.commit();
    }

    private void setResponseDDLPersonal(ModelDDLPersonal ddlPersonalList){
        this.ddlPersonalList = ddlPersonalList;
    }

    public void getDDLPersonal(){
        loadingDialog.show();
        if (InternetConnection.checkConnection(this)) {
            Call<ResponseDDLPersonal> ddlCompany = apiInterface.getDDLPersonal();
            ddlCompany.enqueue(new Callback<ResponseDDLPersonal>() {
                @Override
                public void onResponse(Call<ResponseDDLPersonal> call, Response<ResponseDDLPersonal>
                        response) {
                    if (response.isSuccessful()) {
                        setResponseDDLPersonal(response.body().getDdlPersonal());
                        loadingDialog.dismiss();
                    } else {
                        loadingDialog.dismiss();
                        Snackbar.make(findViewById(R.id.personal_new_order), R.string.error_api,
                                Snackbar.LENGTH_LONG)
                                .show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDDLPersonal> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    Snackbar.make(findViewById(R.id.personal_new_order), t.toString(),
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            Snackbar.make(findViewById(R.id.personal_new_order), R.string.no_connectivity,
                    Snackbar.LENGTH_LONG)
                    .show();
            loadingDialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder b = new AlertDialog.Builder(ActivityNewOrderPersonal.this);
        b.setTitle(getString(R.string.content_question_are_you_sure));
        b.setMessage(getString(R.string.content_information_data_will_lost));
        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent i = new Intent(ActivityNewOrderPersonal.this, MainActivityDrawer.class);
                i.putExtra(Constans.KEY_MENU, menuCategory);
                i.putExtra(Constans.KEY_CURRENT_TAB, getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                startActivity(i);
                finish();
            }
        });
        b.show();
    }
}
