package com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataMainData;
import com.example.a0426611017.carsurvey.R;

import java.io.File;

/**
 * Created by 0426611017 on 2/12/2018.
 */

public class ObjectDataMainDataComplete {
    private ViewGroup container;
    private View view;
    private Context context;

    private TextView numDependents, numOfResidence, familyCardNumber,
            kendaraanYangDimiliki, mobilePhone1,
            mobilePhone2;

    private TextView salutation, maritalStatus, religion, education;

    private TextView namePersonalFamilyCardNumberImage;



    public ObjectDataMainDataComplete(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_data_main_data_complete, viewGroup, false);

        setField();
    }

    public View getView() {
        return view;
    }

    private void setField() {
        salutation = view.findViewById(R.id.value_salutation);
        maritalStatus = view.findViewById(R.id.value_marital_status);
        religion = view.findViewById(R.id.value_religion);
        education = view.findViewById(R.id.value_education);
        numDependents = view.findViewById(R.id.value_num_dependents);
        numOfResidence = view.findViewById(R.id.value_num_of_residence);
        familyCardNumber = view.findViewById(R.id.value_family_card_number);
        namePersonalFamilyCardNumberImage = view.findViewById(R.id.value_family_card_image);
        kendaraanYangDimiliki = view.findViewById(R.id.value_kendaraan_yang_dimiliki);
        mobilePhone1 = view.findViewById(R.id.value_mobile_phone_1);
        mobilePhone2 = view.findViewById(R.id.value_mobile_phone_2);
    }

    public void setForm(final ModelDataMainData modelDataMainData) {
        salutation.setText(modelDataMainData.getSalutation());
        maritalStatus.setText(modelDataMainData.getMaritalstat());
        religion.setText(modelDataMainData.getReligion());
        education.setText(modelDataMainData.getEducation());
        numDependents.setText(modelDataMainData.getNumdependents());
        numOfResidence.setText(modelDataMainData.getNumofresidence());
        familyCardNumber.setText(modelDataMainData.getKkno());

        if (modelDataMainData.getKkimgurl() != null){
            if (!modelDataMainData.getKkimgurl().equals("")){
                namePersonalFamilyCardNumberImage.setText(modelDataMainData.getKkimgurl().split("/")[6]);
            }else{
                namePersonalFamilyCardNumberImage.setText(modelDataMainData.getKkimgurl());
            }
        }else{
            namePersonalFamilyCardNumberImage.setText(modelDataMainData.getKkimg());
        }

        kendaraanYangDimiliki.setText(modelDataMainData.getOwnedvehicle());
        mobilePhone1.setText(modelDataMainData.getMobilephone1());
        mobilePhone2.setText(modelDataMainData.getMobilephone2());

        PreviewImage previewImage = new PreviewImage();
        if (modelDataMainData.getKkimgurl() != null)
            if (!modelDataMainData.getKkimgurl().equals(""))
                previewImage.setPreviewImage(modelDataMainData.getKkimgurl(), context, namePersonalFamilyCardNumberImage);
    }
}
