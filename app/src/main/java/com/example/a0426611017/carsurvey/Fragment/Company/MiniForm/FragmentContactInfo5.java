package com.example.a0426611017.carsurvey.Fragment.Company.MiniForm;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Object.Company.MiniForm.ObjectContactInfo5;
import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseDetailContactInfo;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class FragmentContactInfo5 extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    private boolean isOrder = true;
    private View view;
    private Context context;
    private String nameFile;
    private int idxButton;

    private ObjectContactInfo5 objectContactInfo5;
    private PreviewImage previewImage = new PreviewImage();
    private FileFunction fileFunction = new FileFunction();
    private TakePicture takePicture = new TakePicture();

    private Button buttonIdImage, buttonIdNpwpImage;

    private TextView fileNameIdImage, fileNameIdNpwpImage;

    private ModelDetailContactInfo modelDetailContactInfo;

    private String companyOrderId = "0";
    private String contactOrderId = "0";

    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;
    private Gson gson = new Gson();

    private ResponseDetailContactInfo responseDetailContactInfo = new ResponseDetailContactInfo();

    public static FragmentContactInfo5 newInstance(ModelDetailContactInfo modelDetailContactInfo, String contactOrderId, String companyOrderId, boolean isOrder)
    {
        FragmentContactInfo5 fragment = new FragmentContactInfo5();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, modelDetailContactInfo);
        args.putString(ARG_PARAM2, companyOrderId);
        args.putString(ARG_PARAM3, contactOrderId);
        args.putBoolean(ARG_PARAM4, isOrder);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelDetailContactInfo = (ModelDetailContactInfo) getArguments().getSerializable(ARG_PARAM1);
            companyOrderId = getArguments().getString(ARG_PARAM2);
            contactOrderId = getArguments().getString(ARG_PARAM3);
            isOrder = getArguments().getBoolean(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectContactInfo5 = new ObjectContactInfo5(container, inflater);
        view = objectContactInfo5.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if (modelDetailContactInfo.isModified()) {
            setForm(modelDetailContactInfo);
            modelDetailContactInfo = getComplete();
            if(!isOrder && contactOrderId != null && modelDetailContactInfo.isComplete()) {
                setDisabled();
            }
        }else if(contactOrderId != null){
            getDetailContact(contactOrderId, companyOrderId);
        }

        buttonIdImage = objectContactInfo5.getButtonIdImage();
        buttonIdNpwpImage = objectContactInfo5.getButtonIdNpwpIamge();

        fileNameIdImage = objectContactInfo5.getTextViewIdImage();
        fileNameIdNpwpImage = objectContactInfo5.getTextViewIdNpwpImage();

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }else{
            if(isOrder) {
                buttonIdImage.setEnabled(true);
                buttonIdNpwpImage.setEnabled(true);
            }
        }

        callCamera(buttonIdImage, 1);
        callCamera(buttonIdNpwpImage, 2);

        return view;
    }

    private void callCamera(Button button, int idx){
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                takePicture(idxButton);

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                buttonIdImage.setEnabled(true);
                buttonIdNpwpImage.setEnabled(true);
            }
        }
    }

    public void takePicture(int idx) {

        this.idxButton = idx;
        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        startActivityForResult(intent, 100);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {

                switch (idxButton){
                    case 1:
                        if(data != null){
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();

                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectContactInfo5.setTextViewIdImage(nameFile);
                        objectContactInfo5.setTextViewIdImageUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNameIdImage);
                        break;
                    case 2:
                        if(data != null){
                            try {
                                File file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();

                            } catch (IOException e) {
                                Toast.makeText(context, "Error : "+e.getMessage(), Toast.LENGTH_LONG).show();
                                e.printStackTrace();
                            }
                        }
                        objectContactInfo5.setTextViewIdNpwpImage(nameFile);
                        objectContactInfo5.setTextViewIdNpwpImageUrl(nameFile);
                        previewImage.setPreviewImage(nameFile, context, fileNameIdNpwpImage);
                        break;
                    default:
                        break;
                }

            }
        }
    }

    public ModelDetailContactInfo getForm(){
        return objectContactInfo5.getForm();
    }

    public void setForm(ModelDetailContactInfo modelContactInfoMini){
        objectContactInfo5.setForm(modelContactInfoMini);
    }

    public void setDisabled(){
        objectContactInfo5.setDisabled();
    }

    public ModelDetailContactInfo getComplete(){
        return objectContactInfo5.getComplete();
    }
    private void getDetailContact(final String contactOrderId, final String companyOrderId) {
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailContactInfo> contactDetail = apiInterface.getDetailContactInfo(contactOrderId, companyOrderId);
            contactDetail.enqueue(new Callback<ResponseDetailContactInfo>() {
                @Override
                public void onResponse(Call<ResponseDetailContactInfo> call, Response<ResponseDetailContactInfo>
                        response) {
                    if (response.isSuccessful()) {

                        modelDetailContactInfo = response.body().getModelDetailContactInfo();
                        modelDetailContactInfo.setModified(true);
                        setForm(modelDetailContactInfo);
                        modelDetailContactInfo = getComplete();
                        if(!isOrder && contactOrderId != null && modelDetailContactInfo.isComplete()) {
                            setDisabled();
                        }
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {

                        String error = "";
                        try {
                            responseDetailContactInfo = gson.fromJson(response.errorBody().string(), ResponseDetailContactInfo.class);
                            error = responseDetailContactInfo.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(getString(R.string.dialog_error_server));
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        getString(R.string.dialog_try_again));
                                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailContact(contactOrderId, companyOrderId);
                                    }
                                });

                            }else if(!error.equals("Data Not Found")) {
                                Snackbar.make(view.findViewById(R.id.fragment_contact), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(getString(R.string.dialog_error_server));
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    getString(R.string.dialog_try_again));
                            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailContact(contactOrderId, companyOrderId);
                                }
                            });
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(getString(R.string.dialog_error_server));
                        b.setCancelable(false);
                        b.setMessage(error+"\n" +
                                getString(R.string.dialog_try_again));
                        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailContact(contactOrderId, companyOrderId);
                            }
                        });
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(getString(R.string.dialog_error_server));
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                getString(R.string.dialog_try_again));
                        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailContact(contactOrderId, companyOrderId);
                            }
                        });
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailContactInfo> call, Throwable t) {

                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle(getString(R.string.dialog_error_server));
                    b.setCancelable(false);
                    b.setMessage(t.toString()+"\n" +
                            getString(R.string.dialog_try_again));
                    b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailContact(contactOrderId, companyOrderId);
                        }
                    });
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(getString(R.string.dialog_error_server));
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    getString(R.string.dialog_try_again));
            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailContact(contactOrderId, companyOrderId);
                }
            });
            loadingDialog.dismiss();
        }
    }

}
