package com.example.a0426611017.carsurvey.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 28/02/2018.
 */

public class ModelResponseLogin {
    @SerializedName("ADMIN_NAME")
    private String adminName;

    @SerializedName("ADMIN_ID")
    private String adminId;

    @SerializedName("IMEI")
    private String adminImei;

    @SerializedName("ADMIN_NIK")
    private String adminNik;

    public String getAdminNik() {
        return adminNik;
    }

    public void setAdminNik(String adminNik) {
        this.adminNik = adminNik;
    }

    public String getAdminImei() {
        return adminImei;
    }

    public void setAdminImei(String adminImei) {
        this.adminImei = adminImei;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getAdminId() {
        return adminId;
    }

    public void setAdminId(String adminId) {
        this.adminId = adminId;
    }
}
