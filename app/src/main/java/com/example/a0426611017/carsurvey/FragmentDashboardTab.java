package com.example.a0426611017.carsurvey;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.Constans;

/**
 * Created by 0426611017 on 3/10/2018.
 */

public class FragmentDashboardTab extends Fragment{

    private TabLayout tabLayout;
    private Context context;
    private int tab = 0;


    public static FragmentDashboardTab newInstance(int position ) {
        FragmentDashboardTab fragment = new FragmentDashboardTab();
        Bundle bundle = new Bundle();
        bundle.putInt("tab", position);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard_tab, container, false);

        tabLayout = (TabLayout) view.findViewById(R.id.tab_layout_dash);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.dash_personal)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.dash_company)));
        tabLayout.setBackgroundColor(getResources().getColor(R.color.color_grey));
        tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.color_black));

        tabLayout.setSelectedTabIndicatorHeight((int) (2.5 * getResources().getDisplayMetrics().density));
        setCurrentTab(0);

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                setCurrentTab(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }


        });
        return view;
    }

    private void setCurrentTab(int position){
       replaceFragment(new FragmentDashboard().newInstance(position));
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_container_dash, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }
}
