package com.example.a0426611017.carsurvey.Model.Personal.Mini;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 0426591017 on 2/19/2018.
 */

public class ModelPersonalInfoMini implements Serializable {
    @SerializedName("PERSONAL_ORDER_ID")
    private String personal_order_id;

    @SerializedName("FULLNAME")
    private String fullname;

    @SerializedName("IDTYPE")
    private String idtype;

    @SerializedName("IDNO")
    private String idno;

    @SerializedName("IDIMGURL")
    private String idimgurl;

    private String idimg;

    @SerializedName("KKNO")
    private String kkno;

    @SerializedName("KKIMGURL")
    private String kkimgurl;

    private String kkimg;

    @SerializedName("LOGDATAID")
    private String logdataid;

    @SerializedName("USRCRT")
    private String usrcrt;

    @SerializedName("PERSONAL_ORDER_RTN_ID")
    private String personal_order_id_return;

    @SerializedName("LOG_DATA_RTN_ID")
    private String log_data_id_return;

    private boolean isModified = false;
    private boolean isComplete = false;

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getPersonal_order_id() {
        return personal_order_id;
    }

    public void setPersonal_order_id(String personal_order_id) {
        this.personal_order_id = personal_order_id;
    }

    public String getIdimg() {
        return idimg;
    }

    public void setIdimg(String idimg) {
        this.idimg = idimg;
    }

    public String getKkimg() {
        return kkimg;
    }

    public void setKkimg(String kkimg) {
        this.kkimg = kkimg;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getIdtype() {
        return idtype;
    }

    public void setIdtype(String idtype) {
        this.idtype = idtype;
    }

    public String getIdno() {
        return idno;
    }

    public void setIdno(String idno) {
        this.idno = idno;
    }

    public String getIdimgurl() {
        return idimgurl;
    }

    public void setIdimgurl(String idimgurl) {
        this.idimgurl = idimgurl;
    }

    public String getKkno() {
        return kkno;
    }

    public void setKkno(String kkno) {
        this.kkno = kkno;
    }

    public String getKkimgurl() {
        return kkimgurl;
    }

    public void setKkimgurl(String kkimgurl) {
        this.kkimgurl = kkimgurl;
    }

    public String getLogdataid() {
        return logdataid;
    }

    public void setLogdataid(String logdataid) {
        this.logdataid = logdataid;
    }

    public String getUsrcrt() {
        return usrcrt;
    }

    public void setUsrcrt(String usrcrt) {
        this.usrcrt = usrcrt;
    }

    public String getPersonal_order_id_return() {
        return personal_order_id_return;
    }

    public void setPersonal_order_id_return(String personal_order_id_return) {
        this.personal_order_id_return = personal_order_id_return;
    }

    public String getLog_data_id_return() {
        return log_data_id_return;
    }

    public void setLog_data_id_return(String log_data_id_return) {
        this.log_data_id_return = log_data_id_return;
    }
}
