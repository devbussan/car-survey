package com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonalSG;
import com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm.ObjectDataPersonalSGComplete;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalSgInfo;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by 0426591017 on 2/15/2018.
 */

public class FragmentDataPersonalSGComplete extends Fragment {
    private View view;
    private ObjectDataPersonalSGComplete objectDataPersonalSGComplete;
    private ModelDataPersonalSG modelDataPersonalSG = new ModelDataPersonalSG();

    private Context context;
    private String orderid;
    private Gson gson = new Gson();
    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;

    private ResponseDetailPersonalSgInfo responseDetailPersonalSgInfo = new ResponseDetailPersonalSgInfo();

    public FragmentDataPersonalSGComplete newInstance(String orderid){
        FragmentDataPersonalSGComplete fragmentDataPersonalSgComplete = new FragmentDataPersonalSGComplete();
        Bundle args = new Bundle();
        args.putString("orderid", orderid);
        fragmentDataPersonalSgComplete.setArguments(args);
        return fragmentDataPersonalSgComplete;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            orderid = getArguments().getString("orderid");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        objectDataPersonalSGComplete = new ObjectDataPersonalSGComplete(viewGroup, inflater);
        view = objectDataPersonalSGComplete.getView();
        context = viewGroup.getContext();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading.........");
        loadingDialog.setCancelable(false);

        getDetailInfo("spouse", orderid);

        return view;
    }

    public void setForm(ModelDataPersonalSG modelDataPersonalSG){
        objectDataPersonalSGComplete.setForm(modelDataPersonalSG);
    }

    private void getDetailInfo(final String name, final String orderid){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalSgInfo> infoDetail = apiInterface.getDetailPersonalSgInfo(name, orderid);
            infoDetail.enqueue(new Callback<ResponseDetailPersonalSgInfo>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalSgInfo> call, Response<ResponseDetailPersonalSgInfo>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataPersonalSG = response.body().getModelDataPersonalSG();
                        Log.d("Result", gson.toJson(modelDataPersonalSG));
                        modelDataPersonalSG.setModified(true);
                        setForm(modelDataPersonalSG);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalSgInfo = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalSgInfo.class);
                            error = responseDetailPersonalSgInfo.getMessage();
                            if(!error.equals("Data Not Found")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage("Cannot Connect to Server \n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailInfo(name, orderid);
                                    }
                                });
                                b.show();
                            }else{
                                Snackbar.make(view.findViewById(R.id.sg_detail), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage("Cannot Connect to Server \n" +
                                    "Please try Again ?");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailInfo(name, orderid);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server \n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailInfo(name, orderid);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalSgInfo> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server \n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailInfo(name, orderid);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server \n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailInfo(name, orderid);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }
}
