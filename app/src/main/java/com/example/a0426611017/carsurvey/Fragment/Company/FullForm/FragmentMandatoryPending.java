package com.example.a0426611017.carsurvey.Fragment.Company.FullForm;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.MainFunction.TakePicture;
import com.example.a0426611017.carsurvey.Model.Company.ModelMandatoryInfo;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLCompany;
import com.example.a0426611017.carsurvey.Object.Company.FullForm.ObjectMandatoryFull;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailEvidenceFull;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by c201017001 on 21/12/2017.
 */

public class FragmentMandatoryPending extends Fragment {


    private PreviewImage previewImage = new PreviewImage();
    private FileFunction fileFunction = new FileFunction();
    private TakePicture takePicture = new TakePicture();
    private ObjectMandatoryFull objectMandatory;
    private View view;
    private Context context;
    private String nameFile;
    private int idxButton = 0;
    private TextView fileNameAkta, fileNameSiup, fileNameDomisili, fileNameTdp;
    private Button buttonAkta, buttonSiup, buttontdp, buttonSkDomisili;
    private ModelMandatoryInfo modelMandatoryInfo = new ModelMandatoryInfo();
    private ModelDDLCompany ddlCompany;
    private String custDataId = "0";

    private ResponseDetailEvidenceFull responseDetailEvidenceFull = new ResponseDetailEvidenceFull();

    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;

    private Gson gson = new Gson();

    public FragmentMandatoryPending newInstance(ModelMandatoryInfo mandatoryInfo, ModelDDLCompany ddlCompany, String custDataId) {
        FragmentMandatoryPending fragmentMandatoryPending = new FragmentMandatoryPending();
        Bundle args = new Bundle();
        args.putSerializable("mandatoryInfo", mandatoryInfo);
        args.putSerializable("ddlCompany", ddlCompany);
        args.putString("custDataId", custDataId);
        fragmentMandatoryPending.setArguments(args);
        return fragmentMandatoryPending;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelMandatoryInfo = (ModelMandatoryInfo) getArguments().getSerializable("mandatoryInfo");
            ddlCompany = (ModelDDLCompany) getArguments().getSerializable("ddlCompany");
            custDataId = getArguments().getString("custDataId");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectMandatory = new ObjectMandatoryFull(container, inflater, ddlCompany);
        view = objectMandatory.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if(modelMandatoryInfo.isModified()){
            objectMandatory.setForm(modelMandatoryInfo);
        }else if(custDataId != null && !custDataId.equals("0")){
            getDetailEvidenceFull(custDataId);
        }

        fileNameAkta = objectMandatory.getFileNameAkta();
        fileNameSiup = objectMandatory.getFileNameSiup();
        fileNameDomisili = objectMandatory.getFileNameDomisili();
        fileNameTdp = objectMandatory.getFileNameTdp();
        fileNameTdp = objectMandatory.getFileNameTdp();

        buttonAkta = objectMandatory.getButtonAkta();
        buttonSiup = objectMandatory.getButtonSiup();
        buttonSkDomisili = objectMandatory.getButtonSkDomisili();
        buttontdp = objectMandatory.getButtontdp();

        callCamera(buttonAkta, 1);
        callCamera(buttonSiup, 2);
        callCamera(buttonSkDomisili, 3);
        callCamera(buttontdp, 4);

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[] { Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE }, 0);
        }else{
            objectMandatory.setPermission(0);
        }

        return view;
    }

    private void callCamera(Button button, int idx){
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    takePicture(v, idxButton);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                objectMandatory.setPermission(0);
            }
        }
    }

    public void takePicture(View view, int idx) throws IOException {

        this.idxButton = idx;
        Intent intent = takePicture.getPickImageIntent(context);
        nameFile = takePicture.getNameFile();
        startActivityForResult(intent, 100);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)  {
        File file;
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                switch (idxButton){
                    case 1:
                        if(data != null){
                            try {
                                file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        objectMandatory.setFileNameAkta(nameFile);
                        objectMandatory.setPathFileNameAkta(nameFile);

                        previewImage.setPreviewImage(nameFile, context, fileNameAkta);
                        break;
                    case 2:
                        if(data != null){
                            try {
                                file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        objectMandatory.setFileNameSiup(nameFile);
                        objectMandatory.setPathFileNameSiup(nameFile);

                        previewImage.setPreviewImage(nameFile, context, fileNameSiup);
                        break;
                    case 3:
                        if(data != null){
                            try {
                                file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        objectMandatory.setFileNameDomisili(nameFile);
                        objectMandatory.setPathFileNameDomisili(nameFile);

                        previewImage.setPreviewImage(nameFile, context, fileNameDomisili);
                        break;
                    case 4:
                        if(data != null){
                            try {
                                file = fileFunction.copyFileFromUriToDirPictureBAF(data.getData(), context);

                                nameFile = file.getName();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        objectMandatory.setFileNameTdp(nameFile);
                        objectMandatory.setPathFileNameTdp(nameFile);

                        previewImage.setPreviewImage(nameFile, context, fileNameTdp);
                        break;
                    default:
                        break;
                }

            }
        }
    }

    public ModelMandatoryInfo getForm(){
        return objectMandatory.getForm();
    }

    public void setForm(ModelMandatoryInfo mandatoryInfo){
        objectMandatory.setForm(mandatoryInfo);
    }

    public ModelMandatoryInfo getComplete(){
        return objectMandatory.getComplete();
    }

    private void getDetailEvidenceFull(final String custDataId) {
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailEvidenceFull> evidenceDetail = apiInterface.getDetailFullEvidence(custDataId);
            evidenceDetail.enqueue(new Callback<ResponseDetailEvidenceFull>() {
                @Override
                public void onResponse(Call<ResponseDetailEvidenceFull> call, Response<ResponseDetailEvidenceFull>
                        response) {
                    if (response.isSuccessful()) {
                        modelMandatoryInfo = response.body().getModelMandatoryInfo();
                        modelMandatoryInfo.setModified(true);
                        setForm(modelMandatoryInfo);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailEvidenceFull = gson.fromJson(response.errorBody().string(), ResponseDetailEvidenceFull.class);
                            error = responseDetailEvidenceFull.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder a = new AlertDialog.Builder(context);
                                a.setTitle(getString(R.string.dialog_error_server));
                                a.setCancelable(false);
                                a.setMessage(error+"\n" + getString(R.string.dialog_try_again));
                                a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailEvidenceFull(custDataId);
                                    }
                                });
                            }else if(!error.equals("Data Not Found")){
                                Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }

                        } catch (IOException e) {
                            error = e.getMessage();
                            android.app.AlertDialog.Builder a = new android.app.AlertDialog.Builder(context);
                            a.setTitle(getString(R.string.dialog_error_server));
                            a.setCancelable(false);
                            a.setMessage(error+"\n" +
                                    getString(R.string.dialog_try_again));
                            a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailEvidenceFull(custDataId);
                                }
                            });
                            a.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        android.app.AlertDialog.Builder a = new android.app.AlertDialog.Builder(context);
                        a.setTitle(getString(R.string.dialog_error_server));
                        a.setCancelable(false);
                        a.setMessage("Service response time out\n"+
                                getString(R.string.dialog_try_again));
                        a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailEvidenceFull(custDataId);
                            }
                        });
                        a.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailEvidenceFull> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    android.app.AlertDialog.Builder a = new android.app.AlertDialog.Builder(context);
                    a.setTitle(getString(R.string.dialog_error_server));
                    a.setCancelable(false);
                    a.setMessage(t.toString()+"\n"
                            +getString(R.string.dialog_try_again));
                    a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailEvidenceFull(custDataId);
                        }
                    });
                    a.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            android.app.AlertDialog.Builder a = new android.app.AlertDialog.Builder(context);
            a.setTitle(getString(R.string.dialog_error_server));
            a.setCancelable(false);
            a.setMessage(getString(R.string.dialog_internet_connect)+"\n"
                    +getString(R.string.dialog_try_again));
            a.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            a.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailEvidenceFull(custDataId);
                }
            });
            a.show();
            loadingDialog.dismiss();
        }
    }

}
