package com.example.a0426611017.carsurvey.Fragment.Personal.DetailMiniForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalInfoMini;
import com.example.a0426611017.carsurvey.Object.Personal.DetailMiniForm.ObjectDataPersonalInfoDetailOrder;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseMiniFormPersonal;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDataPersonalInfoDetail extends Fragment {
    private View view;
    private ObjectDataPersonalInfoDetailOrder objectDataPersonalInfoDetailOrder;
    private ModelPersonalInfoMini modelPersonalInfoMini = new ModelPersonalInfoMini();

    private Context context;
    private String personalOrder;
    private Gson gson = new Gson();
    private ProgressDialog progressDialog;
    private ApiInterface apiInterface;

    private ResponseMiniFormPersonal responseMiniFormPersonal = new ResponseMiniFormPersonal();

    public static FragmentDataPersonalInfoDetail newInstance(String personalOrder) {
        FragmentDataPersonalInfoDetail fragmentDataPersonalInfoDetail = new FragmentDataPersonalInfoDetail();
        Bundle args = new Bundle();
        args.putString("personalOrder",personalOrder);
        fragmentDataPersonalInfoDetail.setArguments(args);
        return fragmentDataPersonalInfoDetail;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            personalOrder = getArguments().getString("personalOrder");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectDataPersonalInfoDetailOrder = new ObjectDataPersonalInfoDetailOrder(container, inflater);
        view = objectDataPersonalInfoDetailOrder.getView();
        context = container.getContext();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);
        getDetailPersonalOrder(personalOrder);

        return view;
    }

    public void setForm(ModelPersonalInfoMini modelPersonalInfoMini){
        objectDataPersonalInfoDetailOrder.setForm(modelPersonalInfoMini);
    }

    private void getDetailPersonalOrder(final String personal) {
        progressDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseMiniFormPersonal> personalOrderId = apiInterface.getDetailPersonalMiniForm(personal);
            personalOrderId.enqueue(new Callback<ResponseMiniFormPersonal>() {
                @Override
                public void onResponse(Call<ResponseMiniFormPersonal> call, Response<ResponseMiniFormPersonal> response) {
                    if (response.isSuccessful()) {
                        modelPersonalInfoMini = response.body().getModelPersonalInfoMini();
                        setForm(modelPersonalInfoMini);
                        progressDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseMiniFormPersonal = gson.fromJson(response.errorBody().string(), ResponseMiniFormPersonal.class);
                            error = responseMiniFormPersonal.getMessage();
                            if(!error.equals("Data Not Found")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage("Cannot Connect to Server \n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailPersonalOrder(personal);
                                    }
                                });
                                b.show();
                            }else{
                                Snackbar.make(view.findViewById(R.id.myLayoutPersonal), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage("Cannot Connect to Server \n" +
                                    "Please try Again ?");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailPersonalOrder(personal);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        progressDialog.dismiss();
                    } else {
                        progressDialog.dismiss();
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server \n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailPersonalOrder(personal);
                            }
                        });
                        b.show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseMiniFormPersonal> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server \n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailPersonalOrder(personal);
                        }
                    });
                    b.show();
                    progressDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server \n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailPersonalOrder(personal);
                }
            });
            b.show();
            progressDialog.dismiss();
        }
    }
}
