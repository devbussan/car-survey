package com.example.a0426611017.carsurvey.Fragment.Company.DetailFullForm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.ModelMandatoryInfo;
import com.example.a0426611017.carsurvey.Object.Company.DetailFullForm.ObjectMandatoryInfoDetail;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailCompanyFull;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseDetailEvidenceFull;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentMandatory extends Fragment {
    private static final String ARG_PARAM1 = "param1";

    private Context context;
    private View view;
    private ModelMandatoryInfo modelMandatoryInfo = new ModelMandatoryInfo();
    private ObjectMandatoryInfoDetail objectMandatoryInfo;

    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;
    private Gson gson = new Gson();
    private String custDataId = "0";

    private ResponseDetailCompanyFull responseDetailCompanyFull = new ResponseDetailCompanyFull();
    public FragmentMandatory newInstance(ModelMandatoryInfo mandatoryInfo,String custDataId) {
        FragmentMandatory fragmentMandatory = new FragmentMandatory();
        Bundle args = new Bundle();
        args.putSerializable("mandatoryInfo", mandatoryInfo);
        args.putString(ARG_PARAM1,custDataId);
        fragmentMandatory.setArguments(args);
        return fragmentMandatory;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelMandatoryInfo = (ModelMandatoryInfo) getArguments().getSerializable("mandatoryInfo");
            custDataId = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectMandatoryInfo = new ObjectMandatoryInfoDetail(container, inflater);
        view = objectMandatoryInfo.getView();
        objectMandatoryInfo.setForm(modelMandatoryInfo);
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        getDetailEvidenceFull(custDataId);
        return view;
    }

    public void setForm(ModelMandatoryInfo mandatoryInfo){
        objectMandatoryInfo.setForm(mandatoryInfo);
    }

    private void getDetailEvidenceFull(final String custDataId){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailEvidenceFull> evidenceDetail = apiInterface.getDetailFullEvidence(custDataId);
            evidenceDetail.enqueue(new Callback<ResponseDetailEvidenceFull>() {
                @Override
                public void onResponse(Call<ResponseDetailEvidenceFull> call, Response<ResponseDetailEvidenceFull>
                        response) {
                    if (response.isSuccessful()) {
                        modelMandatoryInfo = response.body().getModelMandatoryInfo();
                        modelMandatoryInfo.setModified(true);
                        setForm(modelMandatoryInfo);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailCompanyFull = gson.fromJson(response.errorBody().string(), ResponseDetailCompanyFull.class);
                            error = responseDetailCompanyFull.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailEvidenceFull(custDataId);
                                    }
                                });
                            }else if(!error.equals("Data Not Found")) {
                                Snackbar.make(view.findViewById(R.id.layout_company_detail), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailEvidenceFull(custDataId);
                                }
                            });
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage(error+"\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailEvidenceFull(custDataId);
                            }
                        });
                        loadingDialog.dismiss();
                    } else {
                        loadingDialog.dismiss();
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage(R.string.error_api+"\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailEvidenceFull(custDataId);
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailEvidenceFull> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.toString()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailEvidenceFull(custDataId);
                        }
                    });
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage(R.string.no_connectivity+"\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailEvidenceFull(custDataId);
                }
            });
            loadingDialog.dismiss();
        }
    }
}
