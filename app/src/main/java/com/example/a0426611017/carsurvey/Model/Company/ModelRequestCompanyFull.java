package com.example.a0426611017.carsurvey.Model.Company;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0414831216 on 2/26/2018.
 */

public class ModelRequestCompanyFull {
    @SerializedName("COMPANYMODEL")
    @Expose
    String companyModel;

    @SerializedName("COMPANYNAME")
    @Expose
    String companyName;

    @SerializedName("COMPANYTYPE")
    @Expose
    String companyType;

    @SerializedName("NPWPNO")
    @Expose
    String nppwNo;

    @SerializedName("INDUSTRYTYPE")
    @Expose
    String industryType;

    @SerializedName("NUMOFEMP")
    @Expose
    String numoFemp;

    @SerializedName("ESTABLISMENTDATE")
    @Expose
    String establisMentdate;

    @SerializedName("USRCRT")
    @Expose
    String usrCrt;

    public String getCompanyModel() {
        return companyModel;
    }

    public void setCompanyModel(String companyModel) {
        this.companyModel = companyModel;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getNppwNo() {
        return nppwNo;
    }

    public void setNppwNo(String nppwNo) {
        this.nppwNo = nppwNo;
    }

    public String getIndustryType() {
        return industryType;
    }

    public void setIndustryType(String industryType) {
        this.industryType = industryType;
    }

    public String getNumoFemp() {
        return numoFemp;
    }

    public void setNumoFemp(String numoFemp) {
        this.numoFemp = numoFemp;
    }

    public String getEstablisMentdate() {
        return establisMentdate;
    }

    public void setEstablisMentdate(String establisMentdate) {
        this.establisMentdate = establisMentdate;
    }

    public String getUsrCrt() {
        return usrCrt;
    }

    public void setUsrCrt(String usrCrt) {
        this.usrCrt = usrCrt;
    }
}
