package com.example.a0426611017.carsurvey.Fragment.Personal.DetailFullForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataJobData;
import com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm.ObjectDataJobDataComplete;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalJob;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDataJobDataComplete extends Fragment {
    private View view;
    private ObjectDataJobDataComplete objectDataJobDataComplete;
    private ModelDataJobData modelDataJobData = new ModelDataJobData();

    private String name;
    private Context context;
    private String custid;
    private Gson gson = new Gson();
    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;

    private ResponseDetailPersonalJob responseDetailPersonalJob = new ResponseDetailPersonalJob();

    public FragmentDataJobDataComplete newInstance(String custid, String name){
        FragmentDataJobDataComplete fragmentDataJobDataComplete = new FragmentDataJobDataComplete();
        Bundle args = new Bundle();
        args.putString("custid", custid);
        args.putString("name", name);
        fragmentDataJobDataComplete.setArguments(args);
        return fragmentDataJobDataComplete;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            custid = getArguments().getString("custid");
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        objectDataJobDataComplete = new ObjectDataJobDataComplete(viewGroup, inflater);
        view = objectDataJobDataComplete.getView();

        context = viewGroup.getContext();

        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading.........");
        loadingDialog.setCancelable(false);

        getDetailJob(name, custid);

        return view;
    }

    public void setForm(ModelDataJobData modelDataJobData){
        objectDataJobDataComplete.setForm(modelDataJobData);
    }

    private void getDetailJob(final String name, String idcustdata){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalJob> jobDetail = apiInterface.getDetailPersonalJob(name, idcustdata);
            jobDetail.enqueue(new Callback<ResponseDetailPersonalJob>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalJob> call, Response<ResponseDetailPersonalJob>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataJobData = response.body().getModelDataJobData();
                        Log.d("Result", gson.toJson(modelDataJobData));
                        modelDataJobData.setModified(true);
                        setForm(modelDataJobData);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalJob = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalJob.class);
                            error = responseDetailPersonalJob.getMessage();
                            if(!error.equals("Data Not Found")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage("Cannot Connect to Server \n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailJob(name, custid);
                                    }
                                });
                                b.show();
                            }else{
                                Snackbar.make(view.findViewById(R.id.job_data_detail), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage("Cannot Connect to Server \n" +
                                    "Please try Again ?");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailJob(name, custid);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);

                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server \n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailJob(name, custid);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalJob> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server \n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailJob(name, custid);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server \n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailJob(name, custid);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }
    }
}
