package com.example.a0426611017.carsurvey.Fragment.Company.DetailMiniForm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Object.Company.DetailMiniForm.ObjectContactInfo5DetailOrder;
import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Contact.ResponseDetailContactInfo;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentContactInfo5DetailOrder extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";


    private Context context;
    private View view;
    private ObjectContactInfo5DetailOrder objectContactInfo5DetailOrder;

    private ProgressDialog loadingDialog;
    private ApiInterface apiInterface;
    private Gson gson = new Gson();

    private String companyOrderId = "0";
    private String contactOrderId = "0";

    private ModelDetailContactInfo modelDetailContactInfo5 = new ModelDetailContactInfo();
    private ResponseDetailContactInfo responseDetailContactInfo = new ResponseDetailContactInfo();



    public static FragmentContactInfo5DetailOrder newInstance(String contactOrderId, String companyOrderId){
        FragmentContactInfo5DetailOrder fragment = new FragmentContactInfo5DetailOrder();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1,contactOrderId);
        args.putString(ARG_PARAM2,companyOrderId);

        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface=ApiClient.getClient().create(ApiInterface.class);
        if(getArguments() != null){
            contactOrderId = getArguments().getString(ARG_PARAM1);
            companyOrderId = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectContactInfo5DetailOrder = new ObjectContactInfo5DetailOrder(container, inflater);
        view = objectContactInfo5DetailOrder.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);
        getDetailContact(contactOrderId,companyOrderId);

        return view;
    }

    public void setForm(ModelDetailContactInfo detailContactInfo){
        objectContactInfo5DetailOrder.setForm(detailContactInfo);
    }

    private void getDetailContact(final String contactOrderId, final String companyOrderId){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailContactInfo> contactOrderID = apiInterface.getDetailContactInfo(contactOrderId, companyOrderId);
            contactOrderID.enqueue(new Callback<ResponseDetailContactInfo>() {
                @Override
                public void onResponse(Call<ResponseDetailContactInfo> call, Response<ResponseDetailContactInfo>
                        response) {
                    if (response.isSuccessful()) {
                        modelDetailContactInfo5 = response.body().getModelDetailContactInfo();
                        setForm(modelDetailContactInfo5);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailContactInfo = gson.fromJson(response.errorBody().string(), ResponseDetailContactInfo.class);
                            error = responseDetailContactInfo.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(getString(R.string.dialog_error_server));
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        getString(R.string.dialog_try_again));
                                b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailContact(contactOrderId,companyOrderId);
                                    }
                                });
                            }else if(!error.equals("Data Not Found")) {
                                Snackbar.make(view.findViewById(R.id.myLayoutCompany), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(getString(R.string.dialog_error_server));
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    getString(R.string.dialog_try_again));
                            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailContact(contactOrderId,companyOrderId);
                                }
                            });
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(getString(R.string.dialog_error_server));
                        b.setCancelable(false);
                        b.setMessage(error+"\n" +
                                getString(R.string.dialog_try_again));
                        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailContact(contactOrderId,companyOrderId);
                            }
                        });
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(getString(R.string.dialog_error_server));
                        b.setCancelable(false);
                        b.setMessage(R.string.error_api+"\n" +
                                getString(R.string.dialog_try_again));
                        b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailContact(contactOrderId,companyOrderId);
                            }
                        });
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailContactInfo> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle(getString(R.string.dialog_error_server));
                    b.setCancelable(false);
                    b.setMessage(t.toString()+"\n" +
                            getString(R.string.dialog_try_again));
                    b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailContact(contactOrderId,companyOrderId);
                        }
                    });
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(getString(R.string.dialog_error_server));
            b.setCancelable(false);
            b.setMessage(getString(R.string.no_connectivity)+"\n" +
                    getString(R.string.dialog_try_again));
            b.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailContact(contactOrderId,companyOrderId);
                }
            });
            loadingDialog.dismiss();
        }
    }
}
