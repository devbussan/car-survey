package com.example.a0426611017.carsurvey.Object.Personal.MiniForm;

import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalInfoMini;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;

import java.util.List;

/**
 * Created by 0426591017 on 2/19/2018.
 */

public class ObjectDataPersonalInfo {
    private View view;
    private Context context;
    private ViewGroup container;
    private ModelDDLPersonal ddlPersonal;

    private EditText customerName, idNumber, familyCardNum;

    private TextView textViewIdImg, textViewPersonalFamilyCardImg,
                    textViewIdImgUrl, textViewPersonalFamilyCardImgUrl;

    private Button buttonIdImage, buttonPersonalFamilyCard;

    private Spinner idType;

    private TextInputEditText textName;
    private TextInputLayout textInputLayout;

    public ObjectDataPersonalInfo(ViewGroup viewGroup, LayoutInflater inflater, ModelDDLPersonal ddlPersonal) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.ddlPersonal = ddlPersonal;
        this.view = inflater.inflate(R.layout.fragment_data_personal_info_order, container, false);

        setField();
    }

    public View getView() {
        return view;
    }

    public TextView getTextViewIdImg() {
        return textViewIdImg;
    }

    public TextView getTextViewPersonalFamilyCardImg() {
        return textViewPersonalFamilyCardImg;
    }

    public TextView getTextViewIdImgUrl() {
        return textViewIdImgUrl;
    }

    public TextView getTextViewPersonalFamilyCardImgUrl() {
        return textViewPersonalFamilyCardImgUrl;
    }

    public void setTextViewIdImg(String textViewIdImg) {
        this.textViewIdImg.setText(textViewIdImg);
    }

    public void setTextViewPersonalFamilyCardImg(String filePersonalFamilyCardImg) {
        this.textViewPersonalFamilyCardImg.setText(filePersonalFamilyCardImg);
    }

    public void setTextViewIdImgUrl(String textViewIdImgUrl) {
        this.textViewIdImgUrl.setText(textViewIdImgUrl);
    }

    public void setTextViewPersonalFamilyCardImgUrl(String textViewPersonalFamilyCardImgUrl) {
        this.textViewPersonalFamilyCardImgUrl.setText(textViewPersonalFamilyCardImgUrl);
    }

    public Button getButtonIdImage() {
        return buttonIdImage;
    }

    public Button getBbuttonPersonalFamilyCard() {
        return buttonPersonalFamilyCard;
    }

    public void setEnabledButtonImage(){
        buttonIdImage.setEnabled(true);
        buttonPersonalFamilyCard.setEnabled(true);
    }

    public void setField() {
        customerName = view.findViewById(R.id.edit_text_customer_name_add_new);
        idType = view.findViewById(R.id.spinner_id_type_add_new);

        idNumber = view.findViewById(R.id.edit_text_id_number_add_new);
        idNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 16){
                    idNumber.setError("Minimum 16 digit");
                }
            }
        });

        textViewIdImg = (TextView) view.findViewById(R.id.text_view_id_image_add_new);
        textViewIdImg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    textViewIdImg.setError(null);
                }
            }
        });
        textViewIdImgUrl = (TextView) view.findViewById(R.id.text_view_id_image_url_add_new);

        familyCardNum = view.findViewById(R.id.edit_text_family_card_number_add_new);
        familyCardNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 16){
                    familyCardNum.setError("Minimum 16 digit");
                }
            }
        });

        textViewPersonalFamilyCardImg = (TextView) view.findViewById(R.id.text_view_familiy_card_number_image_add_new);
        textViewPersonalFamilyCardImg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    textViewPersonalFamilyCardImg.setError(null);
                }
            }
        });
        textViewPersonalFamilyCardImgUrl = (TextView) view.findViewById(R.id.text_view_familiy_card_number_image_url_add_new);

        buttonIdImage = (Button) view.findViewById(R.id.button_camera_id_image_add_new);
        buttonPersonalFamilyCard = (Button) view.findViewById(R.id.button_camera_family_card_number_image_add_new);

        if(ddlPersonal != null){
            idType.setAdapter(setAdapterItemList(ddlPersonal.getIdType()));
        }

        buttonIdImage.setEnabled(false);
        buttonPersonalFamilyCard.setEnabled(false);
    }

    public void setForm(ModelPersonalInfoMini modelPersonalInfoMini) {
        Log.d("DDL PERSONAL", ddlPersonal.getCustomerModel().toString());
        Log.d("Url image", "asd"+modelPersonalInfoMini.getIdimgurl());
        if (ddlPersonal != null) {
            idType.setSelection(ddlPersonal.getIdType().indexOf(modelPersonalInfoMini.getIdtype()));
        }

        customerName.setText(modelPersonalInfoMini.getFullname());
        idNumber.setText(modelPersonalInfoMini.getIdno());

        textViewIdImgUrl.setText(modelPersonalInfoMini.getIdimgurl());
        Log.d("Url image", textViewIdImgUrl.getText().toString());
        PreviewImage previewImage = new PreviewImage();
        String nameIdImage = "";
        if (modelPersonalInfoMini.getIdimgurl().equals("") || modelPersonalInfoMini.getIdimgurl() == null) {
            nameIdImage = "No selected file";
        } else {

            if (modelPersonalInfoMini.getIdimgurl().startsWith("http")) {
                nameIdImage = modelPersonalInfoMini.getIdimgurl().split("/")[6];
                previewImage.setPreviewImage(modelPersonalInfoMini.getIdimgurl(), context, textViewIdImg);
            } else {
                nameIdImage = modelPersonalInfoMini.getIdimg();
                previewImage.setPreviewImage(nameIdImage, context, textViewIdImg);
            }
        }
        textViewIdImg.setText(nameIdImage);

        familyCardNum.setText(modelPersonalInfoMini.getKkno());

        textViewPersonalFamilyCardImgUrl.setText(modelPersonalInfoMini.getKkimgurl());
        Log.d("Url image", textViewPersonalFamilyCardImgUrl.getText().toString());
        PreviewImage previewFamilyCardImage = new PreviewImage();
        String namePersonalFamilyCardImage = "";
        if (modelPersonalInfoMini.getKkimgurl().equals("") || modelPersonalInfoMini.getKkimgurl() == null) {
            namePersonalFamilyCardImage = "No selected file";
        } else {
            if (modelPersonalInfoMini.getKkimgurl().startsWith("http")) {
                namePersonalFamilyCardImage = modelPersonalInfoMini.getKkimgurl().split("/")[6];
                previewFamilyCardImage.setPreviewImage(modelPersonalInfoMini.getKkimgurl(), context, textViewPersonalFamilyCardImg);
            } else {
                namePersonalFamilyCardImage = modelPersonalInfoMini.getKkimg();
                previewFamilyCardImage.setPreviewImage(namePersonalFamilyCardImage, context, textViewPersonalFamilyCardImg);
            }
        }
        textViewPersonalFamilyCardImg.setText(namePersonalFamilyCardImage);

    }

    public ModelPersonalInfoMini getForm(){
        ModelPersonalInfoMini modelPersonalInfoMini = new ModelPersonalInfoMini();

        modelPersonalInfoMini.setFullname(customerName.getText().toString());
        modelPersonalInfoMini.setIdtype(idType.getSelectedItem().toString());
        modelPersonalInfoMini.setIdno(idNumber.getText().toString());
        modelPersonalInfoMini.setIdimg(textViewIdImg.getText().toString());
        modelPersonalInfoMini.setIdimgurl(textViewIdImgUrl.getText().toString());
        modelPersonalInfoMini.setKkno(familyCardNum.getText().toString());
        modelPersonalInfoMini.setKkimg(textViewPersonalFamilyCardImg.getText().toString());
        modelPersonalInfoMini.setKkimgurl(textViewPersonalFamilyCardImgUrl.getText().toString());

        modelPersonalInfoMini.setModified(true);

        return modelPersonalInfoMini;
    }

    private ArrayAdapter<String> setAdapterItemList(List items) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl, items);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;
    }

    public ModelPersonalInfoMini getComplete() {
        ModelPersonalInfoMini modelPersonalInfoMini = getForm();

        Log.d("asuk ka get complete","ceck model satu-satu");

        modelPersonalInfoMini.setComplete(true);
        if (modelPersonalInfoMini.getFullname().length() == 0) {
            customerName.setError("Customer Name is Required");
            modelPersonalInfoMini.setComplete(false);
        } else if (modelPersonalInfoMini.getFullname().trim().length() == 0) {
            customerName.setError("Customer Name not allowed whitespaces only");
            modelPersonalInfoMini.setComplete(false);
        }

        if (modelPersonalInfoMini.getIdno().length() == 0) {
            idNumber.setError("ID Number is Required");
            modelPersonalInfoMini.setComplete(false);
        } else if (modelPersonalInfoMini.getIdno().trim().length() == 0) {
            idNumber.setError("ID Number not allowed whitespaces only");
            modelPersonalInfoMini.setComplete(false);
        } else if (modelPersonalInfoMini.getIdno().trim().length() != 16) {
            idNumber.setError("Minimum and maximum 16 digits");
            modelPersonalInfoMini.setComplete(false);
        }

        if(modelPersonalInfoMini.getIdimg().length() == 0){
            textViewIdImg.setError("ID Image is Required");
            modelPersonalInfoMini.setComplete(false);
        }else if(modelPersonalInfoMini.getIdimg().equals("No selected file")){
            textViewIdImg.setError("ID Image is Required");
            modelPersonalInfoMini.setComplete(false);
        }else {
            textViewIdImg.setError(null);
        }

        if (modelPersonalInfoMini.getKkno().length() == 0) {
            familyCardNum.setError("Family Card Number is Required");
            modelPersonalInfoMini.setComplete(false);
        } else if (modelPersonalInfoMini.getKkno().trim().length() == 0) {
            familyCardNum.setError("Family Card Number not allowed whitespaces only");
            modelPersonalInfoMini.setComplete(false);
        } else if (modelPersonalInfoMini.getKkno().trim().length() != 16) {
            familyCardNum.setError("Minimum and maximum 16 digits");
            modelPersonalInfoMini.setComplete(false);
        }

        if(modelPersonalInfoMini.getKkimg().length() == 0){
            textViewPersonalFamilyCardImg.setError("Family Card Image is Required");
            modelPersonalInfoMini.setComplete(false);
        }else if(modelPersonalInfoMini.getKkimg().equals("No selected file")){
            textViewPersonalFamilyCardImg.setError("Family Card Image is Required");
            modelPersonalInfoMini.setComplete(false);
        }else{
            textViewPersonalFamilyCardImg.setError(null);
        }

        return modelPersonalInfoMini;
    }
}