package com.example.a0426611017.carsurvey.ResponseObjectAPI;

import com.example.a0426611017.carsurvey.Model.ModelDashboard;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 13/03/2018.
 */

public class ResponseDashboard {

    @SerializedName("status")
    private String status;

    @SerializedName("result")
    private ModelDashboard modelDashboard;

    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDashboard getModelDashboard() {
        return modelDashboard;
    }

    public void setModelDashboard(ModelDashboard modelDashboard) {
        this.modelDashboard = modelDashboard;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
