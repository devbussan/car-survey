package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonalSG;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426591017 on 3/7/2018.
 */

public class ResponseDetailPersonalSgInfo {
    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDataPersonalSG modelDataPersonalSG;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDataPersonalSG getModelDataPersonalSG() {
        return modelDataPersonalSG;
    }

    public void setModelDataPersonalSG(ModelDataPersonalSG modelDataPersonalSG) {
        this.modelDataPersonalSG = modelDataPersonalSG;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
