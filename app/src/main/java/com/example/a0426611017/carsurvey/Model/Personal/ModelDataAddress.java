package com.example.a0426611017.carsurvey.Model.Personal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 0426611017 on 2/9/2018.
 */

public class ModelDataAddress implements Serializable {
    @SerializedName("CUST_DATA_ID")
    private String cust_data_id;

    @SerializedName("PERSONAL_ADDR_ID")
    private String personal_addr_id;

    @SerializedName("ADDRESSTYPENAME")
    private String addresstypename;

    @SerializedName("CUSTADDR")
    private String custaddr;

    @SerializedName("CUSTRT")
    private String custrt;

    @SerializedName("CUSTRW")
    private String custrw;

    @SerializedName("ZIPCODE")
    private String zipcode;

    @SerializedName("KELURAHAN")
    private String kelurahan;

    @SerializedName("KECAMATAN")
    private String kecamatan;

    @SerializedName("CITY")
    private String city;

    @SerializedName("PHONE")
    private String phone;

    @SerializedName("FAX")
    private String fax;

    @SerializedName("BLDLOCCLASS")
    private String bldlocclass;

    @SerializedName("BLDOWNER")
    private String bldowner;

    @SerializedName("BLDIMGURL")
    private String bldimgurl;

    private String bldimg;

    @SerializedName("BLDPRICEEST")
    private String bldpriceest;

    @SerializedName("STAYLENGTH")
    private String staylength;

    @SerializedName("DIRECTIONDESC")
    private String directiondesc;

    @SerializedName("NOTES")
    private String notes;

    @SerializedName("USRCRT")
    private String usrcrt;

    @SerializedName("STATUS")
    private String status;

    @SerializedName("PERSONAL_ADDR")
    private String personal_addr_return;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getCust_data_id() {
        return cust_data_id;
    }

    public void setCust_data_id(String cust_data_id) {
        this.cust_data_id = cust_data_id;
    }

    public String getPersonal_addr_id() {
        return personal_addr_id;
    }

    public void setPersonal_addr_id(String personal_addr_id) {
        this.personal_addr_id = personal_addr_id;
    }

    public String getAddresstypename() {
        return addresstypename;
    }

    public void setAddresstypename(String addresstypename) {
        this.addresstypename = addresstypename;
    }

    public String getCustaddr() {
        return custaddr;
    }

    public void setCustaddr(String custaddr) {
        this.custaddr = custaddr;
    }

    public String getCustrt() {
        return custrt;
    }

    public void setCustrt(String custrt) {
        this.custrt = custrt;
    }

    public String getCustrw() {
        return custrw;
    }

    public void setCustrw(String custrw) {
        this.custrw = custrw;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getBldlocclass() {
        return bldlocclass;
    }

    public void setBldlocclass(String bldlocclass) {
        this.bldlocclass = bldlocclass;
    }

    public String getBldowner() {
        return bldowner;
    }

    public void setBldowner(String bldowner) {
        this.bldowner = bldowner;
    }

    public String getBldimgurl() {
        return bldimgurl;
    }

    public void setBldimgurl(String bldimgurl) {
        this.bldimgurl = bldimgurl;
    }

    public String getBldimg() {
        return bldimg;
    }

    public void setBldimg(String bldimg) {
        this.bldimg = bldimg;
    }

    public String getBldpriceest() {
        return bldpriceest;
    }

    public void setBldpriceest(String bldpriceest) {
        this.bldpriceest = bldpriceest;
    }

    public String getStaylength() {
        return staylength;
    }

    public void setStaylength(String staylength) {
        this.staylength = staylength;
    }

    public String getDirectiondesc() {
        return directiondesc;
    }

    public void setDirectiondesc(String directiondesc) {
        this.directiondesc = directiondesc;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getUsrcrt() {
        return usrcrt;
    }

    public void setUsrcrt(String usrcrt) {
        this.usrcrt = usrcrt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getPersonal_addr_return() {
        return personal_addr_return;
    }

    public void setPersonal_addr_return(String personal_addr_return) {
        this.personal_addr_return = personal_addr_return;
    }
}
