package com.example.a0426611017.carsurvey.Model.DDL;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by c201017001 on 08/02/2018.
 */

public class ModelDDLSurvey implements Serializable{

    @SerializedName("LOKASI_SURVEY")
    private List<String> lokasiSurvey;

    @SerializedName("SOURCE_INCOMING")
    private List<String> sourceIncoming;

    @SerializedName("SOURCE_HUBUNGAN_KONSUMEN")
    private List<String> sourceHubunganKonsumen;

    @SerializedName("PENGAJUAN_CALON_DEBITUR")
    private List<String> pengajuanCalonDebitur;

    @SerializedName("TIPE_RUMAH")
    private List<String> tipeRumah;

    @SerializedName("LOKASI_RUMAH")
    private List<String> lokasiRumah;

    @SerializedName("JENIS_BANGUNAN")
    private List<String> jenisBangunan;

    @SerializedName("KONDISI_BANGUNAN")
    private List<String> kondisiBangunan;

    @SerializedName("KETERSEDIAAN_GARASI")
    private List<String> ketersediaanGarasi;

    @SerializedName("RLISTRIK_HUBUNGAN_KONSUMEN")
    private List<String> rListrikHubunganKonsumen;

    @SerializedName("ANGGOTA_ORMAS")
    private List<String> anggotaOrmas;

    @SerializedName("STATUS_KEPEMILIKAN_MOBIL")
    private List<String> statusKepemilikanMobil;

    @SerializedName("STATUS_KEPEMILIKAN_MOTOR")
    private List<String> statusKepemilikanMotor;

    @SerializedName("KARAKTER_KONSUMEN")
    private List<String> karakterKonsumen;

    public List<String> getLokasiSurvey() {
        return lokasiSurvey;
    }

    public void setLokasiSurvey(List<String> lokasiSurvey) {
        this.lokasiSurvey = lokasiSurvey;
    }

    public List<String> getSourceIncoming() {
        return sourceIncoming;
    }

    public void setSourceIncoming(List<String> sourceIncoming) {
        this.sourceIncoming = sourceIncoming;
    }

    public List<String> getSourceHubunganKonsumen() {
        return sourceHubunganKonsumen;
    }

    public void setSourceHubunganKonsumen(List<String> sourceHubunganKonsumen) {
        this.sourceHubunganKonsumen = sourceHubunganKonsumen;
    }

    public List<String> getPengajuanCalonDebitur() {
        return pengajuanCalonDebitur;
    }

    public void setPengajuanCalonDebitur(List<String> pengajuanCalonDebitur) {
        this.pengajuanCalonDebitur = pengajuanCalonDebitur;
    }

    public List<String> getTipeRumah() {
        return tipeRumah;
    }

    public void setTipeRumah(List<String> tipeRumah) {
        this.tipeRumah = tipeRumah;
    }

    public List<String> getLokasiRumah() {
        return lokasiRumah;
    }

    public void setLokasiRumah(List<String> lokasiRumah) {
        this.lokasiRumah = lokasiRumah;
    }

    public List<String> getJenisBangunan() {
        return jenisBangunan;
    }

    public void setJenisBangunan(List<String> jenisBangunan) {
        this.jenisBangunan = jenisBangunan;
    }

    public List<String> getKondisiBangunan() {
        return kondisiBangunan;
    }

    public void setKondisiBangunan(List<String> kondisiBangunan) {
        this.kondisiBangunan = kondisiBangunan;
    }

    public List<String> getKetersediaanGarasi() {
        return ketersediaanGarasi;
    }

    public void setKetersediaanGarasi(List<String> ketersediaanGarasi) {
        this.ketersediaanGarasi = ketersediaanGarasi;
    }

    public List<String> getrListrikHubunganKonsumen() {
        return rListrikHubunganKonsumen;
    }

    public void setrListrikHubunganKonsumen(List<String> rListrikHubunganKonsumen) {
        this.rListrikHubunganKonsumen = rListrikHubunganKonsumen;
    }

    public List<String> getAnggotaOrmas() {
        return anggotaOrmas;
    }

    public void setAnggotaOrmas(List<String> anggotaOrmas) {
        this.anggotaOrmas = anggotaOrmas;
    }

    public List<String> getStatusKepemilikanMobil() {
        return statusKepemilikanMobil;
    }

    public void setStatusKepemilikanMobil(List<String> statusKepemilikanMobil) {
        this.statusKepemilikanMobil = statusKepemilikanMobil;
    }

    public List<String> getStatusKepemilikanMotor() {
        return statusKepemilikanMotor;
    }

    public void setStatusKepemilikanMotor(List<String> statusKepemilikanMotor) {
        this.statusKepemilikanMotor = statusKepemilikanMotor;
    }

    public List<String> getKarakterKonsumen() {
        return karakterKonsumen;
    }

    public void setKarakterKonsumen(List<String> karakterKonsumen) {
        this.karakterKonsumen = karakterKonsumen;
    }
}
