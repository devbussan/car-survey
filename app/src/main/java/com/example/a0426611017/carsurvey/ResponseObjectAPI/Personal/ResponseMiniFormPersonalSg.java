package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalSGMini;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426611017 on 2/21/2018.
 */

public class ResponseMiniFormPersonalSg {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelPersonalSGMini modelPersonalSGMini;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelPersonalSGMini getModelPersonalSGMini() {
        return modelPersonalSGMini;
    }

    public void setModelPersonalSGMini(ModelPersonalSGMini modelPersonalSGMini) {
        this.modelPersonalSGMini = modelPersonalSGMini;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
