package com.example.a0426611017.carsurvey.Fragment.Personal.FullForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataEmergencyContact;
import com.example.a0426611017.carsurvey.Object.Personal.FullForm.ObjectDataEmergencyContactPending;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseDetailPersonalEmergency;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FragmentDataEmergencyContactPending extends Fragment {
    private View view;
    private Context context;

    private ObjectDataEmergencyContactPending objectDataEmergencyContactPending;
    private ModelDataEmergencyContact modelDataEmergencyContact = new ModelDataEmergencyContact();

    private ApiInterface apiInterface;
    private ProgressDialog loadingDialog;
    private String personalCustDataId = null;
    private String name = "";
    private Gson gson = new Gson();
    private boolean isNew = true;

    private ResponseDetailPersonalEmergency responseDetailPersonalEmergency = new ResponseDetailPersonalEmergency();

    public FragmentDataEmergencyContactPending newInstance(ModelDataEmergencyContact modelDataEmergencyContact, boolean isNew, String custId, String name) {
        FragmentDataEmergencyContactPending fragmentDataEmergencyContactPending = new FragmentDataEmergencyContactPending();
        Bundle args = new Bundle();
        args.putSerializable("modelEmergencyPersonal", modelDataEmergencyContact);
        args.putString("personalCustDataId", custId);
        args.putBoolean(Constans.KEY_IS_NEW, isNew);
        args.putString("name",name);
        fragmentDataEmergencyContactPending.setArguments(args);
        return fragmentDataEmergencyContactPending;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            modelDataEmergencyContact = (ModelDataEmergencyContact) getArguments().getSerializable("modelEmergencyPersonal");
            personalCustDataId = getArguments().getString("personalCustDataId");
            isNew = getArguments().getBoolean(Constans.KEY_IS_NEW);
            name = getArguments().getString("name");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        objectDataEmergencyContactPending = new ObjectDataEmergencyContactPending(container, inflater);
        view = objectDataEmergencyContactPending.getView();
        context = container.getContext();

        loadingDialog = new ProgressDialog(context);
        loadingDialog.setMessage("Loading......");
        loadingDialog.setCancelable(false);

        if(modelDataEmergencyContact.isModified()){
            objectDataEmergencyContactPending.setForm(modelDataEmergencyContact);
        }else{
            if (!isNew) {
                getDetailEmergency(name, personalCustDataId);
            }
        }

        return view;
    }

    public ModelDataEmergencyContact getForm(){
        return objectDataEmergencyContactPending.getForm();
    }

    public void setForm(ModelDataEmergencyContact modelDataEmergencyContact){
        objectDataEmergencyContactPending.setForm(modelDataEmergencyContact);
    }

    public ModelDataEmergencyContact getComplete(){
        return objectDataEmergencyContactPending.getComplete();
    }

    private void getDetailEmergency(final String name, final String idcustdata){
        loadingDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseDetailPersonalEmergency> emergencyDetail = apiInterface.getDetailPersonalEmergency(name, idcustdata);
            emergencyDetail.enqueue(new Callback<ResponseDetailPersonalEmergency>() {
                @Override
                public void onResponse(Call<ResponseDetailPersonalEmergency> call, Response<ResponseDetailPersonalEmergency>
                        response) {
                    if (response.isSuccessful()) {
                        modelDataEmergencyContact = response.body().getModelDataEmergency();
                        Log.d("Result", gson.toJson(modelDataEmergencyContact));

                        setForm(modelDataEmergencyContact);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseDetailPersonalEmergency = gson.fromJson(response.errorBody().string(), ResponseDetailPersonalEmergency.class);
                            error = responseDetailPersonalEmergency.getMessage();
                            if(error.equals("Response Time Out")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage(error+"\n" +
                                        "Try Again ?");
                                b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {

                                    }
                                });
                                b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailEmergency(name, idcustdata);
                                    }
                                });
                                b.show();
                            }else if (!error.equals("Data Not Found"))
                                Snackbar.make(view.findViewById(R.id.personal_emergency_full_edit), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage(error+"\n" +
                                    "Try Again ?");
                            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });
                            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailEmergency(name, idcustdata);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Service response time out\n" +
                                "Try Again ?");
                        b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        });
                        b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailEmergency(name, idcustdata);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseDetailPersonalEmergency> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage(t.getMessage()+"\n" +
                            "Try Again ?");
                    b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailEmergency(name, idcustdata);
                        }
                    });
                    b.show();
                    loadingDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("No Internet Connectivity\n" +
                    "Try Again ?");
            b.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            b.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailEmergency(name, idcustdata);
                }
            });
            b.show();
            Log.e("Error","No Internet Connectivity");
            loadingDialog.dismiss();
        }
    }
}
