package com.example.a0426611017.carsurvey.Fragment.Personal.DetailMiniForm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalSGMini;
import com.example.a0426611017.carsurvey.Object.Personal.DetailMiniForm.ObjectDataPersonalSGDetailOrder;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponseMiniFormPersonalSg;
import com.google.gson.Gson;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentDataPersonalSGDetail extends Fragment {
    private View view;
    private ObjectDataPersonalSGDetailOrder objectDataPersonalSGDetailOrder;
    private ModelPersonalSGMini modelPersonalSGMini = new ModelPersonalSGMini();

    private Context context;
    private String personalOrder;
    private Gson gson = new Gson();
    private ProgressDialog progressDialog;
    private ApiInterface apiInterface;

    private ResponseMiniFormPersonalSg responseMiniFormPersonalSg = new ResponseMiniFormPersonalSg();

    public static FragmentDataPersonalSGDetail newInstance(String personalOrder){
        FragmentDataPersonalSGDetail fragmentDataPersonalSGDetail = new FragmentDataPersonalSGDetail();
        Bundle args = new Bundle();
        args.putString("personalOrder", personalOrder);
        fragmentDataPersonalSGDetail.setArguments(args);
        return fragmentDataPersonalSGDetail;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        if (getArguments() != null) {
            personalOrder = getArguments().getString("personalOrder");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        objectDataPersonalSGDetailOrder = new ObjectDataPersonalSGDetailOrder(container, inflater);
        view = objectDataPersonalSGDetailOrder.getView();
        context = container.getContext();

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);

        getDetailPersonalSgOrder(personalOrder);

        return view;
    }

    public void setForm(ModelPersonalSGMini modelPersonalSGMini){
        objectDataPersonalSGDetailOrder.setForm(modelPersonalSGMini);
    }

    private void getDetailPersonalSgOrder(final String personal) {
        progressDialog.show();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseMiniFormPersonalSg> personalOrderId = apiInterface.getDetailPersonalSgMiniForm(personal);
            personalOrderId.enqueue(new Callback<ResponseMiniFormPersonalSg>() {
                @Override
                public void onResponse(Call<ResponseMiniFormPersonalSg> call, Response<ResponseMiniFormPersonalSg> response) {
                    if (response.isSuccessful()) {
                        modelPersonalSGMini = response.body().getModelPersonalSGMini();
                        setForm(modelPersonalSGMini);
                        progressDialog.dismiss();
                    } else if (response.code() == 404) {
                        String error = "";
                        try {
                            responseMiniFormPersonalSg = gson.fromJson(response.errorBody().string(), ResponseMiniFormPersonalSg.class);
                            error = responseMiniFormPersonalSg.getMessage();
                            if(!error.equals("Data Not Found")){
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle("Error Server");
                                b.setCancelable(false);
                                b.setMessage("Cannot Connect to Server \n" +
                                        "Please try Again ?");
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        getDetailPersonalSgOrder(personal);
                                    }
                                });
                                b.show();
                            }else{
                                Snackbar.make(view.findViewById(R.id.sgDetailLayout), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                            }

                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle("Error Server");
                            b.setCancelable(false);
                            b.setMessage("Cannot Connect to Server \n" +
                                    "Please try Again ?");
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getDetailPersonalSgOrder(personal);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);

                        progressDialog.dismiss();
                    } else {
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle("Error Server");
                        b.setCancelable(false);
                        b.setMessage("Cannot Connect to Server \n" +
                                "Please try Again ?");
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                getDetailPersonalSgOrder(personal);
                            }
                        });
                        b.show();
                        progressDialog.dismiss();

                    }
                }
                @Override
                public void onFailure(Call<ResponseMiniFormPersonalSg> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle("Error Server");
                    b.setCancelable(false);
                    b.setMessage("Cannot Connect to Server \n" +
                            "Please try Again ?");
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getDetailPersonalSgOrder(personal);
                        }
                    });
                    b.show();
                    progressDialog.dismiss();
                }
            });
        } else {
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle("Error Server");
            b.setCancelable(false);
            b.setMessage("Cannot Connect to Server \n" +
                    "Please try Again ?");
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getDetailPersonalSgOrder(personal);
                }
            });
            b.show();
            progressDialog.dismiss();
        }
    }
}
