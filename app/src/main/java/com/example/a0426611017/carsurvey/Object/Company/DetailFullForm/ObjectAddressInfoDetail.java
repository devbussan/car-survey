package com.example.a0426611017.carsurvey.Object.Company.DetailFullForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Currency;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Company.ModelAddressInfo;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by c201017001 on 04/01/2018.
 */

public class ObjectAddressInfoDetail {

    private ViewGroup container;
    private LayoutInflater inflater;
    private View view;
    private Context context;

    private TextView address, rt, rw, zipcode, kelurahan, kecamatan, city, phone, fax,
            nameFileBuildingImage, addressType, price, stayLength, description, notes,
            locationClass, ownership;

    private PreviewImage previewImage = new PreviewImage();

    public ObjectAddressInfoDetail(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        this.view = inflater.inflate(R.layout.company_detail_address_info, viewGroup, false);
        this.container = viewGroup;
        this.inflater = inflater;
        findView();
    }

    public View getView() {
        return view;
    }

    private void findView() {
        address = view.findViewById(R.id.address);
        rt = view.findViewById(R.id.rt);
        rw = view.findViewById(R.id.rw);
        zipcode = view.findViewById(R.id.zipcode);
        kelurahan = view.findViewById(R.id.kelurahan);
        kecamatan = view.findViewById(R.id.kecamatan);
        city = view.findViewById(R.id.city);
        phone = view.findViewById(R.id.phone);
        fax = view.findViewById(R.id.fax);

        addressType = view.findViewById(R.id.addressType);

        nameFileBuildingImage = view.findViewById(R.id.locationClassImage);
        price = view.findViewById(R.id.priceEstimates);
        stayLength = view.findViewById(R.id.stayLength);
        description = view.findViewById(R.id.description);
        notes = view.findViewById(R.id.notes);
        locationClass = view.findViewById(R.id.locationClass);
        ownership = view.findViewById(R.id.ownerShip);
    }

    public void setForm(final ModelAddressInfo buildingInfo) {
        address.setText(buildingInfo.getAddress());
        rt.setText(buildingInfo.getRt());
        rw.setText(buildingInfo.getRw());
        zipcode.setText(buildingInfo.getZipcode());
        kelurahan.setText(buildingInfo.getKelurahan());
        kecamatan.setText(buildingInfo.getKecamatan());
        city.setText(buildingInfo.getCity());
        phone.setText(buildingInfo.getPhone());
        fax.setText(buildingInfo.getFax());

        addressType.setText(buildingInfo.getAddressType());

        price.setText(buildingInfo.getPrice());
        if(!price.getText().toString().equals("")){

            String formatted = Currency.set(price);
            price.setText(formatted.replaceAll("[$]",""));
        }
        stayLength.setText(buildingInfo.getStayLength());
        description.setText(buildingInfo.getDescription());
        notes.setText(buildingInfo.getNotes());
        locationClass.setText(buildingInfo.getLocationClass());
        ownership.setText(buildingInfo.getOwnership());

        if (buildingInfo.getBiuldingImageUrl() != null) {
            if(!buildingInfo.getBiuldingImageUrl().equals("")){
                nameFileBuildingImage.setText(buildingInfo.getBiuldingImageUrl().split("/")[6]);
                previewImage.setPreviewImage(buildingInfo.getBiuldingImageUrl(), context, nameFileBuildingImage);
            }else{
                nameFileBuildingImage.setText(buildingInfo.getBiuldingImageUrl());
            }

        }else{
            nameFileBuildingImage.setText(buildingInfo.getBiuldingImageUrl());
        }


    }
}
