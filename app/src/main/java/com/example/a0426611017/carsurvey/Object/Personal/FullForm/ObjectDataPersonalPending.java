package com.example.a0426611017.carsurvey.Object.Personal.FullForm;

/**
 * Created by 0426611017 on 2/12/2018.
 */

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.DatePicker;
import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonal;
import com.example.a0426611017.carsurvey.R;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by 0426611017 on 1/3/2018.
 */

public class ObjectDataPersonalPending {
    private View view;
    private Context context;
    private ViewGroup container;
    private DatePicker datePicker = new DatePicker();
    private ModelDDLPersonal ddlPersonal;

    private EditText customerName, idNumber, idExpiredDate, birthPlace,
            birthDate, npwp, motherMaidenName;

    private TextView namePersonalIdImage, namePersonalIdImageUrl,
            namePersonalNpwpImage, namePersonalNpwpImageUrl;

    private Button buttonIdImage, buttonPersonalNpwp;

    private Spinner customerModel, idType, gender;


    public ObjectDataPersonalPending(ViewGroup viewGroup, LayoutInflater inflater, ModelDDLPersonal ddlPersonal) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;;
        this.ddlPersonal = ddlPersonal;
        this.view = inflater.inflate(R.layout.fragment_data_personal_pending, container, false);

        setField();
    }

    public View getView() {
        return view;
    }

    public TextView getNamePersonalIdImage() {
        return namePersonalIdImage;
    }

    public TextView getNamePersonalNpwpImage() {
        return namePersonalNpwpImage;
    }

    public void setFilePersonalIdImage(String fileIdImage) {
        this.namePersonalIdImage.setText(fileIdImage);
    }

    public void setFilePersonalNpwp(String filePersonalNpwp) {
        this.namePersonalNpwpImage.setText(filePersonalNpwp);
    }

    public Button getButtonIdImage() {
        return buttonIdImage;
    }

    public Button getButtonPersonalNpwp() {
        return buttonPersonalNpwp;
    }

    public TextView getNamePersonalIdImageUrl() {
        return namePersonalIdImageUrl;
    }

    public void setNamePersonalIdImageUrl(String namePersonalIdImageUrl) {
        this.namePersonalIdImageUrl.setText(namePersonalIdImageUrl);
    }

    public TextView getNamePersonalNpwpImageUrl() {
        return namePersonalNpwpImageUrl;
    }

    public void setNamePersonalNpwpImageUrl(String namePersonalNpwpImageUrl) {
        this.namePersonalNpwpImageUrl.setText(namePersonalNpwpImageUrl);
    }

    public void setEnabledButtonImage(){
//        buttonIdImage.setEnabled(true);
        buttonPersonalNpwp.setEnabled(true);
    }

    public void setField() {
        customerModel = view.findViewById(R.id.spinner_customer_model_pending);
        idType = view.findViewById(R.id.spinner_id_type_pending);
        customerName = view.findViewById(R.id.edit_text_customer_name_pending);

        namePersonalIdImage = view.findViewById(R.id.text_view_personal_id_image_pending);
        namePersonalIdImage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    namePersonalIdImage.setError(null);
                }
            }
        });
        namePersonalIdImageUrl = view.findViewById(R.id.text_view_personal_id_url_image_pending);
        buttonIdImage = view.findViewById(R.id.button_camera_id_image_pending);

        idNumber = view.findViewById(R.id.edit_text_id_number_pending);
        idExpiredDate = view.findViewById(R.id.edit_text_id_expired_date_pending);
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, idExpiredDate, context);

        gender = view.findViewById(R.id.spinner_gender_pending);
        birthPlace = view.findViewById(R.id.edit_text_birth_place_pending);
        birthDate = view.findViewById(R.id.edit_text_birth_date_pending);
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, birthDate, context);

        npwp = view.findViewById(R.id.edit_text_npwp_pending);
        npwp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 15){
                    npwp.setError("Minimum 15 digit");
                }
            }
        });

        namePersonalNpwpImage = view.findViewById(R.id.text_view_personal_npwp_pending);
        namePersonalNpwpImage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    namePersonalNpwpImage.setError(null);
                }
            }
        });
        namePersonalNpwpImageUrl = view.findViewById(R.id.text_view_personal_url_npwp_pending);
        buttonPersonalNpwp = view.findViewById(R.id.button_camera_npwp_pending);

        motherMaidenName = view.findViewById(R.id.edit_text_mother_maiden_name_pending);

        if(ddlPersonal != null){
            customerModel.setAdapter(setAdapterItemList(ddlPersonal.getCustomerModel()));
            idType.setAdapter(setAdapterItemList(ddlPersonal.getIdType()));
            idType.setClickable(false);
            idType.setEnabled(false);
            gender.setAdapter(setAdapterItemList(ddlPersonal.getGender()));
        }

        buttonIdImage.setEnabled(false);
        buttonPersonalNpwp.setEnabled(false);
    }

    public void setForm(ModelDataPersonal modelDataPersonal) {
        Log.d("DDL PERSONAL", ddlPersonal.getCustomerModel().toString());
        if (ddlPersonal != null) {
            customerModel.setSelection(ddlPersonal.getCustomerModel().indexOf(modelDataPersonal.getCustmodel()));
            idType.setSelection(ddlPersonal.getIdType().indexOf(modelDataPersonal.getIdtype()));
            gender.setSelection(ddlPersonal.getGender().indexOf(modelDataPersonal.getGenderid()));
        }

        customerName.setText(modelDataPersonal.getCustname());
        idNumber.setText(modelDataPersonal.getIdnumber());

        if (modelDataPersonal.getIdexpdate() != null && !modelDataPersonal.getIdexpdate().equals("")) {
            modelDataPersonal.setIdexpdate(
                    modelDataPersonal.getIdexpdate().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelDataPersonal.getIdexpdate().substring(4, 6))]
                            + " " + modelDataPersonal.getIdexpdate().substring(0, 4)
            );
        }
        idExpiredDate.setText(modelDataPersonal.getIdexpdate());

        birthPlace.setText(modelDataPersonal.getBirthplace());

        if (modelDataPersonal.getBirthdate() != null && !modelDataPersonal.getBirthdate().equals("")) {
            modelDataPersonal.setBirthdate(
                    modelDataPersonal.getBirthdate().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelDataPersonal.getBirthdate().substring(4, 6))]
                            + " " + modelDataPersonal.getBirthdate().substring(0, 4)
            );
        }
        birthDate.setText(modelDataPersonal.getBirthdate());

        npwp.setText(modelDataPersonal.getNpwpnum());

        motherMaidenName.setText(modelDataPersonal.getMothermaidenname());

        namePersonalIdImageUrl.setText(modelDataPersonal.getIdimgurl());
        namePersonalNpwpImageUrl.setText(modelDataPersonal.getNpwpimgurl());

        Log.d("Url image", namePersonalIdImageUrl.getText().toString());
        PreviewImage previewImage = new PreviewImage();
        String nameIdImage;
        if (modelDataPersonal.getIdimgurl().equals("") || modelDataPersonal.getIdimgurl() == null) {
            nameIdImage = "No selected file";
        } else {
            if (modelDataPersonal.getIdimgurl().startsWith("http")) {
                nameIdImage = modelDataPersonal.getIdimgurl().split("/")[6];
                previewImage.setPreviewImage(modelDataPersonal.getIdimgurl(), context, namePersonalIdImage);
            } else {
                nameIdImage = modelDataPersonal.getIdimg();
                previewImage.setPreviewImage(nameIdImage, context, namePersonalIdImage);
            }
        }
        namePersonalIdImage.setText(nameIdImage);

        Log.d("Url image", namePersonalNpwpImageUrl.getText().toString());
        PreviewImage previewNpwpImage = new PreviewImage();
        String nameNpwpImage;
        if (modelDataPersonal.getNpwpimgurl().equals("") || modelDataPersonal.getNpwpimgurl() == null) {
            nameNpwpImage = "No selected file";
        } else {
            if (modelDataPersonal.getNpwpimgurl().startsWith("http")) {
                nameNpwpImage = modelDataPersonal.getNpwpimgurl().split("/")[6];
                previewNpwpImage.setPreviewImage(modelDataPersonal.getNpwpimgurl(), context, namePersonalNpwpImage);
            } else {
                nameNpwpImage = modelDataPersonal.getNpwpimg();
                previewNpwpImage.setPreviewImage(nameIdImage, context, namePersonalNpwpImage);
            }
        }
        namePersonalNpwpImage.setText(nameNpwpImage);
    }

    public ModelDataPersonal getForm() {
        ModelDataPersonal modelDataPersonal = new ModelDataPersonal();

        modelDataPersonal.setCustmodel(customerModel.getSelectedItem().toString());
        modelDataPersonal.setIdtype(idType.getSelectedItem().toString());
        modelDataPersonal.setCustname(customerName.getText().toString());
        modelDataPersonal.setIdimg(namePersonalIdImage.getText().toString());
        modelDataPersonal.setIdimgurl(namePersonalIdImageUrl.getText().toString());
        modelDataPersonal.setIdnumber(idNumber.getText().toString());
        modelDataPersonal.setIdexpdate(idExpiredDate.getText().toString());

        if (modelDataPersonal.getIdexpdate() != null) {
            if (!modelDataPersonal.getIdexpdate().equals("")) {
                modelDataPersonal.setIdexpdate(
                        modelDataPersonal.getIdexpdate().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(modelDataPersonal.getIdexpdate().split(" ")[1])),
                                2,
                                "0"
                        )
                                + modelDataPersonal.getIdexpdate().split(" ")[0]
                );
            }
        }

        modelDataPersonal.setGenderid(gender.getSelectedItem().toString());
        modelDataPersonal.setBirthplace(birthPlace.getText().toString());
        modelDataPersonal.setBirthdate(birthDate.getText().toString());

        if (modelDataPersonal.getBirthdate() != null) {
            if (!modelDataPersonal.getBirthdate().equals("")) {
                modelDataPersonal.setBirthdate(
                        modelDataPersonal.getBirthdate().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(modelDataPersonal.getBirthdate().split(" ")[1])),
                                2,
                                "0"
                        )
                                + modelDataPersonal.getBirthdate().split(" ")[0]
                );
            }
        }

        modelDataPersonal.setNpwpnum(npwp.getText().toString());
        modelDataPersonal.setNpwpimg(namePersonalNpwpImage.getText().toString());
        modelDataPersonal.setNpwpimgurl(namePersonalNpwpImageUrl.getText().toString());
        modelDataPersonal.setMothermaidenname(motherMaidenName.getText().toString());

        modelDataPersonal.setModified(true);

        return modelDataPersonal;
    }

    private ArrayAdapter<String> setAdapterItemList(List items) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl, items);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;
    }

    public ModelDataPersonal getComplete() {
        ModelDataPersonal modelDataPersonal = getForm();
        modelDataPersonal.setComplete(true);

        if (modelDataPersonal.getIdexpdate().length() == 0) {
            idExpiredDate.setError("ID Expired Date is required");
            modelDataPersonal.setComplete(false);
        }else if(modelDataPersonal.getIdexpdate().trim().length() == 0){
            idExpiredDate.setError("ID Expired Date not allowed whitespaces only");
            modelDataPersonal.setComplete(false);
        }

        if (modelDataPersonal.getBirthplace().length() == 0) {
            birthPlace.setError("Birth Place is required");
            modelDataPersonal.setComplete(false);
        }else if(modelDataPersonal.getBirthplace().trim().length() == 0){
            birthPlace.setError("Birth Place not allowed whitespaces only");
            modelDataPersonal.setComplete(false);
        }

        if (modelDataPersonal.getBirthdate().length() == 0) {
            birthDate.setError("Birth Date is required");
            modelDataPersonal.setComplete(false);
        }else if(modelDataPersonal.getBirthdate().trim().length() == 0){
            birthDate.setError("Birth Date not allowed whitespaces only");
            modelDataPersonal.setComplete(false);
        }

        if (modelDataPersonal.getNpwpnum().length() == 0) {
            npwp.setError("NPWP is required");
            modelDataPersonal.setComplete(false);
        }else if(modelDataPersonal.getNpwpnum().trim().length() == 0){
            npwp.setError("NPWP not allowed whitespaces only");
            modelDataPersonal.setComplete(false);
        } else if (modelDataPersonal.getNpwpnum().trim().length() != 15) {
            npwp.setError("Minimum and maximum 15 digits");
            modelDataPersonal.setComplete(false);
        }

        if(modelDataPersonal.getNpwpimg().length() == 0){
            namePersonalNpwpImage.setError("NPWP Image is Required");
            modelDataPersonal.setComplete(false);
        }else if(modelDataPersonal.getNpwpimg().equals("No selected file")){
            namePersonalNpwpImage.setError("NPWP Image is Required");
            modelDataPersonal.setComplete(false);
        }else{
            namePersonalNpwpImage.setError(null);
        }

        if (modelDataPersonal.getMothermaidenname().length() == 0) {
            motherMaidenName.setError("Mother Maiden Name is required");
            modelDataPersonal.setComplete(false);
        }else if(modelDataPersonal.getMothermaidenname().trim().length() == 0){
            motherMaidenName.setError("Mother Maiden Name not allowed whitespaces only");
            modelDataPersonal.setComplete(false);
        }

        return modelDataPersonal;
    }
}
