package com.example.a0426611017.carsurvey.MainFunction;

/**
 * Created by c201017001 on 10/01/2018.
 */

public class FormatDate {
    String [] format = {
            "dd/MM/yyyy",
            "dd/MMM/yyyy",
            "dd MM yyyy",
            "dd MMM yyyy",
            "dd-MM-yyyy",
            "dd-MMM-yyyy",
            "MM/dd/yyyy",
            "MMM/dd/yyyy",
            "MM dd yyyy",
            "MMM dd yyyy",
            "MM-dd-yyyy",
            "MMM-dd-yyyy",
            "dd MMMM yyyy",
    };

    public static final int FORMAT_SLASH_ddMMyyyy = 0;  /* dd/MM/yyyy */

    public static final int FORMAT_SLASH_ddMMMyyyy = 1;  /* dd/MMM/yyyy */

    public static final int FORMAT_SPACE_ddMMyyyy = 2;  /* dd MM yyyy */

    public static final int FORMAT_SPACE_ddMMMyyyy = 3;  /* dd MMM yyyy */

    public static final int FORMAT_DASH_ddMMyyyy = 4;  /* dd-MM-yyyy */

    public static final int FORMAT_DASH_ddMMMyyyy = 5;  /* dd-MMM-yyyy */

    public static final int FORMAT_SLASH_MMddyyyy = 6;  /* MM/dd/yyyy */

    public static final int FORMAT_SLASH_MMMddyyyy = 7;  /* MMM/dd/yyyy */

    public static final int FORMAT_SPACE_MMddyyyy = 8;  /* MM dd yyyy */

    public static final int FORMAT_SPACE_MMMddyyyy = 9;  /* MMM dd yyyy */

    public static final int FORMAT_DASH_MMddyyyy = 10;  /* MM-dd-yyyy */

    public static final int FORMAT_DASH_MMMddyyyy = 11;  /* MMM-dd-yyyy */

    public static final int FORMAT_SPACE_ddMMMMyyyy = 12;  /* dd MMMM yyyy */

    public static final String [] PRINT_MONTH = {
            "",
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember"
    };

}
