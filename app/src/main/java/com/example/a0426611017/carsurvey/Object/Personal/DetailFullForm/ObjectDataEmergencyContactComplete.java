package com.example.a0426611017.carsurvey.Object.Personal.DetailFullForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Model.Personal.ModelDataEmergencyContact;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by 0426611017 on 2/12/2018.
 */

public class ObjectDataEmergencyContactComplete {
    private ViewGroup container;
    private View view;
    private Context context;

    private TextView
            emergencyContactName, customerRelationship, emergencyMobilePhone1,
            emergencyMobilePhone2, emergencyContactAddress, emergencyRt,
            emergencyRw, emergencyZipCode, emergencyKelurahan,
            emergencyKecamatan, emergencyCity, emergencyPhone1;

    public ObjectDataEmergencyContactComplete(ViewGroup viewGroup, LayoutInflater inflater) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_data_emergency_contact_complete, viewGroup, false);

        setField();
    }

    public View getView() {
        return view;
    }

    private void setField() {
        emergencyContactName = view.findViewById(R.id.value_emergency_contact_name);
        customerRelationship = view.findViewById(R.id.value_customer_relationship);
        emergencyMobilePhone1 = view.findViewById(R.id.value_emergency_mobile_phone_1);
        emergencyMobilePhone2 = view.findViewById(R.id.value_emergency_mobile_phone_2);
        emergencyContactAddress = view.findViewById(R.id.value_emergency_contact_address);
        emergencyRt = view.findViewById(R.id.value_emergency_rt);
        emergencyRw = view.findViewById(R.id.value_emergency_rw);
        emergencyZipCode = view.findViewById(R.id.value_emergency_zip_code);
        emergencyKelurahan = view.findViewById(R.id.value_emergency_kelurahan);
        emergencyKecamatan = view.findViewById(R.id.value_emergency_kecamatan);
        emergencyCity = view.findViewById(R.id.value_emergency_city);
        emergencyPhone1 = view.findViewById(R.id.value_emergency_phone_1);

    }

    public void setForm(final ModelDataEmergencyContact modelDataEmergencyContact) {
        emergencyContactName.setText(modelDataEmergencyContact.getEmergencyname());
        customerRelationship.setText(modelDataEmergencyContact.getRelation());
        emergencyMobilePhone1.setText(modelDataEmergencyContact.getEmergencymphn1());
        emergencyMobilePhone2.setText(modelDataEmergencyContact.getEmergencymphn2());
        emergencyContactAddress.setText(modelDataEmergencyContact.getEmergencyaddr());
        emergencyRt.setText(modelDataEmergencyContact.getEmergencyrt());
        emergencyRw.setText(modelDataEmergencyContact.getEmergencyrw());
        emergencyZipCode.setText(modelDataEmergencyContact.getEmergencyzipcode());
        emergencyKelurahan.setText(modelDataEmergencyContact.getEmergencykelurahan());
        emergencyKecamatan.setText(modelDataEmergencyContact.getEmergencykecamatan());
        emergencyCity.setText(modelDataEmergencyContact.getEmergencycity());
        emergencyPhone1.setText(modelDataEmergencyContact.getEmergencyphn());
    }
}

