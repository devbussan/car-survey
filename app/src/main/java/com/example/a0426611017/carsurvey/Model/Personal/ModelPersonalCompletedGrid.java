package com.example.a0426611017.carsurvey.Model.Personal;

/**
 * Created by 0426611017 on 12/14/2017.
 */

public class ModelPersonalCompletedGrid {
    private String personalDate;
    private String personalName;
    private String personalNo_id;
    private String personalPhone_number;

    //personal date
    public String getPersonalDate() {return personalDate;}
    public void setPersonalDate (String personalDate) {this.personalDate = personalDate;}

    //personal name
    public String getPersonalName() {return personalName;}
    public void setPersonalName (String personalName) {this.personalName = personalName;}

    //personal No_id
    public String getPersonalNo_id() {return personalNo_id;}
    public void setPersonalNo_id (String personalNo_id) {this.personalNo_id = personalNo_id;}

    //personal Phone_number
    public String getPersonalPhone_number() {return personalPhone_number;}
    public void setPersonalPhone_number (String personalPhone_number) {this.personalPhone_number = personalPhone_number;}

}
