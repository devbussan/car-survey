package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.Personal.ModelDataFinancial;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 06/03/2018.
 */

public class ResponseDetailPersonalFinancial {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelDataFinancial modelDataFinancial;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelDataFinancial getModelDataFinancial() {
        return modelDataFinancial;
    }

    public void setModelDataFinancial(ModelDataFinancial modelDataFinancial) {
        this.modelDataFinancial = modelDataFinancial;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
