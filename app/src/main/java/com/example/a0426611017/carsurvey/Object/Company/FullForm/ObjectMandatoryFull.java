package com.example.a0426611017.carsurvey.Object.Company.FullForm;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.DatePicker;
import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Company.ModelMandatoryInfo;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLCompany;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by c201017001 on 02/01/2018.
 */

public class ObjectMandatoryFull {

    private ViewGroup container;
    private LayoutInflater inflater;
    private View view;
    private Context context;
    private PreviewImage previewImage = new PreviewImage();
    private DatePicker datePicker = new DatePicker();
    private ModelDDLCompany ddlCompany;

    private Spinner dropdownDebitorScale;
    private DatePickerDialog datePickerDialog;
    private EditText nomorAkta, nomorSiup, nomorDomisili, nomorTdp;
    private EditText dateAkta, dateSiup, dateSkDomisili, dateTdp;
    private EditText debitorGroup, counterPart;
    private TextView fileNameAkta, fileNameSiup, fileNameDomisili, fileNameTdp;
    private TextView pathFileNameAkta, pathFileNameSiup, pathFileNameDomisili, pathFileNameTdp;
    private Button buttonAkta, buttonSiup, buttontdp, buttonSkDomisili;

    private int permission = -1;

    public ObjectMandatoryFull(ViewGroup viewGroup, LayoutInflater inflater, ModelDDLCompany ddlCompany) {
        this.context = viewGroup.getContext();
        this.view = inflater.inflate(R.layout.fragment_company_mandatory_pending, container, false);
        this.container = viewGroup;
        this.inflater = inflater;
        this.ddlCompany = ddlCompany;
        findView();
    }

    public View getView() {
        return view;
    }

    public TextView getFileNameAkta() {
        return fileNameAkta;
    }

    public void setFileNameAkta(String fileNameAkta) {
        this.fileNameAkta.setText(fileNameAkta);
        this.fileNameAkta.setError(null);
    }

    public TextView getFileNameSiup() {
        return fileNameSiup;
    }

    public void setFileNameSiup(String fileNameSiup) {
        this.fileNameSiup.setText(fileNameSiup);
        this.fileNameSiup.setError(null);
    }

    public TextView getFileNameDomisili() {
        return fileNameDomisili;
    }

    public void setFileNameDomisili(String fileNameDomisili) {
        this.fileNameDomisili.setText(fileNameDomisili);
        this.fileNameDomisili.setError(null);
    }

    public TextView getFileNameTdp() {
        return fileNameTdp;
    }

    public void setFileNameTdp(String fileNameTdp) {
        this.fileNameTdp.setText(fileNameTdp);
        this.fileNameTdp.setError(null);
    }

    public TextView getPathFileNameAkta() {
        return pathFileNameAkta;
    }

    public void setPathFileNameAkta(String pathFileNameAkta) {
        this.pathFileNameAkta.setText(pathFileNameAkta);
    }

    public TextView getPathFileNameSiup() {
        return pathFileNameSiup;
    }

    public void setPathFileNameSiup(String pathFileNameSiup) {
        this.pathFileNameSiup.setText(pathFileNameSiup);
    }

    public TextView getPathFileNameDomisili() {
        return pathFileNameDomisili;
    }

    public void setPathFileNameDomisili(String pathFileNameDomisili) {
        this.pathFileNameDomisili.setText(pathFileNameDomisili);
    }

    public TextView getPathFileNameTdp() {
        return pathFileNameTdp;
    }

    public void setPathFileNameTdp(String pathFileNameTdp) {
        this.pathFileNameTdp.setText(pathFileNameTdp);
    }

    public Button getButtonAkta() {
        return buttonAkta;
    }

    public Button getButtonSiup() {
        return buttonSiup;
    }

    public Button getButtontdp() {
        return buttontdp;
    }

    public Button getButtonSkDomisili() {
        return buttonSkDomisili;
    }

    public void setPermission(int permission) {
        this.permission = permission;
    }

    private void findView() {
        nomorAkta = view.findViewById(R.id.editTextNomorAktaPending);
        nomorSiup = view.findViewById(R.id.editTextNomorSiupPending);
        nomorDomisili = view.findViewById(R.id.editTextNomorDomisiliPending);
        nomorTdp = view.findViewById(R.id.editTextNomorTdpPending);

        dateAkta = view.findViewById(R.id.editTextTanggalAktaPending);
        dateAkta.setEnabled(false);
        dateSiup = view.findViewById(R.id.editTextTanggalSiupPending);
        dateSiup.setEnabled(false);
        dateSkDomisili = view.findViewById(R.id.editTextTanggalDomisiliUsahaPending);
        dateSkDomisili.setEnabled(false);
        dateTdp = view.findViewById(R.id.editTextTanggalTdpPending);
        dateTdp.setEnabled(false);

        buttonAkta = view.findViewById(R.id.buttonAktaImagePending);
        buttonAkta.setEnabled(false);
        buttonSiup = view.findViewById(R.id.buttonSiupImagePending);
        buttonSiup.setEnabled(false);
        buttonSkDomisili = view.findViewById(R.id.buttonDomisiliPending);
        buttonSkDomisili.setEnabled(false);
        buttontdp = view.findViewById(R.id.buttonTdpImagePending);
        buttontdp.setEnabled(false);

        fileNameAkta = view.findViewById(R.id.textViewImageAkta);
        fileNameAkta.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    fileNameAkta.setError(null);
                }
            }
        });
        fileNameSiup = view.findViewById(R.id.textViewImageSiup);
        fileNameSiup.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    fileNameSiup.setError(null);
                }
            }
        });
        fileNameDomisili = view.findViewById(R.id.textViewImageDomisili);
        fileNameDomisili.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    fileNameDomisili.setError(null);
                }
            }
        });
        fileNameTdp = view.findViewById(R.id.textViewImageTdp);
        fileNameTdp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    fileNameTdp.setError(null);
                }
            }
        });

        pathFileNameAkta = view.findViewById(R.id.textViewNameFileAktaHidden);
        pathFileNameSiup = view.findViewById(R.id.textViewNameFileSiupHidden);
        pathFileNameDomisili = view.findViewById(R.id.textViewNameFileDomisiliHidden);
        pathFileNameTdp = view.findViewById(R.id.textViewNameFileTdpHidden);

        debitorGroup = view.findViewById(R.id.editTextDebitorGroupPending);
        counterPart = view.findViewById(R.id.editTextCounterpartPending);

        dropdownDebitorScale = view.findViewById(R.id.spinnerDebitorScalePending);
        if (ddlCompany != null) {
            dropdownDebitorScale.setAdapter(setAdapterItemList(ddlCompany.getDebitorBusinessScale()));
        }


        setEnableDisableDateAndButtonPicture(nomorAkta, buttonAkta, dateAkta);
        setEnableDisableDateAndButtonPicture(nomorSiup, buttonSiup, dateSiup);
        setEnableDisableDateAndButtonPicture(nomorDomisili, buttonSkDomisili, dateSkDomisili);
        setEnableDisableDateAndButtonPicture(nomorTdp, buttontdp, dateTdp);

        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, dateAkta, context);
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, dateSiup, context);
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, dateSkDomisili, context);
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, dateTdp, context);

    }

    private ArrayAdapter<String> setAdapterItemList(List list) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl, list);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;

    }

    private void setEnableDisableDateAndButtonPicture(EditText editTextNomor, Button button, EditText editTextDate) {
        final EditText nomor = editTextNomor;
        final Button buttonPicture = button;
        final EditText date = editTextDate;
        nomor.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (nomor.getText().toString().equals("")) {
                    buttonPicture.setEnabled(false);
                    date.setEnabled(false);

                } else {
                    date.setEnabled(true);
                    if (permission != -1) buttonPicture.setEnabled(true);

                }

            }
        });
    }

    public ModelMandatoryInfo getForm() {
        ModelMandatoryInfo mandatoryInfo = new ModelMandatoryInfo();
        mandatoryInfo.setNomorAkta(nomorAkta.getText().toString());
        mandatoryInfo.setNomorSiup(nomorSiup.getText().toString());
        mandatoryInfo.setNomorDomisili(nomorDomisili.getText().toString());
        mandatoryInfo.setNomorTdp(nomorTdp.getText().toString());

        mandatoryInfo.setDateAktaFormat(dateAkta.getText().toString());
        mandatoryInfo.setDateSiupFormat(dateSiup.getText().toString());
        mandatoryInfo.setDateSkDomisiliFormat(dateSkDomisili.getText().toString());
        mandatoryInfo.setDateTdpFormat(dateTdp.getText().toString());

        if (mandatoryInfo.getDateAktaFormat() != null) {
            if (!mandatoryInfo.getDateAktaFormat().equals("")) {
                mandatoryInfo.setDateAkta(
                        mandatoryInfo.getDateAktaFormat().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(mandatoryInfo.getDateAktaFormat().split(" ")[1])),
                                2,
                                "0"
                        )
                                + mandatoryInfo.getDateAktaFormat().split(" ")[0]
                );
            }
        }

        if (mandatoryInfo.getDateSiupFormat() != null) {
            if (!mandatoryInfo.getDateSiupFormat().equals("")) {
                mandatoryInfo.setDateSiup(
                        mandatoryInfo.getDateSiupFormat().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(mandatoryInfo.getDateSiupFormat().split(" ")[1])),
                                2,
                                "0"
                        )
                                + mandatoryInfo.getDateSiupFormat().split(" ")[0]
                );
            }
        }

        if (mandatoryInfo.getDateSkDomisiliFormat() != null) {
            if (!mandatoryInfo.getDateSkDomisiliFormat().equals("")) {
                mandatoryInfo.setDateSkDomisili(
                        mandatoryInfo.getDateSkDomisiliFormat().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(mandatoryInfo.getDateSkDomisiliFormat().split(" ")[1])),
                                2,
                                "0"
                        )
                                + mandatoryInfo.getDateSkDomisiliFormat().split(" ")[0]
                );
            }
        }

        if (mandatoryInfo.getDateTdpFormat() != null) {
            if (!mandatoryInfo.getDateTdpFormat().equals("")) {
                mandatoryInfo.setDateTdp(
                        mandatoryInfo.getDateTdpFormat().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(mandatoryInfo.getDateTdpFormat().split(" ")[1])),
                                2,
                                "0"
                        )
                                + mandatoryInfo.getDateTdpFormat().split(" ")[0]
                );
            }
        }

        mandatoryInfo.setNameImageAkta(fileNameAkta.getText().toString());
        mandatoryInfo.setNameImageSiup(fileNameSiup.getText().toString());
        mandatoryInfo.setNameImageDomisili(fileNameDomisili.getText().toString());
        mandatoryInfo.setNameImageTdp(fileNameTdp.getText().toString());

        mandatoryInfo.setNameImageAktaUrl(pathFileNameAkta.getText().toString());
        mandatoryInfo.setNameImageSiupUrl(pathFileNameSiup.getText().toString());
        mandatoryInfo.setNameImageDomisiliUrl(pathFileNameDomisili.getText().toString());
        mandatoryInfo.setNameImageTdpUrl(pathFileNameTdp.getText().toString());
        mandatoryInfo.setDebitorScale(dropdownDebitorScale.getSelectedItem().toString());
        mandatoryInfo.setDebitorGroup(debitorGroup.getText().toString());
        mandatoryInfo.setCounterPart(counterPart.getText().toString());

        mandatoryInfo.setModified(true);
        return mandatoryInfo;
    }

    public void setForm(final ModelMandatoryInfo mandatoryInfo) {
        nomorAkta.setText(mandatoryInfo.getNomorAkta());
        nomorSiup.setText(mandatoryInfo.getNomorSiup());
        nomorDomisili.setText(mandatoryInfo.getNomorDomisili());
        nomorTdp.setText(mandatoryInfo.getNomorTdp());

        if (mandatoryInfo.getNomorAkta() != null && !mandatoryInfo.getNomorAkta().equals("")) {
            buttonAkta.setEnabled(true);
        } else {
            buttonAkta.setEnabled(false);
        }

        if (mandatoryInfo.getNomorSiup() != null && !mandatoryInfo.getNomorSiup().equals("")) {
            buttonSiup.setEnabled(true);
        } else {
            buttonSiup.setEnabled(false);
        }

        if (mandatoryInfo.getNomorDomisili() != null && !mandatoryInfo.getNomorDomisili().equals("")) {
            buttonSkDomisili.setEnabled(true);
        } else {
            buttonSkDomisili.setEnabled(false);
        }

        if (mandatoryInfo.getNomorTdp() != null && !mandatoryInfo.getNomorTdp().equals("")) {
            buttontdp.setEnabled(true);
        } else {
            buttontdp.setEnabled(false);
        }

        if (mandatoryInfo.getDateAkta() != null && !mandatoryInfo.getDateAkta().equals("")) {
            mandatoryInfo.setDateAktaFormat(
                    mandatoryInfo.getDateAkta().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(mandatoryInfo.getDateAkta().substring(4, 6))]
                            + " " + mandatoryInfo.getDateAkta().substring(0, 4)
            );
        }

        if (mandatoryInfo.getDateSiup() != null && !mandatoryInfo.getDateSiup().equals("")) {
            mandatoryInfo.setDateSiupFormat(
                    mandatoryInfo.getDateSiup().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(mandatoryInfo.getDateSiup().substring(4, 6))]
                            + " " + mandatoryInfo.getDateSiup().substring(0, 4)
            );
        }

        if (mandatoryInfo.getDateSkDomisili() != null && !mandatoryInfo.getDateSkDomisili().equals("")) {
            mandatoryInfo.setDateSkDomisiliFormat(
                    mandatoryInfo.getDateSkDomisili().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(mandatoryInfo.getDateSkDomisili().substring(4, 6))]
                            + " " + mandatoryInfo.getDateSkDomisili().substring(0, 4)
            );
        }

        if (mandatoryInfo.getDateTdp() != null && !mandatoryInfo.getDateTdp().equals("")) {
            mandatoryInfo.setDateTdpFormat(
                    mandatoryInfo.getDateTdp().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(mandatoryInfo.getDateTdp().substring(4, 6))]
                            + " " + mandatoryInfo.getDateTdp().substring(0, 4)
            );
        }

        dateAkta.setText(mandatoryInfo.getDateAktaFormat());
        dateSiup.setText(mandatoryInfo.getDateSiupFormat());
        dateSkDomisili.setText(mandatoryInfo.getDateSkDomisiliFormat());
        dateTdp.setText(mandatoryInfo.getDateTdpFormat());

        String fileName = "No selected file";
        if (mandatoryInfo.getNameImageAktaUrl() != null
                && !mandatoryInfo.getNameImageAktaUrl().equals("")) {
            if (mandatoryInfo.getNameImageAktaUrl().startsWith("http")) {
                fileName = mandatoryInfo.getNameImageAktaUrl().split("/")[6];
                previewImage.setPreviewImage(mandatoryInfo.getNameImageAktaUrl(), context, fileNameAkta);
            } else {
                fileName = mandatoryInfo.getNameImageAkta();
                previewImage.setPreviewImage(fileName, context, fileNameAkta);
            }

        }
        fileNameAkta.setText(fileName);

        fileName = "No selected file";
        if (mandatoryInfo.getNameImageTdpUrl() != null
                && !mandatoryInfo.getNameImageTdpUrl().equals("")) {
            if (mandatoryInfo.getNameImageTdpUrl().startsWith("http")) {
                fileName = mandatoryInfo.getNameImageTdpUrl().split("/")[6];
                previewImage.setPreviewImage(mandatoryInfo.getNameImageTdpUrl(), context, fileNameTdp);
            } else {
                fileName = mandatoryInfo.getNameImageTdp();
                previewImage.setPreviewImage(fileName, context, fileNameTdp);
            }

        }
        fileNameTdp.setText(fileName);

        fileName = "No selected file";
        if (mandatoryInfo.getNameImageDomisiliUrl() != null
                && !mandatoryInfo.getNameImageDomisiliUrl().equals("")) {
            if (mandatoryInfo.getNameImageDomisiliUrl().startsWith("http")) {
                fileName = mandatoryInfo.getNameImageDomisiliUrl().split("/")[6];
                previewImage.setPreviewImage(mandatoryInfo.getNameImageDomisiliUrl(), context, fileNameDomisili);
            } else {
                fileName = mandatoryInfo.getNameImageDomisili();
                previewImage.setPreviewImage(fileName, context, fileNameDomisili);
            }

        }
        fileNameDomisili.setText(fileName);

        fileName = "No selected file";
        if (mandatoryInfo.getNameImageSiupUrl() != null
                && !mandatoryInfo.getNameImageSiupUrl().equals("")) {
            if (mandatoryInfo.getNameImageSiupUrl().startsWith("http")) {
                fileName = mandatoryInfo.getNameImageSiupUrl().split("/")[6];
                previewImage.setPreviewImage(mandatoryInfo.getNameImageSiupUrl(), context, fileNameSiup);
            } else {
                fileName = mandatoryInfo.getNameImageSiup();
                previewImage.setPreviewImage(fileName, context, fileNameSiup);
            }

        }
        fileNameSiup.setText(fileName);

        pathFileNameAkta.setText(mandatoryInfo.getNameImageAktaUrl());
        pathFileNameSiup.setText(mandatoryInfo.getNameImageSiupUrl());
        pathFileNameDomisili.setText(mandatoryInfo.getNameImageDomisiliUrl());
        pathFileNameTdp.setText(mandatoryInfo.getNameImageTdpUrl());

        debitorGroup.setText(mandatoryInfo.getDebitorGroup());
        if (ddlCompany != null) {
            dropdownDebitorScale.setSelection(ddlCompany.getDebitorBusinessScale().indexOf(mandatoryInfo.getDebitorScale()));
        }

        counterPart.setText(mandatoryInfo.getCounterPart());
    }

    public ModelMandatoryInfo getComplete() {
        ModelMandatoryInfo mandatoryInfo = getForm();
        mandatoryInfo.setComplete(true);

        if (mandatoryInfo.getNomorAkta().length() != 0
                && mandatoryInfo.getNomorAkta().trim().length() == 0) {
            nomorAkta.setError("Whitespaces only detected");
            mandatoryInfo.setComplete(false);
        } else if (mandatoryInfo.getNomorAkta().length() != 0) {
            if (mandatoryInfo.getDateAktaFormat().length() == 0) {
                dateAkta.setError("Required");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getDateAktaFormat().trim().length() == 0) {
                dateAkta.setError("Whitespaces only detected");
                mandatoryInfo.setComplete(false);
            }

            if (mandatoryInfo.getNameImageAkta().length() == 0) {
                fileNameAkta.setError("Required");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getNameImageAkta().trim().length() == 0) {
                fileNameAkta.setError("Whitespaces only detected");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getNameImageAkta().equals("No selected file")) {
                fileNameAkta.setError("Required");
                mandatoryInfo.setComplete(false);
            }
        }

        if (mandatoryInfo.getNomorSiup().length() != 0
                && mandatoryInfo.getNomorSiup().trim().length() == 0) {
            nomorSiup.setError("Whitespaces only detected");
            mandatoryInfo.setComplete(false);
        } else if (mandatoryInfo.getNomorSiup().length() != 0) {
            if (mandatoryInfo.getDateSiupFormat().length() == 0) {
                dateSiup.setError("Required");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getDateSiupFormat().trim().length() == 0) {
                dateSiup.setError("Whitespaces only detected");
                mandatoryInfo.setComplete(false);
            }

            if (mandatoryInfo.getNameImageSiup().length() == 0) {
                fileNameSiup.setError("Required");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getNameImageSiup().trim().length() == 0) {
                fileNameSiup.setError("Whitespaces only detected");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getNameImageSiup().equals("No selected file")) {
                fileNameSiup.setError("Required");
                mandatoryInfo.setComplete(false);
            }
        }

        if (mandatoryInfo.getNomorDomisili().length() != 0
                && mandatoryInfo.getNomorDomisili().trim().length() == 0) {
            nomorDomisili.setError("Whitespaces only detected");
            mandatoryInfo.setComplete(false);
        } else if (mandatoryInfo.getNomorDomisili().length() != 0) {
            if (mandatoryInfo.getDateSkDomisiliFormat().length() == 0) {
                dateSkDomisili.setError("Required");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getDateSkDomisiliFormat().trim().length() == 0) {
                dateSkDomisili.setError("Whitespaces only detected");
                mandatoryInfo.setComplete(false);
            }

            if (mandatoryInfo.getNameImageDomisili().length() == 0) {
                fileNameDomisili.setError("Required");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getNameImageDomisili().trim().length() == 0) {
                fileNameDomisili.setError("Whitespaces only detected");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getNameImageDomisili().equals("No selected file")) {
                fileNameDomisili.setError("Required");
                mandatoryInfo.setComplete(false);
            }
        }

        if (mandatoryInfo.getNomorTdp().length() != 0
                && mandatoryInfo.getNomorTdp().trim().length() == 0) {
            nomorTdp.setError("Whitespaces only detected");
            mandatoryInfo.setComplete(false);
        } else if (mandatoryInfo.getNomorTdp().length() != 0) {
            if (mandatoryInfo.getDateTdpFormat().length() == 0) {
                dateTdp.setError("Required");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getDateTdpFormat().trim().length() == 0) {
                dateTdp.setError("Whitespaces only detected");
                mandatoryInfo.setComplete(false);
            }

            if (mandatoryInfo.getNameImageTdp().length() == 0) {
                fileNameTdp.setError("Required");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getNameImageTdp().trim().length() == 0) {
                fileNameTdp.setError("Whitespaces only detected");
                mandatoryInfo.setComplete(false);
            } else if (mandatoryInfo.getNameImageTdp().equals("No selected file")) {
                fileNameTdp.setError("Required");
                mandatoryInfo.setComplete(false);
            }
        }

//        if (mandatoryInfo.getDebitorGroup().length() == 0) {
//            debitorGroup.setError("Required");
//            mandatoryInfo.setComplete(false);
//        } else if (mandatoryInfo.getDebitorGroup().trim().length() == 0) {
//            debitorGroup.setError("Whitespaces only detected");
//            mandatoryInfo.setComplete(false);
//        }

//        if (mandatoryInfo.getCounterPart().length() == 0) {
//            counterPart.setError("Required");
//            mandatoryInfo.setComplete(false);
//        } else if (mandatoryInfo.getCounterPart().trim().length() == 0) {
//            counterPart.setError("Whitespaces only detected");
//            mandatoryInfo.setComplete(false);
//        }


        return mandatoryInfo;
    }

}
