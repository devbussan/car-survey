package com.example.a0426611017.carsurvey.Model.Survey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by c201017001 on 08/02/2018.
 */

public class ModelCekLingkungan implements Serializable{

    @SerializedName("SURVEYENVID")
    @Expose
    String surveyEnId;

    @SerializedName("INFORMNAME")
    @Expose
    String inforName;

    @SerializedName("RELATIONWITHCUST")
    @Expose
    String relationWithCust;

    @SerializedName("OTHERRELATION")
    @Expose
    String otherRelation;

    @SerializedName("CLNDEBDIKENAL")
    @Expose
    String clnDebDiKenal;

    @SerializedName("CHARACTERCUST")
    @Expose
    String characterCust;

    @SerializedName("CHARACTERSPOUSE")
    @Expose
    String characterSpouse;

    @SerializedName("RLTNWITHSPOUSE")
    @Expose
    String rltnWithSpouse;

    @SerializedName("POSITIVEINFO")
    @Expose
    String positiveInfo;

    @SerializedName("NEGATIVEINFO")
    @Expose
    String negativeInfo;

    @SerializedName("HOMESTAT")
    @Expose
    String homeStat;

    @SerializedName("WORKSTAT")
    @Expose
    String workStat;

    @SerializedName("INFOASSET")
    @Expose
    String infoAsset;

    @SerializedName("LAMATINGGAL")
    @Expose
    String lamaTinggal;

    @SerializedName("OTHERINFO")
    @Expose
    String otherInfo;

    @SerializedName("FLAG_SURVEY_ENV")
    @Expose
    String flagSurveyEnv;

    @SerializedName("USRCRT")
    @Expose
    String userCrt;

    @SerializedName("SURVEY_ENV_ID")
    @Expose
    String companySurveyEnvId;

    @SerializedName("SURVEY_ENV_STATUS")
    @Expose
    String companySurveyEnvStatus;

    @SerializedName("SURVEYID")
    @Expose
    String surveyId;

    @SerializedName("SURVEY_ENV")
    @Expose
    private List<String> surveyEnv;

    private boolean isModified = false;

    private boolean isComplete = false;

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public String getSurveyEnId() {
        return surveyEnId;
    }

    public void setSurveyEnId(String surveyEnId) {
        this.surveyEnId = surveyEnId;
    }

    public String getInforName() {
        return inforName;
    }

    public void setInforName(String inforName) {
        this.inforName = inforName;
    }

    public String getRelationWithCust() {
        return relationWithCust;
    }

    public void setRelationWithCust(String relationWithCust) {
        this.relationWithCust = relationWithCust;
    }

    public String getOtherRelation() {
        return otherRelation;
    }

    public void setOtherRelation(String otherRelation) {
        this.otherRelation = otherRelation;
    }

    public String getClnDebDiKenal() {
        return clnDebDiKenal;
    }

    public void setClnDebDiKenal(String clnDebDiKenal) {
        this.clnDebDiKenal = clnDebDiKenal;
    }

    public String getCharacterCust() {
        return characterCust;
    }

    public void setCharacterCust(String characterCust) {
        this.characterCust = characterCust;
    }

    public String getCharacterSpouse() {
        return characterSpouse;
    }

    public void setCharacterSpouse(String characterSpouse) {
        this.characterSpouse = characterSpouse;
    }

    public String getRltnWithSpouse() {
        return rltnWithSpouse;
    }

    public void setRltnWithSpouse(String rltnWithSpouse) {
        this.rltnWithSpouse = rltnWithSpouse;
    }

    public String getPositiveInfo() {
        return positiveInfo;
    }

    public void setPositiveInfo(String positiveInfo) {
        this.positiveInfo = positiveInfo;
    }

    public String getNegativeInfo() {
        return negativeInfo;
    }

    public void setNegativeInfo(String negativeInfo) {
        this.negativeInfo = negativeInfo;
    }

    public String getHomeStat() {
        return homeStat;
    }

    public void setHomeStat(String homeStat) {
        this.homeStat = homeStat;
    }

    public String getWorkStat() {
        return workStat;
    }

    public void setWorkStat(String workStat) {
        this.workStat = workStat;
    }

    public String getInfoAsset() {
        return infoAsset;
    }

    public void setInfoAsset(String infoAsset) {
        this.infoAsset = infoAsset;
    }

    public String getLamaTinggal() {
        return lamaTinggal;
    }

    public void setLamaTinggal(String lamaTinggal) {
        this.lamaTinggal = lamaTinggal;
    }

    public String getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(String otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getFlagSurveyEnv() {
        return flagSurveyEnv;
    }

    public void setFlagSurveyEnv(String flagSurveyEnv) {
        this.flagSurveyEnv = flagSurveyEnv;
    }

    public String getUserCrt() {
        return userCrt;
    }

    public void setUserCrt(String userCrt) {
        this.userCrt = userCrt;
    }

    public String getCompanySurveyEnvId() {
        return companySurveyEnvId;
    }

    public void setCompanySurveyEnvId(String companySurveyEnvId) {
        this.companySurveyEnvId = companySurveyEnvId;
    }

    public String getCompanySurveyEnvStatus() {
        return companySurveyEnvStatus;
    }

    public void setCompanySurveyEnvStatus(String companySurveyEnvStatus) {
        this.companySurveyEnvStatus = companySurveyEnvStatus;
    }

    public List<String> getSurveyEnv() {
        return surveyEnv;
    }

    public void setSurveyEnv(List<String> surveyEnv) {
        this.surveyEnv = surveyEnv;
    }


    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }
}
