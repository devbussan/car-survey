package com.example.a0426611017.carsurvey.Object.Company.DetailMiniForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelDetailCompanyMiniForm;

/**
 * Created by c201017001 on 13/02/2018.
 */

public class ObjectCompanyInfoDetailOrder {

    private View view;
    private ViewGroup container;
    private LayoutInflater inflater;
    private Context context;

    private TextView valueCmoName, valueCustomerName, valueNpwpNumber;

    public ObjectCompanyInfoDetailOrder(ViewGroup container, LayoutInflater inflater) {
        this.container = container;
        this.inflater = inflater;
        this.view = this.inflater.inflate(R.layout.fragment_company_info_detail_order, this.container, false);
        this.context = this.container.getContext();
        setField();
    }

    public View getView() {
        return view;
    }

    private void setField(){
        valueCustomerName = view.findViewById(R.id.value_company_name_add_new_company_info_detail);
        valueNpwpNumber = view.findViewById(R.id.value_npwp_number_add_new_company_info_detail);
    }

    public void setForm(ModelDetailCompanyMiniForm companyInfo){
        valueCustomerName.setText(companyInfo.getCompanyName());
        valueNpwpNumber.setText(companyInfo.getNpwpNo());
    }
}
