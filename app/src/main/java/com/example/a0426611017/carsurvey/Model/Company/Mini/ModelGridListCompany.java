package com.example.a0426611017.carsurvey.Model.Company.Mini;

import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 22/02/2018.
 */

public class ModelGridListCompany {

    @SerializedName("COMPANY_ORDER_H_ID")
    private String companyOrderId;

    @SerializedName("COMPANYNAME")
    private String companyName;

    @SerializedName("NPWPNO")
    private String npwpNo;

    @SerializedName("SURVEY_STAT")
    private String surveyStat;

    @SerializedName("CMO_ID")
    private String cmoID;

    @SerializedName("ADMIN_NAME")
    private String adminName;

    @SerializedName("DTM_CRT")
    private String dateCreate;

    @SerializedName("IS_READ")
    private String isRead;

    @SerializedName("LOG_DATA_ID")
    private String logDataID;

    @SerializedName("COMPANY_CUST_DATA_ID")
    private String custDataId;

    @SerializedName("COMPANY_SURVEY_ID")
    private String surveyId;

    @SerializedName("COMMENT")
    private String comMent;


    public String getCustDataId() {
        return custDataId;
    }

    public void setCustDataId(String custDataId) {
        this.custDataId = custDataId;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public String getCompanyOrderId() {
        return companyOrderId;
    }

    public void setCompanyOrderId(String companyOrderId) {
        this.companyOrderId = companyOrderId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getNpwpNo() {
        return npwpNo;
    }

    public void setNpwpNo(String npwpNo) {
        this.npwpNo = npwpNo;
    }

    public String getSurveyStat() {
        return surveyStat;
    }

    public void setSurveyStat(String surveyStat) {
        this.surveyStat = surveyStat;
    }

    public String getCmoID() {
        return cmoID;
    }

    public void setCmoID(String cmoID) {
        this.cmoID = cmoID;
    }

    public String getAdminName() {
        return adminName;
    }

    public void setAdminName(String adminName) {
        this.adminName = adminName;
    }

    public String getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(String dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getIsRead() {
        return isRead;
    }

    public void setIsRead(String isRead) {
        this.isRead = isRead;
    }

    public String getLogDataID() {
        return logDataID;
    }

    public void setLogDataID(String logDataID) {
        this.logDataID = logDataID;
    }

    public String getComMent() {
        return comMent;
    }

    public void setComMent(String comMent) {
        this.comMent = comMent;
    }
}
