package com.example.a0426611017.carsurvey.Model.Personal;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by 0426611017 on 2/13/2018.
 */

public class ModelDataFinancial implements Serializable {
    @SerializedName("CUST_DATA_ID")
    private String cust_data_id;

    @SerializedName("PERSONAL_FIN_ID")
    private String personal_fin_id;

    @SerializedName("FIXEDINCOME")
    private String fixedincome;

    @SerializedName("SLIPIMGURL")
    private String slipimgurl;

    private String slipimg;

    @SerializedName("VARIABLEINCOME")
    private String variableincome;

    @SerializedName("OTHERINCOME")
    private String otherincome;

    @SerializedName("SPOUSEINCOME")
    private String spouseincome;

    @SerializedName("MONTHLYLIVCOST")
    private String monthlylivcost;

    @SerializedName("OTHERLOANINST")
    private String otherloaninst;

    @SerializedName("USRCRT")
    private String usrcrt;

    @SerializedName("STATUS")
    private String status;

    @SerializedName("PERSONAL_FINANCIAL")
    private String personal_financial_return;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getCust_data_id() {
        return cust_data_id;
    }

    public void setCust_data_id(String cust_data_id) {
        this.cust_data_id = cust_data_id;
    }

    public String getPersonal_fin_id() {
        return personal_fin_id;
    }

    public void setPersonal_fin_id(String personal_fin_id) {
        this.personal_fin_id = personal_fin_id;
    }

    public String getFixedincome() {
        return fixedincome;
    }

    public void setFixedincome(String fixedincome) {
        this.fixedincome = fixedincome;
    }

    public String getSlipimgurl() {
        return slipimgurl;
    }

    public void setSlipimgurl(String slipimgurl) {
        this.slipimgurl = slipimgurl;
    }

    public String getSlipimg() {
        return slipimg;
    }

    public void setSlipimg(String slipimg) {
        this.slipimg = slipimg;
    }

    public String getVariableincome() {
        return variableincome;
    }

    public void setVariableincome(String variableincome) {
        this.variableincome = variableincome;
    }

    public String getOtherincome() {
        return otherincome;
    }

    public void setOtherincome(String otherincome) {
        this.otherincome = otherincome;
    }

    public String getSpouseincome() {
        return spouseincome;
    }

    public void setSpouseincome(String spouseincome) {
        this.spouseincome = spouseincome;
    }

    public String getMonthlylivcost() {
        return monthlylivcost;
    }

    public void setMonthlylivcost(String monthlylivcost) {
        this.monthlylivcost = monthlylivcost;
    }

    public String getOtherloaninst() {
        return otherloaninst;
    }

    public void setOtherloaninst(String otherloaninst) {
        this.otherloaninst = otherloaninst;
    }

    public String getUsrcrt() {
        return usrcrt;
    }

    public void setUsrcrt(String usrcrt) {
        this.usrcrt = usrcrt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public String getPersonal_financial_return() {
        return personal_financial_return;
    }

    public void setPersonal_financial_return(String personal_financial_return) {
        this.personal_financial_return = personal_financial_return;
    }
}

