package com.example.a0426611017.carsurvey.MainFunction;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.R;

import java.io.File;
import java.io.InputStream;

/**
 * Created by c201017001 on 10/01/2018.
 */

public class PreviewImage {

    private FileFunction fileFunction = new FileFunction();
    private ProgressDialog progressDialog;

    public void setPreviewImage(String fileName, Context context, TextView textViewNameFile){
        if (fileName != null) {
            if (fileName.startsWith("http")) {
                String url = Constans.DOMAIN + Constans.URL_IMAGE_THUMB + fileName.split("/")[6];
                setOnclickPreviewImage(textViewNameFile, url, context);
            } else {
                File file = fileFunction.getFilePictureBAF(fileName);
                if (file.exists()) {
                    setOnclickPreviewImage(textViewNameFile, file, context);
                }
            }
        }
    }

    private void previewImage(File filePathName, Context context){
        Uri filePath = Uri.fromFile(filePathName);

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.preview_image);
        ImageView imageView = (ImageView) dialog.findViewById(R.id.imagePreview);
        ProgressBar progressBar = dialog.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        imageView.setImageURI(filePath);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.create();
        dialog.show();
    }

    private void setOnclickPreviewImage(TextView textViewName, final File fileName, final Context context){
        textViewName.setClickable(true);
        textViewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewImage(fileName, context);
            }
        });
        textViewName.setTextColor(context.getColor(R.color.colorPrimary));
    }

    private void previewImage(String url, Context context){
        Log.d("Url", url);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.preview_image);
        ImageView imageView = dialog.findViewById(R.id.imagePreview);
        ProgressBar progressBar = dialog.findViewById(R.id.progressBar);
        new DownloadImageTask(imageView, progressBar)
                .execute(url);

//        progressDialog.dismiss();
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.create();
        dialog.show();
    }

    private void setOnclickPreviewImage(TextView textViewName, final String url, final Context context){
        textViewName.setClickable(true);
        textViewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                previewImage(url, context);
            }
        });
        textViewName.setTextColor(context.getColor(R.color.colorPrimary));
    }

    private class DownloadImageTask extends AsyncTask<String, String, Bitmap> {
        ImageView bmImage;
        ProgressBar progressBar;

        public DownloadImageTask(ImageView bmImage, ProgressBar progressBar) {
            this.bmImage = bmImage;
            this.progressBar = progressBar;
        }

        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

//            TODO TEST, NOT READY TO DELETE
//            int count;
//            try {
//                FileFunction fileFunction = new FileFunction();
//                URL url = new URL(urldisplay);
//                URLConnection urlConnection = url.openConnection();
//                urlConnection.connect();
//                // show progress bar 0-100%
//                int fileLength = urlConnection.getContentLength();
//                InputStream inputStream = new BufferedInputStream(url.openStream(), 8192);
//                OutputStream outputStream = new FileOutputStream(fileFunction.getDefaultPathPictureBAF()+File.separator+urldisplay.split("[/]]")[6]);
//
//                byte data[] = new byte[1024];
//                long total = 0;
//                while ((count = inputStream.read(data)) != -1) {
//                    total += count;
//                    publishProgress("" + (int) ((total * 100) / fileLength));
//                    outputStream.write(data, 0, count);
//                }
//                // flushing output
//                outputStream.flush();
//                // closing streams
//                outputStream.close();
//                inputStream.close();
//
//            } catch (Exception e) {
//                Log.e("Error: ", e.getMessage());
//            }
            return mIcon11;
        }

        protected void onProgressUpdate(String... progress) {
            // progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        protected void onPostExecute(Bitmap result) {
            progressBar.setVisibility(View.GONE);
            bmImage.setImageBitmap(result);
        }
    }
}
