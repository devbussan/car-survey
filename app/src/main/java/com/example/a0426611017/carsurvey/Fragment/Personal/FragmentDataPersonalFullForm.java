package com.example.a0426611017.carsurvey.Fragment.Personal;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.CallApi.ApiPersonal;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.R;

/**
 * Created by 0426611017 on 2/15/2018.
 */

public class FragmentDataPersonalFullForm extends Fragment {
    private View view;
    private Context context;
    private String search;
    private int tab;
    private int menu;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ApiPersonal apiPersonal;
    private ProgressDialog progressDialog;

    private String adminId = null;

    public FragmentDataPersonalFullForm newInstance(String search, int tab, int menu) {
        FragmentDataPersonalFullForm fragmentPersonalFullFormTab = new FragmentDataPersonalFullForm();
        Bundle args = new Bundle();
        args.putString("search", search);
        args.putInt("tab", tab);
        args.putInt("menu", menu);
        fragmentPersonalFullFormTab.setArguments(args);
        return fragmentPersonalFullFormTab;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_company_complete_grid, container, false);
        context = container.getContext();
        search = getArguments().getString("search");
        tab = getArguments().getInt("tab");
        menu = getArguments().getInt("menu");

        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);

        apiPersonal = new ApiPersonal(view, context, progressDialog);
        adminId = context.getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, Context.MODE_PRIVATE).getString(Constans.KEY_ADMIN_ID, null);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipeToRefreshCompanyComplete);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                searchItem("");
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

        searchItem(search);

        return view;
    }

    private void searchItem(String search) {

        GridView gridView = view.findViewById(R.id.gridView);
        TextView textViewError = view.findViewById(R.id.text_view_info_error);

        switch (tab) {
            case Constans.TAB_CHECK_HQ:
                if(apiPersonal.getListPersonal(adminId, String.valueOf(Constans.STATUS_CHECK_HQ), tab, menu, search, gridView, textViewError, true)){
                    Log.d("Info", "succes get list");
                } else {
                    Log.d("Info", "buruan cari errornya !");
                }
                break;
            case Constans.TAB_RETURN_FULL:
                if(apiPersonal.getListPersonal(adminId, String.valueOf(Constans.STATUS_RETURN_FULL), tab, menu, search, gridView, textViewError,true)){
                    Log.d("Info", "sukses");
                } else {
                    Log.d("Info", "buruan cari errornya !");
                }
                break;
            case Constans.TAB_REJECT_FULL:
                if(apiPersonal.getListPersonal(adminId, String.valueOf(Constans.STATUS_REJECT_FULL),tab, menu, search, gridView, textViewError,true)){
                    Log.d("Info", "sukses");
                } else {
                    Log.d("Info", "buruan cari errornya !");
                }
                break;
            case Constans.TAB_COMPLETE:
                if(apiPersonal.getListPersonal(adminId, String.valueOf(Constans.STATUS_COMPLETE),tab, menu, search, gridView, textViewError,true)){
                    Log.d("Info", "sukses");
                } else {
                    Log.d("Info", "buruan cari errornya !");
                }
                break;
            case Constans.TAB_INPUT_FULL:
                if(apiPersonal.getListPersonal(adminId, String.valueOf(Constans.STATUS_INPUT_FULL),tab, menu, search, gridView, textViewError,true)){
                    Log.d("Info", "sukses");
                } else {
                    Log.d("Info", "buruan cari errornya !");
                }
                break;
            default:
                break;
        }
    }
}
