package com.example.a0426611017.carsurvey.ResponseObjectAPI;

import com.example.a0426611017.carsurvey.Model.ModelResponseLogin;
import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 28/02/2018.
 */

public class ResponseLogin {

    @SerializedName("status")
    private String status;

    @SerializedName("result")
    private ModelResponseLogin modelResponseLogin;

    @SerializedName("message")
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelResponseLogin getModelResponseLogin() {
        return modelResponseLogin;
    }

    public void setModelResponseLogin(ModelResponseLogin modelResponseLogin) {
        this.modelResponseLogin = modelResponseLogin;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
