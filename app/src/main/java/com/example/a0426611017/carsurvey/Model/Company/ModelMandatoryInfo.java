package com.example.a0426611017.carsurvey.Model.Company;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by c201017001 on 03/01/2018.
 */

public class ModelMandatoryInfo implements Serializable{

    @SerializedName("COMPANY_OTHER_INFO_ID")
    private String companyCustEvidenceId;

    @SerializedName("COMPANY_CUST_DATA_ID")
    private String companyCustDataId;

    @SerializedName("STATUS")
    private String status;

    @SerializedName("DEBITORBUSSCALE")
    private String debitorScale;

    @SerializedName("NOMORAKTA")
    private String nomorAkta;

    @SerializedName("SIUPNO")
    private String nomorSiup;

    @SerializedName("SKDOMUSAHANO")
    private String nomorDomisili;

    @SerializedName("TDPNO")
    private String nomorTdp;

    @SerializedName("AKTADATE")
    private String dateAkta;

    private String dateAktaFormat;

    @SerializedName("SIUPDATE")
    private String dateSiup;

    private String dateSiupFormat;

    @SerializedName("SKDOMDATE")
    private String dateSkDomisili;

    private String dateSkDomisiliFormat;

    @SerializedName("TDPDATE")
    private String dateTdp;

    private String dateTdpFormat;

    private String nameImageAkta;

    private String nameImageSiup;

    private String nameImageDomisili;

    private String nameImageTdp;

    @SerializedName("AKTAIMGURL")
    private String nameImageAktaUrl;

    @SerializedName("SIUPIMGURL")
    private String nameImageSiupUrl;

    @SerializedName("SKDOMIMGURL")
    private String nameImageDomisiliUrl;

    @SerializedName("TDPIMGURL")
    private String nameImageTdpUrl;

    @SerializedName("DEBITORGRP")
    private String debitorGroup;

    @SerializedName("COUNTERPARTCATEGORY")
    private String counterPart;

    @SerializedName("USRCRT")
    private String userCreate;

    private boolean isModified = false;
    private boolean isComplete = false;

    public String getCompanyCustEvidenceId() {
        return companyCustEvidenceId;
    }

    public void setCompanyCustEvidenceId(String companyCustEvidenceId) {
        this.companyCustEvidenceId = companyCustEvidenceId;
    }

    public String getCompanyCustDataId() {
        return companyCustDataId;
    }

    public void setCompanyCustDataId(String companyCustDataId) {
        this.companyCustDataId = companyCustDataId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDateAktaFormat() {
        return dateAktaFormat;
    }

    public void setDateAktaFormat(String dateAktaFormat) {
        this.dateAktaFormat = dateAktaFormat;
    }

    public String getDateSiupFormat() {
        return dateSiupFormat;
    }

    public void setDateSiupFormat(String dateSiupFormat) {
        this.dateSiupFormat = dateSiupFormat;
    }

    public String getDateSkDomisiliFormat() {
        return dateSkDomisiliFormat;
    }

    public void setDateSkDomisiliFormat(String dateSkDomisiliFormat) {
        this.dateSkDomisiliFormat = dateSkDomisiliFormat;
    }

    public String getDateTdpFormat() {
        return dateTdpFormat;
    }

    public void setDateTdpFormat(String dateTdpFormat) {
        this.dateTdpFormat = dateTdpFormat;
    }

    public String getDebitorScale() {
        return debitorScale;
    }

    public void setDebitorScale(String debitorScale) {
        this.debitorScale = debitorScale;
    }

    public String getNomorAkta() {
        return nomorAkta;
    }

    public void setNomorAkta(String nomorAkta) {
        this.nomorAkta = nomorAkta;
    }

    public String getNomorSiup() {
        return nomorSiup;
    }

    public void setNomorSiup(String nomorSiup) {
        this.nomorSiup = nomorSiup;
    }

    public String getNomorDomisili() {
        return nomorDomisili;
    }

    public void setNomorDomisili(String nomorDomisili) {
        this.nomorDomisili = nomorDomisili;
    }

    public String getNomorTdp() {
        return nomorTdp;
    }

    public void setNomorTdp(String nomorTdp) {
        this.nomorTdp = nomorTdp;
    }

    public String getDateAkta() {
        return dateAkta;
    }

    public void setDateAkta(String dateAkta) {
        this.dateAkta = dateAkta;
    }

    public String getDateSiup() {
        return dateSiup;
    }

    public void setDateSiup(String dateSiup) {
        this.dateSiup = dateSiup;
    }

    public String getDateSkDomisili() {
        return dateSkDomisili;
    }

    public void setDateSkDomisili(String dateSkDomisili) {
        this.dateSkDomisili = dateSkDomisili;
    }

    public String getDateTdp() {
        return dateTdp;
    }

    public void setDateTdp(String dateTdp) {
        this.dateTdp = dateTdp;
    }

    public String getNameImageAkta() {
        return nameImageAkta;
    }

    public void setNameImageAkta(String nameImageAkta) {
        this.nameImageAkta = nameImageAkta;
    }

    public String getNameImageSiup() {
        return nameImageSiup;
    }

    public void setNameImageSiup(String nameImageSiup) {
        this.nameImageSiup = nameImageSiup;
    }

    public String getNameImageDomisili() {
        return nameImageDomisili;
    }

    public void setNameImageDomisili(String nameImageDomisili) {
        this.nameImageDomisili = nameImageDomisili;
    }

    public String getNameImageTdp() {
        return nameImageTdp;
    }

    public void setNameImageTdp(String nameImageTdp) {
        this.nameImageTdp = nameImageTdp;
    }

    public String getNameImageAktaUrl() {
        return nameImageAktaUrl;
    }

    public void setNameImageAktaUrl(String nameImageAktaUrl) {
        this.nameImageAktaUrl = nameImageAktaUrl;
    }

    public String getNameImageSiupUrl() {
        return nameImageSiupUrl;
    }

    public void setNameImageSiupUrl(String nameImageSiupUrl) {
        this.nameImageSiupUrl = nameImageSiupUrl;
    }

    public String getNameImageDomisiliUrl() {
        return nameImageDomisiliUrl;
    }

    public void setNameImageDomisiliUrl(String nameImageDomisiliUrl) {
        this.nameImageDomisiliUrl = nameImageDomisiliUrl;
    }

    public String getNameImageTdpUrl() {
        return nameImageTdpUrl;
    }

    public void setNameImageTdpUrl(String nameImageTdpUrl) {
        this.nameImageTdpUrl = nameImageTdpUrl;
    }

    public String getDebitorGroup() {
        return debitorGroup;
    }

    public void setDebitorGroup(String debitorGroup) {
        this.debitorGroup = debitorGroup;
    }

    public String getCounterPart() {
        return counterPart;
    }

    public void setCounterPart(String counterPart) {
        this.counterPart = counterPart;
    }

    public String getUserCreate() {
        return userCreate;
    }

    public void setUserCreate(String userCreate) {
        this.userCreate = userCreate;
    }

    public boolean isModified() {
        return isModified;
    }

    public void setModified(boolean modified) {
        isModified = modified;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}
