package com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal;

import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelPersonalInfoMini;
import com.google.gson.annotations.SerializedName;

/**
 * Created by 0426611017 on 2/20/2018.
 */

public class ResponseMiniFormPersonal {

    @SerializedName("status")
    String status;

    @SerializedName("result")
    ModelPersonalInfoMini modelPersonalInfoMini;

    @SerializedName("message")
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ModelPersonalInfoMini getModelPersonalInfoMini() {
        return modelPersonalInfoMini;
    }

    public void setModelPersonalInfoMini(ModelPersonalInfoMini modelPersonalInfoMini) {
        this.modelPersonalInfoMini = modelPersonalInfoMini;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
