package com.example.a0426611017.carsurvey.Model.Company.Mini;

import com.google.gson.annotations.SerializedName;

/**
 * Created by c201017001 on 22/02/2018.
 */

public class ModelObjectGridListCompany {

    @SerializedName("LISTCOMPANY")
    private ModelGridListCompany [] modelGridListCompany;

    public ModelGridListCompany[] getModelGridListCompany() {
        return modelGridListCompany;
    }

    public void setModelGridListCompany(ModelGridListCompany[] modelGridListCompany) {
        this.modelGridListCompany = modelGridListCompany;
    }
}
