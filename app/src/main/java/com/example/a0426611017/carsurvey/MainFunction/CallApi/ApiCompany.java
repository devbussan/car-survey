package com.example.a0426611017.carsurvey.MainFunction.CallApi;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.Adapter.Company.AdapterCustomCompleteCompany;
import com.example.a0426611017.carsurvey.Adapter.Company.AdapterCustomPendingCompany;
import com.example.a0426611017.carsurvey.Finance.ActivityCompanyFinancial;
import com.example.a0426611017.carsurvey.MainActivityDrawer;
import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.Model.Company.ModelAddressInfo;
import com.example.a0426611017.carsurvey.Model.Company.ModelDetailContactInfo;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelGridListCompany;
import com.example.a0426611017.carsurvey.Model.Company.ModelMandatoryInfo;
import com.example.a0426611017.carsurvey.Model.Company.Mini.ModelRequestCompanySendHq;
import com.example.a0426611017.carsurvey.Model.Financial.ModelFinance;
import com.example.a0426611017.carsurvey.Model.Survey.ModelCekLingkungan;
import com.example.a0426611017.carsurvey.Model.Survey.ModelSurvey;
import com.example.a0426611017.carsurvey.Model.Personal.Mini.ModelRequestPersonalSendHq;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseCompanySendHq;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Finance.ResponseFinancial;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseFullFormCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Company.ResponseGridListCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseSurveyEnvFullFormCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Survey.ResponseSurveyFullFormCompany;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.Personal.ResponsePersonalSendHq;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by c201017001 on 20/02/2018.
 */

public class ApiCompany {

    private static List<ModelGridListCompany> listcompanyGrid = new ArrayList<>();
    private ApiInterface apiInterface;
    private View view;
    private Context context;
    private ProgressDialog loadingDialog;
    private int tab = 0;
    private int menu = 0;
    private GridView gridView;
    private String search = "";
    private boolean isSuccess = true;
    private SharedPreferences sharedpreferencesMenuCategory;
    private int menuCategory = 0;
    private boolean isSendHq = false;
    private boolean isNext = false;
    private String adminId = null;
    private String logId = null;
    private boolean isConnect = false;
    private Gson gson = new Gson();
    private ResponseGridListCompany responseGridListCompany = new ResponseGridListCompany();
    private ModelAddressInfo modelAddressInfo = new ModelAddressInfo();
    private ModelMandatoryInfo modelMandatoryInfo = new ModelMandatoryInfo();
    private List<ModelDetailContactInfo> modelDetailContactInfos = new ArrayList<>();
    private ResponseFullFormCompany responseFullFormCompany;
    private ResponseSurveyFullFormCompany responseSurveyFullFormCompany;
    private ResponseFinancial responseFinancial = new ResponseFinancial();
    private ResponseSurveyEnvFullFormCompany responseSurveyEnvFullFormCompany;
    private ModelSurvey mdlSurvey = new ModelSurvey();
    private String name = null;
    private String idCust = null;
    private List<ModelCekLingkungan> modelCekLingkunganList = new ArrayList<>();
    private ResponseCompanySendHq responseCompanySendHq = new ResponseCompanySendHq();
    private ResponsePersonalSendHq responsePersonalSendHq = new ResponsePersonalSendHq();
    private int numOfling = 0;
    private boolean isFull = false;
    private String surveyId;

    private boolean surveyIsSaved = false;
    private boolean[] surveyEnvIsSaved = {true, false, false, false, false};
    private TextView textViewError;

    public ApiCompany(View view, Context context, ProgressDialog loadingDialog) {
        this.view = view;
        this.context = context;
        this.loadingDialog = loadingDialog;
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
        sharedpreferencesMenuCategory = this.context.getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, MODE_PRIVATE);
        menuCategory = sharedpreferencesMenuCategory.getInt(Constans.KEY_MENU_CATEGORY, 0);
    }

    public ResponseSurveyFullFormCompany getResponseSurveyFullFormCompany() {
        return responseSurveyFullFormCompany;
    }

    public void setResponseSurveyFullFormCompany(ResponseSurveyFullFormCompany responseSurveyFullFormCompany) {
        this.responseSurveyFullFormCompany = responseSurveyFullFormCompany;
    }

    public ResponseSurveyEnvFullFormCompany getResponseSurveyEnvFullFormCompany() {
        return responseSurveyEnvFullFormCompany;
    }

    public void setResponseSurveyEnvFullFormCompany(ResponseSurveyEnvFullFormCompany responseSurveyEnvFullFormCompany) {
        this.responseSurveyEnvFullFormCompany = responseSurveyEnvFullFormCompany;
    }

    public ResponseFullFormCompany getResponseFullFormCompany() {
        return responseFullFormCompany;
    }

    public void setResponseFullFormCompany(ResponseFullFormCompany responseFullFormCompany) {
        this.responseFullFormCompany = responseFullFormCompany;
    }

    public void setSavedSurvey(boolean isSaved) {
        this.surveyIsSaved = isSaved;
    }

    public void setSavedEnvi(boolean isSaved, int idx) {
        this.surveyEnvIsSaved[idx] = isSaved;
    }

    public boolean getListCompany(String userId,
                                  String statusId,
                                  int tab,
                                  int menu,
                                  String search,
                                  GridView gridView,
                                  TextView textViewError,
                                  boolean isFull) {
        this.tab = tab;
        this.menu = menu;
        this.search = search;
        this.gridView = gridView;
        this.isFull = isFull;
        this.textViewError = textViewError;
        return getListCompany(userId, statusId);
    }

    public void saveSurveyEnv(List<ModelCekLingkungan> modelCekLingkunganList, String pName, final String idSurvey) {
        numOfling = modelCekLingkunganList.size();
        if (numOfling > 0) {
            for (int i = 0; i < numOfling; i++) {
                try {
                    ModelCekLingkungan mdlCek;
                    mdlCek = modelCekLingkunganList.get(i);
                    mdlCek.setUserCrt(adminId);

                    saveEnvironment(mdlCek, pName, idSurvey, i + 1);

                } catch (Exception e) {
                    e.printStackTrace();
                    Snackbar.make(view.findViewById(R.id.layout_activity_survey), pName + "  :  " + idSurvey
                                    + " : " + "Error",
                            Snackbar.LENGTH_LONG)
                            .show();
                    loadingDialog.dismiss();
                }
            }
        } else {
            if(isNext){
                Intent intent = new Intent(context, ActivityCompanyFinancial.class);
                intent.putExtra(Constans.KeySharedPreference.ORDER_ID, idCust);
                intent.putExtra(Constans.KEY_MENU, menu);
                context.startActivity(intent);
                loadingDialog.dismiss();
            }else{
                loadingDialog.dismiss();
                Intent intent = new Intent(context, MainActivityDrawer.class);
                intent.putExtra(Constans.KEY_MENU, menuCategory);
                intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                context.startActivity(intent);
            }
        }
    }

    private void saveEnvironment(final ModelCekLingkungan modelCekLingkungan, final String pName, final String idsurvey, final int idx) {
        Log.d("PEsan", gson.toJson(modelCekLingkungan));
        if (InternetConnection.checkConnection(context)) {
            Call<ResponseSurveyEnvFullFormCompany> saveSurveyEnviCom = apiInterface.saveSurveyEnvFormCompany(modelCekLingkungan, pName, idsurvey);
            saveSurveyEnviCom.enqueue(new Callback<ResponseSurveyEnvFullFormCompany>() {
                @Override
                public void onResponse(Call<ResponseSurveyEnvFullFormCompany> call, Response<ResponseSurveyEnvFullFormCompany> response) {
                    if (response.isSuccessful()) {
                        setResponseSurveyEnvFullFormCompany(response.body());
                        if (responseSurveyEnvFullFormCompany.getStatus().equals("success")) {
                            surveyEnvIsSaved[idx] = true;
//                                setSavedEnvi(true, idx);
                            if (idx == numOfling) {
                                if (isNext) {
                                    loadingDialog.dismiss();
                                    Intent intent = new Intent(context, ActivityCompanyFinancial.class);
                                    intent.putExtra(Constans.KeySharedPreference.ORDER_ID, idCust);
                                    intent.putExtra(Constans.KEY_MENU, menu);
                                    context.startActivity(intent);
                                } else {
                                    loadingDialog.dismiss();
                                    Intent intent = new Intent(context, MainActivityDrawer.class);
                                    intent.putExtra(Constans.KEY_MENU, menuCategory);
                                    intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                                    context.startActivity(intent);
                                }

                            } else {
                                if (isNext) {
                                    loadingDialog.dismiss();
                                    Intent intent = new Intent(context, ActivityCompanyFinancial.class);
                                    intent.putExtra(Constans.KeySharedPreference.ORDER_ID, idCust);
                                    intent.putExtra(Constans.KEY_MENU, menu);
                                    context.startActivity(intent);
                                } else {
                                    loadingDialog.dismiss();
                                    Intent intent = new Intent(context, MainActivityDrawer.class);
                                    intent.putExtra(Constans.KEY_MENU, menuCategory);
                                    intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                                    context.startActivity(intent);
                                }
                            }
                        } else {
                            //   <string name="content_question_are_you_sure">Are You Sure ?</string>
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(context.getString(R.string.dialog_error));
                            b.setCancelable(false);
                            b.setMessage("Failed saved Survey Environment data \n" +
                                     context.getString( R.string.dialog_try_again));
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    saveEnvironment(modelCekLingkungan, pName, idsurvey, idx);

                                }
                            });
                            b.show();
                            loadingDialog.dismiss();
                        }

                    } else if (response.code() == 404) {
                        isConnect = false;
                        String error = "";
                        try {
                            responseSurveyEnvFullFormCompany = gson.fromJson(response.errorBody().string(), ResponseSurveyEnvFullFormCompany.class);
                            error = responseSurveyFullFormCompany.getMessage();

                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(context.getString(R.string.dialog_error));
                            b.setCancelable(false);
                            b.setMessage("Failed saved Survey Environment data \n" +
                                   context.getString(R.string.dialog_try_again) );
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    saveEnvironment(modelCekLingkungan, pName, idsurvey, idx);

                                }
                            });
                            b.show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(context.getString(R.string.dialog_error));
                            b.setCancelable(false);
                            b.setMessage("Failed saved Survey Environment data \n" +
                                 context.getString(R.string.dialog_try_again));
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    saveEnvironment(modelCekLingkungan, pName, idsurvey, idx);

                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", ""+error);
                        loadingDialog.dismiss();
                    } else {
                        Log.e("Other Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(context.getString(R.string.dialog_error));
                        b.setCancelable(false);
                        b.setMessage("Failed saved Survey Environment data \n" +
                                context.getString(R.string.dialog_try_again));
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                saveEnvironment(modelCekLingkungan, pName, idsurvey, idx);

                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseSurveyEnvFullFormCompany> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle(context.getString(R.string.dialog_error));
                    b.setCancelable(false);
                    b.setMessage("Failed saved Survey Environment data \n" +
                            context.getString(R.string.dialog_try_again));
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            saveEnvironment(modelCekLingkungan, pName, idsurvey, idx);

                        }
                    });
                    b.show();

                    loadingDialog.dismiss();
                }
            });
        } else {
            Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(context.getString(R.string.dialog_error));
            b.setCancelable(false);
            b.setMessage("Failed saved Survey Environment data \n" +
                    context.getString(R.string.dialog_try_again));
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    saveEnvironment(modelCekLingkungan, pName, idsurvey, idx);

                }
            });
            b.show();
            loadingDialog.dismiss();
        }


    }

    public void saveSurvey(String surveyId, ModelSurvey modelSurvey, final String name, String idCust, List<ModelCekLingkungan> modelCekLingkunganList, String adminId, int menu, boolean isNext) {
        loadingDialog.show();
        this.mdlSurvey = modelSurvey;
        this.name = name;
        this.idCust = idCust;
        this.adminId = adminId;
        this.surveyId = surveyId;
        this.modelCekLingkunganList = modelCekLingkunganList;
        this.menu = menu;
        this.isNext = isNext;
        try {
            saveSurveyCompany(mdlSurvey, this.name, this.idCust);
        } catch (IOException e) {
            loadingDialog.dismiss();
            Snackbar.make(view.findViewById(R.id.layout_activity_survey), "Survey : Error Upload Image",
                    Snackbar.LENGTH_LONG)
                    .show();
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
    }

    public void saveSurveyCompany(final ModelSurvey mdlSurvey, final String name, String idCustData) throws IOException {
        Log.d("debug", gson.toJson(mdlSurvey));
        Log.d("debug", gson.toJson(name));
        Log.d("debug", gson.toJson(idCustData));
        Log.d("debug", gson.toJson(isNext));
        if (mdlSurvey.isModified()) {
            FileFunction fileFunction = new FileFunction();
            if (!mdlSurvey.getMotorcycleimgurl().startsWith("http")
                    && !mdlSurvey.getMotorcycleimg().equals("No selected file")) {
                mdlSurvey.setMotorcycleimgurl(fileFunction.createBase64(mdlSurvey.getMotorcycleimg()));
            }

            if (!mdlSurvey.getCarimgurl().startsWith("http")
                    && !mdlSurvey.getCarimg().equals("No selected file")) {
                mdlSurvey.setCarimgurl(fileFunction.createBase64(mdlSurvey.getCarimg()));
            }

            if (InternetConnection.checkConnection(context)) {
                //  objectRequestSurveyCompany = new ObjectRequestSurveyCompany(mdlSurvey);
                mdlSurvey.setUsrcrt(adminId);
                Call<ResponseSurveyFullFormCompany> saveSurveyCompany = apiInterface.saveSurveyFullFormCompany(mdlSurvey, name, idCustData);
                saveSurveyCompany.enqueue(new Callback<ResponseSurveyFullFormCompany>() {
                    @Override
                    public void onResponse(Call<ResponseSurveyFullFormCompany> call, Response<ResponseSurveyFullFormCompany> response) {
                        if (response.isSuccessful()) {
                            setResponseSurveyFullFormCompany(response.body());
                            if (responseSurveyFullFormCompany.getStatus().equals("success")) {
                                Log.d("Success", responseSurveyFullFormCompany.getStatus());
                                if (surveyId.equals("0")) {
                                    saveSurveyEnv(modelCekLingkunganList, name, responseSurveyFullFormCompany.getModelSurvey().getSurveyid());
                                } else {
                                    saveSurveyEnv(modelCekLingkunganList, name, surveyId);
                                }
                            } else {
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(context.getString(R.string.dialog_error));
                                b.setCancelable(false);
                                b.setMessage("Failed saved Survey data \n" +
                                        context.getString(R.string.dialog_try_again));
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveSurveyCompany(mdlSurvey, name, idCust);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Snackbar.make(view.findViewById(R.id.layout_activity_survey), "Survey : Error Upload Image",
                                                    Snackbar.LENGTH_LONG)
                                                    .show();
                                            Log.e("Error", e.getMessage());
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                            }
                        } else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";
                            try {
                                responseSurveyFullFormCompany = gson.fromJson(response.errorBody().string(), ResponseSurveyFullFormCompany.class);
                                error = responseSurveyFullFormCompany.getMessage();
                                Snackbar.make(view.findViewById(R.id.layout_activity_survey), error,
                                        Snackbar.LENGTH_LONG)
                                        .show();
                                loadingDialog.dismiss();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(context.getString(R.string.dialog_error));
                                b.setCancelable(false);
                                b.setMessage("Failed saved Survey data \n" +
                                       context.getString(R.string.dialog_try_again));
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveSurveyCompany(mdlSurvey, name, idCust);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Snackbar.make(view.findViewById(R.id.layout_activity_survey), "Survey : Error Upload Image",
                                                    Snackbar.LENGTH_LONG)
                                                    .show();
                                            Log.e("Error", e.getMessage());
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                            } catch (IOException e) {
                                error = e.getMessage();
                                e.printStackTrace();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(context.getString(R.string.dialog_error));
                                b.setCancelable(false);
                                b.setMessage("Failed saved Survey data \n" +
                                        R.string.dialog_try_again);
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        try {
                                            saveSurveyCompany(mdlSurvey, name, idCust);
                                        } catch (IOException e) {
                                            loadingDialog.dismiss();
                                            Snackbar.make(view.findViewById(R.id.layout_activity_survey), "Survey : Error Upload Image",
                                                    Snackbar.LENGTH_LONG)
                                                    .show();
                                            Log.e("Error", e.getMessage());
                                            e.printStackTrace();
                                        }
                                    }
                                });
                                b.show();
                                loadingDialog.dismiss();
                            }

                            Log.e("Error", error);
                        } else {
                            isConnect = false;
                            Log.e("Other Error", String.valueOf(R.string.error_api));
                            isSuccess = false;
                            loadingDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseSurveyFullFormCompany> call, Throwable throwable) {
                        Log.e("Error Retrofit", throwable.toString());
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(context.getString(R.string.dialog_error));
                        b.setCancelable(false);
                        b.setMessage("Failed saved Survey data \n" +
                                context.getString(R.string.dialog_try_again));
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    saveSurveyCompany(mdlSurvey, name, idCust);
                                } catch (IOException e) {
                                    loadingDialog.dismiss();
                                    Snackbar.make(view.findViewById(R.id.layout_activity_survey), "Survey : Error Upload Image",
                                            Snackbar.LENGTH_LONG)
                                            .show();
                                    Log.e("Error", e.getMessage());
                                    e.printStackTrace();
                                }
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                });
            } else {
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle(context.getString(R.string.dialog_error));
                b.setCancelable(false);
                b.setMessage("Failed saved Survey data \n" +
                        context.getString(R.string.dialog_try_again));
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            saveSurveyCompany(mdlSurvey, name, idCust);
                        } catch (IOException e) {
                            loadingDialog.dismiss();
                            Snackbar.make(view.findViewById(R.id.layout_activity_survey), "Survey : Error Upload Image",
                                    Snackbar.LENGTH_LONG)
                                    .show();
                            Log.e("Error", e.getMessage());
                            e.printStackTrace();
                        }
                    }
                });
                b.show();
                Log.e("Error Connection",  String.valueOf( R.string.no_connectivity));
                loadingDialog.dismiss();
            }
        } else {
            if (surveyId.equals("0")) {
                Button buttonDataSurvey = view.findViewById(R.id.button_data_survey);
                buttonDataSurvey.setError("Not Complete");
                Snackbar.make(view.findViewById(R.id.layout_activity_survey), "You Must Fill Survey Data, Please Click Survey Data Button",
                        Snackbar.LENGTH_LONG).show();
                loadingDialog.dismiss();
            } else {
                saveSurveyEnv(modelCekLingkunganList, name, surveyId);
            }
        }
    }

    public boolean getListCompany(final String userId, final String statusId) {
        loadingDialog.show();
        listcompanyGrid.clear();
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseGridListCompany> gridList;
            if (isFull) {
                gridList = apiInterface.getListDataCompanyFull(userId, statusId);
            } else {
                gridList = apiInterface.getListDataCompany(userId, statusId);
            }
            gridList.enqueue(new Callback<ResponseGridListCompany>() {
                @Override
                public void onResponse(Call<ResponseGridListCompany> call, Response<ResponseGridListCompany> response) {
                    if (response.isSuccessful()) {
                        responseGridListCompany = response.body();
                        Log.d("Result", gson.toJson(responseGridListCompany.getModelObjectGridListCompany().getModelGridListCompany()));
                        if (createGridList(responseGridListCompany)) {
                            textViewError.setText(null);
                            Log.d("Info Grid Liist", "Success Create Grid List Company");
                        }
                        loadingDialog.dismiss();
                        isConnect = true;
                    } else if (response.code() == 404) {
                        isConnect = false;
                        String error = "";
                        try {
                            responseGridListCompany = gson.fromJson(response.errorBody().string(), ResponseGridListCompany.class);
                            error = responseGridListCompany.getMessage();
                            if(!error.equals("Data Not Found")){
                                error = error.concat("\n Please Refresh by Pulling Down");
                            }

                        } catch (IOException e) {
                            Log.e("Error",e.getMessage());
                            error = "Something went wrong \n Please Refresh by Pulling Down";
                            e.printStackTrace();
                        }
                        textViewError.setText(error);
                        gridView.setAdapter(null);
                        loadingDialog.dismiss();
                    } else {
                        textViewError.setText(context.getString(R.string.error_api).concat("\n" +
                                "Please Refresh by Pulling Down"));
                        gridView.setAdapter(null);
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseGridListCompany> call, Throwable t) {
                    Log.e("Error Retrofit", t.toString());
                    textViewError.setText(t.toString().concat("\n" +
                            "Please Refresh by Pulling Down"));
                    gridView.setAdapter(null);
                    loadingDialog.dismiss();
                }
            });
        } else {
            Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
            textViewError.setText(context.getString(R.string.content_question_internet_error).concat("\n" +
                    "Please Refresh by Pulling Down"));
            gridView.setAdapter(null);
            loadingDialog.dismiss();
        }
        return isSuccess;
    }

    private boolean createGridList(ResponseGridListCompany responseGridListCompany) {
        if (tab == 0 || tab == 2 || tab == 3) {
            if (!search.equals("")) {
                listcompanyGrid.clear();
                for (ModelGridListCompany modelGridListCompany : responseGridListCompany.getModelObjectGridListCompany().getModelGridListCompany()) {
                    if (modelGridListCompany.getCompanyName().contains(search)
                            || modelGridListCompany.getDateCreate().contains(search)
                            || modelGridListCompany.getNpwpNo().contains(search)) {
                        listcompanyGrid.add(modelGridListCompany);
                    }
                }
                gridView.setAdapter(new AdapterCustomPendingCompany(context, listcompanyGrid, tab, menu));
            } else {
                gridView.setAdapter(new AdapterCustomPendingCompany(context, Arrays.asList(responseGridListCompany.getModelObjectGridListCompany().getModelGridListCompany()), tab, menu));
            }
        } else {
            if (!search.equals("")) {
                listcompanyGrid.clear();
                for (ModelGridListCompany modelGridListCompany : responseGridListCompany.getModelObjectGridListCompany().getModelGridListCompany()) {
                    if (modelGridListCompany.getCompanyName().contains(search)
                            || modelGridListCompany.getDateCreate().contains(search)
                            || modelGridListCompany.getNpwpNo().contains(search)) {
                        listcompanyGrid.add(modelGridListCompany);
                    }
                }
                gridView.setAdapter(new AdapterCustomCompleteCompany(context, listcompanyGrid, tab, menu));
            } else {
                gridView.setAdapter(new AdapterCustomCompleteCompany(context, Arrays.asList(responseGridListCompany.getModelObjectGridListCompany().getModelGridListCompany()), tab, menu));
            }
        }
        return true;
    }

    public void saveFinance(final ModelFinance modelFinance,
                            String orderid,
                            String name,
                            String adminId,
                            String logId,
                            boolean isSendHq) {
        this.adminId = adminId;
        this.logId = logId;
        this.isSendHq = isSendHq;
        saveFinance(modelFinance, orderid, name);
    }

    public void saveFinance(final ModelFinance modelFinance, final String orderid, final String name) {
        loadingDialog.show();
        Log.d("Request", gson.toJson(modelFinance));
        Log.d("Request", name + " " + orderid);
        if (modelFinance.isModified()) {
            if (InternetConnection.checkConnection(context)) {
                Call<ResponseFinancial> saveSurveyCompany = apiInterface.saveFinance(name, orderid, modelFinance);
                saveSurveyCompany.enqueue(new Callback<ResponseFinancial>() {
                    @Override
                    public void onResponse(Call<ResponseFinancial> call, Response<ResponseFinancial> response) {
                        if (response.isSuccessful()) {
                            responseFinancial = response.body();
                            if (responseFinancial.getStatus().equals("success")) {
                                Log.d("Status", responseFinancial.getModelFinance().getResponseFinancing());

                                if (isSendHq) {
                                    ModelRequestCompanySendHq modelRequestCompanySendHq = new ModelRequestCompanySendHq();
                                    modelRequestCompanySendHq.setUsrPd(adminId);
                                    modelRequestCompanySendHq.setStaTus("5");
                                    modelRequestCompanySendHq.setIsReadcmo("0");

                                    ModelRequestPersonalSendHq modelRequestPersonalSendHq = new ModelRequestPersonalSendHq();
                                    modelRequestPersonalSendHq.setUsrPd(adminId);
                                    modelRequestPersonalSendHq.setStatus("5");
                                    modelRequestPersonalSendHq.setIsReadCmo("0");

                                    if (name.equals(Constans.PARAM_WS_PERSONAL)) {
                                        sendHq(modelRequestPersonalSendHq, logId, orderid);
                                    } else {
                                        sendHq(modelRequestCompanySendHq, logId, orderid);
                                    }
                                } else {
                                    loadingDialog.dismiss();
                                    Intent intent = new Intent(context, MainActivityDrawer.class);
                                    intent.putExtra(Constans.KEY_MENU, menuCategory);
                                    intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                                    context.startActivity(intent);
                                }
                            } else {
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(context.getString(R.string.dialog_error));
                                b.setCancelable(false);
                                b.setMessage("Failed saved Financial data \n" +
                                        context.getString(R.string.dialog_try_again));
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFinance(modelFinance, orderid, name);
                                    }
                                });
                                b.show();
                            }
                        } else if (response.code() == 404) {
                            isConnect = false;
                            String error = "";
                            try {
                                responseFinancial = gson.fromJson(response.errorBody().string(), ResponseFinancial.class);
                                error = responseFinancial.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(context.getString(R.string.dialog_error));
                                b.setCancelable(false);
                                b.setMessage("Failed saved Financial data \n" +
                                        context.getString(R.string.dialog_try_again));
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFinance(modelFinance, orderid, name);
                                    }
                                });
                                b.show();
                            } catch (IOException e) {
                                error = e.getMessage();
                                AlertDialog.Builder b = new AlertDialog.Builder(context);
                                b.setTitle(context.getString(R.string.dialog_error));
                                b.setCancelable(false);
                                b.setMessage("Failed saved Financial data \n" +
                                        context.getString(R.string.dialog_try_again));
                                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        saveFinance(modelFinance, orderid, name);
                                    }
                                });
                                b.show();
                                e.printStackTrace();
                            }
                            Log.e("Error", error);
                            loadingDialog.dismiss();
                        } else {
                            Log.e("Other Error", String.valueOf(R.string.error_api));
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(context.getString(R.string.dialog_error));
                            b.setCancelable(false);
                            b.setMessage("Failed saved Financial data \n" +
                                    context.getString(R.string.dialog_try_again));
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    saveFinance(modelFinance, orderid, name);
                                }
                            });
                            b.show();
                            loadingDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseFinancial> call, Throwable throwable) {
                        Log.e("Error Retrofit", throwable.toString());
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(context.getString(R.string.dialog_error));
                        b.setCancelable(false);
                        b.setMessage("Failed saved Financial data \n" +
                                context.getString(R.string.dialog_try_again));
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                saveFinance(modelFinance, orderid, name);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                });
            } else {
                Log.e("Error Connection", String.valueOf(R.string.no_connectivity));
                AlertDialog.Builder b = new AlertDialog.Builder(context);
                b.setTitle(context.getString(R.string.dialog_error));
                b.setCancelable(false);
                b.setMessage("Failed saved Financial data \n" +
                        context.getString(R.string.dialog_try_again));
                b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        saveFinance(modelFinance, orderid, name);
                    }
                });
                b.show();
                loadingDialog.dismiss();
            }
        } else {
            loadingDialog.dismiss();
            Intent intent = new Intent(context, MainActivityDrawer.class);
            intent.putExtra(Constans.KEY_MENU, menuCategory);
            intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
            context.startActivity(intent);
        }
    }

    public void sendHq(final ModelRequestCompanySendHq modelRequestCompanySendHq, final String idLog, final String idOrder) {
        Log.d("Request", gson.toJson(modelRequestCompanySendHq));
        Log.d("Log", "" + idLog);
        Log.d("idOrder", "" + idOrder);
        if (InternetConnection.checkConnection(context)) {
            final Call<ResponseCompanySendHq> sendHq = apiInterface.submitHq(modelRequestCompanySendHq, idLog, idOrder);
            sendHq.enqueue(new Callback<ResponseCompanySendHq>() {
                @Override
                public void onResponse(Call<ResponseCompanySendHq> call, Response<ResponseCompanySendHq> response) {
                    if (response.isSuccessful()) {
                        responseCompanySendHq = response.body();
                        Log.d("Response", responseCompanySendHq.getModelMiniFormCompany().getLogCompany());
                        Snackbar.make(view.findViewById(R.id.layout_finance), responseCompanySendHq.getModelMiniFormCompany().getLogCompany(),
                                Snackbar.LENGTH_LONG)
                                .show();
                        Intent intent = new Intent(context, MainActivityDrawer.class);
                        intent.putExtra(Constans.KEY_MENU, menuCategory);
                        intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                        context.startActivity(intent);
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        isConnect = false;
                        String error = "";
                        try {
                            responseCompanySendHq = gson.fromJson(response.errorBody().string(), ResponseCompanySendHq.class);
                            error = responseCompanySendHq.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(context.getString(R.string.dialog_error));
                            b.setCancelable(false);
                            b.setMessage("Failed saved Financial data \n" +
                                    context.getString(R.string.dialog_try_again));
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sendHq(modelRequestCompanySendHq, idLog, idOrder);
                                }
                            });
                            b.show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(context.getString(R.string.dialog_error));
                            b.setCancelable(false);
                            b.setMessage("Failed Send to HQ \n" +
                                    context.getString(R.string.dialog_try_again));
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sendHq(modelRequestCompanySendHq, idLog, idOrder);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        Log.e("Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(context.getString(R.string.dialog_error));
                        b.setCancelable(false);
                        b.setMessage("Failed Send to HQ \n" +
                                context.getString(R.string.dialog_try_again));
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sendHq(modelRequestCompanySendHq, idLog, idOrder);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponseCompanySendHq> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle(context.getString(R.string.dialog_error));
                    b.setCancelable(false);
                    b.setMessage("Failed Send to HQ \n" +
                            context.getString(R.string.dialog_try_again));
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendHq(modelRequestCompanySendHq, idLog, idOrder);
                        }
                    });
                    b.show();
                }
            });
        } else {
            Log.e("Error", String.valueOf(R.string.no_connectivity));
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(context.getString(R.string.dialog_error));
            b.setCancelable(false);
            b.setMessage("Failed Send to HQ \n" +
                    context.getString(R.string.dialog_try_again));
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendHq(modelRequestCompanySendHq, idLog, idOrder);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }

    }

    public void sendHq(final ModelRequestPersonalSendHq modelRequestPersonalSendHq, final String idLog, final String idOrder) {
        Log.d("Request", gson.toJson(modelRequestPersonalSendHq));
        Log.d("Log", "" + idLog);
        Log.d("idOrder", "" + idOrder);
        if (InternetConnection.checkConnection(context)) {
            Call<ResponsePersonalSendHq> sendHq = apiInterface.submitHq(modelRequestPersonalSendHq, idLog, idOrder);
            sendHq.enqueue(new Callback<ResponsePersonalSendHq>() {
                @Override
                public void onResponse(Call<ResponsePersonalSendHq> call, Response<ResponsePersonalSendHq> response) {
                    if (response.isSuccessful()) {
                        responsePersonalSendHq = response.body();
                        Log.d("Response", responsePersonalSendHq.getModelRequestPersonalSendHq().getLog_personal());
                        Snackbar.make(view.findViewById(R.id.layout_finance), responsePersonalSendHq.getModelRequestPersonalSendHq().getLog_personal(),
                                Snackbar.LENGTH_LONG)
                                .show();
                        Intent intent = new Intent(context, MainActivityDrawer.class);
                        intent.putExtra(Constans.KEY_MENU, menuCategory);
                        intent.putExtra(Constans.KEY_CURRENT_TAB, context.getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, MODE_PRIVATE).getInt(Constans.KEY_TAB, -1));
                        context.startActivity(intent);

                        isConnect = true;
                        loadingDialog.dismiss();
                    } else if (response.code() == 404) {
                        isConnect = false;
                        String error = "";
                        try {
                            responsePersonalSendHq = gson.fromJson(response.errorBody().string(), ResponsePersonalSendHq.class);
                            error = responsePersonalSendHq.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(context.getString(R.string.dialog_error));
                            b.setCancelable(false);
                            b.setMessage("Failed Send to HQ \n" +
                                    context.getString(R.string.dialog_try_again));
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sendHq(modelRequestPersonalSendHq, idLog, idOrder);
                                }
                            });
                            b.show();
                        } catch (IOException e) {
                            error = e.getMessage();
                            AlertDialog.Builder b = new AlertDialog.Builder(context);
                            b.setTitle(context.getString(R.string.dialog_error));
                            b.setCancelable(false);
                            b.setMessage("Failed Send to HQ \n" +
                                    context.getString(R.string.dialog_try_again));
                            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    sendHq(modelRequestPersonalSendHq, idLog, idOrder);
                                }
                            });
                            b.show();
                            e.printStackTrace();
                        }
                        Log.e("Error", error);
                        loadingDialog.dismiss();
                    } else {
                        isConnect = false;
                        Log.e("Error", String.valueOf(R.string.error_api));
                        AlertDialog.Builder b = new AlertDialog.Builder(context);
                        b.setTitle(context.getString(R.string.dialog_error));
                        b.setCancelable(false);
                        b.setMessage("Failed Send to HQ \n" +
                                context.getString(R.string.dialog_try_again));
                        b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sendHq(modelRequestPersonalSendHq, idLog, idOrder);
                            }
                        });
                        b.show();
                        loadingDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ResponsePersonalSendHq> call, Throwable throwable) {
                    Log.e("Error Retrofit", throwable.toString());
                    AlertDialog.Builder b = new AlertDialog.Builder(context);
                    b.setTitle(context.getString(R.string.dialog_error));
                    b.setCancelable(false);
                    b.setMessage("Failed Send to HQ \n" +
                            context.getString(R.string.dialog_try_again));
                    b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            sendHq(modelRequestPersonalSendHq, idLog, idOrder);
                        }
                    });
                    b.show();
                }
            });
        } else {
            Log.e("Error", String.valueOf(R.string.no_connectivity));
            AlertDialog.Builder b = new AlertDialog.Builder(context);
            b.setTitle(context.getString(R.string.dialog_error));
            b.setCancelable(false);
            b.setMessage("Failed Send to HQ \n" +
                    context.getString(R.string.dialog_try_again));
            b.setPositiveButton("RETRY", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendHq(modelRequestPersonalSendHq, idLog, idOrder);
                }
            });
            b.show();
            loadingDialog.dismiss();
        }

    }
}
