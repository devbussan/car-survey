package com.example.a0426611017.carsurvey.Object.Financial;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.Currency;
import com.example.a0426611017.carsurvey.Model.Financial.ModelFinance;
import com.example.a0426611017.carsurvey.R;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by 0414831216 on 2/16/2018.
 */

public class ObjectFinanceDetail  {
    private LayoutInflater inflater;
    private View view;
    private Context context;

    private TextView  label_nama_dealer_detail,label_merek_detail,label_type_pembiayaan_detail,label_seri_detail,
            label_warna_detail,label_aset_condition_detail,label_nama_bpkb_detail,label_bpkb_city_issued_detail,
            label_otr_detail,label_cashback_detail,label_dp_percent_detail,label_dp_amount_detail,label_tenor_detail,
            label_payment_request_detail,label_angsuran_detail,label_asuransi_detail,label_rate_detail,label_admin_detail,
            label_provisi_detail,label_pembayaran_detail,label_cust_notifikasi_detail;

    private TextView  value_nama_dealer_detail,value_merek_detail,value_type_pembiayaan_detail,value_seri_detail,
            value_warna_detail,value_aset_condition_detail,value_nama_bpkb_detail,value_bpkb_city_issued_detail,
            value_otr_detail,value_cashback_detail,value_dp_percent_detail,value_dp_amount_detail,value_tenor_detail,
            value_payment_request_detail,value_angsuran_detail,value_asuransi_detail,value_rate_detail,value_admin_detail,
            value_provisi_detail,value_pembayaran_detail,value_cust_notifikasi_detail;




    public  ObjectFinanceDetail(ViewGroup container, LayoutInflater inflater){
        this.inflater = inflater;
        this.context = container.getContext();
        this.view = this.inflater.inflate(R.layout.fragment_financial_detail,container,false);
        setFieldValue();
        setFieldLabel();
        //        this.modelDDLFinance = modelDDLFinance;
    }


    public View getView() {
        return view;
    }


    private void setFieldLabel() {
        label_nama_dealer_detail = view.findViewById(R.id.label_nama_dealer_detail);
        label_merek_detail = view.findViewById(R.id.label_merek_detail);
        label_type_pembiayaan_detail = view.findViewById(R.id.label_type_pembiayaan_detail);
        label_seri_detail = view.findViewById(R.id.label_seri_detail);
        label_warna_detail = view.findViewById(R.id.label_warna_detail);
        label_aset_condition_detail = view.findViewById(R.id.label_aset_condition_detail);
        label_nama_bpkb_detail = view.findViewById(R.id.label_nama_bpkb_detail);
        label_bpkb_city_issued_detail = view.findViewById(R.id.label_bpkb_city_issued_detail);
        label_otr_detail = view.findViewById(R.id.label_otr_detail);
        label_cashback_detail = view.findViewById(R.id.label_cashback_detail);
        label_dp_percent_detail = view.findViewById(R.id.label_dp_percent_detail);
        label_dp_amount_detail = view.findViewById(R.id.label_dp_amount_detail);
        label_tenor_detail = view.findViewById(R.id.label_tenor_detail);
        label_payment_request_detail = view.findViewById(R.id.label_payment_request_detail);
        label_angsuran_detail = view.findViewById(R.id.label_angsuran_detail);
        label_asuransi_detail = view.findViewById(R.id.label_asuransi_detail);
        label_rate_detail = view.findViewById(R.id.label_rate_detail);
        label_admin_detail = view.findViewById(R.id.label_admin_detail);
        label_provisi_detail = view.findViewById(R.id.label_provisi_detail);
        label_pembayaran_detail = view.findViewById(R.id.label_pembayaran_detail);
        label_cust_notifikasi_detail = view.findViewById(R.id.label_cust_notifikasi_detail);
    }

    private void setFieldValue() {
        value_nama_dealer_detail = view.findViewById(R.id.value_nama_dealer_detail);
        value_merek_detail = view.findViewById(R.id.value_merek_detail);
        value_type_pembiayaan_detail= view.findViewById(R.id.value_type_pembiayaan_detail);
        value_seri_detail = view.findViewById(R.id.value_seri_detail);
        value_warna_detail  = view.findViewById(R.id.value_warna_detail);
        value_aset_condition_detail  = view.findViewById(R.id.value_aset_condition_detail);
        value_nama_bpkb_detail  = view.findViewById(R.id.value_nama_bpkb_detail);
        value_bpkb_city_issued_detail  = view.findViewById(R.id.value_bpkb_city_issued_detail);
        value_otr_detail  = view.findViewById(R.id.value_otr_detail);
        value_cashback_detail  = view.findViewById(R.id.value_cashback_detail);
        value_dp_percent_detail  = view.findViewById(R.id.value_dp_percent_detail);
        value_dp_amount_detail  = view.findViewById(R.id.value_dp_amount_detail);
        value_tenor_detail  = view.findViewById(R.id.value_tenor_detail);
        value_payment_request_detail  = view.findViewById(R.id.value_payment_request_detail);
        value_angsuran_detail  = view.findViewById(R.id.value_angsuran_detail);
        value_asuransi_detail  = view.findViewById(R.id.value_asuransi_detail);
        value_rate_detail  = view.findViewById(R.id.value_rate_detail);
        value_admin_detail  = view.findViewById(R.id.value_admin_detail);
        value_provisi_detail  = view.findViewById(R.id.value_provisi_detail);
        value_pembayaran_detail  = view.findViewById(R.id.value_pembayaran_detail);
        value_cust_notifikasi_detail  = view.findViewById(R.id.value_cust_notifikasi_detail);
    }

    public void setFormForCompany(){
        label_nama_dealer_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_merek_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_type_pembiayaan_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_seri_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_warna_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_aset_condition_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_nama_bpkb_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_bpkb_city_issued_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_otr_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_cashback_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_dp_percent_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_dp_amount_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_tenor_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_payment_request_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_angsuran_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_asuransi_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_rate_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_admin_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_provisi_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_pembayaran_detail.setTextColor(context.getResources().getColor(R.color.color_company));
        label_cust_notifikasi_detail.setTextColor(context.getResources().getColor(R.color.color_company));
    }
    public void setFormForPersonal(){
        label_nama_dealer_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_merek_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_type_pembiayaan_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_seri_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_warna_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_aset_condition_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_nama_bpkb_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_bpkb_city_issued_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_otr_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_cashback_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_dp_percent_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_dp_amount_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_tenor_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_payment_request_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_angsuran_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_asuransi_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_rate_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_admin_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_provisi_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_pembayaran_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
        label_cust_notifikasi_detail.setTextColor(context.getResources().getColor(R.color.color_personal));
    }

    public void  setForm(ModelFinance modelFinance){
        value_nama_dealer_detail.setText(modelFinance.getNamaDealer());
        value_merek_detail.setText(modelFinance.getMerk());
        value_type_pembiayaan_detail.setText(modelFinance.getTypePembiayaan());
        value_seri_detail.setText(modelFinance.getSerial());
        value_warna_detail.setText(modelFinance.getWarna());
        value_aset_condition_detail.setText(modelFinance.getAsetCondition());
        value_nama_bpkb_detail.setText(modelFinance.getNamaBpkb());
        value_bpkb_city_issued_detail.setText(modelFinance.getBpkbCityIssued());
        value_otr_detail.setText(modelFinance.getOtr());
        if(!value_otr_detail.getText().toString().equals("")){
            String cleanString = value_otr_detail.getText().toString().replaceAll("[,.]", "");

            double parsed = Double.parseDouble(cleanString);
            String formatted = NumberFormat.getCurrencyInstance(new Locale("en", "US")).format((parsed / 100));

            value_otr_detail.setText(formatted.replaceAll("[$]",""));
        }
        value_cashback_detail.setText(modelFinance.getCashback());
        if(!value_cashback_detail.getText().toString().equals("")){
            String cleanString = value_cashback_detail.getText().toString().replaceAll("[,.]", "");

            double parsed = Double.parseDouble(cleanString);
            String formatted = NumberFormat.getCurrencyInstance(new Locale("en", "US")).format((parsed / 100));

            value_cashback_detail.setText(formatted.replaceAll("[$]",""));
        }
        value_dp_percent_detail.setText(modelFinance.getDpPercent());
        value_dp_amount_detail.setText(modelFinance.getDpAmount());
        if(!value_dp_amount_detail.getText().toString().equals("")){
            String formatted = Currency.set(value_dp_amount_detail);
            value_dp_amount_detail.setText(formatted.replaceAll("[$]",""));
        }
        value_tenor_detail.setText(modelFinance.getTenor());
        value_payment_request_detail.setText(modelFinance.getPaymentRequest());
        value_angsuran_detail.setText(modelFinance.getAngsuran());
        if(!value_angsuran_detail.getText().toString().equals("")){
            String formatted = Currency.set(value_angsuran_detail);
            value_angsuran_detail.setText(formatted.replaceAll("[$]",""));
        }
        value_asuransi_detail.setText(modelFinance.getAsuransi());
        value_rate_detail.setText(modelFinance.getRate());
        value_admin_detail.setText(modelFinance.getAdmin());
        value_provisi_detail.setText(modelFinance.getProvisi());
        value_pembayaran_detail.setText(modelFinance.getPembayaran());
        value_cust_notifikasi_detail.setText(modelFinance.getCustNotifikasi());
    }

}
