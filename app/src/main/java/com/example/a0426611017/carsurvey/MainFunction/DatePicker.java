package com.example.a0426611017.carsurvey.MainFunction;

import android.app.DatePickerDialog;
import android.content.Context;
import android.view.View;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by c201017001 on 10/01/2018.
 */

public class DatePicker extends FormatDate{

    private DatePickerDialog datePickerDialog;

    public void callDatePicker(int formatDate, EditText editTextDate, final Context context){
        this.callDatePicker(format[formatDate], editTextDate, context);
    }

    public void callDatePicker(final String formatDate, EditText editTextDate, final Context context){
        final EditText date = editTextDate;
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
//              jika tanggal telah diset maka gunakan kondisi if dibawah
                String dateStringTemp = date.getText().toString();
                if(!dateStringTemp.equals("")){
                    try {
                        Date dateCurrent = new SimpleDateFormat(formatDate, new Locale("id")).parse(dateStringTemp);
                        c.setTime(dateCurrent);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    mYear = c.get(Calendar.YEAR);
                    mMonth = c.get(Calendar.MONTH);
                    mDay = c.get(Calendar.DAY_OF_MONTH);
                }
                // date picker dialog
                datePickerDialog = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(android.widget.DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                Calendar cal = Calendar.getInstance();
                                cal.set(year,monthOfYear,dayOfMonth);
                                String dateFormat = new SimpleDateFormat(formatDate, new Locale("id")).format(cal.getTime());
//                                date.setText(dayOfMonth + "/"
//                                        + (monthOfYear + 1) + "/" + year);
                                date.setText(dateFormat);
                                date.setError(null);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }

        });
        date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR); // current year
                    int mMonth = c.get(Calendar.MONTH); // current month
                    int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                    String dateStringTemp = date.getText().toString();
                    if(!dateStringTemp.equals("")){
                        try {
                            Date dateCurrent = new SimpleDateFormat(formatDate, new Locale("id")).parse(dateStringTemp);
                            c.setTime(dateCurrent);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        mYear = c.get(Calendar.YEAR);
                        mMonth = c.get(Calendar.MONTH);
                        mDay = c.get(Calendar.DAY_OF_MONTH);
                    }
                    // date picker dialog
                    datePickerDialog = new DatePickerDialog(context,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(android.widget.DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    // set day of month , month and year value in the edit text

                                    Calendar cal = Calendar.getInstance();
                                    cal.set(year,monthOfYear,dayOfMonth);
                                    String dateFormat = new SimpleDateFormat(formatDate, new Locale("id")).format(cal.getTime());
//                                date.setText(dayOfMonth + "/"
//                                        + (monthOfYear + 1) + "/" + year);
                                    date.setText(dateFormat);
                                    date.setError(null);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialog.show();
                }
            }
        });
    }
}
