package com.example.a0426611017.carsurvey;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.a0426611017.carsurvey.MainFunction.ApiClient;
import com.example.a0426611017.carsurvey.MainFunction.ApiInterface;
import com.example.a0426611017.carsurvey.MainFunction.Constans;
import com.example.a0426611017.carsurvey.MainFunction.FileFunction;
import com.example.a0426611017.carsurvey.MainFunction.InternetConnection;
import com.example.a0426611017.carsurvey.MainFunction.MD5Hashing;
import com.example.a0426611017.carsurvey.Model.ModelRequestLogin;
import com.example.a0426611017.carsurvey.ResponseObjectAPI.ResponseLogin;
import com.google.gson.Gson;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends AppCompatActivity {

    EditText loginUsername, loginPassword;

    Button loginBtn, exitBtn;

    private boolean permission = false;
    private TelephonyManager telephonyManager;

    String adminUsername, adminPassword, adminImei;

    private ResponseLogin responseLogin = new ResponseLogin();

    private Gson gson = new Gson();
    private ProgressDialog progressDialog;

    private SharedPreferences sharedpreferencesCompanyTab;
    private SharedPreferences sharedpreferencesMenuCategory;
    private SharedPreferences sharedpreferencesCompanyOrderID;
    private SharedPreferences sharedpreferencesPersonalOrderID;
    private SharedPreferences sharedpreferencesAdminId;
    private SharedPreferences sharedpreferencesLogId;
    private SharedPreferences sharedpreferencesCustDataId;
    private SharedPreferences sharedpreferencesSurveyId;
    private SharedPreferences sharedpreferencesSpouseDataId;
    private SharedPreferences sharedpreferencesPersonalKKiD;
    private SharedPreferences sharedpreferencesPersonalKKImage;
    private SharedPreferences sharedpreferencesSpouseKKiD;
    private SharedPreferences sharedpreferencesSpouseKKImage;
    private SharedPreferences sharedpreferencesIsActive;
    private SharedPreferences sharedpreferencesUserName;
    private SharedPreferences sharedpreferencesPassword;
    private SharedPreferences sharedpreferencesDate;
    private SharedPreferences sharedpreferencesAdminName;
    private SharedPreferences sharedpreferencesAdminNik;

    private ApiInterface apiInterface;
    MD5Hashing md5Hashing = new MD5Hashing();
    private FileFunction fileFunction = new FileFunction();
    private String isActive = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Window window = getWindow();
        window.setStatusBarColor(getResources().getColor(R.color.color_white));
        setSharedPreference();

        ImageView imageViewLogoLogin = findViewById(R.id.image_logo_login);

        int mustChange = getSharedPreferences(Constans.KeySharedPreference.DATE, MODE_PRIVATE).getInt(Constans.KEY_COMPARE_DATE, -1);
        boolean isTimetoChange = getSharedPreferences(Constans.KeySharedPreference.DATE, MODE_PRIVATE).getBoolean(Constans.KEY_AFTER_DATE, false);

        if(mustChange == 1 || isTimetoChange){
            imageViewLogoLogin.setImageDrawable(getDrawable(R.drawable.logobafbaru));
        }

//        fileFunction.deleteAllFileInDir();
        progressDialog = new ProgressDialog(ActivityLogin.this);
        progressDialog.setMessage("Loading.........");
        progressDialog.setCancelable(false);
        apiInterface = ApiClient.getClient().create(ApiInterface.class);

        loginUsername = findViewById(R.id.username);
        loginPassword = findViewById(R.id.password);

        loginBtn = findViewById(R.id.loginButton);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adminUsername = loginUsername.getText().toString();
                try {
                    adminPassword = md5Hashing.encrypt(loginPassword.getText().toString());
                } catch (NoSuchAlgorithmException e) {
                    adminPassword = "";
                    e.printStackTrace();
                }
                doLogin(adminUsername, adminPassword);
            }
        });

        exitBtn = findViewById(R.id.exit);
        exitBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                finishAffinity();
                System.exit(0);
                }
        });
    }

    public void doLogin(String string1, String string2) {

        if (string1.equals("") || string2.equals("")) {
            Snackbar.make(findViewById(R.id.activity_login), R.string.kosong, Snackbar.LENGTH_LONG).show();
        } else {
            Log.d("Password", string2);
            ModelRequestLogin modelRequestLogin = new ModelRequestLogin();
            modelRequestLogin.setAdminUsername(string1);
            modelRequestLogin.setAdminPassword(string2);
            authLogin(modelRequestLogin);
        }
    }

    private void authLogin(final ModelRequestLogin modelRequestLogin){
        progressDialog.show();
        if (InternetConnection.checkConnection(this)) {
            Map<String, String> mapImei = InternetConnection.getImei(this);
            telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            adminImei = "000000000000000";
            if (mapImei.get("isConnect").equals("1")) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 0);
                } else {
                    permission = true;
                }
                if (permission) {
                    assert telephonyManager != null;
                    adminImei = telephonyManager.getDeviceId(Integer.parseInt(String.valueOf(mapImei.get("idx"))));
                }
            }
            modelRequestLogin.setAdminImei(adminImei);
            if (InternetConnection.checkConnection(this)) {
                final Call<ResponseLogin> authLogin = apiInterface.authLogin(modelRequestLogin);
                authLogin.enqueue(new Callback<ResponseLogin>() {
                    @Override
                    public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin>
                            response) {
                        if (response.isSuccessful()) {
                            responseLogin = response.body();

                            if (responseLogin.getStatus().equals("success")) {
                                if (responseLogin.getModelResponseLogin().getAdminImei().equals("1")) {
                                    setSharedPreference();
                                    Intent i = new Intent(ActivityLogin.this, MainActivityDrawer.class);
                                    SharedPreferences.Editor editor = sharedpreferencesAdminId.edit();
                                    editor.putString(Constans.KEY_ADMIN_ID, responseLogin.getModelResponseLogin().getAdminId());
                                    editor.apply();

                                    editor = sharedpreferencesUserName.edit();
                                    editor.putString(Constans.KEY_USERNAME, modelRequestLogin.getAdminUsername());
                                    editor.apply();

                                    editor = sharedpreferencesAdminName.edit();
                                    editor.putString(Constans.KEY_ADMIN_NAME, responseLogin.getModelResponseLogin().getAdminName());
                                    editor.apply();

                                    editor = sharedpreferencesAdminNik.edit();
                                    editor.putString(Constans.KEY_ADMIN_NIK, responseLogin.getModelResponseLogin().getAdminNik());
                                    editor.apply();

                                    editor = sharedpreferencesPassword.edit();
                                    editor.putString(Constans.KEY_PASSWORD, modelRequestLogin.getAdminPassword());
                                    editor.apply();

                                    editor = sharedpreferencesIsActive.edit();
                                    editor.putString(Constans.KEY_IS_ACTIVE, "1");
                                    editor.apply();

                                    i.putExtra(Constans.KEY_MENU, 0);
                                    startActivity(i);
                                    finish();
                                } else if (responseLogin.getModelResponseLogin().getAdminImei().equals("2")) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogin.this);
                                    builder.setTitle("Confirm Dialog");
                                    builder.setMessage("Your Account has been Registered in other Device\n" +
                                            "Please contact admin");
                                    builder.setCancelable(false);
                                    builder.setNegativeButton("OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog,
                                                                    int which) {

                                                    Log.e("info", "NO");
                                                }
                                            });
                                    builder.show();
                                }
                            }

                            progressDialog.dismiss();
                        } else if (response.code() == 404) {
                            String error = "";
                            try {
                                responseLogin = gson.fromJson(response.errorBody().string(), ResponseLogin.class);
                                error = responseLogin.getMessage();
                            } catch (IOException e) {
                                error = e.getMessage();
                                e.printStackTrace();
                            }
                            Log.e("Error", error);
                            Snackbar.make(findViewById(R.id.activity_login), error,
                                    Snackbar.LENGTH_LONG)
                                    .show();
                            progressDialog.dismiss();
                        } else {
                            progressDialog.dismiss();
                            Snackbar.make(findViewById(R.id.activity_login), R.string.error_api,
                                    Snackbar.LENGTH_LONG)
                                    .show();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseLogin> call, Throwable t) {
                        Log.e("Error Retrofit", t.toString());
                        Snackbar.make(findViewById(R.id.activity_login), t.toString(),
                                Snackbar.LENGTH_LONG)
                                .show();
                        progressDialog.dismiss();
                    }
                });
            } else {
                progressDialog.dismiss();
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityLogin.this);
                builder.setTitle("Confirm Dialog");
                builder.setMessage("Cannot connect to server, try again ?");
                builder.setCancelable(false);
                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                finishAffinity();
                                Log.e("info", "NO");
                            }
                        });
                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        authLogin(modelRequestLogin);
                        Log.e("info", "YES");
                    }
                });
                builder.show();
            }
        }
    }

    private void setSharedPreference(){
        sharedpreferencesCompanyTab = getSharedPreferences(Constans.KeySharedPreference.CURRENT_TAB, Context.MODE_PRIVATE);
        sharedpreferencesMenuCategory = getSharedPreferences(Constans.KeySharedPreference.MENU_CATEGORY, Context.MODE_PRIVATE);
        sharedpreferencesCompanyOrderID = getSharedPreferences(Constans.KeySharedPreference.COMPANY_ORDER_ID, Context.MODE_PRIVATE);
        sharedpreferencesPersonalOrderID = getSharedPreferences(Constans.KeySharedPreference.PERSONAL_ORDER_ID, Context.MODE_PRIVATE);
        sharedpreferencesAdminId = getSharedPreferences(Constans.KeySharedPreference.ADMIN_ID, Context.MODE_PRIVATE);
        sharedpreferencesLogId = getSharedPreferences(Constans.KeySharedPreference.LOG_ID, Context.MODE_PRIVATE);
        sharedpreferencesCustDataId = getSharedPreferences(Constans.KeySharedPreference.CUST_DATA_ID, Context.MODE_PRIVATE);
        sharedpreferencesSurveyId = getSharedPreferences(Constans.KeySharedPreference.SURVEY_ID, Context.MODE_PRIVATE);
        sharedpreferencesSpouseDataId = getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, Context.MODE_PRIVATE);
        sharedpreferencesPersonalKKiD = getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, Context.MODE_PRIVATE);
        sharedpreferencesPersonalKKImage = getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, Context.MODE_PRIVATE);
        sharedpreferencesSpouseKKiD = getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, Context.MODE_PRIVATE);
        sharedpreferencesSpouseKKImage= getSharedPreferences(Constans.KeySharedPreference.SPOUSE_ID, Context.MODE_PRIVATE);
        sharedpreferencesIsActive= getSharedPreferences(Constans.KeySharedPreference.IS_ACTIVE, Context.MODE_PRIVATE);
        sharedpreferencesUserName= getSharedPreferences(Constans.KeySharedPreference.USERNAME, Context.MODE_PRIVATE);
        sharedpreferencesPassword= getSharedPreferences(Constans.KeySharedPreference.PASSWORD, Context.MODE_PRIVATE);
        sharedpreferencesDate= getSharedPreferences(Constans.KeySharedPreference.DATE, Context.MODE_PRIVATE);
        sharedpreferencesAdminName= getSharedPreferences(Constans.KeySharedPreference.ADMIN_NAME, Context.MODE_PRIVATE);
        sharedpreferencesAdminNik= getSharedPreferences(Constans.KeySharedPreference.ADMIN_NIK, Context.MODE_PRIVATE);
    }

    private void clearSharedPreference(){
        sharedpreferencesCompanyTab.edit().clear().apply();
        sharedpreferencesMenuCategory.edit().clear().apply();
        sharedpreferencesCompanyOrderID.edit().clear().apply();
        sharedpreferencesPersonalOrderID.edit().clear().apply();
        sharedpreferencesAdminId.edit().clear().apply();
        sharedpreferencesLogId.edit().clear().apply();
        sharedpreferencesCustDataId.edit().clear().apply();
        sharedpreferencesSurveyId.edit().clear().apply();
        sharedpreferencesSpouseDataId.edit().clear().apply();
        sharedpreferencesPersonalKKiD.edit().clear().apply();
        sharedpreferencesPersonalKKImage.edit().clear().apply();
        sharedpreferencesSpouseKKiD.edit().clear().apply();
        sharedpreferencesSpouseKKImage.edit().clear().apply();
        sharedpreferencesIsActive.edit().clear().apply();
        sharedpreferencesUserName.edit().clear().apply();
        sharedpreferencesPassword.edit().clear().apply();
        sharedpreferencesDate.edit().clear().apply();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == 0) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.d("Permisson",""+permissions);
                permission = true;
                Log.d("Permisson",""+permissions);
            }
        }
    }


    @Override
    public void onBackPressed() {
        finishAffinity();
        System.exit(0);
        }
}