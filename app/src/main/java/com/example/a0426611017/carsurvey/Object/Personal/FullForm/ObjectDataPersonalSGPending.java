package com.example.a0426611017.carsurvey.Object.Personal.FullForm;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.a0426611017.carsurvey.MainFunction.DatePicker;
import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.MainFunction.PreviewImage;
import com.example.a0426611017.carsurvey.Model.Personal.ModelDataPersonalSG;
import com.example.a0426611017.carsurvey.R;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLPersonal;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by 0426591017 on 2/12/2018.
 */

public class ObjectDataPersonalSGPending {
    private View view;
    private Context context;
    private ViewGroup container;
    private DatePicker datePicker = new DatePicker();
    private ModelDDLPersonal ddlPersonal;

    private EditText editTextNameSg, editTextIdNumberSg,
            editTextIdExpiredDateSg, editTextBirthPlaceSg,
            editTextBirthDateSg, editTextNpwpNumberSg,
            editTextMotherMaidenNameSg;

    private TextView namePersonalIdImageSg, namePersonalIdImageSgUrl,
            namePersonalNpwpImageSg, namePersonalNpwpImageSgUrl;

    private Button buttonIdImageSg, buttonPersonalNpwpImageSg;

    private Spinner dropdownSpouseGuarantorModelSG, dropdownIdTypeSG,
            dropdownGenderSG;

    public ObjectDataPersonalSGPending(ViewGroup viewGroup, LayoutInflater inflater, ModelDDLPersonal ddlPersonal) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.ddlPersonal = ddlPersonal;
        this.view = inflater.inflate(R.layout.fragment_data_personal_sg_pending, container, false);

        setField();
    }

    public View getView() {
        return view;
    }

    public TextView getTextViewPersonalIdImageSg() {
        return namePersonalIdImageSg;
    }

    public TextView getTextViewPersonalIdImageSgUrl() {
        return namePersonalIdImageSgUrl;
    }

    public TextView getTextViewPersonalNpwpSg() {
        return namePersonalNpwpImageSg;
    }

    public TextView getTextViewPersonalNpwpSgUrl() {
        return namePersonalNpwpImageSgUrl;
    }

    public Button getButtonIdImageSg() {
        return buttonIdImageSg;
    }

    public Button getButtonPersonalNpwpSg() {
        return buttonPersonalNpwpImageSg;
    }

    public void setFilePersonalIdImageSg(String filePersonalIdImageSg) {
        this.namePersonalIdImageSg.setText(filePersonalIdImageSg);
    }

    public void setFilePersonalIdImageSgUrl(String namePersonalIdImageSgUrl) {
        this.namePersonalIdImageSgUrl.setText(namePersonalIdImageSgUrl);
    }

    public void setFilePersonalNpwpSg(String filePersonalNpwpSg) {
        this.namePersonalNpwpImageSg.setText(filePersonalNpwpSg);
    }

    public void setFilePersonalNpwpSgUrl(String namePersonalNpwpImageSgUrl) {
        this.namePersonalNpwpImageSgUrl.setText(namePersonalNpwpImageSgUrl);
    }

    public void setEnabledButtonImage(){
//        buttonIdImageSg.setEnabled(true);
        buttonPersonalNpwpImageSg.setEnabled(true);
    }

    private void setField(){
        dropdownSpouseGuarantorModelSG = view.findViewById(R.id.spinner_spouse_guarantor_model_sg);
        dropdownIdTypeSG = view.findViewById(R.id.spinner_id_type_sg);
        editTextNameSg = view.findViewById(R.id.edit_text_name_sg);

        namePersonalIdImageSg = view.findViewById(R.id.text_view_personal_id_image_sg);
        namePersonalIdImageSg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    namePersonalIdImageSg.setError(null);
                }
            }
        });
        namePersonalIdImageSgUrl = view.findViewById(R.id.text_view_personal_id_image_url_sg);
        buttonIdImageSg = view.findViewById(R.id.button_camera_id_image_sg);

        editTextIdNumberSg = view.findViewById(R.id.edit_text_id_number_sg);
        editTextIdExpiredDateSg = view.findViewById(R.id.edit_text_id_expired_date_sg);
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, editTextIdExpiredDateSg, context);
        dropdownGenderSG = view.findViewById(R.id.spinner_gender_sg);
        editTextBirthPlaceSg = view.findViewById(R.id.edit_text_birth_place_sg);
        editTextBirthDateSg = view.findViewById(R.id.edit_text_birth_date_sg);
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, editTextBirthDateSg, context);
        editTextNpwpNumberSg = view.findViewById(R.id.edit_text_npwp_num_sg);
        editTextNpwpNumberSg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() < 15) {
                    editTextNpwpNumberSg.setError("Minimum 15 digits");
                }
            }
        });

        namePersonalNpwpImageSg = view.findViewById(R.id.text_view_personal_npwp_sg);
        namePersonalNpwpImageSg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!s.toString().equals("No selected file")){
                    namePersonalNpwpImageSg.setError(null);
                }
            }
        });
        namePersonalNpwpImageSgUrl = view.findViewById(R.id.text_view_personal_npwp_url_sg);
        buttonPersonalNpwpImageSg = view.findViewById(R.id.button_camera_npwp_sg);

        editTextMotherMaidenNameSg = view.findViewById(R.id.edit_text_mother_maiden_name_sg);

        if(ddlPersonal != null){
            dropdownSpouseGuarantorModelSG.setAdapter(setAdapterItemList(ddlPersonal.getCustomerModel()));
            dropdownIdTypeSG.setAdapter(setAdapterItemList(ddlPersonal.getIdType()));
            dropdownIdTypeSG.setClickable(false);
            dropdownIdTypeSG.setEnabled(false);
            dropdownGenderSG.setAdapter(setAdapterItemList(ddlPersonal.getGender()));
        }

        buttonIdImageSg.setEnabled(false);
        buttonPersonalNpwpImageSg.setEnabled(false);
    }

    public void setForm(ModelDataPersonalSG modelDataPersonalSG) {
        Log.d("DDL PERSONAL", ddlPersonal.getCustomerModel().toString());
        if (ddlPersonal != null) {
            dropdownSpouseGuarantorModelSG.setSelection(ddlPersonal.getCustomerModel().indexOf(modelDataPersonalSG.getCustmodel()));
            dropdownIdTypeSG.setSelection(ddlPersonal.getIdType().indexOf(modelDataPersonalSG.getIdtype()));
            dropdownGenderSG.setSelection(ddlPersonal.getGender().indexOf(modelDataPersonalSG.getGenderid()));
        }

        editTextNameSg.setText(modelDataPersonalSG.getCustname());
        editTextIdNumberSg.setText(modelDataPersonalSG.getIdnumber());

        if (modelDataPersonalSG.getIdexpdate() != null && !modelDataPersonalSG.getIdexpdate().equals("")) {
            modelDataPersonalSG.setIdexpdate(
                    modelDataPersonalSG.getIdexpdate().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelDataPersonalSG.getIdexpdate().substring(4, 6))]
                            + " " + modelDataPersonalSG.getIdexpdate().substring(0, 4)
            );
        }
        editTextIdExpiredDateSg.setText(modelDataPersonalSG.getIdexpdate());

        editTextBirthPlaceSg.setText(modelDataPersonalSG.getBirthplace());

        if (modelDataPersonalSG.getBirthdate() != null && !modelDataPersonalSG.getBirthdate().equals("")) {
            modelDataPersonalSG.setBirthdate(
                    modelDataPersonalSG.getBirthdate().substring(6, 8)
                            + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelDataPersonalSG.getBirthdate().substring(4, 6))]
                            + " " + modelDataPersonalSG.getBirthdate().substring(0, 4)
            );
        }
        editTextBirthDateSg.setText(modelDataPersonalSG.getBirthdate());

        editTextNpwpNumberSg.setText(modelDataPersonalSG.getNpwpnum());

        editTextMotherMaidenNameSg.setText(modelDataPersonalSG.getMothermaidenname());

        namePersonalIdImageSgUrl.setText(modelDataPersonalSG.getIdimgurl());
        namePersonalNpwpImageSgUrl.setText(modelDataPersonalSG.getNpwpimgurl());

        Log.d("Url image", namePersonalIdImageSgUrl.getText().toString());
        PreviewImage previewImage = new PreviewImage();
        String nameIdImage;
        if (modelDataPersonalSG.getIdimgurl().equals("") || modelDataPersonalSG.getIdimgurl() == null) {
            nameIdImage = "No selected file";
        } else {
            if (modelDataPersonalSG.getIdimgurl().startsWith("http")) {
                nameIdImage = modelDataPersonalSG.getIdimgurl().split("/")[6];
                previewImage.setPreviewImage(modelDataPersonalSG.getIdimgurl(), context, namePersonalIdImageSg);
            } else {
                nameIdImage = modelDataPersonalSG.getIdimg();
                previewImage.setPreviewImage(nameIdImage, context, namePersonalIdImageSg);
            }
        }
        namePersonalIdImageSg.setText(nameIdImage);

        Log.d("Url image", namePersonalNpwpImageSgUrl.getText().toString());
        PreviewImage previewNpwpImage = new PreviewImage();
        String nameNpwpImage;
        if (modelDataPersonalSG.getNpwpimgurl().equals("") || modelDataPersonalSG.getNpwpimgurl() == null) {
            nameNpwpImage = "No selected file";
        } else {
            if (modelDataPersonalSG.getNpwpimgurl().startsWith("http")) {
                nameNpwpImage = modelDataPersonalSG.getNpwpimgurl().split("/")[6];
                previewNpwpImage.setPreviewImage(modelDataPersonalSG.getNpwpimgurl(), context, namePersonalNpwpImageSg);
            } else {
                nameNpwpImage = modelDataPersonalSG.getNpwpimg();
                previewNpwpImage.setPreviewImage(nameIdImage, context, namePersonalNpwpImageSg);
            }
        }
        namePersonalNpwpImageSg.setText(nameNpwpImage);
    }

    public ModelDataPersonalSG getForm() {
        ModelDataPersonalSG modelDataPersonalSG = new ModelDataPersonalSG();

        modelDataPersonalSG.setCustmodel(dropdownSpouseGuarantorModelSG.getSelectedItem().toString());
        modelDataPersonalSG.setIdtype(dropdownIdTypeSG.getSelectedItem().toString());
        modelDataPersonalSG.setCustname(editTextNameSg.getText().toString());
        modelDataPersonalSG.setIdimg(namePersonalIdImageSg.getText().toString());
        modelDataPersonalSG.setIdimgurl(namePersonalIdImageSgUrl.getText().toString());
        modelDataPersonalSG.setIdnumber(editTextIdNumberSg.getText().toString());
        modelDataPersonalSG.setIdexpdate(editTextIdExpiredDateSg.getText().toString());

        if (modelDataPersonalSG.getIdexpdate() != null) {
            if (!modelDataPersonalSG.getIdexpdate().equals("")) {
                modelDataPersonalSG.setIdexpdate(
                        modelDataPersonalSG.getIdexpdate().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(modelDataPersonalSG.getIdexpdate().split(" ")[1])),
                                2,
                                "0"
                        )
                                + modelDataPersonalSG.getIdexpdate().split(" ")[0]
                );
            }
        }
        modelDataPersonalSG.setGenderid(dropdownGenderSG.getSelectedItem().toString());
        modelDataPersonalSG.setBirthplace(editTextBirthPlaceSg.getText().toString());
        modelDataPersonalSG.setBirthdate(editTextBirthDateSg.getText().toString());

        if (modelDataPersonalSG.getBirthdate() != null) {
            if (!modelDataPersonalSG.getBirthdate().equals("")) {
                modelDataPersonalSG.setBirthdate(
                        modelDataPersonalSG.getBirthdate().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(modelDataPersonalSG.getBirthdate().split(" ")[1])),
                                2,
                                "0"
                        )
                                + modelDataPersonalSG.getBirthdate().split(" ")[0]
                );
            }
        }
        modelDataPersonalSG.setNpwpnum(editTextNpwpNumberSg.getText().toString());
        modelDataPersonalSG.setNpwpimg(namePersonalNpwpImageSg.getText().toString());
        modelDataPersonalSG.setNpwpimgurl(namePersonalNpwpImageSgUrl.getText().toString());
        modelDataPersonalSG.setMothermaidenname(editTextMotherMaidenNameSg.getText().toString());

        modelDataPersonalSG.setModified(true);

        return modelDataPersonalSG;
    }

    private ArrayAdapter<String> setAdapterItemList(List items) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl, items);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;
    }

    public ModelDataPersonalSG getComplete() {
        ModelDataPersonalSG modelDataPersonalSG = getForm();
        modelDataPersonalSG.setComplete(true);

        // TODO: FIELD NOT MANDATORY FROM HERE
//        if (modelDataPersonalSG.getIdexpdate().length() == 0) {
//            editTextIdNumberSg.setError("ID Expired Date is required");
//            modelDataPersonalSG.setComplete(false);
//        }else if(modelDataPersonalSG.getIdexpdate().trim().length() == 0){
//            editTextIdNumberSg.setError("ID Expired Date not allowed whitespaces only");
//            modelDataPersonalSG.setComplete(false);
//        }

        if (modelDataPersonalSG.getBirthplace().length() == 0) {
            editTextIdExpiredDateSg.setError("Birth Place is required");
            modelDataPersonalSG.setComplete(false);
        }else if(modelDataPersonalSG.getBirthplace().trim().length() == 0){
            editTextIdExpiredDateSg.setError("Birth Place not allowed whitespaces only");
            modelDataPersonalSG.setComplete(false);
        }

        if (modelDataPersonalSG.getBirthplace().length() == 0) {
            editTextBirthPlaceSg.setError("Birth Place is required");
            modelDataPersonalSG.setComplete(false);
        }else if(modelDataPersonalSG.getBirthplace().trim().length() == 0){
            editTextBirthPlaceSg.setError("Birth Place not allowed whitespaces only");
            modelDataPersonalSG.setComplete(false);
        }

        if (modelDataPersonalSG.getBirthdate().length() == 0) {
            editTextBirthDateSg.setError("Birth Date is required");
            modelDataPersonalSG.setComplete(false);
        }else if(modelDataPersonalSG.getBirthdate().trim().length() == 0){
            editTextBirthDateSg.setError("Birth Date not allowed whitespaces only");
            modelDataPersonalSG.setComplete(false);
        }

        if (modelDataPersonalSG.getNpwpnum().length() == 0) {
            editTextNpwpNumberSg.setError("NPWP is required");
            modelDataPersonalSG.setComplete(false);
        }else if(modelDataPersonalSG.getNpwpnum().trim().length() == 0){
            editTextNpwpNumberSg.setError("NPWP not allowed whitespaces only");
            modelDataPersonalSG.setComplete(false);
        } else if (modelDataPersonalSG.getNpwpnum().trim().length() != 15) {
            editTextNpwpNumberSg.setError("Minimum and maximum 15 digits");
            modelDataPersonalSG.setComplete(false);
        }

        if(modelDataPersonalSG.getNpwpimg().length() == 0){
            namePersonalNpwpImageSg.setError("NPWP Image is Required");
            modelDataPersonalSG.setComplete(false);
        }else if(modelDataPersonalSG.getNpwpimg().equals("No selected file")){
            namePersonalNpwpImageSg.setError("NPWP Image is Required");
            modelDataPersonalSG.setComplete(false);
        }else{
            namePersonalNpwpImageSg.setError(null);
        }

        if (modelDataPersonalSG.getMothermaidenname().length() == 0) {
            editTextMotherMaidenNameSg.setError("Mother Maiden Name is required");
            modelDataPersonalSG.setComplete(false);
        }else if(modelDataPersonalSG.getMothermaidenname().trim().length() == 0){
            editTextMotherMaidenNameSg.setError("Mother Maiden Name not allowed whitespaces only");
            modelDataPersonalSG.setComplete(false);
        }

        return modelDataPersonalSG;
    }
}
