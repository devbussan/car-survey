package com.example.a0426611017.carsurvey.Object.Company.FullForm;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.a0426611017.carsurvey.MainFunction.DatePicker;
import com.example.a0426611017.carsurvey.MainFunction.FormatDate;
import com.example.a0426611017.carsurvey.Model.Company.ModelCompanyInfo;
import com.example.a0426611017.carsurvey.Model.DDL.ModelDDLCompany;
import com.example.a0426611017.carsurvey.R;

import org.apache.commons.lang.StringUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Created by c201017001 on 02/01/2018.
 */

public class ObjectCompanyInfoFull {

    private ViewGroup container;
    private View view;
    private Context context;
    private EditText customerName, npwp, industryType, numOfEmployee, establismentDate;
    private Spinner customerModel, companyType;

    private ModelDDLCompany ddlCompany;

    public ObjectCompanyInfoFull(ViewGroup viewGroup, LayoutInflater inflater, ModelDDLCompany ddlCompany) {
        this.context = viewGroup.getContext();
        this.container = viewGroup;
        this.view = inflater.inflate(R.layout.fragment_company_info_pending, container, false);
        this.ddlCompany = ddlCompany;

        findView();
    }

    public View getView() {
        return view;
    }

    public void findView() {
        customerName = view.findViewById(R.id.editTextNamePending);
        npwp = view.findViewById(R.id.editTextNpwpPending);

        customerModel = view.findViewById(R.id.spinnerModelPending);
        companyType = view.findViewById(R.id.spinnerTypePending);

        if (ddlCompany != null) {
            customerModel.setAdapter(setAdapterItemList(ddlCompany.getCompanyModel()));
            companyType.setAdapter(setAdapterItemList(ddlCompany.getCompanyType()));
        }

        industryType = view.findViewById(R.id.editTextIndustryTypePending);
        numOfEmployee = view.findViewById(R.id.editTextEmployeePending);
        establismentDate = view.findViewById(R.id.editTextEstablismentPending);

        DatePicker datePicker = new DatePicker();
        datePicker.callDatePicker(FormatDate.FORMAT_SPACE_ddMMMMyyyy, establismentDate, context);

    }

    public void setForm(ModelCompanyInfo modelCompanyInfo) {

        customerName.setText(modelCompanyInfo.getCompanyName());
        npwp.setText(modelCompanyInfo.getNpwp());

        if (ddlCompany != null) {
            customerModel.setSelection(ddlCompany.getCompanyModel().indexOf(modelCompanyInfo.getCustomerModel()));
            companyType.setSelection(ddlCompany.getCompanyType().indexOf(modelCompanyInfo.getCompanyType()));
        }

        industryType.setText(modelCompanyInfo.getIndustryType());
        numOfEmployee.setText(modelCompanyInfo.getNumOfEmployee());

        if (modelCompanyInfo.getEstablismentDate() != null) {
            if(!modelCompanyInfo.getEstablismentDate().equals("")) {
                modelCompanyInfo.setEstablismentDateFormat(
                        modelCompanyInfo.getEstablismentDate().substring(6, 8)
                                + " " + FormatDate.PRINT_MONTH[Integer.parseInt(modelCompanyInfo.getEstablismentDate().substring(4, 6))]
                                + " " + modelCompanyInfo.getEstablismentDate().substring(0, 4)
                );
            }
        }
        establismentDate.setText(modelCompanyInfo.getEstablismentDateFormat());
    }

    public ModelCompanyInfo getForm() {
        ModelCompanyInfo modelCompanyInfo = new ModelCompanyInfo();

        modelCompanyInfo.setCompanyName(customerName.getText().toString());
        modelCompanyInfo.setNpwp(npwp.getText().toString());
        modelCompanyInfo.setCustomerModel(customerModel.getSelectedItem().toString());
        modelCompanyInfo.setCompanyType(companyType.getSelectedItem().toString());
        modelCompanyInfo.setIndustryType(industryType.getText().toString());
        modelCompanyInfo.setNumOfEmployee(numOfEmployee.getText().toString());
        modelCompanyInfo.setEstablismentDateFormat(establismentDate.getText().toString());
        if (modelCompanyInfo.getEstablismentDateFormat() != null) {
            if (!modelCompanyInfo.getEstablismentDateFormat().equals("")) {
                modelCompanyInfo.setEstablismentDate(
                        modelCompanyInfo.getEstablismentDateFormat().split(" ")[2]
                                + StringUtils.leftPad(
                                String.valueOf(Arrays.asList(FormatDate.PRINT_MONTH).indexOf(modelCompanyInfo.getEstablismentDateFormat().split(" ")[1])),
                                2,
                                "0"
                        )
                                + modelCompanyInfo.getEstablismentDateFormat().split(" ")[0]
                );
            }
        }
        modelCompanyInfo.setModified(true);

        return modelCompanyInfo;
    }

    public ModelCompanyInfo getComplete() {
        ModelCompanyInfo modelCompanyInfo = getForm();
        modelCompanyInfo.setComplete(true);

        if (modelCompanyInfo.getCompanyName().length() == 0) {
            customerName.setError("Required");
            modelCompanyInfo.setComplete(false);
        } else if (modelCompanyInfo.getCompanyName().trim().length() == 0) {
            customerName.setError("Whitespaces only detected");
            customerName.setText(null);
            modelCompanyInfo.setComplete(false);
        }

        if (modelCompanyInfo.getNpwp().length() == 0) {
            npwp.setError("Required");
            modelCompanyInfo.setComplete(false);
        } else if (modelCompanyInfo.getNpwp().trim().length() == 0) {
            npwp.setError("Whitespaces only detected");
            npwp.setText(null);
            modelCompanyInfo.setComplete(false);
        }else if (modelCompanyInfo.getNpwp().trim().length() != 15){
            npwp.setError("Minimum and maximum 15 digits");
            modelCompanyInfo.setComplete(false);
        }

//        if(modelCompanyInfo.getIndustryType().length() == 0){
//            industryType.setError("Required");
//            modelCompanyInfo.setComplete(false);
//        }else if(modelCompanyInfo.getIndustryType().trim().length() == 0){
//            industryType.setError("Whitespaces only detected");
//            modelCompanyInfo.setComplete(false);
//        }

        if(modelCompanyInfo.getNumOfEmployee().length() == 0){
            numOfEmployee.setError("Required");
            modelCompanyInfo.setComplete(false);
        }else if(modelCompanyInfo.getNumOfEmployee().trim().length() == 0){
            numOfEmployee.setError("Whitespaces only detected");
            modelCompanyInfo.setComplete(false);
        }

        if(modelCompanyInfo.getEstablismentDateFormat().length() == 0){
            establismentDate.setError("Required");
            modelCompanyInfo.setComplete(false);
        }else if(modelCompanyInfo.getEstablismentDateFormat().trim().length() == 0){
            establismentDate.setError("Whitespaces only detected");
            modelCompanyInfo.setComplete(false);
        }


        return modelCompanyInfo;
    }

    private ArrayAdapter<String> setAdapterItemList(List items) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.layout_dropdown_item, R.id.itemddl, items);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;

    }

}
