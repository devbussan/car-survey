package com.example.a0426611017.carsurvey.Fragment.Personal.Pending;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a0426611017.carsurvey.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

public class FragmentPersonalNextPending extends Fragment {

    View view;
    Context context;
    //for camera function
    String nameFile;
    int idxButton = 0;
    Uri file;
    // until here
    TextView fileNameIdImagePending, fileNamePersonalNpwpPending, fileNamePersonalBuildingImageLocationPending, fileNameFamilyCardImagePending, fileNameMonthlyFixedIncomeImagePending;
    Spinner dropdownCustomerModel, dropdownIdType, dropdownGender, dropdownAddressTypeName, dropdownBuildingLocationClass, dropdownBuildingOwnership, dropdownSalutation, dropdownMaritalStatus, dropdownReligion, dropdownEducation;
    private String[] itemsCustomerName = new String[]{"Profesional", "Employee", "Enterpreneur", "Non Professional"};
    private String[] itemsIdType = new String[]{"AKTA", "KTP", "eKTP", "KIMS", "KITAS", "NIK", "NPWP", "OTHER", "PASSPORT"};
    private String[] itemsGender = new String[]{"Pria", "Wanita"};
    private String[] itemsAddressTypeName = new String[]{"Legal Address", "Residence Address", "Emergency Address", "Business Address", "NPWP Address", ""};
    private String[] itemsBuildingLocationClass = new String[]{"Gang", "Gang Kecil", "Jalan Besar", "Jalan Kecil", "Other"};
    private String[] itemsBuildingOwnership = new String[]{"Perusahaan", "Keluarga", "KPR", "Kontrak", "Sendiri", "Rumah Pasangan", "Other", "Kost", "Keluarga (Orang Tua)", "Keluarga (Selain Keluarga)"};
    private String[] itemsSalutation = new String[]{"Tuan", "Nyonya", "Nona"};
    private String[] itemsMaritalStatus = new String[]{"Married", "Single", "Widow/Widower", "Other"};
    private String[] itemsReligion = new String[]{"Budha", "Hindu", "Islam", "Katolik", "Kong Hu Chu"};
    private String[] itemsEducation = new String[]{"Diploma 1", "Diploma 2", "Diploma 3", "Lainnya", "S-1", "S-2", "S-3", "SD", "SMP"};

    //DatePicker
    DatePickerDialog datePickerDialogPending;

    //ButtonCamera
    Button buttonIdImagePending, buttonPersonalNpwpPending, buttonPersonalBuildingImageLocationPending, buttonFamilyCardImagePending, buttonMonthlyFixedIncomeImagePending;

    /*Edit Text Personal Info*/
    EditText customerNamePending, idNumberPending, idExpiredDate, birthPlacePending, npwpPending, motherMaidenNamePending, personalAddressPending, personalRtPending;
    EditText personalRwPending, personalZipCodePending, personalKelurahanPending, personalKecamatanPending, personalCityPending, personalPhonePending, personalFaxPending;

    /*Edit Text Contact Info*/
    EditText buildingPriceEstimatesPending;
    EditText buildingStayLengthPending, directionDescriptionPending, notesPending, numDependentsPending, numOfResidencePending, familyCardNumberPending, kendaraanYangDimilikiPending, mobilePhone1Pending;
    EditText mobilePhone2Pending;

    /*Edit Text Occupation Info*/
    EditText profesionNamePending, jobPositionPending, jobStatusPending, namaPerusahaanPending, industryTypeNamePending, employmentEstablishmentPending, jobAddressPending, jobRtPending, jobRwPending;
    EditText jobZipCodePending, jobKelurahanPending, jobKecamatanPending, jobCityPending, jobPhone1Pending;

    /*Edit Text Occupation Info*/
    EditText emergencyContactNamePending, customerRelationshipPending, emergencyMobilePhone1Pending;
    EditText emergencyMobilePhone2Pending, emergencyContactAddressPending, emergencyRtPending, emergencyRwPending, emergencyZipCodePending, emergencyKelurahanPending, emergencyKecamatanPending;
    EditText emergencyCityPending, emergencyPhone1Pending;

    /*Edit Text Income Info*/
    EditText monthlyFixedIncomePending, monthlyVariabelIncomePending, otherBusinessIncomePending, spouseGuarantorIncomePending, monthlyLivingCostPending, otherLoanInstallmentPending;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        context = container.getContext();
        view = inflater.inflate(R.layout.fragment_personal_next_pending, container, false);
        //customer model pending
        dropdownCustomerModel = (Spinner) view.findViewById(R.id.spinner_customer_model_pending);
        dropdownCustomerModel.setAdapter(setItemList(itemsCustomerName));
        //id type
        dropdownIdType = (Spinner) view.findViewById(R.id.spinner_id_type_pending);
        dropdownIdType.setAdapter(setItemList(itemsIdType));
        //gender
        dropdownGender = (Spinner) view.findViewById(R.id.spinner_gender_pending);
        dropdownGender.setAdapter(setItemList(itemsGender));
        //address type name
        dropdownAddressTypeName = (Spinner) view.findViewById(R.id.spinner_address_type_name_pending);
        dropdownAddressTypeName.setAdapter(setItemList(itemsAddressTypeName));
        //building location class
        dropdownBuildingLocationClass = (Spinner) view.findViewById(R.id.spinner_building_location_class_pending);
        dropdownBuildingLocationClass.setAdapter(setItemList(itemsBuildingLocationClass));
        //building ownership
        dropdownBuildingOwnership = (Spinner) view.findViewById(R.id.spinner_building_ownership_pending);
        dropdownBuildingOwnership.setAdapter(setItemList(itemsBuildingOwnership));
        //salutation
        dropdownSalutation = (Spinner) view.findViewById(R.id.spinner_salutation_pending);
        dropdownSalutation.setAdapter(setItemList(itemsSalutation));
        //marital status
        dropdownMaritalStatus = (Spinner) view.findViewById(R.id.spinner_marital_status_pending);
        dropdownMaritalStatus.setAdapter(setItemList(itemsMaritalStatus));
        //religion
        dropdownReligion = (Spinner) view.findViewById(R.id.spinner_religion_pending);
        dropdownReligion.setAdapter(setItemList(itemsReligion));
        //education
        dropdownEducation = (Spinner) view.findViewById(R.id.spinner_education_pending);
        dropdownEducation.setAdapter(setItemList(itemsEducation));


        //edittext personal info pending
        customerNamePending = (EditText) view.findViewById(R.id.edit_text_customer_name_pending);
        idNumberPending = (EditText) view.findViewById(R.id.edit_text_id_number_pending);
        idExpiredDate = (EditText) view.findViewById(R.id.edit_text_id_expired_date_pending);
        birthPlacePending = (EditText) view.findViewById(R.id.edit_text_birth_place_pending);
        npwpPending = (EditText) view.findViewById(R.id.edit_text_npwp_pending);
        motherMaidenNamePending = (EditText) view.findViewById(R.id.edit_text_mother_maiden_name_pending);
        personalAddressPending = (EditText) view.findViewById(R.id.edit_text_personal_address_pending);
        personalRtPending = (EditText) view.findViewById(R.id.edit_text_personal_rt_pending);
        personalRwPending = (EditText) view.findViewById(R.id.edit_text_personal_rw_pending);
        personalZipCodePending = (EditText) view.findViewById(R.id.edit_text_personal_zip_code_pending);
        personalKelurahanPending = (EditText) view.findViewById(R.id.edit_text_personal_kelurahan_pending);
        personalKecamatanPending = (EditText) view.findViewById(R.id.edit_text_personal_kecamatan_pending);
        personalCityPending = (EditText) view.findViewById(R.id.edit_text_personal_city_pending);
        personalPhonePending = (EditText) view.findViewById(R.id.edit_text_personal_phone_pending);
        personalFaxPending = (EditText) view.findViewById(R.id.edit_text_personal_fax_pending);

        //edittext personal info pending
        buildingPriceEstimatesPending = (EditText) view.findViewById(R.id.edit_text_building_price_estimates_pending);
        buildingStayLengthPending = (EditText) view.findViewById(R.id.edit_text_building_stay_length_pending);
        directionDescriptionPending = (EditText) view.findViewById(R.id.edit_text_direction_description_pending);
        notesPending = (EditText) view.findViewById(R.id.edit_text_notes_pending);
        numDependentsPending = (EditText) view.findViewById(R.id.edit_text_num_dependents_pending);
        numOfResidencePending = (EditText) view.findViewById(R.id.edit_text_num_of_residences_pending);
        familyCardNumberPending = (EditText) view.findViewById(R.id.edit_text_family_card_number_pending);
        kendaraanYangDimilikiPending = (EditText) view.findViewById(R.id.edit_text_kendaraan_yang_dimiliki_pending);
        mobilePhone1Pending = (EditText) view.findViewById(R.id.edit_text_mobile_phone_1_pending);
        mobilePhone2Pending = (EditText) view.findViewById(R.id.edit_text_mobile_phone_2_pending);

        //edittext occupation info pending
        profesionNamePending = (EditText) view.findViewById(R.id.edit_text_profesion_name_pending);
        jobPositionPending = (EditText) view.findViewById(R.id.edit_text_job_position_pending);
        namaPerusahaanPending = (EditText) view.findViewById(R.id.edit_text_nama_perusahaan_usaha_pending);
        industryTypeNamePending = (EditText) view.findViewById(R.id.edit_text_industry_type_name_pending);
        employmentEstablishmentPending = (EditText) view.findViewById(R.id.edit_text_employment_establishment_date_pending);
        jobAddressPending = (EditText) view.findViewById(R.id.edit_text_job_address_pending);
        jobRtPending = (EditText) view.findViewById(R.id.edit_text_job_rt_pending);
        jobRwPending = (EditText) view.findViewById(R.id.edit_text_job_rw_pending);
        jobZipCodePending = (EditText) view.findViewById(R.id.edit_text_job_zip_code_pending);
        jobKelurahanPending = (EditText) view.findViewById(R.id.edit_text_job_kelurahan_pending);
        jobKecamatanPending = (EditText) view.findViewById(R.id.edit_text_job_kecamatan_pending);
        jobCityPending = (EditText) view.findViewById(R.id.edit_text_job_city_pending);
        jobPhone1Pending = (EditText) view.findViewById(R.id.edit_text_job_phone_1_pending);

        //edittext emergency pending
        emergencyContactNamePending = (EditText) view.findViewById(R.id.edit_text_emergency_contact_name_pending);
        customerRelationshipPending = (EditText) view.findViewById(R.id.edit_text_customer_relationship_pending);
        emergencyMobilePhone1Pending = (EditText) view.findViewById(R.id.edit_text_emergency_mobile_phone_1_pending);
        emergencyMobilePhone2Pending = (EditText) view.findViewById(R.id.edit_text_emergency_mobile_phone_2_pending);
        emergencyContactAddressPending = (EditText) view.findViewById(R.id.edit_text_emergency_contact_address_pending);
        emergencyRtPending = (EditText) view.findViewById(R.id.edit_text_emergency_rt_pending);
        emergencyRwPending = (EditText) view.findViewById(R.id.edit_text_emergency_rw_pending);
        emergencyZipCodePending = (EditText) view.findViewById(R.id.edit_text_emergency_zip_code_pending);
        emergencyKelurahanPending = (EditText) view.findViewById(R.id.edit_text_emergency_kelurahan_pending);
        emergencyKecamatanPending = (EditText) view.findViewById(R.id.edit_text_emergency_kecamatan_pending);
        emergencyCityPending = (EditText) view.findViewById(R.id.edit_text_emergency_city_pending);
        emergencyPhone1Pending = (EditText) view.findViewById(R.id.edit_text_emergency_phone_1_pending);

        //edittext income info pending
        monthlyFixedIncomePending = (EditText) view.findViewById(R.id.edit_text_monthly_fixed_income_pending);
        monthlyVariabelIncomePending = (EditText) view.findViewById(R.id.edit_text_monthly_variable_income_pending);
        otherBusinessIncomePending = (EditText) view.findViewById(R.id.edit_text_other_business_income_pending);
        spouseGuarantorIncomePending = (EditText) view.findViewById(R.id.edit_text_spouse_guarantor_income_pending);
        monthlyLivingCostPending = (EditText) view.findViewById(R.id.edit_text_monthly_living_cost_pending);
        otherLoanInstallmentPending = (EditText) view.findViewById(R.id.edit_text_other_loan_installment_pending);

        //camera name
        fileNameIdImagePending = (TextView) view.findViewById(R.id.text_view_personal_id_image_pending);
        fileNamePersonalNpwpPending = (TextView) view.findViewById(R.id.text_view_personal_npwp_pending);
        fileNamePersonalBuildingImageLocationPending = (TextView) view.findViewById(R.id.text_view_personal_building_location_image_pending);
        fileNameFamilyCardImagePending = (TextView) view.findViewById(R.id.text_view_personal_family_card_number_image_pending);
        fileNameMonthlyFixedIncomeImagePending = (TextView) view.findViewById(R.id.text_view_personal_monthly_fixed_income_image_pending);

        buttonFamilyCardImagePending = (Button) view.findViewById(R.id.button_camera_family_card_number_image_pending);
        buttonMonthlyFixedIncomeImagePending = (Button) view.findViewById(R.id.button_camera_monthly_fixed_income_image_pending);
        buttonIdImagePending = (Button) view.findViewById(R.id.button_camera_id_image_pending);
        buttonPersonalNpwpPending =  (Button) view.findViewById(R.id.button_camera_npwp_pending);
        buttonPersonalBuildingImageLocationPending = (Button) view.findViewById(R.id.button_camera_building_location_image_pending);

        callCamera(buttonIdImagePending, 1);
        callCamera(buttonPersonalNpwpPending, 2);
        callCamera(buttonPersonalBuildingImageLocationPending, 3);
        callCamera(buttonFamilyCardImagePending, 4);
        callCamera(buttonMonthlyFixedIncomeImagePending, 5);

        //datepicker
        EditText dateIdExpiredDatePending, dateEmploymentEstablishmentPending, dateBirthDatePending;

        //datepicker id's
        dateEmploymentEstablishmentPending = (EditText) view.findViewById(R.id.edit_text_employment_establishment_date_pending);
        dateIdExpiredDatePending = (EditText) view.findViewById(R.id.edit_text_id_expired_date_pending);
        dateBirthDatePending = (EditText) view.findViewById(R.id.edit_text_birth_date_pending);

        //datepicker pending
        callDatePicker(dateEmploymentEstablishmentPending);
        callDatePicker(dateIdExpiredDatePending);
        callDatePicker(dateBirthDatePending);


        //camera button
        // id image camera
//        Button idImageCamera = (Button) view.findViewById(R.id.button_camera_id_image_pending);
//        idImageCamera.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivity(i);
//            }
//        });
//
//        //npwp image camera
//        Button npwpImageCamera = (Button) view.findViewById(R.id.button_camera_npwp_pending);
//        npwpImageCamera.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivity(i);
//            }
//        });
//
//        //building location image camera
//        Button buildingLocationImageCamera = (Button) view.findViewById(R.id.button_camera_building_location_image_pending);
//        buildingLocationImageCamera.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivity(i);
//            }
//        });
//
//        //family card number image camera
//        Button familyCardNumberImageCamera = (Button) view.findViewById(R.id.button_camera_family_card_number_image_pending);
//        familyCardNumberImageCamera.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivity(i);
//            }
//        });
//
//        //monthly fixed income image
//        Button monthlyFixedIncomeImageCamera = (Button) view.findViewById(R.id.button_camera_monthly_fixed_income_image_pending);
//        monthlyFixedIncomeImageCamera.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View view) {
//                Intent i = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivity(i);
//            }
//        });

        return view;


    }

    private ArrayAdapter<String> setItemList(String[] items) {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity().getApplicationContext(), R.layout.layout_dropdown_item, R.id.itemddl, items);
        adapter.setDropDownViewResource(R.layout.layout_dropdown_item);
        return adapter;
    }

    private void callDatePicker(EditText editTextDate){
        final EditText date = editTextDate;
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
//              jika tanggal telah diset maka gunakan kondisi if dibawah
                String dateStringTemp = date.getText().toString();
                if(!dateStringTemp.equals("")){
                    mYear = Integer.parseInt(dateStringTemp.split("/")[2]);
                    mMonth = Integer.parseInt(dateStringTemp.split("/")[1])-1;
                    mDay = Integer.parseInt(dateStringTemp.split("/")[0]);
                }
                // date picker dialog
                datePickerDialogPending = new DatePickerDialog(context,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text

                                date.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialogPending.show();
            }

        });
        date.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    final Calendar c = Calendar.getInstance();
                    int mYear = c.get(Calendar.YEAR); // current year
                    int mMonth = c.get(Calendar.MONTH); // current month
                    int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                    String dateStringTemp = date.getText().toString();
                    if(!dateStringTemp.equals("")){
                        mYear = Integer.parseInt(dateStringTemp.split("/")[2]);
                        mMonth = Integer.parseInt(dateStringTemp.split("/")[1])-1;
                        mDay = Integer.parseInt(dateStringTemp.split("/")[0]);
                    }
                    // date picker dialog
                    datePickerDialogPending = new DatePickerDialog(context,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    // set day of month , month and year value in the edit text

                                    date.setText(dayOfMonth + "/"
                                            + (monthOfYear + 1) + "/" + year);

                                }
                            }, mYear, mMonth, mDay);
                    datePickerDialogPending.show();
                }
            }
        });
    }

    private void callCamera(Button button, int idx){
        final int idxButton = idx;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
//                startActivity(intent);
//                Toast.makeText(context,nameFile+":"+idxButton,Toast.LENGTH_SHORT).show();
                takePicture(v, idxButton);

            }
        });
    }

    public void takePicture(View view, int idx) {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        file = Uri.fromFile(getOutputMediaFile());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
//        intent.putExtra("idx", idx);
        this.idxButton = idx;

        startActivityForResult(intent, 100);

    }

    private File getOutputMediaFile(){
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "BAF");
        Log.i("Directory","Path : "+mediaStorageDir.getPath()+" || Absolute Path :"+mediaStorageDir.getAbsolutePath());
        if (!mediaStorageDir.exists()){
            if (!mediaStorageDir.mkdirs()){
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        nameFile = "IMG_"+ timeStamp + ".jpg";
        return new File(mediaStorageDir.getPath() + File.separator +
                "IMG_"+ timeStamp + ".jpg");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        Toast.makeText(context,requestCode+":"+resultCode,Toast.LENGTH_SHORT).show();
        if (requestCode == 100) {
            if (resultCode == RESULT_OK) {
                switch (idxButton){
                    case 1:
                        fileNameIdImagePending.setText(nameFile);
                        break;
                    case 2:
                        fileNamePersonalNpwpPending.setText(nameFile);
                        break;
                    case 3:
                        fileNamePersonalBuildingImageLocationPending.setText(nameFile);
                        break;
                    case 4:
                        fileNameFamilyCardImagePending.setText(nameFile);
                        break;
                    case 5:
                        fileNameMonthlyFixedIncomeImagePending.setText(nameFile);
                        break;
                    default:
                        break;
                }


                Toast.makeText(context,nameFile+":"+idxButton,Toast.LENGTH_SHORT).show();
            }
        }
    }

}
